(function() {

    var management_style_json = null;
    var management_script_json = null;
    
    window.management_load_css = function() {
        var arg = arguments,
            path = Array.prototype.splice.call(arg,0,1);
        for (var i = 0; i < arg.length ; i++) {
            var name = arg[i];
            var href = path + "/" + management_style_json[name];
            document.write('<link rel="stylesheet" type="text/css" href='+href+'>')
        }
    }

    window.management_load_script = function() {
        var arg = arguments,
            path = Array.prototype.splice.call(arg,0,1);
        for (var i = 0; i < arg.length ; i++) {
            var name = arg[i];
            var src = path + "/" + management_script_json[name];
            document.write('<script src='+ src +' type="text/javascript"></script>');
        }
    }

})();