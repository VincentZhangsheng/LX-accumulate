;(function($) {

	var ChangeOrderEquimentList = {
		init:function(prams) {
			this.props = _.extend({
				changeOrderProductId:null,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "change-order/equiment-list", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$cancelButton  = $('.cancelButton', this.$modal);
			this.$dataListTpl = $(".dataListTpl", this.$modal).html();
			this.$dataListTable = $(".dataListTable", this.$modal);

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			self.$cancelButton.click(function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.searchData();

			// self.$modal.off('click').on('click', '.chooseEquiment', function(event) {
			// 	event.preventDefault();
			// 	var array = $('[name=checkItem]').rtlCheckArray();
			// 	sessionStorage.setItem(Rental.common.key.print_equiment, JSON.stringify(array)); 
			// 	window.open(PageUrl.printCode);
			// });

			// self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		searchData:function(prams) {
			var self = this;

			if(!self.props.changeOrderProductId) {
				bootbox.alert('找不到换货单商品项ID');
				return;
			}

			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				changeOrderProductId:self.props.changeOrderProductId
			}, prams || {});
			
			Rental.ajax.submit("{0}changeOrder/pageChangeOrderProductEquipment".format(SitePath.service), commitData, function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载设备列表",response.description);
				}
			}, null ,"加载设备列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', self.$modal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					equipmentStatusStr:function() {
						return Enum.equipmentStatus.getValue(this.equipmentStatus);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.ChangeOrderEquimentList = ChangeOrderEquimentList;

})(jQuery)