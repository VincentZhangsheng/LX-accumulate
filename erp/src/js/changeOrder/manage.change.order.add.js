;(function($){
	
	var AddChangeOrder = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_change_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_change_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_change_order_add];
		},
		initDom:function() {
			this.$form = $("#addOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_change_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor, self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			self.initCommonEvent(); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				if(!self.state.customerNo) {
					bootbox.alert('请选择客户');
					return;
				}

				if(!self.state.changeOrderConsignInfo) {
					bootbox.alert('请选择收货地址');
					return;	
				}

				var commitData = {
					customerNo:self.state.customerNo,
					changeReasonType:formData['changeReasonType'],
					changeReason:formData['changeReason'],
					remark:formData['remark'],
					changeMode:formData['changeMode'],
					rentStartTime:new Date(formData['rentStartTime']).getTime(),
					owner:formData['owner'],
				}

				// var address = $('[name=customerConsignInfoId]').filter(':checked').data('address');
				commitData.changeOrderConsignInfo = {
					consigneeName:self.state.changeOrderConsignInfo.consigneeName,
					consigneePhone:self.state.changeOrderConsignInfo.consigneePhone,
					province:self.state.changeOrderConsignInfo.province,
					city:self.state.changeOrderConsignInfo.city,
					district:self.state.changeOrderConsignInfo.district,
					address:self.state.changeOrderConsignInfo.address,
				}

				var changeOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						if(sku.hasOwnProperty('destChangeProduct')) {
							changeOrderProductList.push({
								srcChangeProductSkuId:sku.skuId,
								destChangeProductSkuId:sku.destChangeProduct.skuId,
								isNew:sku.destChangeProduct.isNew,
								changeProductSkuCount:sku.changeProductSkuCount,
							})
						}
					})
				});

				var changeOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					if(material.hasOwnProperty('destChangeMaterial')) {
						changeOrderMaterialList.push({
							srcChangeMaterialNo:material.materialNo,
							destChangeMaterialNo:material.destChangeMaterial.materialNo,
							isNew:material.destChangeMaterial.isNew,
							changeMaterialCount:material.changeMaterialCount,
						})	
					}
				});

				if(changeOrderProductList.length == 0 && changeOrderMaterialList.length == 0) {
					bootbox.alert('请选择要换的商品或配件');
					return;
				}

				commitData.changeOrderProductList = changeOrderProductList;
				commitData.changeOrderMaterialList = changeOrderMaterialList;

				var des = "创建换货单";
				Rental.ajax.submit("{0}changeOrder/add".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建换货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建换货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
		}
	};


	window.AddChangeOrder = _.extend(AddChangeOrder, ChangeOrderMixIn);

})(jQuery);