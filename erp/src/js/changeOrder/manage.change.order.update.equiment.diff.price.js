;(function($) {

	var ChangeOrderUpdateEquimentDiffPrice = {
		init:function(prams) {
			this.props = _.extend({
				changeOrderNo:null,
				changeOrderProductId:null,
				callBackFunc:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "change-order/update-equipment-diff-price", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$editForm = $('.editForm', this.$modal);
			this.$cancelButton  = $('.cancelButton', this.$modal);
			this.$dataListTpl = $(".dataListTpl", this.$modal).html();
			this.$dataListTable = $(".dataListTable", this.$modal);

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			self.$cancelButton.click(function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.searchData();

			Rental.form.initFormValidation(this.$editForm, function(form){
				self.save();
			});

		},
		searchData:function(prams) {
			var self = this;

			if(!self.props.changeOrderProductId) {
				bootbox.alert('找不到换货单商品项ID');
				return;
			}

			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				changeOrderProductId:self.props.changeOrderProductId
			}, prams || {});
			
			Rental.ajax.submit("{0}changeOrder/pageChangeOrderProductEquipment".format(SitePath.service), commitData, function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载设备列表",response.description);
				}
			}, null ,"加载设备列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', self.$modal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					equipmentStatusStr:function() {
						return Enum.equipmentStatus.getValue(this.equipmentStatus);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		getDiffPriceList:function() {
			return $('.equipmentTr').map(function(index,item) {
				return {
					changeOrderProductEquipmentId:$('input.changeOrderProductEquipmentId', $(this)).val(),
					priceDiff:$.trim($('input.priceDiff', $(this)).val()),
				}
			}).toArray();
		},
		save:function() {
			var self = this;
			try {

				if(!self.props.changeOrderNo) {
					bootbox.alert('找不到换货单编号');
					return false;
				}

				var commitData = {
					changeOrderNo:self.props.changeOrderNo,
					changeOrderProductEquipmentList:self.getDiffPriceList(),
				}

				var des = "编辑设备差价";
				Rental.ajax.submit("{0}changeOrder/updateEquipmentPriceDiff".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null , des, 'dialogLoading');

			} catch(e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function() {
			this.doSearch(this.Pager.pagerData.currentPage);
			this.props.callBack && this.props.callBack();
		}
	};

	window.ChangeOrderUpdateEquimentDiffPrice = ChangeOrderUpdateEquimentDiffPrice;

})(jQuery)