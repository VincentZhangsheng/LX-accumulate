;(function($){
	
	var EditChangeOrder = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_change_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_change_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_change_order_edit];
		},
		initDom:function() {
			this.$form = $("#editOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_change_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor, self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到换货单编号');
				return;
			}
			Rental.ajax.ajaxData('changeOrder/detail', { changeOrderNo:self.state.no }, '加载换货单详细信息', function(response) {
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.initState(data);
			this.initFormData(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		initState:function(data) {
			this.state.customerNo = data.customerNo;
			this.state.changeOrderConsignInfo = data.changeOrderConsignInfo;
			this.renderOrderConsignInfo();
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
	  		Rental.ui.renderFormData(self.$form, data);
	  		$('[name=rentStartTime]', self.$form).val(new Date(data.rentStartTime).format('yyyy-MM-dd'));
		},
		edit:function() {
			var self = this;
			try {
				if(!self.state.no) {
					bootbox.alert('找不到换货单号');
					return;
				}

				var formData = Rental.form.getFormData(self.$form);

				if(!self.state.customerNo) {
					bootbox.alert('请选择客户');
					return;
				}

				if(!self.state.changeOrderConsignInfo) {
					bootbox.alert('请选择收货地址');
					return;	
				}

				var commitData = {
					changeOrderNo:self.state.no,
					customerNo:self.state.customerNo,
					changeReasonType:formData['changeReasonType'],
					changeReason:formData['changeReason'],
					remark:formData['remark'],
					changeMode:formData['changeMode'],
					rentStartTime:new Date(formData['rentStartTime']).getTime(),
					owner:formData['owner'],
				}

				commitData.changeOrderConsignInfo = {
					consigneeName:self.state.changeOrderConsignInfo.consigneeName,
					consigneePhone:self.state.changeOrderConsignInfo.consigneePhone,
					province:self.state.changeOrderConsignInfo.province,
					city:self.state.changeOrderConsignInfo.city,
					district:self.state.changeOrderConsignInfo.district,
					address:self.state.changeOrderConsignInfo.address,
				}

				var changeOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						console.log(sku)
						if(sku.hasOwnProperty('destChangeProduct')) {
							var changeOrderProductItem = {
								srcChangeProductSkuId:sku.skuId,
								destChangeProductSkuId:sku.destChangeProduct.skuId,
								isNew:sku.destChangeProduct.hasOwnProperty('isNew') ? sku.destChangeProduct.isNew : sku.isNew,
								changeProductSkuCount:sku.changeProductSkuCount,
							}
							if(sku.hasOwnProperty('changeOrderProductId')) changeOrderProductItem.changeOrderProductId = sku.changeOrderProductId;
							changeOrderProductList.push(changeOrderProductItem);
						}
					})
				});

				var changeOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					if(material.hasOwnProperty('destChangeMaterial')) {
						var changeOrderMaterial = {
							srcChangeMaterialNo:material.materialNo,
							destChangeMaterialNo:material.destChangeMaterial.materialNo,
							isNew:material.destChangeMaterial.hasOwnProperty('isNew') ? material.destChangeMaterial.isNew : material.isNew,
							changeMaterialCount:material.changeMaterialCount,
						};
						if(material.hasOwnProperty('changeOrderMaterialId')) changeOrderMaterial.changeOrderMaterialId = material.changeOrderMaterialId;	
						changeOrderMaterialList.push(changeOrderMaterial);
					}
				});

				if(changeOrderProductList.length == 0 && changeOrderMaterialList.length == 0) {
					bootbox.alert('请选择要换的商品或配件');
					return;
				}

				commitData.changeOrderProductList = changeOrderProductList;
				commitData.changeOrderMaterialList = changeOrderMaterialList;

				var des = "创建换货单";
				Rental.ajax.submit("{0}changeOrder/update".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建换货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑换货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } 
				    }
				});
			}
		}
	};


	window.EditChangeOrder = _.extend(EditChangeOrder, ChangeOrderMixIn);

})(jQuery);