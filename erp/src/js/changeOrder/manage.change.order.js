;(function($) {

	var ChangeOrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_change_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_change_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_change_order_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_change_order_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_change_order_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_change_order_submit],
				stock = this.currentPageAuthor.children[AuthorCode.manage_change_order_stock],
				send = this.currentPageAuthor.children[AuthorCode.manage_change_order_send],
				complete = this.currentPageAuthor.children[AuthorCode.manage_change_order_complete],
				changedEquimentInStorage = this.currentPageAuthor.children[AuthorCode.manage_change_order_changed_equiment_in_storage],
				unChangeEquimentInStorage = this.currentPageAuthor.children[AuthorCode.manage_change_order_un_change_equiment_in_storage],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_change_order_cancel],
				addRemark = this.currentPageAuthor.children[AuthorCode.manage_change_order_add_remark],
				updateDiffPrice = this.currentPageAuthor.children[AuthorCode.manage_change_order_update_diff_price],
				completeInStorage = this.currentPageAuthor.children[AuthorCode.manage_change_order_complete_instorage];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			stock && this.rowActionButtons.push(AuthorUtil.button(view,'stockButton','','备货'));
			send && this.rowActionButtons.push(AuthorUtil.button(send,'sendButton','','发货'));
			complete && this.rowActionButtons.push(AuthorUtil.button(complete,'completeButton','','完成'));
			changedEquimentInStorage && this.rowActionButtons.push(AuthorUtil.button(view,'changedEquimentInStorageButtonInList','','已更换设备入库'));
			unChangeEquimentInStorage && this.rowActionButtons.push(AuthorUtil.button(view,'unChangeEquimentInStorageButtonInList','','未换设备入库'));

			addRemark && this.rowActionButtons.push(AuthorUtil.button(view,'addRemarkButton','','添加设备入库备注'));
			updateDiffPrice && this.rowActionButtons.push(AuthorUtil.button(view,'updateDiffPriceButton','','修改设备差价'));
			completeInStorage && this.rowActionButtons.push(AuthorUtil.button(view,'completeInStorageButton','','完成验货'));

			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.changeOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function() {
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));
			
			self.searchData();  //初始化列表数据

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#changeOrderStatus'),
				data:Enum.array(Enum.changeOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

			// Rental.ui.events.initMonthpicker($("#monthpicker2"));
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.changeOrder, searchData);

			Rental.ajax.ajaxData('changeOrder/page', searchData, '加载换货单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource: _.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					isDamageValue:function() {
						return this.isDamage == 1 ? "是":"否";
					},
					changeReasonTypeValue:function(){
						return Enum.changeReasonType.getValue(this.changeReasonType);
					},
					changeModeValue:function() {
						return Enum.changeMode.getValue(this.changeMode);	
					},
					changeOrderStatusValue:function() {
						return Enum.changeOrderStatus.getValue(this.changeOrderStatus);	
					},
					changeOrderStatusClass:function() {
						return Enum.changeOrderStatus.getClass(this.changeOrderStatus);	
					}
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.ChangeOrderManage = _.extend(ChangeOrderManage, ChangeOrderHandleMixin);

})(jQuery);