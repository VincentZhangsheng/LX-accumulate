;(function($) {

	var ChangeOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			self.$materialDataListTable && self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.changeOrder
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				self.cancel($(this).data('no'), handleCallBack);
			});

			container.on('click', '.sendButton', function(event) {
				event.preventDefault();
				self.delivery($(this).data('no'), handleCallBack);
			});

			container.on('click', '.completeButton', function(event) {
				event.preventDefault();
				self.complete($(this).data('no'), handleCallBack);
			});

			container.on('click', '.stockProductButton', function(event) {
				event.preventDefault();
				self.stockProduct($(this).data('no'), handleCallBack);
			});

			container.on('click', '.clearStockProductButton', function(event) {
				event.preventDefault();
				self.clearStockProduct($(this).data('no'), handleCallBack);
			});

			container.on('click', '.changedEquimentInStorageButton', function(event) {
				event.preventDefault();
				self.changedEquimentInStorage($(this).data('no'), handleCallBack);
			});

			container.on('click', '.unChangeEquimentInStorageButton', function(event) {
				event.preventDefault();
				self.unChangeEquimentInStorage($(this).data('no'), handleCallBack);
			});

			container.on('click', '.stockMaterialButton', function(event) {
				event.preventDefault();
				self.stockMaterial($(this).data('no'), $(this).data('changeordermaterialid'), handleCallBack);
			});

			container.on('click', '.clearStockMaterialButton', function(event) {
				event.preventDefault();
				self.clearStockMaterial($(this).data('no'), $(this).data('changeordermaterialid'), handleCallBack);
			});

			container.on('click', '.viewStockEquipment', function(event) {
				event.preventDefault();
				var changeorderproductid = $(this).data('changeorderproductid');
				ChangeOrderEquimentList.init({
					changeOrderProductId:changeorderproductid,
					callBack:function() {
						self.loadData();
					}
				});
			});

			container.on('click', '.viewStockMaterial', function(event) {
				event.preventDefault();
				var changeordermaterialid = $(this).data('changeordermaterialid');
				ChangeOrderMaterialList.init({
					changeOrderMaterialId:changeordermaterialid,
					callBack:function() {
						self.loadData();
					}
				});
			});

			container.on('click', '.changedMaterialInStorageButton', function(event) {
				event.preventDefault();
				self.changedMaterialInStorage({
					no:$(this).data('no'),
					srcChangeMaterialNo:$(this).data('srcchangematerialno'),
					count:$(this).data('changematerialcount'),
					changeOrderMaterialId:$(this).data('changeordermaterialid')
				}, handleCallBack)
			});

			container.on('click', '.equimentAddRemarkButton', function(event) {
				event.preventDefault();
				self.updateEquipmentRemark({ no:$(this).data('no') }, handleCallBack);
			});

			container.on('click', '.materialAddRemarkButton', function(event) {
				event.preventDefault();
				self.updateMaterialRemark({ changeOrderMaterialId:$(this).data('changeordermaterialid') }, handleCallBack);
			});

			container.on('click', '.completeInStorageButton', function(event) {
				event.preventDefault();
				self.completeInStorage($(this).data('no'), handleCallBack);
			});

			container.on('click', '.updateEquipmentDiffPriceButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no'), changeOrderProductId = $(this).data('changeorderproductid');
				ChangeOrderUpdateEquimentDiffPrice.init({
					changeOrderNo:no,
					changeOrderProductId:changeOrderProductId,
					callBackFunc:handleCallBack || function() {}
				})
			});

			container.on('click', '.updateMaterialDiffPriceButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no'), changeordermaterialid = $(this).data('changeordermaterialid');
				ChangeOrderUpdateMaterialDiffPrice.init({
					changeOrderNo:no,
					changeOrderMaterialId:changeordermaterialid,
					callBackFunc:handleCallBack || function() {}
				})
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交换货单', submitUrl = 'changeOrder/commit';
			if(isAudit) {
				self.orderDo(submitUrl, {
					changeOrderNo:prams.no,
					verifyUserId:prams.verifyUserId,
					commitRemark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
				return;
			} else {
				bootbox.confirm('确认'+des+'？', function(result) {
					result && self.orderDo(submitUrl, {
						changeOrderNo:prams.no
					}, des, callBack);
				});
			}
		},
		stockProduct:function(no, callBack) {
			this.stockUpForChangeSubmit({
				no:no,
				des:'换货设备备货',
				operationType:Enum.operationType.num.stock, 
				callBack:callBack,
			})
		},
		clearStockProduct:function(no, callBack) {
			this.stockUpForChangeSubmit({
				no:no,
				des:'换货设备清货',
				operationType:Enum.operationType.num.remove, 
				callBack:callBack,
			})
		},
		stockUpForChangeSubmit:function(prams) {
			var self = this;
			OrderPicking.init({
				title:prams.des,
				callBack:function(res) {
					self.orderDo(prams.url || 'changeOrder/stockUpForChange', {
						changeOrderNo:prams.no,
						equipmentNo:res.equipmentNo,
						operationType:prams.operationType, 
					}, prams.des, prams.callBack);
					return true;
				}
			});
		},
		stockMaterial:function(no, changeOrderMaterialId, callBack) {
			var self = this;
			bootbox.confirm('确认备货此配件? ', function(result) {
				if(result) {
					self.orderDo('changeOrder/stockUpForChange', {
						changeOrderNo:no,
						changeOrderMaterialId:changeOrderMaterialId,
						operationType:Enum.operationType.num.stock, 
					}, '配件备货', callBack);
					return true;
				}
			})
		},
		clearStockMaterial:function(no, changeOrderMaterialId, callBack) {
			var self = this;
			bootbox.confirm('确认清货此配件? ', function(result) {
				if(result) {
					self.orderDo('changeOrder/stockUpForChange', {
						changeOrderNo:no,
						changeOrderMaterialId:changeOrderMaterialId,
						operationType:Enum.operationType.num.remove, 
					}, '配件清货', callBack);
					return true;
				}
			})
		},
		changedEquimentInStorage:function(no, callBack) {
			var self = this, des = '已更换设备入库';
			OrderPicking.init({
				title:des,
				callBack:function(res) {
					self.orderDo('changeOrder/doChangeEquipment', {
						changeOrderNo:no,
						srcEquipmentNo:res.equipmentNo,
					}, des, callBack);
					return true;
				}
			});
		},
		unChangeEquimentInStorage:function(no, callBack) {
			var self = this, des = '未换设备入库';
			OrderPicking.init({
				title:des,
				callBack:function(res) {
					self.orderDo('changeOrder/processNoChangeEquipment', {
						changeOrderNo:no,
						equipmentNo:res.equipmentNo,
					}, des, callBack);
					return true;
				}
			});
		},
		changedMaterialInStorage:function(prams, callBack) {
			console.log(prams);
			var self = this;
			InputModal.init({
				title:'更换物料入库',
				url:'change-order/material-in-storage',
				initFunc:function(modal) {
					$('[name=count]', modal).val(prams.count);
				},
				callBack:function(result) {
					self.orderDo('changeOrder/doChangeMaterial', {
						changeOrderNo:prams.no,
						srcChangeMaterialNo:prams.srcChangeMaterialNo,
						changeOrderMaterialId:prams.changeOrderMaterialId,
						realChangeMaterialCount:result.count,
					}, '更换物料入库', callBack);
					return true;
				}
			})
		},
		updateEquipmentRemark:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'添加设备备注',
				url:'change-order/equipment-remark-modal',
				initFunc:function(modal) {
				},
				callBack:function(result, $modal) {
					self.orderDo('changeOrder/updateChangeEquipmentRemark', {
						changeOrderNo:prams.no,
						equipmentNo:result.equipmentNo,
						remark:result.remark,
					}, '添加设备备注', callBack);
					// return true;
					$('[name=equipmentNo]', $modal).val('').focus();
				}
			});
		},
		updateMaterialRemark:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'添加配件备注',
				url:'common-modal/remark',
				initFunc:function(modal) {},
				callBack:function(result, $modal) {
					self.orderDo('changeOrder/updateChangeMaterialRemark', {
						changeOrderMaterialId:prams.changeOrderMaterialId,
						remark:result.remark,
					}, '添加配件备注', callBack);
					return true;
				}
			});
		},
		orderDo:function(url, prams, des, callBack) {
			// if(!prams.changeOrderNo) {
			// 	bootbox.alert('找不到换货单编号');
			// 	return;
			// }
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		delivery:function(no, callBack) {
			var self = this;
			bootbox.confirm('确认发货？', function(result) {
				result && self.orderDo('changeOrder/delivery', {changeOrderNo:no}, '换货单发货', callBack);
			});
		},
		completeInStorage:function(no, callBack) {
			var self = this;
			bootbox.confirm('确认完成入库验货？', function(result) {
				result && self.orderDo('changeOrder/confirmChangeOrder', {changeOrderNo:no}, '完成入库验货', callBack);
			});
		},
		cancel:function(no, callBack) {
			var self = this;
			bootbox.confirm('确认取消换货单？', function(result) {
				result && self.orderDo('changeOrder/cancel', {changeOrderNo:no}, '取消换货单', callBack);
			});
		},
		complete:function(no, callBack) {	
			var self = this;
			InputModal.init({
				title:'结束换货单',
				url:'change-order/complete-input',
				initFunc:function(modal) { 
					self.initComplete(no, modal);
				},
				callBack:function(result) {
					self.orderDo('changeOrder/end', _.extend({changeOrderNo:no}, result), '结束换货单', callBack);
					return true;
				}
			})
		},
		initComplete:function(no, modal) {
			
			Rental.ui.renderSelect({
				data:Enum.array(Enum.changeMode),
				container:$('[name=changeMode]', modal),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			Rental.ui.events.initDatePicker($('#rentStartTimePicker'), $("[name=rentStartTime]", self.$form));

			this.getChangeOrderByNo(no, function(order) {
				Rental.ui.renderFormData($('form', modal), order);
				$('[name=rentStartTime]', self.$form).val(new Date(order.rentStartTime).format('yyyy-MM-dd'));
			});
		},
		getChangeOrderByNo:function(no, callback) {
			Rental.ajax.ajaxData('changeOrder/detail', { changeOrderNo: no}, '加载换货单详细信息', function(response) {
				callback && callback(response.resultMap.data);
			});
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				button.no = order.changeOrderNo; 
				rowActionButtons.push(button);
				var unSubmit = order.changeOrderStatus == Enum.changeOrderStatus.num.unSubmit,
					waitForStock = order.changeOrderStatus == Enum.changeOrderStatus.num.waitForStock,
					inStock = order.changeOrderStatus == Enum.changeOrderStatus.num.inStock,
					inHand = order.changeOrderStatus == Enum.changeOrderStatus.num.inHand;

				if((button.class == 'submitButton' || button.class == 'editButton' || button.class == 'cancelButton') && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf(['stockButton', 'stockProductButton', 'clearStockProductButton', 'stockMaterialButton', 'clearStockMaterialButton'], button.class) > -1 && !(waitForStock || inStock)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf(['sendButton'], button.class) > -1 && !inStock) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf([
					'changedEquimentInStorageButtonInList', 
					'unChangeEquimentInStorageButtonInList', 
					'changedEquimentInStorageButton', 
					'unChangeEquimentInStorageButton', 
					'changedMaterialInStorageButton',
					'equimentAddRemarkButton',
					'materialAddRemarkButton',
					], button.class) > -1 && (order.changeOrderStatus != Enum.changeOrderStatus.num.delivered && !inHand)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf([
					'updateEquipmentDiffPriceButton', 
					'updateMaterialDiffPriceButton', 
					], button.class) > -1 && order.changeOrderStatus != Enum.changeOrderStatus.num.completeInStorage) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf(['addRemarkButton','updateDiffPriceButton','completeInStorageButton'], button.class) > -1 && !inHand) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'completeButton') && order.changeOrderStatus != Enum.changeOrderStatus.num.completeInStorage) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		}
	}

	window.ChangeOrderHandleMixin = ChangeOrderHandleMixin;

})(jQuery);