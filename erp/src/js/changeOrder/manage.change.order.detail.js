;(function($){
	
	var ChangeOrderDetail = {
		// state:{
		// 	no:null,
		// 	customer:null,
		// 	company:null,
		// 	customerNo:null,
		// 	chooseProductList:new Array(),
		// 	chooseMaterialList:new Array(),
		// 	returnOrderConsignInfo:null,
		// },
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_change_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_change_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_change_order_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.productAcitonButtons = new Array();
			this.productRowAcitonButtons = new Array();
			this.materialRowAcitonButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_change_order_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_change_order_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_change_order_detail],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_change_order_submit],
				stock = this.currentManageListAuthor.children[AuthorCode.manage_change_order_stock],
				send = this.currentManageListAuthor.children[AuthorCode.manage_change_order_send],
				complete = this.currentManageListAuthor.children[AuthorCode.manage_change_order_complete],
				changedEquimentInStorage = this.currentManageListAuthor.children[AuthorCode.manage_change_order_changed_equiment_in_storage],
				unChangeEquimentInStorage = this.currentManageListAuthor.children[AuthorCode.manage_change_order_un_change_equiment_in_storage],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_change_order_cancel],
				addRemark = this.currentManageListAuthor.children[AuthorCode.manage_change_order_add_remark],
				updateDiffPrice = this.currentManageListAuthor.children[AuthorCode.manage_change_order_update_diff_price],
				completeInStorage = this.currentManageListAuthor.children[AuthorCode.manage_change_order_complete_instorage];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			// stock && this.actionButtons.push(AuthorUtil.button(stock,'stockButton','','备货'));
			send && this.actionButtons.push(AuthorUtil.button(send,'sendButton','','发货'));
			completeInStorage && this.actionButtons.push(AuthorUtil.button(completeInStorage,'completeInStorageButton','','完成验货'));
			complete && this.actionButtons.push(AuthorUtil.button(complete,'completeButton','','完成'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));

			stock && this.productAcitonButtons.push(AuthorUtil.button(stock,'stockProductButton','','商品备货'));
			stock && this.productAcitonButtons.push(AuthorUtil.button(stock,'clearStockProductButton','','商品清货'));
			changedEquimentInStorage && this.productAcitonButtons.push(AuthorUtil.button(changedEquimentInStorage,'changedEquimentInStorageButton','','已更换设备入库'));
			unChangeEquimentInStorage && this.productAcitonButtons.push(AuthorUtil.button(unChangeEquimentInStorage,'unChangeEquimentInStorageButton','','未换设备入库'));
			addRemark && this.productAcitonButtons.push(AuthorUtil.button(addRemark,'equimentAddRemarkButton','','添加设备入库备注'));

			updateDiffPrice && this.productRowAcitonButtons.push(AuthorUtil.button(updateDiffPrice,'updateEquipmentDiffPriceButton','','修改设备差价'));

			stock && this.materialRowAcitonButtons.push(AuthorUtil.button(stock,'stockMaterialButton','','备货'));
			stock && this.materialRowAcitonButtons.push(AuthorUtil.button(stock,'clearStockMaterialButton','','清货'));
			changedEquimentInStorage && this.materialRowAcitonButtons.push(AuthorUtil.button(changedEquimentInStorage,'changedMaterialInStorageButton','','更换物料入库'));
			addRemark && this.materialRowAcitonButtons.push(AuthorUtil.button(addRemark,'materialAddRemarkButton','','添加配件备注'));
			updateDiffPrice && this.materialRowAcitonButtons.push(AuthorUtil.button(updateDiffPrice,'updateMaterialDiffPriceButton','','修改配件差价'));
		},
		initDom:function() {
			this.$form = $("#detailOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_change_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			}); 

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到换货单编号');
				return;
			}
			Rental.ajax.ajaxData('changeOrder/detail', { changeOrderNo:self.state.no }, '加载换货单详细信息', function(response) {
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderProductActionButtons(data);
			this.renderOrderBaseInfo(data);
			this.initChooseProductListHasProductRowActionButtons(data);
			this.initChooseMaterialListHasMaterialRowActionButtons(data);
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, order),
				no:self.state.no,
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderProductActionButtons:function(order) {
			var self = this,
				actionButtonsTpl = $('#productActionButtonsTpl').html();
				actionButtons = self.filterAcitonButtons(self.productAcitonButtons, order)
			var data = {
				hasActionButtons:actionButtons.length > 0,
				acitonButtons:actionButtons,
			}
			Mustache.parse(actionButtonsTpl);
			$("#productActionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#changeOrderNo").html(order.changeOrderNo);
			var data = _.extend(Rental.render, {
				data:order,
				isDamageValue:function() {
					return this.isDamage == 1 ? "是":"否";
				},
				changeReasonTypeValue:function(){
					return Enum.changeReasonType.getValue(this.changeReasonType);
				},
				changeModeValue:function() {
					return Enum.changeMode.getValue(this.changeMode);	
				},
				changeOrderStatusValue:function() {
					return Enum.changeOrderStatus.getValue(this.changeOrderStatus);	
				},
				changeOrderStatusClass:function() {
					return Enum.changeOrderStatus.getClass(this.changeOrderStatus);	
				}
			})
			var tpl = $("#orderBaseInfoTpl").html();
			Mustache.parse(tpl);
			$('#orderBaseInfo').html(Mustache.render(tpl, data));
		},
		initChooseProductListHasProductRowActionButtons:function(order) {
			this.state.chooseProductList = this.resolveChooseProductList(order.changeOrderProductList);
			this.renderProductList(this.filterAcitonButtons(this.productRowAcitonButtons, order));
		}, 
		initChooseMaterialListHasMaterialRowActionButtons:function(order) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(order.changeOrderMaterialList);
			this.renderMaterialList(this.filterAcitonButtons(this.materialRowAcitonButtons, order));
		},
	};

	window.ChangeOrderDetail = _.extend(ChangeOrderDetail, ChangeOrderMixIn, ChangeOrderHandleMixin);

})(jQuery);