/**
 * author:liaojianming
 * date: 2018-3-26
 * decription: 导入资金账单流水列表
 */
;(function($) {
	var JurnalAmountManage = {
		init: function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor: function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_financeial]; //财务管理
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list]; //财务管理-资金流水附件
			this.initActionButtons();
		},
		initActionButtons: function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list_view],
				upload = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list_upload],
				confirm = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list_confirm],
				pushdown = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list_pushdowm],
				deleteItem = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list_delete],
				confirmAttribute = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_attachment_list_delete];

			upload && this.commonActionButtons.push(AuthorUtil.button(upload,'upLoadButton','fa fa-plus','上传'));
			view && this.rowActionButtons.push(AuthorUtil.button(_.extend(view, {menuUrl: 'jurnal-amount/list'}),'viewButton','','查看'));
			confirm && this.rowActionButtons.push(AuthorUtil.button(confirm,'confirmButton','','确认'));
			pushdown && this.rowActionButtons.push(AuthorUtil.button(pushdown,'pushdownButton','','下推'));
			deleteItem && this.rowActionButtons.push(AuthorUtil.button(deleteItem, 'deleteButton', '', '删除'));
		},
		initDom: function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.jurnalUploadList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});
			this.Pager = new Pager();
		},
		initEvent: function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor, this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			//初始化列表数据
			this.searchData();

			// 初始化时间控件
			Rental.ui.events.initRangeDatePicker($('#slipTimePicker'), $('#slipTimePickerInput'),  $("#slipDayStart"), $("#slipDayEnd"));

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			// 上传表格附件
			self.$actionCommonButtons.on("click", '.upLoadButton', function(event) {
				event.preventDefault();
				var user = Rental.localstorage.getUser();
				var subCompanyId = user.roleList[0].subCompanyId

				UploadExcelMsg.init({
					subCompanyId:subCompanyId,
					callBack:function(){
						self.searchData();
					}
				});
			});

			// 确认
			this.$dataListTable.on('click', '.confirmButton', function(event) {
				event.preventDefault();
				var bankSlipId = $(this).attr('data-bankSlipId');
				self.submitConfirmBankSlip(bankSlipId);
			})

			// 下推
			this.$dataListTable.on('click', '.pushdownButton' ,function(event) {
				event.preventDefault();
				var bankSlipId = $(this).attr('data-bankSlipId');
				self.submitpushDownBankSlip(bankSlipId);
			})

			// 删除
			this.$dataListTable.on('click', '.deleteButton', function(event) {
				event.preventDefault();
				var bankSlipId = $(this).attr('data-bankSlipId');
				self.deleteBankSlip(bankSlipId);
			})
			
			// 获取分公司下拉列表
			ApiData.company({
				success: function(response) {
					Rental.ui.renderSelect({
						container: $("#subCompanyName"),
						data: response,
						func: function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change: function(val) {
							self.searchData();
						},
						defaultText: '请选择分公司（全部）'
					});
				}
			});

			// 获取单据状态下拉列表 
			Rental.ui.renderSelect({
				container: $("#slipStatus"),
				data: Enum.array(Enum.slipStatus),
				func: function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change: function(val) {
					self.searchData({pageNo: 1});
				},
				defaultText: '请选择单据状态（全部）'
			});

			// 获取银行类别下拉列表
			Rental.ui.renderSelect({
				container: $("#bankType"),
				data: Enum.array(Enum.bankType),
				func: function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change: function(val) {
					self.searchData({pageNo: 1});
				},
				defaultText:'请选择银行类别（全部）'
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.render({});
		},
		// 确认资金流水
		submitConfirmBankSlip: function(bankSlipId) {
			var self = this;
			bootbox.confirm('确认资金流水？', function(result) {
				if(result) {
					self.do('bankSlip/confirmBankSlip', { bankSlipId: bankSlipId }, '确认资金流水', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage});
					});	
				}
			});
		},
		// 下推
		submitpushDownBankSlip: function(bankSlipId) {
			var self = this;
			bootbox.confirm('确认资金流水下推？', function(result) {
				if(result) {
					self.do('bankSlip/pushDownBankSlip', { bankSlipId: bankSlipId }, '下推', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage});
					});	
				}
			});
		},
		// 删除对公银行流水
		deleteBankSlip: function(bankSlipId) {
			var self = this,
				des = '确认删除对公银行流水?',
				data = {bankSlipId: bankSlipId};
			bootbox.confirm(des, function(result) {
				if(result) {
					self.do('bankSlip/deleteBankSlip', data, '删除对公银行流水', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage})
					})
				}
			})
		},
		// 确认归属
		confirmAttribution: function(bankSlipId) {
			var self = this,
				des = '确认归属?',
				data = {bankSlipId: bankSlipId};
			bootbox.confirm(des, function(result) {
				if(result) {
					self.do('bankSlip/deleteBankSlip', data, '确认归属', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage})
					})
				}
			})
		},
		do: function(url, prams, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des, response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des, response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		renderCommonActionButton: function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons': self.commonActionButtons}));
		},
		searchData: function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo: 1,
				pageSize: this.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.jurnalUploadList, searchData);
			Rental.ajax.ajaxData('bankSlip/pageBankSlip', searchData, '查询资金流水', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch: function(pageNo) {
			this.searchData({pageNo: pageNo || 1});
		},
		render: function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList: function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons: hasCommonActionButtons,
				hasRowActionButtons: hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData": listData,
					rowActionButtons: function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"bankTypeValue": function() {
						return Enum.bankType.getValue(this.bankType);
					},
					"slipStatusValue": function() {
						return Enum.slipStatus.getValue(this.slipStatus);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		filterAcitonButtons: function(buttons, jurnalData) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
				if((button.class == 'confirmButton') && (jurnalData.slipStatus == Enum.slipStatus.num.unConfirm || !(_.isNumber(jurnalData.claimCount) && jurnalData.claimCount > 0))) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
				}
				if((button.class == 'pushdownButton') && !(jurnalData.slipStatus == Enum.slipStatus.num.unConfirm)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
				}
				if((button.class == 'deleteButton') && !(jurnalData.slipStatus == Enum.slipStatus.num.unConfirm)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
				}
			});
			return rowActionButtons;
		}
	};

	window.JurnalAmountManage = JurnalAmountManage;

})(jQuery);