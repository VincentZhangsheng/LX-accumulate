/*
* decription: 未知流水详情
* author: zhangsheng
* time: 2018-08-22
*/
;(function($) {
    var JurnalHighseasDetail = {
        state:{
			customer:null,
			chooseCustomerList:new Array(),
			bankSlipDetailId:null,
			tradeAmount:null,
            detailStatus:null,
            jurnal:null
		},
        init:function() {
            this.initAuthor();
            this.initDom();
            this.initEvent();
        },
        initAuthor:function() {
            try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_financeial];
                this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_list];
                this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_detail];
                this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
        },
        initActionButtons:function() {
            this.actionButtons = new Array();
            this.rowActionButtons = new Array();

            if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

            var claim = this.currentManageListAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_claim],
                goPay = this.currentManageListAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_gopay];
                
			claim && this.actionButtons.push(AuthorUtil.button(claim,'claimButton','','认领'));

            goPay && this.rowActionButtons.push(AuthorUtil.button(_.extend(goPay, {menuUrl:"statement-order/list"}),'toPay','','前往支付'));
        },
        initDom:function() {
            this.$form = $("#statementOrderDetailForm");
			this.$dataListTpl = $("#dataListTpl").html();
            this.$dataListTable = $("#dataListTable");
            
            this.$operationTpl = $("#operationRecordTpl").html();
            this.$operationTable = $("#operationRecordTable");

            this.state.bankSlipDetailId = Rental.helper.getUrlPara('bankSlipDetailId');
            this.Pager = new Pager();
        },
        initEvent:function() {
            var self = this;

            Layout.chooseSidebarMenu(AuthorCode.manage_financeial_jurnal_highseas_list); //激活选中menu菜单
            Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑
            
            self.loadData();

            self.initHandleEvent(self.$form,function() {
				self.loadData();
            })
            
            //认领
            self.$form.on('click', '.claimButton', function(event) {
				event.preventDefault();
				self.state.chooseCustomerList = new Array();
				var detailStatus = $(this).data("status");
				self.state.bankSlipDetailId = $(this).data('bankslipdetailid');
                self.state.tradeAmount = $(this).data('tradeamount');
				self.searchClaimData(self.state.bankSlipDetailId);
            })
            
            self.searchOperationData({})
        },
        loadData:function() {
            var self = this;
            if(!self.state.bankSlipDetailId) {
                bootbox.alert('没找到资金流水ID');
                return;
            }
            Rental.ajax.ajaxData('bankSlip/queryUnknownBankSlipDetail', {bankSlipDetailId:self.state.bankSlipDetailId}, '加载资金流水详细', function(response) {
                self.state.jurnal = response.resultMap.data;
                self.initData(response.resultMap.data);
            })
        },
        initData:function(data) {
            this.renderBaseInfo(data);
            this.renderDetailList(data);
            this.renderActionButton(data);
        },
        renderActionButton:function(jurnal) {
            var self = this;
            var actionButtonsTpl = $('#actionButtonsTpl').html();
            var data = {
                "actionButtons":self.filterAcitonButtons(self.actionButtons, jurnal),
                bankslipdetailid:function() {
                    return jurnal.bankSlipDetailId;
                },
                detailStatus:function() {
                    return jurnal.detailStatus;
                },
                tradeAmount:function() {
                    return jurnal.tradeAmount;
                },
                isLocalization:function() {
                    return jurnal.isLocalization;
                }
            }
            Mustache.parse(actionButtonsTpl);
            $("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
        },
		//选择客户弹窗
		claimChooseCustomerByAll:function() {
			var self = this;
			ChooseCustomerByAll.init({
				batch:true,
				callBack:function(customer) {
					self.chooseCustomer(self.state.chooseCustomerList,customer);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {
					if(self.state.chooseCustomerList.length > 0) {
						tradeDetail.init({
							chooseCustomerList:self.state.chooseCustomerList,
							bankSlipDetailId:self.state.bankSlipDetailId,
							tradeAmount:self.state.tradeAmount,
                            continueChooseCustomeFunc:null,
                            changeContinueFunc:function() {
								_.isObject(JurnalHighseasDetail) &&  _.isFunction(JurnalHighseasDetail.claimChooseCustomerByAll) && JurnalHighseasDetail.claimChooseCustomerByAll();
							},
							callBack:function() {
								self.loadData();
							}
						});
					}
				}
			});
		},
		//选择客户
		chooseCustomer:function(chooseCustomerList, customer) {
			if(!chooseCustomerList) {
				chooseCustomerList = new Array();
			}	
			if(chooseCustomerList.length > 0) {
				var _Index = _.findIndex(chooseCustomerList, {customerNo:customer.customerNo});
				if(_Index > -1) {
					return chooseCustomerList;
				} else {
					chooseCustomerList.push({
						customerName:customer.customerName,
						customerNo:customer.customerNo
					})
				}
			} else {
				chooseCustomerList.push({
					customerName:customer.customerName,
					customerNo:customer.customerNo
				})
			}
			return chooseCustomerList;
		},
        searchClaimData:function(bankSlipDetailId) {
			var self = this;
			Rental.ajax.ajaxData('bankSlip/queryUnknownBankSlipDetail', {bankSlipDetailId:bankSlipDetailId}, '加载资金流水详细', function(response) {
				var data = response.resultMap.data
                if(data.hasOwnProperty("bankSlipClaimList") && data.bankSlipClaimList.length > 0) {
					self.state.chooseCustomerList = data.bankSlipClaimList;
					self.renderClaimList();
				} else {
					self.claimChooseCustomerByAll();
				}
			})
		},
		renderClaimList:function() {
			var self = this;
			tradeDetail.init({
				chooseCustomerList:self.state.chooseCustomerList,
				bankSlipDetailId:self.state.bankSlipDetailId,
                tradeAmount:self.state.tradeAmount,
                continueChooseCustomeFunc:null,
                changeContinueFunc:function() {
                    _.isObject(JurnalHighseasDetail) &&  _.isFunction(JurnalHighseasDetail.claimChooseCustomerByAll) && JurnalHighseasDetail.claimChooseCustomerByAll();
                },
				callBack:function() {
					self.loadData();
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {},
			});
		},
        renderBaseInfo:function(jurnal) {
            this.renderHeaderInfo(jurnal, $('#headerInfoTpl').html(), $('#headerInfo'))
            this.renderOrderInfo(jurnal, $('#jurnalBaseInfoTpl').html(), $('#jurnalBaseInfo'));
        },
        renderHeaderInfo:function(jurnal, tpl, container) {
            var data = _.extend(Rental.render, {
                "payerName":jurnal.payerName
            });
            Mustache.parse(tpl);
            container.html(Mustache.render(tpl, data));
        },
        renderOrderInfo:function(jurnal, tpl, container) {
            var self = this;
            var data = _.extend(Rental.render, {
                jurnal:jurnal,
                loanSignValue:function() {
                    return Enum.loanSign.getValue(this.loanSign);
                },
                detailStatusValue:function() {
                    return Enum.detailStatus.getValue(this.detailStatus);
                },
                dataStatusValue:function() {
                    return self.getDataStatusValue(this.dataStatus);
                },
                isLocalizationValue:function() {
                    return Enum.isLocalization.getValue(this.isLocalization);
                },
                bankSlipBankTypeValue:function() {
                    return Enum.bankType.getValue(this.bankSlipBankType);
                }
            });
            Mustache.parse(tpl);
            container.html(Mustache.render(tpl, data));
        },
        renderDetailList:function(jurnal) {
            var self = this;
            var hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;
            var listData = jurnal.hasOwnProperty("bankSlipClaimList") ? jurnal.bankSlipClaimList : [];

            var data = {
                hasRowActionButtons:hasRowActionButtons,
                dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    "rowActionButtons":function() {
						var _this = this;
						return self.filterRowButtons(self.rowActionButtons, jurnal)
					},
                    dataStatusValue:function() {
                        return self.getDataStatusValue(this.dataStatus);
                    },
                    rechargeStatusValue:function() {
                        return self.getRechargeStatusValue(this.rechargeStatus);
                    }
                })
            }
            Mustache.parse(this.$dataListTpl);
            this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        },
        searchOperationData:function(prams) {
            var self = this;
			var searchData = $.extend({
				pageNo:1,
                pageSize:this.Pager.defautPrams.pageSize,
                bankSlipDetailId:this.state.bankSlipDetailId
			}, prams || {});

			Rental.ajax.ajaxDataNoLoading('bankSlip/pageBankSlipDetailOperationLog', searchData, '查询操作记录', function(response) {
				self.rederOperation(response.resultMap.data);	
			});
        },
        doSearchOperation:function(pageNo) {
            this.searchOperationData({pageNo:pageNo || 1})
        },
        rederOperation:function(data) {
            var self = this;
			self.renderOperationList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearchOperation(pageNo);
			}, $('.pagerContainer',$("#operationRecord")));
        },
        renderOperationList:function(data) {
            var self = this;
            var operationListData = data.hasOwnProperty("itemList") ? data.itemList : [];
            var operationTpl = $('#operationRecordTpl').html();

            var data = {
				operationDataSource:_.extend(Rental.render, {
					"operationListData":operationListData,
					operationTypeValue:function() {
						return self.getOperationTypeValue(this.operationType);
					}
				})
            }

			Mustache.parse(operationTpl);
			$("#operationRecordTable").html(Mustache.render(operationTpl, data));
        },
        getDataStatusValue:function(status) {
            switch (status) {
                case 0:
                    return "不可用";
                case 1: 
                    return "可用";
                case 2:
                    return "删除";
                default:
                    return "";
            }
        },
        getRechargeStatusValue:function(recharge) {
            switch (recharge) {
                case 0:
                    return "初始化";
                case 1: 
                    return "正在充值";
                case 2:
                    return "充值成功";
                case 3:
                    return "充值失败";
                default:
                    return "";
            }
        },
        getOperationTypeValue:function(operationType) {
            switch (operationType) {
                case 1:
                    return "下推";
                case 2: 
                    return "属地化";
                case 3:
                    return "自动属地化";
                case 4:
                    return "取消属地化";
                case 5:
                    return "隐藏";
                case 6: 
                    return "取消隐藏";
                case 7:
                    return "自动认领";
                case 8:
                    return "认领";
                case 9:
                    return "充值";
                case 10: 
                    return "删除";
                case 11: 
                    return "下推公海";
                default:
                    return "";
            }
        },
        filterRowButtons:function(buttons, jurnalData) {
            var rowActionButtons = new Array(),
                confirmed = jurnalData.detailStatus == Enum.detailStatus.num.confirmed;

            (buttons || []).forEach(function(button, index) {
                rowActionButtons.push(button);
                if(button.class == 'toPay' && !confirmed) {
                    rowActionButtons.splice(rowActionButtons.length - 1,1);
                }
            });
            return rowActionButtons;
        }
    }
    window.JurnalHighseasDetail = _.extend(JurnalHighseasDetail, JurnalHandleMixin);
})(jQuery);