/**
 * decription: 资金流水明细列表
 * author:zhangsheng
 * time: 2018-3-27
 */
;(function($) {

	var JurnalDetailListManage = {
		state:{
			bankSlipId:null,
			customer:null,
			chooseCustomerList:new Array(),
			bankSlipDetailId:null,
			tradeAmount:null,
			detailStatus:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_financeial]; //财务管理
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list]; //财务管理-资金流水附件
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();
			this.exportButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var exportJurnal = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_export],
				claim = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_claim],
				ignore = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_ignore],
				view = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_detail],
				confirm = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_confirm],
				hide = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_hide],
				show = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_show],
				verify = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_confirm_affiliation],
				pushdown = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_pushdown],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_list_cancel_affiliation];

			verify && this.commonActionButtons.push(AuthorUtil.button(verify,'designateBtn','fa fa-plus','批量属地化'));
			exportJurnal && this.commonActionButtons.push(AuthorUtil.button(exportJurnal,'exportBtn','fa fa-plus','导出'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			claim && this.rowActionButtons.push(AuthorUtil.button(claim,'claimButton','','认领'));
			confirm && this.rowActionButtons.push(AuthorUtil.button(confirm,'confirmButton','','确认'));
			// ignore && this.rowActionButtons.push(AuthorUtil.button(ignore,'ignoreButton','','忽略'));
			hide && this.rowActionButtons.push(AuthorUtil.button(hide,'hideButton','','隐藏'));
			show && this.rowActionButtons.push(AuthorUtil.button(show,'showButton','','显示'));
			verify && this.rowActionButtons.push(AuthorUtil.button(verify,'verifyDesignate','','属地化'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelDesignate','','取消属地化'));
			pushdown && this.rowActionButtons.push(AuthorUtil.button(pushdown,'pushdown','','下推公海'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.$exportForm = $("#exportForm");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.jurnalAmountDetailList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.state.bankSlipId = Rental.helper.getUrlPara('bankSlipId');
			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			self.renderBankSlipId();
			Rental.ui.events.initRangeDatePicker($('#slipTimePicker'), $('#slipTimePickerInput'),  $("#slipDayStart"), $("#slipDayEnd"));

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			Rental.ui.renderSelect({
				container: $('#loanSign'),
				data:Enum.array(Enum.loanSign),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '全部 （借贷标志）'
			})

			Rental.ui.renderSelect({
				container: $('#isLocalization'),
				data:Enum.array(Enum.isLocalization),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '是否属地化'
			})

			Rental.ui.renderSelect({
				container:$('#detailStatus'),
				data:Enum.array(Enum.detailStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（明细状态）'
			});

			ApiData.company({
				success:function(response) {

					var ownerSubCompanyList = response

					var companyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.telemarketing && company.subCompanyId != Enum.subCompany.num.channelBigCustomer;
					});

					function renderCompany(container, defaultText, companyData) {
						Rental.ui.renderSelect({
							container:container,
							data:companyData,
							func:function(opt, item) {
								return opt.format(item.subCompanyId, item.subCompanyName);
							},
							change:function(val) {
								self.searchData();
							},
							defaultText:defaultText
						});
					}

					renderCompany($("#subCompanyId"), '属地化分公司', companyList);
					renderCompany($("#ownerSubCompanyId"), '数据归属公司', ownerSubCompanyList);
				}
			});

			self.renderStatus();
			self.searchData();  //初始化列表数据
			self.render(new Object());

			self.initHandleEvent(self.$dataListTable,function() {
				self.doSearch(self.Pager.pagerData.currentPage)
			})

			//认领
			self.$dataListTable.on('click', '.claimButton', function(event) {
				event.preventDefault();
				self.state.chooseCustomerList = new Array();
				var detailStatus = $(this).data("status");
				self.state.bankSlipDetailId = $(this).data('bankslipdetailid');
				self.state.tradeAmount = $(this).data('tradeamount');
				self.searchClaimData(self.state.bankSlipDetailId)
			})

			//批量指派
			self.$actionCommonButtons.on('click','.designateBtn',function(event) {
				event.preventDefault();
				self.designate();
			})

			//导出
			self.$actionCommonButtons.on('click','.exportBtn',function(event) {
				event.preventDefault();
				self.export();
			})
		},
		//批量指派
		designate:function() {
			var self = this, 
				bankSlipDetailList = new Array(),
				array = $('[name=checkItem]', self.$dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', self.$dataListTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择流水');
				return;
			}

			filterChecked.each(function(i, item) {
				var bankSlipDetailId = $(this).val();
				var bankSlipIndex = _.findIndex(bankSlipDetailList, {bankSlipDetailId:bankSlipDetailId});
				if(bankSlipIndex < 0) {
					bankSlipDetailList.push({bankSlipDetailId:bankSlipDetailId});
				}
			});
			ChooseCompany.init({
				batch:true,
				callBack:function(subCompany) {
					var commitData = {
						bankSlipDetailList: bankSlipDetailList,
						localizationSubCompanyId:subCompany.subCompanyId
					}
					bootbox.confirm('确认归属？', function(result) {
						result && self.designateDo(commitData, '确认归属', function() {
							self.doSearch(self.Pager.pagerData.currentPage)
						});
						Rental.modal.close();
					});
				}
			});
		},
		designateDo:function(commitData, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'bankSlip/localizationBankSlipDetail'), commitData ,function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
		},
		//选择客户弹窗
		claimChooseCustomerByAll:function() {
			var self = this;
			ChooseCustomerByAll.init({
				batch:true,
				callBack:function(customer) {
					self.chooseCustomer(self.state.chooseCustomerList,customer);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {
					if(self.state.chooseCustomerList.length > 0) {
						tradeDetail.init({
							chooseCustomerList:self.state.chooseCustomerList,
							bankSlipDetailId:self.state.bankSlipDetailId,
							tradeAmount:self.state.tradeAmount,
							continueChooseCustomeFunc:null,
							changeContinueFunc:function() {
								_.isObject(JurnalDetailListManage) &&  _.isFunction(JurnalDetailListManage.claimChooseCustomerByAll) && JurnalDetailListManage.claimChooseCustomerByAll();
							},
							callBack:function() {
								self.doSearch(self.Pager.pagerData.currentPage);
							}
						});
					}
				}
			});
		},
		//选择客户
		chooseCustomer:function(chooseCustomerList, customer) {
			if(!chooseCustomerList) {
				chooseCustomerList = new Array();
			}	
			if(chooseCustomerList.length > 0) {
				var _Index = _.findIndex(chooseCustomerList, {customerNo:customer.customerNo});
				if(_Index > -1) {
					return chooseCustomerList;
				} else {
					chooseCustomerList.push({
						customerName:customer.customerName,
						customerNo:customer.customerNo
					})
				}
			} else {
				chooseCustomerList.push({
					customerName:customer.customerName,
					customerNo:customer.customerNo
				})
			}
			return chooseCustomerList;
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		renderStatus:function() {
			var detailStatus = Rental.helper.getUrlPara('status')
			if(!!detailStatus) {
				Rental.ui.renderFormData(this.$searchForm, {detailStatus:detailStatus});
			}
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				loanSign:1,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.jurnalAmountDetailList, searchData);

			Rental.ajax.ajaxData('bankSlip/pageBankSlipDetail', searchData, '查询资金流水', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',$("#content")));
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					"rowActionButtons":function() {
						var _this = this;
						return self.filterAcitonButtons(self.rowActionButtons, _this)
					},
					loanSignValue:function() {
						return Enum.loanSign.getValue(this.loanSign);
					},
					detailStatusValue:function() {
						return Enum.detailStatus.getValue(this.detailStatus);
					},
					isLocalizationValue:function() {
						return Enum.isLocalization.getValue(this.isLocalization);
					},
					bankSlipBankTypeValue:function() {
						return Enum.bankType.getValue(this.bankSlipBankType);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderBankSlipId:function() {
			if(!!this.state.bankSlipId) {
				$('[name="bankSlipId"]',this.$searchForm).val(this.state.bankSlipId).attr("readonly","readonly");
			}
		},
		searchClaimData:function(bankSlipDetailId) {
			var self = this;
			Rental.ajax.ajaxData('bankSlip/queryBankSlipDetail', {bankSlipDetailId:bankSlipDetailId}, '加载资金流水详细', function(response) {
				var data = response.resultMap.data
                if(data.hasOwnProperty("bankSlipClaimList") && data.bankSlipClaimList.length > 0) {
					self.state.chooseCustomerList = data.bankSlipClaimList;
					self.renderClaimList();
				} else {
					self.claimChooseCustomerByAll();
				}
			})
		},
		renderClaimList:function() {
			var self = this;
			tradeDetail.init({
				chooseCustomerList:self.state.chooseCustomerList,
				bankSlipDetailId:self.state.bankSlipDetailId,
				tradeAmount:self.state.tradeAmount,
				continueChooseCustomeFunc:null,
				changeContinueFunc:function() {
					_.isObject(JurnalDetailListManage) &&  _.isFunction(JurnalDetailListManage.claimChooseCustomerByAll) && JurnalDetailListManage.claimChooseCustomerByAll();
				},
				callBack:function() {
					self.doSearch(self.Pager.pagerData.currentPage);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {},
			});
		},
		export:function() {
			var self = this, 
				array = $('[name=checkItem]', self.$dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', self.$dataListTable).filter(':checked');

			var timeData = $('#slipTimePickerInput').val();
			if(!!timeData) {
				dayList = timeData.split(" ");
				$('#exportForm [name="slipDayStart"]').val(dayList[0])
				$('#exportForm [name="slipDayEnd"]').val(dayList[2])
			}
			$('#exportForm [name="bankSlipId"]').val($("#bankSlipId").val())
			$('#exportForm [name="payerName"]').val(encodeURI($("#payerName").val()))
			$('#exportForm [name="detailStatus"]').val($("#detailStatus").val())
			$('#exportForm [name="isLocalization"]').val($("#isLocalization").val())

			$('#exportForm').attr({
				'method':'post',
				'action':SitePath.service + 'exportExcel/exportPageBankSlipDetail'
			});
			if(array.length > 0) {
				bootbox.confirm('导出数据为当前搜索条件下所有数据，继续导出？', function(result) {
					$("#exportForm").submit()
				});
			} else {
				$("#exportForm").submit();
			}
		}
	};

	window.JurnalDetailListManage = _.extend(JurnalDetailListManage, JurnalHandleMixin);

})(jQuery);		