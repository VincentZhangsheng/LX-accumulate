/**
 * decription: 交易明细
 * author:zhangsheng
 * time: 2018-3-28
 */
;(function($) {

	var tradeDetail = {
		state:{
			tradeDetailList: new Array()
		},
		init:function(prams) {
			this.props = _.extend({
                chooseCustomerList:new Array(),
                bankSlipDetailId:null,
                tradeAmount:null,
				callBack:function() {},
				complete:function() {},
				modalCloseCallBack:function() {},
				continueChooseCustomeFunc:function() {},
				changeContinueFunc:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
            var self = this;
			Rental.modal.open({src:SitePath.base + "jurnal-amount/tradeDetail", type:'ajax', 
			ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			},
			afterClose:function() {
				self.props.modalCloseCallBack && self.props.modalCloseCallBack();
				_.isFunction(self.props.continueChooseCustomeFunc) && self.props.continueChooseCustomeFunc();
			}
		});
		},
		initDom:function(pModal) {
			this.$modal = pModal.container;
			this.$tradeDetailModal = $("#tradeDetail");
			this.$tradeAmount = $("#tradeAmountNum");
			this.$form = $("form", this.$modal);
			this.$tradeDetailTpl = $("#tradeDetailTpl").html();
            this.$tradeDetailTable = $("#tradeDetailTable");
		},
		initEvent:function() {
            var self = this;
            
			//初始化
			self.renderTradeAmount();
			self.renderTradeDetailData();
            self.renderTradeDetailList();
			
			self.$tradeDetailTable.rtlCheckAll('checkAll','checkItem');

			$("#batchAddCustomer").click(function(event) {
				self.props.continueChooseCustomeFunc = function() {
					_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
					// _.isObject(JurnalDetailListManage) &&  _.isFunction(JurnalDetailListManage.claimChooseCustomerByAll) && JurnalDetailListManage.claimChooseCustomerByAll();
				}
				Rental.modal.close();
			});

			$("#batchDeleteCustomer").click(function(event) {
				self.batchDeleteTrade();
				self.renderTradeDetailList();
			});

			//改变客户交易值
			self.$tradeDetailTable.on('change', '.claimAmount', function(event) {
				event.preventDefault();
				var $tradeRow = $(this).closest('.tradeRow');
				self.changeTradeItem($tradeRow);
				self.renderTradeDetailList();
			});

			//删除一条客户交易
			self.$tradeDetailTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				self.deleteTradeItem($(this));
				self.renderTradeDetailList();
			});

			Rental.form.initFormValidation(self.$form,function(form){
				self.claim();
			});

			self.$form.on('click', '.cancelBtn', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			Rental.helper.onOnlyDecmailNum(self.$tradeDetailTable, '.claimAmount', 2); 
		},
		renderTradeDetailData:function() {
			this.state.tradeDetailList = this.props.chooseCustomerList;
		},
		renderTradeDetailList:function() {
			var self = this;
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":self.state.tradeDetailList,
				})
			}
			Mustache.parse(this.$tradeDetailTpl);
			this.$tradeDetailTable.html(Mustache.render(self.$tradeDetailTpl, data));
		},
		renderTradeAmount:function() {
			this.$tradeAmount.html("交易总额：￥" + (parseFloat(this.props.tradeAmount)).toFixed(2));
		},
		getTradeRow:function($tradeRow) {
			return {
				customerNo:$.trim($('.customerNo', $tradeRow).val()),
				claimAmount:$.trim($('.claimAmount', $tradeRow).val())
			}
		},
		changeTradeItem:function($tradeRow) {
			var self = this;
			var trade = self.getTradeRow($tradeRow);
			var tradeIndex = _.findIndex(self.state.tradeDetailList, {customerNo:trade.customerNo});
			
			var claimArray = $('.claimAmount',self.$tradeDetailTable);
			var	filterValued = new Array();
			var notValued = new Array();
			var enteredMoneyTotal = 0;
			claimArray.each(function() {
				if(!!$(this).val()) {
					filterValued.push($(this));
					enteredMoneyTotal += parseFloat($(this).val());
				} else {
					notValued.push($(this));
				}
			})

			if(enteredMoneyTotal > self.props.tradeAmount && notValued.length > 0) {
				// var forceValue = parseFloat($('.claimAmount',$tradeRow).val()) + self.props.tradeAmount - enteredMoneyTotal;
				// $('.claimAmount',$tradeRow).val(forceValue);
				// self.state.tradeDetailList[tradeIndex] = _.extend(self.state.tradeDetailList[tradeIndex],{claimAmount:forceValue});

				notValued.forEach(function(item){
					var rowIndex = item.parents(".tradeRow").index();
					self.state.tradeDetailList[rowIndex] = _.extend(self.state.tradeDetailList[rowIndex],{claimAmount:0});
				})
			} 

			self.state.tradeDetailList[tradeIndex] = _.extend(self.state.tradeDetailList[tradeIndex],{claimAmount:trade.claimAmount});

			if((claimArray.length - filterValued.length) == 1) {
				claimArray.each(function() {
					if(!$(this).val()) {
						var rowIndex = $(this).parents(".tradeRow").index();
						var surplus = parseFloat(self.props.tradeAmount) - enteredMoneyTotal;
						surplus = surplus > 0 ? surplus : 0;
						self.state.tradeDetailList[rowIndex] = _.extend(self.state.tradeDetailList[rowIndex],{claimAmount:surplus});
					}
				})
			}
		},
		deleteTradeItem:function($deleteButton) {
			var self = this;
			var customerNo = $deleteButton.data("customerno");
			var tradeIndex = _.findIndex(self.state.tradeDetailList, {customerNo:customerNo});
			self.state.tradeDetailList.splice(tradeIndex,1);
		},
		batchDeleteTrade:function() {
			var self = this, 
				array = $('[name=checkItem]', self.$tradeDetailTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', self.$tradeDetailTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择客户');
				return self.state.tradeDetailList;
			}

			filterChecked.each(function(i, item) {
				var customerNo = $(this).val();
				var tradeIndex = _.findIndex(self.state.tradeDetailList, {customerNo:customerNo});
				self.state.tradeDetailList.splice(tradeIndex,1);
			});
		},
		claim:function() {
			try {
				var self = this,
					formData = Rental.form.getFormData(self.$form);
				var commitData = {
					bankSlipDetailId: self.props.bankSlipDetailId,
					remark: formData['remark']
				}

				var totalTradePrice = 0;
				self.state.tradeDetailList.forEach(function(trade){
					totalTradePrice += parseFloat(trade.claimAmount);
				})
				
				// if(totalTradePrice != self.props.tradeAmount) {
				// 	bootbox.alert('客户分配金额之和不等于交易总额');
				// 	return;
				// }
				
				commitData.claimParam = new Array();
				if(self.state.tradeDetailList.length > 0) {
					self.state.tradeDetailList.forEach(function(trade){
						commitData.claimParam.push({
							customerNo:trade.customerNo,
							claimAmount:trade.claimAmount
						})
					})
				}

				var des = '操作';
				Rental.ajax.submit("{0}bankSlip/claimBankSlipDetail".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						Rental.modal.close();
						self.props.callBack();
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch (e) {
				Rental.notification.error("认领失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		}
	};

	window.tradeDetail = tradeDetail;

})(jQuery)