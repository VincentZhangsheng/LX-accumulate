;(function($) {

	var AmountClaimDetailList = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_financeial];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_financeial_jurnal_amount_claim_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			// 获取单据状态下拉列表 
			Rental.ui.events.initRangeDatePicker($('#sureTimePicker'), $('#sureTimePickerInput'),  $("#startSlipDetailUpdateTime"), $("#endSlipDetailUpdateTime"));
			Rental.ui.events.initRangeDatePicker($('#tradeTimePicker'), $('#tradeTimePickerInput'),  $("#startTradeTime"), $("#endTradeTime"));
			Rental.ui.events.initRangeDatePicker($('#claimTimePicker'), $('#claimTimePickerInput'),  $("#startClaimUpdateTime"), $("#endClaimUpdateTime"));

			self.renderCommonActionButton();

			// 获取银行类别下拉列表
			Rental.ui.renderSelect({
				container: $("#bankType"),
				data: Enum.array(Enum.bankType),
				func: function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change: function(val) {
					self.searchData();
				},
				defaultText:'请选择银行类别（全部）'
			});
			
			// 获取明细状态下拉列表
			Rental.ui.renderSelect({
				container:$('#detailStatus'),
				data:Enum.array(Enum.detailType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'请选择明细状态'
			});


			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			console.log(searchData);
			if(searchData.startTradeAmount != ""){
				searchData.startTradeAmount = parseFloat(searchData.startTradeAmount);
			}
			if(searchData.endTradeAmount != ""){
				searchData.endTradeAmount = parseFloat(searchData.endTradeAmount);
			}
			if(searchData.startClaimAmount != ""){
				searchData.startClaimAmount = parseFloat(searchData.startClaimAmount);
			}
			if(searchData.endClaimAmount != ""){
				searchData.endClaimAmount = parseFloat(searchData.endClaimAmount);
			}
			
			Rental.ajax.ajaxData('bankSlip/pageBankSlipClaimDetail', searchData, '流水认领明细', function(response) {
				// console.log('searchData', response.resultMap.data)
				self.render(response.resultMap.data.bankSlipClaimDetailPage);	
				self.renderTotalInfo(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				data = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						"bankTypeValue":function() {
							return Enum.bankType.getValue(this.bankType);
						},
						"detailTypeValue":function() {
							return Enum.detailType.getValue(this.detailStatus);
						},
					})
				}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderTotalInfo:function(data) {
			// console.log(data)
			$("#recordCount").html('记录数：' + data.claimCount).addClass('text-danger');
			$("#totalMoney").html('总金额数：' + (data.hasOwnProperty('totalAmount') ? Rental.helper.fmoney(data.totalAmount) : '0.00')).addClass('text-danger');
		}
	};

	window.AmountClaimDetailList = AmountClaimDetailList;
	
})(jQuery);