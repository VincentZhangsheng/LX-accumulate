/**
 * author:liaojianming
 * date: 2018-3-26
 * decription: 导入资金账单流水
 */
;(function($){
	
	var UploadExcelMsg = {
		init:function(prams) {
			this.props = _.extend({
				subCompanyId:null,
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/jurnal-attachment-list/file/upload", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			var self = this;
			this.$modal = pModal.container;
			this.$form = $("#UploadExcelForm");
			this.$cancelButton = $('.cancelButton', this.$form);
			this.$companyName = $("#subCompanyId");
			this.formData = {};
			this.$excelInput = $("input[name=excelUrl]");
			this.$upLoadExcel = $("#upLoadExcel");
		},
		initEvent:function() {
			var self = this;

			this.getCompanyName();

			this.initDropzone();

			//初始化银行类型下拉框
			Rental.ui.renderSelect({
				container:$('#bankType'),
				data:(Enum.array(Enum.bankType)).filter(function(item) {
					return item.num != 12
				}),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					// self.searchData();
				},
				defaultText:'请选择银行类型'
			});

			//初始化时间控件
			self.initDatePicker();

			Rental.form.initFormValidation(self.$form,function(form){
				self.importExcel();
			});
			
			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initDatePicker:function() {
			var self = this , defaultStart = moment().subtract('days', 29), defaultEnd = moment();
			$('#slipDayPicker').daterangepicker({
			    "singleDatePicker": true,
			    "showDropdowns": true,
			    "showWeekNumbers": true,
			    "startDate": defaultEnd,
			    "endDate": defaultEnd
			}, function(start, end, label) {
				  $("#slipDayInput").val(start.format('YYYY-MM-DD'));
				  $("#slipDay").val(new Date(start).getTime())
			});
		},
		initDropzone:function() {
			var self = this;
            this.$upLoadExcel.dropzone({
                url: SitePath.base + 'file/upload',
                maxFiles: 1,
                maxFilesize:20,//MB
                acceptedFiles: ".xlsx, .xls",
                addRemoveLinks: true,
                dictCancelUpload:"取消上传",
                dictRemoveFile:'删除',
                dictFileTooBig:'文件过大（不能大于4M）',
                dictMaxFilesExceeded:"超过最大上传数量",
                success:function(e,response) {
					self.formData.excelUrl = response.resultMap.data;
					self.$excelInput.val(self.formData.excelUrl);
					if(response.success === false) {
						bootbox.alert('银行资金流水附件上传失败，请重试！');
						return;
					}
					bootbox.alert('银行资金流水附件上传成功');
					$(".dz-details", self.$upLoadExcel).css({
						"width": "115px",
						"height": "100px",
						"background-image": "url(" + SitePath.staticManagement + "img/timg.jpg)",
						"backgroundSize": "cover"
					})
					$(".dz-upload", self.$upLoadExcel).css({
						"background": "#8cc657" 
					})
				},
				error: function(error) {
					if(error) {
						if($(".dz-preview").size() > 1) {
							$(".dz-preview:last", self.$upLoadExcel).remove();
							bootbox.alert("最多只能上传一张附件表格！");
						}
						else {
							bootbox.alert("表格上传失败，请检查格式后重试，支持.xlsx和.xls格式的表格!");
						}
					}
				}
            });
		},
		getCompanyName: function() {
			var self = this;
			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("#subCompanyId"),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							// console.log(val);
						},
						defaultText:'请选择分公司名称'
					});
				}
			});
		},
		importExcel:function() {
			try {
				var self = this, des = "导入资金账单流水";
				self.formData = Rental.form.getFormData(self.$form);
				self.formData.subCompanyId = self.props.subCompanyId;

				//校验是否上传Excel
				var $preview = $('.dz-file-preview');
				if($preview.children().size() < 0) {
					bootbox.alert("请上传Excel表格");
				}
				
				Rental.ajax.submit("{0}bankSlip/importExcel".format(SitePath.service), self.formData, function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null ,des,'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack && self.props.callBack();
			}
			return false;
		}
	};

	window.UploadExcelMsg = UploadExcelMsg;

})(jQuery);