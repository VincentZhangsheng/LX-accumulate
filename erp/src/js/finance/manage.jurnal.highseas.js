/**
 * decription: 未知流水列表
 * author:zhangsheng
 * time: 2018-8-22
 */
;(function($) {

	var JurnalHighseasManage = {
		state:{
			bankSlipId:null,
			customer:null,
			chooseCustomerList:new Array(),
			bankSlipDetailId:null,
			tradeAmount:null,
			detailStatus:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_financeial]; //财务管理
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var claim = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_claim],
				view = this.currentPageAuthor.children[AuthorCode.manage_financeial_jurnal_highseas_view];

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			claim && this.rowActionButtons.push(AuthorUtil.button(claim,'claimButton','','认领'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.$exportForm = $("#exportForm");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.jurnalAmountDetailList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.state.bankSlipId = Rental.helper.getUrlPara('bankSlipId');
			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			Rental.ui.events.initRangeDatePicker($('#slipTimePicker'), $('#slipTimePickerInput'),  $("#slipDayStart"), $("#slipDayEnd"));

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			Rental.ui.renderSelect({
				container: $('#loanSign'),
				data:Enum.array(Enum.loanSign),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '全部 （借贷标志）'
			})

			Rental.ui.renderSelect({
				container:$('#detailStatus'),
				data:Enum.array(Enum.detailStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（明细状态）'
			});

			self.searchData();  //初始化列表数据
			self.render(new Object());

			self.initHandleEvent(self.$dataListTable,function() {
				self.doSearch(self.Pager.pagerData.currentPage)
			})

			//认领
			self.$dataListTable.on('click', '.claimButton', function(event) {
				event.preventDefault();
				self.state.chooseCustomerList = new Array();
				var detailStatus = $(this).data("status");
				self.state.bankSlipDetailId = $(this).data('bankslipdetailid');
				self.state.tradeAmount = $(this).data('tradeamount');
				self.searchClaimData(self.state.bankSlipDetailId);
			})
		},
		//选择客户弹窗
		claimChooseCustomerByAll:function() {
			var self = this;
			ChooseCustomerByAll.init({
				batch:true,
				callBack:function(customer) {
					self.chooseCustomer(self.state.chooseCustomerList,customer);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {
					if(self.state.chooseCustomerList.length > 0) {
						tradeDetail.init({
							chooseCustomerList:self.state.chooseCustomerList,
							bankSlipDetailId:self.state.bankSlipDetailId,
							tradeAmount:self.state.tradeAmount,
							continueChooseCustomeFunc:null,
							changeContinueFunc:function() {
								_.isObject(JurnalHighseasManage) &&  _.isFunction(JurnalHighseasManage.claimChooseCustomerByAll) && JurnalHighseasManage.claimChooseCustomerByAll();
							},
							callBack:function() {
								self.doSearch(self.Pager.pagerData.currentPage);
							}
						});
					}
				}
			});
		},
		//选择客户
		chooseCustomer:function(chooseCustomerList, customer) {
			if(!chooseCustomerList) {
				chooseCustomerList = new Array();
			}	
			if(chooseCustomerList.length > 0) {
				var _Index = _.findIndex(chooseCustomerList, {customerNo:customer.customerNo});
				if(_Index > -1) {
					return chooseCustomerList;
				} else {
					chooseCustomerList.push({
						customerName:customer.customerName,
						customerNo:customer.customerNo
					})
				}
			} else {
				chooseCustomerList.push({
					customerName:customer.customerName,
					customerNo:customer.customerNo
				})
			}
			return chooseCustomerList;
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				loanSign:1,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.jurnalAmountDetailList, searchData);

			Rental.ajax.ajaxData('bankSlip/pageUnknownBankSlipDetail', searchData, '查询未知资金流水', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',$("#content")));
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					"rowActionButtons":function() {
						var _this = this;
						return self.filterAcitonButtons(self.rowActionButtons, _this)
					},
					loanSignValue:function() {
						return Enum.loanSign.getValue(this.loanSign);
					},
					detailStatusValue:function() {
						return Enum.detailStatus.getValue(this.detailStatus);
					},
					isLocalizationValue:function() {
						return Enum.isLocalization.getValue(this.isLocalization);
					},
					bankSlipBankTypeValue:function() {
						return Enum.bankType.getValue(this.bankSlipBankType);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		searchClaimData:function(bankSlipDetailId) {
			var self = this;
			Rental.ajax.ajaxData('bankSlip/queryUnknownBankSlipDetail', {bankSlipDetailId:bankSlipDetailId}, '加载资金流水详细', function(response) {
				var data = response.resultMap.data
                if(data.hasOwnProperty("bankSlipClaimList") && data.bankSlipClaimList.length > 0) {
					self.state.chooseCustomerList = data.bankSlipClaimList;
					self.renderClaimList();
				} else {
					self.claimChooseCustomerByAll();
				}
			})
		},
		renderClaimList:function() {
			var self = this;
			tradeDetail.init({
				chooseCustomerList:self.state.chooseCustomerList,
				bankSlipDetailId:self.state.bankSlipDetailId,
				tradeAmount:self.state.tradeAmount,
				continueChooseCustomeFunc:null,
				changeContinueFunc:function() {
					_.isObject(JurnalHighseasManage) &&  _.isFunction(JurnalHighseasManage.claimChooseCustomerByAll) && JurnalHighseasManage.claimChooseCustomerByAll();
				},
				callBack:function() {
					self.doSearch(self.Pager.pagerData.currentPage);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {},
			});
		},
	};

	window.JurnalHighseasManage = _.extend(JurnalHighseasManage, JurnalHandleMixin);

})(jQuery);		