;(function($) {

	var FinancialStatisticsManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_financeial]; //财务管理
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_financeial_statistics];
		},
		initDom:function() {
			this.$exportForm = $("#exportForm");
			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

            Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
            Rental.ui.events.initMonthpicker($("#monthTime"));
            Rental.helper.onOnlyNumber($("#weekNumber")); 

			Rental.ui.renderSelect({
				container:$("#statisticsInterval"),
				data:Enum.array(Enum.statisticsType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					if(val == 2) {
						$("#weekInput").addClass("hide")
						$("#weekNumber").val("")
					}
					if(val == 1) {
						if($("#weekInput").hasClass("hide")) {
							$("#weekInput").removeClass("hide")
						}
					}
				},
				defaultText:'请选择'
			});
			
            $("#weekNumber").on("change",function() {
                var num = $(this).val();
                if(num > 6) {
                    bootbox.alert("输入的第几周不能大于6");
				    $(this).val("")
                }
            })

            $("#recountButton").on("click",function(event) {
                event.preventDefault();
                self.restatistics()
            })

			//导出
			$("#exportButton").on('click',function(event) {
				event.preventDefault();
				self.export();
			})
        },
        restatistics:function() {
			var self = this;
			var commitData = self.getTimeData();

			if(!commitData.statisticsInterval) {
				bootbox.alert("请选择导出类型");
				return;
			}
			if(!$("#monthTime").val()) {
                bootbox.alert("请选择年月");
				return;
			}
			if(commitData.statisticsInterval == 1 && !commitData.weekOfMonth) {
				bootbox.alert("请输入第几周");
				return;
			}

            Rental.ajax.submit("{0}statistics/reStatisticsFinanceData".format(SitePath.service),commitData,function(response){
				if(response.success) {
					Rental.notification.success('重新统计财务历史数据',response.description || '成功');
				} else {
					Rental.notification.error('重新统计财务历史数据',response.description || '失败');
				}
			}, null ,'重新统计财务历史数据','dialogLoading');
        },
		export:function() {
			var self = this;
			var timeData = self.getTimeData();

			if(!timeData.statisticsInterval) {
				bootbox.alert("请选择导出类型");
				return;
			}
			if(!$("#monthTime").val()) {
                bootbox.alert("请选择年月");
				return;
            }

			var $yearIpt = $('<input type="text" name="year" />').val(timeData.year);
			var $monIpt = $('<input type="text" name="month" />').val(timeData.month);
			var $weekIpt = $('<input type="text" name="weekOfMonth" />').val(timeData.weekOfMonth);
			var $statisticsInterval = $('<input type="text" name="statisticsInterval" />').val(timeData.statisticsInterval);

			if(timeData.statisticsInterval == 1 && !timeData.weekOfMonth) {
				bootbox.alert("请输入第几周");
				return;
			}

			$('#exportForm').html("");
			$('#exportForm').append($yearIpt,$monIpt,$weekIpt,$statisticsInterval);
			$('#exportForm').attr({
				'method':'post',
				'action':SitePath.service + 'exportExcel/exportStatisticsFinanceData'
			});
            $("#exportForm").submit();
        },
        getTimeData:function() {
            var pickDate = $("#monthTime").val();
			var weekOfMonth = $("#weekNumber").val();
			var statisticsInterval = $("#statisticsInterval").val()

            var pickDateArr = pickDate.split("-");
            return {
				statisticsInterval:statisticsInterval,
                year:pickDateArr[0],
                month:parseInt(pickDateArr[1]),
				weekOfMonth:weekOfMonth
            }
        }
	};

	window.FinancialStatisticsManage = FinancialStatisticsManage;

})(jQuery);		