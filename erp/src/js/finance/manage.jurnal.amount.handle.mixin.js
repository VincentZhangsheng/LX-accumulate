;(function($) {
	var JurnalHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;
            
            //忽略
			container.on('click', '.ignoreButton', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				self.ignore(bankSlipDetailId, handleCallBack);
			})
			
			//确认
			container.on('click', '.confirmButton', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				self.confirmDo(bankSlipDetailId, handleCallBack);
            })

            //隐藏
			container.on('click', '.hideButton', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				self.hide(bankSlipDetailId, handleCallBack);
            });
            
            //显示
			container.on('click', '.showButton', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				self.show(bankSlipDetailId, handleCallBack);
			});

            //确认归属
			container.on('click', '.verifyDesignate', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				var status = $(this).data('status');
				var isLocal = $(this).data('islocal')
				self.verifyDesignate(bankSlipDetailId, handleCallBack, status, isLocal);
			});

            //取消归属
			container.on('click', '.cancelDesignate', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				var status = $(this).data('status');
				self.cancelDesignate(bankSlipDetailId, handleCallBack, status);
			});

			//未知
			container.on('click', '.pushdown', function(event) {
				event.preventDefault();
				var bankSlipDetailId = $(this).data('bankslipdetailid');
				self.pushdown(bankSlipDetailId, handleCallBack);
			});
		},
		confirmDo:function(bankSlipDetailId, callBack) {
			var self = this;
			bootbox.confirm('确认该流水表？', function(result) {
				result && self.bankSlipDo('bankSlip/confirmBankSlipDetail', bankSlipDetailId, '确认资金流水', callBack);
			});
		},
		//忽略
		ignore:function(bankSlipDetailId, callBack) {
			var self = this;
			bootbox.confirm('确认忽略该流水表？', function(result) {
				result && self.bankSlipDo('bankSlip/ignoreBankSlipDetail', bankSlipDetailId, '忽略资金流水', callBack);
			});
        },
        //隐藏
		hide:function(bankSlipDetailId, callBack) {
			var self = this;
			bootbox.confirm('确认隐藏该流水表？', function(result) {
				result && self.bankSlipDo('bankSlip/hideBankSlipDetail', bankSlipDetailId, '隐藏资金流水', callBack);
			});
        },
        //显示
		show:function(bankSlipDetailId, callBack) {
			var self = this;
			bootbox.confirm('确认显示该流水表？', function(result) {
				result && self.bankSlipDo('bankSlip/displayBankSlipDetail', bankSlipDetailId, '显示资金流水', callBack);
			});
		},
		//下推公海
		pushdown:function(bankSlipDetailId, callBack) {
			var self = this;
			bootbox.confirm('确认该流水下推公海？', function(result) {
				result && self.bankSlipDo('bankSlip/unknownBankSlipDetail', bankSlipDetailId, '资金流水下推公海', callBack);
			});
        },
        //确认归属
		verifyDesignate:function(bankSlipDetailId, callBack, status, isLocal) {
			var self = this, 
				bankSlipDetailList = new Array();

			bankSlipDetailList.push({bankSlipDetailId:bankSlipDetailId});

			ChooseCompany.init({
				batch:true,
				callBack:function(subCompany) {
					var commitData = {
						bankSlipDetailList: bankSlipDetailList,
						localizationSubCompanyId:subCompany.subCompanyId
					}
					bootbox.confirm('确认归属？', function(result) {
						// result && self.singleDesignateDo(commitData, '确认归属', callBack);
						// Rental.modal.close();
						result && self.sureDesignateConfirm(commitData, callBack, status, isLocal);
					});
				}
			});
        },
        //取消归属
		cancelDesignate:function(bankSlipDetailId, callBack, status) {
			var self = this;
			bootbox.confirm('确认取消归属？', function(result) {
				// Rental.modal.close();
				// result && self.bankSlipDo('bankSlip/cancelLocalizationBankSlipDetail', bankSlipDetailId, '确认取消归属', callBack);
				result && self.cancelDesignateConfirm('bankSlip/cancelLocalizationBankSlipDetail', bankSlipDetailId, '确认取消归属', callBack, status);
			});
		},
        bankSlipDo:function(url, bankSlipDetailId, des, callBack) {
			if(!bankSlipDetailId) {
				bootbox.alert('找不到银行对公流水标识');
				return;
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url),{bankSlipDetailId:bankSlipDetailId},function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
		},
		cancelDesignateConfirm:function(url,bankSlipDetailId, des, callBack, status) {
			var self = this;
			if(status == 2) {
				bootbox.confirm('当前流水为已认领状态，取消归属将使改流水数据清零，是否仍要取消？', function(result) {
					Rental.modal.close();
					result && self.bankSlipDo(url, bankSlipDetailId, des, callBack);
				});
			} else {
				Rental.modal.close();
				self.bankSlipDo(url, bankSlipDetailId, des, callBack);
			}
		},
		sureDesignateConfirm:function(commitData,callBack,status, isLocal) {
			var self = this;
			if(status == 2 && isLocal == 1) {
				bootbox.confirm('当前流水为已认领状态，确认归属将使改流水数据清零，是否仍要确认？', function(result) {
					Rental.modal.close();
					result && self.singleDesignateDo(commitData, '确认归属', callBack);
				});
			} else {
				Rental.modal.close();
				self.singleDesignateDo(commitData, '确认归属', callBack);
			}
		},
        singleDesignateDo:function(commitData, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'bankSlip/localizationBankSlipDetail'), commitData ,function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
		},
		filterAcitonButtons:function(buttons, jurnalData) {
			var rowActionButtons = new Array(),
				isLocal = jurnalData.isLocalization == 1,
				isHighseas = jurnalData.isLocalization == 2,
				sured = jurnalData.detailStatus == Enum.detailStatus.num.confirmed,
                confirmed = jurnalData.detailStatus == Enum.detailStatus.num.confirmed || jurnalData.detailStatus == Enum.detailStatus.num.hide,
				unclaimed = jurnalData.detailStatus != Enum.detailStatus.num.unclaimed,
				claimed = jurnalData.detailStatus == Enum.detailStatus.num.claimed,
                show = jurnalData.detailStatus != Enum.detailStatus.num.hide,
                hide = (jurnalData.detailStatus == Enum.detailStatus.num.hide || jurnalData.detailStatus == Enum.detailStatus.num.confirmed ||
                        jurnalData.detailStatus == Enum.detailStatus.num.claimed);
		
			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
				if(button.class == 'claimButton' && confirmed) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'verifyDesignate') && sured) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(button.class == 'ignoreButton' && unclaimed) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'showButton') && show) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'hideButton') && hide) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'cancelDesignate') && (!isLocal || sured)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'confirmButton') && !claimed) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'pushdown') && isHighseas) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
        }
	}
	window.JurnalHandleMixin = JurnalHandleMixin;

})(jQuery);