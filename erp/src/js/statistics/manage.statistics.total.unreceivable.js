;(function($) {

	var StatisticsTotalUnreceivable = {
		state:{
			dataList:[],
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statistics];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_statistics_total_unreceiceable_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#timePicker'), $('#timePickerInput'),  $("#startTime"), $("#endTime"));

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#rentLengthType'),
				data:Enum.array(Enum.rentLengthType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（业务类型）'
			});

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$('#subCompanyId'),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId.toString(), item.subCompanyName.toString());
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'全部（公司）'
					});
				}
			});

			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			Rental.ajax.ajaxData('statistics/queryStatisticsUnReceivable', searchData, '加载未收汇总列表', function(response) {
				self.render(response.resultMap.data.statisticsUnReceivableDetailPage);	
				self.renderTotalInfo(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				data = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
					})
				}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderTotalInfo:function(data) {
			$("#totalCount").html('总条数：' + data.totalCount).addClass('text-danger');
			$("#totalLastMonthRent").html("￥" + (data.hasOwnProperty('totalLastMonthRent') ? Rental.helper.fmoney(data.totalLastMonthRent) : '0.00')).addClass('text-danger');
			$("#totalUnReceivableLong").html("￥" + (data.hasOwnProperty('totalUnReceivableLong') ? Rental.helper.fmoney(data.totalUnReceivableLong) : '0.00')).addClass('text-danger');
			$("#totalUnReceivableShort").html("￥" + (data.hasOwnProperty('totalUnReceivableShort') ? Rental.helper.fmoney(data.totalUnReceivableShort) : '0.00')).addClass('text-danger');
			$("#totalUnReceivable").html("￥" + (data.hasOwnProperty('totalUnReceivable') ? Rental.helper.fmoney(data.totalUnReceivable) : '0.00')).addClass('text-danger');
			$("#totalUnReceivablePercentage").html(data.totalUnReceivablePercentage+"%").addClass('text-danger');
			$("#totalCustomerCount").html(data.totalCustomerCount).addClass('text-danger');
			$("#totalUnReceivableCustomerCountShort").html(data.totalUnReceivableCustomerCountShort).addClass('text-danger');
			$("#totalUnReceivableCustomerCountLong").html(data.totalUnReceivableCustomerCountLong).addClass('text-danger');
			$("#totalRentedCustomerCountShort").html(data.totalRentedCustomerCountShort).addClass('text-danger');
			$("#totalRentingCustomerCountLong").html(data.totalRentingCustomerCountLong).addClass('text-danger');
		}
	};

	window.StatisticsTotalUnreceivable = StatisticsTotalUnreceivable;

})(jQuery);