/**
 * decription: 统计管理-业务员提成统计
 * author:张胜
 * time: 2018-4-28
 */
;(function($) {

	var SalesmanDeductManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statistics];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_statistics_salesman_deduct_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

            self.render(new Object());
            
            Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
            
            Rental.ui.events.initMonthpicker($("#startTime"));
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.renderCommonActionButton();

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$('#subCompanyId'),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId.toString(), item.subCompanyName.toString());
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'全部（公司）'
					});
				}
            });
            
            Rental.ui.renderSelect({
				container: $('#orderBy'),
				data:Enum.array(Enum.orderBy),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '排序关键字'
			})
			
			Rental.ui.renderSelect({
				container: $('#rentLengthType'),
				data:Enum.array(Enum.rentLengthType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '租赁类型'
            })
            
            Rental.ui.renderSelect({
				container: $('#orderType'),
				data:Enum.array(Enum.deductOrderType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '排序规则'
			})

			self.searchData(self.getCurTime());  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var formData = Rental.form.getFormData(self.$searchForm);
			var curTime = self.getCurTime();
			if(!formData.startTime) {
				formData.startTime = curTime.startTime;
			}

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, formData || {}, prams || {});

			Rental.ajax.ajaxData('statistics/querySalesman', searchData, '加载业务员提成列表', function(response) {
				self.render(response.resultMap.data.statisticsSalesmanDetailPage);
				self.renderTotalInfo(response.resultMap.data);
			});
		},
		getCurTime:function() {
			var curTime = new Date();
			var year = curTime.getFullYear();
			var curMonth = curTime.getMonth();
			var initSearchTime = new Date(year,curMonth,1).getTime();
			return {startTime:initSearchTime}
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(deductData) {
			var self = this,
                listData = deductData.hasOwnProperty("itemList") ? deductData.itemList : [],
                hasTotalData = deductData.hasOwnProperty("itemList") && deductData.itemList.length > 0;

            var data = {
					dataSource:_.extend(Rental.render, {
                        hasTotalData: hasTotalData,
                        "deductData":deductData,
						"listData":listData,
						rentLengthTypeValue:function() {
							return Enum.rentLengthType.getValue(this.rentLengthType);
						}
					})
				}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderTotalInfo:function(data) {
			data.hasOwnProperty("totalCount") && (data.totalCount > 0) && $("#totalInfo").removeClass("hide");
			$("#totalCount").html(data.hasOwnProperty("totalCount") ? ("总条数：" + data.totalCount) : "")
			$("#totalDealsCount").html(data.hasOwnProperty("totalDealsCount") ? data.totalDealsCount : "")
			$("#totalDealsProductCount").html(data.hasOwnProperty("totalDealsProductCount") ? data.totalDealsProductCount : "")
			$("#totalDealsAmount").html(data.hasOwnProperty("totalDealsAmount") ? ("￥" + data.totalDealsAmount.toFixed(2)) : "")
			$("#totalReceive").html(data.hasOwnProperty("totalReceive") ? ("￥" + data.totalReceive.toFixed(2)) : "")
			$("#totalIncome").html(data.hasOwnProperty("totalIncome") ? ("￥" + data.totalIncome.toFixed(2)) : "")
		}
	};

	window.SalesmanDeductManage = SalesmanDeductManage;

})(jQuery);