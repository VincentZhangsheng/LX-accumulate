;(function($) {

	var StatisticsDetailUnReceivable = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statistics];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_statistics_detail_unreceiceable_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			// Rental.ui.events.initRangeDatePicker($('#timePicker'), $('#timePickerInput'),  $("#startTime"), $("#endTime"));

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#rentLengthType'),
				data:Enum.array(Enum.rentLengthType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（业务类型）'
			});

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$('#subCompanyId'),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId.toString(), item.subCompanyName.toString());
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'全部（公司）'
					});
				}
			});

			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			Rental.ajax.ajaxData('statistics/queryUnReceivable', searchData, '未收明细', function(response) {
				console.log('searchData', response.resultMap.data)
				self.render(response.resultMap.data.unReceivableDetailPage);	
				self.renderTotalInfo(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				data = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						customerDetailUrl:function() {
							return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
						},
						orderDetailUrl:function() {
							return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
						},
						rentLengthTypeValue:function() {
							return Enum.rentLengthType.getValue(this.rentLengthType)
						},
						orderItemTypeValue:function() {
							switch(this.orderItemType) {
								case 1:
									return "商品";
								case 2:
									return "配件";
								default:
									return '';
							}
						}
					})
				}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderTotalInfo:function(data) {
			console.log(data)
			$("#totalCount").html('总条数：' + data.totalCount).addClass('text-danger');
			$("#totalUnReceivable").html("￥" + (data.hasOwnProperty('totalUnReceivable') ? Rental.helper.fmoney(data.totalUnReceivable) : '0.00')).addClass('text-danger');
		}
	};

	window.StatisticsDetailUnReceivable = StatisticsDetailUnReceivable;

})(jQuery);