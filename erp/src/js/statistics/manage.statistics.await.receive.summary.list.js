/**
 * decription: 统计管理-待收汇总
 * author:liaojianming
 * time: 2018-4-14
 */
;(function($) {

	var StatisticsAwaitReciveSummaryManage = {
		state:{
			dataList:[],
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statistics];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_statistics_total_awaitreceivesummary_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.awaitReceiveListSummary);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#rentLengthType'),
				data:Enum.array(Enum.rentLengthType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（业务类型）'
			});

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$('#subCompanyId'),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId.toString(), item.subCompanyName.toString());
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'全部（公司）'
					});
				}
			});

			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.awaitReceiveListSummary, searchData);

			Rental.ajax.ajaxData('statistics/queryStatisticsAwaitReceivable', searchData, '加载待收入汇总列表', function(response) {
				self.render(response.resultMap.data.statisticsAwaitReceivableDetailPage);
				self.renderTotalInfo(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				data = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						rentLengthTypeValue:function() {
							return Enum.rentLengthType.getValue(this.rentLengthType)
						}
					})
				}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderTotalInfo: function(data) {
			$("#totalCount").html('总条数：' + data.totalCount).addClass('text-danger');
			$("#totalLastMonthRent").html((data.hasOwnProperty('totalLastMonthRent') ? "￥" + data.totalLastMonthRent : '0.00')).addClass('text-danger');
			$("#totalAwaitReceivableLong").html((data.hasOwnProperty('totalAwaitReceivableLong') ? "￥" + data.totalAwaitReceivableLong : '0.00')).addClass('text-danger');
			$("#totalAwaitReceivableShort").html((data.hasOwnProperty('totalAwaitReceivableShort') ? "￥" + data.totalAwaitReceivableShort : '0.00')).addClass('text-danger');
			$("#totalAwaitReceivable").html((data.hasOwnProperty('totalAwaitReceivable') ?"￥" + data.totalAwaitReceivable : '0.00')).addClass('text-danger');
			$("#totalAwaitReceivablePercentage").html(data.totalAwaitReceivablePercentage+"%").addClass('text-danger');
			$("#totalCustomerCount").html(data.totalCustomerCount).addClass('text-danger');
			$("#totalAwaitReceivableCustomerCountShort").html(data.totalAwaitReceivableCustomerCountShort).addClass('text-danger');
			$("#totalAwaitReceivableCustomerCountLong").html(data.totalAwaitReceivableCustomerCountLong).addClass('text-danger');
			$("#totalRentedCustomerCountShort").html(data.totalRentedCustomerCountShort).addClass('text-danger');
			$("#totalRentingCustomerCountLong").html(data.totalRentingCustomerCountLong).addClass('text-danger');
		}
	};

	window.StatisticsAwaitReciveSummaryManage = StatisticsAwaitReciveSummaryManage;

})(jQuery);