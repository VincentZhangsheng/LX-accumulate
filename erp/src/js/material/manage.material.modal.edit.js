;(function($){

	var EditMaterialModal = {
		init:function(prams,callBack) {
			this.prams = prams;
			this.calllBack = callBack;
			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "material-modal-manage/edit", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#editMaterialModalForm");
			this.$cancelDialogButton = $('.cancelDialogButton', this.$form);
		},
		initEvent:function() {
			var self = this;

			self.initMaterialTypeSelect();

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.$cancelDialogButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initMaterialTypeSelect:function() {
			var self = this;
			ApiData.queryMaterialType({
				searchData:{
					isCapacityMaterial:0,
				},
				success:function(res) {
					Rental.ui.renderSelect({
						data: res, //Enum.array(Enum.materialType),
						container:$('[name=materialType]', self.$modal),
						func:function(opt, item) {
							return opt.format(item.materialTypeId, item.materialTypeName);
						},
						// change:callBack || function() {}
					});
				}
			});
		},
		loadData:function() {
			var self = this;
			if(!self.prams) {
				bootbox.alert('没找到配件型号');
				return;
			}
			Rental.ajax.submit("{0}material/queryModelById".format(SitePath.service), {materialModelId:self.prams.materialModelId}, function(response) {
				if(response.success) {
					self.data = response.resultMap.data;
					self.initFormData(response.resultMap.data);
				} else {
					Rental.notification.error("加载配件型号详情",response.description || "失败");
				}
			}, null ,"加载配件型号详情", 'listLoading');
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			$.each(array,function(i,item){
				if(data.hasOwnProperty(item.name)) {
					var dom = self.$form.find("[name="+item.name+"]");
	            	dom.val(data[item.name]);
	            	dom.data('defaultvalue',data[item.name]);
				}
	        });
	        $('#materialType').prop('disabled', true);
		},
		edit:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);
			formData.materialModelId = self.prams.materialModelId;

			if(!formData.materialModelId) {
				bootbox.alert("没有找到需要的配件型号ID");
				return false;
			}

			Rental.ajax.submit("{0}material/updateModel".format(SitePath.service),formData,function(response){
				if(response.success) {
					Rental.notification.success("编辑配件型号",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("编辑配件型号",response.description || '失败');
				}
			}, null ,"编辑配件型号");
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
				self.$form.find('.state-success').removeClass('state-success');
				self.$form[0].reset();
			}
			return false;
		}
	};

	window.EditMaterialModal = EditMaterialModal;

})(jQuery);