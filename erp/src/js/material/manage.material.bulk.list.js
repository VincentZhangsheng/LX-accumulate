//配件管理

;(function($) {

	var BulkMaterialManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_maetrial];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_bulk_material_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();
			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$createStartTime = $("#createStartTime");
			this.$createEndTime = $("#createEndTime");
			this.$createTimePicker = $('#createTimePicker');
			this.$createTimePickerInput = $('#createTimePickerInput');
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.renderCommonActionButton(); //渲染操作按钮及事件

			// Rental.ui.renderEnumSelect($('#bulkMaterialType'), Enum.array(Enum.materialType), '全部（配件类型）', function() {
			// 	self.searchData();
			// });
			// Rental.ui.renderEnumSelect($('#bulkMaterialStatus'), Enum.array(Enum.bulkMaterialStatus), '全部（状态）', function() {
			// 	self.searchData();
			// });

			Rental.ui.renderSelect({
				data: Rental.localstorage.materialType.get(),
				container:$('#bulkMaterialType'),
				func:function(opt, item) {
					return opt.format(item.materialTypeId, item.materialTypeName);
				},
				change:function() {
					self.searchData();
				},
				defaultText:'全部（配件类型）'
			});

			Rental.ui.renderSelect({
				data: Enum.array(Enum.bulkMaterialStatus),
				container:$('#bulkMaterialStatus'),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function() {
					self.searchData();
				},
				defaultText:'全部（状态）'
			});

			ApiData.warehouse({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$('#currentWarehouseId'),
						data:response,
						func:function(opt, item) {
							return opt.format(item.warehouseId, item.warehouseName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'当前所在仓库'
					});
				}
			});

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.render(new Object());
			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			Rental.ui.events.imgGallery(this.$dataListTable);
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}material/queryAllBulkMaterial".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询散料列表",response.description || '失败');
				}
			}, null ,"查询散料列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					bulkMaterialTypeValue:function() {
						return Enum.materialType.getValue(this.bulkMaterialType);
					},
					// bulkMaterialPriceFormat:function() {
					// 	return this.bulkMaterialPrice ?  this.bulkMaterialPrice.toFixed(2) : '0.00';
					// },
					bulkMaterialStatusValue:function() {
						return Enum.bulkMaterialStatus.getValue(this.bulkMaterialStatus);
					},
					bulkMaterialStatusClass:function() {
						return Enum.bulkMaterialStatus.getClass(this.bulkMaterialStatus);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.BulkMaterialManage = BulkMaterialManage;

})(jQuery);