;(function($){

	var AddMaterialModal = {
		init:function(callBack) {
			this.calllBack = callBack;
			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "material-modal-manage/add", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$form = $("#addMaterialModalForm");
			this.$cancelDialogButton = $('.cancelDialogButton', this.$form);
		},
		initEvent:function() {
			var self = this;

			self.initMaterialTypeSelect();

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});

			self.$cancelDialogButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initMaterialTypeSelect:function() {
			var self = this;
			ApiData.queryMaterialType({
				searchData:{
					isCapacityMaterial:0,
				},
				success:function(res) {
					console.log(res);
					Rental.ui.renderSelect({
						data: res, //Enum.array(Enum.materialType),
						container:$('[name=materialType]', self.$modal),
						func:function(opt, item) {
							return opt.format(item.materialTypeId, item.materialTypeName);
						},
						// change:callBack || function() {}
					});
				}
			});
		},
		add:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);

			Rental.ajax.submit("{0}material/addModel".format(SitePath.service),formData,function(response){
				if(response.success) {
					Rental.notification.success("添加配件型号",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("添加配件型号",response.description || '失败');
				}
			}, null ,"添加配件型号",'dialogLoading');
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
				self.$form.find('.state-success').removeClass('state-success');
				self.$form[0].reset();
			}
			return false;
		}
	};

	window.AddMaterialModal = AddMaterialModal;

})(jQuery);