;(function($){
	
	var MaterialAdd = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_maetrial];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_maetrial_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_maetrial_list_add];
		},
		initDom:function() {
			this.$form = $("#addMaterialForm");
			this.$dropzoneMaterialImg = $("#dropzoneMaterialImg");
			this.$categoryId = $("#categoryId");
			this.$materialPropertiesContainer = $('#materialPropertiesContainer');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_maetrial_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.createMaterial();
			});

			self.initCommonEvent();

			self.renderMaterialType(function(val) {
				self.changeMaterialType(val);
			});
		},
		changeMaterialType:function(val) {
			var mapMaterialType = Rental.localstorage.materialType.getMap();
				materialType = mapMaterialType[val];
			var isNoMaterialModel = materialType.isCapacityMaterial == 1; //含字面量字段
			$('[name=materialModelName]', this.$form).prop({disabled:isNoMaterialModel});
			$('.chooseMaterialModalButton', this.$form).prop({disabled:isNoMaterialModel});
			$('[name=materialCapacityValue]', this.$form).prop({disabled:!isNoMaterialModel});
		},
		createMaterial:function() {
			try {

				var self = this;

				var materialImgList = self.getImageList(self.$dropzoneMaterialImg);

				var formData = Rental.form.getFormData(self.$form);

				if((formData['materialType'] != Enum.materialType.num.ram 
					&& formData['materialType'] != Enum.materialType.num.ssd 
					&& formData['materialType'] != Enum.materialType.num.hdd) && !formData['materialModelId']) {
					bootbox.alert('必须选择配件型号');
					return;
				}

				if((formData['materialType'] == Enum.materialType.ram 
					|| formData['materialType'] == Enum.materialType.ssd 
					|| formData['materialType'] == Enum.materialType.hdd) && !formData['materialCapacityValue']) {
					bootbox.alert('必须填写配件值大小');
					return;
				}

				var materialCapacityValue = formData['materialCapacityValue'];
				if(materialCapacityValue) {
					materialCapacityValue = parseInt(materialCapacityValue)*1024;
				}

				var commitData = {
					materialType:formData['materialType'],
					materialModelId:formData['materialModelId'],
					materialName:formData['materialName'],
					materialCapacityValue:materialCapacityValue,
					isRent:formData['isRent'],
					// timeRentPrice:formData['timeRentPrice'],
					dayRentPrice:formData['dayRentPrice'],
					monthRentPrice:formData['monthRentPrice'],
					materialPrice:formData['materialPrice'],
					newDayRentPrice:formData['newDayRentPrice'],
					newMonthRentPrice:formData['newMonthRentPrice'],
					newMaterialPrice:formData['newMaterialPrice'],
					materialDesc:formData['materialDesc'],
					brandId:formData['brandId'],
					materialModel:formData['materialModel'],
					isReturnAnyTime:formData['isReturnAnyTime'],
					k3MaterialNo:formData['k3MaterialNo'],
					materialImgList:materialImgList,
				}

				Rental.ajax.submit("{0}material/add".format(SitePath.service),commitData,function(response){

					if(response.success) {
						Rental.notification.success("添加配件",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("添加配件",response.description || '失败');
					}
					
				}, null ,"添加配件", 'dialogLoading');

			} catch(e) {
				Rental.notification.error("添加配件失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加配件",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.MaterialAdd = _.extend(MaterialAdd, MaterialMixin);

})(jQuery);