/**
* 商品公用类
*/
;(function() {

	MaterialMixin = {
		state:{
			productProperties:new Array(),
			skuProperties:new Array(),
			productSkuList:new Array(),
		},
		initCommonEvent:function(prams) {
			var self = this;

			prams = _.extend({
				initDropzone:true,
			}, prams || {});

			self.loadBrand();

	        prams.initDropzone && Rental.ui.events.initDropzone($('.dropzoneImg'), 'material/uploadImage');

	        self.$form.on('click', '.chooseMaterialModalButton', function(event) {
				event.preventDefault();
				var materialType = $('[name=materialType]', self.$form).val();
				if(!materialType) {
					bootbox.alert('请先选择配件类型');
					return;
				}
				MaterialModalChoose.init({
					materialType:materialType,
				}, function(rowData) {
					$("[name=materialModelName]", self.$form).val(rowData.modelName);
					$("[name=materialModelId]", self.$form).val(rowData.materialModelId);
				});
			});


		},
		renderMaterialType:function(callBack) {
			Rental.ui.renderSelect({
				data: Rental.localstorage.materialType.get(),
				container:$('#materialType'),
				func:function(opt, item) {
					return opt.format(item.materialTypeId, item.materialTypeName);
				},
				change:callBack || function() {}
			});
		},
		loadBrand:function(changeFunc) {
			ApiData.brand({
				success:function(response) {
					Rental.ui.renderSelect({
						data:response,
						container:$('[name=brandId]'),
						func:function(opt, item) {
							return opt.format(item.brandId, item.brandName);
						},
						change:changeFunc || function() {},
					});
				}
			});
		},
		getImageList:function(container) {
			var imgArray = new Array();
			$('.dz-image-preview', container).each(function(index, el) {
				var img = $(el).data('img');
				img && imgArray.push({materialImgId:img.materialImgId});	
			});
			return imgArray;
		}
	}

	window.MaterialMixin = MaterialMixin;

})(jQuery);