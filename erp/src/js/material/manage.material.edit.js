;(function($){
	
	var MaterialEdit = {
		state:{
			material:null,
			materialNo:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_maetrial];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_maetrial_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_maetrial_list_edit];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			this.state.materialNo = Rental.helper.getUrlPara('no');
			this.state.notEditArray = ['materialModelName', 'materialType', 'materialCapacityValue', 'materialModel', 'brandId'];

			this.$form = $("#editMaterialForm");
			this.$dropzoneMaterialImg = $("#dropzoneMaterialImg");
			this.$categoryId = $("#categoryId");
			this.$materialPropertiesContainer = $('#materialPropertiesContainer');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_maetrial_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form,function() {
				self.editMaterial();
			});

			self.initCommonEvent();

			self.renderMaterialType(function(val) {
				// self.changeMaterialType(val);
			})
		},
		loadData:function() {
			var self = this;
			if(!self.state.materialNo) {
				bootbox.alert('没找到配件编号');
				return;
			}
			Rental.ajax.submit("{0}material/queryMaterialByNo".format(SitePath.service), {materialNo:self.state.materialNo}, function(response) {
				if(response.success) {
					// self.data = response.resultMap.data;
					self.state.material  = response.resultMap.data;
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载配件详情",response.description || "失败");
				}
			}, null ,"加载配件详情", 'listLoading');
		},
		initData:function(data) {
			this.initFormData(data);
			this.initMaterialImg(data);
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			$.each(array,function(i,item){
				var dom = self.$form.find("[name="+item.name+"]"),
					val = "";
				if(data.hasOwnProperty(item.name)) {
					val = data[item.name];
					if(item.name == 'materialCapacityValue') {
						val = parseInt(val)/1024;
					}
				}
				dom.val(val);
            	dom.data('defaultvalue', val);
	        });

	        self.state.notEditArray.forEach(function(item) {
	        	$('[name='+item+']', self.$form).prop({disabled:true});
	        })
		},
		initMaterialImg:function(data) {
			if(!data.hasOwnProperty('materialImgList')) return ;
			this.initImg(data.materialImgList,this.$dropzoneMaterialImg);
		},
		initImg:function(imgs,container) {
			container.addClass('dz-started dz-demo');
			var examplePreviewTpl = $("#examplePreviewTpl").html();
			Mustache.parse(examplePreviewTpl);

			$.each(imgs,function(index, item) {
				setTimeout(function() {
					var renderData = {
						imgs:item,
						dataImg:function() {
							return JSON.stringify(this);
						}
					}
					var imgBox = Mustache.render(examplePreviewTpl,renderData);
					var $imgBox = $(imgBox);

					container.append($imgBox);
					$imgBox.addClass('animated fadeIn').removeClass('hidden');
					$imgBox.on('click','a.dz-remove',function(event) {
						event.preventDefault();		
						$(this).parent('.example-preview').remove();
					});

					Rental.ui.events.initViewer()
				},500);
			});
		},
		editMaterial:function() {
			try {

				var self = this;

				var materialImgList = self.getImageList(self.$dropzoneMaterialImg);

				var formData = Rental.form.getFormData(self.$form);

				// if((formData['materialType'] != Enum.materialType.num.ram 
				// 	&& formData['materialType'] != Enum.materialType.num.ssd 
				// 	&& formData['materialType'] != Enum.materialType.num.hdd) && !formData['materialModelId']) {
				// 	bootbox.alert('必须选择配件型号');
				// 	return;
				// }

				// if((formData['materialType'] == Enum.materialType.ram 
				// 	|| formData['materialType'] == Enum.materialType.ssd 
				// 	|| formData['materialType'] == Enum.materialType.hdd) && !formData['materialCapacityValue']) {
				// 	bootbox.alert('必须填写配件值大小');
				// 	return;
				// }

				var commitData = {
					materialNo:self.state.materialNo,
					materialType:formData['materialType'],
					materialModelId:formData['materialModelId'],
					materialName:formData['materialName'],
					materialCapacityValue: formData['materialCapacityValue'],
					isRent:formData['isRent'],
					dayRentPrice:formData['dayRentPrice'],
					monthRentPrice:formData['monthRentPrice'],
					materialPrice:formData['materialPrice'],
					newDayRentPrice:formData['newDayRentPrice'],
					newMonthRentPrice:formData['newMonthRentPrice'],
					newMaterialPrice:formData['newMaterialPrice'],
					materialDesc:formData['materialDesc'],
					brandId: formData['brandId'],
					materialModel:formData['materialModel'],
					isReturnAnyTime:formData['isReturnAnyTime'],
					k3MaterialNo:formData['k3MaterialNo'],
					materialImgList:materialImgList,
				}

				self.state.notEditArray.forEach(function(item) {
					commitData[item] = self.state.material[item];
				})

				Rental.ajax.submit("{0}material/update".format(SitePath.service),commitData,function(response){

					if(response.success) {
						Rental.notification.success("编辑配件",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑配件",response.description || '失败');
					}
					
				}, null ,"编辑配件");

			} catch(e) {
				Rental.notification.error("编辑配件失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑配件",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form.find('.state-success').removeClass('state-success');
				        }
				    }
				});
			}
		}
	};

	window.MaterialEdit = _.extend(MaterialEdit, MaterialMixin);

})(jQuery);