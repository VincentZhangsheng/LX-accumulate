//配件管理

;(function($) {

	var MaterialManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_maetrial];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_maetrial_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_maetrial_list_add];
			var edit = this.currentPageAuthor.children[AuthorCode.manage_maetrial_list_edit];
			var view = this.currentPageAuthor.children[AuthorCode.manage_maetrial_list_detail];
			var del = this.currentPageAuthor.children[AuthorCode.manage_maetrial_list_delete];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			// del && this.commonActionButtons.push(AuthorUtil.button(del,'delButton','fa fa-trash-o','删除'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'delButton','','删除'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$createStartTime = $("#createStartTime");
			this.$createEndTime = $("#createEndTime");
			this.$createTimePicker = $('#createTimePicker');
			this.$createTimePickerInput = $('#createTimePickerInput');
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.materialList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.renderCommonActionButton(); //渲染操作按钮及事件

			Rental.ui.renderSelect({
				data: Rental.localstorage.materialType.get(),
				container:$('#materialType'),
				func:function(opt, item) {
					return opt.format(item.materialTypeId, item.materialTypeName);
				},
				change:function() {
					self.searchData();
				},
				defaultText:'全部（配件类型）'
			});

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.render(new Object());
			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$actionCommonButtons.on("click", '.delButton', function(event) {
				event.preventDefault();
				bootbox.alert('开发中...');
			});

			self.$dataListTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.del(materialNo);
			});

			Rental.ui.events.imgGallery(this.$dataListTable);
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.materialList, searchData);
			
			Rental.ajax.submit("{0}material/queryAllMaterial".format(SitePath.service),searchData,function(response){

				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询配件列表",response.description || '失败');
				}
				
			}, null ,"查询配件列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.materialDetail, this.materialNo);
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.materialImgList, this.materialName);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '上架':"下架";
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		del:function(prams) {
			var self = this;
			if(!prams) {
				bootbox.alert('没找到配件编号');
				return;
			}
			function d() {
				Rental.ajax.submit("{0}material/delete".format(SitePath.service),{materialNo:prams},function(response){
					if(response.success) {
						Rental.notification.success('删除配件',response.description || '成功');
						self.doSearch(self.Pager.pagerData.currentPage);
					} else {
						Rental.notification.error("删除配件",response.description || '失败');
					}
				}, null ,"查删除配件");
			}
			bootbox.confirm('确认删除？',function(result) {
				result && d();
			});
		}
	};

	window.MaterialManage = MaterialManage;

})(jQuery);