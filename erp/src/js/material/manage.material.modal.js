//配件管理

;(function($) {

	var MaterialModalManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_maetrial];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_material_modal];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_material_modal_add];
			var edit = this.currentPageAuthor.children[AuthorCode.manage_material_modal_edit];
			var del = this.currentPageAuthor.children[AuthorCode.manage_material_modal_delete];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'deleteButton','','删除'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.renderCommonActionButton(); //渲染操作按钮及事件

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.render(new Object());
			self.searchData();  //初始化列表数据

			Rental.ui.renderSelect({
				data: Rental.localstorage.materialType.get(),
				container:$('#materialType'),
				func:function(opt, item) {
					return opt.format(item.materialTypeId, item.materialTypeName);
				},
				change:function() {
					self.searchData();
				},
				defaultText:'全部（配件类型）'
			});

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$actionCommonButtons.on("click", '.addButton', function(event) {
				event.preventDefault();
				AddMaterialModal.init(function() {
					self.doSearch();
				});
			});

			self.$dataListTable.on('click', '.editButton', function(event) {
				event.preventDefault();
				var materialmodelid = $(this).data('materialmodelid');
				var modelname = $(this).data('modelname');
				EditMaterialModal.init({
					materialModelId:materialmodelid,
					modelName:modelname
				}, function() {
					self.doSearch(self.Pager.pagerData.currentPage);
				})
			});

			self.$dataListTable.on('click', '.deleteButton', function(event) {
				event.preventDefault();
				var materialmodelid = $(this).data('materialmodelid');
				self.del(materialmodelid);
			});

			Rental.ui.events.imgGallery(this.$dataListTable);
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}material/queryModel".format(SitePath.service),searchData,function(response){

				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询配件列表",response.description || '失败');
				}
				
			}, null ,"查询配件列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		del:function(prams) {
			var self = this;
			if(!prams) {
				bootbox.alert('没找到配件型号ID');
				return;
			}
			function d() {
				Rental.ajax.submit("{0}material/deleteModel".format(SitePath.service),{materialModelId:prams},function(response){
					if(response.success) {
						self.doSearch(self.Pager.pagerData.currentPage);
					} else {
						Rental.notification.error("删除配件型号",response.description || '失败');
					}
				}, null ,"查删除配件型号");
			}
			bootbox.confirm('确认删除？',function(result) {
				result && d();
			});
		}
	};

	window.MaterialModalManage = MaterialModalManage;

})(jQuery);