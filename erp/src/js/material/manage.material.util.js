;(function($) {

	var MaterialUtil = {
		renderMaterialTypeSelect:function(changeFunc) {
			Rental.ui.renderSelect({
				data:Enum.array(Enum.materialType),
				container:$('#materialType'),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:changeFunc || function() {}
			});
		},
		initDropzone:function() {
			var self = this;
 			$('.dropzoneImg').dropzone({
		        url: SitePath.base + "/material/uploadImage",
		        maxFiles: 20,
		        maxFilesize:2,//MB
		        acceptedFiles: ".jpg,.ipeg,.png",
		        addRemoveLinks: true,
		        dictCancelUpload:"取消上传",
		        dictRemoveFile:'删除',
		        dictFileTooBig:'文件过大（不能大于2M）',
		        dictMaxFilesExceeded:"超过最大上传数量",
		        success:function(e,response) {
		        	self.dropzoneSuccess(e,response)
		        }
		    });
		},
		dropzoneSuccess:function(e,response) {
			var previewElement = $(e.previewElement);
			var progress = $(".dz-progress",previewElement);
			var error = $(".dz-error-mark",previewElement);
			var remove = $('.dz-remove',previewElement);

			progress.css({opacity:'0'});
			remove.html("删除");

			if(response.success){
				previewElement.data("img",response.resultMap.data[0]);
			} else {
				error.css({opacity:'1'});
			}
		},
		loadBrand:function() {
			var self = this,
				commitData = {
					pageNo:1,
					pageSize:10000,
				};
			Rental.ajax.submit("{0}basic/queryAllBrand".format(SitePath.service), commitData, function(response){
				if(response.success) {
					self.renderBand(response.resultMap.data);
				} else {
					Rental.notification.error("加载商品类别失败",response.description);
				}
			}, null ,"加载商品类别");
		},
		renderBand:function(data) {
			if(!(data && data.hasOwnProperty('itemList'))) return;
			Rental.ui.renderSelect({
				data:data.itemList,
				container:$('[name=brandId]'),
				func:function(opt, item) {
					return opt.format(item.brandId, item.brandName);
				}
			});
		}
	};

	window.MaterialUtil = MaterialUtil;

})(jQuery);