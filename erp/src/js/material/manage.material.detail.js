;(function($){
	
	var MaterialDetial = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_maetrial];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_maetrial_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_maetrial_list_detail];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			this.materialNo = Rental.helper.getUrlPara('no');

			this.$form = $("#detailMaterialForm");
			this.$dropzoneMaterialImg = $("#dropzoneMaterialImg");
			this.$categoryId = $("#categoryId");
			this.$materialPropertiesContainer = $('#materialPropertiesContainer');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_maetrial_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			self.initCommonEvent({
				initDropzone:false,
			});

			self.renderMaterialType(function(val) {
				// self.changeMaterialType(val);
			})
		},
		loadData:function() {
			var self = this;
			if(!self.materialNo) {
				bootbox.alert('没找到配件编号');
				return;
			}
			Rental.ajax.submit("{0}material/queryMaterialByNo".format(SitePath.service), {materialNo:self.materialNo}, function(response) {
				if(response.success) {
					self.data = response.resultMap.data;
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载配件详情",response.description || "失败");
				}
			}, null ,"加载配件详情");
		},
		initData:function(data) {
			this.initFormData(data);
			this.initMaterialImg(data);
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			$.each(array,function(i,item){
				var dom = self.$form.find("[name="+item.name+"]"),
					val = "";
				if(data.hasOwnProperty(item.name)) {
					val = data[item.name];
					if(item.name == 'materialCapacityValue') {
						val = parseInt(val)/1024;
					}
				}
				dom.val(val);
            	dom.data('defaultvalue', val);
				dom.prop({disabled:true});
	        });
		},
		initMaterialImg:function(data) {
			if(!data.hasOwnProperty('materialImgList')) return ;
			this.initImg(data.materialImgList,this.$dropzoneMaterialImg);
		},
		initImg:function(imgs,container) {
			container.addClass('dz-started dz-demo');
			var examplePreviewTpl = $("#examplePreviewTpl").html();
			Mustache.parse(examplePreviewTpl);

			$.each(imgs,function(index, item) {
				setTimeout(function() {
					var renderData = {
						imgs:item,
						dataImg:function() {
							return JSON.stringify(this);
						}
					}
					var imgBox = Mustache.render(examplePreviewTpl,renderData);
					var $imgBox = $(imgBox);

					container.append($imgBox);
					$imgBox.addClass('animated fadeIn').removeClass('hidden');
					$imgBox.on('click','a.dz-remove',function(event) {
						event.preventDefault();		
						$(this).parent('.example-preview').remove();
					});

					Rental.ui.events.initViewer()
				},500);
			});
		},
	};

	window.MaterialDetial = _.extend(MaterialDetial, MaterialMixin);

})(jQuery);