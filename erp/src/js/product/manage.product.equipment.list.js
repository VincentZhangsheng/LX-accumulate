;(function($) {

	var EquipmentManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_equipment_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			Rental.ui.renderSelect({
				container:$('#equipmentStatus'),
				data:Enum.array(Enum.equipmentStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（在租状态）'
			});
			
			// Rental.ui.renderEnumSelect($('#equipmentStatus'), Enum.array(Enum.equipmentStatus), '全部（在租状态）', function() {
			// 	self.searchData();
			// });

			Rental.ui.events.imgGallery(this.$dataListTable);
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			self.renderWarehouse();
		},
		renderWarehouse:function() {
			var self = this;
			ApiData.warehouse({
				success:function(response) {
					self.renderWarehouseSelect($('#currentWarehouseId'), '当前所在仓库', response);
					self.renderWarehouseSelect($('#ownerWarehouseId'), '归属仓库', response);
				}
			});
		},
		renderWarehouseSelect:function(container, defaultText, response) {
			var self = this;
			Rental.ui.renderSelect({
				container:container,
				data:response,
				func:function(opt, item) {
					return opt.format(item.warehouseId, item.warehouseName);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:defaultText
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(EquipmentManage.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}/product/queryAllProductEquipment".format(SitePath.service),searchData,function(response){

				if(response.success) {
					EquipmentManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询设备列表",response.description);
				}
				
			}, null ,"查询设备列表",'listLoading');
		},
		doSarch:function(pageNo) {
			EquipmentManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSarch);
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					equipmentStatusValue:function() {
						return Enum.equipmentStatus.getValue(this.equipmentStatus);
					},
					equipmentStatusClass:function() {
						return Enum.equipmentStatus.getClass(this.equipmentStatus);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isNewBool:function() {
						return this.isNew == 1;
					},
					// isNewBadgeClass:function() {
					// 	return this.isNew == 1 ? 'badge-primary':'badge-default';
					// },
					// isNewValue:function() {
					// 	return this.isNew == 1 ? '新':'旧';
					// }
				})
			}
			
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.EquipmentManage = EquipmentManage;

})(jQuery);