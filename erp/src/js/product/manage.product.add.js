;(function($){
	
	var AddProduct = {
		init:function(callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_product_add];
		},
		initDom:function() {
			this.$form = $("#addProductForm");
			this.$dropzoneProductImg = $("#dropzoneProductImg");
			this.$dropzoneProductDesImg = $("#dropzoneProductDesImg");
			this.$categoryId = $("#categoryId");
			this.$skuPropertiesContainer = $("#skuPropertiesContainer");
			this.$productPropertiesContainer = $("#productPropertiesContainer");
			this.$skuTable = $("#skuTable");
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_product_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.createProduct();
			});

	        self.initCommonEvent();
		},
		createProduct:function() {
			try {

				var self = this;

				var productImgList = self.getImageList(self.$dropzoneProductImg);
				// if(productImgList.length == 0) {
				// 	bootbox.alert("请上传商品图片");
				// 	return false;
				// }

				var productDescImgList = self.getImageList(self.$dropzoneProductDesImg);

				// if(productDescImgList.length == 0) {
				// 	bootbox.alert("请上传商品介绍图片");
				// 	return false;
				// }

				var productPropertyList = new Array()
				$(".productPropertyRido", self.productPropertiesContainer).filter(':checked').each(function(i,item) {
					var perporty = $(item).data('property');
					productPropertyList.push({
						propertyId:perporty.propertyId,
						propertyValueId:perporty.categoryPropertyValueId,
						isSku:1,
						remark:'',
					});
				});

				if(productPropertyList.length == 0) {
					bootbox.alert("请选择商品属性");
					return false;
				}

				var productSkuList = self.state.productSkuList;

				if(productSkuList.length == 0) {
					bootbox.alert("请选择商品销售属性生成SKU，并且填写对应的sku价格");
					return false;
				}

				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					productImgList:productImgList,
					productDescImgList:productDescImgList,
					productSkuList:productSkuList,
					productPropertyList:productPropertyList,

					productName:formData['productName'],
					categoryId:formData['categoryId'],
					subtitle:formData['subtitle'],
					unit:formData['unit'],
					listPrice:formData['listPrice'],
					isRent:formData['isRent'],
					productDesc:formData['productDesc'],
					brandId:formData['brandId'],
					productModel:formData['productModel'],
					isReturnAnyTime:formData['isReturnAnyTime'],
					k3ProductNo:formData['k3ProductNo']
				}

				Rental.ajax.submit("{0}/product/add".format(SitePath.service),commitData,function(response){

					if(response.success) {
						Rental.notification.success("成功添加商品",response.description);
						self.callBackFunc(response);
					} else {
						Rental.notification.error("添加商品失败",response.description);
					}
					
				}, null ,"添加商品",'dialogLoading');

			} catch(e) {
				Rental.notification.error("添加商品失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加商品",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.AddProduct = _.extend(AddProduct, ProductMixin);

})(jQuery);