/**
 * description: 组合商品公用方法
 * author: zhangsheng
 * time: 2018/03/13
 */

;(function($) {

	groupedProductMixin = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			groupedProduct:new Object(),
		},
		initCommonEvent:function() {
			var self = this;

			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});;

			self.renderProductList();
			self.renderMaterialList();

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

            //选择商品
			$("#batchAddProduct").click(function(event) {
				GroupProductChoose.modal({
					showIsNewCheckBox:false,
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});
            });
            
            //删除商品
            $("#batchDeleteProduct").click(function(event) {
				self.batchDeleteProduct();
            });
            
            self.$dataListTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				self.deleteProduct($(this));
				self.renderProductList();
            });
            
            self.$dataListTable.on('change', '.productCount', function(event) {
				event.preventDefault();
				var $productRow = $(this).closest('tr');
				self.changeProduct($productRow);
			});

			self.$dataListTable.on('click', '.viewButton', function(event) {
				event.preventDefault();
				var $this =  $(this), productId = $this.data('productid');
				self.viewSku($this,productId);
            });
			
			self.$dataListTable.on('click', '.hideAffixPanel', function(event) {
				event.preventDefault();
				$(this).closest('.productDetial').slideUp('fast');
			});

            //选择配件
			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms:new Object(), 
					showIsNewCheckBox:false,
					callBack:function(material) {
						self.chooseMaterial(material);
					}
				});
			});

            //删除商品
			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDeleteMaterial();
			});

			self.$materialDataListTable.on('click', '.delMaterialButton', function(event) {
				event.preventDefault();
				self.deleteMaterial($(this));
			});

			self.$materialDataListTable.on('change', '.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeMaterial($materialRow);
			});
        },
		renderProductList:function(){
			var self = this;
			$("#orderItemProductCount").html(self.state.chooseProductList.length);
			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":self.state.chooseProductList,
				})
			}
			Mustache.parse(self.$dataListTpl);
			self.$dataListTable.html(Mustache.render(self.$dataListTpl, data));
		},
		renderMaterialList:function(rowActionButtons) {
			OrderManageItemRender.renderMaterialList(this.state.chooseMaterialList, this.$materialDataListTpl, this.$materialDataListTable, rowActionButtons, this.state.order);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = groupedProductUtil.chooseProduct(this.state.chooseProductList, product);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		changeProduct:function($productRow) {
			this.state.chooseProductList = groupedProductUtil.changeProduct(this.state.chooseProductList, $productRow);
			console.log(this.state.chooseProductList);
			this.renderProductList();
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = groupedProductUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			this.renderProductList();	
		},
		deleteProduct:function($deleteButton) {
			this.state.chooseProductList = groupedProductUtil.deleteProduct($deleteButton, this.state.chooseProductList);
			this.renderProductList();
		},
		chooseMaterial:function(material) {
			this.state.chooseMaterialList = groupedProductUtil.chooseMaterial(this.state.chooseMaterialList, material);
            this.renderMaterialList();
		},
		changeMaterial:function($materialRow) {
			this.state.chooseMaterialList = groupedProductUtil.changeMaterial(this.state.chooseMaterialList, $materialRow);
			this.renderMaterialList();
		},
		batchDeleteMaterial:function() {
			this.state.chooseMaterialList = groupedProductUtil.batchDeleteMaterial(this.$materialDataListTable, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		deleteMaterial:function($button) {
			var materialNo = $button.data('materialno'), isNew = $button.data('isnew');
			this.state.chooseMaterialList = groupedProductUtil.deleteMaterial(materialNo, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		//查看商品Sku
		viewSku:function($button,id) {
			var detailTr =  $button.closest('tr').next('tr'), productDetial = $('.productDetial',detailTr);
			if(productDetial.size() > 0) {
				productDetial.slideDown(); 
			} else {
				this.getProductById($('td',detailTr),id);	
			}
		},
		getProductById:function(container,id) {
			var self = this;
			Rental.ajax.ajaxData('product/queryProductById', {productId:id}, '加载商品详细信息', function(response) {
				self.renderRowProduct(container, response.resultMap.data);
			});
		},
		renderRowProduct:function(container,product) {
			var self = this;
			var data  = {
				product:product,
				productJSONString:function() {
					return JSON.stringify(this);
				},
				skuListSource:function() {
					var skuList = this.productSkuList;
					return _.extend(Rental.render, {
						skuList:skuList,
						propertiesToStr:function() {
							var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				}
			}
			var tpl = $('#productDetialTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
	};

	window.groupedProductMixin = groupedProductMixin;

})(jQuery);