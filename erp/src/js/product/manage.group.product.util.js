/**
 * description: 组合商品公用方法
 * author: zhangsheng
 * time: 2018/03/13
 */

;(function($) {

	groupedProductUtil = {
		chooseProduct:function(chooseProductList, product) {	
			if(!chooseProductList) {
				chooseProductList = new Array();
			}
			if(!product.hasOwnProperty("serialNumber")) {
				product.serialNumber = Rental.helper.randomString(8);
			}
			chooseProductList.push(product);
			return chooseProductList;
		},
		getRowData:function($productRow) {
			return {
				jointProductProductId:$productRow.data("jointid"),
				productId:$('.productId', $productRow).val(),
				serialNumber:$productRow.data("serial"),
				productCount:$.trim($('.productCount', $productRow).val())
			};
        },
		changeProduct:function(chooseProductList, $productRow) {
            var product = this.getRowData($productRow);
            var productIndex = _.findIndex(chooseProductList, {serialNumber:product.serialNumber});
            chooseProductList[productIndex] = _.extend(chooseProductList[productIndex], product);
			return chooseProductList;
        },
        batchDelete:function($dataListTable, chooseProductList) {
			var self = this, 
				array = $('[name=checkItem]', $dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $dataListTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择商品');
				return chooseProductList;
			}

			filterChecked.each(function(item) {
				var serialNumber = $(this).data('serial');
				chooseProductList = self.deleteProductFunc(serialNumber, chooseProductList);
			});

			return chooseProductList;
		},
		deleteProduct:function($deleteButton, chooseProductList) {
			var serialNumber = $(this).data('serial');
			return this.deleteProductFunc(serialNumber, chooseProductList);
		},
		deleteProductFunc:function(serialNumber, chooseProductList) {
			var currProductIndex = _.findIndex(chooseProductList, {serialNumber:serialNumber});
			chooseProductList.splice(currProductIndex, 1);
			return chooseProductList;
		},
		chooseMaterial:function(chooseMaterialList, material) {
			try {
				if(chooseMaterialList.length > 0) {
					var index  = _.findIndex(chooseMaterialList, {materialNo:material.materialNo});
					if(index > -1) {
						chooseMaterialList[index] = _.extend(chooseMaterialList, material || {});
					} else {
						chooseMaterialList.push(material)
					}
				} else {
					chooseMaterialList.push(material)
				}
				Rental.notification.success("选择配件", '成功');

				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
		},
		chooseMaterialByIsNew:function(chooseMaterialList, material) {
			try {
				if(chooseMaterialList.length > 0) {
					var index  = _.findIndex(chooseMaterialList, {materialNo:material.materialNo, isNew:material.isNewMaterial});
					if(index > -1) {
						chooseMaterialList[index] = _.extend(chooseMaterialList, material || {});
					} else {
						chooseMaterialList.push(material)
					}
				} else {
					chooseMaterialList.push(material)
				}
				Rental.notification.success("选择配件", '成功');

				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
        },
        getMaterialList:function($materialDataListTable) {
			var self = this;
			var materialList = $('tr.materialRow', $materialDataListTable).map(function() {
				return self.getMaterialByMaterialRow($(this));
			}).toArray();
			return materialList;
		},
		getMaterialByMaterialRow:function($materialRow) {
			return  {
				jointMaterialId:$materialRow.data("jointid"),
				materialNo:$('.materialNo', $materialRow).val(),
				materialCount:$.trim($('.materialCount', $materialRow).val())
			};
		},
		changeMaterial:function(chooseMaterialList, $materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var index = _.findIndex(chooseMaterialList, {materialNo:material.materialNo});
			chooseMaterialList[index] = _.extend(chooseMaterialList[index], material);
			return chooseMaterialList;
		},
		batchDeleteMaterial:function($materialDataListTable, chooseMaterialList) {
			var self = this,
				array = $('[name=checkItem]', $materialDataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $materialDataListTable).filter(':checked');
			if(filterChecked.length == 0) {
				bootbox.alert('请选择配件');
				return chooseMaterialList;
			}

			filterChecked.each(function(index, el) {
				var materialNo = $(this).val(), 
					isNew = $(this).data('isnew');
				chooseMaterialList = self.deleteMaterial(materialNo, chooseMaterialList);
			});

			return chooseMaterialList;
		},
		deleteMaterial:function(materialNo, chooseMaterialList) {
			if(chooseMaterialList.length == 0) return chooseMaterialList;
			var index = _.findIndex(chooseMaterialList, {materialNo:materialNo});
			chooseMaterialList.splice(index, 1);
			return chooseMaterialList;
		},
		initChooseProductList:function(data) {
			if(!data) return;
			
			var	chooseProductList = data.map(function(item) {
				if(!item.product.hasOwnProperty("serialNumber")) {
					item.product.serialNumber = Rental.helper.randomString(8);
				}
				item.product.productCount = item.productCount;
				item.product.jointProductProductId = item.jointProductProductId;
				return item.product;
			})
			return chooseProductList;
		},
		initChooseMaterialList:function(data) {
            if(!data) return;
			var	chooseMaterialList = data.map(function(item) {
                var material = item.material;
                return _.extend(material, item);
            });
			return chooseMaterialList;
		},
	}

	window.groupedProductUtil = groupedProductUtil;

})(jQuery);