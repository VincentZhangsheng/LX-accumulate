;(function($) {
	var GroupProductChoose = {
		init:function(callBack) {
			this.props = {
				productId:null, //商品ID,用户过滤商品信息
				showIsNewCheckBox:false, //是否显示新旧商品，供选择
				isVerifyStock:true, //是否验证库存
				callBack:callBack,
			};
			this.show();
		},
		modal:function(prams) {
			this.props = _.extend({
				productId:null,
				isVerifyStock:true,
				showIsNewCheckBox:false,
				callBack:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "grouped-product/chooseProduct", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$chooseModal = modal.container;//$("#chooseProductModal");
			this.$searchForm = $("#chooseProductSearchForm");
			this.$dataListTpl = $("#chooseProductModalDataListTpl").html();
			this.$dataListTable = $("#chooseProductDataListTable");

			!!this.props.productId && $('[name=productId]', this.$searchForm).val(this.props.productId).addClass('hide');

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '[name=isNewProduct]', function(event) {
				var isChecked = $(this).prop('checked'), $skuTr = $(this).closest('.skuTr');
				if(isChecked) {
					$(".isNew", $skuTr).removeClass('hide');
					$(".isUsed", $skuTr).addClass('hide');
				} else {
					$(".isUsed", $skuTr).removeClass('hide');
					$(".isNew", $skuTr).addClass('hide');
				}
			});

            self.$dataListTable.on('click', '.viewButton', function(event) {
				event.preventDefault();
				var $this =  $(this), productId = $this.data('productid');
				self.viewSku($this,productId);
            });

            self.$dataListTable.on('click', '.hideAffixPanel', function(event) {
				event.preventDefault();
				$(this).closest('.productDetial').slideUp('fast');
			});
            
			self.$dataListTable.on('click', '.chooseButton', function(event) {
                event.preventDefault();
                var rowData = $(this).closest("tr").data("rowdata");
				self.chooseProduct(rowData);
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = _.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				isRent:1, //在租
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('product/queryAllProduct', searchData, '查询商品列表失败', function(response) {
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', self.$chooseModal));
		},
		renderList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    productJSONString:function() {
                        return JSON.stringify(this);
                    },
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '在租':"下架";
					},
					totalStock:function() {
						return _.reduce(this.productSkuList, function(pre, item) {
							return pre + parseInt(item.stock);
						}, 0);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		viewSku:function($button,id) {
			var detailTr =  $button.closest('tr').next('tr'), productDetial = $('.productDetial',detailTr);
			if(productDetial.size() > 0) {
				productDetial.slideDown(); 
			} else {
				this.getProductById($('td',detailTr),id);	
			}
		},
		getProductById:function(container,id) {
			var self = this;
			Rental.ajax.ajaxData('product/queryProductById', {productId:id}, '加载商品详细信息', function(response) {
				self.renderRowProduct(container, response.resultMap.data);
			});
		},
		renderRowProduct:function(container,product) {
			var self = this;
			var data  = {
				product:product,
				productJSONString:function() {
					return JSON.stringify(this);
				},
				skuListSource:function() {
					var skuList = this.productSkuList;
					return _.extend(Rental.render, {
						showIsNewCheckBox:self.props.showIsNewCheckBox,
						skuList:skuList,
						propertiesToStr:function() {
							var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				}
			}
			var tpl = $('#chooseProductModalDetialTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		chooseProduct:function(product) {
            var self = this;
            if(!product) return;
            
            var productObj = {
                productId:product.productId,
                productName:product.productName,
                productNo:product.productNo,
                categoryName:product.categoryName,
                brandName:product.brandName,
                k3ProductNo:product.k3ProductNo,
            }
			self.props.callBack && self.props.callBack(_.extend({}, productObj));
		}
	};

	window.GroupProductChoose = GroupProductChoose;

})(jQuery)