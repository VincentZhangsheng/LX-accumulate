/**
 * description: 添加组合商品
 * author: zhangsheng
 * time: 2018/03/13
 */

;(function($){
	
	var addGroupedProduct = {
		state:{
			id:null,
			chooseProductList:new Array(),
            chooseMaterialList:new Array(),
            groupedProduct:new Object()
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_grouped_product];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_grouped_product_add];
		},
		initDom:function() {
			this.$form = $("#addProductForm");
            
            this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable'); 
		},
		initEvent:function() {
			var self = this;
			
			Layout.chooseSidebarMenu(AuthorCode.manage_grouped_product); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});

			self.initCommonEvent();
		},
		add:function() {
			try {
				var self = this,
					formData = Rental.form.getFormData(self.$form);
				
				var isNew = $('[name=isNew]', self.$form).prop('checked');
				formData['isNew'] = isNew ? 1 : 0;

				var commitData = {
					jointProductName:formData['jointProductName'],
					remark:formData['remark']
				}

				//所选商品、设备
				var jointProductProductList = self.state.chooseProductList.map(function(item) {
					return {
						productId:item.productId,
						productCount:item.productCount
					}
				});
				var jointMaterialList = groupedProductUtil.getMaterialList(self.$materialDataListTable);
				jointMaterialList = jointMaterialList.map(function(item) {
					return {
						materialNo:item.materialNo,
						materialCount:item.materialCount
					}
				})
                
                if(jointProductProductList.length == 0 && jointMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(jointProductProductList.length > 0) {
					commitData.jointProductProductList = jointProductProductList;
				}

				if(jointMaterialList.length > 0) {
					commitData.jointMaterialList = jointMaterialList;
				}

				var des = '添加组合商品';
				Rental.ajax.submit("{0}jointProduct/add".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("添加组合商品", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加组合商品",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		},
	};

	window.addGroupedProduct = _.extend(addGroupedProduct, groupedProductMixin);

})(jQuery);