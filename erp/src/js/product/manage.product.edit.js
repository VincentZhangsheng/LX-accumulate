;(function($){
	
	var EditProduct = {
		state:{
			productId:null,
			productProperties:new Array(),
			skuProperties:new Array(),
			productSkuList:new Array(),
			product:null,
		},
		init:function(productId,callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_product_edit];
		},
		initDom:function() {
			this.$editProductModal = $("#editProductModal");
			this.$form = $("#editProductForm");
			this.$dropzoneProductImg = $("#dropzoneProductImg");
			this.$dropzoneProductDesImg = $("#dropzoneProductDesImg");
			this.$categoryId = $("#categoryId");
			this.$skuPropertiesContainer = $("#skuPropertiesContainer");
			this.$productPropertiesContainer = $("#productPropertiesContainer");
			this.$skuTable = $("#skuTable");
			this.data = [];

			this.state.productId = Rental.helper.getUrlPara('id');
			$("input[name=productId]",this.$form).val(this.productId);
			$("#panelTitle").html('<i class="fa fa-pencil"></i>编辑商品');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_product_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form, function(form) {
				self.editProduct();
			});

			self.loadData();

	        self.initCommonEvent();
		},
		loadData:function() {
			var self = this;
			if(!self.state.productId) {
				bootbox.alert('没有找到商品ID');
				return;
			}
			Rental.ajax.submit("{0}/product/queryProductById".format(SitePath.service),{productId:self.state.productId},function(response){
				if(response.success) {
					self.state.product = response.resultMap.data;
					self.initFormData(self.state.product);
				} else {
					Rental.notification.error("查询商品详细信息失败",response.description);
				}
			}, null ,"查询商品详细信息",'dialogLoading');
		},
		initFormData:function(data){
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			$.each(array,function(i,item){
				if(data.hasOwnProperty(item.name)) {
					var dom = self.$form.find("[name="+item.name+"]");
	            	dom.val(data[item.name]);
	            	dom.data('defaultvalue',data[item.name]);
				}
	        });
	        $('[name=categoryId]', self.$form).prop({disabled:true});
	        $('[name=brandId]', self.$form).prop({disabled:true});
	        $('[name=productModel]', self.$form).prop({disabled:true});


	        self.initProductImg(data);
	        self.initProductDesImg(data);

        	self.state.productSkuList = (data.productSkuList ||[]).map(function(item, index) {
        		item.rowId = Rental.helper.randomString(8) + index;
        		return item;
        	});

	        //根据类别查询类别下所有属性
	       	var categoryId = this.$categoryId.data('defaultvalue');
	       	this.queryPropertiesByCategoryId(categoryId, function() {
	       		 self.renderSKUTable();
	       		 self.checkedProductProperty(data);
	       	});
		},
		initProductImg:function() {
			if(!this.state.product.hasOwnProperty('productImgList')) return ;
			this.initImg(this.state.product.productImgList,this.$dropzoneProductImg);
		},
		initProductDesImg:function() {
			if(!this.state.product.hasOwnProperty('productDescImgList')) return ;
			this.initImg(this.state.product.productDescImgList,this.$dropzoneProductDesImg);
		},
		initImg:function(imgs,container) {
			container.addClass('dz-started dz-demo');
			var examplePreviewTpl = $("#examplePreviewTpl").html();
			Mustache.parse(examplePreviewTpl);

			$.each(imgs,function(index, item) {
				setTimeout(function() {
					var renderData = {
						productImg:item,
						dataImg:function() {
							return JSON.stringify(this);
						}
					}
					var imgBox = Mustache.render(examplePreviewTpl,renderData);
					var $imgBox = $(imgBox);

					container.append($imgBox);
					$imgBox.addClass('animated fadeIn').removeClass('hidden');
					$imgBox.on('click','a.dz-remove',function(event) {
						event.preventDefault();		
						$(this).parent('.example-preview').remove();
					});

					Rental.ui.events.initViewer()
				},500);
			});
		},
		checkedProductProperty:function(data) {
			data.hasOwnProperty('productPropertyList') && data.productPropertyList.forEach(function(item) {
				$(".productPropertyRido", self.productPropertiesContainer).filter('[value='+item.propertyValueId+']').prop({checked:true});
			})
		},
		editProduct:function() {
			
			try { 

				var self = this;

				var productImgList = self.getImageList(self.$dropzoneProductImg);
				// if(productImgList.length == 0) {
				// 	bootbox.alert("请上传商品图片");
				// 	return false;
				// }

				var productDescImgList = self.getImageList(self.$dropzoneProductDesImg);

				// if(productDescImgList.length == 0) {
				// 	bootbox.alert("请上传商品介绍图片");
				// 	return false;
				// }

				var productPropertyList = new Array()
				$(".productPropertyRido", self.productPropertiesContainer).filter(':checked').each(function(i,item) {
					var perporty = $(item).data('property');
					productPropertyList.push({
						propertyId:perporty.propertyId,
						propertyValueId:perporty.categoryPropertyValueId,
						isSku:1,
						//skuId:'',
						remark:'',
					});
				});

				if(productPropertyList.length == 0) {
					bootbox.alert("请选择商品属性");
					return false;
				}

				var productSkuList = self.state.productSkuList;

				if(productSkuList.length == 0) {
					bootbox.alert("请选择商品销售属性生成SKU，并且填写对应的sku价格");
					return false;
				}

				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					productImgList:productImgList,
					productDescImgList:productDescImgList,
					productSkuList:productSkuList,
					productPropertyList:productPropertyList,

					productName:formData['productName'],
					categoryId: self.state.product.categoryId,//formData['categoryId'],
					subtitle:formData['subtitle'],
					unit:formData['unit'],
					// listPrice:formData['listPrice'],
					isRent:formData['isRent'],
					productDesc:formData['productDesc'],
					isReturnAnyTime:formData['isReturnAnyTime'],
					k3ProductNo:formData['k3ProductNo'],
					brandId: self.state.product.brandId, //formData['brandId'],
					productModel:self.state.product.productModel, //formData['productModel'],
					productId:self.state.productId,
				}

				Rental.ajax.submit("{0}/product/update".format(SitePath.service),commitData,function(response){

					if(response.success) {
						Rental.notification.success("编辑商品",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑商品",response.description  || '失败');
					}
					
				}, null ,"编辑商品",'dialogLoading');

			} catch(e) {
				Rental.notification.error("编辑商品失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑商品",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	// self.$form.find('.state-success').removeClass('state-success');
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.EditProduct = _.extend(EditProduct, ProductMixin);

})(jQuery);