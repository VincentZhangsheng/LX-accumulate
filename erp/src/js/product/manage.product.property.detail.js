;(function($) {

	var ProductPropertyDetail = {
        state:{
            id:null,
            chooseMaterial:null,
			propertyValueName:null,
            dataOrder:null,
			property:null,
			categoryPropertyValueId:null
        },
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
            try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
                this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_property_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_product_property_detail];
                this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
            this.rowActionButtons = new Array();
            
			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;
            
			var add = this.currentManageListAuthor.children[AuthorCode.manage_product_property_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_product_property_edit];
			
			add && this.actionButtons.push(AuthorUtil.button(add,'addPropertyVal','fa fa-plus','添加属性值'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','修改'));
		},
		initDom:function() {
            this.$propertyDetailTpl = $("#propertyDetailTpl").html();
			this.$propertyDetail = $("#propertyDetail");
			this.$dataListTpl = $("#dataListTpl").html();
            this.$dataListTable = $("#dataListTable");
			
			this.Pager = new Pager();
            this.state.id = Rental.helper.getUrlPara('id');
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData(); 
			self.renderActionButton();
			
            $("#addPropertyVal").on('click', function(event) {
				event.preventDefault();
				var propertyInfo = self.state.property;
				self.state.propertyValueName = null,
				self.state.dataOrder = null
	        	self.addProperty.init({
					propertyInfo:propertyInfo,
					callBack:function(propertyValueName,dataOrder) {
						self.state.propertyValueName = propertyValueName,
						self.state.dataOrder = dataOrder
					},
					complete:function() {
						self.loadData(); 
					},
					continueChooseMaterialFunc:function() {},
					changeContinueFunc:function() {
						_.isObject(ProductPropertyDetail) &&  _.isFunction(ProductPropertyDetail.chooseMaterialModal) && ProductPropertyDetail.chooseMaterialModal(propertyInfo);
					}
				});
			});

			self.$dataListTable.on("click",".editButton", function(event) {
				event.preventDefault();
				var propertyInfo = self.state.property;
				var rowData = $(this).closest("tr").data("info");
				var materialModelId = rowData.hasOwnProperty("materialModelId") ? rowData.materialModelId : "";
				var modelName = rowData.hasOwnProperty("modelName") ? rowData.modelName : "";
				var propertyCapacityValue = rowData.hasOwnProperty("propertyCapacityValue") ? rowData.propertyCapacityValue : "";

				self.state.propertyValueName = rowData.propertyValueName;
				self.state.dataOrder = rowData.dataOrder;
				self.state.categoryPropertyValueId = rowData.categoryPropertyValueId;
	        	self.editProperty.init({
					propertyInfo:propertyInfo,
					chooseMaterial:{materialModelId:materialModelId,modelName:modelName},
					propertyValueName: rowData.propertyValueName,
					propertyCapacityValue:propertyCapacityValue,
					categoryPropertyValueId:self.state.categoryPropertyValueId,
					dataOrder:self.state.dataOrder,
					callBack:function(propertyValueName,dataOrder) {
						self.state.propertyValueName = propertyValueName,
						self.state.dataOrder = dataOrder
					},
					complete:function() {
						self.loadData(); 
					},
					continueChooseMaterialFunc:function() {},
					changeContinueFunc:function() {
						_.isObject(ProductPropertyDetail) &&  _.isFunction(ProductPropertyDetail.editChooseMaterialModal) && ProductPropertyDetail.editChooseMaterialModal(propertyInfo);
					}
				});
			})
		},
		loadData:function(prams) {
            var self = this;
            
            if(!self.state.id) {
				bootbox.alert('没找到分类属性id');
				return;
            }
            
			Rental.ajax.submit("{0}product/detailProductCategoryProperty".format(SitePath.service),{categoryPropertyId:self.state.id},function(response){
				if(response.success) {
                    self.initData(response.resultMap.data);	
					self.state.property = response.resultMap.data;
					self.getPropertyList();
				} else {
					Rental.notification.error("查询属性详情失败",response.description);
				}
			}, null , "查询属性详情", 'listLoading');
        },
        initData:function(data) {
            this.renderPropertyInfo(data);
            this.renderPropertyValList(data)
        },
        renderPropertyInfo:function(property) {
            var self = this;
            var data = _.extend(Rental.render, {
                property:property,
                isInputVal:function() {
                    return this.isInput == 1 ? "是" : (this.isInput == 0 ? "否" : "")
                },
                propertyTypeVal:function() {
                    return this.propertyType == 1 ? "租赁属性" : (this.propertyType == 2 ? "商品属性" : "")
                }
			});
			Mustache.parse(self.$propertyDetailTpl);
			self.$propertyDetail.html(Mustache.render(self.$propertyDetailTpl, data));
		},
		renderActionButton:function() {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl,{'acitonButtons':self.actionButtons}));
		},
		getPropertyList: function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
				categoryId: self.state.property.categoryId,
				propertyId: self.state.property.categoryPropertyId
			}, prams || {});
			Rental.ajax.ajaxDataNoLoading('product/pageProductCategoryPropertyValue', searchData, '加载属性值列表', function(response) {
				self.renderProperty(response.resultMap.data);
			});
		},
		renderProperty: function(data) {
			var self = this;
			self.renderPropertyValList(data);
			self.Pager.init(data,function(pageNo) {
				self.getPropertyList({pageNo: pageNo || 1})
			}, $('.pagerContainer', self.container));
		},
		doSearch: function(pageNo) {
			this.getPropertyList({pageNo: pageNo || 1})
		},
		renderPropertyValList:function(propertyData) {
			var self = this;
			
			var listData = propertyData.hasOwnProperty("itemList") ? propertyData.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;

			var data = {
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					propertyValInfo:function() {
						return JSON.stringify(this);
					},
					capacityValue:function() {
						if(this.hasOwnProperty("propertyCapacityValue") && !!this.propertyCapacityValue) {
							return this.propertyCapacityValue
						}
						if(this.hasOwnProperty("modelName") && !!this.modelName) {
							return this.modelName
						}
					}
				})
			}
			Mustache.parse(self.$dataListTpl);
			self.$dataListTable.html(Mustache.render(self.$dataListTpl, data));
        },
        chooseMaterialModal:function(propertyInfo) {
			var self = this;
			MaterialModalChoose.init({
				materialType:propertyInfo.materialType,
			}, function(rowData) {
				self.state.chooseMaterial = rowData;
			}, function() {
				self.addProperty.init({
					propertyInfo:propertyInfo,
					chooseMaterial:self.state.chooseMaterial,
					propertyValueName: self.state.propertyValueName,
					dataOrder: self.state.dataOrder,
					callBack:function(propertyValueName,dataOrder) {
						self.state.propertyValueName = propertyValueName,
						self.state.dataOrder = dataOrder
					},
					complete:function() {
						self.loadData(); 
					},
					continueChooseMaterialFunc:function() {},
					changeContinueFunc:function() {
						_.isObject(ProductPropertyDetail) &&  _.isFunction(ProductPropertyDetail.chooseMaterialModal) && ProductPropertyDetail.chooseMaterialModal(propertyInfo);
					}
				});
			});
		},
        addProperty:{
			init:function(prams) {
				this.props = _.extend({
					propertyInfo:null,
					chooseMaterial:null,
					propertyValueName:null,
					dataOrder:null,
					callBack:function() {},
					complete:function() {},
					modalCloseCallBack:function() {},
					continueChooseMaterialFunc:function() {},
					changeContinueFunc:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "product-manage/add-property", type:'ajax', 
                    ajaxContentAdded:function(pModal) {
                        self.initDom();
                        self.initEvent();	
                    },
                    afterClose:function() {
                        self.props.modalCloseCallBack && self.props.modalCloseCallBack();
                        _.isFunction(self.props.continueChooseMaterialFunc) && self.props.continueChooseMaterialFunc();
                    }
                });
			},
			initDom:function() {
				this.$form = $("#inputForm");
				this.$cancelButton = $('.cancelButton', this.$form);
			},
			initEvent:function() {
				var self = this;
				
				Rental.form.initFormValidation(self.$form, function(form){
					self.submit();
				});
	
				self.$cancelButton.on('click', function(event) {
					event.preventDefault();
					Rental.modal.close();
				});

				self.renderForm()

				$("#chooseMaterialModal").on("click",function() {
					self.props.continueChooseMaterialFunc = function() {
						_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
					}
					Rental.modal.close();
				})

				self.$form.on("change", "#propertyValueName,#dataOrder", function() {
					var propertyValueName = $("#propertyValueName").val()
					var dataOrder = $("#dataOrder").val()
					self.props.hasOwnProperty('callBack') && self.props.callBack(propertyValueName,dataOrder);
				})
			},
			renderForm:function() {
				var self = this
				if(self.props.propertyInfo.hasOwnProperty("isCapacityMaterial") && self.props.propertyInfo.isCapacityMaterial == 1) {
					$("#capacityCol").removeClass("hide")
				} else if(self.props.propertyInfo.hasOwnProperty("isCapacityMaterial") && self.props.propertyInfo.isCapacityMaterial == 0) {
					$("#materialModelCol").removeClass("hide")
				}

				if(!!self.props.chooseMaterial) {
					$("#materialModelName").val(self.props.chooseMaterial.modelName);
					$("#materialModelId").val(self.props.chooseMaterial.materialModelId);
				}

				if(!!self.props.propertyValueName) {
					$("#propertyValueName").val(self.props.propertyValueName);
				}

				if(!!self.props.dataOrder) {
					$("#dataOrder").val(self.props.dataOrder);
				}
			},
			submit:function() {
				try {
					var self = this;
					var formData = Rental.form.getFormData(self.$form);
					
					var commitData = {
						propertyId: self.props.propertyInfo.categoryPropertyId,
						categoryId: self.props.propertyInfo.categoryId,
						propertyValueName: formData["propertyValueName"],
                        dataOrder: formData["dataOrder"]
					}

					if(!!formData["propertyCapacityValue"]) {
						commitData.propertyCapacityValue = formData["propertyCapacityValue"]
					}

					if(!!formData["materialModelId"]) {
						commitData.materialModelId = formData["materialModelId"]
					}

					Rental.ajax.submit("{0}product/addProductCategoryPropertyValue".format(SitePath.service),commitData,function(response){
						if(response.success) {
							Rental.notification.success("添加商品属性",response.description || '成功');
							self.callBackFunc();
						} else {
							Rental.notification.error("添加商品属性",response.description || '失败');
						}
					}, null ,"添加商品属性",'dialogLoading');			
				} catch (e) {
					Rental.notification.error("添加商品属性", Rental.lang.commonJsError +  '<br />' + e );
				}
			},
			callBackFunc:function(response) {
				Rental.modal.close();
				this.props.hasOwnProperty('callBack') && this.props.callBack(response);
				this.props.hasOwnProperty('complete') && this.props.complete();
			},
		},
		editProperty:{
			init:function(prams) {
				this.props = _.extend({
					propertyInfo:null,
					chooseMaterial:null,
					propertyValueName:null,
					propertyCapacityValue:null,
					categoryPropertyValueId:null,
					dataOrder:null,
					callBack:function() {},
					complete:function() {},
					modalCloseCallBack:function() {},
					continueChooseMaterialFunc:function() {},
					changeContinueFunc:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "product-manage/add-property", type:'ajax', 
                    ajaxContentAdded:function(pModal) {
                        self.initDom();
                        self.initEvent();	
                    },
                    afterClose:function() {
                        self.props.modalCloseCallBack && self.props.modalCloseCallBack();
                        _.isFunction(self.props.continueChooseMaterialFunc) && self.props.continueChooseMaterialFunc();
                    }
                });
			},
			initDom:function() {
				this.$form = $("#inputForm");
				this.$cancelButton = $('.cancelButton', this.$form);
			},
			initEvent:function() {
				var self = this;
				
				Rental.form.initFormValidation(self.$form, function(form){
					self.submit();
				});
	
				self.$cancelButton.on('click', function(event) {
					event.preventDefault();
					Rental.modal.close();
				});

				self.renderForm()
				$(".modalTitle").html("修改商品属性值")

				$("#chooseMaterialModal").on("click",function() {
					self.props.continueChooseMaterialFunc = function() {
						_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
					}
					Rental.modal.close();
				})

				self.$form.on("change", "#propertyValueName,#dataOrder", function() {
					var propertyValueName = $("#propertyValueName").val()
					var dataOrder = $("#dataOrder").val()
					self.props.hasOwnProperty('callBack') && self.props.callBack(propertyValueName,dataOrder);
				})
			},
			renderForm:function() {
                var self = this
				if(self.props.propertyInfo.hasOwnProperty("isCapacityMaterial") && self.props.propertyInfo.isCapacityMaterial == 1) {
					$("#capacityCol").removeClass("hide")
				} else if(self.props.propertyInfo.hasOwnProperty("isCapacityMaterial") && self.props.propertyInfo.isCapacityMaterial == 0) {
					$("#materialModelCol").removeClass("hide")
				}

				if(!!self.props.chooseMaterial) {
					$("#materialModelName").val(self.props.chooseMaterial.modelName);
					$("#materialModelId").val(self.props.chooseMaterial.materialModelId);
				}

				if(!!self.props.propertyCapacityValue) {
					$("#propertyCapacityValue").val(self.props.propertyCapacityValue);
				}

				if(!!self.props.propertyValueName) {
					$("#propertyValueName").val(self.props.propertyValueName);
				}

				if(!!self.props.dataOrder) {
					$("#dataOrder").val(self.props.dataOrder);
				}
			},
			submit:function() {
				try {
					var self = this;
					var formData = Rental.form.getFormData(self.$form);
					
					var commitData = {
						categoryPropertyValueId: self.props.categoryPropertyValueId,
						propertyValueName: formData["propertyValueName"],
						dataOrder: formData["dataOrder"]
					}

					if(!!formData["propertyCapacityValue"]) {
						commitData.propertyCapacityValue = formData["propertyCapacityValue"]
					}

					if(!!formData["materialModelId"]) {
						commitData.materialModelId = formData["materialModelId"]
					}

					Rental.ajax.submit("{0}product/updateCategoryPropertyValue".format(SitePath.service),commitData,function(response){
						if(response.success) {
							Rental.notification.success("修改商品属性",response.description || '成功');
							self.callBackFunc();
						} else {
							Rental.notification.error("修改商品属性",response.description || '失败');
						}
					}, null ,"修改商品属性",'dialogLoading');			
				} catch (e) {
					Rental.notification.error("修改商品属性", Rental.lang.commonJsError +  '<br />' + e );
				}
			},
			callBackFunc:function(response) {
				Rental.modal.close();
				this.props.hasOwnProperty('callBack') && this.props.callBack(response);
				this.props.hasOwnProperty('complete') && this.props.complete();
			},
		},
		editChooseMaterialModal:function(propertyInfo) {
			var self = this;
			MaterialModalChoose.init({
				materialType:propertyInfo.materialType,
			}, function(rowData) {
				self.state.chooseMaterial = rowData;
			}, function() {
				self.editProperty.init({
					propertyInfo:propertyInfo,
					chooseMaterial:self.state.chooseMaterial,
					propertyValueName: self.state.propertyValueName,
					categoryPropertyValueId:self.state.categoryPropertyValueId,
					dataOrder:self.state.dataOrder,
					callBack:function(propertyValueName,dataOrder) {
						self.state.propertyValueName = propertyValueName,
						self.state.dataOrder = dataOrder
					},
					complete:function() {
						self.loadData(); 
					},
					continueChooseMaterialFunc:function() {},
					changeContinueFunc:function() {
						_.isObject(ProductPropertyDetail) &&  _.isFunction(ProductPropertyDetail.editChooseMaterialModal) && ProductPropertyDetail.editChooseMaterialModal(propertyInfo);
					}
				});
			});
		},
	};

	window.ProductPropertyDetail = ProductPropertyDetail;

})(jQuery);