;(function($){
	
	var ViewProduct = {
		init:function(productId,callBack,currentPage) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_product_detail];
		},
		initDom:function() {
			this.$editProductModal = $("#editProductModal");
			this.$form = $("#viewProductForm");
			this.$dropzoneProductImg = $("#dropzoneProductImg");
			this.$dropzoneProductDesImg = $("#dropzoneProductDesImg");
			this.$categoryId = $("#categoryId");
			this.$skuPropertiesContainer = $("#skuPropertiesContainer");
			this.$productPropertiesContainer = $("#productPropertiesContainer");
			this.$skuTable = $("#skuTable");
			this.data = [];

			this.productId = Rental.helper.getUrlPara('id');
			$("input[name=productId]",this.$form).val(this.productId);
		},
		initEvent:function() {
			var self = this;
			
			Layout.chooseSidebarMenu(AuthorCode.manage_product_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

	        ProductUtil.loadCategory(this.$categoryId);
	        ProductUtil.loadBrand();

	        self.loadData();

	        self.initCommonEvent({
	        	initDropzone:false
	        });
		},
		loadData:function() {
			var self = this;
			Rental.ajax.ajaxData('product/queryProductById', {productId:self.productId}, '查询商品详细信息', function(response) {
				self.data = response.resultMap.data;
				self.initFormData(self.data);	
			});
		},
		initFormData:function(data){
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			
			$.each(array,function(i,item){
				if(data.hasOwnProperty(item.name)) {
					var dom = self.$form.find("[name="+item.name+"]");
					dom.prop({disabled:true});
	            	dom.val(data[item.name]);
	            	dom.data('defaultvalue',data[item.name]);
				}
	        });
	        self.initProductImg(data);
	        self.initProductDesImg(data);

	        self.state.productSkuList = data.productSkuList;

	        //根据类别查询类别下所有属性
	       	var categoryId = this.$categoryId.data('defaultvalue');
	       	this.queryPropertiesByCategoryId(categoryId, function() {
	       		 self.renderSKUTable();
	       		 self.checkedProductProperty(data);
	       	});
		},
		initProductImg:function() {
			if(!this.data.hasOwnProperty('productImgList')) return ;
			this.initImg(this.data.productImgList,this.$dropzoneProductImg);
		},
		initProductDesImg:function() {
			if(!this.data.hasOwnProperty('productDescImgList')) return ;
			this.initImg(this.data.productDescImgList,this.$dropzoneProductDesImg);
		},
		initImg:function(imgs,container) {
			container.addClass('dz-started dz-demo');
			var examplePreviewTpl = $("#examplePreviewTpl").html();
			Mustache.parse(examplePreviewTpl);

			$.each(imgs,function(index, item) {
				setTimeout(function() {
					var renderData = {
						productImg:item,
						dataImg:function() {
							return JSON.stringify(this);
						}
					}
					var imgBox = Mustache.render(examplePreviewTpl,renderData);
					var $imgBox = $(imgBox);

					container.append($imgBox);
					$imgBox.addClass('animated fadeIn').removeClass('hidden');
					$imgBox.on('click','a.dz-remove',function(event) {
						event.preventDefault();		
						$(this).parent('.example-preview').remove();
					});

					Rental.ui.events.initViewer()
				},500);
			});
		},
		initProductImg:function() {
			if(!this.data.hasOwnProperty('productImgList')) return ;
			this.initImg(this.data.productImgList,this.$dropzoneProductImg);
		},
		initProductDesImg:function() {
			if(!this.data.hasOwnProperty('productDescImgList')) return ;
			this.initImg(this.data.productDescImgList,this.$dropzoneProductDesImg);
		},
		initImg:function(imgs,container) {
			container.addClass('dz-started dz-demo');
			var examplePreviewTpl = $("#examplePreviewTpl").html();
			Mustache.parse(examplePreviewTpl);

			$.each(imgs,function(index, item) {
				setTimeout(function() {
					var renderData = {
						productImg:item,
						dataImg:function() {
							return JSON.stringify(this);
						}
					}
					var imgBox = Mustache.render(examplePreviewTpl,renderData);
					var $imgBox = $(imgBox);

					container.append($imgBox);
					$imgBox.addClass('animated fadeIn').removeClass('hidden');
					$imgBox.on('click','a.dz-remove',function(event) {
						event.preventDefault();		
						$(this).parent('.example-preview').remove();
					});

					Rental.ui.events.initViewer()
				},500);
			});
		},
		checkedProductProperty:function(data) {
			data.productPropertyList.forEach(function(item) {
				$(".productPropertyRido", self.productPropertiesContainer).filter('[value='+item.propertyValueId+']').prop({checked:true});
			})
		},
	};

	window.ViewProduct = _.extend(ViewProduct, ProductMixin);

})(jQuery);