/**
 * description: 组合商品列表
 * author: zhangsheng
 * time: 2018/03/13
 */

;(function($) {

	var GroupedProductManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_grouped_product];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_grouped_product_add];
			var edit = this.currentPageAuthor.children[AuthorCode.manage_grouped_product_edit];
			var view = this.currentPageAuthor.children[AuthorCode.manage_grouped_product_detail];
			var del = this.currentPageAuthor.children[AuthorCode.manage_grouped_product_del];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'delButton','','删除'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.groupedProductList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
            });
            
            Rental.ui.events.initRangeDatePicker($('#timePicker'), $('#timePickerInput'), $("#startDate"), $("#endDate"));

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$dataListTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				var jointProductId = $(this).data('productid');
				self.del(jointProductId);
			});

			Rental.ui.renderSelect({
				container: $('#isNew'),
				data:Enum.array(Enum.isNewGroup),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '组合商品新旧'
			})

			//渲染操作按钮及事件
			this.renderCommonActionButton();
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			
			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(GroupedProductManage.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.groupedProductList, commitData);

			Rental.ajax.submit("{0}jointProduct/page".format(SitePath.service),commitData,function(response){
				if(response.success) {
					GroupedProductManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询商品列表失败",response.description);
				}
			}, null , "查询商品列表", 'listLoading');
		},
		doSearch:function(pageNo) {
			GroupedProductManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSearch);
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    dataStatusValue:function() {
                        switch(this.dataStatus) {
                            case 0:
                                return "不可用";
                            case 1:
                                return "可用";
                            case 2:
                                return "删除";
                            default:
                                return "";
                        }
					},
					groupedProductTostr:function() {
						var str = this.hasOwnProperty('jointProductProductList') && this.jointProductProductList.length > 0 ? this.jointProductProductList.map(function(item) {
							return item.hasOwnProperty("product") ? (item.product.hasOwnProperty("productName") ? item.product.productName : "") : ""
						}).join(' | ') : ''; 
						return str;
					},
					groupedMaterialTostr:function() {
						var str = this.hasOwnProperty('jointMaterialList') && this.jointMaterialList.length > 0 ? this.jointMaterialList.map(function(item) {
							return item.hasOwnProperty("material") ? (item.material.hasOwnProperty("materialName") ? item.material.materialName : "") : ""
						}).join(" | ") : '';
						return str;
					},
					isNewValue:function() {
						return this.isNew == 1 ? "全新" : "次新";
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		del:function(prams) {
			var self = this;
			if(!prams) {
				bootbox.alert('没找到组合商品ID');
				return;
			}
			function d() {
				Rental.ajax.submit("{0}jointProduct/delete".format(SitePath.service),{jointProductId:prams},function(response){
					if(response.success) {
						Rental.notification.success('删除组合商品',response.description || '成功');
						self.doSearch(self.Pager.pagerData.currentPage);
					} else {
						Rental.notification.error("删除组合商品",response.description || '失败');
					}
				}, null ,"查删除组合商品");
			}
			bootbox.confirm('确认删除？',function(result) {
				result && d();
			});
		}
	};

	window.GroupedProductManage = GroupedProductManage;

})(jQuery);