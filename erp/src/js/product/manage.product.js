;(function($) {

	var ProductManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_product_add];
			var edit = this.currentPageAuthor.children[AuthorCode.manage_product_edit];
			var view = this.currentPageAuthor.children[AuthorCode.manage_product_detail];
			var confirmStock = this.currentPageAuthor.children[AuthorCode.manage_product_stock];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			confirmStock && this.rowActionButtons.push(AuthorUtil.button(confirmStock,'confirmStock','','库存查询'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.productList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			this.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			//渲染操作按钮及事件
			this.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#isRent'),
				data:[{num:1, value:'上架'}, {num:0, value:'下架'}],
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（在租状态）'
			});

			ProductUtil.loadBrand(function() {
				self.searchData();
			});

			self.$dataListTable.on("click",".confirmStock",function(event) {
				event.preventDefault();
				var productName = $(this).data("name");
				var k3ProductNo = $(this).data("no");
				self.confirmStock(productName,k3ProductNo)
			})

			Rental.ui.events.imgGallery(this.$dataListTable);
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			
			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(ProductManage.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.productList, commitData);

			Rental.ajax.submit("{0}/product/queryAllProduct".format(SitePath.service),commitData,function(response){
				if(response.success) {
					ProductManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询商品列表失败",response.description);
				}
			}, null , "查询商品列表", 'listLoading');
		},
		doSearch:function(pageNo) {
			ProductManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSearch);
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '上架':"下架";
					},
					totalStock:function() {
						return _.reduce(this.productSkuList, function(pre, item) {
							return pre + parseInt(item.stock);
						}, 0);
					},
					dayRentPriceRange: function() {
						return self.rangPrice(this.productSkuList, "dayRentPrice");
					},
					monthRentPriceRang: function() {
						return self.rangPrice(this.productSkuList, "monthRentPrice");
					},
					newDayRentPriceRange:function() {
						return self.rangPrice(this.productSkuList, "newDayRentPrice");	
					},
					newMonthRentPriceRang:function() {
						return self.rangPrice(this.productSkuList, "newMonthRentPrice");	
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		rangPrice:function(list, pram) {
			if(!(_.isArray(list) && list.length > 0)) return "";
			var min = _.min(list, pram);
			var max = _.max(list, pram);
			if(min != max) {
				return '￥{0} ~ ￥{1}'.format(min[pram].toFixed(2), max[pram].toFixed(2));
			} else {
				return "￥" + max[pram].toFixed(2);
			}
		},
		confirmStock:function(productName,k3ProductNo) {
			var self = this;
			confirmProductStock.init({
				productName:productName,
				k3ProductNo:k3ProductNo
			})
		}
	};

	window.ProductManage = ProductManage;

})(jQuery);