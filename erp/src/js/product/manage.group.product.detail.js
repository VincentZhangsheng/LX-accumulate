/**
 * description: 组合商品详情
 * author: zhangsheng
 * time: 2018/03/14
 */

;(function($){
	var groupedProductDetail = {
		state:{
			id:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_grouped_product];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_grouped_product_detail];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
            this.$form = $("#groupedProductDetailForm");
            
            this.$headTpl = $("#groupedProductHeadTpl").html();
            this.$headModule = $("#groupedProductHead");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.id = Rental.helper.getUrlPara('id');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_grouped_product); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			Rental.ui.events.imgGallery(this.$form);

			self.$dataListTable.on('click', '.viewButton', function(event) {
				event.preventDefault();
				var $this =  $(this), productId = $this.data('productid');
				self.viewSku($this,productId);
            });
			
			self.$dataListTable.on('click', '.hideAffixPanel', function(event) {
				event.preventDefault();
				$(this).closest('.productDetial').slideUp('fast');
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.id) {
				 bootbox.alert("没找到组合商品ID");
				 return;
			}
			Rental.ajax.ajaxData('jointProduct/query', {jointProductId:self.state.id}, '查询组合商品详细信息', function(response) {
				self.render(response.resultMap.data);
			});
		},
		render:function(group) {
			this.renderGroupedProductHead(group);
            this.renderProductList(group);
			this.renderMaterialList(group);
			this.renderCount(group);
		},
		renderGroupedProductHead:function(group) {
			var self = this;

			var data = _.extend(Rental.render, {
				groupedProduct:group,
				isNewValue:function() {
					return this.isNew == 1 ? "全新" : "次新";
				},
            })
            Mustache.parse(self.$headTpl);
			self.$headModule.html(Mustache.render(self.$headTpl, data));
		},
		renderProductList:function(group) {
            var self = this;
            var listData = group.hasOwnProperty("jointProductProductList") ? group.jointProductProductList : [];
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					// "mainImg":function() {
					// 	return Rental.helper.mainImgUrlFormat(this.product.productImgList);
					// },
					// "productImgJSON":function() {
					// 	return Rental.helper.imgListToJSONStringify(this.product.productImgList, this.product.productName);
					// },
				})
            }
			Mustache.parse(self.$dataListTpl);
			self.$dataListTable.html(Mustache.render(self.$dataListTpl, data));
		},
		renderMaterialList:function(group) {
			var self = this;
			var listData = group.hasOwnProperty("jointMaterialList") ? group.jointMaterialList : [];
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					materialName:function() {
						return this.material.materialName;
					},
					brandName:function() {
						return this.material.brandName;
                    },
                    materialTypeName:function() {
                        return this.material.materialTypeName;
                    },
                    materialPrice:function() {
                        return this.material.materialPrice;
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.material.materialImgList);
					},
					"materialImgJSON":function() {
						return Rental.helper.imgListToJSONStringify(this.material.materialImgList, this.material.materialName);
					},
				})
            }
			Mustache.parse(self.$materialDataListTpl);
			self.$materialDataListTable.html(Mustache.render(self.$materialDataListTpl, data));
		},
		viewSku:function($button,id) {
			var detailTr =  $button.closest('tr').next('tr'), productDetial = $('.productDetial',detailTr);
			if(productDetial.size() > 0) {
				productDetial.slideDown(); 
			} else {
				this.getProductById($('td',detailTr),id);	
			}
		},
		getProductById:function(container,id) {
			var self = this;
			Rental.ajax.ajaxData('product/queryProductById', {productId:id}, '加载商品详细信息', function(response) {
				self.renderRowProduct(container, response.resultMap.data);
			});
		},
		renderRowProduct:function(container,product) {
			var self = this;
			var data  = {
				product:product,
				productJSONString:function() {
					return JSON.stringify(this);
				},
				skuListSource:function() {
					var skuList = this.productSkuList;
					return _.extend(Rental.render, {
						skuList:skuList,
						propertiesToStr:function() {
							var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				}
			}
			var tpl = $('#productDetialTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		renderCount:function(data) {
			$("#orderItemProductCount").html(data.hasOwnProperty("jointProductProductList") ? data.jointProductProductList.length : 0);
			$("#orderItemMaterialCount").html(data.hasOwnProperty("jointMaterialList") ? data.jointMaterialList.length : 0);
		}
	};
	
	window.groupedProductDetail = groupedProductDetail;

})(jQuery);