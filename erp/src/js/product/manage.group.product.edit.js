/**
 * description: 编辑组合商品
 * author: zhangsheng
 * time: 2018/03/14
 */

;(function($){
	
	var editGroupedProduct = {
		state:{
			id:null,
			chooseProductList:new Array(),
            chooseMaterialList:new Array(),
            groupedProduct:new Object(),
            order:new Object(),
		},
		init:function() {
            this.initHeaderTitle();
			this.initAuthor();
			this.initDom();
            this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_grouped_product];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_grouped_product_edit];
		},
		initDom:function() {
			this.$form = $("#editProductForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.id = Rental.helper.getUrlPara('id');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_grouped_product); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form, function() {
				self.edit();
			});

            self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.id) {
				bootbox.alert('没找到组合商品ID');
				return;
			}
			Rental.ajax.submit("{0}jointProduct/query".format(SitePath.service),{jointProductId:self.state.id},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载组合商品详细信息",response.description || '失败');
				}
			}, null ,"加载组合商品详细信息");
		},
		initData:function(data) {
            this.state.order = data;
            Rental.ui.renderFormData(this.$form, data);
			this.initChooseProductList(data);
            this.initChooseMaterialList(data);
        },
		initChooseProductList:function(data) {
			this.state.chooseProductList = groupedProductUtil.initChooseProductList(data.jointProductProductList);
			this.renderProductList();
		},	
		initChooseMaterialList:function(data) {
			this.state.chooseMaterialList = groupedProductUtil.initChooseMaterialList(data.jointMaterialList);
			this.renderMaterialList();
		},
		edit:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var isNew = $('[name=isNew]', self.$form).prop('checked');
				formData['isNew'] = isNew ? 1 : 0;

				var commitData = {
					jointProductId:self.state.id,
                    jointProductName:formData['jointProductName'],
					remark:formData['remark']
				}

				var jointProductProductList = self.state.chooseProductList.map(function(item) {
					if(!!item.jointProductProductId) {
						return {
							jointProductProductId:item.jointProductProductId,
							productId:item.productId,
							productCount:item.productCount
						}
					} else {
						return {
							productId:item.productId,
							productCount:item.productCount
						}
					}
				});
				var jointMaterialList = groupedProductUtil.getMaterialList(self.$materialDataListTable);
				jointMaterialList = jointMaterialList.map(function(item) {
					if(!!item.jointMaterialId) {
						return {
							jointMaterialId:item.jointMaterialId,
							materialNo:item.materialNo,
							materialCount:item.materialCount
						}
					} else {
						return {
							materialNo:item.materialNo,
							materialCount:item.materialCount
						}
					}
				})
                
                if(jointProductProductList.length == 0 && jointMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(jointProductProductList.length > 0) {
					commitData.jointProductProductList = jointProductProductList;
				}

				if(jointMaterialList.length > 0) {
					commitData.jointMaterialList = jointMaterialList;
				}

				Rental.ajax.submit("{0}jointProduct/update".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success("编辑组合商品",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑组合商品",response.description || '失败');
					}
				}, null ,"编辑组合商品",'dialogLoading');
			} catch(e) {
				Rental.notification.error("编辑组合商品失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑组合商品",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.loadData();
				        }
				    }
				});
			}
        },
        initHeaderTitle:function() {
            $("#panelTitle").html('<i class="fa fa-pencil"></i>编辑组合商品')
        }
	};

	window.editGroupedProduct = _.extend(editGroupedProduct, groupedProductMixin);

})(jQuery);