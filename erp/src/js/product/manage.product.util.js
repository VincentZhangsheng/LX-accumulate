;(function($) {

	var ProductUtil = {
		initDropzone:function() {
			var self = this;
 			$('.dropzoneImg').dropzone({
		        url: SitePath.base + "/product/uploadImage",
		        maxFiles: 20,
		        maxFilesize:2,//MB
		        acceptedFiles: ".jpg,.ipeg,.png",
		        addRemoveLinks: true,
		        dictCancelUpload:"取消上传",
		        dictRemoveFile:'删除',
		        dictFileTooBig:'文件过大（不能大于2M）',
		        dictMaxFilesExceeded:"超过最大上传数量",
		        success:function(e,response) {
		        	self.dropzoneSuccess(e,response)
		        }
		    });
		},
		dropzoneSuccess:function(e,response) {
			var previewElement = $(e.previewElement);
			var progress = $(".dz-progress",previewElement);
			var error = $(".dz-error-mark",previewElement);
			var remove = $('.dz-remove',previewElement);

			progress.css({opacity:'0'});
			remove.html("删除");

			if(response.success){
				previewElement.data("img",response.resultMap.data[0]);
			} else {
				error.css({opacity:'1'});
			}
		},
		loadCategory:function(container) {
			var self = this;
			ApiData.category({
				searchData:{
					categoryType:Enum.categoryType.num.product
				},
				success:function(response) {
					self.renderSelectCategory(response, container);
				}
			});
		},
		renderSelectCategory:function(data, container) {
			var option = "<option value={0}>{1}</option>", level = 0;

			function blank(num) {
				var b = new Array();
				for (var i = 0; i < num*4; i++) {
					b.push('&nbsp;');
				}
				return b;
			}

			function func(category, array) {
				var bs = blank(level).join('');
				for (var i = 0; i < category.length; i++) {
					var curr = category[i];
					array.push(option.format(curr.categoryId,bs + curr.categoryName));
					if(curr.hasOwnProperty('children')) {
						level++;
						func(curr.children,array)
					}
				}

				level--;
			}

			var opts = new Array();
			func(data,opts);
			container.html(opts.join(''));

			var defaultvalue = container.data('defaultvalue');
			if(defaultvalue) {
				container.val(defaultvalue);
			}
		},
		//数组笛卡尔积计算
		descartes:function(dimvalue) {
			var result = new Array();
			function d(layer, cur) {
				if(layer < dimvalue.length - 1) {
					if(dimvalue[layer].length == 0) {
						d(layer+1,cur);
					} else {
						for (var i = 0; i < dimvalue[layer].length; i++) {
							var list = new Array();
							list = list.concat(cur);
							list.push(dimvalue[layer][i])
							d(layer+1, list);
						}
					}

				} else if(layer == dimvalue.length - 1) {
					if(dimvalue[layer].length == 0) {
						result.push(cur);
					} else {
						for (var i = 0; i < dimvalue[layer].length; i++) {
							var list = new Array();
							list = list.concat(cur);
							list.push(dimvalue[layer][i])
							result.push(list);
						}
					}
				}
			}

			d(0, new Array());

			return result;
		},
		getSkuByPerporties:function(perporties, skuList, result, isAdd) {
			var sku = null;

			skuList.forEach(function(el) {
				if(el.hasOwnProperty('productSkuPropertyList') && el.productSkuPropertyList.length >= perporties.length) {

					//当商品原SKU属性列表 包含新生成的shu属性列表

					var every = perporties.every(function(property,i) {
						var isSome = el.productSkuPropertyList.some(function(skuProperty) {
							return skuProperty.propertyValueId == property.categoryPropertyValueId;
						});
						return isSome;
					});
					if(every) {
						var array = new Array();
						el.productSkuPropertyList.forEach(function(skuProperty){
							var isSome = perporties.some(function(property) {
								return skuProperty.propertyValueId == property.categoryPropertyValueId;
							});
							if(isSome) {
								array.push(skuProperty);
							}
						});
						if(array.length > 0) el.productSkuPropertyList = array;
						sku = el;
					}
				} else if (el.hasOwnProperty('productSkuPropertyList') && el.productSkuPropertyList.length < perporties.length) {
					
					//当商品原SKU属性列表 被包含于新生成的shu属性列表

					var every = el.productSkuPropertyList.every(function(skuProperty,i) {
						var isSome = perporties.some(function(property) {
							return skuProperty.propertyValueId == property.categoryPropertyValueId;
						});
						return isSome;
					});
					if(every) {
						var array = new Array();
						perporties.forEach(function(property){
							var isSome = el.productSkuPropertyList.some(function(skuProperty) {
								return skuProperty.propertyValueId == property.categoryPropertyValueId;
							});
							if(!isSome) {
								property.propertyValueId = property.categoryPropertyValueId;
								array.push(property);
							}
						});
						if(array.length > 0) el.productSkuPropertyList = el.productSkuPropertyList.concat(array);
						sku = el;
					}
				}

				//判断结果结合是否已经存在该SKU，存在需要执行下一步，不存在跳出循环，返回结果
				if(sku) {
					var has = sku && result.some(function(item) {
						if(isAdd) {
							return item.rowId == sku.rowId;
						} else {
							return item.skuId == sku.skuId;
						}
						
					});

					if(has) {
						sku = null;
					} else {
						return;
					}
				}
			});

			return sku;
		},
		loadBrand:function(changeFunc) {
			ApiData.brand({
				success:function(response) {
					Rental.ui.renderSelect({
						data:response,
						container:$('[name=brandId]'),
						func:function(opt, item) {
							return opt.format(item.brandId, item.brandName);
						},
						change:changeFunc || function() {},
						defaultText:'全部（品牌）'
					});
				}
			});
		},
	};

	window.ProductUtil = ProductUtil;

})(jQuery);