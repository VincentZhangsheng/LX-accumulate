;(function($) {

	var SKUManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_sku_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var instorageAuthor = this.currentPageAuthor.children[AuthorCode.manage_product_sku_list_instorage];
			if(instorageAuthor) {
				this.rowActionButtons.push({
					menuName:instorageAuthor.menuName,
					menuUrl:instorageAuthor.menuUrl,
					class:"instorageButton",
					iClass:'',
					text:'入库'	
				});
			}
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$skuListTpl = $("#skuListTpl").html();
			this.$skuListTbody = $("#skuListTbody");
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			this.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			this.$btnRefresh.on("click",function(){
				self.searchData();
			});

			this.$skuListTbody.on('click', '.instorageButton', function(event) {
				event.preventDefault();
				var $this = $(this);
				var productId = $this.data('productid');
				var skuId = $this.data('skuid');
				bootbox.prompt({
				    title: "输入入库数量",
				    inputType: 'number',
				    callback: function (result) {
				    	if(result === null) {
				    		return true;
				    	}
				    	if($.trim(result) == "") {
				    		Rental.notification.error("入库",'请输入数量');
				    		return false;
				    	}
				        self.instorage({productId:productId,productSkuId:skuId,productCount:$.trim(result)});
				    }
				});
			});
			
		},
		searchData:function(prams) {
			var _this = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(SKUManage.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}/product/queryProductSkuList".format(SitePath.service),searchData,function(response){

				if(response.success) {
					SKUManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询SKU列表失败",response.description);
				}
				
			}, null ,"查询SKU列表", 'listLoading');
		},
		doSarch:function(pageNo) {
			SKUManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			Pager.init(data,this.doSarch);
		},
		renderList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				"listData":listData,
				"skuPriceFixed":function() {
					return !!this.skuPrice ? this.skuPrice.toFixed(2) : "";
				},
				"originalPriceFixed":function() {
					return !!this.originalPrice ? this.originalPrice.toFixed(2) : "";
				},
				"salePriceFixed":function() {
					return !!this.salePrice ? this.salePrice.toFixed(2) : "";
				},
				propertiesForamt:function() {
					if(this.hasOwnProperty('productSkuPropertyList')) {
						return this.productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(' ');
					} 
					return "";
				},
				acitonButtons:function() {
					return {
						acitons:self.rowActionButtons
					}
				}
			}

			Mustache.parse(this.$skuListTpl);
			this.$skuListTbody.html(Mustache.render(this.$skuListTpl, data));
		},
		instorage:function(prams) {
			var self = this;
			Rental.ajax.submit("{0}/product/productInStorage".format(SitePath.service),prams,function(response){
				if(response.success) {
					Rental.notification.success("入库",'成功入库');
					self.doSarch(Pager.pagerData.currentPage);
				} else {
					Rental.notification.error("入库",response.description || '入库失败');
				}
			}, null ,"入库");
		}
	};

	window.SKUManage = SKUManage;

})(jQuery);