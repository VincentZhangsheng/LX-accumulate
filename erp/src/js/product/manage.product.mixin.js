/**
* 商品公用类
*/
;(function() {

	ProductMixin = {
		state:{
			productProperties:new Array(),
			skuProperties:new Array(),
			productSkuList:new Array(),
			chooseMaterial:null,
			propertyValueName:null,
			dataOrder:null
		},
		initCommonEvent:function(prams) {
			var self = this;

			prams = _.extend({
				initDropzone:true,
			}, prams || {});

			Rental.ui.renderSelect({
				container:$("#unit"),
				data:Enum.array(Enum.productUnit),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				},
				defaultText:'请选择'
			});
			
			self.loadCategory(self.$categoryId);
			self.loadBrand();

	        self.$categoryId.on('change', function(event) {
	        	event.preventDefault();
	        	var value = $(this).val();
	        	value && self.queryPropertiesByCategoryId(value);
	        });

	        prams.initDropzone && Rental.ui.events.initDropzone($('.dropzoneImg'), 'product/uploadImage');

	        self.$form.on('click', '.addSku', function(event) {
	        	event.preventDefault();
	        	self.addSku();
	        });

	        self.$form.on('click', '.resetCheckedBox', function(event) {
	        	event.preventDefault();
	        	$('[name^=property-]', self.$form).prop({checked:false});
	        });

	        this.$skuPropertiesContainer.on('click', '[type=checkbox]', function(event) {
	        	// event.preventDefault();
	        	self.checkPropery($(this));
	        });

	        this.$skuTable.on('change', '.skuPrice,.dayRentPrice,.monthRentPrice,.newSkuPrice,.newDayRentPrice,.newMonthRentPrice', function(event) {
	        	event.preventDefault();
	        	self.changeSkuVal($(this));
	        });

	        this.$skuTable.on('click', '.delSku', function(event) {
	        	event.preventDefault();
	        	self.delSku($(this));
			});
			
			// self.$form.on('click', '.addProperty', function(event) {
			// 	event.preventDefault();
			// 	var propertyInfo = $(this).data("info");
			// 	self.state.propertyValueName = null,
			// 	self.state.dataOrder = null
	        // 	self.addProperty.init({
			// 		propertyInfo:propertyInfo,
			// 		callBack:function(propertyValueName,dataOrder) {
			// 			self.state.propertyValueName = propertyValueName,
			// 			self.state.dataOrder = dataOrder
			// 			self.queryPropertiesByCategoryId($("#categoryId").val());
			// 		},
			// 		continueChooseMaterialFunc:function() {},
			// 		changeContinueFunc:function() {
			// 			_.isObject(ProductMixin) &&  _.isFunction(ProductMixin.chooseMaterialModal) && ProductMixin.chooseMaterialModal(propertyInfo);
			// 		}
			// 	});
			// });
		},
		loadCategory:function(container) {
			var self = this;
			ApiData.category({
				searchData:{
					categoryType:Enum.categoryType.num.product
				},
				success:function(response) {
					self.renderSelectCategory(response, container);
				}
			});
		},
		renderSelectCategory:function(data, container) {
			var option = "<option value={0}>{1}</option>", level = 0;

			function blank(num) {
				var b = new Array();
				for (var i = 0; i < num*4; i++) {
					b.push('&nbsp;');
				}
				return b;
			}

			function func(category, array) {
				var bs = blank(level).join('');
				for (var i = 0; i < category.length; i++) {
					var curr = category[i];
					array.push(option.format(curr.categoryId,bs + curr.categoryName));
					if(curr.hasOwnProperty('children')) {
						level++;
						func(curr.children,array)
					}
				}

				level--;
			}

			var opts = new Array();
			func(data,opts);
			container.html(opts.join(''));

			var defaultvalue = container.data('defaultvalue');
			if(defaultvalue) {
				container.val(defaultvalue);
			}
		},
		loadBrand:function(changeFunc) {
			ApiData.brand({
				success:function(response) {
					Rental.ui.renderSelect({
						data:response,
						container:$('[name=brandId]'),
						func:function(opt, item) {
							return opt.format(item.brandId, item.brandName);
						},
						change:changeFunc || function() {},
					});
				}
			});
		},
		queryPropertiesByCategoryId:function(categoryId, callback) {
			var self = this;
			Rental.ajax.ajaxData('product/queryPropertiesByCategoryId', {categoryId:categoryId}, '加载属性', function(response) {
				self.resolveProperties(response.resultMap.data);
				callback && callback();
			});
		},
		resolveProperties:function(data) {
			var skuProperties = new Array();
			var productProperties = new Array();
			$.each(data,function(index, el) {
				if(el.propertyType == Rental.Enum.propertyType.num.sku) {
					skuProperties.push(el);
				} else {
					productProperties.push(el);
				}
			});
			this.productProperties = productProperties;
			this.skuProperties = skuProperties;

			this.state = _.extend(this.state, {
				productProperties:productProperties,
				skuProperties:skuProperties,
			})

			this.renderProductProperties();
			this.renderSKUProperties();
		},
		//商品属性
		renderProductProperties:function() {
			var self = this,
				renderData = {
					hasProperties:self.state.productProperties.length > 0,
					properties:self.state.productProperties,
					propertyStr:function() {
						return JSON.stringify(this);
					},
					propertyValueList:function(){
						return {
							list:this.productCategoryPropertyValueList,
							property:function() {
								return JSON.stringify(this);
							}
						};
					}
				}

			var tpl = $("#productPropertiesTpl").html();
			Mustache.parse(tpl);
			this.$productPropertiesContainer.html(Mustache.render(tpl,renderData));

			//初始化滚动条
            $('.nano',this.$productPropertiesContainer).nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		//sku销售属性
		renderSKUProperties:function() {
			var self = this,
				renderData = {
					hasProperties:self.state.skuProperties.length > 0,
					properties:self.state.skuProperties,
					propertyStr:function() {
						return JSON.stringify(this);
					},
					propertyValueList:function(){
						return {
							list:this.productCategoryPropertyValueList,
							property:function() {
								return JSON.stringify(this);
							}
						};
					}
				}

			var tpl = $("#skuPropertiesTpl").html();
			Mustache.parse(tpl);
			this.$skuPropertiesContainer.html(Mustache.render(tpl,renderData));

			//初始化滚动条
            $('.nano',this.$skuPropertiesContainer).nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		checkPropery:function($this) {
			var isChecked = $this.prop('checked');
			if(isChecked) {
				var $propertyCheckBoxs = $this.closest('.propertyCheckBoxs');
				$('[type=checkbox]', $propertyCheckBoxs).prop({checked:false});	
			}
			$this.prop({checked:isChecked});
		},
		addSku:function() {
			var self = this,
				productSkuPropertyList = new Array();

			self.state.skuProperties.forEach(function(property){
				$("input[name=property-"+ property.categoryPropertyId +"]", self.$skuPropertiesContainer).filter(':checked').each(function(i,item) {
					var propertyValue = $(item).data('property');
					productSkuPropertyList.push({
						propertyId:propertyValue.propertyId,
						propertyValueId:propertyValue.categoryPropertyValueId,
						propertyValueName:propertyValue.propertyValueName,
						isSku:1,
					});
				});
			});

			var productSku = {
				rowId: Rental.helper.randomString(8),//((new Date()).getTime()).toString(),
				skuPrice:null,
				dayRentPrice:null,
				monthRentPrice:null,
				productSkuPropertyList:productSkuPropertyList,
			}

			if(self.isContainsSku(productSku)) {
				bootbox.alert('SKU已经存在');
				return;
			}

			self.state.productSkuList.push(productSku);
			self.renderSKUTable();
		},
		isContainsSku:function(productSku) {
			return this.state.productSkuList.some(function(sku) {
				if(sku.productSkuPropertyList.length != productSku.productSkuPropertyList.length) {
					return false;
				}
				return sku.productSkuPropertyList.every(function(propertyValue) {
					return productSku.productSkuPropertyList.some(function(somePropertyValue) {
						return somePropertyValue.propertyValueId == propertyValue.propertyValueId;
					});
				});
			});
		},
		renderSKUTable:function() {
			var self = this;
			var data = {
				ths:self.state.skuProperties,
				sku:{
					list:self.state.productSkuList,
					curskuProperty:function() {
						return JSON.stringify(this.productSkuPropertyList);
					},
					property:function() {
						var cur = this.productSkuPropertyList,
							arr =  self.state.skuProperties.map(function(n){
								return _.find(cur,{propertyId:n.categoryPropertyId}) || {};
							});
						return arr || {};
					}
				}
			}
			var tpl = $("#skuTableTpl").html();
			Mustache.parse(tpl);
			this.$skuTable.html(Mustache.render(tpl, data));
		},
		changeSkuVal:function($this) {
			var sku = this.getSku($this.closest('.skuRow'));
			var index = _.findIndex(this.state.productSkuList, {rowId:sku.rowId});
			this.state.productSkuList[index] = _.extend(this.state.productSkuList[index], sku);
		},
		getSku:function($skuRow) {
			return {
				rowId:$('.rowId', $skuRow).val(),
				skuPrice:$.trim($('.skuPrice', $skuRow).val()),
				dayRentPrice:$.trim($('.dayRentPrice', $skuRow).val()),
				monthRentPrice:$.trim($('.monthRentPrice', $skuRow).val()),
				newSkuPrice:$.trim($('.newSkuPrice', $skuRow).val()),
				newDayRentPrice:$.trim($('.newDayRentPrice', $skuRow).val()),
				newMonthRentPrice:$.trim($('.newMonthRentPrice', $skuRow).val()),
			}
		},
		delSku:function($this) {
			var rowId = $this.data('rowid'),
				index = _.findIndex(this.state.productSkuList, {rowId:rowId});
			if(index > -1) {
				this.state.productSkuList.splice(index, 1);	
				this.renderSKUTable();
			}
		},
		getImageList:function(container) {
			var imgArray = new Array();
			$('.dz-image-preview', container).each(function(index, el) {
				var img = $(el).data('img');
				imgArray.push({productImgId:img.productImgId});
			});
			return imgArray;
		},
		// chooseMaterialModal:function(propertyInfo) {
		// 	var self = this;
		// 	MaterialModalChoose.init({
		// 		materialType:propertyInfo.materialType,
		// 	}, function(rowData) {
		// 		self.state.chooseMaterial = rowData;
		// 	}, function() {
		// 		self.addProperty.init({
		// 			propertyInfo:propertyInfo,
		// 			chooseMaterial:self.state.chooseMaterial,
		// 			propertyValueName: self.state.propertyValueName,
		// 			dataOrder: self.state.dataOrder,
		// 			callBack:function(propertyValueName,dataOrder) {
		// 				self.state.propertyValueName = propertyValueName,
		// 				self.state.dataOrder = dataOrder
		// 				self.queryPropertiesByCategoryId($("#categoryId").val());
		// 			},
		// 			continueChooseMaterialFunc:function() {},
		// 			changeContinueFunc:function() {
		// 				_.isObject(ProductMixin) &&  _.isFunction(ProductMixin.chooseMaterialModal) && ProductMixin.chooseMaterialModal(propertyInfo);
		// 			}
		// 		});
		// 	});
		// },
		// addProperty:{
		// 	init:function(prams) {
		// 		this.props = _.extend({
		// 			propertyInfo:null,
		// 			chooseMaterial:null,
		// 			propertyValueName:null,
		// 			dataOrder:null,
		// 			callBack:function() {},
		// 			complete:function() {},
		// 			modalCloseCallBack:function() {},
		// 			continueChooseMaterialFunc:function() {},
		// 			changeContinueFunc:function() {}
		// 		}, prams || {});
		// 		this.show();
		// 	},
		// 	show:function() {
		// 		var self = this;
		// 		Rental.modal.open({src:SitePath.base + "product-manage/add-property", type:'ajax', 
		// 		ajaxContentAdded:function(pModal) {
		// 			self.initDom();
		// 			self.initEvent();	
		// 		},
		// 		afterClose:function() {
		// 			self.props.modalCloseCallBack && self.props.modalCloseCallBack();
		// 			_.isFunction(self.props.continueChooseMaterialFunc) && self.props.continueChooseMaterialFunc();
		// 		}
		// 	});
		// 	},
		// 	initDom:function() {
		// 		this.$form = $("#inputForm");
		// 		this.$cancelButton = $('.cancelButton', this.$form);
		// 	},
		// 	initEvent:function() {
		// 		var self = this;
				
		// 		Rental.form.initFormValidation(self.$form, function(form){
		// 			self.submit();
		// 		});
	
		// 		self.$cancelButton.on('click', function(event) {
		// 			event.preventDefault();
		// 			Rental.modal.close();
		// 		});

		// 		self.renderForm()

		// 		$("#chooseMaterialModal").on("click",function() {
		// 			self.props.continueChooseMaterialFunc = function() {
		// 				_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
		// 			}
		// 			Rental.modal.close();
		// 		})

		// 		self.$form.on("change", "#propertyValueName,#dataOrder", function() {
		// 			var propertyValueName = $("#propertyValueName").val()
		// 			var dataOrder = $("#dataOrder").val()
		// 			self.props.hasOwnProperty('callBack') && self.props.callBack(propertyValueName,dataOrder);
		// 		})
		// 	},
		// 	renderForm:function() {
		// 		var self = this
		// 		if(self.props.propertyInfo.hasOwnProperty("isCapacityMaterial") && self.props.propertyInfo.isCapacityMaterial == 1) {
		// 			$("#capacityCol").removeClass("hide")
		// 		} else if(self.props.propertyInfo.hasOwnProperty("isCapacityMaterial") && self.props.propertyInfo.isCapacityMaterial == 0) {
		// 			$("#materialModelCol").removeClass("hide")
		// 		}

		// 		if(!!self.props.chooseMaterial) {
		// 			$("#materialModelName").val(self.props.chooseMaterial.modelName);
		// 			$("#materialModelId").val(self.props.chooseMaterial.materialModelId);
		// 		}

		// 		if(!!self.props.propertyValueName) {
		// 			$("#propertyValueName").val(self.props.propertyValueName);
		// 		}

		// 		if(!!self.props.dataOrder) {
		// 			$("#dataOrder").val(self.props.dataOrder);
		// 		}
		// 	},
		// 	submit:function() {
		// 		try {
		// 			var self = this;
		// 			var formData = Rental.form.getFormData(self.$form);
					
		// 			var commitData = {
		// 				propertyId: self.props.propertyInfo.categoryPropertyId,
		// 				categoryId: self.props.propertyInfo.categoryId,
		// 				propertyValueName: formData["propertyValueName"],
		// 				dataOrder: formData["dataOrder"]
		// 			}

		// 			if(!!formData["propertyCapacityValue"]) {
		// 				commitData.propertyCapacityValue = formData["propertyCapacityValue"]
		// 			}

		// 			if(!!formData["materialModelId"]) {
		// 				commitData.materialModelId = formData["materialModelId"]
		// 			}

		// 			Rental.ajax.submit("{0}product/addProductCategoryPropertyValue".format(SitePath.service),commitData,function(response){
		// 				if(response.success) {
		// 					Rental.notification.success("添加商品属性",response.description || '成功');
		// 					self.callBackFunc();
		// 				} else {
		// 					Rental.notification.error("添加商品属性",response.description || '失败');
		// 				}
		// 			}, null ,"添加商品属性",'dialogLoading');			
		// 		} catch (e) {
		// 			Rental.notification.error("添加商品属性", Rental.lang.commonJsError +  '<br />' + e );
		// 		}
		// 	},
		// 	callBackFunc:function(response) {
		// 		Rental.modal.close();
		// 		this.props.hasOwnProperty('callBack') && this.props.callBack(response);
		// 	},
		// },
	}

	window.ProductMixin = ProductMixin;

})(jQuery);