;(function($) {

	var ProductPropertyManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_property_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.propertyList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.loadCategory($("#categoryId"));
			self.searchData();

			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.$searchForm.on('change', "#categoryId", function(event) {
	        	event.preventDefault();
	        	self.searchData();
	        });
		},
		loadCategory:function(container) {
			var self = this;
			ApiData.category({
				searchData:{
					categoryType:Enum.categoryType.num.product
				},
				success:function(response) {
					self.renderSelectCategory(response, container);
				}
			});
		},
		renderSelectCategory:function(data, container) {
			var option = "<option value={0}>{1}</option>", level = 0;

			function blank(num) {
				var b = new Array();
				for (var i = 0; i < num*4; i++) {
					b.push('&nbsp;');
				}
				return b;
			}

			function func(category, array) {
				var bs = blank(level).join('');
				for (var i = 0; i < category.length; i++) {
					var curr = category[i];
					array.push(option.format(curr.categoryId,bs + curr.categoryName));
					if(curr.hasOwnProperty('children')) {
						level++;
						func(curr.children,array)
					}
				}

				level--;
			}

			var opts = new Array();
			func(data,opts);
			container.html(opts.join(''));

			var defaultvalue = container.data('defaultvalue');
			if(defaultvalue) {
				container.val(defaultvalue);
			}
		},
		searchData:function(prams) {
			var self = this;
			
			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			},  self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.propertyList, commitData);

			Rental.ajax.submit("{0}/product/pageProductCategoryProperty".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询属性列表失败",response.description);
				}
			}, null , "查询属性列表", 'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
            var self = this;
			this.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',$("#content")));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    isInputVal:function() {
                        return this.isInput == 1 ? "是" : (this.isInput == 0 ? "否" : "")
                    },
                    propertyTypeVal:function() {
                        return this.propertyType == 1 ? "租赁属性" : (this.propertyType == 2 ? "商品属性" : "")
                    }
				})
			}
			Mustache.parse(self.$dataListTpl);
			self.$dataListTable.html(Mustache.render(self.$dataListTpl, data));
		},
	};

	window.ProductPropertyManage = ProductPropertyManage;

})(jQuery);