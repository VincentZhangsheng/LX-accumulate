Rental.notification = {
	error:function(title,message) {
        this.showPNotify(title,message,{
    		noteStyle:"danger",
	        noteShadow:true,
	        noteOpacity:1,
	        noteStack:"stack_top_right",
	        width:"290px",
        });
		
	},
	success:function(title,message) {
		this.showPNotify(title,message,{
    		noteStyle:"success",
	        noteShadow:true,
	        noteOpacity:1,
	        noteStack:"stack_top_right",
	        width:"290px",
        });
	},
	showPNotify:function(title,message,prams) {
		new PNotify({
            title: title,
            text: message,
            shadow: prams.noteShadow,
            opacity: prams.noteOpacity,
            addclass: prams.noteStack,
            type: prams.noteStyle,
            stack: this.stacks[prams.noteStack],
            width: this.findWidth(prams.noteStack),
            delay: 3000
        });
	},
	findWidth:function(noteStack) {
        if (noteStack == "stack_bar_top") {
            return "100%";
        }
        if (noteStack == "stack_bar_bottom") {
            return "70%";
        } else {
            return "290px";
        }
    },
	stacks:{
        stack_top_right: {
            "dir1": "down",
            "dir2": "left",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_top_left: {
            "dir1": "down",
            "dir2": "right",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_bottom_left: {
            "dir1": "right",
            "dir2": "up",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_bottom_right: {
            "dir1": "left",
            "dir2": "up",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_bar_top: {
            "dir1": "down",
            "dir2": "right",
            "push": "top",
            "spacing1": 0,
            "spacing2": 0
        },
        stack_bar_bottom: {
            "dir1": "up",
            "dir2": "right",
            "spacing1": 0,
            "spacing2": 0
        },
        stack_context: {
            "dir1": "down",
            "dir2": "left",
            "context": $("#stack-context")
        },
    },
}
