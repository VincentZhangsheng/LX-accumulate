var Rental = Rental || {};

var SitePath = SitePath || {
    base:"http://192.168.10.167:8081/",
    service:"http://192.168.10.167:8081/",
};

function set_ajax(){
    $.ajaxSetup({
        url:PageUrl.home, // 默认URL
        type: "POST" , // 默认使用POST方式
        beforeSend:function(XMLHttpRequest){
            !!$("[type=submit]") && $("[type=submit]").prop({
                disabled: true
            })
        },
        complete:function(event,request, settings){
            !!$("[type=submit]") && $("[type=submit]").prop({
                disabled: false
            })
        },
        error: function(jqXHR, textStatus, errorMsg) { 
            if(jqXHR.status==401 || jqXHR.responseText == "timeout"){
                window.location.href = "{0}?ru={1}".format(PageUrl.Login, encodeURIComponent(window.location.href));
            } else {
                alert(errorMsg)
            }
            console.log("ajaxWrong",jqXHR,textStatus,errorMsg);
        }
    });
};

Rental.init = function() {
	Rental.validate.init();
    set_ajax();
    Rental.ui.events.initResetForm();
};

/**
* 公用键
*/
Rental.common = {
    key: {
        print_equiment:'print_equiment', //for print equiment    
    }
}

Rental.lang = {
    commonErrorDes:"系统操作，请联系研发部同学",
    commonJsError:"页面脚本开小差了，请联系研发部同学",
    commonSysContact:"如有问题请联系系统管理员",
    error:{
        'J000000':"成功",
    },
};

$.extend(Date.prototype,{
    format:function(fmt){
        var o = {
            "M+" : this.getMonth()+1,                 //月份
            "d+" : this.getDate(),                    //日
            "h+" : this.getHours(),                   //小时
            "m+" : this.getMinutes(),                 //分
            "s+" : this.getSeconds(),                 //秒
            "q+" : Math.floor((this.getMonth()+3)/3), //季度
            "S"  : this.getMilliseconds()             //毫秒
        };
        if(/(y+)/.test(fmt))
            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
        for(var k in o)
            if(new RegExp("("+ k +")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        return fmt;
    }
});

$.extend(String.prototype,{
    format:function(format){
        if (arguments == null || arguments.length == 0) {
            return this;
        }
        var s = this;
        for (var i = 0, len = arguments.length; i < len; i++) {
            var a = arguments[i];
            if(a === 0) {
                a = '0'
            };
            s = s.replaceAll("{" + i.toString() + "}", !!a ? a.toString() : "");
        }
        return s;
    },
    replaceAll:function (source, target, ignoreCase) {
        if (ignoreCase == null) {
            ignoreCase = false;
        }
        source = source.replace(/([\\.$^{[(|)*+?\\\\])/g, "\\$1");
        if (!RegExp.prototype.isPrototypeOf(source)) {
            return this.replace(new RegExp(source, (ignoreCase ? "gi" : "g")), target);
        } else {
            return this.replace(source, target);
        }
    },
    clearSpace:function(){
        return this.replace(/\s+/g, "");
    },
    escapeHTML:function(){
        return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    },
    unescapeHTML:function(){
        return this.stripTags().replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&');
    },
    checkMobile:function(){
        return /^0{0,1}(13[0-9]|15[0-9]|17[0-9]|18[0-9]|14[57])[0-9]{8}$/.test(this);
    },
    formatMobile:function(){
        return this.replace(/\s/g, '').replace(/(\d{3})(\d{4})(?=\d)/g, "$1 $2 ")
    }
});

Rental.helper = {
    buildError:function (errorCode) {
        return Rental.lang.error[errorCode + ''];
    },
    isBlankString:  function (s) {
        return !s || !s.trim();
    },
    checkIsNotNull: function(str){
        if (str == null || str == undefined || typeof str == undefined || str == "null" || str == "undefined" || String(str) == "") {
            return false;
        }
        else {
            return true;
        }
    },
    checkIDCard:function(input) {
        var idNum = input,
            errors = new Array(
                "验证通过" ,
                "身份证号码位数不对" ,
                "身份证含有非法字符" ,
                "身份证号码校验错误" ,
                "身份证地区非法"
            ),
            re, //身份号码位数及格式检验
            len = idNum.length,
            idcard_array = new Array();

        //身份证位数检验
        if (len != 15 && len != 18) {
            return false;
            //return errors[1];
        } else if (len == 15) {
            re = new RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{3})$/);
        } else {
            re = new RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})([0-9xX])$/);
        }
        var area = { 11:"北京" ,12:"天津" ,13:"河北" ,14:"山西" ,
            15:"内蒙古" ,21:"辽宁" ,22:"吉林" ,23:"黑龙江" ,31:"上海" ,
            32:"江苏" ,33:"浙江" ,34:"安徽" ,35:"福建" ,36:"江西" ,
            37:"山东" ,41:"河南" ,42:"湖北" ,43:"湖南" ,44:"广东" ,
            45:"广西" ,46:"海南" ,50:"重庆" ,51:"四川" ,52:"贵州" ,
            53:"云南" ,54:"西藏" ,61:"陕西" ,62:"甘肃" ,63:"青海" ,
            64:"宁夏" ,65:"新疆" ,71:"台湾" ,81:"香港" ,82:"澳门" ,
            91:"国外"
        }

        idcard_array = idNum.split("");
        //地区检验
        if (area[parseInt(idNum.substr(0 ,2))] == null) {
            return false;
            //return errors[4];
        }
        //出生日期正确性检验
        var a = idNum.match(re);
        if (a != null) {
            if (len == 15) {
                var DD = new Date("19" + a[3] + "/" + a[4] + "/" + a[5]);
                var flag = DD.getYear() == a[3] && (DD.getMonth() + 1) == a[4] && DD.getDate() == a[5];
            }
            else if (len == 18) {
                var DD = new Date(a[3] + "/" + a[4] + "/" + a[5]);
                var flag = DD.getFullYear() == a[3] && (DD.getMonth() + 1) == a[4] && DD.getDate() == a[5];
            }
            if (!flag) {
                return false;
                //return "身份证出生日期不对！";
            }
            //检验校验位
            if (len == 18) {
                var S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7
                    + (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9
                    + (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10
                    + (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5
                    + (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8
                    + (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4
                    + (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2
                    + parseInt(idcard_array[7]) * 1
                    + parseInt(idcard_array[8]) * 6
                    + parseInt(idcard_array[9]) * 3;
                var Y = S % 11;
                var M = "F";
                var JYM = "10X98765432";
                var M = JYM.substr(Y ,1); //判断校验位
                //检测ID的校验位
                if (M == idcard_array[17]) {
                    return "1";
                }
                else {
                    return false;
                    //return errors[3];
                }
            }
        } else {
            return false;
            //return errors[2];
        }
        return true;
    },
     //获取URL参数
    getUrlPara:function(key){
        var uri = window.location.search;
        var re = new RegExp("" +key+ "=([^&?]*)", "ig");
        return ((uri.match(re))?(uri.match(re)[0].substr(key.length+1)):null);
    },
    goBack:function(){
        window.history.go(-1);
    },
    max:function(arry){
        return Math.max.apply({},arry); 
    },
    min:function(arry){
        return Math.min.apply({},arry) 
    },
    getKeyCode:function (e) {
        var evt = e || window.event;
        return evt.keyCode || evt.which || evt.charCode;
    },
    boolToStr:function(bool) {
        return bool ? "是":"否";
    },
    timestampFormat:function(timestamp, format) {
        return timestamp && (new Date(timestamp)).format(format || "yyyy-MM-dd hh:mm:ss");
    },
    timestampShortFormat:function(timestamp, format) {
        return timestamp && (new Date(timestamp)).format(format || "yyyy-MM-dd");
    },
    timestampMonthFormat:function(timestamp, format) {
        return timestamp && (new Date(timestamp)).format(format || "yyyy-MM");
    },
    mainImgUrlFormat:function(list) {
        var defaultImg = '{0}{1}'.format(SitePath.staticManagement, 'img/no-img.png');
        if(!list) return defaultImg;
        if(list.length == 0) return  defaultImg;
        var mainImg = list[0];
        return mainImg.imgDomain + mainImg.imgUrl;
    },
    imgListToJSONStringify:function(list, title) {
        if(!list) return "";
        if(list.length == 0) return  "";
        var array = list.map(function(item) {
            return {
                src:item.imgDomain + item.imgUrl,
                title:title
            }
        });
        return JSON.stringify(array);
    },
    randomString:function(len, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz,randomPoz+1);
        }
        return randomString;
    },
    getKeyCode:function (e) {
        var evt = e || window.event;
        return evt.keyCode || evt.which || evt.charCode;
    },
    keyOnlyNumber:function(e){
        var keyCode = this.getKeyCode(e)
        if (keyCode == 48 || keyCode == 96) {
            //if (sender.val() != "") {
            return true;
            //}
        } else {
            if (keyCode == 8 || keyCode == 9 || keyCode == 37 || keyCode == 39) {
                return true;
            } else {
                if (keyCode > 95 && keyCode < 106) {
                    return true;
                } else {
                    if (keyCode > 47 && keyCode < 58) {
                        return true;
                    }
                }
            }
        }
        return false;
    },
    onlyNumber:function (sender) {
        var self = this;
        sender.keydown(function(e) {
            return self.keyOnlyNumber(e);
        });
    },
    onOnlyNumber:function (container, className) {
        var self = this;
        container.on('keydown', className, function(event) {
             var v = self.keyOnlyNumber(event);
             return v;
        });
    },
    onOnlyDecmailNum:function (container, className, decimalLength) {
        var self = this;
        container.on('keydown', className, function(event) {
             var v = self.onlyDecmailNum($(this), decimalLength);
             return v;
        });
    },
    onlyDecmailNum:function(txtBoxObj, decimalLength) {
        var self = this;

        if (txtBoxObj.size == 0) {
            return;
        }

        function getDecimalLength(s) {
            var index = s.indexOf(".");
            if (index < 0) {
                return 0;
            }
            return s.length - index - 1;
        }
        var lastValue = "";
        txtBoxObj.keydown(function(event){
            var val = $.trim(txtBoxObj.val());
            if (getDecimalLength(val) > decimalLength) {
                txtBoxObj.val(lastValue);
                return false;
            }
            lastValue = val;
            var core = self.getKeyCode(event);
            if (core == 190 || core == 110) {
                /*输入小数点*/
                if (lastValue == "" || lastValue.indexOf(".") > -1) {
                    return false;
                }
            } else {
                return self.keyOnlyNumber(event);
            }
            return true;
        });
        txtBoxObj.keyup(function (event) {
            var val = $.trim(txtBoxObj.val());
            if (val != "") {
                try {
                    parseFloat(val);
                    if (getDecimalLength(val) > decimalLength) {
                        txtBoxObj.val(lastValue);
                    }
                }
                catch (e) {
                    txtBoxObj.val(lastValue);
                }
            }
            return true;
        });
    },
    s4:function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    },
    guid:function() {
        return (this.s4()+this.s4()+"-"+this.s4()+"-"+this.s4()+"-"+this.s4()+"-"+this.s4()+this.s4()+this.s4());
    },
    digitUppercase:function(n) {
        var fraction = ['角', '分'];
        var digit = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
        var unit = [
            ['元', '万', '亿'],
            ['', '拾', '佰', '仟'],
          ];
        var num = Math.abs(n);
        var s = '';
        fraction.forEach(function(item, index) {
            s += (digit[Math.floor(num * 10 * (10 * index)) % 10] + item).replace(/零./, '');
        });
        s = s || '整';
        num = Math.floor(num);
        for (var i = 0; i < unit[0].length && num > 0; i += 1) {
            var p = '';
            for (var j = 0; j < unit[1].length && num > 0; j += 1) {
              p = digit[num % 10] + unit[1][j] + p;
              num = Math.floor(num / 10);
            }
            s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
        }
        return s.replace(/(零.)*零元/, '元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整');
    },
    formatMoney:function(s, n) {
        /*
         * 参数说明：
         * s：要格式化的数字
         * n：保留几位小数
         * */
        n = n > 0 && n <= 20 ? n : 2;
        s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
        var l = s.split(".")[0].split("").reverse(),
            r = s.split(".")[1];
        t = "";
        for (i = 0; i < l.length; i++) {
            t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
        }
        return t.split("").reverse().join("") + "." + r;
    },
    fmoney:function(s) {
        return Rental.helper.formatMoney(s, 2);
    }
};

Rental.modal = {
    //src:#addUserModal
    open:function(prams) {
        var options = $.extend({
            removalDelay: 500, 
            src:'',
            type:'inline',
            animation:'mfp-zoomIn',
            midClick:true,
            closeOnBgClick:true,
            parseAjax:function(mfpResponse){
            },
            ajaxContentAdded:function(self) {
            },
            beforeClose: function() {},
            close: function(callBack) {},
            afterClose: function() {},
        }, prams || {} );

        $.magnificPopup.open({
            removalDelay: 500, 
            items: { 
                src: options.src,
                type:options.type,
            },
            callbacks: {
                beforeOpen: function(e) {
                    this.st.mainClass = options.animation;
                },
                parseAjax: function(mfpResponse) {
                    //console.log('Ajax content loaded:', mfpResponse);
                },
                ajaxContentAdded: function() {
                    // Ajax content is loaded and appended to DOM
                    options.ajaxContentAdded(this);
                },
                beforeClose: function() {
                    options.beforeClose(this);
                },
                close: function(callBack) {
                   options.close(this);
                },
                afterClose: function() {
                  options.afterClose(this);
                },
            },
            midClick:options.midClick,
            closeOnBgClick:options.closeOnBgClick,
        });
    },
    close:function() {
        $.magnificPopup.close();
    },
    imgs:function(imgList) {
        if(imgList) {
            $.magnificPopup.open({
                items:imgList,
                gallery: {
                  enabled: true
                },
                type: 'image'
            })
        }
    }
};

Rental.ui =  {
    events:{
        imgGallery:function(container, eventType, className) {
            eventType = eventType || 'click';
            className = className || '.open-img-popup';
            container.on(eventType, className, function(event) {
                event.preventDefault();
                var imgList = $(this).data("imgs");
                Rental.modal.imgs(imgList);
            }); 
        },
        initMonthpicker:function($input, showDefaultTime) {
             $input.monthpicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                // showOn: 'both',
                // buttonText: '<i class="fa fa-calendar-o"></i>',
                showButtonPanel: true,
                beforeShow: function(input, inst) {}
            });

            function setSearchDate(s) {
                if(!s) return;
                s = s.startOf('month');
                $input.val("{0}".format(s.format('YYYY-MM')));
            }

            var self = this, 
                rangepircerPrams =  {
                    format:'YYYY-MM-DD',
                    showDropdowns:true,
                    "linkedCalendars": false,
                    "autoUpdateInput": false,
                };

            if(showDefaultTime) {
                rangepircerPrams.startDate = showDefaultTime;
            }

            setSearchDate(rangepircerPrams.startDate);
        },
        initDatePicker:function($picker, $input) {
            if(!$picker) return;
            if($picker.size() == 0) return;
            var defaultStart = moment().subtract('days', 29), defaultEnd = moment();
            $picker.daterangepicker({
                "singleDatePicker": true,
                "showDropdowns": true,
                "showWeekNumbers": true,
                showDropdowns:true,
                "startDate": defaultEnd,
                "endDate": defaultEnd,
            }, function(start, end, label) {
                // console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
                $input.val(start.format('YYYY-MM-DD'));
            });
        },
        initRangeDatePicker:function($picker, $pickerInput, $start, $end, showDefaultTime, startTime, endTime) {

            function setSearchDate(s, e) {
                if(!s && !e) return;
                //起始时间中午12点开始，到结束时间11：59：59秒结束
                s = s.startOf('day');
                e = e.endOf('day');
                $start.val(s.valueOf());
                $end.val(e.valueOf());
                $pickerInput.val("{0} - {1}".format(s.format('YYYY-MM-DD'), e.format('YYYY-MM-DD')));
            }

            var self = this, 
                rangepircerPrams =  {
                    // "autoApply": true,
                    format:'YYYY-MM-DD',
                    showDropdowns:true,
                    "linkedCalendars": false,
                    "autoUpdateInput": false,
                };

            if(showDefaultTime) {
                rangepircerPrams.startDate = moment().subtract('days',29);
                rangepircerPrams.endDate = moment();
            }

            if(!!startTime && !!endTime) {
                rangepircerPrams.startDate = startTime; //moment().subtract(29,'days');
                rangepircerPrams.endDate = endTime; //moment();
            }

            // console.log(rangepircerPrams)

            $picker.daterangepicker(rangepircerPrams, function(start, end, label) {
                setSearchDate(start, end);
            });

            setSearchDate(rangepircerPrams.startDate, rangepircerPrams.endDate);
        },
        initDropzone:function(dropzoneContainer, url, maxFiles) {
            var self = this;
            dropzoneContainer.dropzone({
                url: SitePath.base + url,
                maxFiles: maxFiles || 20,
                maxFilesize:4,//MB
                acceptedFiles: ".jpg,.jpeg,.png",
                addRemoveLinks: true,
                dictCancelUpload:"取消上传",
                dictRemoveFile:'删除',
                dictFileTooBig:'文件过大（不能大于4M）',
                dictMaxFilesExceeded:"超过最大上传数量",
                success:function(e,response) {
                    Rental.ui.events.dropzoneSuccess(e,response)
                },
                // error:function(e) {
                //     console.log(e)
                // }
            });
        },
        getImageList:function(container, des) {
            var imgList = new Array(), $preview = $('.dz-image-preview', container);
            $preview.each(function(index, el) {
                var img = $(el).data('img');
                if(img) imgList.push({imgId:img.imgId});
            });
            if($preview.size() > 0 && $preview.size() != imgList.length) {
                Rental.notification.error(des || '附件上传', '有未上传成功的图片，请检查');
                return new Array();
            }
            return imgList;
        },
        dropzoneSuccess:function(e,response) {
            var previewElement = $(e.previewElement);
            var progress = $(".dz-progress",previewElement);
            var error = $(".dz-error-mark",previewElement);
            var remove = $('.dz-remove',previewElement);

            progress.css({opacity:'0'});
            remove.html("删除");

            if(response.success){
                var img = response.resultMap.data[0];
                previewElement.data("img",img);
                $("img", previewElement).attr({src:img.imgDomain + img.imgUrl});
            } else {
                error.css({opacity:'1'});
            }

            Rental.ui.events.initViewer()
        },
        initViewer:function() {
            $(".dz-details").each(function(){
				$(this).viewer({
                    navbar:false,
                    show:function() {
                        $(".viewer-prev,.viewer-next").css("display","none")
                    }
                })
            })
        },
        initResetForm:function() {
            $('button[type=reset]').on('click', function(event) {
                event.preventDefault();
                var form = $(this).closest('form');
                var array = form.serializeArray();  
                $.each(array,function(i,item){
                    $("[name="+item.name+"]", form).val("");
                });
            });
        },
        initDropDownMenu:function(container) {
            container.on('hover', '.dropdown-toggle', function(event) {
                event.preventDefault();
                $(this).closest('btn-group').addClass('open');
            });
        }
    },
    multiselect:function(prams) {
        var props = {
            opt:'<option value="{0}">{1}</option>',
            data:prams.data,
            container:prams.container,
            optionsContainer:prams.optionsContainer || prams.container,
            func:prams.func,
            change:prams.change || function() {},
            defaultText:prams.defaultText || '全部',
            selectClass:prams.selectClass || 'multiselect dropdown-toggle btn btn-sm btn-default light mr10',
            nonSelectedText:prams.nonSelectedText || " ",
            nSelectedText:prams.nSelectedText || " ",
            allSelectedText:prams.allSelectedText || " ",
            showDefaultText:_.isBoolean(prams.showDefaultText) ? prams.showDefaultText : true,
        };
        var opts = new Array();
        (props.data || []).forEach(function(item) {
            var opt = props.func(props.opt, item);
            !!opt && opts.push(opt);
        });

        props.showDefaultText && opts.unshift(props.opt.format('', props.defaultText));
        props.optionsContainer.html(opts.join(''));

        props.container.multiselect({ 
            buttonClass: props.selectClass, 
            nonSelectedText: props.nonSelectedText,
            nSelectedText: props.nSelectedText,
            allSelectedText: props.allSelectedText,
        }).change(function(event) {
                var val = $(this).val();
                props.change(val);
            });
    },
    renderSelect:function(prams) {
        var props = {
            opt:'<option value="{0}">{1}</option>',
            data:prams.data,
            container:prams.container,
            func:prams.func || function() {},
            change:prams.change || function() {},
            defaultText:prams.defaultText || '请选择',
        };
        // var opts = (props.data || []).map(function(item) {
        //     return props.func(props.opt, item);
        // });
        var opts = new Array();
        (props.data || []).forEach(function(item) {
            var opt = props.func(props.opt, item);
            !!opt && opts.push(opt);
        });
        opts.unshift(props.opt.format('', props.defaultText));
        props.container.html(opts.join('')).change(function(event) {
                            var val = $(this).val();
                            props.change(val);
                        });

        var defaultvalue = props.container.data('defaultvalue');
        if(defaultvalue) {
            props.container.val(defaultvalue);
        }
    },
    renderEnumSelect:function(container, data, defaultText, changeFunc, renderOptFunc) {
        Rental.ui.multiselect({
            container:container,
            data:data,
            func:renderOptFunc || function(opt, item) {
                return opt.format(item.num, item.value);
            },
            change:changeFunc ||function(val) {},
            defaultText:defaultText
        });
    },
    renderFormData:function($form, data) {
        var array = $form.serializeArray();  
            $.each(array,function(i,item) {
            if(data.hasOwnProperty(item.name)) {
                var dom = $form.find("[name="+item.name+"]");
                dom.val(data[item.name]);
                dom.data('defaultvalue', data[item.name]);
            }
        });
    },
    renderFormByData:function($form, data) {
        if(!data) return;
        var keys = _.keys(data);
        _.isArray(keys) && keys.forEach(function(item) {
            var dom = $form.find("[name="+item+"]"),
                value = data[item];
            if(dom.data('type') == 'date') {
                value = _.isNumber(value) ? new Date(value).format(dom.data('format')):'';
            }
            dom.val(value);
            dom.data('defaultvalue', value);
        });
    },
    loadingDialog:function(msg) {
        msg = msg || '处理中...'
        return bootbox.dialog({
                message: '<p class="text-center"><i class="fa fa-spin fa-spinner mr5"></i>'+msg+'</p>',
                closeButton: false
        });
    },
};
/**
* render mixin
**/
Rental.render = {
    boolText:function() {
        return function(bool, render) {
            return render(bool) && parseInt(render(bool)) == 1 ? '是' : '否';
        }
    },
    price:function() {
        return function(amount, render) {
            return render(amount) ? Rental.helper.fmoney(parseFloat(render(amount))) : '0.00';
        }
    },
    time:function() {
        return function(time, render) {
            return render(time) ? Rental.helper.timestampFormat(parseInt(render(time))) : '';
        }
    },
    shortTime:function() {
        return function(time, render) {
            return render(time) ? Rental.helper.timestampShortFormat(parseInt(render(time))) : '';
        }  
    },
    clockTime:function() {
        return function(time, render) {
            return render(time) ? '<i class="fa fa-clock-o mr5"></i>' + Rental.helper.timestampFormat(parseInt(render(time))) : '';
        }  
    },
    clockShorTime:function() {
        return function(time, render) {
            return render(time) ? '<i class="fa fa-clock-o mr5"></i>' + Rental.helper.timestampShortFormat(parseInt(render(time))) : '';
        }  
    },
    clockMonthTime:function(){
        return function(time, render) {
            return render(time) ? Rental.helper.timestampMonthFormat(parseInt(render(time))) : '';
        }
    },
    rowKey:function() {
        return (new Date).getTime();//parseInt(Math.random()*(100+1),10); //0-100随机数
    },
    rowGuid:function() {
        return Rental.helper.guid();
    },
    /**
    * 配件值大小换算（后端返回都是M单位，前端换成成G/T）
    */
    memorysize:function() {
        return function(val, render) {
            var val = render(val);
            if(val) {
                var g =  parseInt(val) / 1024;
                var t = parseInt(val) / 1024 / 1024;
                if(t >= 1) {
                    return [t, "T"].join(' ');
                }
                return [g, "G"].join(' ');
            }
            return "";
        }  
    },
    emailIcon:function() {
        return function(email, render) {
            return render(email) ? '<i class="fa envelope-o mr5"></i>' + render(email) : '';
        }  
    },
    isNewValue:function() {
        return function(isNew, render) {
            return render(isNew) && parseInt(render(isNew)) == 1 ? '全新' : '次新';
        }  
    },
    isNewValueBadge:function() {
        return function(isNew, render) {
            var badge = '';
            if(render(isNew)) {
                switch(parseInt(render(isNew))) {
                    case 0:
                        badge = '<span class="badge mr5 badge-default">次新</span>';
                        break;
                    case 1:
                        badge = '<span class="badge mr5 badge-primary">全新</span>';
                        break;
                    default:
                        badge = '';
                        break;
                }
            }
            return badge;
        }  
    }
}

