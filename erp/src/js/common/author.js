var AuthorCode = {
	
	home:200286, //首页	
	home_data_analysis:200287, //数据统计
	home_business_workbench:200315, //商务工作台
	home_sales_workbench:200316, //业务工作台

	manage_user:200006, // 用户管理 
	manage_user_list:200007, // 用户管理-用户列表 
	manage_user_list_add:200013, // 用户管理-添加用户
	manage_user_list_eidt:200014, // 用户管理-编辑用户
	manage_user_list_view:200015, // 用户管理-查看用户
	manage_user_list_uppassword:200016, // 用户管理-修改密码
	manage_user_list_access_data:200023, // 用户管理-用户列表-数据授权

	manage_role_list:200017, // 用户管理-角色列表 
	manage_role_list_add:200018, // 用户管理-角色列表-添加角色
	manage_role_list_eidt:200019, // 用户管理-角色列表-编辑角色
	manage_role_list_delete:200020, // 用户管理-角色列表-查看角色
	manage_role_list_access_function:200021, // 用户管理-角色列表-功能授权
	manage_role_list_access_data:200022, // 用户管理-角色列表-数据授权

	manage_department:200076, //用户管理-组织架构
	manage_department_add:200077, //用户管理-组织架构-添加部门
	manage_department_edit:200078, //用户管理-组织架构-编辑部门
	manage_department_delete:200079, //用户管理-组织架构-删除部门

	manage_order:200008, //订单管理
	manage_order_list:200009, //订单管理-列表
	manage_order_detail:200010, //订单管理-详细页面
	manage_order_add:200011, //订单管理-添加
	manage_order_edit:200012, //订单管理-编辑
	manage_order_submit:200084, //订单管理-提交订单
	manage_order_confirm_receipt:200085, //订单管理-确定收货
	manage_order_confirm_receipt_new:200279, //订单管理-新确定收货
	manage_order_cancel:200086, //订单管理-取消订单
	manage_order_picking:200087, //订单管理-配货
	manage_order_deliver_goods:200088, //订单管理-发货
	manage_order_pay:200099, //订单管理-支付
	manage_order_print:200117, //订单管理-打印交货单
	manage_order_pick_print:200334, //订单管理-打印备货单
	manage_order_watie_for_delivery_list:200129, //订单管理-待发货列表
	manage_order_strong_cancel:200227, //订单管理-强制取消
	manage_order_again:200232, //订单管理-再来一单
	manage_order_add_remark:200266, //订单管理-添加备注
	manage_order_add_waite_for_audit:200268, //订单管理-待审核
	manage_order_relet:200270, //订单管理-订单-续租
	manage_order_recalculation:200274, //订单管理-订单-重算结算单
	manage_order_relet_view:200281, //订单管理-订单-续租-查看续租单
	manage_order_relet_commit:200282, //订单管理-订单-续租-提交续租单
	manage_order_relet_edit:200283, //订单管理-订单-续租-编辑续租单
	manage_order_relet_cancel:200288, //订单管理-订单-续租-取消续租单
	manage_order_relet_recalculation:200299, //订单管理-订单-续租-重算续租单
	manage_order_statement_change:200293, //订单管理-订单-修改结算日
	manage_order_statement_split:200294, //订单管理-订单-分段结算
	manage_order_detail_showK3No:200323, //订单管理-订单详情-显示k3编码
	manage_order_edit_order_item_price:200325, //订单管理-修改订单商品项单价
	manage_order_for_undergraduate_list:200324, //订单管理-大学生订单列表

	manage_product:200001, //商品管理
	manage_product_list:200002, //商品管理-列表
	manage_product_detail:200003, //商品管理-详细页面
	manage_product_add:200004, //商品管理-添加
	manage_product_edit:200005, //商品管理-编辑
	manage_product_stock:200336, //商品管理-库存
	manage_product_confirm_stock:200337, //商品管理-确认库存
	manage_product_query_stock:200338, //商品管理-查询库存
	manage_product_equipment_list:200024, //商品管理-设备列表
	manage_product_sku_list:200019, //商品管理-SKU列表
	manage_product_sku_list_instorage:200020, //商品管理-SKU入库
	manage_product_property_list:200340, //商品管理-属性列表
	manage_product_property_detail:200341, //商品管理-属性详情
	manage_product_property_edit:200342, //商品管理-属性值修改
	manage_product_property_add:200343, //商品管理-属性值添加
	
	manage_customer:200065, //客户管理

	manage_business_customer_list:200071, //企业客户管理-列表
	manage_business_customer_detail:200072, //企业客户管理-详细页面
	manage_business_customer_add:200073, //企业客户管理-添加
	manage_business_customer_edit:200074, //企业客户管理-编辑
	manage_business_customer_delete:200075, //企业客户管理-删除
	manage_business_customer_risk:200081, //企业客户管理-风控授信
	manage_business_customer_address_add:200083, //企业客户管理-添加收货地址
	manage_business_customer_manual_charge:200121, //企业客户管理-加款
	manage_business_customer_manual_deduct:200122, //企业客户管理-扣款
	manage_business_customer_audit:200131, //企业客户管理-审核
	manage_business_customer_submitAudit:200130, //企业客户管理-提交审核
	manage_business_customer_disabled:200134, //企业客户管理-禁用
	manage_business_customer_settlement_date:200171,//企业客户管理-结算时间设置
	manage_business_customer_short_rental_upper_limit:200172,//企业客户管理-短租上限金额设置
	manage_business_customer_set_risk_credit_amount_used:200225,//企业客户管理-设置客户已使用授信额度
	manage_business_customer_return_visit_record_add:200249,//企业客户管理-添加回访记录
	manage_business_customer_view_disabled:200276,//企业客户管理-查看禁用客户
	manage_business_customer_statement_order_confirm:200289,//企业客户管理-结算单数据确认
	manage_business_customer_statement_of_account_export:200301,//企业客户管理-导出对账单
	manage_business_customer_confirm_baddebt:200303,//企业客户管理-确认坏账
	manage_business_customer_edit_salesman:200312,//企业客户管理-编辑业务员&联合开发人
	manage_business_customer_add_subCompany:200346,//企业客户管理-添加子公司

	manage_customer_list:200066, //个人客户管理-列表
	manage_customer_detail:200067, //个人客户管理-详细页面
	manage_customer_add:200068, //个人客户管理-添加
	manage_customer_edit:200069, //个人客户管理-编辑
	manage_customer_delete:200070, //个人客户管理-删除
	manage_customer_risk:200080, //个人客户管理-风控授信
	manage_customer_address_add:200082, //个人客户管理-添加收货地址
	manage_customer_manual_charge:200123, //个人客户管理-加款
	manage_customer_manual_deduct:200124, //个人客户管理-扣款
	manage_customer_audit:200133, //个人客户管理-审核
	manage_customer_submitAudit:200132, //个人客户管理-提交审核
	manage_customer_disbaled:200135, //个人客户管理-禁用
	manage_customer_settlement_date:200169,//个人客户管理-结算时间设置
	manage_customer_short_rental_upper_limit:200170,//个人客户管理-短租上限金额设置
	manage_customer_manage_set_risk_credit_amount_used:200226,//设置客户已使用授信额度
	manage_customer_view_disabled:200277,//查看禁用客户
	manage_customer_statement_order_confirm:200290,//-结算单数据确认
	manage_customer_statement_of_account_export:200302,//个人客户管理-导出对账单
	manage_customer_confirm_baddebt:200304,//个人客户管理-确认坏账
	manage_customer_edit_salesman:200313,//个人客户管理-编辑业务员&联合开发人

	manage_customer_list_for400:200305, //个人客户-400客户查询列表
	manage_customer_list_detail_for400:200307, //个人客户-400客户详情
	manage_business_customer_list_for400:200306, //个人客户-400客户查询列表
	manage_business_customer_detail_list_for400:200308, //个人客户-400客户详情

	manage_purchase:200025, //采购管理
	manage_purchase_list:200026, //采购管理-列表
	manage_purchase_list_add:200027, //采购管理-列表-添加
	manage_purchase_list_edit:200028, //采购管理-列表-编辑
	manage_purchase_list_detail:200029, //采购管理-列表-详细
	manage_purchase_list_submit_audit:200030, //采购管理-列表-提交审核
	manage_purchase_list_delete:200054, //采购管理-列表-删除
	manage_purchase_list_continue_to_purchase:200055, //采购管理-列表-继续采购
	manage_purchase_list_end_purchase:200056, //采购管理-列表-结束采购
	manage_purchase_list_compel_delte:200119, //采购管理-列表-强制取消

	manage_purchase_receive:200049, //收货单列表
	manage_purchase_receive_detail:200050, //收货单列表-详细
	manage_purchase_receive_edit:200051, //收货单列表-编辑
	manage_purchase_receive_confirm:200089, //收货单列表-确认收货
	manage_purchase_receive_detail_edit_product_equipemnt_price:200115, //收货单列表-详细-编辑设备价格
	manage_purchase_receive_detial_edit_product_material_price:200116, //收货单列表-详细-编辑配件价格
	manage_purchase_receive_commit:200120, //收货单列表-提交收货单

	manage_purchase_delivery:200052, //采购发货单
	manage_purchase_delivery_detail:200053, //采购发货单-详细

	manage_warehouse:200036,  //仓库管理
	manage_warehouse_list:200037,  //仓库管理-列表
	manage_warehouse_list_detail:200038,  //仓库管理-列表-详细
	manage_warehouse_list_add:200039,  //仓库管理-列表-添加
	manage_warehouse_list_edit:200040,  //仓库管理-列表-编辑
	manage_warehouse_list_delete:200042,  //仓库管理-列表-删除

	manage_supplier:200031,  //供应商管理
	manage_supplier_list:200032,  //供应商管理-列表
	manage_supplier_list_detail:200033,  //供应商管理-列表-详细
	manage_supplier_list_add:200034,  //供应商管理-列表-添加
	manage_supplier_list_edit:200035,  //供应商管理-列表-编辑
	manage_supplier_list_delete:200041,  //供应商管理-列表-删除

	manage_maetrial:200043, //配件管理
	manage_maetrial_list:200044, //配件管理-列表
	manage_maetrial_list_detail:200045, //配件管理-列表-详细
	manage_maetrial_list_add:200046, //配件管理-列表-添加
	manage_maetrial_list_edit:200047, //配件管理-列表-编辑
	manage_maetrial_list_delete:200048, //配件管理-列表-删除

	manage_material_modal:200061, //配件管理-型号
	manage_material_modal_add:200062, //配件管理-添加
	manage_material_modal_edit:200063, //配件管理-编辑
	manage_material_modal_delete:200064, //配件管理-删除
	manage_bulk_material_list:200100, //配件管理-散料列表

	manage_audit:200057,  //审核管理
	manage_audit_list:200058,  //审核管理-工作流
	manage_audit_detail:200059,  //审核管理-工作流-详细
	manage_audit_do_audit:200060,  //审核管理-工作流-审核

	manage_renter_order:200090,  //退货单管理
	manage_renter_order_list:200091,  //退货单管理-列表
	manage_renter_order_detail:200092,  //退货单管理-详细
	manage_renter_order_add:200093,  //退货单管理-添加
	manage_renter_order_edit:200094,  //退货单管理-编辑
	manage_renter_order_equipment_return:200095,  //退货单管理-退还设备
	manage_renter_order_material_return:200096,  //退货单管理-退还配件
	manage_renter_order_end:200097,  //退货单管理-结束退货单
	manage_renter_order_cancel:200098,  //退货单管理-取消退货单
	manage_renter_order_submit:200101,  //退货单管理-提交退货单

	manage_deployment_order:200102, //调拨单管理
	manage_deployment_order_list:200103, //调拨单管理-列表
	manage_deployment_order_add:200104, //调拨单管理-添加
	manage_deployment_order_edit:200105, //调拨单管理-编辑
	manage_deployment_order_detail:200106, //调拨单管理-详细
	manage_deployment_order_submit:200107, //调拨单管理-提交
	manage_deployment_order_stock_up:200108, //调拨单管理-备货
	manage_deployment_order_deliver_goods:200109, //调拨单管理-发货
	manage_deployment_order_end:200110, //调拨单管理-结束
	manage_deployment_order_cancel:200111, //调拨单管理-取消

	manage_statement_order:200112, //结算单管理
	manage_statement_order_list:200113, //结算单管理-结算单
	manage_statement_order_pay:200114, //结算单管理-结算单-支付
	manage_statement_order_create_correct_order:200197, //结算单管理-结算单-创建冲正单
	manage_statement_order_detail:200118, //结算单管理-结算单-详细
	manage_monthly_statement_order_list:200192, //结算单管理-对账单
	manage_monthly_statement_order_detail:200193, //结算单管理-对账单-详细
	manage_statement_correct_order_List:200198, //结算单管理-冲正单
	manage_statement_correct_order_detail:200199, //结算单管理-冲正单-详情
	manage_statement_correct_order_submit:200200, //结算单管理-冲正单-提交
	manage_statement_correct_order_cancel:200201, //结算单管理-冲正单-取消
	manage_statement_order_user_coupon:200267, //结算单管理-冲正单-使用优惠卷
	manage_statement_order_export:200273, //结算单管理-结算单-导出
	manage_statement_order_list_export:200280, //结算单管理-结算单-财务导出
	manage_statement_order_pary_orderItem:200314, //结算单管理-结算单-支付结算单项

	manage_transfer_order:200136, //流转单管理
	manage_transfer_order_in_list:200137, //流转单管理-流入单
	manage_transfer_order_in_detail:200138, //流转单管理-流入单-详情
	manage_transfer_order_in_add:200139, //流转单管理-流入单-添加
	manage_transfer_order_in_edit:200140, //流转单管理-流入单-编辑
	manage_transfer_order_in_cancel:200141, //流转单管理-流入单-取消
	manage_transfer_order_in_submit:200142, //流转单管理-流入单-提交
	manage_transfer_order_in_end:200143, //流转单管理-流入单-结束

	manage_transfer_order_out_list:200144, //流转单管理-流出单
	manage_transfer_order_out_detail:200145, //流转单管理-流出单-详情
	manage_transfer_order_out_add:200146, //流转单管理-流出单-添加
	manage_transfer_order_out_edit:200147, //流转单管理-流出单-编辑
	manage_transfer_order_out_cancel:200148, //流转单管理-流出单-取消
	manage_transfer_order_out_submit:200149, //流转单管理-流出单-提交
	manage_transfer_order_out_stock:200150, //流转单管理-流出单-备货
	manage_transfer_order_out_clear_stock:200151, //流转单管理-流出单-清货
	manage_transfer_order_out_end:200152, //流转单管理-流出单-结束

	manage_peer_supplier_list:200153, //同行-列表
	manage_peer_supplier_detail:200154, //同行-详情
	manage_peer_supplier_add:200155, //同行-添加
	manage_peer_supplier_edit:200156, //同行-编辑

	manage_peer_order_list:200157,  //同行调拨单-列表
	manage_peer_order_detail:200158,  //同行调拨单-形象
	manage_peer_order_add:200159,  //同行调拨单-添加
	manage_peer_order_edit:200160,  //同行调拨单-编辑
	manage_peer_order_cancel:200161,  //同行调拨单-取消
	manage_peer_order_submit:200162,  //同行调拨单-提交
	manage_peer_order_confirm:200163,  //同行调拨单-确认
	manage_peer_order_return:200164,  //同行调拨单-归还
	manage_peer_order_confirm_return:200165,  //同行调拨单-确认退货

	manage_statistics:200166, //统计管理
	manage_statistics_income_list:200167, //统计管理-收入统计
	manage_statistics_detail_unreceiceable_list:200178, //统计管理-未收明细
	manage_statistics_total_unreceiceable_list:200179, //统计管理-未收汇总
	manage_statistics_total_awaitreceive_list:200261, //统计管理-代收明细
	manage_statistics_total_awaitreceivesummary_list:200262, //统计管理-代收汇总
	manage_statistics_salesman_deduct_list:200269, //统计管理-业务员提成

	manage_recharge_list:200168,

	manage_message:200173, //消息管理
	manage_message_site:200174, //站内信
	manage_message_site_inbox:200175, //站内信 - 收件箱
	manage_message_site_outbox:200176, //站内信 - 发件箱
	manage_message_site_sedmessage:200177, //站内信 - 发送消息

	manage_change_order:200180, //换货单
	manage_change_order_list:200181, //换货单-列表
	manage_change_order_detail:200182, //换货单-列表-详情
	manage_change_order_add:200183, //换货单-列表-添加
	manage_change_order_edit:200184, //换货单-列表-编辑
	manage_change_order_submit:200185, //换货单-列表-提交
	manage_change_order_stock:200186, //换货单-列表-备货
	manage_change_order_send:200187, //换货单-列表-发货
	manage_change_order_complete:200188, //换货单-列表-成功
	manage_change_order_cancel:200189, //换货单-列表-取消
	manage_change_order_changed_equiment_in_storage:200190, //换货单-列表-更换设备入库
	manage_change_order_un_change_equiment_in_storage:200191, //换货单-列表-未更换设备入库
	manage_change_order_add_remark:200194, //换货单-列表-添加备注
	manage_change_order_update_diff_price:200195, //换货单-列表-修改差价
	manage_change_order_complete_instorage:200196, //换货单-列表-完成验货入库

	manage_k3_manage:200202, //k3数据管理
	manage_k3_manage_order_list:200203, //k3数据管理-订单
	manage_k3_manage_order_detail:200204, //k3数据管理-订单-详情
	
	manage_k3_manage_return_order:200206, //k3数据管理-退货单
	manage_k3_manage_create_return_order:200205, //k3数据管理-退货单-创建退货单
	manage_k3_manage_return_order_detial:200207, //k3数据管理-退货单-详情
	manage_k3_manage_return_order_add_product:200208, //k3数据管理-退货单-添加商品
	manage_k3_manage_return_order_delete_product:200209, //k3数据管理-退货单-删除退货商品
	manage_k3_manage_return_order_send:200210, //k3数据管理-退货单-发送退货单到K3
	manage_k3_manage_return_order_edit:200211, //k3数据管理-退货单-编辑
	manage_k3_manage_return_order_cancel:200221, //k3数据管理-退货单-取消
	manage_k3_manage_return_order_submit:200222, //k3数据管理-退货单-提交
	manage_k3_manage_return_order_strong_cancel:200259, //k3数据管理-退货单-强制取消
	manage_k3_manage_return_order_print:200271, //k3数据管理-退货单-打印
	manage_k3_manage_return_order_create_statement:200275, //k3数据管理-退货单-生成结算单
	manage_k3_manage_return_order_recalculationt:200300, //k3数据管理-退货单-重算退货单
	manage_k3_manage_return_order_rollback:200339, //k3数据管理-退货单-退货单回滚

	manage_k3_manage_change_order:200214, //k3数据管理-换货单
	manage_k3_manage_create_change_order:200216, //k3数据管理-换货单-创建换货单
	manage_k3_manage_change_order_detial:200215, //k3数据管理-换货单-详情
	manage_k3_manage_change_order_add_product:200218, //k3数据管理-换货单-添加商品
	manage_k3_manage_change_order_delete_product:200219, //k3数据管理-换货单-删除换货商品
	manage_k3_manage_change_order_send:200220, //k3数据管理-换货单-发送换货单到K3
	manage_k3_manage_change_order_edit:200217, //k3数据管理-换货单-编辑
	manage_k3_manage_change_order_cancel:200223, //k3数据管理-换货单-取消
	manage_k3_manage_change_order_submit:200224, //k3数据管理-换货单-提交

	manage_company_list:200212, //分公司列表
	manage_company_add_short_receivable_amount:200213, //分公司列表-设置短租应收上限

	manage_grouped_product:200228, //商品管理-组合商品列表
	manage_grouped_product_detail:200229, //商品管理-组合商品详情
	manage_grouped_product_add:200230, //商品管理-组合商品添加
	manage_grouped_product_edit:200231, //商品管理-组合商品编辑
	manage_grouped_product_del:200233, //商品管理-组合商品删除

	manage_financeial:200236, //财务管理
	manage_financeial_jurnal_amount_attachment_list:200237, //财务管理-资金流水附件
	manage_financeial_jurnal_amount_attachment_list_pushdowm:200238, //财务管理-资金流水附件-下推
	manage_financeial_jurnal_amount_attachment_list_confirm:200239, //财务管理-资金流水附件-确认
	manage_financeial_jurnal_amount_attachment_list_upload:200240, //财务管理-资金流水附件-上传
	manage_financeial_jurnal_amount_attachment_list_view:200241, //财务管理-资金流水附件-查看
	manage_financeial_jurnal_amount_attachment_list_delete:200246, //财务管理-资金流水附件-删除
	manage_financeial_jurnal_amount_list:200242, //财务管理-资金流水
	manage_financeial_jurnal_amount_list_claim:200243, //财务管理-资金流水-认领
	manage_financeial_jurnal_amount_list_ignore:200244, //财务管理-资金流水-忽略
	manage_financeial_jurnal_amount_list_detail:200245, //财务管理-资金流水-查看
	manage_financeial_jurnal_amount_list_confirm:200278, //财务管理-资金流水-确认
	manage_financeial_jurnal_amount_list_hide:200247, //财务管理-资金流水-隐藏
	manage_financeial_jurnal_amount_list_show:200248, //财务管理-资金流水-显示
	manage_financeial_jurnal_amount_list_confirm_affiliation:200264, //财务管理-资金流水-确认归属
	manage_financeial_jurnal_amount_list_cancel_affiliation:200265, //财务管理-资金流水-取消归属
	manage_financeial_jurnal_amount_export:200272, //财务管理-资金流水-导出
	manage_financeial_jurnal_amount_confirm:200278, //财务管理-资金流水-确认
	manage_financeial_jurnal_amount_gopay:200292, //财务管理-资金流水-前往支付
	manage_financeial_jurnal_amount_pushdown:200351, //财务管理-资金流水-下推公海
	manage_financeial_jurnal_amount_claim_list:200344, //财务管理-流水认领明细
	manage_financeial_statistics:200322, //财务管理-财务周报统计
	manage_financeial_jurnal_highseas_list:200347, //财务管理-流水公海
	manage_financeial_jurnal_highseas_view:200349, //财务管理-流水公海-查看
	manage_financeial_jurnal_highseas_claim:200350, //财务管理-流水公海-认领
	manage_financeial_jurnal_highseas_detail:200348, //财务管理-流水公海-详情
	manage_financeial_jurnal_highseas_gopay:200352, //财务管理-流水公海-前往支付

	manage_activities:200250, //活动管理
	manage_activities_coupon_group_list:200251, //活动管理 - 批次优惠券
	manage_activities_coupon_group_detail:200252, //活动管理 - 批次优惠券 - 详情
	manage_activities_coupon_group_add:200253, //活动管理 - 批次优惠券 - 添加
	manage_activities_coupon_group_edit:200254, //活动管理 - 批次优惠券 - 编辑
	manage_activities_coupon_group_detele:200255, //活动管理 - 批次优惠券 - 删除
	manage_activities_coupon_group_to_add:200256, //活动管理 - 批次优惠券 - 批次增券
	manage_activities_coupon_group_cancellation:200263, //活动管理 - 批次优惠券 - 作废
	// manage_activities_coupon_group_to_add:200256, //活动管理 - 批次优惠券 - 批次增券
	manage_activities_coupon_card:200257, //活动管理 - 优惠券
	manage_activities_coupon_abolish:200258, //活动管理 - 优惠券 - 作废
	manage_activities_coupon_give_out:200260, //活动管理 - 优惠券 - 发放优惠券
	manage_activities_coupon_give_out:200260, //活动管理 - 优惠券 - 发放优惠券

	manage_data_analysis:200284, //数据分析 
	manage_data_analysis_dynamic_sql:200285, //数据分析 - sql查询
	manage_data_analysis_sql_list:200295, //数据分析 - sql语句列表
	manage_data_analysis_sql_list_add:200296, //数据分析 - sql语句列表-新增
	manage_data_analysis_sql_list_del:200297, //数据分析 - sql语句列表-删除
	manage_data_analysis_sql_list_search:200298, //数据分析 - sql语句列表-查询
	manage_data_analysis_sql_list_update:200333, //数据分析 - sql语句列表-更新
	manage_data_analysis_sql_list_edit:200335, //数据分析 - sql语句列表-编辑

	manage_data_analysis_execute_list:200309, //数据分析 - sql执行情况列表
	manage_data_analysis_execute_list_pass_and_execute:200310, //数据分析 - sql执行情况列表-通过并执行
	manage_data_analysis_execute_list_reject:200311, //数据分析 - sql执行情况列表-拒绝执行

	manage_notice_list:200317, //系统管理-系统公告列表
	manage_notice_list_detail:200319, //系统管理-系统公告列表-详情
	manage_notice_list_add:200318, //系统管理-系统公告列表-添加
	manage_notice_list_edit:200320, //系统管理-系统公告列表-编辑
	manage_notice_list_delete:200321, //系统管理-系统公告列表-删除

	manage_task_list:200326, //系统管理-任务列表
	manage_task_list_add:200327, //系统管理-任务列表-添加
	manage_task_list_edit:200329, //系统管理-任务列表-编辑
	manage_task_list_update_executor:200328, //系统管理-任务列表-更新执行者信息
	manage_task_list_delete:200330, //系统管理-任务列表-删除
	manage_task_list_pause:200331, //系统管理-任务列表-暂停
	manage_task_list_restart:200332, //系统管理-任务列表-重启

	manage_online_user_list:200345, //系统管理-在线人数列表
};

var AuthorCodeIcon = {
	200006:"glyphicons glyphicons-user_add", //用户管理
	200008:"glyphicons glyphicons-coins", //订单管理
	200001:"glyphicons glyphicons-shopping_bag", //商品管理
	200065:"fa fa-group", //客户管理
	200025:"glyphicon glyphicon-shopping-cart", //客户管理
	200031:'fa fa-cube',  //供应商管理
	200036:'fa fa-cubes',  //仓库管理
	200043:'glyphicons glyphicons-tags',  //配件管理
	200057:'fa fa-check-square-o',  //审核管理
	200090:"glyphicons glyphicons-coins", //退货单管理
	200102:"glyphicons glyphicons-coins", //调拨单管理
	200112:"glyphicons glyphicons-coins", //结算单管理
	200136:"glyphicons glyphicons-coins", //流转单管理
	200166:"glyphicons glyphicons-charts", //流转单管理
	200173:"fa fa-envelope-o", //流转单管理
	200180:"glyphicons glyphicons-coins", //流转单管理
	200202:"glyphicons glyphicons-coins", //流转单管理
	200236:"glyphicons glyphicons-coins", //财务管理
	200250:"glyphicons glyphicons-coins", //财务管理
	200284:"glyphicons glyphicons-coins", //财务管理
};

var AuthorUtil = {
	button:function(author,styleClass,iclass,text,isShow, isBlank) {
		if(!author) return null;
		return {
				menuName:author.menuName,
				menuUrl:author.menuUrl,
				class:styleClass,
				iClass:iclass,
				text:text,
				show: typeof(isShow) === "boolean" ? isShow :  true,	
				blank: typeof(isBlank) === "boolean" ? isBlank : false,
			}
	}
};