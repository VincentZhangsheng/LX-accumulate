;(function($){

	function Pager(){
		this.defautPrams = {
			currentPage:1,
			pageSize:15,
			pageCount:0,
			totalCount:0,
			showPageCount:5,  //当前显示页码个数
		};
		this.init = function(prams,doSearchFunc,$container) {
			this.initDom(prams,$container);
			this.render();
			this.initEvent(doSearchFunc);
		};
		this.initDom = function(prams,$container) {
			this.pagerData = this.resolvePageData($.extend(this.defautPrams,prams));
			this.$pagerContainer = $container || $(".pagerContainer");
			this.$pagerTpl = $("#pagerTpl").html();
			this.inputPage = $("[name='inputPage']", this.$pagerContainer);
		};
		this.initEvent = function(doSearchFunc) {
			var self = this;

			this.$pagerContainer.off('click','button').on('click', 'button', function(event) {
				event.preventDefault();
				var pageNo = $(this).data("pageno");
				doSearchFunc && doSearchFunc(pageNo);
			});
			this.$pagerContainer.off('change', '[name=inputPage]').on('change', '[name=inputPage]', function(event) {
				event.preventDefault();
				var pageNo = $.trim($(this).val());
				self.gotoPage(pageNo, doSearchFunc);
			});
			this.$pagerContainer.off('keydown', '[name=inputPage]').on('keydown', '[name=inputPage]', function(event) {
				return Rental.helper.keyOnlyNumber(event);
			});
			this.$pagerContainer.off('click', '.goToPage').on('click', '.goToPage', function(event) {
				event.preventDefault();
				var pageNo = $.trim(self.inputPage.val());
				self.gotoPage(pageNo, doSearchFunc);
			});
		};
		this.gotoPage = function(pageNo, doSearchFunc) {
			if(parseInt(pageNo) > this.pagerData.pageCount) {
				pageNo = this.pagerData.pageCount;
			} else if(parseInt(pageNo) < 1) {
				pageNo = 1;
			}
			doSearchFunc && doSearchFunc(pageNo);
		};
		this.resolvePageData = function(data){
			var start, end, pageArr = [];
			if(data.pageCount <= data.showPageCount){
	            start = 1;
	            end = data.pageCount;
	        } else if((parseInt(data.showPageCount/2) + data.currentPage) >= data.pageCount){
	            start = data.pageCount - data.showPageCount +1;
	            end = data.pageCount;
	        }else if((data.currentPage - parseInt(data.showPageCount/2))>0 && (data.currentPage + parseInt(data.showPageCount/2)) < data.pageCount){
	            start = data.currentPage - parseInt(data.showPageCount/2);
	            end = data.currentPage + parseInt(data.showPageCount/2);
	        }else{
	            start = 1;
	            end = data.showPageCount;
	        }
	        for(var i=start;i<=end;i++){
	            pageArr.push({pageNo:i,choosed:i==data.currentPage});
	        }
	        data['pageArr'] = pageArr;
	        return data;
		};
		this.render = function(){
			Mustache.parse(this.$pagerTpl);
			this.$pagerContainer.html(Mustache.render(this.$pagerTpl,{
				"pager":this.pagerData,
				"preSatatus":function() {
					return this.currentPage == 1 ? "disabled":"";
				},
				preno:function() {
					return this.currentPage-1;
				},
				"nextStatus":function(){
					return this.currentPage == this.pageCount ? "disabled":"";
				},
				nextno:function() {
					return this.currentPage + 1;
				},
			}));
		}
	};

	window.Pager = Pager;

})(jQuery);