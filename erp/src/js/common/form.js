Rental.form = {
	initFormValidation:function($form,callBack) {
		var _this = this;
		$form.validate({
            
            /* @validation states + elements 
            ------------------------------------------- */
            
            errorClass: "state-error has-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation highlighting + error placement  
            ---------------------------------------------------- */ 
            
            highlight: function(element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function(error, element) {
               if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
               } else {
                    error.insertAfter(element.parent());
               }
            },
            submitHandler: function (form) {
                if(callBack) {
                	callBack (form)
                } else {
                	_this.submit(form);
                }
            }
                        
        }); 
	},
    initSearchFormValidation:function($form,callBack) {
        var _this = this;
        $form.validate({
            
            /* @validation states + elements 
            ------------------------------------------- */
            
            errorClass: "has-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation highlighting + error placement  
            ---------------------------------------------------- */ 
            
            highlight: function(element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass); //.addClass(validClass);
            },
            errorPlacement: function(error, element) {
               if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
               } else {
                    error.insertAfter(element.parent());
               }
            },
            submitHandler: function (form) {
                if(callBack) {
                    callBack (form)
                } else {
                    _this.submit(form);
                }
            }
                        
        }); 
    },
	getFormData: function($form) {
        var data = $form.serializeArray();
        var object = new Object();

        $.each(data,function(i,n){
            var _this = $('[name='+ n.name +']', $form);
            if(_this.hasClass('nopram') == false) {
                if(_this.data('type') == 'optgroup') {
                    object[n.name] = _this.val();
                    if(_.isArray(object[n.name])) {
                        object[n.name] = object[n.name].filter(function(optItemValue) {
                            return !!optItemValue;
                        });
                    } 
                    if(object[n.name].length == 0)  object[n.name] = null;

                } else {
                    object[n.name] = $.trim(n.value).escapeHTML();
                    if(_this.data('type') == 'date') {
                        object[n.name] = new Date(object[n.name]).getTime();
                    }
                }
                
            }
        });
        return object;
    },
	submit:function(form) {
		var $form = $(form);
		var data = this.getFormData($form);
		this.submitAction(form, data);
	},
	submitAction:function(form,data) {
		var $form = $(form);
		$.ajax({
            type: form.method || 'POST',
            url: $form.attr("action"),
            data: JSON.stringify(data),
            contentType:'application/json',
            dataType: "json",
            cache: false,
            success: function(response) {
                Rental.ajax.ajaxDone(response,$form.attr("callback-func"),$form.attr("description"));
            },
            error: function(xhr, ajaxOptions, thrownError) {
               Rental.ajax.ajaxError(xhr, ajaxOptions, thrownError,$form.attr("description"));
            }
        });
	},
    setFiled:function($form,name,value){
        $("input[name="+name+"]",$form).val(value);
    }
}
