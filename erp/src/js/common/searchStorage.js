Rental.searchStorage = {
	enum:{
		businessCustomer:'searchStorage_BusinessCustomer', //企业客户列表
		customer:'searchStorage_customer', //个人客户列表
		order:'searchStorage_order', //订单列表
		orderWaiteForDelivery:'searchStorage_orderWaiteForDelivery',//待发货订单
		orderToAudit:'searchStorage_orderToAudit', //订单列表
		incomeList:'searchStorage_incomeList', //统计
		materialList:'searchStorage_materialList', //配件列表
		productList:'searchStorage_productList', //商品列表
		purchaseList:'searchStorage_purchaseList',//采购单列表
		purchaseReceive:'searchStorage_purchaseReceive',//收货单
		purchaseDelivery:'searchStorage_purchaseDelivery',//发货单
		deploymentOrder:'searchStorage_deploymentOrder',//调拨单
		deploymentPeerOrder:'searchStorage_deploymentPeerOrder',//同行调拨单
		orderTransferIn:'searchStorage_orderTransferIn',//流入单
		orderTransferOut:'searchStorage_orderTransferOut',//流出单
		returnOrder:'searchStorage_returnOrder',//退货单
		changeOrder:'searchStorage_changeOrder',//换货单
		statementMonthlyOrder:'searchStorage_statementMonthlyOrder',//对账单
		statementCorrectOrder:'searchStorage_statementCorrectOrder',//冲正单
		statementOrder:'searchStorage_statementOrder',//结算单
		auditList:'searchStorage_auditList',//工作流
		supplierList:'searchStorage_supplierList',//供应商
		supplierPeer:'searchStorage_supplierPeer',//同行
		groupedProductList:'searchStorage_groupedProductList',//组合商品
		jurnalUploadList:'manage_financeial_jurnal_amount_attachment_list_pushdowm',
		jurnalAmountDetailList:'searchStorage_jurnalAmountDetailList',//资金流水明细列表
		couponBatchList: 'manage_activities_coupon_group_list', //批次列表
		couponCardList: 'manage_activities_coupon_card', //优惠券列表
		awaitReceiveList: 'manage_statistics_total_awaitreceive_list', // 统计管理-待收明细
		awaitReceiveListSummary: 'manage_statistics_total_awaitreceivesummary_list', // //统计管理-代收汇总
		k3Order:'searchStorage_k3Order',//订单
		k3ReturnOrder:'searchStorage_k3ReturnOrder',//退货单
		propertyList:'searchStorage_propertyList'
	},
	clearAll:function() {
		var self = this, keys = _.keys(Rental.searchStorage.enum);
		keys.forEach(function(item){
			self.clear(Rental.searchStorage.enum[item])
		});
	},
	clear:function(key) {
		sessionStorage.removeItem(key);
	},
	set:function(key, value) {
		sessionStorage.setItem(key, JSON.stringify(value));
	},
	get:function(key) {
		var value = sessionStorage.getItem(key);
		return !!value ? JSON.parse(value) : null;
	}
}