Rental.localstorage = {
	const:{
		currentUser:"current_user",
		authorMenu:"author_menu",
		mapAuthor:"map_author",
		materialType:"materialType_info",
		mapMaterialType:"map_materialType_info",
		areaData:'area_data'
	},
	clearAll:function() {
		this.removeUser();
		this.removeAutor();
		this.removeMapAuthor();
		this.materialType.remove();
		this.materialType.removeMap();
		this.area.remove();
	},
	setUser:function(prams) {
		localStorage.setItem(this.const.currentUser,JSON.stringify(prams));
	},
	getUser:function() {
		var user = localStorage.getItem(this.const.currentUser);
		if(!user) {
			user = this.ajaxCurrentUser();
			this.setUser(user);
			return user;
		}
		return JSON.parse(user);
	},
	removeUser:function() {
		localStorage.removeItem(this.const.currentUser);
	},
	ajaxCurrentUser:function(){
		var user = null;
		$.ajax({
            type: 'POST',
            url: SitePath.service + '/user/getCurrentUser',
            contentType:'application/json',
            dataType: "json",
            async: false,
            cache: false,
            success: function(response) {
                if(response.success){
                	user = response.resultMap.data;
                } else {
                	Rental.notification.error("加载用户信息失败",response.description);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
               Rental.ajax.ajaxError(xhr, ajaxOptions, thrownError);
            }
        });
        return user;
	},
	setAutor:function(prams) {
		localStorage.setItem(this.const.authorMenu,JSON.stringify(prams));
	},
	getAuthor:function() {
		var author = localStorage.getItem(this.const.authorMenu);
		if(!author) {
			author = this.ajaxAutor();
			author && this.setAutor(author);
			return author;
		}
		return JSON.parse(author);
	},
	ajaxAutor:function() {
		var author = null;
		$.ajax({
            type: 'POST',
            url: SitePath.service + '/menu/getMenu',
            contentType:'application/json',
            dataType: "json",
            async: false,
            cache: false,
            success: function(response) {
                if(response.success){
                	author = response.resultMap.data;
                } else {
                	Rental.notification.error("加载菜单失败",response.description);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
               Rental.ajax.ajaxError(xhr, ajaxOptions, thrownError);
            }
        });
        return author;
	},
	removeAutor:function() {
		localStorage.removeItem(Rental.localstorage.const.authorMenu);
	},
	setMapAuthor:function(prams) {
		localStorage.setItem(Rental.localstorage.const.mapAuthor,JSON.stringify(prams));
	},
	getMapAuthor:function() {
		var author = localStorage.getItem(this.const.mapAuthor);
		if(!author) {
			author = this.getAuthor();
			author = this.resolveAuthorToMap(author);
			this.setMapAuthor(author);
			return author;
		}
		return JSON.parse(author);
	},
	resolveAuthorToMap:function(prams) {
		function func(currentAuthor,resoleAuthor) {
			for(var i in currentAuthor) { 
				resoleAuthor[currentAuthor[i].menuId] = $.extend({},currentAuthor[i]);
				if(currentAuthor[i].hasOwnProperty('children')) {
					resoleAuthor[currentAuthor[i].menuId].children = new Object();
					func(currentAuthor[i].children,resoleAuthor[currentAuthor[i].menuId].children);
				}
			}
		}
		var mapAuthor = new Object();
		func(prams,mapAuthor);
		
		return mapAuthor;
	},
	removeMapAuthor:function(){
		localStorage.removeItem(this.const.mapAuthor);
	},
	materialType:{
		ajaxMaterialType:function() {
			var res = null;
			$.ajax({
	            type: 'POST',
	            data:JSON.stringify({
	            	pageNo:1,
					pageSize:10000,
	            }),
	            url: SitePath.service + 'material/queryType',
	            contentType:'application/json',
	            dataType: "json",
	            async: false,
	            cache: false,
	            success: function(response) {
	                if(response.success){
	                	res = response.resultMap.data.itemList;
	                } else {
	                	Rental.notification.error("加载配件类型", response.description);
	                }
	            },
	            error: function(xhr, ajaxOptions, thrownError) {
	               Rental.ajax.ajaxError(xhr, ajaxOptions, thrownError);
	            }
	        });
	        return res;
		},
		set:function(prams) {
			localStorage.setItem(Rental.localstorage.const.materialType, JSON.stringify(prams));
		},
		get:function() {
			var data = localStorage.getItem(Rental.localstorage.const.materialType);
			if(!data) {
				data = this.ajaxMaterialType();
				data && this.set(data);
				return data;
			}
			return JSON.parse(data);
		},
		remove:function() {
			localStorage.removeItem(Rental.localstorage.const.materialType);
		},
		setMap:function(prams) {
			localStorage.setItem(Rental.localstorage.const.mapMaterialType,JSON.stringify(prams));
		},
		getMap:function() {
			var data = localStorage.getItem(Rental.localstorage.const.mapMaterialType);
			if(!data) {
				data = this.get();
				data = _.indexBy(data, 'materialTypeId');
				this.setMap(data);
				return data;
			}
			return JSON.parse(data);
		},
		removeMap:function(){
			localStorage.removeItem(Rental.localstorage.const.mapMaterialType);
		},
	},
	area:{
		set:function(prams) {
			localStorage.setItem(Rental.localstorage.const.areaData, JSON.stringify(prams));
		},
		get:function() {
			var data = localStorage.getItem(Rental.localstorage.const.areaData);
			return !data ? null : JSON.parse(data);
		},
		remove:function() {
			localStorage.removeItem(Rental.localstorage.const.areaData);
		},
	}
}