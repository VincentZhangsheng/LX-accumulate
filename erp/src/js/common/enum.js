;(function($){
	Enum = {
		array:function(enum, selectval) {
			var array = new Array();
			for (var key in enum.num) {
				array.push({
					num:enum.num[key],
					value:enum.getValue(enum.num[key]),
					selected:enum.num[key] == selectval, 
				}); 
			}
			return array;
		},
		arrayByEnumObj:function(enum, obj, selectval) {
			var array = new Array();
			for (var key in obj) {
				array.push({
					num:obj[key],
					value:enum.getValue(obj[key]),
					selected:obj[key] == selectval, 
				}); 
			}
			return array;
		},
		orderTimeAxisStatus:{
			num:{
				unSubmit:0,
				inReview:4,
				waitForDelivery:8,
				inHand:12,
				delivered:16,
				confirmReceipt:20,
				partRestitution:22,
				completeRestitution:24,
				cancel:28,
				end:32,
				paid:36,
				reject:40,
				closed:48
			},
			array:function() {
				var array = new Array();
				for (var key in this.num) {
					array.push({
						num:this.num[key],
						value:this.getValue(this.num[key])
					}); 
				}
				return array;
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.waitForDelivery:
						return "待发货";
					case this.num.inHand:
						return "处理中";
					case this.num.delivered:
						return "已发货";
					case this.num.confirmReceipt:
						return "租赁中";
					case this.num.partRestitution:
						return "部分归还";
					case this.num.completeRestitution:
						return "已归还";
					case this.num.cancel:
						return "已取消";
					case this.num.end:
						return "已结束";
					case this.num.paid:
						return "已付款";
					case this.num.reject:
						return "已拒绝";
					case this.num.closed:
						return "已关闭";
					
				}
				return "";
			},
		},
		//订单状态
		orderStatus:{
			num:{
				unSubmit:0,
				inReview:4,
				waitForDelivery:8,
				inHand:12,
				delivered:16,
				confirmReceipt:20,
				partRestitution:22,
				completeRestitution:24,
				cancel:28,
				end:32,
				// paid:36,
				reject:40,
				close:48
			},
			array:function() {
				var array = new Array();
				for (var key in this.num) {
					array.push({
						num:this.num[key],
						value:this.getValue(this.num[key])
					}); 
				}
				return array;
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.waitForDelivery:
						return "待发货";
					case this.num.inHand:
						return "处理中";
					case this.num.delivered:
						return "已发货";
					case this.num.confirmReceipt:
						return "租赁中";
					case this.num.partRestitution:
						return "部分归还";
					case this.num.completeRestitution:
						return "已归还";
					case this.num.cancel:
						return "已取消";
					case this.num.end:
						return "已结束";
					// case this.num.paid:
					// 	return "已付款";
					case this.num.reject:
						return "已拒绝";
					case this.num.close:
						return "已关闭";
					
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.waitForDelivery:
						return "text-info";
					case this.num.inHand:
						return "text-alert";
					case this.num.delivered:
						return "text-success";
					case this.num.confirmReceipt:
						return "text-success";
					case this.num.completeRestitution:
						return "text-system";
					case this.num.cancel:
						return "text-muted ";
					case this.num.end:
						return "text-muted";
					case this.num.reject:
						return "text-muted";
				}
				return "";
			},
		},
		//订单操作类型
		orderOperationType:{
			num:{
				createOrder:1,
				editOrder:2,
				submitAudit:3,
				passAudit:4,
				reject:5,
				delivered:6,
				confirmReceipt:7,
				return:8,
				cancel:9,
				strongCancel:10,
				pay:11,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.createOrder:
						return "创建";
					case this.num.editOrder:
						return "修改";
					case this.num.submitAudit:
						return "提交审核";
					case this.num.passAudit:
						return "审核通过";
					case this.num.reject:
						return "审核拒绝";
					case this.num.delivered:
						return "发货";
					case this.num.confirmReceipt:
						return "确认收货";
					case this.num.return:
						return "退货";
					case this.num.cancel:
						return "取消订单";
					case this.num.strongCancel:
						return "强制取消订单";
					case this.num.pay:
						return "结算支付";
					
				}
				return "";
			},
		},
		/**
		* 订单商品退还是否逾期
		*/
		isReturnOverDue:{			
			num:{
				overDue:1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.overDue:
						return "到期未处理";
				}
				return "";
			},
		},
		/**
		* 退货单状态
		*/
		returnOrderStatus:{			
			num:{
				unSubmit:0,
				inReview:4,
				forPickup:8,
				inHand:12,
				cancel:16,
				completed:20,
				reject:24,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.forPickup:
						return "待取货";
					case this.num.inHand:
						return "处理中";
					case this.num.cancel:
						return "已取消";
					case this.num.completed:
						return "已完成";
					case this.num.reject:
						return "已驳回";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.forPickup:
						return "text-success";
					case this.num.inHand:
						return "text-system";
					case this.num.cancel:
						return "text-muted";
					case this.num.completed:
						return "text-muted";
					case this.num.reject:
						return "text-danger";
				}
			}
		},
		/**
		* 支付订单状态 
		*/
		payStatus:{
			num:{
				nopay:0,
				partPaid:4,
				paid:8,
				refunded:16,
				overdue:20,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.nopay:
						return "未支付";
					case this.num.partPaid:
						return "部分支付";
					case this.num.paid:
						return "已支付";
					case this.num.refunded:
						return "已退款";
					case this.num.overdue:
						return "逾期";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.nopay:
						return "text-info";
					case this.num.partPaid:
						return "text-system";
					case this.num.paid:
						return "text-success";
					case this.num.refunded:
						return "text-warning";
					case this.num.overdue:
						return "text-danger";
				}
				return "";
			}
		},
		/**
		* 调拨单状态
		*/
		deploymentOrderStatus:{			
			num:{
				unSubmit:0,
				inReview:4,
				inHand:8,
				delivered:12,
				completed:16,
				cancel:20,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.inHand:
						return "处理中";
					case this.num.delivered:
						return "已发货";
					case this.num.completed:
						return "已完成";
					case this.num.cancel:
						return "已取消";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.inHand:
						return "text-system";
					case this.num.delivered:
						return "text-success";
					case this.num.completed:
						return "text-muted";
					case this.num.cancel:
						return "text-muted";
				}
				return "";
			},
		},
		/**
		* 调拨类型
		*/
		deploymentType:{			
			num:{
				borrow:1,
				sell:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.borrow:
						return "借调";
					case this.num.sell:
						return "售调";
				}
				return "";
			},
		},
		//身份认证状态
		verifyStatus:{
			num:{
				nocommit:0,
				commited:1,
				verified:2,
				reject:3,
				cancel:4,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.nocommit:
						return "待认证";
					case this.num.commited:
						return "认证中";
					case this.num.verified:
						return "已认证";
					case this.num.reject:
						return "认证失败";
					case this.num.cancel:
						return "已取消";
				}
				return "";
			},
		},
		//租赁方式
		rentType:{
			num:{
				// byNum:1,
				byDay:1,
				byMonth:2,
			},
			getValue:function(num) {
				switch(num) {
					// case this.num.byNum:
					// 	return "按次数";
					case this.num.byDay:
						return "按天租";
					case this.num.byMonth:
						return "按月租";
				}
				return "";
			},
			getUnit:function(num) {
				switch(num) {
					// case this.num.byNum:
					// 	return "按次数";
					case this.num.byDay:
						return "天";
					case this.num.byMonth:
						return "个月";
				}
				return "";
			}
		},
		//付款方式
		payMode:{
			num:{
				useAfaterPay:1,
				useBeforePay:2,
			},
			numForOrder:{
				useAfaterPay:1,
				useBeforePay:2,
				thirtyPercent:3,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.useAfaterPay:
						return "先付后用";
					case this.num.useBeforePay:
						return "先用后付";
					case this.numForOrder.thirtyPercent:
						return "首付30%";
				}
				return "";
			}
		},
		//用户类型
		customerType:{
			num:{
				business:1,
				personal:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.business:
						return "企业用户";
					case this.num.personal:
						return "个人用户";
				}
				return "";
			},
			getUrl:function(num, no) {
				switch(num) {
					case this.num.business:
						return '{0}?no={1}'.format(PageUrl.companyCustomerDetail, no);
					case this.num.personal:
						return '{0}?no={1}'.format(PageUrl.customerDetail, no);
					default:
						return '';
				}
			}
		},
		//属性类型
		propertyType:{
			num:{
				sku:1,
				product:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.sku:
						return "销售属性";
					case this.num.product:
						return "商品属性";
				}
				return "";
			}
		},
		//设备状态
		equipmentStatus:{
			num:{
				leisure:1,
				renting:2,
				maintenanceing:3,
				scrap:4,
				transfers:5,
				flowMoveing:6,
				flowMoveOut:7,
				returning:8,
				hasReturn:9,
			},
			array:function() {
				var array = new Array();
				for (var key in this.num) {
					array.push({
						num:this.num[key],
						value:this.getValue(this.num[key])
					}); 
				}
				return array;
			},
			getValue:function(num) {
				switch(num) {
					case this.num.leisure:
						return "空闲中";
					case this.num.renting:
						return "租赁中";
					case this.num.maintenanceing:
						return "维修中";
					case this.num.scrap:
						return "已报废";
					case this.num.transfers:
						return "调拨中";
					case this.num.flowMoveing:
						return "流转中";
					case this.num.flowMoveOut:
						return "流出";
					case this.num.returning:
						return "归还中";
					case this.num.hasReturn:
						return "已归还";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.leisure:
						return "text-success";
					case this.num.renting:
						return "text-muted";
					case this.num.maintenanceing:
						return "text-warning";
					case this.num.scrap:
						return "text-danger";
				}
				return "";
			}
		},
		//权限按钮类型
		roleButtonType:{
			num:{
				general:2, //普通按钮
				batch:3, //批量按钮
				row:4, //行内按钮
				batchOrRow:5, //批量行内按钮
			}
		},
		//采购单状态
		purchaseOrderStatus:{
			num:{
				unSubmit:0,
				inReview:3,
				purchasing:6,
				partPurchase:9,
				allPurchase:12,
				finish:15,
				cancel:18,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.purchasing:
						return "采购中";
					case this.num.partPurchase:
						return "部分采购";
					case this.num.allPurchase:
						return "全部采购";
					case this.num.finish:
						return "已结束";
					case this.num.cancel:
						return "已取消";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-primary";
					case this.num.inReview:
						return "text-info";
					case this.num.purchasing:
						return "text-system";
					case this.num.partPurchase:
						return "text-info";
					case this.num.allPurchase:
						return "text-success";
					case this.num.finish:
						return "text-muted";
					case this.num.cancel:
						return "text-muted";
				}
				return "";
			}
		},
		/**
		* 收货单状态
		*/
		purchaseReceiveOrderStatus:{
			num:{
				unConfirm:0,
				confirmed:1,
				commited:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unConfirm:
						return "待收货";
					case this.num.confirmed:
						return "已签单";
					case this.num.commited:
						return "已提交";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unConfirm:
						return "text-primary";
					case this.num.confirmed:
						return "text-success";
					case this.num.commited:
						return "text-muted";
				}
				return "";
			}
		},
		/**
		* 分拨状态
		*/
		autoAllotStatus:{
			num:{
				unAllot:0,
				allotTo:1,
				beAllot:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unAllot:
						return "未分拨";
					case this.num.allotTo:
						return "已分拨";
					case this.num.beAllot:
						return "被分拨";
				}
				return "";
			}
		},
		//工作流类型
		workflowType:{
			num:{
				purchase:1, 
				order:2,
				deploymentOrder:3,
				changeOrder:4,
				returnOrder:5,
				repairOrder:6,
				transferInOrder:9,
				transferOutOrder:10,
				peerBorrowIn:11,
				peerReturn:12,
				correctOrder:13,
				k3ChangeOrder:14,
				k3ReturnOrder:15,
				customer:16,
				customerConsignInfo:17,
				channelBigCustomer:18,
				reletOrder:19,
				// customerOwnerByTelSeller:17, //电销客户审批
			},
			getValue:function(num) {
				switch(num) {
					case this.num.purchase:
						return "采购单";
					case this.num.order:
						return "订单";
					case this.num.deploymentOrder:
						return "调拨单";
					case this.num.changeOrder:
						return "换货单";
					case this.num.returnOrder:
						return "退货单";
					case this.num.repairOrder:
						return "维修单";
					case this.num.transferInOrder:
						return "流入单";
					case this.num.transferOutOrder:
						return "流出单";
					case this.num.peerBorrowIn:
						return "同行调拨借入";
					case this.num.peerReturn:
						return "同行调拨归还";
					case this.num.correctOrder:
						return "冲正单";
					case this.num.k3ChangeOrder:
						return "K3换货单";
					case this.num.k3ReturnOrder:
						return "K3退货单";
					case this.num.customer:
						return "客户";
					case this.num.customerConsignInfo:
						return "客户收货地址";
					case this.num.channelBigCustomer:
						return "渠道大客户";
					case this.num.reletOrder:
						return "续租单";
					default:
						return '';
				}
			},
			getUrl:function(num, no) {
				switch(num) {
					case this.num.purchase:
						return '{0}?no={1}'.format(PageUrl.purchaseDetail, no);
					case this.num.order:
						return '{0}?no={1}'.format(PageUrl.orderDetail, no);
					case this.num.deploymentOrder:
						return '{0}?no={1}'.format(PageUrl.deplymentOrderDetail, no);
					// case this.num.changeOrder:
					// 	return '{0}?no={1}'.format(PageUrl.changeOrderDetail, no);
					case this.num.returnOrder:
						return '{0}?no={1}'.format(PageUrl.returnOrderDetail, no);
					// case this.num.repairOrder:
					// 	return '{0}?no={1}'.format(PageUrl.repairOrderDetail, no);
					case this.num.transferInOrder:
						return '{0}?no={1}'.format(PageUrl.transferInOrderDetail, no);
					case this.num.transferOutOrder:
						return '{0}?no={1}'.format(PageUrl.transferOutOrderDetail, no);
					case this.num.peerBorrowIn:
					case this.num.peerReturn:
						return '{0}?no={1}'.format(PageUrl.peerOrderDetail, no);
					case this.num.correctOrder:
						return '{0}?no={1}'.format(PageUrl.statementCorrectOrderDetail, no);
					case this.num.k3ChangeOrder:
						return '{0}?no={1}'.format(PageUrl.k3ChangeOrderDetail, no);
					case this.num.k3ReturnOrder:
						return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, no);
					case this.num.customer:
					case this.num.channelBigCustomer:
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, no);		
					case this.num.customerConsignInfo:
						return '{0}?id={1}'.format(PageUrl.commonCustomerDetailByConsignInfoId, no);
					case this.num.reletOrder:
						return '{0}?no={1}'.format(PageUrl.reletDetail, no);	
					default:
						return '';
				}
			}
		},
		workflowVerifyStatus:{
			num:{
				unSubmit:0, 
				inReview:1,
				pass:2,
				reject:3,
				cancel:4, 
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.pass:
						return "通过";
					case this.num.reject:
						return "驳回";
					case this.num.cancel:
						return "取消";
					default:
						return '';
				}
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "primary";
					case this.num.inReview:
						return "info";
					case this.num.pass:
						return "success";
					case this.num.reject:
						return "danger";
					case this.num.cancel:
						return "muted";
					default:
						return '';
				}
			}
		},
		//仓库类型
		warehouseType:{
			num:{
				default:1,
				shipment:2, 
			},
			getValue:function(num) {
				switch(num) {
					case this.num.default:
						return "默认仓";
					case this.num.shipment:
						return "出货仓";
					default:
						return '';
				}
			}
		},
		//公司类型
		companyType:{
			num:{
				head:1,
				filiale:2, 
				supplier:2, 
			},
			getValue:function(num) {
				switch(num) {
					case this.num.head:
						return "总公司";
					case this.num.filiale:
						return "分公司";
					case this.num.supplier:
						return "供应商";
					default:
						return '';
				}
			}
		},
		//配件类型
		materialType:{
			num:{
				ram:1,
				mainboard:2, 
				cpu:3,
				hdd:4, 
				graphics:5, 
				power:6, 
				radiator:7, 
				ssd:8, 
				catia:9, 
				other:100, 
			},
			getValue:function(num) {
				var mapMaterialType = Rental.localstorage.materialType.getMap();
				return mapMaterialType &&  mapMaterialType[num] ? mapMaterialType[num].materialTypeName : '';
				
				// switch(num) {
				// 	case this.num.ram:
				// 		return "内存";
				// 	case this.num.mainboard:
				// 		return "主板";
				// 	case this.num.cpu:
				// 		return "CPU";
				// 	case this.num.hdd:
				// 		return "机械硬盘";
				// 	case this.num.graphics:
				// 		return "显卡";
				// 	case this.num.power:
				// 		return "电源";
				// 	case this.num.radiator:
				// 		return "散热器";
				// 	case this.num.ssd:
				// 		return "固态硬盘";
				// 	case this.num.catia:
				// 		return "机箱";
				// 	case this.num.other:
				// 		return "其它";
				// 	default:
				// 		return '';
				// }
			}
		},
		//类别类型
		categoryType:{
			num:{
				product:1,
				material:2, 
			},
			getValue:function(num) {
				switch(num) {
					case this.num.product:
						return "商品";
					case this.num.material:
						return "配件";
					default:
						return '';
				}
			}
		},
		//采购类型
		purchaseType:{
			num:{
				bigMountings:1,
				smallMountings:2, 
			},
			getValue:function(num) {
				switch(num) {
					case this.num.bigMountings:
						return "整机大四件";
					case this.num.smallMountings:
						return "小配件";
					default:
						return '';
				}
			}
		},
		/**
		* 散料状态 
		*/
		bulkMaterialStatus:{
			num:{
				leisure:1,
				renting:2,
				maintenanceing:3,
				scrap:4,
				transfers:5,
				flowMoveing:6,
				flowMoveOut:7,
				returning:8,
				hasReturn:9,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.leisure:
						return "空闲中";
					case this.num.renting:
						return "租赁中";
					case this.num.maintenanceing:
						return "维修中";
					case this.num.scrap:
						return "已报废";
					case this.num.transfers:
						return "调拨中";
					case this.num.flowMoveing:
						return "流转中";
					case this.num.flowMoveOut:
						return "流出";
					case this.num.returning:
						return "归还中";
					case this.num.hasReturn:
						return "已归还";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.leisure:
						return "text-success";
					case this.num.renting:
						return "text-muted";
					case this.num.maintenanceing:
						return "text-warning";
					case this.num.scrap:
						return "text-danger";
					case this.num.transfers:
						return "text-alert";
				}
				return "";
			}
		},
		operationType:{
			num:{
				stock:1,
				remove:3,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.stock:
						return "装配";
					case this.num.remove:
						return "移除";
				}
				return "";
			}
		},
		/**
		* 结算单状态
		*/
		statementOrderStatus:{
			num:{
				init:0,
				part:4,
				completed:8,
				noSttlement:16,
				hasCorrect:20,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.init:
						return "未支付";
					case this.num.part:
						return "部分支付";
					case this.num.completed:
						return "完成支付";
					case this.num.noSttlement:
						return "无需结算";
					case this.num.hasCorrect:
						return "已冲正";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.init:
						return "text-primary";
					case this.num.part:
						return "text-info";
					case this.num.completed:
						return "text-muted";
					case this.num.noSttlement:
						return "text-muted";
				}
				return "";
			}
		},
		/**
		* 单子类型
		*/
		orderType:{
			num:{
				// purchase:1, 
				order:1,
				deploymentOrder:2,
				changeOrder:3,
				returnOrder:4,
				repairOrder:5,
			},
			getValue:function(num) {
				switch(num) {
					// case this.num.purchase:
					// 	return "采购单";
					case this.num.order:
						return "订单";
					case this.num.deploymentOrder:
						return "调拨单";
					case this.num.changeOrder:
						return "换货单";
					case this.num.returnOrder:
						return "退货单";
					case this.num.repairOrder:
						return "维修单";
					default:
						return '';
				}
			},
			getUrl:function(num, no) {
				switch(num) {
					case this.num.order:
						return '{0}?no={1}'.format(PageUrl.orderDetail, no);
					case this.num.deploymentOrder:
						return "#";
					case this.num.changeOrder:
						return "#";
					case this.num.returnOrder:
						return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, no);
					case this.num.repairOrder:
						return "#";
					default:
						return '';
				}
			}
		},
		/**
		* 送货方式
		*/
		deliveryMode:{
			num:{
				express:1,
				pickup:2,
				lxExpress:3,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.express:
						return "快递";
					case this.num.pickup:
						return "自提";
					case this.num.lxExpress:
						return "凌雄配送";
					default:
						return '';
				}
			},
			withOutLX:function(selectval) {
				return [
					{
						num:this.num.express,
						value:this.getValue(this.num.express),
						selected:this.num.express == selectval, 
					},
					{
						num:this.num.pickup,
						value:this.getValue(this.num.pickup),
						selected:this.num.pickup == selectval, 
					}
				]
			}
		},
		/*
		* 客户来源
		*/
		customerOrigin:{
			num:{
				siteActivities:1,
				expo:2,
				businessContact:3,
				baidu:4,
				fromFriend:5,
				jd:7,
				other:6,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.siteActivities:
						return "地推活动";
					case this.num.expo:
						return "展会";
					case this.num.businessContact:
						return "业务联系";
					case this.num.baidu:
						return "百度推广";
					case this.num.fromFriend:
						return "朋友推荐";
					case this.num.jd:
						return "京东";
					case this.num.other:
						return "其它";
					default:
						return '';
				}
			}
		},
		/*
		* 客户状态
		*/
		customerStatus:{
			num:{
				unSubmit:0,
				commited:1,
				pass:2,
				reject:3,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "未提交";
					case this.num.commited:
						return "审核中";
					case this.num.pass:
						return "审核通过";
					case this.num.reject:
						return "驳回";
					default:
						return '';
				}
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-primary";
					case this.num.commited:
						return "text-info";
					case this.num.pass:
						return "text-success";
					case this.num.reject:
						return "text-danger";
					default:
						return '';
				}
			}
		},
		/*
		* 是否授信
		*/
		riskStatus:{
			num:{
				unrisk:0,
				risk:1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unrisk:
						return "未授信";
					case this.num.risk:
						return "已授信";
					default:
						return '';
				}
			}
		},
		/*
		* 是否为坏账客户
		*/
		badDebtStatus:{
			num:{
				notBadDebt:0,
				badDebt:1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.notBadDebt:
						return "否";
					case this.num.badDebt:
						return "是";
					default:
						return '';
				}
			}
		},
		/*
		* 客户所属行业
		*/
		customerIndustiry:{
			num:{
				jiSuanjiYingJian:1,
				huLianWangDianZiShangWu:2,
				networkVideoLiveMarketing:3,
				tongXunDianZi:4,
				p2p:5,
				bank:6,
				jiaoYuPeiXun:7,
				zhengFuShiyeFeiYingliJiGou:8,
				meiTiYingShiWenHuaChuanMei:9,
				hunShaSheYing:10,
				fuWuYiLiao:11,
				zhuanYeFuWu:12,
				zhanLanHuiYi:13,
				fangDiChan:14,
				jiaJuJianZhu:15,
				JiaoTongYunShu:16,
				weiXiuJiaZheng:17,
				jiaGongZhiZhao:18,
				kuaiShuXiaoFei:19,
				other:99,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.jiSuanjiYingJian:
						return "计算机软硬件(互联网推广_APP开发等)";
					case this.num.huLianWangDianZiShangWu:
						return "互联网电子商务";
					case this.num.networkVideoLiveMarketing:
						return "网络游戏_视频直播_微信营销";
					case this.num.tongXunDianZi:
						return "通信_电信_电子产品";
					case this.num.p2p:
						return "互联网金融_贷款_期货现货贵金属_分期支付_担保拍卖";
					case this.num.bank:
						return "实业投资_保险_证券_银行";
					case this.num.jiaoYuPeiXun:
						return "教育_培训";
					case this.num.zhengFuShiyeFeiYingliJiGou:
						return "政府_公共事业_非盈利性机构";
					case this.num.meiTiYingShiWenHuaChuanMei:
						return "媒体_出版_影视_文化传播";
					case this.num.hunShaSheYing:
						return "婚纱摄影_旅游度假_酒店餐饮";
					case this.num.fuWuYiLiao:
						return "服务娱乐_医疗美容";
					case this.num.zhuanYeFuWu:
						return "专业服务(租赁服务_企业注册_人力资源_贸易报关_中介咨询_实体广告等)";
					case this.num.zhanLanHuiYi:
						return "展览会议_公关活动";
					case this.num.fangDiChan:
						return "房地产_建筑建设_开发";
					case this.num.jiaJuJianZhu:
						return "家居建材_装饰设计";
					case this.num.JiaoTongYunShu:
						return "交通运输_物流仓储_供应链";
					case this.num.weiXiuJiaZheng:
						return "维修安装_家政_叫车服务";
					case this.num.jiaGongZhiZhao:
						return "加工制造_工业自动化_汽车摩托车销售";
					case this.num.kuaiShuXiaoFei:
						return "快速消费品(服饰日化_食品烟酒等)";
					case this.num.other:
						return "其它";
					default:
						return '';
				}
			}
		},
		/*
		* 流转单状态
		*/
		transferOrderStatus:{
			num:{
				unSubmit:0,
				stockUp:2,
				inReview:4,
				success:8,
				cancel:16,
				end:20,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "未提交";
					case this.num.stockUp:
						return "备货中";
					case this.num.inReview:
						return "审核中";
					case this.num.success:
						return "已成功";
					case this.num.cancel:
						return "已取消";
					case this.num.end:
						return "已结束";
					default:
						return '';
				}
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-primary";
					case this.num.stockUp:
						return "text-alert";
					case this.num.inReview:
						return "text-info";
					case this.num.success:
						return "text-success";
					case this.num.cancel:
						return "text-muted";
					case this.num.end:
						return "text-muted";
				}
				return "";
			}
		},
		/*
		* 流转类型
		*/
		transferOrderType:{
			num:{
				borrowIn:1,
				testIn:2,
				originalAssets:3,
				other:99,
				lost:51,
				selled:52,
				testOut:53,
			},
			in:{
				borrowIn:1,
				testIn:2,
				originalAssets:3,
				other:99,
			},
			out:{
				lost:51,
				selled:52,
				testOut:53,
				other:99,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.borrowIn:
						return "外借流入";
					case this.num.testIn:
						return "试验机流入";
					case this.num.originalAssets:
						return "原有资产";
					case this.num.other:
						return "其它";
					case this.num.lost:
						return "丢失";
					case this.num.selled:
						return "售出";
					case this.num.testOut:
						return "试验机归还";
					default:
						return '';
				}
			}
		},
		/*
		* 流转类型
		*/
		transferOrderMode:{
			num:{
				in:1,
				out:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.in:
						return "流入";
					case this.num.out:
						return "流出";
					default:
						return '';
				}
			}
		},
		/*
		* 同行调拨单状态
		*/
		peerDeploymentOrderStatus:{
			num:{
				unSubmit:0,
				inReview:4,
				inHand:8,
				confirm:12,
				returnAudit:16,
				returnInHand:20,
				hasReturn:24,
				cancel:28,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "未提交";
					case this.num.inReview:
						return "审核中";
					case this.num.inHand:
						return "处理中";
					case this.num.confirm:
						return "租赁中";
					case this.num.returnAudit:
						return "归还审核中";
					case this.num.returnInHand:
						return "归还处理中";
					case this.num.hasReturn:
						return "已归还";
					case this.num.cancel:
						return "已取消";
					default:
						return '';
				}
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.inHand:
						return "text-alert";
					case this.num.confirm:
						return "text-success";
					case this.num.returnAudit:
						return "text-primary";
					case this.num.returnInHand:
						return "text-alert";
					case this.num.hasReturn:
						return "text-muted";
					case this.num.cancel:
						return "text-muted";
					default:
						return '';
				}
			},
		},
		returnReasonType:{
			num:{
				// canNotReturn:1,
				// expire:2,
				// aheadOfTime:3,
				// equipmentFailure:5,
				// subjectiveFactor:6,
				// change:7,
				// rentForThreeOrSixMonths:10,
				trouble:12,
				upgrade:13,
				orderCancel:14,
				priceReasonToBuy:15,
				priceReasonChangeSupplier:16,
				qualityReasonToBuy:17,
				qualityReasonChangeSupplier:18,
				untimelyService:19,
				idle:9,
				shut:8,
				dimission:20,
				overdue:4,
				// other:11,
			},
			getValue:function(num) {
				switch(num) {
					// case this.num.canNotReturn:
					// 	return "客户方设备不愿或无法退还";
					// case this.num.expire:
					// 	return "期满正常收回";
					// case this.num.aheadOfTime:
					// 	return "提前退租";
					case this.num.overdue:
						return "未按时付款或风险等原因上门收回";
					// case this.num.equipmentFailure:
					// 	return "设备故障等我方原因导致退货";
					// case this.num.subjectiveFactor:
					// 	return "主观因素等客户方原因导致退货";
					// case this.num.change:
					// 	return "更换设备";
					case this.num.shut:
						return "公司经营不善/倒闭";
					case this.num.idle:
						return "项目结束闲置";
					// case this.num.rentForThreeOrSixMonths:
					// 	return "满三个月或六个月随租随还";
					case this.num.trouble:
						return "商品故障更换";
					case this.num.upgrade:
						return "配置升级更换";
					case this.num.orderCancel:
						return "订单作废/取消";
					case this.num.priceReasonToBuy:
						return "价格原因转购买";
					case this.num.priceReasonChangeSupplier:
						return "价格原因换供应商";
					case this.num.qualityReasonToBuy:
						return "商品质量问题转购买";
					case this.num.qualityReasonChangeSupplier:
						return "商品质量问题换供应商";
					case this.num.untimelyService:
						return "服务不及时造成退货";
					case this.num.dimission:
						return "人员离职/学生毕业闲置";
					// case this.num.other:
					// 	return "其它";
				}
			},
		},
		/**
		* 金额类型
		*/
		statementDetailType:{
			num:{
				zujin:1,
				yajin:2,
				dixiao:3,
				none:4,
				other:5,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.zujin:
						return "租金";
					case this.num.yajin:
						return "押金";
					case this.num.dixiao:
						return "抵消租金";
					case this.num.none:
						return "暂无";
					case this.num.other:
						return "其它";
				}
			},
		},
		/**
		* 金额类型
		*/
		productUnit:{
			num:{
				tai:300016,
				tao:300017,
				jian:300018,
				tiao:300019,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.tai:
						return "台";
					case this.num.tao:
						return "套";
					case this.num.jian:
						return "件";
					case this.num.tiao:
						return "条";
				}
			},
		},
		/**
		* 业务类型
		*/
		rentLengthType:{
			num:{
				short:1,
				long:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.short:
						return "短租";
					case this.num.long:
						return "长租";
				}
			},
		},
		chargeType:{
			num:{
				nolineBanking:1,
				wxpay:2,
				alipayapp:3,
				balance:4,
				alipayLifeNum:5,
				others:-2,
				handle:-1,
				transferToAdd:100
			},
			getValue:function(num) {
				switch(num) {
					case this.num.nolineBanking:
						return "网银";
					case this.num.wxpay:
						return "微信公众号";
					case this.num.alipayapp:
						return "支付宝APP";
					case this.num.balance:
						return "余额";
					case this.num.alipayLifeNum:
						return "支付宝生活号";
					case this.num.others:
						return "其他";
					case this.num.handle:
						return "手动加款";
					case this.num.transferToAdd:
						return "对公转账加款";
				}
			},
		},
		/**
		* 充值状态
		*/
		chargeStatus:{
			num:{
				init:0,
				rechargeing:4,
				rechargeComplated:8,
				rechargeFail:16,
				rechargeLose:20,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.init:
						return "待支付";
					case this.num.rechargeing:
						return "充值中";
					case this.num.rechargeComplated:
						return "充值成功";
					case this.num.rechargeFail:
						return "充值失败";
					case this.num.rechargeLose:
						return "充值失效";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.init:
						return "text-primary";
					case this.num.rechargeing:
						return "text-info";
					case this.num.rechargeComplated:
						return "text-success";
					case this.num.rechargeFail:
						return "text-muted";
					case this.num.rechargeLose:
						return "text-muted";
				}
				return "";
			}
		},
		/**
		* 结算时间
		*/
		settlementDate: {
			num:{
				twentyOfMoth:20,
				endOfMonth:31,
				calendarMonth:-1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.twentyOfMoth:
						return "每月20号";
					case this.num.endOfMonth:
						return "月末最后一天";
					case this.num.calendarMonth:
						return "自然月";
				}
				return "";
			},
		},
		/**
		* 换货单状态
		**/
		changeOrderStatus:{
			num:{
				unSubmit:0,
				inReview:4,
				waitForStock:8,
				inStock:12,
				delivered:16,
				inHand:20,
				completeInStorage:22,
				cancel:24,
				completed:28,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.waitForStock:
						return "待备货";
					case this.num.inStock:
						return "备货中";
					case this.num.delivered:
						return "已发货";
					case this.num.inHand:
						return "处理中";
					case this.num.completeInStorage:
						return "完成验货";
					case this.num.cancel:
						return "已取消";
					case this.num.completed:
						return "已完成";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.waitForStock:
						return "text-info";
					case this.num.inStock:
						return "text-alert";
					case this.num.delivered:
						return "text-success";
					case this.num.inHand:
						return "text-system";
					case this.num.cancel:
						return "text-muted ";
					case this.num.completed:
						return "text-muted";
				}
				return "";
			}
		},
		/**
		* 换货原因类型
		*/
		changeReasonType: {
			num:{
				upgrade:0,
				damage:1,
				other:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.upgrade:
						return "升级";
					case this.num.damage:
						return "损坏";
					case this.num.other:
						return "其它";
				}
				return "";
			},
		},
		/**
		* 换货方式
		*/
		changeMode: {
			num:{
				pickup:1,
				lxExpress:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.pickup:
						return "上门取件";
					case this.num.lxExpress:
						return "邮寄";
				}
				return "";
			},
		},
		/**
		* 冲正单状态
		**/
		statementOrderCorrectStatus:{
			num:{
				unSubmit:0,
				inReview:1,
				correctSuccess:2,
				correctFail:3,
				cancel:4,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.correctSuccess:
						return "冲正成功";
					case this.num.correctFail:
						return "冲正失败";
					case this.num.cancel:
						return "取消";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.waitForCorrect:
						return "text-info";
					case this.num.correctSuccess:
						return "text-success";
					case this.num.correctFail:
						return "text-danger";
					case this.num.cancel:
						return "text-muted";
				}
				return "";
			}
		},
		// /*
		// * 客户状态
		// * Edit by: zhangsheng
		// * Time: 2018-02-22  
		//  */
		// customerStatus:{
		// 	num:{
		// 		initialize:0,
		// 		SubmitData:1,
		// 		approve:2,
		// 		reject:3
		// 	},
		// 	getValue:function(num) {
		// 		switch(num) {
		// 			case this.num.initialize:
		// 				return "初始化";
		// 			case this.num.SubmitData:
		// 				return "资料提交";
		// 			case this.num.approve:
		// 				return "审核通过";
		// 			case this.num.reject:
		// 				return "资料驳回";
		// 		}
		// 		return "";
		// 	}
		// },
		/*
		* description:K3退还单退还方式
		* author:weblee
		* Date:2018-02-24
		 */
		returnMode:{
			num:{
				pickup:1,
				customerReturn:2,
				customerExpress:3,
				transfer:4
			},
			getValue:function(num) {
				switch(num) {
					case this.num.pickup:
						return "上门取货";
					case this.num.customerReturn:
						return "客户自还";
					case this.num.customerExpress:
						return "客户快递";
					case this.num.transfer:
						return "转让设备";
				}
				return "";
			}
		},
		/*
		* description:K3换货单换货方式
		* author:weblee
		* Date:2018-02-24
		 */
		k3ChangeMode:{
			num:{
				pickup:1,
				// customerReturn:2,
				customerExpress:2,
				// transfer:4
			},
			getValue:function(num) {
				switch(num) {
					case this.num.pickup:
						return "上门取货";
					// case this.num.customerReturn:
					// 	return "客户自还";
					case this.num.customerExpress:
						return "客户快递";
					// case this.num.transfer:
					// 	return "转让设备";
				}
				return "";
			}
		},
		k3ChangeOrderStatus:{
			num:{
				unSubmit:0,
				inReview:4,
				cancel:24,
				completed:28,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.cancel:
						return "已取消";
					case this.num.completed:
						return "已完成";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.cancel:
						return "text-muted ";
					case this.num.completed:
						return "text-muted";
				}
				return "";
			}
		},
		k3ReturnOrderStatus:{
			num:{
				unSubmit:0,
				inReview:4,
				inHand:12,
				cancel:16,
				completed:20,
				reject:24
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "待提交";
					case this.num.inReview:
						return "审核中";
					case this.num.inHand:
						return "处理中";
					case this.num.cancel:
						return "已取消";
					case this.num.completed:
						return "已完成";
					case this.num.reject:
						return "已驳回";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-info";
					case this.num.inReview:
						return "text-primary";
					case this.num.inHand:
						return "text-primary";
					case this.num.cancel:
						return "text-muted ";
					case this.num.completed:
						return "text-muted";
					case this.num.reject:
						return "text-danger";
				}
				return "";
			}
		},
		k3recordType:{
			num:{
				customer:1,
				supplier:2,
				product:3,
				material:4,
				order:5,
				user:6,
				returnOrder:7,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.customer:
						return "客户";
					case this.num.supplier:
						return "供应商";
					case this.num.product:
						return "商品";
					case this.num.material:
						return "配件";
					case this.num.order:
						return "订单";
					case this.num.user:
						return "用户";
					case this.num.returnOrder:
						return "退货单";
				}
				return "";
			}
		},
		result:{
			num:{
				success:1,
				fail:0,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.success:
						return "成功";
					case this.num.fail:
						return "失败";
				}
				return "";
			}
		},
		/*
		*
		*/
		subCompany:{
			num:{
				headOffice:1,
				telemarketing:10,
				channelBigCustomer:11,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.headOffice:
						return "总公司";
					case this.num.telemarketing:
						return "电销";
					case this.num.channelBigCustomer:
						return "渠道大客户";
				}
				return "";
			}
		},
		/**
		 * author: liaojianming
		 * date: 2018-3-27
		 * description: bankType and slipStatus
		 */
		bankType:{
			num:{
				zfb:1,
				zg:2,
				jt:3,
				nj:4,
				ny:5,
				gs:6,
				js:7,
				pa:8,
				zs:9,
				pf:10,
				hk:11,
				kft:12,
				cash:13,
				weift:14,
				unknow:15,
				jd:16,
				unionPay:17
			},
			getValue:function(num) {
				switch(num) {
					case this.num.zfb:
						return "支付宝";
					case this.num.zg:
						return "中国银行";
						case this.num.jt:
						return "交通银行";
					case this.num.nj:
						return "南京银行";
						case this.num.ny:
						return "农业银行";
					case this.num.gs:
						return "工商银行";
						case this.num.js:
						return "建设银行";
					case this.num.pa:
						return "平安银行";
						case this.num.zs:
						return "招商银行";
					case this.num.pf:
						return "浦发银行";
					case this.num.hk:
						return "汉口银行";
					case this.num.kft:
						return "快付通";
					case this.num.cash:
						return "现金库存";
					case this.num.weift:
						return "威富通";
					case this.num.unknow:
						return "支付未知渠道";
					case this.num.jd:
						return "京东";
					case this.num.unionPay:
						return "银联";
				}
				return "";
			}
		},
		slipStatus:{
			num:{
				unConfirm: 0,
				pushDown: 1,
				allConfirm: 2
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unConfirm:
						return "未下推";
					case this.num.pushDown:
						return "已下推";
					case this.num.allConfirm:
						return "已结束";
				}
				return "";
			}
		},
		/*
		* 资金流水借贷标志、明细状态、是否属地化
		* author: zhangsheng
		* Time: 2018-03-27  
		*/
		loanSign:{
			num:{
				income:1,
				expend:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.income:
						return "贷（收入）";
					case this.num.expend:
						return "借（支出）";
				}
				return "";
			}
		},
		detailStatus:{
			num:{
				unclaimed:1,
				claimed:2,
				confirmed:3,
				hide:5
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unclaimed:
						return "未认领";
					case this.num.claimed:
						return "已认领";
					case this.num.confirmed:
						return "已确认";
					case this.num.hide:
						return "隐藏";
				}
				return "";
			}
		},
		isLocalization:{
			num:{
				localization:1,
				unLocalization:0,
				highseas:2
			},
			getValue:function(num) {
				switch(num) {
					case this.num.localization:
						return "是";
					case this.num.unLocalization:
						return "否";
					case this.num.highseas:
						return "公海流水";
				}
				return "";
			}
		},
		detailType:{
			num:{
				claimed:2,
				confirmed:3
			},
			getValue:function(num) {
				switch(num) {
					case this.num.claimed:
						return "已认领";
					case this.num.confirmed:
						return "已确认";
				}
				return "";
			}
		},
		/**
		* author:liwenbin
		* date:2018-03-30
		* description:客户地址审核状态
		*/
		customerConsignInfoVerifyStatus:{
			num:{
				unSubmit:0,
				commited:1,
				shortList:2,
				finalPass:3,
				reject:4
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "未审核";
					case this.num.commited:
						return "审核中";
					case this.num.shortList:
						return "初审通过";
					case this.num.finalPass:
						return "终审通过";
					case this.num.reject:
						return "驳回";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unSubmit:
						return "text-primary";
					case this.num.commited:
						return "text-info";
					case this.num.shortList:
						return "text-info";
					case this.num.finalPass:
						return "text-success";
					case this.num.reject:
						return "text-danger";
					default:
						return '';
				}
			}
		},
		/**
		 * author: liaojianming
		 * date: 2018-4-4
		 * description: 优惠券类型 优惠券使用状态
		*/
		couponType: {
			num: {
				equipment: 1,
				billCoupon: 2
			},
			getValue: function(num) {
				switch(num) {
					case this.num.equipment:
						return "设备优惠券";
					case this.num.billCoupon:
						return "结算单优惠券";
					default:
						return "";
				}
			}
		},
		isOnline: {
			num: {
				offline: 0,
				online: 1
			},
			getValue: function(num) {
				switch(num) {
					case this.num.offline:
						return "否";
					case this.num.online:
						return "是";
					default:
						return "";
				}
			}
		},
		couponStatus: {
			num: {
				unaccalimed: 0,
				available: 4,
				used: 8
			},
			getValue: function(num) {
				switch(num) {
					case this.num.unaccalimed:
						return "未领取";
					case this.num.available:
						return "可用";
					case this.num.used:
						return "已用";
					default:
						return "";
				}
			}
		},
		/*
		* 取消/强制取消订单原因
		* author: zhangsheng
		* Time: 2018-03-27  
		*/
		cancelReason: {
			num: {
				wrongOrder: 1,
				changeNumber: 2,
				changePrice: 3,
				changeMaterial: 4,
				changeSettlementDate: 5,
				changePayModal: 6,
				changeTenancyTerm: 7,
				changeModal: 8,
				changeContactInfo: 9,
				wrongPeerTransfer: 10,
				equipmentTrouble: 12,
				wrongCustomerName: 13,
				customerCancel: 14,
				outOfStore: 15,
				discrepancies: 16,
			},
			getValue: function(num) {
				switch(num) {
					case this.num.wrongOrder:
						return "下错单";
					case this.num.changeNumber:
						return "变更数量";
					case this.num.changePrice:
						return "变更单价";
					case this.num.changeMaterial:
						return "变更配件";
					case this.num.changeSettlementDate:
						return "变更结算日";
					case this.num.changePayModal:
						return "变更支付方式";
					case this.num.changeTenancyTerm:
						return "变更时间/租期";
					case this.num.changeModal:
						return "变更型号/配置";
					case this.num.changeContactInfo:
						return "变更收货人信息";
					case this.num.wrongPeerTransfer:
						return "同行调货选错";
					case this.num.equipmentTrouble:
						return "设备故障换货";
					case this.num.wrongCustomerName:
						return "客户名称错误";
					case this.num.customerCancel:
						return "客户取消订单";
					case this.num.outOfStore:
						return "缺货取消";
					case this.num.discrepancies:
						return "实际出货与订单不符";
					default:
						return "";
				}
			}
		},
		/*
		* 业务员提成统计排序规则、排序字段
		* author: zhangsheng
		* Time: 2018-03-27  
		*/
		orderBy: {
			num: {
				subCompanyName: "subCompanyName",
				salesmanName: "salesmanName",
				dealsCount: "dealsCount",
				dealsProductCount: "dealsProductCount",
				dealsAmount: "dealsAmount",
				awaitReceivable: "awaitReceivable",
				income: "income",
			},
			getValue: function(num) {
				switch(num) {
					case this.num.subCompanyName:
						return "分公司";
					case this.num.salesmanName:
						return "业务员姓名";
					case this.num.dealsCount:
						return "成交单数";
					case this.num.dealsProductCount:
						return "成交台数";
					case this.num.dealsAmount:
						return "成交金额";
					case this.num.awaitReceivable:
						return "待收";
					case this.num.income:
						return "本期回款";
					default:
						return "";
				}
			}
		},
		deductOrderType: {
			num: {
				asc: "ASC",
				desc: "DESC",
			},
			getValue: function(num) {
				switch(num) {
					case this.num.asc:
						return "ASC";
					case this.num.desc:
						return "DESC";
					default:
						return "";
				}
			}
		},
		/*
		* 组合商品新旧
		*/
		isNewGroup: {
			num: {
				new: 1,
				subNew: 0,
			},
			getValue: function(num) {
				switch(num) {
					case this.num.new:
						return "全新";
					case this.num.subNew:
						return "次新";
					default:
						return "";
				}
			}
		},

		/*打印记录type*/
		printReferType:{
			num:{
				order:1,
				returnOrder:2
			},
			getValue:function(num) {
				switch(num) {
					case this.num.order:
						return "订单";
					case this.num.returnOrder:
						return "退货单";
					default:
						return "";
				}
			}
		},
		/*企业客户户账户流水类型*/
		customerAccountLogType:{
			num:{
				handRecharge:1,
				charge:2,
				balancePaid:3,
				refund:4,
				inlineRecharge:5,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.handRecharge:
						return "手动充值";
					case this.num.charge:
						return "扣款";
					case this.num.balancePaid:
						return "余额支付";
					case this.num.refund:
						return "退款";
					case this.num.inlineRecharge:
						return "线上充值";
					default:
						return "";
				}
			}
		},
		/*收货变更原因*/
		changeReasonType:{
			num:{
				failure:1,
				outOfRange:2,
				other:3,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.failure:
						return "设备故障";
					case this.num.outOfRange:
						return "商品数量超过实际需求";
					case this.num.other:
						return "其他";
					default:
						return "";
				}
			}
		},
		/*续租单状态*/
		reletOrderStatus:{
			num:{
				toSubmit:0,
				inAudit:4,
				reletSuccess:8,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.toSubmit:
						return "待提交";
					case this.num.inAudit:
						return "审核中";
					case this.num.reletSuccess:
						return "续租成功";
					default:
						return "";
				}
			},
			getClass:function(num) {
				switch(num) {
					case this.num.toSubmit:
						return "text-info";
					case this.num.inAudit:
						return "text-primary";
					case this.num.reletSuccess:
						return "text-muted";
				}
			}
		},
		/*分段结算截止类型*/
		sectionSettlementType:{
			num:{
				endOfMonth:0,
				current:1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.endOfMonth:
						return "结算到本月底";
					case this.num.current:
						return "结算到下一结算日";
					default:
						return "";
				}
			},
		},
		/*续租单状态*/
		dynamicSqlStatus:{
			num:{
				inReview:0,
				pass:1,
				reject:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.inReview:
						return "审核中";
					case this.num.pass:
						return "审核通过";
					case this.num.reject:
						return "拒绝执行";
					default:
						return "";
				}
			},
		},
		/*财务统计类型*/
		statisticsType:{
			num:{
				week:1,
				month:2,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.week:
						return "周报";
					case this.num.month:
						return "月报";
					default:
						return "";
				}
			},
		},
		/*定时任务Job类型*/
		taskJobType:{
			num:{
				rest:1,
				mq:2,
				redis:3
			},
			getValue:function(num) {
				switch(num) {
					case this.num.rest:
						return "rest";
					case this.num.mq:
						return "mq";
					case this.num.redis:
						return "redis";
				}
			}
		},
		/*定时任务系统类型*/
		taskSystemType:{
			num:{
				erp:1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.erp:
						return "erp";
				}
			}
		},
		/*延时任务类型*/
		taskType:{
			num:{
				monthlyStatement:1,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.monthlyStatement:
						return "对账单导出";
					default:
						return "";
				}
			},
		},
		/*延时任务状态*/
		taskStatus:{
			num:{
				inLine:1,
				handle:2,
				complete:3,
				cancel:4,
				failure:5,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.inLine:
						return "排队中";
					case this.num.handle:
						return "处理中";
					case this.num.complete:
						return "已完成";
					case this.num.cancel:
						return "已取消";
					case this.num.failure:
						return "执行失败";
					default:
						return "";
				}
			}
		},
		//大学生订单状态
		undergraduateOrderStatus:{
			num:{
				unactived:0,
				usable:1,
				paying:2,
				payed:3,
				delivered:4,
				renting:5,
				refund:6,
				returnBack:7,
				payFailure:8
			},
			array:function() {
				var array = new Array();
				for (var key in this.num) {
					array.push({
						num:this.num[key],
						value:this.getValue(this.num[key])
					}); 
				}
				return array;
			},
			getValue:function(num) {
				switch(num) {
					case this.num.unactived:
						return "未激活";
					case this.num.usable:
						return "未付款";
					case this.num.paying:
						return "支付中";
					case this.num.payed:
						return "已付款";
					case this.num.delivered:
						return "已发货";
					case this.num.renting:
						return "租赁中";
					case this.num.refund:
						return "已退款";
					case this.num.returnBack:
						return "已归还";
					case this.num.payFailure:
						return "付款失败";
				}
				return "";
			},
			getClass:function(num) {
				switch(num) {
					case this.num.unactived:
						return "text-info";
					case this.num.usable:
						return "text-primary";
					case this.num.paying:
						return "text-info";
					case this.num.payed:
						return "text-alert";
					case this.num.delivered:
						return "text-success";
					case this.num.renting:
						return "text-success";
					case this.num.refund:
						return "text-system";
					case this.num.returnBack:
						return "text-muted ";
					case this.num.payFailure:
						return "text-muted";
				}
				return "";
			},
		},
		/*打印单据类型*/
		referType:{
			num:{
				service:1,
				delivery:2,
				pick:3
			},
			getValue:function(num) {
				switch(num) {
					case this.num.service:
						return "技术服务交货单";
					case this.num.delivery:
						return "租赁商品交货单";
					case this.num.pick:
						return "商品备货单";
					default:
						return "";
				}
			}
		},
		/*sql语句类型*/
		sqlType:{
			num:{
				default:0,
				seach:1,
				update:2,
				del:3,
				other:4
			},
			getValue:function(num) {
				switch(num) {
					case this.num.default:
						return "默认";
					case this.num.seach:
						return "查询";
					case this.num.update:
						return "更新";
					case this.num.del:
						return "删除";
					case this.num.other:
						return "其他";
				}
				return "";
			}
		},
	}

	Rental.Enum = Enum;

}(jQuery));