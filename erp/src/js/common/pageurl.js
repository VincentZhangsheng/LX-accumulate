var PageUrl = {
	Login:SitePath.base + "login",
	home:SitePath.base + "home",
	printCode:SitePath.base +'print-manage/barcode',

	purchaseDetail:SitePath.base + "purchase-manage/detail",
	purchaseReceiveDetail:SitePath.base + "purchase-receive-manage/detail",
	purchaseDeliveryDetail:SitePath.base + "purchase-delivery-manage/detail",
	orderDetail:SitePath.base + "order-manage/detail",
	deplymentOrderDetail:SitePath.base + "order-deployment-manage/detail",
	transferInOrderDetail:SitePath.base + "order-transfer-in-manage/detail",
	transferOutOrderDetail:SitePath.base + "order-transfer-out-manage/detail",
	returnOrderDetail:SitePath.base + "order-return-manage/detail",
	peerOrderDetail:SitePath.base + "peer-manage/order-detail",
	k3orderDetail:SitePath.base + "k3-order/detail",
	k3ChangeOrderDetail:SitePath.base + "change-order-k3/detail",
	k3ReturnOrderDetail:SitePath.base + "return-order-k3/detail",
	statementCorrectOrderDetail:SitePath.base + "correct-order/detail",
	reletDetail:SitePath.base + 'order-manage/relet-detail',
	statementOrderDetail:SitePath.base + "statement-order/detail",
	materialDetail:SitePath.base + "material-manage/detail",
	workflowDetail:SitePath.base + "audit-manage/detail",

	SiteMessageInboxList:SitePath.base + 'site-message/inbox-list',
	SiteMessageOutboxList:SitePath.base + 'site-message/outbox-list',

	companyCustomerDetail:SitePath.base + 'customer-business-manage/detail', //企业详细
	customerDetail:SitePath.base + 'customer-manage/detail', //个人详细
	commonCustomerDetail:SitePath.base + 'customer-common-manage/detail', //个人/企业详细
	serviceCompanyCustomerDetail:SitePath.base + 'customer-business-manage/service-detail', //400客服企业详细
	serviceCustomerDetail:SitePath.base + 'customer-manage/service-detail', //400客服个人详细

	commonCustomerDetailByConsignInfoId:SitePath.base + 'customer-common-manage/to-detail-by-consignInfoId', //个人详细

	auditManageList:SitePath.base + 'audit-manage/list', //审核工作流列表

	noAccess:SitePath.base + 'no-access',
};