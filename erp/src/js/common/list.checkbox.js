;(function($) {

	$.fn.rtlCheckAll = function(checkAllName,checkItemName) {
		var container = this;

		container.on('click', '[name='+checkAllName+']', function(event) {
			var isChecked = $(this).prop("checked");
			$('[name='+checkItemName+']',container).prop({checked:isChecked});
		});

		container.on('click', '[name='+checkItemName+']', function(event) {
			var isChecked = $(this).prop("checked");
	        if(isChecked) {
	            var checkBoxs = $('[name='+checkItemName+']').filter(function(index,item) {
	            	return $(item).prop("checked") == false;
	            });
	            isChecked = checkBoxs.length == 0;
	        }
	        $('[name='+checkAllName+']',container).prop({checked:isChecked});
		});
	};

	$.fn.rtlCheckArray = function() {
		return this.filter(':checked').map(function() {
			return $(this).val();
		}).toArray();
	};

})(jQuery);