Rental.validate = {
	init:function() {
		jQuery.extend(jQuery.validator.messages, {
            required: "必填字段",
            remote: "不可用，请重新输入",
            email: "请输入正确格式的电子邮件",
            url: "请输入合法的网址",
            date: "请输入合法的日期",
            dateISO: "请输入合法的日期 (ISO).",
            number: "请输入合法的数字",
            digits: "只能输入整数",
            creditcard: "请输入合法的信用卡号",
            equalTo: "请再次输入相同的值",
            accept: "请输入拥有合法后缀名的字符串",
            maxlength: jQuery.validator.format("请输入一个 长度最多是 {0} 的字符串"),
            minlength: jQuery.validator.format("请输入一个 长度最少是 {0} 的字符串"),
            rangelength: jQuery.validator.format("请输入 一个长度介于 {0} 和 {1} 之间的字符串"),
            range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
            max: jQuery.validator.format("请输入一个最大为{0} 的值"),
            min: jQuery.validator.format("请输入一个最小为{0} 的值")
        });
        
        // 手机号码验证
    	jQuery.validator.addMethod("mobilephone",function(value,element) {
            var length = value.length;
            var mobile = /^(13[0-9]{9})|(14[0-9]{9})|(15[0-9]{9})|(16[0-9]{9})|(17[0-9]{9})|(18[0-9]{9})|(19[0-9]{9})$/;
            return this.optional(element) || (length == 11 && mobile.test(value));
        },"请输入正确的手机号码");

        // 电话号码验证
        jQuery.validator.addMethod("telephone",function(value,element) {
            var length = value.length;
            var tel =/^(?:(?:0\d{2,3})-)?(?:\d{7,8})(-(?:\d{3,}))?$/;
            return this.optional(element) || tel.test(value);
        },"请正确填写电话号码(0755-8888888)");

    	// 手机号码或电话号码验证
    	jQuery.validator.addMethod("isPhone",function(value,element) {
            var length = value.length;
            var start = value.substring(0, 1);
            var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
            var tel =/^(?:(?:0\d{2,3})-)?(?:\d{7,8})(-(?:\d{3,}))?$/;
            if (start == 1) {
            	return this.optional(element) || (length == 11 && mobile.test(value));
            } else if (start == 0) {            	
            	return this.optional(element) || tel.test(value);
            } else {
            	return false;
            }
        },"请正确填写电话号码<br />例如:13000000000或0755—22222007");

        // 字母或数字
        jQuery.validator.addMethod("letterOrNumber", function(value, element) {
            var chrnum = /^([a-zA-Z0-9]+)$/;
            return this.optional(element) || (chrnum.test(value));
        }, "只能输入数字和字母(字符A-Z, a-z, 0-9)");

         // 字母或数字
        jQuery.validator.addMethod("letterOrNumber2", function(value, element) {
            var chrnum = /^([A-Za-z0-9\-]+)$/;
            return this.optional(element) || (chrnum.test(value));
        }, "只能输入数字、字母、横杠(字符A-Z, a-z, 0-9, -)");

        // 税率（0-1 包含0、1）之间小数
        jQuery.validator.addMethod("taxRate", function(value, element) {
            var chrnum = /^(0(\.\d+)?|1)$/;
            return this.optional(element) || (chrnum.test(value));
        }, "只能输入0到1之间小数（包含0或1）");

         // 税率（0-100 包含0、1）之间小数
        jQuery.validator.addMethod("taxRate2", function(value, element) {
            var chrnum = /^(\d|[1-9]\d|100)$/;
            return this.optional(element) || (chrnum.test(value));
        }, "只能输入0到100之间数值（包含0或100）");


        // 字母或汉字
        jQuery.validator.addMethod("letterOrChinese", function(value, element) {
            var chrnum = /^([a-zA-Z\u0391-\uFFE5.]+)$/;
            return this.optional(element) || (chrnum.test(value));
        }, "只能输入汉字和字母");

        // 金额验证
        jQuery.validator.addMethod("moneyNumber",function(value, element) {
            // var decimalsValue =/^(?!0+(?:\.0+)?$)(?:[1-9]\d*|0)(?:\.\d{1,2})?$/ ;
            var decimalsValue =/^(?!(?:\.0+)?$)(?:[1-9]\d*|0)(?:\.\d{1,2})?$/ ;
            return  this.optional(element) || (decimalsValue.test(value));
        }, "请输入合法金额并且只能精确到分"); 

        // 金额验证(大于0)
        jQuery.validator.addMethod("moneyNumber2",function(value, element) {
            // var decimalsValue =/^(?!0+(?:\.0+)?$)(?:[1-9]\d*|0)(?:\.\d{1,2})?$/ ;
            var decimalsValue =/^(?!(?:\.0+)?$)(?:[1-9]\d*|0)(?:\.\d{1,2})?$/ ;
            var isTrue =  this.optional(element) || (decimalsValue.test(value));
            if(isTrue) {
                isTrue = parseFloat(value) > 0;
            }
            return isTrue;
        }, "请输入合法金额并且只能精确到分"); 

        // 纳税人识别号
        jQuery.validator.addMethod("taxpayerId", function(value, element) {
        	var length = value.length;
            return this.optional(element) || (length == 15) || (length == 18);
        }, "您输入的识别号码位数有误，请确认重新输入");

        // 银行卡号
        jQuery.validator.addMethod("bankcard", function(value, element) {
        	var val = value.replace(/\s/g, "");
			var len = val.length;
			var newVal = "";
			for (var i = 0; i < len; i++) {
				if (i > 0 && i % 4 == 0)
					newVal += " ";
				newVal += val.charAt(i);
			}
			$(element).val(newVal);
            return this.optional(element) || (len == 16) || (len == 19);
        }, "请输入合法的银行卡号");

        //登陆账号验证：邮箱或手机号
        jQuery.validator.addMethod("loginIdentity",function(value,element) {
            var length = value.length;
            var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
            var email = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/;
            return this.optional(element) || ((length == 11 && mobile.test(value)) || email.test(value));
        },"请输入正确的邮件或手机号");

        //通用密码格式验证
        jQuery.validator.addMethod("commonPassword",function(value,element) {
            var length = value.length;
            // var chrnum = /^([a-zA-Z0-9]+)$/;
            // return this.optional(element) || ((length >=6 && length <= 24) && chrnum.test(value));

            var chrnum = /^(?!([A-Z]*|[a-z]*|[0-9]*|[!-/:-@\[-`{-~]*|[A-Za-z]*|[A-Z0-9]*|[A-Z!-/:-@\[-`{-~]*|[a-z0-9]*|[a-z!-/:-@\[-`{-~]*|[0-9!-/:-@\[-`{-~]*)$)[A-Za-z0-9!-/:-@\[-`{-~]{8,20}$/;
             return this.optional(element) || (chrnum.test(value));

        },"请输入正确的密码（8-20位, 须含大、小写字母、数字、特殊符中三类）");

        //通用图形验证码格式
        jQuery.validator.addMethod("commonImgVerifyCode",function(value,element) {
            var length = value.length;
            return this.optional(element) || (length ==4);
        },"请输入正确的验证码（4位数字或字符）");

        //验证整数
        jQuery.validator.addMethod("checkInteger",function(value, element) {
            var number = /^\d+$/;
            return this.optional(element) || (number.test(value));
        },"请输入正整数");

        //验证身份证
        jQuery.validator.addMethod("checkIDCard",function(value, element) {
             // return Rental.helper.checkIDCard(value);
            var len = value.length;
            if (len == 15 || len == 18) {
                return Rental.helper.checkIDCard(value);
            } else {
                var chrnum = /^([a-zA-Z0-9]+)$/;
                return this.optional(element) || (chrnum.test(value));
            }
        },"请输入正确的身份证号");
	}
}
