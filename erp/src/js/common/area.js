;(function($){

	function Area() {
		
		this.state = {
			province:null,
			city:null,
			district:null,
			disabled:false,
		}

		this.opt = '<option value="{0}">{1}</option>';

		this.setDefaultValue = function(prams) {
			this.state = {
				province:prams.province || '',
				city:prams.city || '',
				district:prams.district || '',
				disabled:prams.disabled || false
			}
		}

		this.changeValue = function(prams) {
			this.setDefaultValue(prams);
			this.render();
		}

		this.init = function(prams) {
			this.props = {
				$province:prams.$province,
				$city:prams.$city,
				$district:prams.$district,
				callBack:prams.callBack
			};
			this.initDom();
			this.initEvent();
		};

		this.initDom = function() {
		}

		this.initEvent = function() {
			var self = this;
			self.getAreaList();
			self.props.$province.change(function(event) {
				event.preventDefault();
				self.changeProvice();
			});
			self.props.$city.change(function(event) {
				event.preventDefault();
				self.changeCity();
			});
			self.props.$district.change(function(event) {
				event.preventDefault();
				self.changDistrict();
			});
		}

		this.changeProvice = function(prams) {
			var self = this;

			var areaProvinceId = prams || self.props.$province.val();
			var areaCity = self.state.area[areaProvinceId].areaCityList;
			if(!areaCity  || (_.isArray(areaCity) && areaCity.length == 0 )) {
				self.renderCity(new Array());
				self.renderDistrict(new Array());
				self.props.$city.prop({disabled:true});
				self.props.$district.prop({disabled:true})
				return;
			}
			self.state.areaCity =  _.indexBy(areaCity,'areaCityId');
			self.renderCity(areaCity);
			self.changeCity(areaCity[0].areaCityId);
			self.state.province = areaProvinceId;
		}

		this.changeCity = function(prams) {
			var self = this;
			var areaCityId = prams || self.props.$city.val();
			var areaDistrict = self.state.areaCity[areaCityId].areaDistrictList;
			if(!areaDistrict || (_.isArray(areaDistrict) && areaDistrict.length == 0)) {
				self.renderDistrict(new Array());
				self.props.$district.prop({disabled:true});
				return;
			}
			self.renderDistrict(areaDistrict);
			self.state.city = areaCityId;
		}

		this.changDistrict = function(prams) {
			var self = this;
			var district = prams || self.props.$district.val();
			self.state.district = district;
		}

		this.getAreaList = function() {
			var self = this;

			var areaData = Rental.localstorage.area.get();
			if(!areaData) {
				Rental.ajax.ajaxData('area/getAreaList', {}, '获取省份城市信息', function(response) {
					self.render(response.resultMap.data);
					self.props.callBack(response.resultMap.data);
					Rental.localstorage.area.set(response.resultMap.data)
				});
			} else  {
				self.render(areaData);
				self.props.callBack(areaData);
			}

			// Rental.ajax.ajaxData('area/getAreaList', {}, '获取省份城市信息', function(response) {
			// 	self.render(response.resultMap.data);
			// 	self.props.callBack(response.resultMap.data);
			// });
		}

		this.render = function(data) {
			var self = this;
			data = data || Rental.localstorage.area.get();
			if(!_.isArray(data)) {
				self.getAreaList();
				// Rental.notification.error("获取省份城市信息",'没有获取到正确的省份城市地区信息，' + Rental.lang.commonSysContact);
				return;
			}

			self.state.area = _.indexBy(data,'areaProvinceId');

			var areaCity = new Array(); //self.state.area[data[0].areaProvinceId].areaCityList;
			if(!!this.state.province) {
				var areaCityObj = self.state.area[this.state.province];
				if(areaCityObj && areaCityObj.hasOwnProperty('areaCityList')) {
					areaCity = areaCityObj.areaCityList;
					self.state.areaCity =  _.indexBy(areaCity,'areaCityId');
				}
			}

			var areaDistrict =  new Array(); //self.state.areaCity[areaCity[0].areaCityId].areaDistrictList;
			if(!!this.state.city) {
				var areaDistrictObj = self.state.areaCity[this.state.city];
				if(areaDistrictObj && areaDistrictObj.hasOwnProperty('areaDistrictList')) {
					areaDistrict = areaDistrictObj.areaDistrictList;
				}
			}

			self.renderProvince(data);
			self.renderCity(areaCity);
			self.renderDistrict(areaDistrict);

			self.state.province && self.props.$province.val(self.state.province || '').prop({disabled:self.state.disabled});
			self.state.city && self.props.$city.val(self.state.city || '').prop({disabled:self.state.disabled});
			self.state.district && self.props.$district.val(self.state.district || '').prop({disabled:self.state.disabled});
		}

		this.renderProvince = function(data) {
			var self = this;
			this.renderSelect(data, self.props.$province, function(item) {
				return self.opt.format(item.areaProvinceId, item.provinceName);
			}, '省份');
		}

		this.renderCity = function(data) {
			var self = this;
			this.renderSelect(data, self.props.$city, function(item) {
				return self.opt.format(item.areaCityId, item.cityName);
			}, '城市');
		}

		this.renderDistrict = function(data) {
			var self = this;
			this.renderSelect(data, self.props.$district, function(item) {
				return self.opt.format(item.areaDistrictId, item.districtName);
			}, "地区")
		}

		this.renderSelect = function(data, container, func, type) {
			var self = this, defaultOpt = self.opt.format('', '请选择{0}'.format(type || ''));
			if(!_.isArray(data)) return;
			if(data.length == 0) {
				container.html(defaultOpt);
				return;
			}
			var disabled = container.prop('disabled');
			disabled && container.prop({disabled:false});
			
			var opts = data.map(func);
			opts.unshift(defaultOpt);
			container.html(opts.join(''));
		}
	}

	window.Area = Area;

})(jQuery);