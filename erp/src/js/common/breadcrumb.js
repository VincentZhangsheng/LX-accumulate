/*
* Author：weblee
* description：面包屑
*/
(function($) {
	var Breadcrumb = {
		init:function(prams) {
			this.initDom();
			this.resolveData(prams);
			this.render();
		},
		initDom:function() {
			this.$tpl = $("#breadcrumbTpl").html();
			this.$breadcrumb = $("#breadcrumb");
		},
		resolveData:function(prams) {
			prams = prams || [{menuName:"首页",menuUrl:"/home"}];
			var lastIndex = prams.length-1;
			var lastLink = prams[lastIndex];
			this.data = {
				"default":lastLink,
				"breadcrumb":function(){
					return {
						links:prams,
						isLink:function() {
							return this && this.hasOwnProperty('menuUrl') && !!this.menuUrl;
						}		
					}
				},
			}
		},
		render:function() {
			Mustache.parse(this.$tpl);
	        $("#breadcrumb").html(Mustache.render(this.$tpl,this.data));
		}
	};

	window.Breadcrumb = Breadcrumb;

})(jQuery);