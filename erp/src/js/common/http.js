Rental.ajax  =  {
    submit:function(url,data,success,error,description,loadingType) {
        
        var self = this;
        self.loading.show(loadingType);

        $.ajax({
            type:'POST',
            url: url,
            data: JSON.stringify(data),
            contentType:'application/json',
            dataType: "json",
            cache: false,
            success:function(response) {
                self.loading.close();
                if(success) {
                    success(response);
                } else {
                    Rental.ajax.ajaxDone(response,$form.attr("callback-func"));    
                }
            },
            error:function(xhr, ajaxOptions, thrownError) {
               self.loading.close();
                if(error) {
                    error(xhr, ajaxOptions, thrownError)
                } else {
                    Rental.ajax.ajaxError(xhr, ajaxOptions, thrownError,description);     
                }
            }
        });
    },
    submitNoLoading:function(url,data,success,error,description) {
        
        var self = this;
        // self.loading.show(loadingType);

        $.ajax({
            type:'POST',
            url: url,
            data: JSON.stringify(data),
            contentType:'application/json',
            dataType: "json",
            cache: false,
            success:function(response) {
                // self.loading.close();
                if(success) {
                    success(response);
                } else {
                    Rental.ajax.ajaxDone(response,$form.attr("callback-func"));    
                }
            },
            error:function(xhr, ajaxOptions, thrownError) {
               // self.loading.close();
                if(error) {
                    error(xhr, ajaxOptions, thrownError)
                } else {
                    Rental.ajax.ajaxError(xhr, ajaxOptions, thrownError,description);     
                }
            }
        });
    },
    ajaxData:function(url, prams, des, success, error, loading) {
        Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
            if(response.success) {
                success && success(response);
            } else {
                error && error();
                Rental.notification.error(des, response.description || '失败');
            }
        }, error, des, loading || 'listLoading');
    },
    ajaxDataNoLoading:function(url, prams, des, success, error) {
        Rental.ajax.submitNoLoading("{0}{1}".format(SitePath.service, url), prams, function(response){
            if(response.success) {
                success && success(response);
            } else {
                error && error();
                Rental.notification.error(des, response.description || '失败');
            }
        }, null , des);
    },
    loading: {
        isListLoading:false,
        isDialogLoading:false,
        loadingObj:null,
        show:function(loadingType) {
            this.isListLoading = loadingType == "listLoading";
            this.isDialogLoading = loadingType == "dialogLoading";

            if(this.isListLoading) {
                this.loadingObj = $('.listLoading');
                this.loadingObj.removeClass("hide");
            } else if(this.isDialogLoading) {
                this.loadingObj = Rental.ui.loadingDialog();
            }
        },
        close:function() {
            if(this.isListLoading) {
                this.loadingObj.addClass("hide");
            } else if(this.isDialogLoading) {
                this.loadingObj.modal('hide');
            }
        }
    },
	ajaxError:function(xhr, ajaxOptions, thrownError, description) {
		if(xhr.status==401 || xhr.responseText == "timeout"){
            window.location.href = "{0}?ru={1}".format(PageUrl.login,encodeURIComponent(window.location.href));
            return;
        } 

        var message = "<div>"+Rental.lang.commonErrorDes+"</div>"
    				+ "<div>" + "error: " + xhr.status + "</div>"
                    + "<div>" + "message: " + xhr.statusText + "</div>"
                    + "<div>" + "ajaxOptions: " + ajaxOptions + "</div>"
                    + "<div>" + "thrownError: " + thrownError + "</div>";

        Rental.notification.error(!description ? "请求异常" : "{0}异常".format(description),message);
	},
	ajaxDone:function(response,$callbackFunc,description) {
		if($callbackFunc){
            if(!$.isFunction($callbackFunc)){
                $callbackFunc = eval('(' + $callbackFunc + ')');
            }
            var res = $callbackFunc(response);
            if(res==false){
                this.done(response,description);
            }
        } else {
            this.done(response,description);
        }
	},
	done:function(response,description) {
        if(response.success) {
            Rental.notification.success(description || "操作成功",response.description);
        } else {
            Rental.notification.error(!description ? "操作失败" : "{0}失败".format(description),response.description);
        }
	}
}