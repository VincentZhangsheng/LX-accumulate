//sql语句列表
;(function($) {
	var DynamicSqlManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_data_analysis];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_data_analysis_sql_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

            var add = this.currentPageAuthor.children[AuthorCode.manage_data_analysis_sql_list_add];
			var sqlSearch = this.currentPageAuthor.children[AuthorCode.manage_data_analysis_sql_list_search];
			var del = this.currentPageAuthor.children[AuthorCode.manage_data_analysis_sql_list_del];
			var update = this.currentPageAuthor.children[AuthorCode.manage_data_analysis_sql_list_edit];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			sqlSearch && this.rowActionButtons.push(AuthorUtil.button(_.extend(sqlSearch,{menuUrl:"data-analysis/dynamicSql"}),'searchButton','','查询'));
			update && this.rowActionButtons.push(AuthorUtil.button(update,'updateButton','','编辑'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'delButton','','删除'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.renderCommonActionButton(); //渲染操作按钮及事件

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.render(new Object());
			self.searchData();  //初始化列表数据

            $("#actionCommonButtons .addButton").on('click', function(event) {
				event.preventDefault();
				self.addSql.init({
                    callBack:function() {
                        self.searchData();
                    }
                });
			});
			
			// self.$dataListTable.on('mouseover', '.sqlContent', function(event) {
			// 	var nextTr = $(this).closest("tr").next();
			// 	nextTr.find("td").addClass("active")
			// });
			
			self.$dataListTable.on('click', '.sqlContent', function(event) {
				var nextTr = $(this).closest("tr").next();
				nextTr.find("td").toggleClass("active")
			});
			
			self.$dataListTable.on('click', '.updateButton', function(event) {
				event.preventDefault();
				var dynamicSqlId = $(this).data('id');
				var trDate = $(this).closest('tr').data("str");
				var updateDate = {
					dynamicSqlId : trDate.dynamicSqlId,
					sqlTitle : trDate.sqlTitle,
					sqlContent : trDate.sqlContent,
				};
				self.editSql.init({
					sqlData:trDate,
                    callBack:function() {
                        self.searchData();
                    }
                });
				// self.update(updateDate);
			});
			
			self.$dataListTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				var dynamicSqlId = $(this).data('id');
				self.del(dynamicSqlId);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.ajaxData("dynamicSql/page",searchData, "查询sql语句列表", function(response){
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				// rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					sqlTypeValue:function(){
						return Enum.sqlType.getValue(this.sqlType);
					},
					dataToStr:function(){
						return JSON.stringify(this);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array(),
			seachBtn = order.sqlType == Enum.sqlType.num.seach,
			editBtn = order.sqlType == Enum.sqlType.num.update;
		
			(buttons || []).forEach(function(button, index) {
			// 	button.orderNo = order.orderNo;
			// 	button.canReletOrder = order.canReletOrder;
			// 	button.statementDate = order.statementDate;
				rowActionButtons.push(button);

				if((button.class == 'searchButton') && !seachBtn) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'updateButton') && !editBtn) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

			});
			return rowActionButtons;
		},
		update:function(prams) {
			var self = this;
			if(!prams) {
				bootbox.alert('没找到sql');
				return;
			}
			Rental.ajax.ajaxData("dynamicSql/update",prams, "更新sql语句列表", function(response){
				self.searchData();
			})
        },
		del:function(prams) {
			var self = this;
			if(!prams) {
				bootbox.alert('没找到sql');
				return;
			}
			function d() {
				Rental.ajax.submit("{0}dynamicSql/delete".format(SitePath.service),{dynamicSqlId:prams},function(response){
					if(response.success) {
						Rental.notification.success('删除sql语句',response.description || '成功');
						self.doSearch(self.Pager.pagerData.currentPage);
					} else {
						Rental.notification.error("删除sql语句",response.description || '失败');
					}
				}, null, "删除sql语句", "dialogLoading");
			}
			bootbox.confirm('确认删除？',function(result) {
				result && d();
			});
        },
        addSql:{
            init:function(prams) {
                this.props = _.extend({
                    callBack:function() {}
                }, prams || {});
    
                this.show();
            },
            show:function() {
                var self = this;
                Rental.modal.open({src:SitePath.base + "data-analysis/sql-add", type:'ajax', 
                    ajaxContentAdded:function(pModal) {
                        self.initDom(pModal);
                        self.initEvent();	
                    },
                });
            },
            initDom:function(pModal) {
                this.$addSqlForm = $("#addSqlForm");
            },
            initEvent:function() {
                var self = this;
                Rental.form.initFormValidation(self.$addSqlForm,function(form){
                    self.add();
                });
    
                self.$addSqlForm.on('click', '.cancelButton', function(event) {
                    event.preventDefault();
                    Rental.modal.close();
                });
            },
            add:function() {
                try {
					var self = this,
						sqlTitle = $('[name=sqlTitle]',self.$addSqlForm).val(),
						sqlContent = $('[name=sqlContent]',self.$addSqlForm).val(),
						remark = $('[name=remark]',self.$addSqlForm).val(),
                        formData = {
							sqlTitle:sqlTitle,
							sqlContent:sqlContent,
							remark:remark
						};
    
                    var des = '添加sql语句';
                    Rental.ajax.submit("{0}dynamicSql/create".format(SitePath.service),formData,function(response){
                        if(response.success) {
                            Rental.notification.success(des,response.description || '成功');
                            Rental.modal.close();
                            self.props.callBack();
                        } else {
                            Rental.notification.error(des,response.description || '失败');
                        }
                    }, null, des, 'dialogLoading');
                } catch (e) {
                    Rental.notification.error("认领失败", Rental.lang.commonJsError +  '<br />' + e );
                }
            }
		},
		
        editSql:{
            init:function(prams) {
                this.props = _.extend({
					sqlData:null,
                    callBack:function() {}
				}, prams || {});
				this.sqlId = this.props.sqlData.dynamicSqlId;
                this.show();
            },
            show:function(pram) {
                var self = this;
                Rental.modal.open({src:SitePath.base + "data-analysis/sql-edit", type:'ajax', 
                    ajaxContentAdded:function(pModal) {
                        self.initDom(pModal);
                        self.initEvent();	
                    },
                });
            },
            initDom:function(pModal) {
                this.$editSqlForm = $("#editSqlForm");
            },
            initEvent:function() {
                var self = this;
                Rental.form.initFormValidation(self.$editSqlForm,function(form){
                    self.edit();
                });
    
                self.$editSqlForm.on('click', '.cancelButton', function(event) {
                    event.preventDefault();
                    Rental.modal.close();
				});
				
				Rental.ui.renderFormData(self.$editSqlForm, self.props.sqlData);
            },
            edit:function() {
                try {
					var self = this,
						sqlTitle = $('[name=sqlTitle]',self.$editSqlForm).val(),
						sqlContent = $('[name=sqlContent]',self.$editSqlForm).val(),
                        formData = {
							dynamicSqlId:self.sqlId,
							sqlTitle:sqlTitle,
							sqlContent:sqlContent,
						};
    
                    var des = '编辑sql语句';
                    Rental.ajax.submit("{0}dynamicSql/update".format(SitePath.service),formData,function(response){
                        if(response.success) {
                            Rental.notification.success(des,response.description || '成功');
                            Rental.modal.close();
							self.props.callBack();
                        } else {
                            Rental.notification.error(des,response.description || '失败');
                        }
                    }, null, des, 'dialogLoading');
                } catch (e) {
                    Rental.notification.error("认领失败", Rental.lang.commonJsError +  '<br />' + e );
                }
            }
        }
	};

	window.DynamicSqlManage = DynamicSqlManage;

})(jQuery);