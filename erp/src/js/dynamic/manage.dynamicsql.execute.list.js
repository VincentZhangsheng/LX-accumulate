//sql语句列表
;(function($) {
	var DynamicSqlExecuteManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_data_analysis];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_data_analysis_execute_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var pass = this.currentPageAuthor.children[AuthorCode.manage_data_analysis_execute_list_pass_and_execute];
			var refuse = this.currentPageAuthor.children[AuthorCode.manage_data_analysis_execute_list_reject];

			pass && this.rowActionButtons.push(AuthorUtil.button(pass,'passButton','','通过并执行'));
			refuse && this.rowActionButtons.push(AuthorUtil.button(refuse,'refuseButton','','拒绝执行'));
		},
		initDom:function() {
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.render(new Object());
			self.searchData();  //初始化列表数据
			
			// self.$dataListTable.on('mouseover', '.sqlContent', function(event) {
			// 	var nextTr = $(this).closest("tr").next();
			// 	nextTr.find("td").addClass("active")
			// });
			
			self.$dataListTable.on('click', '.sqlContent', function(event) {
				var nextTr = $(this).closest("tr").next();
				nextTr.find("td").toggleClass("active")
            });
            
            self.$dataListTable.on('click', '.passButton', function(event) {
                event.preventDefault();
				var dynamicSqlHolderId = $(this).data("id");
				self.pass(dynamicSqlHolderId)
            });
            
            self.$dataListTable.on('click', '.refuseButton', function(event) {
                event.preventDefault();
				var dynamicSqlHolderId = $(this).data("id");
				self.refuse(dynamicSqlHolderId, function() {
                    self.doSearch(self.Pager.pagerData.currentPage);
                })
			});

			this.$dataListTable.on('click', '.viewButton', function(event) {
				event.preventDefault();
				var result = $(this).data("result");
				$('#resultContent').html("查询结果：" + result + '<button title="Close (Esc)" type="button" class="mfp-close">×</button>');
				Layout.init();

				$.magnificPopup.open({
                    removalDelay: 500,
                    items: {
                        src: "#resultContent"
                    }, 
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = $("#animation-switcher").find('.active-animation').attr('data-effect');
							this.st.mainClass = Animation;
						},
						close:function() {}
                    },
                    midClick: true 
                });
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, prams || {});
			
			Rental.ajax.ajaxData("dynamicSql/adoptList",searchData, "查询sql语句执行情况列表", function(response){
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(sqlData) {
			var self = this;
			
			var listData = sqlData.hasOwnProperty("itemList") ? sqlData.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;

			var data = {
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    statusVal:function() {
                        return Enum.dynamicSqlStatus.getValue(this.status)
					},
					rowActionButtons:function() {
						return self.filterRowButtons(this);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        },
        do:function(url, commitData, des, callBack) {
            Rental.ajax.submit("{0}{1}".format(SitePath.service, url),commitData,function(response){
                if(response.success) {
                    Rental.notification.success(des,response.description || '成功');
                    callBack && callBack();
                } else {
                    Rental.notification.error(des,response.description || '失败');
                }
            }, null, des, "dialogLoading");
        },
		pass:function(dynamicSqlHolderId) {
			var self = this;
			if(!dynamicSqlHolderId) {
				bootbox.alert('没找到sql');
				return;
            }
            var commitData = {
                dynamicSqlHolderId:dynamicSqlHolderId
            }
			bootbox.confirm('确认通过并执行动态sql？',function(result) {
				result && self.do("dynamicSql/adopt", commitData, "审核通过并执行动态sql", function() {
                    self.doSearch(self.Pager.pagerData.currentPage);
                });
			});
        },
        refuse:function(dynamicSqlHolderId, callBack) {
            var self = this;
            if(!dynamicSqlHolderId) {
				bootbox.alert('没找到sql');
				return;
            }
            InputModal.init({
				title:'拒绝执行动态sql原因',
				url:'data-analysis/refuse',
				callBack:function(confirmResult) {
					self.do('dynamicSql/reject', { dynamicSqlHolderId:dynamicSqlHolderId, result:confirmResult.result }, '拒绝执行动态sql原因', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			})
		},
		filterRowButtons:function(dynamicSqlData) {
			var self = this;
			var rowActionButtons = new Array();
			(self.rowActionButtons || []).forEach(function(button, index) {
				rowActionButtons.push(button);

				if(dynamicSqlData.status == Enum.dynamicSqlStatus.num.pass || dynamicSqlData.status == Enum.dynamicSqlStatus.num.reject) {
					rowActionButtons = [];
				}
			});
			return rowActionButtons;
		}
	};

	window.DynamicSqlExecuteManage = DynamicSqlExecuteManage;

})(jQuery);