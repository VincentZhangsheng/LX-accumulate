;(function($){
	var DataAnalysisDynamicSql = {
		state:{
			id:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_data_analysis];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_data_analysis_dynamic_sql];
			// this.initActionButtons();
		},
		initDom:function() {
			this.$form = $("#dynamicsqlForm");
			this.$exprotForm = $("#exportForm");

			this.state.no = Rental.helper.getUrlPara('id');
		},
		initEvent:function(){
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form, function() {
				self.searchData();
			});

			if(!!this.state.no) {
				self.initSqlContent()
			}

			$("#exportExcel").click(function(event) {
				self.export();
			});
		},
		searchData:function() {
			var self = this, formData = Rental.form.getFormData(self.$form);
			var sql  = $.trim($('#sql').val());
			var remark  = $.trim($('#remark').val());
			if(!!sql == false) {
				bootbox.alert('请输入sql语句');
				return;
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service, 'dynamicSql/select'), {sql:sql,remark:remark}, function(response){
	           if(response.success) {
	           		self.render(response);
	           		// $("#resultContainer").html(JSON.stringify(response, null, 4)).removeClass('hide');
	           } else {
	           		Rental.notification.error("动态sql查询失败 ",response.description || '动态sql查询失败');
	           }
	        }, function() {
	        	Rental.notification.error("查询失败 ", '动态sql查询失败');
	        } , '', 'dialogLoading');
		},
		initSqlContent:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('dynamicSql/detail', {dynamicSqlId:self.state.no}, '加载sql详情', function(response) {
				$("#sql").val(response.resultMap.data.sqlContent)
				self.searchData();
			});
		},
		render:function(res) {
			var data = res.resultMap.data;
			if(!!data == false) return;

			var headArr = data.shift();
				headArr.unshift('序号');
			var headStr = headArr.map(function(item) {
				return "<th><span class='nowrap'>"+item+"</span></th>";
			}).join("");

			var datTrList = data.map(function(item, index) {
				var tdArr = new Array();
				tdArr = item.map(function(valueItem) {
					return "<td><span class='nowrap'>"+valueItem+"</span></td>"; 
				});
				tdArr.unshift("<td>"+(index + 1)+"</td>");
				return "<tr>{0}</tr>".format(tdArr.join(''));
			}).join("");

			$("#tableContainer").html('<table class="table table-bordered"><thead><tr id="tbHeader">'+headStr+'</tr></thead><tbody id="tbdoy">'+datTrList+'</tbody></table>');

			$('.table').fixedHeaderTable({
	            // footer: true,
	            altClass: 'odd',
	            height: 500
	        });
		},
		export:function() {
			var self = this; //formData = Rental.form.getFormData(self.$form);
			var sql  = $.trim($('#sql').val());
			if(!!sql == false) {
				bootbox.alert('请输入sql语句');
				return;
			}
			var $sqlInput = $('<input type="text" name="sql" />').val(encodeURIComponent(sql));
			self.$exprotForm.html("");
			self.$exprotForm.append($sqlInput);
			self.$exprotForm.attr({
				'method':'post',
				'action':SitePath.service + 'exportExcel/exportDynamicSql'
			});
			self.$exprotForm.submit();
		}
	}

	window.DataAnalysisDynamicSql = DataAnalysisDynamicSql;

})(jQuery);