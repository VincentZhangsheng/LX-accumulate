;(function($){
	var DataAnalysisDynamicSqlUpdate = {
		state:{
			id:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_data_analysis];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_data_analysis_sql_list_update];
			// this.initActionButtons();
		},
		initDom:function() {
			this.$form = $("#dynamicsqlUpdateForm");

			this.state.no = Rental.helper.getUrlPara('id');
		},
		initEvent:function(){
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form, function() {
				self.updateData();
			});

			if(!!this.state.no) {
				self.initSqlContent()
			}
		},
		updateData:function() {
			var self = this, formData = Rental.form.getFormData(self.$form);
			var sql  = $.trim($('#sql').val());
			var remark  = $.trim($('#remark').val());
			if(!!sql == false) {
				bootbox.alert('请输入sql语句');
				return;
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service, 'dynamicSql/executeDML'), {sql:sql,remark:remark}, function(response){
	           if(response.success) {
					bootbox.alert('更新操作需要被审核才可执行');
	           		// $("#resultContainer").html(JSON.stringify(response, null, 4)).removeClass('hide');
	           } else {
	           		Rental.notification.error("动态sql更新失败 ",response.description || '动态sql更新失败');
	           }
	        }, function() {
	        	Rental.notification.error("更新失败 ", '动态sql更新失败');
	        } , '', 'dialogLoading');
		},
		initSqlContent:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('dynamicSql/detail', {dynamicSqlId:self.state.no}, '加载sql详情', function(response) {
				$("#sql").val(response.resultMap.data.sqlContent)
			});
		},
	}

	window.DataAnalysisDynamicSqlUpdate = DataAnalysisDynamicSqlUpdate;

})(jQuery);