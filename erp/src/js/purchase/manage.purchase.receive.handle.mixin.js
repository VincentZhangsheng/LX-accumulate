;(function($){

	PurchaseReceiveHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			container.on('click', '.confirmButton', function(event) {
				event.preventDefault();
				self.confirm({
					purchaseReceiveNo:$(this).data('no')
				}, handleCallBack);
			});

			container.on('click', '.commitButton', function(event) {
				event.preventDefault();
				self.commit({
					purchaseReceiveNo:$(this).data('no')
				}, handleCallBack);
			});

			container.on('click', '.updateEquipmentRemarkButton', function(event) {
				event.preventDefault();
				self.updateEquipmentRemarkEvent({
					purchaseReceiveNo:$(this).data('no')
				}, handleCallBack);
			});

			container.on('click', '.updateMaterialRemarkButton', function(event) {
				event.preventDefault();
				self.updateMaterialRemarkEvent({
					purchaseReceiveOrderMaterialId:$(this).data('purchasereceiveordermaterialid')
				}, handleCallBack);
			});
		},
		orderDo:function(url, prams, des, callBack) {
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		confirm:function(prams, callBack) {
			var self = this;
			bootbox.confirm('确定收货', function(result) {
				result && self.orderDo('purchaseOrder/confirmPurchaseReceiveOrder', prams, '确认收货', callBack);
			});
		},
		commit:function(prams, callBack) {
			var self = this;
			bootbox.confirm('确定提交', function(result) {
				result && self.orderDo('purchaseOrder/commitPurchaseReceiveOrder', prams, '确定提交', callBack);
			});
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array(),
				unConfirm = order.purchaseReceiveOrderStatus == Enum.purchaseReceiveOrderStatus.num.unConfirm,
				allotTo = order.autoAllotStatus == Enum.autoAllotStatus.num.allotTo,
				beCommit = order.purchaseReceiveOrderStatus == Enum.purchaseReceiveOrderStatus.num.confirmed && order.purchaseReceiveOrderStatus != Enum.purchaseReceiveOrderStatus.num.commited,
				notEditPrice = order.purchaseReceiveOrderStatus != Enum.purchaseReceiveOrderStatus.num.commited || order.autoAllotStatus == Enum.autoAllotStatus.num.allotTo;

			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
				if((button.class == 'confirmButton' || button.class == 'editButton') && !unConfirm || allotTo) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
				if(button.class == 'commitButton' && (!beCommit  || allotTo)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);			
				}
				if(button.class.indexOf('editOrderProductEuqipmentButton') > -1 && notEditPrice) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
				if(button.class.indexOf('editOrderMaterialBulkButton') > -1 && notEditPrice) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
				if((button.class == 'updateEquipmentRemarkButton' || button.class == 'updateMaterialRemarkButton') && (order.purchaseReceiveOrderStatus != Enum.purchaseReceiveOrderStatus.num.confirmed || allotTo)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
				if(button.class == 'prinEquipmentRemarkButton' && ((order.purchaseReceiveOrderStatus != Enum.purchaseReceiveOrderStatus.num.confirmed && order.purchaseReceiveOrderStatus != Enum.purchaseReceiveOrderStatus.num.commited) || allotTo)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
			});
			return rowActionButtons;
		},
		updateEquipmentRemarkEvent:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'添加设备备注',
				url:'purchase-receive-manage-udpate-equipment-remark/modal',
				initFunc:function(modal) {
				},
				callBack:function(result, $modal) {
					self.updateEquipmentRemarkSubmit({
						purchaseReceiveOrderNo:prams.purchaseReceiveNo,
						productEquipment:{
							equipmentNo:result.equipmentNo,
							purchaseReceiveRemark:result.purchaseReceiveRemark
						}
					}, callBack);
					$('[name=equipmentNo]', $modal).val('').focus();
				}
			});
		},
		updateEquipmentRemarkSubmit:function(prams, callBack) {
			this.orderDo('purchaseOrder/updateReceiveEquipmentRemark', prams, '添加设备备注', callBack);
		},
		updateMaterialRemarkEvent:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'添加配件备注',
				url:'common-modal/remark',
				initFunc:function(modal) {},
				callBack:function(result, $modal) {
					var purchaseReceiveOrderMaterialList = new Array();
					purchaseReceiveOrderMaterialList.push({
						purchaseReceiveOrderMaterialId:prams.purchaseReceiveOrderMaterialId,
						remark:result.remark
					})
					self.updateMaterialRemarkSubmit({
						purchaseReceiveOrderNo:self.state.no,
						purchaseReceiveOrderMaterialList:purchaseReceiveOrderMaterialList
					}, callBack);
					return true;
				}
			});
		},
		updateMaterialRemarkSubmit:function(prams, callBack) {
			if(!prams.purchaseReceiveOrderNo) {
				bootbox.alert('找不到收货单编号');
				return;
			}
			this.orderDo('purchaseOrder/updatePurchaseReceiveMaterialRemark', prams, '添加配件备注', callBack);
		}
	}

	window.PurchaseReceiveHandleMixin = PurchaseReceiveHandleMixin;

})(jQuery);