;(function($) {

	var PurchaseReceiveEdit = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_receive];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_receive_edit];
		},
		initDom:function() {
			this.$form = $("#editPurchaseReceiveForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.no = Rental.helper.getUrlPara('no');

			this.chooseProductList = new Array();    //已经选择的商品集合
			this.chooseMaterialList = new Array();   //已经选择的配件集合
		},
		initEvent:function() {
			var self = this;
			
			Breadcrumb.init([self.currentManageAuthor, self.currentManageListAuthor, self.currentPageAuthor]); //面包屑
			Layout.chooseSidebarMenu(AuthorCode.manage_purchase_receive); //激活选中menu菜单
			
			self.loadData(); 

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			//返回
			this.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});

			//选择仓库
			$("#chooseWarehouse").on('click',function(event) {
				WarehouseChoose.init($(this));	
			});

			//选择供应商
			this.$form.on('click', '.chooseSupplier', function(event) {
				event.preventDefault();
				SupplierChoose.init($(this));
			});

			//选择商品
			$("#batchAddProduct").click(function(event) {
				// ProductChoose.init(function(product) {
				// 	self.chooseProduct(product);
				// });
				var array = $('[name=checkItem]', this.$dataListTable).rtlCheckArray();
				if(array.length == 0) {
					bootbox.alert('请选择商品');
					return;
				}
				if(array.length > 1) {
					bootbox.alert('只能选择一个商品');
					return;
				}
				ProductChoose.modal({
					productId:array[0],
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});
			});

			//批量删除商品
			$("#batchDeleteProduct").click(function(event) {
				self.batchDelete();
			});

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			this.renderProductList();

			//删除商品sku项
			this.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
			});

			//编辑商品sku项的配件配置
			this.$dataListTable.on('click', '.editMaterialButton', function(event) {
				event.preventDefault();
				var $panel = $(this).closest('.skuRow').next('.editMaterialTr').find('.editMaterialPannel');
				$panel.slideToggle('fast');
			});

			//选择配件 - 商品sku项的配件选择
			this.$dataListTable.on('click', '.chooseMaterialButton', function(event) {
				event.preventDefault();
				var $this = $(this),  materialType = $this.data('materialtype');
				MaterialChoose.init({
					searchParms:{ materialType:materialType }, 
					callBack:function(material) {
						self.chooseMaterial($this,material);
					}
				});
			});

			//商品sku项目配件-新添加
			this.$dataListTable.on('click', '.newSkuMaterialButton', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow'),
					$materialInputRows = $('.materialInputRow',$materialRow);
					$lastRow = $materialInputRows.last(),
					$cloneRow = $lastRow.clone(true);
				$('.btn-group', $materialRow).show();
				$cloneRow.insertAfter($lastRow);

				var $materialRow = $(this).closest('.materialRow');
				self.changMaterial($materialRow);
			});

			//商品sku项目配件-删除
			this.$dataListTable.on('click', '.delSkuMaterialButton', function(event) {
				event.preventDefault();
				var $materialInputTable = $(this).closest('.materialInputTable');
				if($('.materialInputRow', $materialInputTable).size() == 1) {
					Rental.notification.error("删除配件", '最后一条不能删除');
					return
				};
				 $(this).closest('.materialInputRow').remove();
			});

			//商品项sku的配件信息数量发生变化时
			this.$dataListTable.on('change', '.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changMaterial($materialRow);
			});

			self.renderMaterialList();  //渲染采购单配件项列表
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			//选择采购单项配件
			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms: new Object(), 
					callBack:function(material) {
						self.choosePurcaseOrderItemMaterial(material);
					}
				});
			});

			self.$materialDataListTable.on('change', '.materialAmount,.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.purchaseOrderItemMaterialRow');
				self.changePurcaseOrderItemMaterial($materialRow);
			});

			self.$materialDataListTable.on('click', '.delPurchaseOrderItemMaterialButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.delPurchaseOrderItemMaterial(materialNo);
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDelPurchaseOrderItemMaterial();
			});

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		loadData:function() {
			var self = this;
			if(!self.no) {
				bootbox.alert('没找到采购收料单编号');
				return;
			}
			Rental.ajax.submit("{0}purchaseOrder/queryPurchaseReceiveOrderByNo".format(SitePath.service), {purchaseReceiveNo:self.no}, function(response) {
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载采购收料单详细",response.description || "失败");
				}
			}, null ,"加载采购收料单详细", 'listLoading');
		},
		initData:function(data) {
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.renderBaseInfo(data);
		},
		initChooseProductList:function(data) {
			var data = PurchaseUtil.resolvePurchaseReceiveOrderProductList(data);
			this.chooseProductList = data.hasOwnProperty('purchaseReceiveOrderProductList')  ? PurchaseUtil.initChooseProductList(data.purchaseReceiveOrderProductList) : new Array();
			this.renderProductList();
		},
		initChooseMaterialList:function(data) {
			var data = PurchaseUtil.resolvePurchaseReceiveOrderMaterialList(data);
			this.chooseMaterialList = data.hasOwnProperty('purchaseReceiveOrderMaterialList') ? PurchaseUtil.initChooseMaterialList(data.purchaseReceiveOrderMaterialList) : new Array();
			this.renderMaterialList();
		},
		renderBaseInfo:function(purchaseReceiveOrder) {

			$("#purchaseReceiveNo").html(purchaseReceiveOrder.purchaseReceiveNo);
			$('[name=isNew][value='+purchaseReceiveOrder.isNew+']', this.$form).prop({checked:true});

			var data = {
				dataSource:purchaseReceiveOrder,
				isInvoiceStr:function() {
					return Rental.helper.boolToStr(this.isInvoice);
				},
				isNewStr:function() {
					return Rental.helper.boolToStr(this.isNew);
				},
				warehouseName:function() {
					return this.hasOwnProperty('warehouseSnapshot') ? JSON.parse(this.warehouseSnapshot).warehouseName : '';
				},
				autoAllotStatusStr:function() {
					switch(this.autoAllotStatus) {
						case 0:
							return '未分拨';
						case 1:
							return '已分拨';
						case 2:
							return '被分拨';
					}
				},
				createTimeFormat:function() {
					return Rental.helper.timestampFormat(this.createTime);
				},
				updateTimeFormat:function() {
					return Rental.helper.timestampFormat(this.updateTime);
				},
				confirmTimeFormat:function() {
					return Rental.helper.timestampFormat(this.confirmTime);
				},
				purchaseOrderAmountTotalFixed:function() {
					return this.purchaseOrderAmountTotal && '￥' + this.purchaseOrderAmountTotal.toFixed(2);
				},
				purchaseOrderAmountRealFixed:function() {
					return this.purchaseOrderAmountReal && '￥' + this.purchaseOrderAmountReal.toFixed(2);
				},
				purchaseOrderAmountStatementFixed:function() {
					return this.purchaseOrderAmountStatement && '￥' + this.purchaseOrderAmountStatement.toFixed(2);
				},
			}
			var dataTpl = $("#baserInfoTpl").html();
			Mustache.parse(dataTpl);
			$("#baseInfo").html(Mustache.render(dataTpl, data));
		},
		chooseProduct:function(product) {
			var self = this;

			self.chooseProductList = PurchaseUtil.chooseProduct(self.chooseProductList, product);

			self.renderProductList();

			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		chooseMaterial:function($button, material) {
			var parnets = $button.closest('.materialInputRow');
			$('.materialCapacityValue',parnets).val(material.materialCapacityValue);
			$('.materialNo',parnets).val(material.materialNo);
			$('.materialName',parnets).val(material.materialName);

			//更新数据源
			this.changMaterial($button.closest('.materialRow'));
		},
		changMaterial:function($materialRow) {
			PurchaseUtil.changMaterial($materialRow, this.chooseProductList);
		},
		renderProductList:function() {
			this.chooseProductList = PurchaseUtil.keepSkuInputValue(this.$dataListTable, this.chooseProductList);  
			PurchaseUtil.renderProductList(this.chooseProductList || [], this.$dataListTpl, this.$dataListTable);
		},
		deleteSku:function($deleteButton) {
			this.chooseProductList = PurchaseUtil.deleteSku($deleteButton, this.chooseProductList);
			if(this.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		batchDelete:function() {
			this.chooseProductList = PurchaseUtil.batchDelete(this.$dataListTable, this.chooseProductList);
			this.renderProductList();
		},
		//选择采购单项配件
		choosePurcaseOrderItemMaterial:function(material) {
			this.chooseMaterialList = PurchaseUtil.choosePurcaseOrderItemMaterial(material, this.chooseMaterialList);
			this.renderMaterialList();
		},
		changePurcaseOrderItemMaterial:function($materialRow) {
			this.chooseMaterialList = PurchaseUtil.changePurcaseOrderItemMaterial($materialRow, this.chooseMaterialList);
		},
		delPurchaseOrderItemMaterial:function(materialNo) {
			var currMaterialIndex = PurchaseUtil.getChooseMaterialIndex(materialNo, this.chooseMaterialList);
			this.chooseMaterialList.splice(currMaterialIndex,1);			
			this.renderMaterialList();
		},
		batchDelPurchaseOrderItemMaterial:function() {
			this.chooseMaterialList = PurchaseUtil.batchDelPurchaseOrderItemMaterial(this.$materialDataListTable, this.chooseMaterialList);
			this.renderMaterialList();
		},
		renderMaterialList:function() {
			PurchaseUtil.renderMaterialList(this.chooseMaterialList || [], this.$materialDataListTpl, this.$materialDataListTable);
		},
		getSkuInputValue:function($dataListTable) {
			return PurchaseUtil.getSkuInputValueFunc($dataListTable, function($row) {
				var inputSku = {
					purchaseReceiveOrderProductId:$.trim($('.purchaseReceiveOrderProductId',$row).val()),
					// productId:$.trim($('.productId',$row).val()),
					realProductSkuId:$.trim($('.productSkuId',$row).val()),
					realProductCount:$.trim($('.productCount',$row).val()),
					remark:$.trim($('.remark',$row).val()),
				};
				return inputSku;
			});
		},
		getPurchaseReceiveOrderMaterialList:function(chooseMaterialList) {
			return chooseMaterialList.map(function(item) {
				return {
					realMaterialNo:item.materialNo,
					realMaterialCount:item.materialCount,
					remark:item.remark || '',
				}
			})
		},
		edit:function() {
			var self = this;
			try {

				if(!self.no) {
					bootbox.alert('找不到收货单编号');
					return;
				}

				var formData = Rental.form.getFormData(self.$form);
				
				if(!formData['isNew']) {
					bootbox.alert('请选择"是否是全新机器"');
					return;
				}

				var commitData = {
					purchaseReceiveNo:self.no,
					isNew:formData['isNew'],
				}

				var purchaseReceiveOrderProductList = this.getSkuInputValue(self.$dataListTable);
				var purchaseReceiveOrderMaterialList = this.getPurchaseReceiveOrderMaterialList(self.chooseMaterialList);

				if(purchaseReceiveOrderProductList.length == 0 && purchaseReceiveOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(purchaseReceiveOrderProductList.length > 0) {
					commitData.purchaseReceiveOrderProductList = purchaseReceiveOrderProductList;
				}

				if(purchaseReceiveOrderMaterialList.length > 0) {
					commitData.purchaseReceiveOrderMaterialList = purchaseReceiveOrderMaterialList;
				}

				Rental.ajax.submit("{0}purchaseOrder/updatePurchaseReceiveOrder".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success("编辑收货单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑收货单",response.description || '失败');
					}
				}, null ,"编辑收货单", 'dialogLoading');
			} catch(e) {
				Rental.notification.error("编辑收货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑收货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.loadData();
				        }
				    }
				});
			}
		}
	};

	window.PurchaseReceiveEdit = PurchaseReceiveEdit;

})(jQuery);