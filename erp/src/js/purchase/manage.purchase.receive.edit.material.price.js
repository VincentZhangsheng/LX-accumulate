;(function($) {

	var EditMaterialPrice = {
		init:function(prams) {
			this.props = _.extend({
				purchaseReceiveOrderNo:null,
				purchaseReceiveOrderMaterialId:null,
				materialCount:null,
				materialAmount:null,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "purchase-receive-manage-material-bulk/edit", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$cancelButton  = $('.cancelButton', this.$modal);
			this.$searchForm = $(".searchForm", this.$modal);
			this.$editForm = $('.editForm', this.$modal);
			this.$dataListTpl = $(".dataListTpl", this.$modal).html();
			this.$dataListTable = $(".dataListTable", this.$modal);

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Rental.form.initFormValidation(this.$editForm, function(form){
				self.save();
			});

			self.$cancelButton.click(function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.$dataListTable.on('click', '.addRow', function(event) {
				event.preventDefault();
				var $row = $(this).closest('.materialRow');
					$cloneRow = $row.clone(true, true);
				$row.after($cloneRow);
				$('.delRow', $cloneRow).removeClass('hide');
			});

			self.$dataListTable.on('click', '.delRow', function(event) {
				event.preventDefault();
			 	$(this).closest('.materialRow').remove();
			});

			self.searchData();
		},
		searchData:function(prams) {
			var self = this;
			if(!self.props.purchaseReceiveOrderMaterialId) {
				bootbox.alert('找不到收货单配件项ID');
				return;
			}
			Rental.ajax.submit("{0}purchaseOrder/getPurchaseReceiveMaterialPriceList".format(SitePath.service), { purchaseReceiveOrderMaterialId:self.props.purchaseReceiveOrderMaterialId }, function(response){
				if(response.success) {
					self.renderList(response.resultMap.data);	
				} else {
					Rental.notification.error("加载配件价格列表",response.description);
				}
			}, null ,"加载配件价格列表",'listLoading');
		},
		renderList:function(data) {
			$('.materialCount', this.$modal).html(this.props.materialCount);
			$('.materialAmount', this.$modal).html("￥"+this.props.materialAmount);

			var self = this;
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":data,
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		getPriceList:function() {
			return $('.materialRow', this.$modal).map(function(index,item) {
				return {
					count:$('input.count', $(this)).val(),
					price:$.trim($('input.price', $(this)).val()),
				}
			}).toArray();
		},
		save:function() {
			var self = this;
			try {

				if(!self.props.purchaseReceiveOrderMaterialId) {
					bootbox.alert('找不到收货单配件项ID');
					return false;
				}

				var priceList = self.getPriceList();
				var totalCount = priceList.reduce(function(total,item) {
					total += parseInt(item.count);
					return total;
				}, 0);

				if(totalCount > self.props.materialCount) {
					bootbox.alert('所填数量之和不能超过采购数量');
					return;
				}

				var commitData = {
					purchaseReceiveOrderMaterialId:self.props.purchaseReceiveOrderMaterialId,
					purchaseReceiveOrderMaterialPriceList:priceList,
				}

				var des = "编辑配件采购价格";
				Rental.ajax.submit("{0}purchaseOrder/updatePurchaseReceiveMaterialPrice".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null , des, 'dialogLoading');

			} catch(e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function() {
			self.props.callBack && self.props.callBack();
		}
	};

	window.EditMaterialPrice = EditMaterialPrice;

})(jQuery)