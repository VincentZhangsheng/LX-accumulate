;(function($){
	
	var EditPurchase = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_edit];
		},
		initDom:function() {
			this.$form = $("#editPurchaseForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.purchaseNo = Rental.helper.getUrlPara('no');
			this.chooseProductList = new Array();    //已经选择的商品集合
			this.chooseMaterialList = new Array();   //已经选择的配件集合
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_purchase_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.initCommonEvent();
		},
		loadData:function() {
			var self = this;
			if(!self.purchaseNo) {
				bootbox.alert('没找到采购单号');
				return;
			}
			Rental.ajax.ajaxData('purchaseOrder/queryPurchaseOrderByNo', {purchaseNo:self.purchaseNo}, '加载采购单', function(response) {
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.initFormData(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		initChooseProductList:function(data) {
			this.chooseProductList = data.hasOwnProperty('purchaseOrderProductList')  ? PurchaseUtil.initChooseProductList(data.purchaseOrderProductList) : new Array();
			this.renderProductList();
		},
		initChooseMaterialList:function(data) {
			this.chooseMaterialList = data.hasOwnProperty('purchaseOrderMaterialList') ? PurchaseUtil.initChooseMaterialList(data.purchaseOrderMaterialList) : new Array();
			this.renderMaterialList();
		},
		initFormData:function(data) {
			if(!data) return;
			
			var self = this;

			var array = self.$form.serializeArray();  

			$.each(array,function(i,item){
				if(data.hasOwnProperty(item.name)) {
					var dom = self.$form.find("[name="+item.name+"]");
	            	dom.val(data[item.name]);
	            	dom.data('defaultvalue',data[item.name]);
				}
	        });

			//仓库名称读取快照仓库名称
	        if(data.hasOwnProperty('warehouseSnapshot')) {
	        	var warehouseSnapshot = JSON.parse(data.warehouseSnapshot);
	        	$('[name=warehouseName]', self.$form).val(warehouseSnapshot.warehouseName);
	        	$('[name=warehouseNo]', self.$form).val(warehouseSnapshot.warehouseNo);
	        }

	        $('[name=isNew][value='+data.isNew+']', self.$form).prop({checked:true});
	        $('[name=isInvoice][value='+data.isInvoice+']', self.$form).prop({checked:true});
		},
		edit:function() {
			var self = this;
			try {

				if(!self.purchaseNo) {
					bootbox.alert('找不到采购单编号');
					return;
				}

				var formData = Rental.form.getFormData(self.$form);
				
				if(!formData['isNew']) {
					bootbox.alert('请选择"是否是全新机器"');
					return;
				}

				if(!formData['isInvoice']) {
					bootbox.alert('请选择"是否有发票"');
					return;
				}

				var commitData = {
					purchaseNo:self.purchaseNo,
					productSupplierId:formData['productSupplierId'],
					invoiceSupplierId:formData['invoiceSupplierId'],
					warehouseNo:formData['warehouseNo'],
					isInvoice:formData['isInvoice'],
					isNew:formData['isNew'],
					purchaseType:formData['purchaseType'],
					taxRate:formData['taxRate'],
				}

				var purchaseOrderProductList = PurchaseUtil.getSkuInputValue(self.$dataListTable);
				var purchaseOrderMaterialList = PurchaseUtil.getPurcaseOrderItemMaterialList(self.chooseMaterialList);

				if(purchaseOrderProductList.length == 0 && purchaseOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(purchaseOrderProductList.length > 0) {
					commitData.purchaseOrderProductList = purchaseOrderProductList;
				}

				if(purchaseOrderMaterialList.length > 0) {
					commitData.purchaseOrderMaterialList = purchaseOrderMaterialList;
				}

				Rental.ajax.submit("{0}purchaseOrder/update".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success("编辑采购单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑采购单",response.description || '失败');
					}
				}, null ,"编辑采购单", 'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建采购单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑采购单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	// self.loadData();
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.EditPurchase = _.extend(EditPurchase, PurchaseMixin);

})(jQuery);