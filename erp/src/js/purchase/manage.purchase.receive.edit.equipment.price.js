;(function($) {

	var EditQquipmentPrice = {
		init:function(prams) {
			this.props = _.extend({
				purchaseReceiveOrderNo:null,
				purchasePrice:null,
				purchaseReceiveOrderProductId:null,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "purchase-receive-manage-product-equipment/edit", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$cancelButton  = $('.cancelButton', this.$modal);
			this.$searchForm = $(".searchForm", this.$modal);
			this.$editForm = $('.editForm', this.$modal);
			this.$dataListTpl = $(".dataListTpl", this.$modal).html();
			this.$dataListTable = $(".dataListTable", this.$modal);

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			Rental.form.initFormValidation(this.$editForm, function(form){
				self.save();
			});

			self.$cancelButton.click(function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.searchData();
		},
		searchData:function(prams) {
			var self = this;

			if(!self.props.purchaseReceiveOrderProductId) {
				bootbox.alert('找不到收货单商品项ID');
				return;
			}

			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				purchaseReceiveOrderProductId:self.props.purchaseReceiveOrderProductId
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}purchaseOrder/pageReceiveOrderProductEquipment".format(SitePath.service), commitData, function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载设备列表",response.description);
				}
			}, null ,"加载设备列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', self.$modal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					purchasePrice:function() {
						return !!this.purchasePrice ? this.purchasePrice.toFixed(2) : this.srcPurchasePrice;
					},
					isEditPurchasePrice:function() {
						return !!this.purchasePrice;
					},
					isEditPurchasePriceText:function() {
						return !!this.purchasePrice ? '已编辑' : '未编辑';
					},
					equipmentStatusStr:function() {
						return Enum.equipmentStatus.getValue(this.equipmentStatus);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		getPriceList:function() {
			return $('.equipmentTr').map(function(index,item) {
				return {
					equipmentNo:$('input.equipmentNo', $(this)).val(),
					purchasePrice:$.trim($('input.purchasePrice', $(this)).val()),
				}
			}).toArray();
		},
		save:function() {
			var self = this;
			try {

				if(!self.props.purchaseReceiveOrderNo) {
					bootbox.alert('找不到收货单编号');
					return false;
				}

				var commitData = {
					purchaseReceiveOrderNo:self.props.purchaseReceiveOrderNo,
					equipmentList:self.getPriceList(),
				}

				var des = "编辑设备采购价格";
				Rental.ajax.submit("{0}purchaseOrder/updatePurchaseReceiveEquipmentPrice".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null , des, 'dialogLoading');

			} catch(e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function() {
			this.doSearch(this.Pager.pagerData.currentPage);
			this.props.callBack && this.props.callBack();
		}
	};

	window.EditQquipmentPrice = EditQquipmentPrice;

})(jQuery)