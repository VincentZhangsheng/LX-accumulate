/**
* 收货单
*/
;(function($) {

	var PurchaseReceiveManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_receive];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var edit = this.currentPageAuthor.children[AuthorCode.manage_purchase_receive_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_purchase_receive_detail],
				confirm = this.currentPageAuthor.children[AuthorCode.manage_purchase_receive_confirm],
				commit = this.currentPageAuthor.children[AuthorCode.manage_purchase_receive_commit];

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			confirm && this.rowActionButtons.push(AuthorUtil.button(confirm,'confirmButton','','确认收货'));
			commit && this.rowActionButtons.push(AuthorUtil.button(commit,'commitButton','','提交'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.purchaseReceive);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			self.searchData();  //初始化列表数据

			self.renderCommonActionButton(); //渲染操作按钮及事件

			// self.initDatePicker();

			Rental.ui.renderSelect({
				container:$('#purchaseReceiveOrderStatus'),
				data:Enum.array(Enum.purchaseReceiveOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（收货状态）'
			});

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			self.initHandleEvent(self.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(PurchaseReceiveManage.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.purchaseReceive, searchData);
			
			Rental.ajax.submit("{0}purchaseOrder/pagePurchaseReceive".format(SitePath.service),searchData,function(response){
				if(response.success) {
					PurchaseReceiveManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询采购收货单列表",response.description || '失败');
				}
			}, null ,"查询采购收货单列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			this.renderList(data);
			this.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.purchaseReceiveDetail, this.purchaseReceiveNo);
					},
					purchaseDetailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.purchaseDetail, this.purchaseOrderNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					autoAllotStatusStr:function() {
						return Enum.autoAllotStatus.getValue(this.autoAllotStatus);
					},
					purchaseReceiveOrderStatusValue:function() {
						return Enum.purchaseReceiveOrderStatus.getValue(this.purchaseReceiveOrderStatus);
					},
					purchaseReceiveOrderStatusClass:function() {
						return Enum.purchaseReceiveOrderStatus.getClass(this.purchaseReceiveOrderStatus);
					},
					isInvoiceStr:function() {
						return Rental.helper.boolToStr(this.isInvoice);
					},
					isNewStr:function() {
						return Rental.helper.boolToStr(this.isNew);
					},
					purchaseOrderStatusStr:function() {
						return Enum.purchaseOrderStatus.getValue(this.purchaseOrderStatus);
					},
					warehouse:function() {
						if(this.hasOwnProperty('warehouseSnapshot')) {
							var warehouse = JSON.parse(this.warehouseSnapshot);
							return warehouse.subCompanyName + " " + warehouse.warehouseName;
						}
						return "";
					},
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.PurchaseReceiveManage = _.extend(PurchaseReceiveManage, PurchaseReceiveHandleMixin);

})(jQuery);