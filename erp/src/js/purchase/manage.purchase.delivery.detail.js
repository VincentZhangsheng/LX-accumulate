;(function($) {

	var PurchaseDeliveryDetail = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_delivery];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_delivery_detail];
		},
		initDom:function() {
			this.$form = $("#detailPurchaseForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.no = Rental.helper.getUrlPara('no');

			this.chooseProductList = new Array();    //已经选择的商品集合
			this.chooseMaterialList = new Array();   //已经选择的配件集合
		},
		initEvent:function() {
			var self = this;
			
			Breadcrumb.init([self.currentManageAuthor, self.currentManageListAuthor, self.currentPageAuthor]); //面包屑
			Layout.chooseSidebarMenu(AuthorCode.manage_purchase_delivery); //激活选中menu菜单
			
			self.loadData(); 

			this.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			this.$dataListTable.on('click', '.showMaterialButton', function(event) {
				event.preventDefault();
				var $panel = $(this).closest('.skuRow').next('.editMaterialTr').find('.editMaterialPannel');
				$panel.slideToggle('fast');
			});
		},
		loadData:function() {
			var self = this;
			if(!self.no) {
				bootbox.alert('没找到采购发货单编号');
				return;
			}
			Rental.ajax.submit("{0}purchaseOrder/queryPurchaseDeliveryOrderByNo".format(SitePath.service), {purchaseDeliveryNo:self.no}, function(response) {
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载采购发货单详细",response.description || "失败");
				}
			}, null ,"加载采购发货单详细");
		},
		initData:function(data) {
			this.renderBaseInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		initChooseProductList:function(data) {
			this.chooseProductList = data.hasOwnProperty('purchaseDeliveryOrderProductList')  ? PurchaseUtil.initChooseProductList(data.purchaseDeliveryOrderProductList) : new Array();
			this.renderProductList();
		},
		initChooseMaterialList:function(data) {
			this.chooseMaterialList = data.hasOwnProperty('purchaseDeliveryOrderMaterialList') ? PurchaseUtil.initChooseMaterialList(data.purchaseDeliveryOrderMaterialList) : new Array();
			this.renderMaterialList();
		},
		renderBaseInfo:function(purchaseDeliveryOrder) {
			$("#purchaseDeliveryNo").html(purchaseDeliveryOrder.purchaseDeliveryNo);
			var data = {
				dataSource:purchaseDeliveryOrder,
				isInvoiceStr:function() {
					return Rental.helper.boolToStr(this.isInvoice);
				},
				isNewStr:function() {
					return Rental.helper.boolToStr(this.isNew);
				},
				warehouseName:function() {
					return this.hasOwnProperty('warehouseSnapshot') ? JSON.parse(this.warehouseSnapshot).warehouseName : '';
				},
				createTimeFormat:function() {
					return Rental.helper.timestampFormat(this.createTime);
				},
				updateTimeFormat:function() {
					return Rental.helper.timestampFormat(this.updateTime);
				},
				deliveryTimeFormat:function() {
					return Rental.helper.timestampFormat(this.deliveryTime);
				},
				purchaseDeliveryOrderStatusValue:function() {
					switch(this.purchaseDeliveryOrderStatus) {
						case 0:
							return "待发货";
						case 1:
							return "已发货";
					}
					return "";
				}
			}
			var dataTpl = $("#baserInfoTpl").html();
			Mustache.parse(dataTpl);
			$("#baseInfo").html(Mustache.render(dataTpl, data));
		},
		renderProductList:function() {
			PurchaseUtil.renderProductList(this.chooseProductList || [], this.$dataListTpl, this.$dataListTable);
		},
		renderMaterialList:function() {
			PurchaseUtil.renderMaterialList(this.chooseMaterialList || [], this.$materialDataListTpl, this.$materialDataListTable);
		},
	};

	window.PurchaseDeliveryDetail = PurchaseDeliveryDetail;

})(jQuery);