;(function($){

	PurchaseHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			// container.on('click', '.submitAuditButton', function(event) {
			// 	event.preventDefault();
			// 	var purchaseNo = $(this).data('purchaseno');
			// 	self.isNeedVerify(purchaseNo, handleCallBack);
			// });

			container.on('click', '.submitAuditButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('purchaseno'),
					workflowType:Enum.workflowType.num.purchase,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.deleteButton', function(event) {
				event.preventDefault();
				var purchaseno = $(this).data('purchaseno');
				bootbox.confirm('取消采购单？',function(result) {
					result && self.delPurchaseOrder(purchaseno, handleCallBack);
				});
			});

			container.on('click', '.compelDelButton', function(event) {
				event.preventDefault();
				var purchaseno = $(this).data('purchaseno');
				bootbox.confirm('强制取消采购单？',function(result) {
					result && self.compelDelPurchaseOrder(purchaseno, handleCallBack);
				});
			});

			container.on('click', '.continueToPurchaseButton', function(event) {
				event.preventDefault();
				var purchaseno = $(this).data('purchaseno');
				bootbox.confirm('继续发起采购？',function(result) {
					result && self.continueToPurchase(purchaseno, handleCallBack);
				})
			});

			container.on('click', '.endPurchaseButton', function(event) {
				event.preventDefault();
				var purchaseno = $(this).data('purchaseno');
				bootbox.confirm('结束采购订单？',function(result) {
					result && self.endPurchase(purchaseno, handleCallBack);
				})
			});
		},
		purchaseDo:function(url, purchaseNo, des, callBack) {
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url),{purchaseNo:purchaseNo},function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					//self.doSearch(self.Pager.pagerData.currentPage);
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des);
		},
		delPurchaseOrder:function(purchaseNo, callBack) {
			this.purchaseDo('purchaseOrder/cancel', purchaseNo, '取消采购单', callBack);
		},
		compelDelPurchaseOrder:function(purchaseNo, callBack) {
			this.purchaseDo('purchaseOrder/strongCancel', purchaseNo, '强制取消采购单', callBack);
		},
		continueToPurchase:function(purchaseNo, callBack) {
			this.purchaseDo('purchaseOrder/continuePurchaseOrder', purchaseNo, '继续采购', callBack);
		},
		endPurchase:function(purchaseNo, callBack) {
			this.purchaseDo('purchaseOrder/endPurchaseOrder', purchaseNo, '结束采购', callBack);	
		},
		// isNeedVerify:function(purchaseNo, callBack) {
		// 	if(!purchaseNo) {
		// 		bootbox.alert('找不到采购单编号');
		// 		return;
		// 	}
		// 	var self = this;
		// 	Rental.ajax.submit("{0}workflow/isNeedVerify".format(SitePath.service),{workflowType:Enum.workflowType.num.purchase},function(response){
		// 		if(response.success) {
		// 			if(response.resultMap.data) {
		// 				self.submittoAudit({purchaseNo:purchaseNo}, callBack);
		// 			} else {
		// 				self.submitOrder({purchaseNo:purchaseNo}, callBack);
		// 			}
		// 		} else {
		// 			Rental.notification.error('请求提交',response.description || '失败');
		// 		}
		// 	}, null ,'请求提交');
		// },
		// submittoAudit:function(prams, callBack) {
		// 	var self = this;
		// 	SubmitAudit.init({
		// 		workflowType:Enum.workflowType.num.purchase,
		// 		workflowReferNo:prams.purchaseNo,
		// 		callBack:function(res) {
		// 			var commitData = {
		// 				purchaseNo:prams.purchaseNo,
		// 				verifyUserId:res.verifyUser,
		// 				remark:res.commitRemark,
		// 			}
		// 			self.submitOrder(commitData, callBack);
		// 		}
		// 	})
		// },
		submitOrder:function(prams, callBack, isAudit) {
			// var self = this;
			// Rental.ajax.submit("{0}purchaseOrder/commit".format(SitePath.service),prams,function(response) {
			// 	if(response.success) {
			// 		Rental.notification.success('提交采购单',response.description || '成功');
			// 		callBack && callBack();
			// 		Rental.modal.close();
			// 	} else {
			// 		Rental.notification.error('提交采购单',response.description || '失败');
			// 	}
			// }, null ,'提交采购单');

			var self = this;
			if(!isAudit) {
				bootbox.confirm('确认{0}？'.format(des), function(result) {
					result && self.submitOrderFunc({
						purchaseNo:prams.no,
					}, callBack);
				});
				return;
			} else {
				self.submitOrderFunc({
					purchaseNo:prams.no,
					verifyUserId:prams.verifyUserId,
					remark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		submitOrderFunc:function(prams, callBack) {
			var self = this;
			Rental.ajax.submit("{0}purchaseOrder/commit".format(SitePath.service), prams, function(response) {
				if(response.success) {
					Rental.notification.success('提交采购单',response.description || '成功');
					callBack && callBack();
					Rental.modal.close();
				} else {
					Rental.notification.error('提交采购单',response.description || '失败');
				}
			}, null ,'提交采购单');
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array(),
				unSubmit = order.purchaseOrderStatus == Enum.purchaseOrderStatus.num.unSubmit,
				partPurchase = order.purchaseOrderStatus == Enum.purchaseOrderStatus.num.partPurchase,
				allPurchase =  order.purchaseOrderStatus == Enum.purchaseOrderStatus.num.allPurchase,
				canCompelDelButton = order.purchaseOrderStatus == Enum.purchaseOrderStatus.num.inReview || order.purchaseOrderStatus == Enum.purchaseOrderStatus.num.purchasing;
			
			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
				if((button.class == 'submitAuditButton' || button.class == 'editButton' || button.class == 'deleteButton') && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if(button.class == 'continueToPurchaseButton' && !partPurchase) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);	
				}
				if(button.class == 'endPurchaseButton' && !(allPurchase || partPurchase)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
				if(button.class == 'compelDelButton' && !canCompelDelButton) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);		
				}
			});
			return rowActionButtons;
		}
	}

	window.PurchaseHandleMixin = PurchaseHandleMixin;

})(jQuery);