;(function($) {

	var PurchaseDeliveryManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_delivery];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_purchase_delivery_detail];
			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$createStartTime = $("#createStartTime");
			this.$createEndTime = $("#createEndTime");
			this.$createTimePicker = $('#createTimePicker');
			this.$createTimePickerInput = $('#createTimePickerInput');
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.purchaseDelivery);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			self.render(new Object());

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			self.initDatePicker();

			self.initPurchaseReceiveOrderStatusSelect();

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		initDatePicker:function() {
			var self = this;
			// var defaultStart = moment().subtract('days', 29), defaultEnd = moment();
			// this.setSearchDate(defaultStart.format('YYYY-MM-DD'),defaultEnd.format('YYYY-MM-DD'));
			this.$createTimePicker.daterangepicker({
				"autoApply": true,
				format:'YYYY-MM-DD',
			    // "startDate":defaultStart,
			    // "endDate": defaultEnd
			}, function(start, end, label) {
				self.setSearchDate(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
			});
		},
		initPurchaseReceiveOrderStatusSelect:function() {
			var self = this;
			$('#purchaseReceiveOrderStatus').multiselect({
				 buttonClass: 'multiselect dropdown-toggle btn btn-sm btn-default light mr10'
			}).change(function(event) {
				self.searchData();
			});
		},
		setSearchDate:function(start,end) {
			this.$createStartTime.val(new Date(start + " 00:00:00").getTime());
			this.$createEndTime.val(new Date(end + " 23:59:59").getTime());
			this.$createTimePickerInput.val("{0} - {1}".format(start,end));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(PurchaseDeliveryManage.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.purchaseDelivery, searchData);
			
			// Rental.ajax.submit("{0}purchaseOrder/pagePurchaseDelivery".format(SitePath.service),searchData,function(response){

			// 	if(response.success) {
			// 		PurchaseDeliveryManage.render(response.resultMap.data);	
			// 	} else {
			// 		Rental.notification.error("查询采购单列表",response.description || '失败');
			// 	}
				
			// }, null ,"查询采购单列表",'listLoading');

			Rental.ajax.ajaxData('purchaseOrder/pagePurchaseDelivery', searchData, '查询采购单列表', function(response) {
				PurchaseDeliveryManage.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.purchaseDeliveryDetail, this.purchaseDeliveryNo);
					},
					purchaseDetailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.purchaseDetail, this.purchaseOrderNo);
					},
					purchaseDeliveryOrderStatusValue:function() {
						switch(this.purchaseDeliveryOrderStatus) {
							case 0:
								return '待收货';
							case 1:
								return '已发货';
						}
					},
					isInvoiceStr:function() {
						return Rental.helper.boolToStr(this.isInvoice);
					},
					isNewStr:function() {
						return Rental.helper.boolToStr(this.isNew);
					},
					warehouse:function() {
						if(this.hasOwnProperty('warehouseSnapshot')) {
							var warehouse = JSON.parse(this.warehouseSnapshot);
							return warehouse.subCompanyName + " " + warehouse.warehouseName;
						}
						return "";
					},
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.PurchaseDeliveryManage = PurchaseDeliveryManage;

})(jQuery);