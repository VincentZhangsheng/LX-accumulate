;(function($){
	
	var AddPurchase = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_add];
		},
		initDom:function() {
			this.$form = $("#addPurchaseForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.chooseProductList = new Array();    //已经选择的商品集合
			this.chooseMaterialList = new Array();   //已经选择的配件集合
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_purchase_list); //激活选中menu菜单

			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});	

			self.initCommonEvent();
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);
				
				if(!formData['isNew']) {
					bootbox.alert('请选择"是否是全新机器"');
					return;
				}

				if(!formData['isInvoice']) {
					bootbox.alert('请选择"是否有发票"');
					return;
				}

				var commitData = {
					productSupplierId:formData['productSupplierId'],
					invoiceSupplierId:formData['invoiceSupplierId'],
					warehouseNo:formData['warehouseNo'],
					isInvoice:formData['isInvoice'],
					isNew:formData['isNew'],
					purchaseType:formData['purchaseType'],
					taxRate:formData['taxRate'],
				}

				var purchaseOrderProductList = PurchaseUtil.getSkuInputValue(self.$dataListTable);
				var purchaseOrderMaterialList = PurchaseUtil.getPurcaseOrderItemMaterialList(self.chooseMaterialList);

				if(purchaseOrderProductList.length == 0 && purchaseOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(purchaseOrderProductList.length > 0) {
					commitData.purchaseOrderProductList = purchaseOrderProductList;
				}

				if(purchaseOrderMaterialList.length > 0) {
					commitData.purchaseOrderMaterialList = purchaseOrderMaterialList;
				}

				Rental.ajax.submit("{0}purchaseOrder/add".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success("创建采购单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("创建采购单",response.description || '失败');
					}
				}, null ,"创建采购单", 'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建采购单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建采购单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.AddPurchase = _.extend(AddPurchase, PurchaseMixin);

})(jQuery);