;(function($) {

	PurchaseUtil = {
		chooseProduct:function(chooseProductList, product) {	
			if(!chooseProductList) {
				chooseProductList = new Array();
			}	
			if(chooseProductList.length > 0) {
				var currProductIndex = _.findIndex(chooseProductList, {productId:product.productId});
				var currProduct = currProductIndex > -1 ? chooseProductList[currProductIndex] : null;

				if(currProduct) {
					product.chooseProductSkuList.map(function(item) {
						var index = _.findIndex(currProduct.chooseProductSkuList, {skuId:item.skuId});
						if(index > -1) {
							currProduct.chooseProductSkuList[index] = _.extend(currProduct.chooseProductSkuList[index], item);
						} else {
							currProduct.chooseProductSkuList.push(item);
						}
					});
				} else {
					chooseProductList.push(product);
				}
			} else {
				chooseProductList.push(product);
			}

			chooseProductList = chooseProductList.map(function(itemProduct) {
				itemProduct.chooseProductSkuList = itemProduct.chooseProductSkuList.map(function(itemSku) {
					//在sku下申明一个shouldMaterialList变量
					if(!itemSku.hasOwnProperty("shouldMaterialList")) {
						itemSku.shouldMaterialList = itemSku.shouldProductCategoryPropertyValueList.map(function(shouldItem) {
							shouldItem.materialList = new Array();
							if(itemSku.hasOwnProperty('productMaterialList')) {
								// shouldItem.materialList = _.where(itemSku.productMaterialList, {materialType:shouldItem.materialType, materialModelId: shouldItem.materialModelId})
								if(shouldItem.hasOwnProperty('materialModelId')) {
									shouldItem.materialList = _.where(itemSku.productMaterialList, {materialType:shouldItem.materialType, materialModelId: shouldItem.materialModelId})	
								} else {
									shouldItem.materialList = _.where(itemSku.productMaterialList, {materialType:shouldItem.materialType})
								}
							}

							if(shouldItem.materialList.length == 0) {
								shouldItem.materialList.push({
									materialNo:'',
									materialName:'',
									materialCount:1, //默认数量为1,
								});
							}
							//可以新增配件，配件数值得时候，可以由不同数值的配件组成，这时候可能存在多条，需要新增。
							shouldItem.canAdd = shouldItem.hasOwnProperty('propertyCapacityValue'); 
							return shouldItem;
						});
					}
					return itemSku;
				});
				return itemProduct;
			});

			return chooseProductList;
		},
		renderProductList:function(listData, tpl, container, rowActionButtons) {
			rowActionButtons = rowActionButtons || new Array();
			$("#orderItemProductCount").html(listData.length);

			var skuList = new Array();
			if(listData) {
				listData.forEach(function(item) {
					skuList = skuList.concat(item.chooseProductSkuList);
				});
			}

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":skuList,
					rowActionButtons:rowActionButtons,
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '在租':"下架";
					},
					propertiesToStr:function() {
						var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					rowKey:function() {
						return parseInt(Math.random()*(100+1),10); //0-100随机数
					},
				})
			}
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		changMaterial:function($materialRow, chooseProductList) {
			var materialtype = $materialRow.data('materialtype'), 
				skuid = $materialRow.data('skuid'), 
				productid = $materialRow.data('productid');

			var materialList = $('.materialInputRow', $materialRow).map(function(index, el) {
				var $el = $(el);
				 return {
				 	materialCapacityValue:$('.materialCapacityValue', $el).val(),
				 	materialNo:$('.materialNo', $el).val(),
					materialName:$('.materialName', $el).val(),
					materialCount:$.trim($('.materialCount', $el).val()),
				 }
			}).toArray();

			var currProductIndex = _.findIndex(chooseProductList, {productId: productid}),
				currProduct = chooseProductList[currProductIndex];

			var currSku, currSkuIndex;
			if(currProduct && currProduct.hasOwnProperty('chooseProductSkuList')) {
				currSkuIndex = _.findIndex(currProduct.chooseProductSkuList, {skuId:skuid});
				currSku = currProduct.chooseProductSkuList[currSkuIndex];
			}

			if(currSku && currSku.hasOwnProperty('shouldMaterialList')) {
				var currShouldMaterialItemIndex = _.findIndex(currSku.shouldMaterialList, {materialType:materialtype});
				chooseProductList[currProductIndex].chooseProductSkuList[currSkuIndex].shouldMaterialList[currShouldMaterialItemIndex].materialList = materialList;
			}
			return chooseProductList;
		},
		keepSkuInputValue:function($dataListTable, chooseProductList) {
			if(chooseProductList.length == 0) return chooseProductList;
			
			var inputSkuList = this.getSkuInputValue($dataListTable);
			
			chooseProductList = chooseProductList.map(function(item) {
				item.chooseProductSkuList = item.chooseProductSkuList.map(function(sku) {
					var inputSkuIndex = _.findIndex(inputSkuList, {productSkuId:parseInt(sku.skuId)});
					if(inputSkuIndex > -1) {
						sku = _.extend(sku, inputSkuList[inputSkuIndex] || {});
					}
					return sku;
				});
				return item;
			});

			return chooseProductList; 
		},
		getSkuInputValueFunc:function($dataListTable, itemSkuFunc) {
			var inputSkuList = new Array();
			$('.skuRow', $dataListTable).each(function(index, item) {
				var $this = $(item);
				var inputSku = _.extend(new Object(), itemSkuFunc($this));
				inputSku.productMaterialList = new Array();
				var $editMaterialTr = $(this).next('.editMaterialTr');
				$('.materialInputRow', $editMaterialTr).each(function(materialIndex,inputRow) {
					var $inputRow = $(inputRow);
					inputSku.productMaterialList.push({
						materialNo:$('.materialNo', $inputRow).val(),
						materialCount:$.trim($('.materialCount', $inputRow).val()),
					});
				});
				inputSkuList.push(inputSku);
			});
			return inputSkuList;
		},
		getSkuInputValue:function($dataListTable) {
			return this.getSkuInputValueFunc($dataListTable, function($row) {
				var inputSku = {
					productId:parseInt($.trim($('.productId',$row).val())),
					productSkuId:parseInt($.trim($('.productSkuId',$row).val())),
					productAmount:$.trim($('.productAmount',$row).val()),
					productCount:$.trim($('.productCount',$row).val()),
					productMaterialList:new Array(),
				};
				return inputSku;
			});
		},
		deleteSku:function($deleteButton, chooseProductList) {
			var self = this, 
				skuId = $deleteButton.data('skuid'), 
				productId = $deleteButton.data('productid');

			chooseProductList = self.deleteSkuFunc(productId, skuId, chooseProductList);
			return chooseProductList;
		},
		batchDelete:function($dataListTable, chooseProductList) {
			var self = this, 
				array = $('[name=checkItem]', $dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $dataListTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择商品');
				return;
			}

			filterChecked.each(function(i, item) {
				var skuId = $(this).data('skuid'), 
					productId = $(this).val(),
				chooseProductList = self.deleteSkuFunc(productId, skuId, isNew, chooseProductList);
			});

			return chooseProductList;
		},
		deleteSkuFunc:function(productId, skuId, chooseProductList) {
			var currProductIndex = _.findIndex(chooseProductList, {productId:parseInt(productId)}),
				skuList = chooseProductList[currProductIndex].chooseProductSkuList,
				skuIndex = _.findIndex(skuList, {skuId:parseInt(skuId)});
			
			skuList.splice(skuIndex, 1);
			if(skuList.length == 0) {
				chooseProductList.splice(currProductIndex,1);
			} else {
				chooseProductList[currProductIndex].chooseProductSkuList = skuList;
			}
			return chooseProductList;
		},
		choosePurcaseOrderItemMaterial:function(material, chooseMaterialList) {
			try {
				if(chooseMaterialList.length > 0) {
					var index = _.findIndex(chooseMaterialList, {materialNo:material.materialNo});
					index < 0 && chooseMaterialList.push(material)
				} else {
					chooseMaterialList.push(material)
				}
				Rental.notification.success("选择配件", '成功');
				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
		},
		changePurcaseOrderItemMaterial:function($materialRow, chooseMaterialList) {
			var material = {
				materialNo:$('.materialNo', $materialRow).val(),
				materialAmount:$.trim($('.materialAmount', $materialRow).val()),
				materialCount:$.trim($('.materialCount', $materialRow).val()),
			}

			var remarkInput = $('.remark', $materialRow);
			if(remarkInput.length > 0) {
				material.remark = $.trim(remarkInput.val());
			}

			var currMaterialIndex = _.findIndex(chooseMaterialList, {materialNo:material.materialNo});
				currMaterial = chooseMaterialList[currMaterialIndex];

			chooseMaterialList[currMaterialIndex] = _.extend(currMaterial, material);

			return chooseMaterialList;
		},
		getChooseMaterialIndex:function(materialNo, chooseMaterialList) {
			var currMaterial, currMaterialIndex;
			chooseMaterialList.forEach(function(item, index) {
				if(item.materialNo == materialNo) {
					currMaterial = item;
					currMaterialIndex = index;
					return;
				}
			});
			return currMaterialIndex;
		},
		batchDelPurchaseOrderItemMaterial:function($materialDataListTable, chooseMaterialList) {
			var array = $('[name=checkItem]', $materialDataListTable).rtlCheckArray();
			if(array.length == 0) {
				bootbox.alert('请选择配件');
				return;
			}
			chooseMaterialList = chooseMaterialList.filter(function(filterItem) {
				var isSome = array.some(function(someItem) {
					return someItem == filterItem.materialNo;
				});
				return !isSome;
			});
			return chooseMaterialList;
		},
		getPurcaseOrderItemMaterialList:function(chooseMaterialList) {
			return chooseMaterialList.map(function(item) {
				return {
					materialNo:item.materialNo,
					materialAmount:item.materialAmount,
					materialCount:item.materialCount,
				}
			})
		},
		renderMaterialList:function(listData, tpl, container, rowActionButtons) {
			rowActionButtons = rowActionButtons || new Array();
			$("#orderItemMaterialCount").html(listData.length);
			var data = {
				hasRowActionButtons:rowActionButtons.length > 0,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:rowActionButtons,
					materialJSONString:function() {
						return JSON.stringify(this);
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.materialImgList);
					},
				})
			}
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('productSnapshot') ? JSON.parse(item.productSnapshot) : null;
					var newItem = new Object();
					if(product) {
						newItem = $.extend({},product.productSkuList[0],item); //商品快照，sku列表，只有当前选择的一条。
					} else {
						newItem = $.extend({},item); 
					}

					newItem.shouldMaterialList =  (newItem.shouldProductCategoryPropertyValueList || []).map(function(shouldItem) {
						shouldItem.materialList = new Array();
						if(newItem.hasOwnProperty('productMaterialList')) {
							if(shouldItem.hasOwnProperty('materialModelId')) {
								shouldItem.materialList = _.where(newItem.productMaterialList, {materialType:shouldItem.materialType, materialModelId: shouldItem.materialModelId})	
							} else {
								shouldItem.materialList = _.where(newItem.productMaterialList, {materialType:shouldItem.materialType})
							}
						}

						if(shouldItem.materialList.length == 0) {
							shouldItem.materialList.push({
								materialNo:'',
								materialName:'',
								materialCount:1, //默认数量为1,
							});
						}
						//可以新增配件，配件数值得时候，可以由不同数值的配件组成，这时候可能存在多条，需要新增。
						shouldItem.canAdd = shouldItem.hasOwnProperty('propertyCapacityValue'); 
						return shouldItem;
					});

					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					pre[item.productId].chooseProductSkuList.push(newItem);
					pre[item.productId] = $.extend(pre[item.productId], product);
			
					return pre;

				},{});

				for(var key in orderItemsMapByProductId) {
					chooseProductList.push(orderItemsMapByProductId[key]);
				}
			}
			return chooseProductList;
		},
		initChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.materialSnapshot);
					var newMaterial = $.extend({}, material, item);
					return newMaterial;
				});
			}
			return chooseMaterialList;
		},
		/**
		* 方法作用：由于收货单项结构发生变化，为了公用原来方法，这里数据结构做了一下转换
		*/
		resolvePurchaseReceiveOrderProductList:function(data) {
			if(!data) return;
			data.hasOwnProperty('purchaseReceiveOrderProductList') && data.purchaseReceiveOrderProductList == data.purchaseReceiveOrderProductList.map(function(item) {
				
				var srcProduct = {
					srcProductId:item.productId,
					srcProductName:item.productName,
					srcProductSkuId:item.productSkuId,
					srcProductSnapshot:item.productSnapshot,
					srcProductCount:item.productCount,
				};

				var crrentProduct = {
					productId:item.realProductId,
					productName:item.realProductName,
					productSkuId:item.realProductSkuId,
					productSnapshot:item.realProductSnapshot,
					productCount:item.realProductCount,
				};

				item = _.extend(item, srcProduct, crrentProduct);

				return item;
			});

			return data;
		},
		resolvePurchaseReceiveOrderMaterialList:function(data) {
			if(!data) return;
			data.hasOwnProperty('purchaseReceiveOrderMaterialList') && data.purchaseReceiveOrderMaterialList == data.purchaseReceiveOrderMaterialList.map(function(item) {
				
				var srcMaterial = {
					srcMaterialId:item.materialId,
					srcMaterialNo:item.materialNo,
					srcMaterialName:item.materialName,
					srcMaterialSnapshot:item.materialSnapshot,
					srcMaterialCount:item.materialCount,
				};

				var crrentMaterial = {
					materialId:item.realMaterialId,
					materialNo:item.realMaterialNo,
					materialName:item.realMaterialName,
					materialSnapshot:item.realMaterialSnapshot,
					materialCount:item.realMaterialCount,
				};

				item = _.extend(item, srcMaterial, crrentMaterial);

				return item;
			});

			return data;
		},
	}

	window.PurchaseUtil = PurchaseUtil;

})(jQuery)