;(function($){
	
	var PurchaseDetial = {
		state:{
			no:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_detail],
				submitAudit = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_submit_audit],
				del = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_delete],
				continueToPurchase = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_continue_to_purchase],
				endPurchase = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_end_purchase],
				compelDel = this.currentManageListAuthor.children[AuthorCode.manage_purchase_list_compel_delte];

			add && this.actionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submitAudit && this.actionButtons.push(AuthorUtil.button(submitAudit,'submitAuditButton','','提交审核'));
			continueToPurchase && this.actionButtons.push(AuthorUtil.button(continueToPurchase,'continueToPurchaseButton','','继续采购'));
			endPurchase && this.actionButtons.push(AuthorUtil.button(endPurchase,'endPurchaseButton','','结束采购'));
			del && this.actionButtons.push(AuthorUtil.button(del,'deleteButton','','取消'));
			compelDel && this.actionButtons.push(AuthorUtil.button(compelDel,'compelDelButton','','强制取消'));
		},
		initDom:function() {
			this.$form = $("#detailPurchaseForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');

			this.chooseProductList = new Array();    //已经选择的商品集合
			this.chooseMaterialList = new Array();   //已经选择的配件集合
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_purchase_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			this.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			this.$dataListTable.on('click', '.showMaterialButton', function(event) {
				event.preventDefault();
				var $panel = $(this).closest('.skuRow').next('.editMaterialTr').find('.editMaterialPannel');
				$panel.slideToggle('fast');
			});

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到采购单号');
				return;
			}
			Rental.ajax.submit("{0}purchaseOrder/queryPurchaseOrderByNo".format(SitePath.service), {purchaseNo:self.state.no}, function(response) {
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载采购单",response.description || "失败");
				}
			}, null ,"加载采购单", 'listLoading');
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderBaseInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.renderPurchaseDeliveryOrderList(data);
			this.renderPurchaseReceiveOrderList(data);
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, order),
				purchaseno:self.state.no,
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		initChooseProductList:function(data) {
			this.chooseProductList = data.hasOwnProperty('purchaseOrderProductList')  ? PurchaseUtil.initChooseProductList(data.purchaseOrderProductList) : new Array();
			this.renderProductList();
		},
		initChooseMaterialList:function(data) {
			this.chooseMaterialList = data.hasOwnProperty('purchaseOrderMaterialList') ? PurchaseUtil.initChooseMaterialList(data.purchaseOrderMaterialList) : new Array();
			this.renderMaterialList();
		},
		renderBaseInfo:function(purchaseOrder) {
			$("#purchaseOrderNo").html(purchaseOrder.purchaseNo);
			var data = _.extend(Rental.render, {
				dataSource:purchaseOrder,
				purchaseTypeStr:function() {
					return Enum.purchaseType.getValue(this.purchaseType);
				},
				isInvoiceStr:function() {
					return Rental.helper.boolToStr(this.isInvoice);
				},
				isNewStr:function() {
					return Rental.helper.boolToStr(this.isNew);
				},
				warehouseName:function() {
					return this.hasOwnProperty('warehouseSnapshot') ? JSON.parse(this.warehouseSnapshot).warehouseName : '';
				},
				purchaseOrderStatusValue:function() {
					return Enum.purchaseOrderStatus.getValue(this.purchaseOrderStatus);
				}
			});
			var dataTpl = $("#baserInfoTpl").html();
			Mustache.parse(dataTpl);
			$("#baseInfo").html(Mustache.render(dataTpl, data));
		},
		renderProductList:function() {
			PurchaseUtil.renderProductList(this.chooseProductList || [], this.$dataListTpl, this.$dataListTable);
		},
		renderMaterialList:function() {
			PurchaseUtil.renderMaterialList(this.chooseMaterialList || [], this.$materialDataListTpl, this.$materialDataListTable);
		},
		renderPurchaseDeliveryOrderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("purchaseDeliveryOrderList") ? data.purchaseDeliveryOrderList : [];

			var data = {
				dataSource:{
					"listData":listData,
					purchaseDeliveryOrderStatusStr:function() {
						switch(this.purchaseDeliveryOrderStatus) {
							case 0:
								return '待收货';
							case 1:
								return '已发货';
						}
					},
					isInvoiceStr:function() {
						return Rental.helper.boolToStr(this.isInvoice);
					},
					isNewStr:function() {
						return Rental.helper.boolToStr(this.isNew);
					},
					deliveryTimeFormat:function() {
						return Rental.helper.timestampFormat(this.deliveryTime);
					},
					warehouse:function() {
						if(this.hasOwnProperty('warehouseSnapshot')) {
							var warehouse = JSON.parse(this.warehouseSnapshot);
							return warehouse.subCompanyName + " " + warehouse.warehouseName;
						}
						return "";
					},
				}
			}

			var dataListTpl = $('#dataPurchaseDeliveryOrderListTpl').html();
			Mustache.parse(dataListTpl);
			$('#dataPurchaseDeliveryOrderListTable').html(Mustache.render(dataListTpl, data));
		},
		renderPurchaseReceiveOrderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("purchaseReceiveOrderList") ? data.purchaseReceiveOrderList : [];
				
			var data = {
				dataSource:{
					"listData":listData,
					autoAllotStatusStr:function() {
						return Enum.autoAllotStatus.getValue(this.autoAllotStatus)
					},
					purchaseReceiveOrderStatusStr:function() {
						return Enum.purchaseReceiveOrderStatus.getValue(this.purchaseReceiveOrderStatus);
					},
					isInvoiceStr:function() {
						return Rental.helper.boolToStr(this.isInvoice);
					},
					isNewStr:function() {
						return Rental.helper.boolToStr(this.isNew);
					},
					purchaseOrderStatusStr:function() {
						return Enum.purchaseOrderStatus.getValue(this.purchaseOrderStatus);
					},
					confirmTimeFormat:function() {
						return Rental.helper.timestampFormat(this.confirmTime);
					},
					warehouse:function() {
						if(this.hasOwnProperty('warehouseSnapshot')) {
							var warehouse = JSON.parse(this.warehouseSnapshot);
							return warehouse.subCompanyName + " " + warehouse.warehouseName;
						}
						return "";
					},
				}
			}
			var dataListTpl = $("#dataPurchaseReceiveOrderListTpl").html();
			Mustache.parse(dataListTpl);
			$('#dataPurchaseReceiveOrderListTable').html(Mustache.render(dataListTpl, data));
		}
	};

	window.PurchaseDetial = _.extend(PurchaseDetial, PurchaseHandleMixin);

})(jQuery);