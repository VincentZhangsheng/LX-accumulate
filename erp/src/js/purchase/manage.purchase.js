;(function($) {

	var PurchaseManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_detail],
				submitAudit = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_submit_audit],
				del = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_delete],
				continueToPurchase = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_continue_to_purchase],
				endPurchase = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_end_purchase],
				compelDel = this.currentPageAuthor.children[AuthorCode.manage_purchase_list_compel_delte];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			//submitAudit && this.commonActionButtons.push(AuthorUtil.button(submitAudit,'submitAuditButton','fa fa-eye','提交审核'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submitAudit && this.rowActionButtons.push(AuthorUtil.button(submitAudit,'submitAuditButton','','提交审核'));
			continueToPurchase && this.rowActionButtons.push(AuthorUtil.button(continueToPurchase,'continueToPurchaseButton','','继续采购'));
			endPurchase && this.rowActionButtons.push(AuthorUtil.button(endPurchase,'endPurchaseButton','','结束采购'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'deleteButton','','取消'));
			compelDel && this.rowActionButtons.push(AuthorUtil.button(compelDel,'compelDelButton','','强制取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.purchaseList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			self.searchData();  //初始化列表数据

			self.render(new Object());

			Rental.ui.renderSelect({
				container:$('#purchaseOrderStatus'),
				data:Enum.array(Enum.purchaseOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（采购单状态）'
			});

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			Rental.ui.events.imgGallery(this.$dataListTable);

			self.initHandleEvent(self.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		initDatePicker:function() {
			var self = this;
			this.$createTimePicker.daterangepicker({
				"autoApply": true,
				format:'YYYY-MM-DD',
			}, function(start, end, label) {
				self.setSearchDate(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
			});
		},
		setSearchDate:function(start,end) {
			this.$createStartTime.val(new Date(start + " 00:00:00").getTime());
			this.$createEndTime.val(new Date(end + " 23:59:59").getTime());
			this.$createTimePickerInput.val("{0} - {1}".format(start,end));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.purchaseList, searchData);

			Rental.ajax.ajaxData('purchaseOrder/page', searchData, '查询采购单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.purchaseDetail, this.purchaseNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					isInvoiceStr:function() {
						return Rental.helper.boolToStr(this.isInvoice);
					},
					isNewStr:function() {
						return Rental.helper.boolToStr(this.isNew);
					},
					purchaseOrderStatusValue:function() {
						return Enum.purchaseOrderStatus.getValue(this.purchaseOrderStatus);
					},
					purchaseOrderStatusClass:function() {
						return Enum.purchaseOrderStatus.getClass(this.purchaseOrderStatus);
					},
					warehouse:function() {
						if(this.hasOwnProperty('warehouseSnapshot')) {
							var warehouse = JSON.parse(this.warehouseSnapshot);
							return warehouse.subCompanyName + " " + warehouse.warehouseName;
						}
						return "";
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.PurchaseManage = _.extend(PurchaseManage, PurchaseHandleMixin);

})(jQuery);