;(function($) {

	var PurchaseReceiveDetail = {
		state:{
			no:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_purchase];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_purchase_receive];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_receive_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.productRowActionButtons = new Array();
			this.materialRowActionButtons = new Array();
			this.updateRemarkActionButtons = new Array();


			var edit = this.currentManageListAuthor.children[AuthorCode.manage_purchase_receive_edit],
				confirm = this.currentManageListAuthor.children[AuthorCode.manage_purchase_receive_confirm],
				commit = this.currentManageListAuthor.children[AuthorCode.manage_purchase_receive_commit];
				// printBarcodeAuthor = this.currentManageListAuthor.children[AuthorCode.manage_purchase_receive_edit]; //打印条形码

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			confirm && this.actionButtons.push(AuthorUtil.button(confirm,'confirmButton','','确认收货'));
			commit && this.actionButtons.push(AuthorUtil.button(commit,'commitButton','','提交'));
			edit && this.updateRemarkActionButtons.push(AuthorUtil.button(edit,'updateEquipmentRemarkButton','','添加备注'));

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var editOrderProductEuqipment = this.currentPageAuthor.children[AuthorCode.manage_purchase_receive_detail_edit_product_equipemnt_price],
				editOrderMaterialBulk = this.currentPageAuthor.children[AuthorCode.manage_purchase_receive_detial_edit_product_material_price];

			editOrderProductEuqipment && this.productRowActionButtons.push(AuthorUtil.button(editOrderProductEuqipment,'editOrderProductEuqipmentButton hide','','编辑设备价格'));
			this.productRowActionButtons.push(AuthorUtil.button({
				menuUrl:'purchase-receive-manage-equiment/list',
				menuName:'打印条形码'
			},'prinEquipmentRemarkButton','','打印条形码'));

			editOrderMaterialBulk && this.materialRowActionButtons.push(AuthorUtil.button(editOrderMaterialBulk,'editOrderMaterialBulkButton hide','','编辑配件价格'));
			edit && this.materialRowActionButtons.push(AuthorUtil.button(edit,'updateMaterialRemarkButton','','添加备注'));
		},
		initDom:function() {
			this.$form = $("#detailPurchaseReceiveForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');

			this.chooseProductList = new Array();    //已经选择的商品集合
			this.chooseMaterialList = new Array();   //已经选择的配件集合

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		initEvent:function() {
			var self = this;
			
			Breadcrumb.init([self.currentManageAuthor, self.currentManageListAuthor, self.currentPageAuthor]); //面包屑
			Layout.chooseSidebarMenu(AuthorCode.manage_purchase_receive); //激活选中menu菜单
			
			self.loadData(); 

			self.$dataListTable.on('click', '.showMaterialButton', function(event) {
				event.preventDefault();
				var $panel = $(this).closest('.skuRow').next('.editMaterialTr').find('.editMaterialPannel');
				$panel.slideToggle('fast');
			});

			self.$dataListTable.on('click', '.editOrderProductEuqipmentButton', function(event) {
				event.preventDefault();
				EditQquipmentPrice.init({
					purchaseReceiveOrderNo:self.state.no,
					purchaseReceiveOrderProductId:$(this).data('purchasereceiveorderproductid'),
					callBack:function() {
						self.loadData(); 
					},
				});
			});

			self.$materialDataListTable.on('click', '.editOrderMaterialBulkButton', function(event) {
				event.preventDefault();
				EditMaterialPrice.init({
					purchaseReceiveOrderNo:self.state.no,
					purchaseReceiveOrderMaterialId:$(this).data('purchasereceiveordermaterialid'),
					materialCount:$(this).data('materialcount'),
					materialAmount:$(this).data('materialamount'),
					callBack:function() {
						self.loadData(); 
					},
				});
			});

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			});

			self.$dataListTable.on("click", '.prinEquipmentRemarkButton', function(event) {
				event.preventDefault();
				// BarcodePrint.init({
				// 	dataSource:['test10001', 'test10002', 'test10003', 'test10004'],
				// 	container:$('#barCodeContainer')
				// });

				PurchaseEquimentList.init({
					purchaseReceiveOrderNo:self.state.no,
					purchaseReceiveOrderProductId:$(this).data('purchasereceiveorderproductid'),
					callBack:function() {
						self.loadData();
					},
				});
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到采购收料单编号');
				return;
			}
			Rental.ajax.submit("{0}purchaseOrder/queryPurchaseReceiveOrderByNo".format(SitePath.service), {purchaseReceiveNo:self.state.no}, function(response) {
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载采购收料单详细",response.description || "失败");
				}
			}, null ,"加载采购收料单详细");
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderUpdateEquipmentRemarkButton(data);
			this.renderBaseInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.loadPurchaseOrder(data);
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, order),
				no:self.state.no,
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderUpdateEquipmentRemarkButton:function(order) {
			var self = this,
				actionButtonsTpl = $('#updateEquipmentRemarkButtonsTpl').html();
				actionButtons = self.filterAcitonButtons(self.updateRemarkActionButtons, order)
			var data = {
				hasActionButtons:actionButtons.length > 0,
				buttons:{
					acitonButtons:actionButtons,
					no:self.state.no,
				}
			}
			Mustache.parse(actionButtonsTpl);
			$("#updateEquipmentRemarkButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderBaseInfo:function(purchaseReceiveOrder) {
			$("#purchaseReceiveNo").html(purchaseReceiveOrder.purchaseReceiveNo);
			var data = _.extend(Rental.render, {
				dataSource:purchaseReceiveOrder,
				isInvoiceStr:function() {
					return Rental.helper.boolToStr(this.isInvoice);
				},
				isNewStr:function() {
					return Rental.helper.boolToStr(this.isNew);
				},
				warehouseName:function() {
					return this.hasOwnProperty('warehouseSnapshot') ? JSON.parse(this.warehouseSnapshot).warehouseName : '';
				},
				autoAllotStatusStr:function() {
					return Enum.autoAllotStatus.getValue(this.autoAllotStatus);
				},
			});
			var dataTpl = $("#baserInfoTpl").html();
			Mustache.parse(dataTpl);
			$("#baseInfo").html(Mustache.render(dataTpl, data));
		},
		initChooseProductList:function(order) {
			var data = PurchaseUtil.resolvePurchaseReceiveOrderProductList(order);
			this.chooseProductList = data.hasOwnProperty('purchaseReceiveOrderProductList')  ? PurchaseUtil.initChooseProductList(data.purchaseReceiveOrderProductList) : new Array();
			this.renderProductList(order);
		},
		initChooseMaterialList:function(order) {
			var data = PurchaseUtil.resolvePurchaseReceiveOrderMaterialList(order);
			this.chooseMaterialList = data.hasOwnProperty('purchaseReceiveOrderMaterialList') ? PurchaseUtil.initChooseMaterialList(data.purchaseReceiveOrderMaterialList) : new Array();
			this.renderMaterialList(order);
		},
		renderProductList:function(order) {
			this.chooseProductList = PurchaseUtil.keepSkuInputValue(this.$dataListTable, this.chooseProductList);
			PurchaseUtil.renderProductList(this.chooseProductList || [], this.$dataListTpl, this.$dataListTable, this.filterAcitonButtons(this.productRowActionButtons, order));
		},
		renderMaterialList:function(order) {
			this.chooseProductList = PurchaseUtil.keepSkuInputValue(this.$dataListTable, this.chooseProductList);
			PurchaseUtil.renderMaterialList(this.chooseMaterialList || [], this.$materialDataListTpl, this.$materialDataListTable, this.filterAcitonButtons(this.materialRowActionButtons, order));
		},
		loadPurchaseOrder:function(data) {
			var self = this;
			if(!data.purchaseOrderNo) {
				bootbox.alert('没找到采购单号');
				return;
			}
			Rental.ajax.submit("{0}purchaseOrder/queryPurchaseOrderByNo".format(SitePath.service), {purchaseNo:data.purchaseOrderNo}, function(response) {
				if(response.success) {
					self.showRowActionButtonsByPurchaseOrderStatus(response.resultMap.data);
				} else {
					Rental.notification.error("加载采购单",response.description || "失败");
				}
			}, null ,"加载采购单");
		},
		showRowActionButtonsByPurchaseOrderStatus:function(purchaseOrder) {
			if(purchaseOrder.purchaseOrderStatus != Enum.purchaseOrderStatus.finish) {
				$('.editOrderProductEuqipmentButton').removeClass('hide');
				$('.editOrderMaterialBulkButton').removeClass('hide');
			} else {
				$('.editOrderProductEuqipmentButton').remove();
				$('.editOrderMaterialBulkButton').remove();
			}
		}
	};

	window.PurchaseReceiveDetail = _.extend(PurchaseReceiveDetail, PurchaseReceiveHandleMixin);

})(jQuery);