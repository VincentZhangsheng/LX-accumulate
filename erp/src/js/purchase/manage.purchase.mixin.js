;(function($) {

	PurchaseMixin = {
		initCommonEvent:function() {
			var self = this;

			//选择仓库
			$("#chooseWarehouse").on('click',function(event) {
				WarehouseChoose.init({
					callBack:function(warehouse) {
						console.log(warehouse)
						$('[name=warehouseNo]', self.$form).val(warehouse.warehouseNo);
						$('[name=warehouseName]', self.$form).val(warehouse.warehouseName);
					}
				});	
			});

			//选择供应商
			this.$form.on('click', '.chooseSupplier', function(event) {
				event.preventDefault();
				SupplierChoose.init({
					callBack:function(supplier) {
						console.log(supplier)
						$('[name=productSupplierId]', self.$form).val(supplier.supplierId);
						$('[name=productSupplierName]', self.$form).val(supplier.supplierName);
					}
				});
			});

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.renderProductList();

			self.renderMaterialList();  //渲染采购单配件项列表
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			//选择商品
			$("#batchAddProduct").click(function(event) {
				event.preventDefault();

				var purchaseType = $('[name=purchaseType]', self.$form).val();
				var searchParms = new Object();

				if(!purchaseType) {
					bootbox.alert('请先选择采购类型');
					return;	
				}

				//只有选择小配件需要过滤选择配件
				if(parseInt(purchaseType) == 2) {
					bootbox.alert('采购类型为小配件，不能添加商品');
					return;	
				}

				ProductChoose.modal({
					isVerifyStock:false,
					// showIsNewCheckBox:true,
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});

				// ProductChoose.init(function(product) {
				// 	self.chooseProduct(product);
				// });
			});

			//批量删除商品
			$("#batchDeleteProduct").click(function(event) {
				self.batchDelete();
			});

			//删除商品sku项
			this.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
			});

			//编辑商品sku项的配件配置
			this.$dataListTable.on('click', '.editMaterialButton', function(event) {
				event.preventDefault();
				var $panel = $(this).closest('.skuRow').next('.editMaterialTr').find('.editMaterialPannel');
				$panel.slideToggle('fast');
			});

			//选择配件 - 商品sku项的配件选择
			this.$dataListTable.on('click', '.chooseMaterialButton', function(event) {
				event.preventDefault();
				var $this = $(this),  
					materialType = $this.data('materialtype'),
					materialModelId = $this.data('materialmodelid');
				MaterialChoose.init({
					isVerifyStock:false,
					searchParms:{ 
						materialType:materialType,
						materialModelId:materialModelId,
					}, 
					callBack:function(material) {
						self.chooseMaterial($this, material);
					}
				});
			});

			//商品sku项配件-新添加
			this.$dataListTable.on('click', '.newSkuMaterialButton', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				var lastRow = $('.materialInputRow',$materialRow).last();
				var cloneRow =  lastRow.clone(true);
				$('.btn-group',cloneRow).show();
				cloneRow.insertAfter(lastRow);

				var $materialRow = $(this).closest('.materialRow');
				self.changMaterial($materialRow);
			});

			//商品sku项目配件-删除
			this.$dataListTable.on('click', '.delSkuMaterialButton', function(event) {
				event.preventDefault();
				 $(this).closest('.materialInputRow').remove();
			});

			//商品项sku的配件信息数量发生变化时
			this.$dataListTable.on('change', '.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changMaterial($materialRow);
			});

			this.$dataListTable.on('change', '.productAmount,.productCount', function(event) {
				event.preventDefault();
				PurchaseUtil.keepSkuInputValue(self.$dataListTable,self.chooseProductList);
				self.renderTotalProductInfo();
			});

			//选择采购单项配件
			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();

				var purchaseType = $('[name=purchaseType]', self.$form).val();
				var searchParms = new Object();

				if(!purchaseType) {
					bootbox.alert('请先选择采购类型');
					return;	
				}

				//只有选择小配件需要过滤选择配件
				if(parseInt(purchaseType) == 2) {
					searchParms.isMainMaterial = 0; 	
				}

				MaterialChoose.init({
					btach:true,
					isVerifyStock:false,
					searchParms:searchParms, 
					callBack:function(material) {
						self.choosePurcaseOrderItemMaterial(material);
					}
				});
			});

			self.$materialDataListTable.on('change', '.materialAmount,.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.purchaseOrderItemMaterialRow');
				self.changePurcaseOrderItemMaterial($materialRow);
			});

			self.$materialDataListTable.on('click', '.delPurchaseOrderItemMaterialButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.delPurchaseOrderItemMaterial(materialNo);
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDelPurchaseOrderItemMaterial();
			});

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		chooseProduct:function(product) {
			var self = this;
			self.chooseProductList = PurchaseUtil.chooseProduct(self.chooseProductList, product);
			self.renderProductList();

			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		chooseMaterial:function($button, material) {
			var parnets = $button.closest('.materialInputRow');
			$('.materialCapacityValue',parnets).val(material.materialCapacityValue);
			$('.materialNo',parnets).val(material.materialNo);
			$('.materialName',parnets).val(material.materialName);

			//更新数据源
			this.changMaterial($button.closest('.materialRow'));
		},
		changMaterial:function($materialRow) {
			PurchaseUtil.changMaterial($materialRow, this.chooseProductList);
		},
		renderProductList:function() {
			this.chooseProductList = PurchaseUtil.keepSkuInputValue(this.$dataListTable, this.chooseProductList);  
			PurchaseUtil.renderProductList(this.chooseProductList || [], this.$dataListTpl, this.$dataListTable);
			this.renderTotalProductInfo();
		},
		renderTotalProductInfo:function() {
			if(this.chooseProductList.length == 0) {
				return;
			}
			var totalProduct = this.chooseProductList.reduce(function(pre, item) {
				var total = item.chooseProductSkuList.reduce(function(skuPre, skuItem) {
					var productCount = 0, productAmount = 0;
					if(skuItem.productCount && skuItem.productAmount) {
						productCount = parseInt(skuItem.productCount);
						productAmount = parseFloat(skuItem.productAmount);
					}
					skuPre.count += productCount;
					skuPre.price += (productCount*productAmount);
					return skuPre; 
				},{count:0, price:0});
				pre.totalCount += total.count;
				pre.totalPrice += total.price;
				return pre;
			}, {totalCount:0, totalPrice:0});

			var data = _.extend(Rental.render, {
				totalProduct:totalProduct,
			});
			var tpl = $("#totalProductTpl").html();
			Mustache.parse(tpl);
			$("#totalProduct").html(Mustache.render(tpl, data));
		},
		renderTotalMatrialInfo:function() {
			if(this.chooseMaterialList.length == 0) {
				return;
			}
			var totalMaterial = this.chooseMaterialList.reduce(function(pre, item) {
				var materialCount = 0, materialAmount = 0;
				if(item.materialCount && item.materialAmount) {
					materialCount = parseInt(item.materialCount);
					materialAmount = parseFloat(item.materialAmount);
				}
				pre.totalCount += materialCount;
				pre.totalPrice += (materialCount*materialAmount);
				return pre;
			}, {totalCount:0, totalPrice:0});
			var data = _.extend(Rental.render, {
				totalMaterial:totalMaterial,
			});
			var tpl = $("#totalMaterialTpl").html();
			Mustache.parse(tpl);
			$("#totalMaterial").html(Mustache.render(tpl, data));
		},
		deleteSku:function($deleteButton) {
			this.chooseProductList = PurchaseUtil.deleteSku($deleteButton, this.chooseProductList);
			this.renderProductList();	
		},
		batchDelete:function() {
			this.chooseProductList = PurchaseUtil.batchDelete(this.$dataListTable, this.chooseProductList);
			this.renderProductList();	
		},
		//选择采购单项配件
		choosePurcaseOrderItemMaterial:function(material) {
			this.chooseMaterialList = PurchaseUtil.choosePurcaseOrderItemMaterial(material, this.chooseMaterialList);
			this.renderMaterialList();
		},
		changePurcaseOrderItemMaterial:function($materialRow) {
			this.chooseMaterialList = PurchaseUtil.changePurcaseOrderItemMaterial($materialRow, this.chooseMaterialList);
			this.renderTotalMatrialInfo();
		},
		delPurchaseOrderItemMaterial:function(materialNo) {
			var currMaterialIndex = _.findIndex(this.chooseMaterialList, {materialNo:materialNo});
			this.chooseMaterialList.splice(currMaterialIndex,1);			
			this.renderMaterialList();
		},
		batchDelPurchaseOrderItemMaterial:function() {
			this.chooseMaterialList = PurchaseUtil.batchDelPurchaseOrderItemMaterial(this.$materialDataListTable, this.chooseMaterialList);
			this.renderMaterialList();
		},
		renderMaterialList:function() {
			PurchaseUtil.renderMaterialList(this.chooseMaterialList || [], this.$materialDataListTpl, this.$materialDataListTable);
			this.renderTotalMatrialInfo();
		},
	};

	window.PurchaseMixin = PurchaseMixin;

})(jQuery);