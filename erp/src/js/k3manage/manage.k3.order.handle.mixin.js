;(function($) {

	var K3OrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			console.log('initHandleEvent');

			container.on('click', '.rowDelButton', function(event) {
				event.preventDefault();
				var k3ReturnOrderDetailId = $(this).data('k3returnorderdetailid');
				self.handleDo('k3/deleteReturnOrder', {k3ReturnOrderDetailId:k3ReturnOrderDetailId}, '删除退货商品', handleCallBack);
			});

			container.on('click', '.sendButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				bootbox.confirm('确认删除？',function(result) {
					result && self.handleDo('k3/sendToK3', {returnOrderNo:returnOrderNo}, '发送退货单到K3', handleCallBack);	
				});
			});
		},
		handleDo:function(url, prams, des, callBack) {
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array(),
				unSubmit = order.orderStatus == Enum.orderStatus.num.unSubmit,
				picking = order.orderStatus == Enum.orderStatus.num.waitForDelivery || order.orderStatus == Enum.orderStatus.num.inHand, //配货条件
				deliverGoods = order.orderStatus == Enum.orderStatus.num.inHand, //发货条件
				confirmReceipt = order.orderStatus == Enum.orderStatus.num.delivered; //确认收货条件
		
			(buttons || []).forEach(function(button, index) {
				button.orderNo = order.orderNo;
				rowActionButtons.push(button);
				if((button.class == 'submitButton' || button.class == 'editButton' || button.class == 'cancelButton') && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'pickingButton' || button.class == 'pickingProductButton' || button.class == 'removePickingProductButton'  || button.class == 'pickingMaterialButton' || button.class == 'removePickingMaterialButton') && !picking) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'deliverGoodsButton') && !deliverGoods) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'confirmReceiptButton') && !confirmReceipt) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		}		
	}

	window.K3OrderHandleMixin = K3OrderHandleMixin;

})(jQuery);