;(function($){
	
	var K3OrderDetail = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
			customerNo:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_order_detail];
				// this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.pickingProductButtons = new Array;
			this.pickingMaterialButtons = new Array;

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var edit = this.currentManageListAuthor.children[AuthorCode.manage_order_edit],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_order_submit],
				confirmReceipt = this.currentManageListAuthor.children[AuthorCode.manage_order_confirm_receipt],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_order_cancel],
				picking = this.currentManageListAuthor.children[AuthorCode.manage_order_picking],
				deliverGoods = this.currentManageListAuthor.children[AuthorCode.manage_order_deliver_goods],
				print = this.currentManageListAuthor.children[AuthorCode.manage_order_print];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			deliverGoods && this.actionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			confirmReceipt && this.actionButtons.push(AuthorUtil.button(confirmReceipt,'confirmReceiptButton','','确认收货'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
			print && this.actionButtons.push(AuthorUtil.button(print,'printButton','','打印',true,true));
		},
		initDom:function() {
			this.$form = $("#orderDetailForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_k3_manage_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			});

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('k3/queryOrderByNo', {orderNo:self.state.no}, '加载订单详细信息', function(response) {
				self.initData(response.resultMap.data);
			}, null, 'dialogLoading');
		},
		initData:function(data) {
			this.state.order = data;
			this.renderOrderBaseInfo(data);
			this.renderOrderDetailInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.renderActionButton(data);
			// this.defaultTab();
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, order),
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#orderNo").html(order.orderNo);
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderDetailInfo:function(order) {
			this.renderOrderInfo(order, $('#orderDetailInfoTpl').html(), $('#orderDetailInfo'));
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				orderStatusValue:function() {
					return Enum.orderStatus.getValue(order.orderStatus);
				},
				totalRentalAmoutFont:function() {
					var totalProductAmount = this.hasOwnProperty('totalProductAmount') ? parseFloat(this.totalProductAmount) : 0;
					var totalMaterialAmount = this.hasOwnProperty('totalMaterialAmount') ? parseFloat(this.totalMaterialAmount) : 0;
					return (totalProductAmount + totalMaterialAmount).toFixed(2);
				},
				deliveryModeValue:function() {
					return Enum.deliveryMode.getValue(this.deliveryMode);
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			// this.state.chooseProductList = OrderManageUtil.initChooseProductList(data.orderProductList);
			this.renderProductList(data.orderProductList);
		},	
		initChooseMaterialList:function(data) {
			// this.state.chooseMaterialList = OrderManageUtil.initChooseMaterialList(data.orderMaterialList);
			this.renderMaterialList(data.orderMaterialList);
		},
		defaultTab:function() {
			if(this.state.chooseProductList.length == 0) {
				$('.orderItemTabs li').removeClass('active').eq(1).addClass('active');
				$('.tab-pane-order').removeClass('active').eq(1).addClass('active');
			} 
		},
	};

	window.K3OrderDetail = _.extend(K3OrderDetail, K3OrderMixIn, K3OrderHandleMixin);

})(jQuery);