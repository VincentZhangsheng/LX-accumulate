;(function($) {

	K3ReturnOrderMixIn = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			k3CustomerNo:null,
			k3CustomerName:null,
		},
		initCommonEvent:function() {
			var self = this;
			
			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});		

			$("#chooseCustomer").click(function(event) {
				event.preventDefault();
				ChooseCustomer.init({
					callBack:function(customer) {
						self.setCustomer(customer.customerNo, customer.customerName);
						self.initAddressInfo(customer.customerNo);
					}
				});
			});

			$("#chooseBusinessCustomer").click(function(event) {
				event.preventDefault();
				ChooseBusinessCustomer.init({
					callBack:function(company) {
						self.setCustomer(company.customerNo, company.customerName);
						self.initAddressInfo(company.customerNo);
					}
				});
			});
			
			Rental.ui.events.initDatePicker($('#returnTimePicker'), $("[name=returnTime]", self.$form));	
			
			Rental.ui.renderSelect({
				data:Enum.array(Enum.returnMode),
				container:$('[name=returnMode]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			Rental.ui.renderSelect({
				data:Enum.array(Enum.returnReasonType),
				container:$('[name=returnReasonType]', self.$form),
				func:function(opt, item) {
					return item.num == Enum.returnReasonType.num.aheadOfTime ? null : opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			ApiData.company({
				success:function(response) {
					var filterCompany = _.filter(response, function(item) {
						return item.subCompanyId  != Enum.subCompany.num.headOffice && item.subCompanyId != Enum.subCompany.num.telemarketing && item.subCompanyId != Enum.subCompany.num.channelBigCustomer;
					});
					Rental.ui.renderSelect({
						container:$("#deliverySubCompanyId"),
						data: filterCompany, //response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							// self.searchData();
						},
						defaultText:'请选择发货公司'
					});
				}
			});

			self.renderProductList();

			$('#batchAddProduct').click(function(event) {
				event.preventDefault();
				K3OrderItemChoose.init({
					buyerCustomerNo:self.state.k3CustomerNo,
					buyerRealName:self.state.k3CustomerName,
					callBack:function(productArray) {
						self.chooseProduct(productArray);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				event.preventDefault();
				self.batchDeleteProduct();
				self.renderProductList();
			});

			self.$dataListTable.on('click', '.deleteProductButton', function(event) {
				event.preventDefault();
				self.deleteProduct($(this));
			});

			self.$dataListTable.on('change', '.productCount,.remark', function(event) {
				event.preventDefault();
				// var $productRow = $(this).closest('.productRow');
				self.changeProductEvent($(this));
			});

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		chooseProduct:function(productArray) {
			this.state.chooseProductList = this.chooseProductFunc(this.state.chooseProductList, productArray);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		chooseProductFunc:function(chooseProductList, productArray) {
			var self = this;
			if(!(_.isArray(productArray) && productArray.length > 0)) {
				bootbox.alert('请选择订单商品项');
				return [];
			}
			var firstProduct = productArray[0]; 
			if(!!self.state.k3CustomerNo && !!firstProduct.k3CustomerNo && self.state.k3CustomerNo != firstProduct.k3CustomerNo) {
				bootbox.alert('请选择同一个客户下的商品');
				return [];	
			}
			if(!self.state.k3CustomerNo) {
				self.setCustomer(firstProduct.k3CustomerNo, firstProduct.k3CustomerName);
			}
			productArray = productArray.map(function(item) {
				item.rowid = Rental.helper.randomString(8);
				return item;
			});	

			if(chooseProductList.length == 0) {
				chooseProductList = productArray;
			} else {
				productArray.forEach(function(item) {
					var productIndex = _.findIndex(chooseProductList, { productNo:item.productNo, orderItemId:item.orderItemId, orderNo:item.orderNo, orderEntry:item.orderEntry });
					if(productIndex == -1) chooseProductList.push(item);
				});	
			}
			return chooseProductList;
		},
		setCustomer:function(k3CustomerNo, k3CustomerName) {
			this.state.k3CustomerNo = k3CustomerNo;
			this.state.k3CustomerName = k3CustomerName;
			$('[name=customerName]', this.$form).val(k3CustomerName);

			// if(initAddress == true) {
			// 	$('[name=returnAddress]', self.$form).val("");
			// 	this.initAddressInfo(this.state.k3CustomerNo);
			// }
		},
		renderProductList:function(rowActionButtons) {			
			this.renderProductListFunc(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable, rowActionButtons);
		},
		renderProductListFunc:function(listData, tpl, container, rowActionButtons) {
			var dataLength = listData.length;
			$("#orderItemProductCount").text(dataLength + "");
			rowActionButtons = rowActionButtons || new Array();
			var data = _.extend(Rental.render, {
				listData:listData,
				rowActionButtons:rowActionButtons,
				orderDetailUrl:function() {
					return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
				},
				propertiesToStr:function() {
					var productSku = this.hasOwnProperty("productSkuSnapshot") ? JSON.parse(this.productSkuSnapshot) :
					(this.hasOwnProperty("orderProduct") ? (this.orderProduct.hasOwnProperty("productSkuSnapshot") ? JSON.parse(this.orderProduct.productSkuSnapshot) : {}) : {});

					var str = productSku.hasOwnProperty('productSkuList') &&  productSku.productSkuList[0] && productSku.productSkuList[0].hasOwnProperty('productSkuPropertyList') ? productSku.productSkuList[0].productSkuPropertyList.map(function(item) {
						return item.propertyValueName;
					}).join(" | ") : '';
					return str;
				},
				skuPropertiesToStr:function() {

				},
				productNumber:function() {
					var productSku = this.hasOwnProperty("productSkuSnapshot") ? JSON.parse(this.productSkuSnapshot) : {};
					var productNo = productSku.hasOwnProperty('productNo') ? productSku.productNo : "";
					return productNo;
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = this.batchDeleteFunc(this.$dataListTable, this.state.chooseProductList);
			this.renderProductList();	
		},
		deleteProduct:function($deleteButton) {
			var productNo = $deleteButton.data('productno'),
				rowid = $deleteButton.data('rowid');
			this.state.chooseProductList = this.deleteProductFunc(productNo, rowid, this.state.chooseProductList);
			this.renderProductList();
		},
		batchDeleteFunc:function($dataListTable, chooseProductList) {
			var self = this, 
				array = $('[name=checkItem]', $dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $dataListTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择商品');
				return chooseProductList;
			}

			filterChecked.each(function(i, item) {
				var productNo = $(this).data('productno');
				var rowId = $(this).data('rowid');
				chooseProductList = self.deleteProductFunc(productNo, rowId, chooseProductList);
			});

			return chooseProductList;
		},
		deleteProductFunc:function(productNo, rowid, chooseProductList) {
			var currProductIndex = _.findIndex(chooseProductList, {productNo:productNo, rowid:rowid});
			if(currProductIndex > -1) {
				chooseProductList.splice(currProductIndex,1);
			}
			return chooseProductList;
		},
		changeProductEvent:function($countInp) {
			this.state.chooseProductList = this.changeProduct(this.state.chooseProductList, $countInp);
		},
		getProductByProductRow:function($productRow) {
			return {
				orderNo:$('.orderNo', $productRow).val(),
				productNo:$('.productNo', $productRow).val(),
				productCount:$('.productCount', $productRow).val(),
				remark:$.trim($('.remark', $productRow).val()),
				rowid:$.trim($('.rowid', $productRow).val()),
			};
		},
		changeProduct:function(chooseProductList, $countInp) {
			var self = this;
			var $productRow = $countInp.closest('.productRow');
			var product = this.getProductByProductRow($productRow);
			var productIndex = _.findIndex(chooseProductList, {productNo:product.productNo, rowid:product.rowid});

			if(product.productCount > chooseProductList[productIndex].canReturnCount) {
				bootbox.alert('退货数量不能大于可退货数量');
				product.productCount = chooseProductList[productIndex].canReturnCount
				$countInp.val(chooseProductList[productIndex].canReturnCount);
			}
			if(product.productCount < 0) {
				bootbox.alert('退货数量不能小余0');
				product.productCount = 0;
				$countInp.val("0");
			}
			
			chooseProductList[productIndex] = _.extend(chooseProductList[productIndex], product);
			return chooseProductList;
		},
		initChooseProductList:function(data, rowActionButtons) {
			this.state.chooseProductList = data.k3ReturnOrderDetailList;
			this.renderProductList(rowActionButtons);
		},
		initAddressInfo:function(customerNo) {
			var self = this;
			$('[name=returnAddress]', self.$form).val("");
			ApiData.customerDetail({
				customerNo:customerNo,
				success:function(data) {
					data.hasOwnProperty("lastOrderAddress") && $('[name=returnAddress]', self.$form).val(data.lastOrderAddress);
					data.hasOwnProperty("lastOrderConsigneeName") && $('[name=returnContacts]', self.$form).val(data.lastOrderConsigneeName);
					data.hasOwnProperty("lastOrderConsigneePhone") && $('[name=returnPhone]', self.$form).val(data.lastOrderConsigneePhone);
				}
			})
		}
	};

	window.K3ReturnOrderMixIn = K3ReturnOrderMixIn;

})(jQuery);