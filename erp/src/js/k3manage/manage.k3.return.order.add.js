;(function($){
	
	var K3AddReturnOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			k3CustomerNo:null,
			k3CustomerName:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_return_order];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_create_return_order];
		},
		initDom:function() {
			this.$form = $("#addOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_k3_manage_return_order); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			self.initCommonEvent(); // from order mixin

			self.initDeliverySubCompanyId();
		},
		initDeliverySubCompanyId:function() {
			var user = Rental.localstorage.getUser();
			var subCompanyId =  user.hasOwnProperty('roleList') && !!user.roleList[0] ? user.roleList[0].subCompanyId : null;
			if(subCompanyId != Enum.subCompany.num.headOffice && subCompanyId != Enum.subCompany.num.telemarketing && subCompanyId != Enum.subCompany.num.channelBigCustomer) {
				$("#deliverySubCompanyId").val(subCompanyId).data('defaultvalue', subCompanyId);	
			}
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					k3CustomerNo:self.state.k3CustomerNo,
					k3CustomerName:self.state.k3CustomerName,
					returnTime:new Date(formData['returnTime']).getTime(),
					returnAddress:formData['returnAddress'],
					returnContacts:formData['returnContacts'],
					returnPhone:formData['returnPhone'],
					returnMode:formData['returnMode'],
					logisticsAmount:!!formData['logisticsAmount'] ? formData['logisticsAmount'] : 0,
					serviceAmount:!!formData['serviceAmount'] ? formData['serviceAmount'] : 0,
					remark:formData['orderRemark'],
					returnReasonType:formData['returnReasonType'],
					deliverySubCompanyId:formData['deliverySubCompanyId']
				}

				var k3ReturnOrderDetailList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					k3ReturnOrderDetailList.push({
						orderNo:product.orderNo,
						orderEntry:product.orderEntry,
						productNo:product.productNo,
						productName:product.productName,
						productCount:product.productCount,
						remark:product.remark,
						orderItemId:product.orderItemId,
						// orderSubCompanyId:product.orderSubCompanyId,
					});
				});

				if(k3ReturnOrderDetailList.length == 0) {
					bootbox.alert('请选择要退的商品');
					return;
				}

				// var filterOrderDetailList = k3ReturnOrderDetailList.filter(function(item) {
				// 	return item.orderSubCompanyId != formData['deliverySubCompanyId']
				// })
				// if(filterOrderDetailList.length > 0) {
				// 	bootbox.alert('请选择订单归属公司或者订单发货公司与发货公司一致的商品');
				// 	return;
				// }

				var filterChooseOrderList = self.state.chooseProductList.filter(function(item) {
					return item.k3CustomerNo != self.state.k3CustomerNo
				})
				if(filterChooseOrderList.length > 0) {
					bootbox.alert('选择的客户与商品所属客户不一致');
					return;
				}

				commitData.k3ReturnOrderDetailList = k3ReturnOrderDetailList;

				Rental.ajax.submit("{0}k3/createReturnOrderFromERP".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success("创建退货单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("创建退货单",response.description || '失败');
					}
				}, null ,"创建退货单",'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建退货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建退货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.K3AddReturnOrder = _.extend(K3AddReturnOrder, K3ReturnOrderMixIn);

})(jQuery);