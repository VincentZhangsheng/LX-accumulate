;(function($) {

	K3ChangeOrderMixIn = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			k3CustomerNo:null,
			k3CustomerName:null,
		},
		initCommonEvent:function() {
			var self = this;
			
			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});		
			
			Rental.ui.events.initDatePicker($('#changeTimePicker'), $("[name=changeTime]", self.$form));	
			
			Rental.ui.renderSelect({
				data:Enum.array(Enum.k3ChangeMode),
				container:$('[name=changeMode]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			self.renderProductList();

			self.$form.on('click', '.batchAddProduct', function(event) {
				event.preventDefault();
				K3OrderItemChoose.init({
					buyerCustomerNo:self.state.k3CustomerNo,
					callBack:function(productArray) {
						console.log(productArray);
						self.chooseProduct(productArray);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				event.preventDefault();
				self.batchDeleteProduct();
				self.renderProductList();
			});

			self.$dataListTable.on('click', '.chooseChangeProduct', function(event) {
				event.preventDefault();
				var productId = $(this).data('productid'), 
					productNo = $(this).data('productno'), 
					orderItemId = $(this).data('orderitemid'), 
					changeSkuId = $(this).data('changeskuid');
				ProductChoose.modal({
					// showIsNewCheckBox:true,
					// productId:productId,
					callBack:function(product) {
						self.chooseChangeProduct(orderItemId, changeSkuId, product);
					}
				});
			});

			self.$dataListTable.on('click', '.chooseChangeMaterial', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno'),
					productNo = $(this).data('productno'), 
					orderItemId = $(this).data('orderitemid'), 
					changeMaterialId = $(this).data('changematerialid');
				MaterialChoose.init({
					// btach:true,
					searchParms:new Object(), 
					// showIsNewCheckBox:true,
					callBack:function(material) {
						self.chooseChangeMaterial(orderItemId, changeMaterialId, material);
					}
				});
			});

			self.$dataListTable.on('click', '.deleteProductButton', function(event) {
				event.preventDefault();
				self.deleteProduct($(this));
			});

			self.$dataListTable.on('change', '.productCount,.remark', function(event) {
				event.preventDefault();
				var $productRow = $(this).closest('.productRow');
				self.changeProductEvent($productRow);
			});

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		chooseProduct:function(productArray) {
			console.log(productArray);
			this.state.chooseProductList = this.chooseProductFunc(this.state.chooseProductList, productArray);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		chooseProductFunc:function(chooseProductList, productArray) {
			var self = this;
			if(!(_.isArray(productArray) && productArray.length > 0)) {
				bootbox.alert('请选择订单商品项');
				return;
			}
			var firstProduct = productArray[0]; 
			if(!!self.state.k3CustomerNo && self.state.k3CustomerNo != firstProduct.k3CustomerNo) {
				bootbox.alert('请选择同一个客户下的商品');
				return;	
			}
			if(!self.state.k3CustomerNo) {
				self.setCustomer(firstProduct.k3CustomerNo, firstProduct.k3CustomerName);
			}
			if(chooseProductList.length == 0) {
				chooseProductList = productArray;
			} else {
				productArray.forEach(function(item) {
					var _findIndex = _.findIndex(chooseProductList, {orderItemId: item.orderItemId});
					if(_findIndex > -1) {
						chooseProductList[_findIndex] = _.extend(chooseProductList[_findIndex], item);
					} else {
						chooseProductList.push(item);
					}
				});	
			}
			return chooseProductList;
		},
		chooseChangeProduct:function(orderItemId, changeSkuId, product) {
			if(product.chooseProductSkuList.length > 1) {
				bootbox.alert('只能选择一个商品配置');
				return;
			}
			
			var sku = product.chooseProductSkuList[0];
			var productIndex = _.findIndex(this.state.chooseProductList, {orderItemId:orderItemId});

			this.state.chooseProductList[productIndex] = _.extend(this.state.chooseProductList[productIndex], {
				changeSkuId:sku.skuId,
				changeProductName:product.productName,
			}); 

			this.renderProductList();
			bootbox.alert('成功选择更换商品');
			Rental.modal.close();
		},
		chooseChangeMaterial:function(orderItemId, changeMaterialId, material) {
			if(!!material == false) {
				bootbox.alert('请选择配件');
				return;
			}
			
			// var sku = product.chooseProductSkuList[0];
			var productIndex = _.findIndex(this.state.chooseProductList, {orderItemId:orderItemId});

			this.state.chooseProductList[productIndex] = _.extend(this.state.chooseProductList[productIndex], {
				changeMaterialId:material.materialId,
				changeProductName:material.materialName,
			}); 

			this.renderProductList();
			bootbox.alert('成功选择更换配件');
			Rental.modal.close();
		},
		setCustomer:function(k3CustomerNo, k3CustomerName) {
			this.state.k3CustomerNo = k3CustomerNo;
			this.state.k3CustomerName = k3CustomerName;
		},
		renderProductList:function(rowActionButtons) {			
			this.renderProductListFunc(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable, rowActionButtons);
		},
		renderProductListFunc:function(listData, tpl, container, rowActionButtons) {
			console.log(listData)
			$("#changeItemProductCount").html(listData.length);
			rowActionButtons = rowActionButtons || new Array();
			var data = _.extend(Rental.render, {
				listData:listData,
				rowActionButtons:rowActionButtons,
				hasChooseChange:function() {
					return (this.hasOwnProperty('changeSkuId') && !!this.changeSkuId) || (this.hasOwnProperty('changeMaterialId') && !!this.changeMaterialId); 
				},
				isProduct:function() {
					return this.hasOwnProperty('productId') && !!this.productId;
				},
				isMaterial:function() {
					return this.hasOwnProperty('materialId') && !!this.materialId;
				},
				chooseButtonClass:function() {
					if(this.hasOwnProperty('productId') && !!this.productId) {
						return "chooseChangeProduct"
					} else if (this.hasOwnProperty('materialId') && !!this.materialId) {
						return "chooseChangeMaterial";
					}
					return "";
				},
				hasK3ChangeOrderDetailId:function() {
					return this.hasOwnProperty('k3ChangeOrderDetailId') && !!this.k3ChangeOrderDetailId;
				},
				rentTypeValue:function() {
					return Enum.rentType.getValue(this.rentType);
				},
				rentTypeUnit:function() {
					return Enum.rentType.getUnit(this.rentType);
				},
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = this.batchDeleteFunc(this.$dataListTable, this.state.chooseProductList);
			this.renderProductList();	
		},
		deleteProduct:function($deleteButton) {
			var productNo = $deleteButton.data('productno');
			this.state.chooseProductList = this.deleteProductFunc(productNo, this.state.chooseProductList);
			this.renderProductList();
		},
		batchDeleteFunc:function($dataListTable, chooseProductList) {
			var self = this, 
				array = $('[name=checkItem]', $dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $dataListTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择商品');
				return chooseProductList;
			}

			filterChecked.each(function(i, item) {
				var productNo = $(this).data('productno');
				chooseProductList = self.deleteProductFunc(productNo, chooseProductList);
			});

			return chooseProductList;
		},
		deleteProductFunc:function(productNo, chooseProductList) {
			var currProductIndex = _.findIndex(chooseProductList, {productNo:productNo});
			if(currProductIndex > -1) {
				chooseProductList.splice(currProductIndex,1);
			}
			return chooseProductList;
		},
		changeProductEvent:function($productRow) {
			this.state.chooseProductList = this.changeProduct(this.state.chooseProductList, $productRow);
		},
		getProductByProductRow:function($productRow) {
			return {
				productNo:$('.productNo', $productRow).val(),
				productCount:$('.productCount', $productRow).val(),
				remark:$.trim($('.remark', $productRow).val()),
				productDiffAmount:$.trim($('.productDiffAmount', $productRow).val()),
			};
		},
		changeProduct:function(chooseProductList, $productRow) {
			var product = this.getProductByProductRow($productRow);
			var productIndex = _.findIndex(chooseProductList, {productNo:product.productNo});
			chooseProductList[productIndex] = _.extend(chooseProductList[productIndex], product);
			return chooseProductList;
		},
		initChooseProductList:function(data, rowActionButtons) {
			this.state.chooseProductList = data.k3ChangeOrderDetailList;
			this.renderProductList(rowActionButtons);
		}
	};

	window.K3ChangeOrderMixIn = K3ChangeOrderMixIn;

})(jQuery);