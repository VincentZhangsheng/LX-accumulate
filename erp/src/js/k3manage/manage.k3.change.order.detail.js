;(function($){
	
	var K3ChangeOrderDetail = {
		state:{
			no:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_change_order];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_detial];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();

			this.productAcitonButtons = new Array();
			this.rowAcitonButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_create_change_order],
				view = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_detial],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_edit],
				send = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_send],
				addProduct = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_add_product],
				delProduct = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_delete_product],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_cancel],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_change_order_submit];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));	
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));	
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));	
			// send && this.actionButtons.push(AuthorUtil.button(send,'sendButton','','发送'));
			addProduct && this.productAcitonButtons.push(AuthorUtil.button(addProduct,'batchAddProduct','','添加商品'));

			// addProduct && this.rowAcitonButtons.push(AuthorUtil.button(delProduct,'chooseChangeProduct','','选择更换商品'));			
			delProduct && this.rowAcitonButtons.push(AuthorUtil.button(delProduct,'rowDelButton','','删除'));
		},
		initDom:function() {
			this.$form = $("#detailOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_k3_manage_change_order); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form, function() {
				self.edit();
			});

			self.initCommonEvent(); 

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			}); 

			self.$form.on('click', '.saveButton', function(event) {
				event.preventDefault();
				self.addProductToChangeOrder();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到退还单编号');
				return;
			}
			Rental.ajax.ajaxData('k3/queryChangeOrderByNo', {changeOrderNo:self.state.no}, '加载换货单详细信息', function(response) {
				self.initData(response.resultMap.data);
			}, null, 'listLoading');
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderProductActionButtons(data);
			this.setCustomer(data.k3CustomerNo, data.k3CustomerName);
			this.renderOrderBaseInfo(data);
			this.initChooseProductList(data, this.filterAcitonButtons(this.rowAcitonButtons, data));
		},
		renderActionButton:function(returnOrder) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, returnOrder),
				no:self.state.no,
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderProductActionButtons:function(returnOrder) {
			var self = this,
				actionButtonsTpl = $('#productActionsButtonsTpl').html();
				actionButtons = self.filterAcitonButtons(self.productAcitonButtons, returnOrder);

			var data = {
				hasActionButtons:actionButtons.length > 0,
				acitonButtons:actionButtons,
			}
			
			Mustache.parse(actionButtonsTpl);
			$("#productActionsButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#changeOrderNo").html(order.changeOrderNo);
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				changeModeValue:function() {
					return Enum.k3ChangeMode.getValue(this.changeMode);
				},
				changeOrderStatusValue:function() {
					return Enum.k3ChangeOrderStatus.getValue(this.changeOrderStatus)
				}			
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		addProductToChangeOrder:function() {
			var self = this;
			try {

				var commitData = new Object();
				var k3ChangeOrderDetailList = new Array();
			
				self.state.chooseProductList.forEach(function(product) {
					if(!product.hasOwnProperty('k3ChangeOrderDetailId') && !!product.k3ChangeOrderDetailId == false) {
						var item  = {
							orderNo:product.orderNo,
							orderEntry:product.orderEntry,
							productNo:product.productNo,
							productName:product.productName,
							productCount:product.productCount,
							remark:product.remark,
							orderItemId:product.orderItemId,
							productDiffAmount:product.productDiffAmount,
						}
						
						if(product.hasOwnProperty('changeSkuId')) {
							item.changeSkuId = product.changeSkuId;
							item.changeProductName = product.changeProductName;
						}

						if(product.hasOwnProperty('changeMaterialId')) {
							item.changeMaterialId = product.changeMaterialId;
							item.changeProductName = product.changeProductName;
						}

						if(product.hasOwnProperty('changeSkuId') || product.hasOwnProperty('changeMaterialId')) {
							k3ChangeOrderDetailList.push(item);
						} 
						
					}
				});

				if(k3ChangeOrderDetailList.length == 0) {
					bootbox.alert('请选择要换的商品');
					return;
				}

				commitData.k3ChangeOrderDetailList = k3ChangeOrderDetailList;
				commitData.changeOrderNo = self.state.no;

				var des = "保存新添加的换货商品";
				Rental.ajax.submit("{0}k3/addChangeOrder".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.loadData();
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("{0}失败".format(des), Rental.lang.commonJsError +  '<br />' + e );
			}
		}
	};

	window.K3ChangeOrderDetail = _.extend(K3ChangeOrderDetail, K3ChangeOrderMixIn, K3ChangeOrderHandleMixin);

})(jQuery);