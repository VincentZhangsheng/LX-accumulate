;(function($) {

	var K3ChangeOrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_change_order];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_create_change_order],
				view = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_detial],
				edit = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_edit],
				send = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_send],
				addProduct = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_add_product],
				delProduct = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_delete_product],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_cancel],
				submit = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_change_order_submit];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','','添加'));
			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));	
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));	
			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));	
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));	
			// send && this.rowActionButtons.push(AuthorUtil.button(send,'sendButton','','发送'));
			// delProduct && this.rowActionButtons.push(AuthorUtil.button(delProduct,'delProductButton','','删除商品'));	
			// addProduct && this.rowActionButtons.push(AuthorUtil.button(addProduct,'addProductButton','','添加商品'));	
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#changePicker'), $('#changeTimePicker'),  $("#changeStartTime"), $("#changeEndTime"));

			self.searchData();  //初始化列表数据

			self.renderCommonActionButton();

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			Rental.ajax.ajaxData('k3/queryChangeOrder', searchData, '加载换货单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},	
					changeOrderStatusValue:function() {
						return Enum.k3ChangeOrderStatus.getValue(this.changeOrderStatus)
					},
					changeOrderStatusClass:function() {
						return Enum.k3ChangeOrderStatus.getClass(this.changeOrderStatus)	
					}			
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.K3ChangeOrderManage = _.extend(K3ChangeOrderManage, K3ChangeOrderHandleMixin);

})(jQuery);