;(function($){
	
	var K3EditReturnOrder = {
		state:{
			no:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			k3CustomerNo:null,
			k3CustomerName:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_return_order];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_edit];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			this.$form = $("#editOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_k3_manage_return_order); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form, function() {
				self.edit();
			});

			self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到退还单编号');
				return;
			}
			Rental.ajax.submit("{0}k3/queryReturnOrderByNo".format(SitePath.service),{returnOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载退还单详细信息",response.description || '失败');
				}
			}, null ,"加载退还单详细信息");
		},
		initData:function(data) {
			this.setCustomer(data.k3CustomerNo, data.k3CustomerName);
			this.initFormData(data);
			// this.initChooseProductList(data);
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
	  		Rental.ui.renderFormData(self.$form, data);
	  		$('[name=returnTime]', self.$form).val(new Date(data.returnTime).format('yyyy-MM-dd'));
		},
		edit:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					returnOrderNo:self.state.no,
					// k3CustomerNo:self.state.k3CustomerNo,
					// k3CustomerName:self.state.k3CustomerName,
					returnTime:new Date(formData['returnTime']).getTime(),
					returnAddress:formData['returnAddress'],
					returnContacts:formData['returnContacts'],
					returnPhone:formData['returnPhone'],
					returnMode:formData['returnMode'],

					logisticsAmount:!!formData['logisticsAmount'] ? formData['logisticsAmount'] : 0,
					serviceAmount:!!formData['serviceAmount'] ? formData['serviceAmount'] : 0,
					remark:formData['remark'],
					returnReasonType:formData['returnReasonType'],
					deliverySubCompanyId:formData['deliverySubCompanyId']
				}

				// var k3ReturnOrderDetailList = new Array();
				// self.state.chooseProductList.forEach(function(product) {
				// 	k3ReturnOrderDetailList.push({
				// 		orderNo:product.orderNo,
				// 		orderEntry:product.orderEntry,
				// 		productNo:product.productNo,
				// 		productName:product.productName,
				// 		productCount:product.productCount,
				// 		remark:product.remark,
				// 	});
				// });

				// if(k3ReturnOrderDetailList.length == 0) {
				// 	bootbox.alert('请选择要退的商品');
				// 	return;
				// }

				// commitData.k3ReturnOrderDetailList = k3ReturnOrderDetailList;

				var des = '编辑退货单';
				Rental.ajax.submit("{0}k3/updateReturnOrderFromERP".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des,'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建退货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑退货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							// self.$form[0].reset();
							// window.location.reload();
				        }
				    }
				});
			}
		}
	};


	window.K3EditReturnOrder = _.extend(K3EditReturnOrder, K3ReturnOrderMixIn);

})(jQuery);