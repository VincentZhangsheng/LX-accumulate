;(function($) {

	var K3OrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_order_detail];

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));	
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.k3Order);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			// Rental.form.initSearchFormValidation(this.$searchForm,function(){
			// 	self.searchData();
			// });

			// self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			// self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
			// 	event.preventDefault();
			// 	self.searchData();
			// });

			// Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));
			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"), null, moment().subtract(3,'month'), moment());

			

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#orderStatus'),
				data:Enum.array(Enum.orderStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（订单状态）'
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			// var self = this;
			// var searchData = $.extend({
			// 	pageNo:1,
			// 	pageSize:self.Pager.defautPrams.pageSize,
			// }, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			// Rental.searchStorage.set(Rental.searchStorage.enum.k3Order, searchData);

			// Rental.ajax.ajaxData('k3/queryOrder', searchData, '加载订单列表', function(response) {
			// 	self.render(response.resultMap.data);	
			// });
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.k3orderDetail, this.orderNo);
					},
					customerDetailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.buyerCustomerNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"orderStatusStr":function(){
						return Enum.orderStatus.getValue(this.orderStatus);
					},
					orderStatusClass:function() {
						return Enum.orderStatus.getClass(this.orderStatus);
					},
					payStatusValue:function() {
						return Enum.payStatus.getValue(this.payStatus);
					},
					payStatusClass:function() {
						return Enum.payStatus.getClass(this.payStatus);
					},
					"rentTypeStr":function() {
						return Enum.rentType.getValue(this.rentType);
					},
					orderItemName:function() {
						if(this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0) {
							return this.orderProductList[0].productName;
						}
						if(this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0) {
							return this.orderMaterialList[0].materialName;
						}
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.K3OrderManage = _.extend(K3OrderManage, K3OrderHandleMixin);

})(jQuery);