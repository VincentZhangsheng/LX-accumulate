;(function($){
	
	var K3AddChangeOrder = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_change_order];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_create_change_order];
		},
		initDom:function() {
			this.$form = $("#addOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_k3_manage_change_order); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			self.initCommonEvent(); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					k3CustomerNo:self.state.k3CustomerNo,
					k3CustomerName:self.state.k3CustomerName,
					changeTime:new Date(formData['changeTime']).getTime(),
					changeAddress:formData['changeAddress'],
					changeContacts:formData['changeContacts'],
					changePhone:formData['changePhone'],
					changeMode:formData['changeMode'],
					logisticsAmount:!!formData['logisticsAmount'] ? formData['logisticsAmount'] : 0,
					serviceAmount:!!formData['serviceAmount'] ? formData['serviceAmount'] : 0,
					remark:formData['remark'],
				}

				var k3ChangeOrderDetailList = new Array();

				self.state.chooseProductList.forEach(function(product) {
					var item  = {
						orderNo:product.orderNo,
						orderEntry:product.orderEntry,
						productNo:product.productNo,
						productName:product.productName,
						productCount:product.productCount,
						remark:product.remark,
						orderItemId:product.orderItemId,
						productDiffAmount: product.productDiffAmount, //product.orderItemId,
					}
					
					if(product.hasOwnProperty('changeSkuId')) {
						item.changeSkuId = product.changeSkuId;
						item.changeProductName = product.changeProductName;
					}

					if(product.hasOwnProperty('changeMaterialId')) {
						item.changeMaterialId = product.changeMaterialId;
						item.changeProductName = product.changeProductName;
					}

					k3ChangeOrderDetailList.push(item);
				});

				if(k3ChangeOrderDetailList.length == 0) {
					bootbox.alert('请选择要换的商品');
					return;
				}

				commitData.k3ChangeOrderDetailList = k3ChangeOrderDetailList;

				var des = "创建换货单";
				Rental.ajax.submit("{0}k3/createChangeOrder".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("{0}失败".format(des), Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建换货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
		}
	};


	window.K3AddChangeOrder = _.extend(K3AddChangeOrder, K3ChangeOrderMixIn);

})(jQuery);