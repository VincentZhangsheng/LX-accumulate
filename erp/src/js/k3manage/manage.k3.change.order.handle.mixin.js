;(function($) {

	var K3ChangeOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;
			
			container.on('click', '.rowDelButton', function(event) {
				event.preventDefault();
				var k3ChangeOrderDetailId = $(this).data('k3changeorderdetailid');
				bootbox.confirm('确认删除？',function(result) {
					result && self.handleDo('k3/deleteChangeOrder', {k3ChangeOrderDetailId:k3ChangeOrderDetailId}, '删除换货商品', handleCallBack);
				});
			});

			container.on('click', '.sendButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				bootbox.confirm('确认发送订单到K3？',function(result) {
					result && self.handleDo('k3/sendChangeOrderToK3', {returnOrderNo:returnOrderNo}, '发送换货单到K3', handleCallBack);	
				});
			});

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				console.log($(this).data('no'))
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.k3ChangeOrder,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var changeOrderNo = $(this).data('no');
				bootbox.confirm('确认取消换货单？',function(result) {
					result && self.handleDo('k3/cancelK3ChangeOrder', {changeOrderNo:changeOrderNo}, '取消换货单', handleCallBack);	
				});
			});

			container.on('click', '.addProductButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				K3OrderItemChoose.init({
					callBack:function(productArray) {
						self.addProduct(no, productArray, handleCallBack);
					}
				});
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交换货单', submitUrl = 'k3/commitK3ChangeOrder';
			if(!isAudit) {
				bootbox.confirm('确认'+des+'？', function(result) {
					result && self.handleDo(submitUrl, {
						changeOrderNo:prams.no,
					}, des, callBack);
				});
				return;
			} else {
				self.handleDo(submitUrl, {
					changeOrderNo:prams.no,
					verifyUserId:prams.verifyUserId,
					commitRemark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		addProduct:function(no, productArray, callback) {

			var commitData = {
				changeOrderNo:no,
				k3ChangeOrderDetailList:productArray,
			}

			this.handleDo('k3/addReturnOrder', commitData, '添加换货商品', function() {
				callback && callback();
				bootbox.confirm({
				    message: "成功添换货商品",
				    buttons: {
				        confirm: {
				            label: '完成',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	Rental.modal.close();
				        }
				    }
				});
				
			});	
		},
		handleDo:function(url, prams, des, callBack) {
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
		
			(buttons || []).forEach(function(button, index) {
				button.no = order.changeOrderNo;
				rowActionButtons.push(button);

				if(_.indexOf([
					'editButton', 
					'sendButton', 
					'rowDelButton', 
					'addProductButton', 
					'cancelButton', 
					'submitButton', 
					], button.class) > -1 && order.changeOrderStatus != Enum.changeOrderStatus.num.unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

			});
			return rowActionButtons;
		}		
	}

	window.K3ChangeOrderHandleMixin = K3ChangeOrderHandleMixin;

})(jQuery);