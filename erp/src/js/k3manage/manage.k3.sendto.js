;(function($) {

	var K3SendToManage = {
		state:{},
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			Rental.ui.renderSelect({
				container:$('#recordType'),
				data:Enum.array(Enum.k3recordType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（记录类型）'
			});

			Rental.ui.renderSelect({
				container:$('#receiveResult'),
				data:Enum.array(Enum.result),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（接收状态）'
			});

			Rental.ui.renderSelect({
				container:$('#sendResult'),
				data:Enum.array(Enum.result),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（推送状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$dataListTable.on('click', '.sendToK3', function(event) {
				event.preventDefault();
				self.orderDo('k3/seedAgainK3SendRecord', {k3SendRecordId:$(this).data('k3sendrecordid')}, '发送数据到K3', function(){
					self.doSearch(self.Pager.pagerData.currentPage);
				})
			});

			$("#batchSendDataToK3").on('click', function(event) {
				event.preventDefault();
				self.bathToSendK3();
			});
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			Rental.ajax.ajaxData('k3/queryK3SendRecord', searchData, '加载推送数据', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					recordTypeValue:function() {
						return Enum.k3recordType.getValue(this.recordType);
					},
					sendResultValue:function() {
						return Enum.result.getValue(this.sendResult);
					},
					receiveResultValue:function() {
						return Enum.result.getValue(this.receiveResult);
					},
					noSend:function() {
						// return this.sendResult != Enum.result.num.success || this.receiveResult != Enum.result.num.success;
						return true;
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		orderDo:function(url, prams, des, callBack) {
			// if(!prams.k3SendRecordId) {
			// 	bootbox.alert('找不到发送ID');
			// 	return;
			// }
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		bathToSendK3:function() {
			var self = this;
			InputModal.init({
				title:'发送数据到K3',
				url:'send-data-to-k3/modal',
				initFunc:function(modal) {

					Rental.ui.renderSelect({
						container:$('[name=recordType]', modal),
						data:Enum.array(Enum.k3recordType),
						func:function(opt, item) {
							return opt.format(item.num.toString(), item.value.toString());
						},
						change:function(val) {
						},
						defaultText:'请选择'
					});

					Rental.ui.events.initRangeDatePicker($('#sendTimePicker'), $('#sendTime'),  $("#startTime"), $("#endTime"));
				},
				callBack:function(result) {
					self.orderDo('k3/batchSendDataToK3', {
						recordType:result.recordType,
						batchType:result.batchType,
						startTime:result.startTime,
						endTime:result.endTime,
						intervalTime:result.intervalTime,
					}, '发送数据到K3');
					return true;
				}
			})
		}
	};

	window.K3SendToManage = K3SendToManage;

})(jQuery);