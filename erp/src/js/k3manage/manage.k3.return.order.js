;(function($) {

	var K3ReturnOrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_return_order];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_create_return_order],
				view = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_detial],
				edit = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_edit],
				send = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_send],
				addProduct = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_add_product],
				delProduct = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_delete_product],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_cancel],
				strongCancel = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_strong_cancel],
				print = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_print],
				createStatement = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_create_statement],
				recalReturn = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_recalculationt],
				rollback = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_rollback],
				submit = this.currentPageAuthor.children[AuthorCode.manage_k3_manage_return_order_submit];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','','添加'));
			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','详情'));	
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑基本信息'));	
			// send && this.rowActionButtons.push(AuthorUtil.button(send,'sendButton','','发送'));

			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));	
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));	
			strongCancel && this.rowActionButtons.push(AuthorUtil.button(strongCancel,'revokeReturnOrderButton','','强制取消'));
			createStatement && this.rowActionButtons.push(AuthorUtil.button(createStatement,'statementButton','','生成结算单'));
			recalReturn && this.rowActionButtons.push(AuthorUtil.button(recalReturn,'recalButton','','退货重算'));
			rollback && this.rowActionButtons.push(AuthorUtil.button(rollback,'rollbackButton','','退货单回滚'));
			print && this.rowActionButtons.push(AuthorUtil.button(print,'printButton','','打印',true,true));	
			// delProduct && this.rowActionButtons.push(AuthorUtil.button(delProduct,'delProductButton','','删除商品'));	
			// addProduct && this.rowActionButtons.push(AuthorUtil.button(addProduct,'addProductButton','','添加商品'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.k3ReturnOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			// Rental.ui.events.initRangeDatePicker($('#returnPicker'), $('#returnPickerInput'),  $("#returnStartTime"), $("#returnEndTime"), null, moment().subtract(3,'month'), moment());
			Rental.ui.events.initRangeDatePicker($('#returnPicker'), $('#returnPickerInput'),  $("#returnStartTime"), $("#returnEndTime"));

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#returnOrderStatus'),
				data:Enum.array(Enum.k3ReturnOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（退货单状态）'
			});

			ApiData.company({
				success:function(response) {
					var deliverySubCompanyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.headOffice && company.subCompanyId != Enum.subCompany.num.telemarketing && company.subCompanyId != Enum.subCompany.num.channelBigCustomer;
					});

					Rental.ui.renderSelect({
						container:$("#deliverySubCompanyId"),
						data:deliverySubCompanyList,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:"请选择退货公司"
					});
				}
			});

			self.renderReturnStatus();
			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});


			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		renderReturnStatus:function() {
			var returnOrderStatus = Rental.helper.getUrlPara('returnOrderStatus')
			if(!!returnOrderStatus) {
				Rental.ui.renderFormData(this.$searchForm, {returnOrderStatus:returnOrderStatus});
			}
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.k3ReturnOrder, searchData);

			Rental.ajax.ajaxData('k3/queryReturnOrder', searchData, '加载退货单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, this.returnOrderNo);
					},
					customerDetailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.k3CustomerNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},	
					returnOrderStatusValue:function() {
						return Enum.k3ReturnOrderStatus.getValue(this.returnOrderStatus);
					},
					returnOrderStatusClass:function() {
						return Enum.k3ReturnOrderStatus.getClass(this.returnOrderStatus);
					},
					showViewWorkFlowButton:function() {
						return this.returnOrderStatus == Enum.k3ReturnOrderStatus.num.inReview || this.returnOrderStatus == Enum.k3ReturnOrderStatus.num.reject;
					},
					successStatusVal:function() {
						return this.successStatus == 1 ? "已结算" : (this.successStatus == 0 ? "未结算" : "")
					},
					successStatusClass:function() {
						return this.successStatus == 1 ? "text-success" : (this.successStatus == 0 ? "text-danger" : "")
					},	
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.K3ReturnOrderManage = _.extend(K3ReturnOrderManage, K3ReturnOrderHandleMixin);

})(jQuery);