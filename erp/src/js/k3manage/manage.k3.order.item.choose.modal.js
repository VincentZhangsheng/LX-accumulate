;(function($) {
	var K3OrderItemChoose = {
		init:function(prams) {
			this.props = _.extend({
				buyerCustomerNo:null,
				buyerRealName:null,
				callBack:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "k3-order/item-choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$chooseModal = modal.container;

			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			self.render(new Object());
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			self.searchData();  //初始化列表数据

			// self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('[name=orderStatus]', self.$chooseModal),
				data:Enum.array(Enum.orderStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（订单状态）'
			});

			// self.$dataListTable.on('click', '.chooseButton', function(event) {
			// 	event.preventDefault();
			// 	var $this =  $(this), productId = $this.data('productid');
			// 	self.choose($this,productId);
			// });

			// self.$dataListTable.on('click', '.confirmChoose', function(event) {
			// 	event.preventDefault();
			// 	self.confirm($(this));
			// });

			// self.$dataListTable.on('click', '.cancelChoose', function(event) {
			// 	event.preventDefault();
			// 	$(this).closest('.productDetial').slideUp();
			// });

			self.$dataListTable.on('click', '.chooseOrderItems', function(event) {
				event.preventDefault();
				var orderNo = $(this).data("orderno");
				self.choose($(this),orderNo);
			});

			self.$dataListTable.on('click', '.cancelChoose', function(event) {
				event.preventDefault();
				$(this).closest('.orderItmesContainer').toggle('fast');
			});

			self.$dataListTable.on('click', '.confirmChoose', function(event) {
				event.preventDefault();
				self.confirm($(this));
			});

			self.$dataListTable.on('click', '.selector', function(event) {
				e.preventDefault()
			  	$(this).tab('show')
			});

			self.$dataListTable.on('click', '.tab-menu', function(event) {
				event.preventDefault();
				var id = $(this).children('a').attr('href');
				$(id).siblings('.tab-pane').removeClass('active');
				$(id).addClass('active');
			});

			self.$dataListTable.on('change', '.productCount', function(event) {
				event.preventDefault();
				self.changeProductCount($(this));
			});

			self.$dataListTable.on('change', '.materialCount', function(event) {
				event.preventDefault();
				self.changeMaterialCount($(this));
			});

			Rental.helper.onOnlyNumber(self.$dataListTable, '.productCount'); 
			Rental.helper.onOnlyNumber(self.$dataListTable, '.materialCount'); 
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, {
				customerNo:self.props.buyerCustomerNo,
			}, prams || {});
			Rental.ajax.ajaxData('k3/queryOrderForReturn', searchData, '加载订单列表', function(response) {
				self.render(response.resultMap.data);	
			}, null, 'dialogLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					orderJSONString:function() {
						return JSON.stringify(this);
					},
					"orderStatusStr":function(){
						return Enum.orderStatus.getValue(this.orderStatus);
					},
					orderStatusClass:function() {
						return Enum.orderStatus.getClass(this.orderStatus);
					},
					payStatusValue:function() {
						return Enum.payStatus.getValue(this.payStatus);
					},
					payStatusClass:function() {
						return Enum.payStatus.getClass(this.payStatus);
					},
					"rentTypeStr":function() {
						return Enum.rentType.getValue(this.rentType);
					},
					orderItemName:function() {
						if(this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0) {
							return this.orderProductList[0].productName;
						}
						if(this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0) {
							return this.orderMaterialList[0].materialName;
						}
					},
					rentingProductCount:function() {
						if(this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0) {
							return this.orderProductList[0].rentingProductCount;
						}
						if(this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0) {
							return this.orderMaterialList[0].rentingMaterialCount;
						}
					},
					hasOrderProduct:function() {
						return this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0;
					},
					hasOrderMaterialList:function() {
						return this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0;
					},
					orderProductListCount:function() {
						return this.hasOwnProperty('orderProductList') ? this.orderProductList.length : 0;
					},
					orderMaterialListCount:function() {
						return this.hasOwnProperty('orderMaterialList') ? this.orderMaterialList.length : 0;
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function($button,id) {
			var detailTr =  $button.closest('tr').next('tr'), productDetial = $('.orderItmesContainer',detailTr);
			if(productDetial.size() > 0) {
				productDetial.slideDown(); 
			} else {
				this.getOrderById($('td',detailTr),id);	
			}
		},
		getOrderById:function(container,orderNo) {
			var self = this;
			Rental.ajax.ajaxData('k3/queryOrderByNoForReturn', {orderNo:orderNo}, '加载订单详细信息', function(response) {
				self.renderRowProduct(container,response.resultMap.data);
			}, null, 'dialogLoading');
		},
		renderRowProduct:function(container,order) {
			var self = this;
			var data  = {
				order:order,
				orderJSONString:function() {
					return JSON.stringify(this);
				},
				orderProductData:function() {
					var productList = this.orderProductList;
					return _.extend(Rental.render, {
						showIsNewCheckBox:self.props.showIsNewCheckBox,
						productList:productList,
						skuInfo:function() {
							return JSON.stringify(this);
						},
						propertiesToStr:function() {
							var productInfo = this.hasOwnProperty('productSkuSnapshot') ? JSON.parse(this.productSkuSnapshot) : {};
							var skuList = (productInfo.hasOwnProperty("productSkuList") && productInfo.productSkuList.length > 0 ) ? productInfo.productSkuList[0] : {};
							var str = skuList.hasOwnProperty('productSkuPropertyList') ? skuList.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				},
				orderMaterialData:function() {
					var materialList = this.orderMaterialList;
					return _.extend(Rental.render, {
						showIsNewCheckBox:self.props.showIsNewCheckBox,
						materialList:materialList,
						skuInfo:function() {
							return JSON.stringify(this);
						},
						propertiesToStr:function() {
							var str = this.hasOwnProperty('materialSkuPropertyList') ? this.materialSkuPropertyList.map(function(item) {
								return item.materialValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				}
			}
			var tpl = $('#orderProductListTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		changeProductCount:function(countIpt) {
			var productInfo = countIpt.closest("tr").data("skuinfo")
			var chooseVal = countIpt.val();
			if(chooseVal > productInfo.canReturnProductCount) {
				bootbox.alert('退货数量不能大于可退货数量');
				countIpt.val(productInfo.canReturnProductCount)
			}
			if(chooseVal < 0) {
				bootbox.alert('退货数量不能小余0');
				countIpt.val("0")
			}
		},
		changeMaterialCount:function(countIpt) {
			var productInfo = countIpt.closest("tr").data("skuinfo")
			var chooseVal = countIpt.val();
			if(chooseVal > productInfo.canReturnMaterialCount) {
				bootbox.alert('退货数量不能大于可退货数量');
				countIpt.val(productInfo.canReturnMaterialCount)
			}
			if(chooseVal < 0) {
				bootbox.alert('退货数量不能小余0');
				countIpt.val("0")
			}
		},
		confirm:function($confirmButton) {
			var $container = $confirmButton.closest('.orderItmesContainer');
			var order = $container.data('order');

			var checkedProduct = $('[name=productNo]',$container).filter(':checked');
			var checkedMaterial = $('[name=fNumber]',$container).filter(':checked');

			if(checkedProduct.length == 0 && checkedMaterial.length == 0) {
				bootbox.alert("请选择订单商品项或配件项");
				return false;
			}

			var chooseProduct = new Array();
			var orderSubCompanyId = (order.hasOwnProperty("orderSubCompanyId") && (order.orderSubCompanyId != Enum.subCompany.num.headOffice && order.orderSubCompanyId != Enum.subCompany.num.telemarketing && order.orderSubCompanyId != Enum.subCompany.num.channelBigCustomer)) ? order.orderSubCompanyId : 
					(order.hasOwnProperty("deliverySubCompanyId") ? order.deliverySubCompanyId : ""),
					orderSubCompanyName = order.hasOwnProperty("orderSubCompanyName") ? order.orderSubCompanyName : "",
					deliverySubCompanyName = order.hasOwnProperty("deliverySubCompanyName") ? order.deliverySubCompanyName : "";

			checkedProduct.length > 0 && checkedProduct.each(function(i, item) {
				var checkbox = $(item),
					productTr = checkbox.closest('.productTr');
					orderItemId = parseInt($('.orderItemId', productTr).val()),
					fEntryID = parseInt($('.fEntryID', productTr).val()),
					product = _.findWhere(order.orderProductList, {productNumber: checkbox.val(), orderProductId:orderItemId, fEntryID:fEntryID}),
					productCount = $('.productCount', productTr).val(),
					remark = $('.remark', productTr).val(),
					skuStr = productTr.data("skulist");

				if(product) {
					chooseProduct.push({
						orderNo:order.orderNo,
						orderEntry:product.fEntryID,
						productNo:product.productNumber,
						productName:product.productName,
						productCount:productCount,
						rentingCount:product.rentingProductCount,
						canReturnCount:product.canReturnProductCount,
						k3CustomerNo:order.buyerCustomerNo,
						k3CustomerName:order.buyerCustomerName,
						orderItemId:product.orderProductId,
						productId:product.productId,
						rentType:product.rentType,
						rentLengthType:product.rentLengthType,
						rentTimeLength:product.rentTimeLength,
						remark:remark,
						orderSubCompanyId:orderSubCompanyId,
						orderSubCompanyName:orderSubCompanyName,
						deliverySubCompanyName:deliverySubCompanyName,
						skuStr:skuStr
					})
				}
			});

			checkedMaterial.length > 0 && checkedMaterial.each(function(i, item) {
				var checkbox = $(item),
					materialTr = checkbox.closest('.materialTr');
					orderItemId = parseInt($('.orderItemId', materialTr).val()),
					fEntryID = parseInt($('.fEntryID', materialTr).val()),
					material = _.findWhere(order.orderMaterialList, {fNumber: checkbox.val(), orderMaterialId:orderItemId, fEntryID:fEntryID}),
					materialCount = $('.materialCount', materialTr).val(),
					remark = $('.remark', materialTr).val();
				
				if(material) {
					chooseProduct.push({
						orderNo:order.orderNo,
						orderEntry:material.fEntryID,
						productNo:material.fNumber,
						productName:material.materialName,
						productCount:materialCount,
						rentingCount:material.rentingMaterialCount,
						canReturnCount:material.canReturnMaterialCount,
						k3CustomerNo:order.buyerCustomerNo,
						k3CustomerName:order.buyerCustomerName,
						orderItemId:material.orderMaterialId,
						materialId:material.materialId,
						rentType:material.rentType,
						rentLengthType:material.rentLengthType,
						rentTimeLength:material.rentTimeLength,
						remark:remark,
						orderSubCompanyId:orderSubCompanyId,
						orderSubCompanyName:orderSubCompanyName,
						deliverySubCompanyName:deliverySubCompanyName,
						productSkuSnapshot:material.productSkuSnapshot
					})
				}
			});

			this.props.callBack && this.props.callBack(chooseProduct);
		}
	};

	window.K3OrderItemChoose = K3OrderItemChoose;

})(jQuery)