;(function($) {

	K3OrderMixIn = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			customerNo:null,
			order:new Object(),
		},
		initCommonEvent:function() {
			var self = this;

			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});

			self.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		renderProductList:function(data){
			 this.renderProductListFunc(data, this.$dataListTpl, this.$dataListTable, null, this.state.order);
		},
		renderProductListFunc:function(listData, tpl, container, rowActionButtons, order) {
			console.log(listData)
			$("#orderItemProductCount").html(listData.length);
			rowActionButtons = rowActionButtons || new Array();
			var data = _.extend(Rental.render, {
				listData:listData,
				rowActionButtons:rowActionButtons,
				productInfo:function() {
					if(this.hasOwnProperty('categoryName')) {
						return this;
					}
					if(this.hasOwnProperty('productSkuSnapshot')) {
						return JSON.parse(this.productSkuSnapshot);
					}
					if(this.hasOwnProperty('returnProductSkuSnapshot')) {
						return JSON.parse(this.returnProductSkuSnapshot);
					}
					if(this.hasOwnProperty('deploymentProductSkuSnapshot')) {
						return JSON.parse(this.deploymentProductSkuSnapshot);	
					}
				},
				propertiesToStr:function() {
					var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
						return item.propertyValueName;
					}).join(" | ") : '';
					return str;
				},
				rowKey:function() {
					return parseInt(Math.random()*(100+1),10); //0-100随机数
				},
				rentTypeList:function() {
					return Enum.array(Enum.rentType, this.rentType);
				},
				rentTypeValue:function() {
					return Enum.rentType.getValue(this.rentType);
				},
				rentTypeUnit:function() {
					return Enum.rentType.getUnit(this.rentType);
				},
				isNewProductBool:function() {
					return this.isNewProduct == 1;
				},
				isNewIntValue:function() {
					if(this.hasOwnProperty('isNewProduct')) {
						return this.isNewProduct;
					} else if(this.hasOwnProperty('isNew')) {
						return this.isNew;
					}
				},
				currentSkuPrice:function() {
					if((this.hasOwnProperty('isNewProduct') && this.isNewProduct) || (this.hasOwnProperty('isNew') && this.isNew)) {
						return this.newSkuPrice ? this.newSkuPrice.toFixed(2) : '';
					} else {
						return this.skuPrice ? this.skuPrice.toFixed(2) : '';
					}
				},
				isNewProductBadgeClass:function() {
					return this.isNewProduct == 1 ? 'badge-primary':'badge-default';
				},
				showDepositAmount:function() {
					//小于90天需要设备押金，和选择支付方式
					// return this.rentType == Enum.rentType.num.byDay && parseInt(this.rentTimeLength) < 90;
					console.log(order);
					return order.rentType == Enum.rentType.num.byDay && parseInt(order.rentTimeLength) < 90;
				},
				payModeList:function() {
					var array = Enum.array(Enum.payMode, this.payMode);	
					if(order.rentType == Enum.rentType.num.byDay) {
						array = Enum.arrayByEnumObj(Enum.payMode, Enum.payMode.numForOrder, this.payMode);	
					}
					return array;
				},
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				"mainImg":function() {
					return Rental.helper.mainImgUrlFormat(this.productImgList);
				},
				'productImgJSON':function() {
					return Rental.helper.imgListToJSONStringify(this.productImgList);
				},
				isRentStr:function() {
					return this.isRent == 1 ? '在租':"下架";
				},
				rowspan:function() {
					return this.hasOwnProperty('chooseProductSkuList') ? this.chooseProductSkuList.length : 0;
				},
				orderProductEquipmentListCount:function() {
					return this.hasOwnProperty('orderProductEquipmentList') ? this.orderProductEquipmentList.length : 0;
				},
				/**
				* 订单详细配货设备列信息
				*/
				orderProductEquipmentListData:function() {
					var orderProductEquipmentList = this.orderProductEquipmentList;
					return _.extend(Rental.render, {
						list:orderProductEquipmentList,
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				},
				/**
				* 调拨单详细配货设备列信息
				*/
				deploymentOrderProductEquipmentListData:function() {
					var deploymentOrderProductEquipmentList = this.deploymentOrderProductEquipmentList;
					return _.extend(Rental.render, {
						deploymentOrderProductEquipmentListSource:deploymentOrderProductEquipmentList,
						sourcceLength:deploymentOrderProductEquipmentList.length,
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				},
				/**
				* 调拨单 添加、编辑商品项小计联动计算
				*/
				deploymentProductAmountTotal:function() {
					var deploymentProductAmount = 0;
					if(this.hasOwnProperty('deploymentProductUnitAmount') && this.hasOwnProperty('deploymentProductSkuCount')) {
						deploymentProductAmount = parseFloat(this.deploymentProductUnitAmount)*parseFloat(this.deploymentProductSkuCount);
					} else if(this.hasOwnProperty('deploymentProductAmount')) {
						deploymentProductAmount = this.deploymentProductAmount;
					}
					return deploymentProductAmount ? deploymentProductAmount.toFixed(2) : 0;
				},
				/*
				* 换货单更换商品信息
				*/
				destChangeProductInfo:function() {
					var self = this;
					if(self.hasOwnProperty('destChangeProduct')) {
						return {
							destChangeProductData:self.destChangeProduct,
							propertiesToStr:function() {
								var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
									return item.propertyValueName;
								}).join(" | ") : '';
								return str;
							},
							productInfo:function() {
								return this;
							},
							isNewIntValue:function() {
								if(this.hasOwnProperty('isNewProduct')) {
									return this.isNewProduct;
								} else if(this.hasOwnProperty('isNew')) {
									return this.isNew;
								} else if(self.hasOwnProperty('isNewProduct')) {
									return self.isNewProduct;
								} else if(self.hasOwnProperty('isNew')) {
									return self.isNew;
								} 
							},
						}
					} else {
						return null;
					}
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		renderMaterialList:function(data) {
			this.renderMaterialListFunc(data, this.$materialDataListTpl, this.$materialDataListTable, null, this.state.order);
		},
		renderMaterialListFunc:function(listData, tpl, container, rowActionButtons, order) {
			rowActionButtons = rowActionButtons || new Array();
			$("#orderItemMaterialCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:rowActionButtons,
					materialInfo:function() {
						if(this.hasOwnProperty('brandName')) {
							return this;
						}
						if(this.hasOwnProperty('returnMaterialSnapshot')) {
							return JSON.parse(this.returnMaterialSnapshot);
						}
						if(this.hasOwnProperty('deploymentProductMaterialSnapshot')) {
							return JSON.parse(this.deploymentProductMaterialSnapshot);	
						}
					},
					materialJSONString:function() {
						return JSON.stringify(this);
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList, this.materialName);
					},
					rentTypeList:function() {
						return Enum.array(Enum.rentType, this.rentType);
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					isNewMaterialBool:function() {
						return this.isNewMaterial == 1;
					},
					isNewIntValue:function() {
						if(this.hasOwnProperty('isNewMaterial')) {
							return this.isNewMaterial;
						} else if(this.hasOwnProperty('isNew')) {
							return this.isNew;
						}
					},
					currentMaterialPrice:function() {
						if((this.hasOwnProperty('isNewMaterial') && this.isNewMaterial) || (this.hasOwnProperty('isNew') && this.isNew)) {
							return this.newMaterialPrice ? this.newMaterialPrice.toFixed(2) : '';
						} else {
							return this.materialPrice ? this.materialPrice.toFixed(2) : '';
						}
					},
					isNewMaterialBadgeClass:function() {
						return this.isNewMaterial == 1 ? 'badge-primary':'badge-default';
					},
					isNewMaterialValue:function() {
						return this.isNewMaterial == 1 ? '全新':'次新';
					},
					showDepositAmount:function() {
						//小于90天需要设备押金，和选择支付方式
						// return this.rentType == Enum.rentType.num.byDay && parseInt(this.rentTimeLength) < 90;
						return order.rentType == Enum.rentType.num.byDay && parseInt(order.rentTimeLength) < 90;
					},
					payModeList:function() {
						var array = Enum.array(Enum.payMode, this.payMode);	
						if(order.rentType == Enum.rentType.num.byDay) {
							array = Enum.arrayByEnumObj(Enum.payMode, Enum.payMode.numForOrder, this.payMode);	
						}
						return array;
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
				
				})
			}
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
	};

	window.K3OrderMixIn = K3OrderMixIn;

})(jQuery);