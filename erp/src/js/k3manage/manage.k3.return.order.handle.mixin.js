;(function($) {

	var K3ReturnOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;
			
			container.on('click', '.rowDelButton', function(event) {
				event.preventDefault();
				var k3ReturnOrderDetailId = $(this).data('k3returnorderdetailid');
				bootbox.confirm('确认删除？',function(result) {
					result && self.handleDo('k3/deleteReturnOrder', {k3ReturnOrderDetailId:k3ReturnOrderDetailId}, '删除退货商品', handleCallBack);
				});
			});

			container.on('click', '.sendButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				bootbox.confirm('确认发送订单到K3？',function(result) {
					result && self.handleDo('k3/sendToK3', {returnOrderNo:returnOrderNo}, '发送退货单到K3', handleCallBack);	
				});
			});

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.k3ReturnOrder,
					verifyAttachment:false,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				bootbox.confirm('确认取消退货单？',function(result) {
					result && self.handleDo('k3/cancelK3ReturnOrder', {returnOrderNo:returnOrderNo}, '取消退货单', handleCallBack);	
				});
			});

			container.on('click', '.revokeReturnOrderButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				bootbox.confirm('确认取消退货单？',function(result) {
					result && self.handleDo('k3/revokeReturnOrder', {returnOrderNo:returnOrderNo}, '取消退货单', handleCallBack);	
				});
			});

			container.on('click', '.recalButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				bootbox.confirm('确认重算退货单？',function(result) {
					result && self.handleDo('statementOrder/reStatementK3ReturnOrderRentOnly', {returnOrderNo:returnOrderNo}, '重算退货单', handleCallBack);	
				});
			});

			container.on('click', '.addProductButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				K3OrderItemChoose.init({
					buyerCustomerNo:self.state.k3CustomerNo,
					buyerRealName:self.state.k3CustomerName,
					callBack:function(productArray) {
						self.addProduct(no, productArray, handleCallBack);
					}
				});
			});

			container.on('click', '.viewWorkFlowButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('returnorderno');
				ViewWorkFlow.init({
					workflowType:Enum.workflowType.num.k3ReturnOrder,
					workflowReferNo:returnOrderNo,
				});
			});

			// 生成结算单
			container.on('click', '.statementButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				self.createStatment(returnOrderNo, handleCallBack);
			});

			// 退货单回滚
			container.on('click', '.rollbackButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('no');
				self.rollbackFunc(returnOrderNo, handleCallBack);
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交退货单', submitUrl = 'k3/commitK3ReturnOrder';
			if(!isAudit) {
				bootbox.confirm('确认'+des+'？', function(result) {
					result && self.handleDo(submitUrl, {
						returnOrderNo:prams.no,
					}, des, callBack);
				});
				return;
			} else {
				self.handleDo(submitUrl, {
					returnOrderNo:prams.no,
					verifyUserId:prams.verifyUserId,
					remark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		addProduct:function(no, productArray, callback) {
			var self = this;
			var k3ReturnOrderDetailList = new Array();

			productArray.forEach(function(item) {
				var productIndex = _.findIndex(self.state.chooseProductList, {productNo:item.productNo, orderItemId:item.orderItemId.toString(), orderNo:item.orderNo, orderEntry:item.orderEntry.toString()});
				if(productIndex == -1) k3ReturnOrderDetailList.push(item);
			});	

			if(k3ReturnOrderDetailList.length == 0) {
				bootbox.alert('退货单中已存在该订单商品');
				return;
			}

			var filterReturnOrderList = k3ReturnOrderDetailList.filter(function(item) {
				return item.productCount == 0;
			})

			if(filterReturnOrderList.length > 0) {
				bootbox.alert('请选择在租数量大于0的商品项或配件项');
				return;
			}

			var commitData = {
				returnOrderNo:no,
				k3ReturnOrderDetailList:k3ReturnOrderDetailList,
			}

			this.handleDo('k3/addReturnOrder', commitData, '添加退货商品', function() {
				callback && callback();
				bootbox.confirm({
				    message: "成功添加退货商品",
				    buttons: {
				        confirm: {
				            label: '完成',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	Rental.modal.close();
				        }
				    }
				});
			});	

			// this.state.chooseProductList = this.chooseProductFunc(this.state.chooseProductList, productArray);
			// this.renderProductList();
			// bootbox.confirm({
			//     message: "成功选择商品",
			//     buttons: {
			//         confirm: {
			//             label: '完成',
			//             className: 'btn-primary'
			//         },
			//         cancel: {
			//             label: '继续',
			//             className: 'btn-default'
			//         }
			//     },
			//     callback: function (result) {
			//         if(result) {
			//         	Rental.modal.close();
			//         }
			//     }
			// });
		},
		createStatment:function(returnOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认生成结算单？', function(result) {
				result && self.handleDo('statementOrder/createK3ReturnOrderStatement', {returnOrderNo:returnOrderNo}, '生成结算单', callBack);
			});
		},
		rollbackFunc:function(returnOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认该退货单回滚？', function(result) {
				result && self.handleDo('statementOrder/rollbackSuccessReturnOrder', {returnOrderNo:returnOrderNo}, '退货单回滚', callBack);
			});
		},
		handleDo:function(url, prams, des, callBack) {
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
			var successStatus = order.successStatus == 1;
		
			(buttons || []).forEach(function(button, index) {
				button.no = order.returnOrderNo;
				rowActionButtons.push(button);

				if(_.indexOf([
					// 'editButton', 
					'sendButton', 
					// 'rowDelButton', 
					// 'addProductButton', 
					// 'cancelButton', 
					// 'submitButton', 
					], button.class) > -1 && order.returnOrderStatus != Enum.k3ReturnOrderStatus.num.unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf([
					'editButton',
					'rowDelButton',
					'submitButton',
					'addProductButton',
					'cancelButton',
					], button.class) > -1 && (order.returnOrderStatus != Enum.k3ReturnOrderStatus.num.reject && order.returnOrderStatus != Enum.k3ReturnOrderStatus.num.unSubmit)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf([
					'revokeReturnOrderButton', 
					], button.class) > -1 && (order.returnOrderStatus != Enum.k3ReturnOrderStatus.num.inHand)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'statementButton') && (successStatus || order.returnOrderStatus != Enum.k3ReturnOrderStatus.num.completed)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'recalButton' || button.class == 'rollbackButton') && !successStatus) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		}					
	}

	window.K3ReturnOrderHandleMixin = K3ReturnOrderHandleMixin;

})(jQuery);