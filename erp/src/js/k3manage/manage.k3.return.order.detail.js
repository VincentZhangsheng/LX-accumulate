;(function($){
	
	var K3ReturnOrderDetail = {
		state:{
			no:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_return_order];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_detial];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();

			this.productAcitonButtons = new Array();
			this.rowAcitonButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_create_return_order],
				view = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_detial],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_edit],
				send = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_send],
				addProduct = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_add_product],
				delProduct = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_delete_product],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_cancel],
				strongCancel = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_strong_cancel],
				createStatement = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_create_statement],
				print = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_print],
				rollback = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_rollback],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_submit];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));	
			// send && this.actionButtons.push(AuthorUtil.button(send,'sendButton','','发送'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));	
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));	
			strongCancel && this.actionButtons.push(AuthorUtil.button(strongCancel,'revokeReturnOrderButton','','强制取消'));	
			createStatement && this.actionButtons.push(AuthorUtil.button(createStatement,'statementButton','','生成结算单'));
			print && this.actionButtons.push(AuthorUtil.button(print,'recalButton','','退货重算'));
			rollback && this.actionButtons.push(AuthorUtil.button(rollback,'rollbackButton','','退货单回滚'));
			print && this.actionButtons.push(AuthorUtil.button(print,'printButton','','打印',true,true));
			
			delProduct && this.rowAcitonButtons.push(AuthorUtil.button(delProduct,'rowDelButton','','删除'));
			addProduct && this.productAcitonButtons.push(AuthorUtil.button(delProduct,'addProductButton','','添加商品'));
		},
		initDom:function() {
			this.$form = $("#detailOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_k3_manage_return_order); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form, function() {
				self.edit();
			});

			self.initCommonEvent(); 

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			}); 

			orderPrintRecord.init({
				referNo:self.state.no,
                referType:Enum.printReferType.num.returnOrder
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到退还单编号');
				return;
			}
			Rental.ajax.submit("{0}k3/queryReturnOrderByNo".format(SitePath.service),{returnOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载退还单详细信息",response.description || '失败');
				}
			}, null ,"加载退还单详细信息", 'listLoading');
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderReturnProductActionButtons(data);
			this.setCustomer(data.k3CustomerNo, data.k3CustomerName);
			this.renderOrderBaseInfo(data);
			this.initChooseProductList(data, this.filterAcitonButtons(this.rowAcitonButtons, data));
		},
		renderActionButton:function(returnOrder) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, returnOrder),
				no:self.state.no,
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderReturnProductActionButtons:function(returnOrder) {
			var self = this,
				actionButtonsTpl = $('#returnProductActionsButtonsTpl').html();
				actionButtons = self.filterAcitonButtons(self.productAcitonButtons, returnOrder);

			var data = {
				hasActionButtons:actionButtons.length > 0,
				acitonButtons:actionButtons,
			}
			
			Mustache.parse(actionButtonsTpl);
			$("#returnProductActionsButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#returnOrderNo").html(order.returnOrderNo);
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				customerDetailUrl:function() {
					return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.k3CustomerNo);
				},
				returnModeValue:function() {
					return Enum.returnMode.getValue(this.returnMode);
				},
				returnOrderStatusValue:function() {
					return Enum.k3ReturnOrderStatus.getValue(this.returnOrderStatus);
				},
				returnOrderStatusClass:function() {
					return Enum.k3ReturnOrderStatus.getClass(this.returnOrderStatus);
				},
				returnReasonTypeValue:function() {
					return Enum.returnReasonType.getValue(this.returnReasonType);
				}			
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
	};

	window.K3ReturnOrderDetail = _.extend(K3ReturnOrderDetail, K3ReturnOrderMixIn, K3ReturnOrderHandleMixin);

})(jQuery);