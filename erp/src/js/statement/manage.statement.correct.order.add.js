;(function($) {

	var AddCorrectOrder =  {
		init:function(prams) {
			this.props = _.extend({
				statementOrderId:null,
				statementOrderReferId:null,
				statementOrderItemId:null,
				callBack:function() {}
			}, prams);

			this.open();
		},
		open:function() {
			var self = this;
			InputModal.init({
				title:'创建冲正单',
				url:'correct-order/add',
				initFunc:function(modal) {},
				callBack:function(result) {
					self.add(result);
					// return true;
				}
			})
		},
		add:function(result) {
			var self = this;
			try {

				var commitData =  _.extend({
					statementOrderId: self.props.statementOrderId,
					statementOrderReferId: self.props.statementOrderReferId,
					statementOrderItemId: self.props.statementOrderItemId,
				}, result);
				
				var des = '创建冲正单';
				Rental.ajax.submit("{0}correct/create".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.props.callBack && self.props.callBack();
						Rental.modal.close();
					} else {
						Rental.notification.error(des, response.description || '失败');
						return false;
					}
				}, null, des, 'dialogLoading');

			} catch(e) {
				Rental.notification.error("创建冲正单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		}
	}

	window.AddCorrectOrder = AddCorrectOrder;

})(jQuery)