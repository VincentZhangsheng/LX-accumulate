/**
* 冲正单明细
*/
;(function($){

	var StatementCorrectOrderDetail = {
		state:{
			no:null,
			order:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statement_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_statement_correct_order_List];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_statement_correct_order_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function() {
			this.actionButtons = new Array();
			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var pay = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_pay];
			pay && this.actionButtons.push(AuthorUtil.button(pay,'payButton','','支付'));
		},
		initDom:function() {
			this.$form = $("#detailForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_statement_correct_order_List); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到冲正单编号');
				return;
			}
			Rental.ajax.ajaxData('correct/query', {statementCorrectNo:self.state.no}, '加载冲正单详细', function(response) {
				self.state.order = response.resultMap.data;
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderBaseInfo(data);
			this.renderOrderDetailInfo(data);
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, order),
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderBaseInfo:function(order) {
			$("#statementCorrectNo").html(order.statementCorrectNo);
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				orderDetailUrl:function() {
					if(this.statementOrderCorrectType == 1) {
						return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
					} 
					if(this.statementOrderCorrectType == 2) {
						return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, this.orderNo);
					}
				},
				customerUrl:function() {
					return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
				},
				statementOrderCorrectStatusValue:function() {
					return Enum.statementOrderCorrectStatus.getValue(this.statementOrderCorrectStatus);
				},
				statementOrderCorrectStatusClass:function() {
					return Enum.statementOrderCorrectStatus.getClass(this.statementOrderCorrectStatus);
				},
				hasStatementCorrectFailReason:function() {
					return !!this.statementCorrectFailReason;
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		renderOrderDetailInfo:function(order) {
			if(!order.statementOrderDetail) return;
			var data = _.extend(Rental.render, {
				order:order.statementOrderDetail,
				orderTypeValue:function() {
					return Enum.orderType.getValue(this.orderType);
				},
				orderItemTypeValue:function() {
					switch(this.orderItemType) {
						case 1:
							return "商品";
						case 2:
							return "配件";
					}
					return "";
				},
				itemRentTypeValue:function() {
					return Enum.rentType.getValue(this.itemRentType);
				},
				statementDetailTypeValue:function() {
					return Enum.statementDetailType.getValue(this.statementDetailType);
				},
				statementDetailStatusValue:function() {
					return Enum.statementDetailStatus.getValue(this.statementDetailStatus);
				}
			});
			var tpl = $('#orderDetailInfoTpl');
			Mustache.parse(tpl);
			$('#orderDetailInfo').html(Mustache.render(tpl, data)).removeClass('hide');
		}
	};

	window.StatementCorrectOrderDetail = _.extend(StatementCorrectOrderDetail, StatementCorrectOrderHanleMixin);

})(jQuery);