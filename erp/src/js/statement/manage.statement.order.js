/**
* 结算单
**/
;(function($) {
	var StatementOrderManage = {
		state:{
			statementOrderCustomerNo:null,
			customerName:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statement_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_statement_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();
			this.exportButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_statement_order_detail],
				pay = this.currentPageAuthor.children[AuthorCode.manage_statement_order_pay],
				exportButton = this.currentPageAuthor.children[AuthorCode.manage_statement_order_list_export];

			exportButton && this.commonActionButtons.push(AuthorUtil.button(_.extend(exportButton, {menuUrl: 'exportExcel/exportPageStatementOrder'}),'exportButton','','财务导出'));
			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));	
			pay && this.rowActionButtons.push(AuthorUtil.button(pay,'payButton','','支付'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.statementOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			var statementOrderCustomerNo = Rental.helper.getUrlPara('customerNo');
			var customerName = Rental.helper.getUrlPara('customerName')
			this.state.statementOrderCustomerNo = statementOrderCustomerNo ? statementOrderCustomerNo : "";
			this.state.customerName = customerName ? decodeURI(customerName) : "";

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor, this.currentPageAuthor]); //面包屑
			self.renderCustomerName();
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});
			
			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#statementExpectPayStartTime"), $("#statementExpectPayEndTime"));

			self.renderCommonActionButton();
			
			Rental.ui.renderSelect({
				container:$('#statementOrderStatus'),
				data:Enum.array(Enum.statementOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（结算单状态）'
			});

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("#subCompanyId"),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

			self.renderParams();
			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$actionCommonButtons.on("click", '.exportButton', function(event) {
				event.preventDefault();
				self.exportList.modal();
			});

			self.$dataListTable.on('click', '.payButton', function(event) {
				event.preventDefault();
				var statementOrderNo = $(this).data('no'),
					statementPaidAmount = $(this).data('statementpaidamount'),
					statementamount = $(this).data('statementamount'),
					customerName = $(this).data('customername'),
					customerNo = $(this).data('customerno'),
					balanceAmount = "";

				Rental.ajax.submit("{0}customer/detailCustomer".format(SitePath.service),{customerNo:customerNo},function(response){
					if(response.success) {
						balanceAmount = response.resultMap.data.customerAccount.hasOwnProperty("balanceAmount") ? response.resultMap.data.customerAccount.balanceAmount : 0;
						self.payConfirm(statementOrderNo, customerName, statementamount, statementPaidAmount,balanceAmount);
					} else {
						Rental.notification.error("查询客户详情",response.description || '失败');
					}
				}, null ,"查询客户详情",'dialogLoading');
			});
		},
		renderParams:function() {
			var statementOrderStatus = Rental.helper.getUrlPara('statementOrderStatus')
			var isWorkbench = Rental.helper.getUrlPara('isWorkbench')
			if(!!statementOrderStatus) {
				Rental.ui.renderFormData(this.$searchForm, {statementOrderStatus:statementOrderStatus});
			}
			if(!!isWorkbench) {
				Rental.ui.renderFormData(this.$searchForm, {isWorkbench:isWorkbench});
			}
		},
		renderCustomerName:function() {
			if(!!this.state.customerName) {
				$('[name="statementOrderCustomerName"]',this.$searchForm).val(this.state.customerName).attr("readonly","readonly");
			}
		},
		payConfirm:function(statementOrderNo, customerName, statementamount, statementPaidAmount,balanceAmount) {
			var self = this;
			var remindMsg = '<div class="row mn">'
							+ '<div class="pull-left">' 
							+ '<p>客户名称：<span>' + customerName + '<span></p>'
							+ '<p>账户余额：￥<span class="text-success fs16">'+ (parseFloat(balanceAmount)).toFixed(2) + '<span></p>'
							+ '<p>支付金额：￥<span class="text-danger fs16">'+ (parseFloat(statementamount)-parseFloat(statementPaidAmount)).toFixed(2) + '<span></p>'
							+ '</div>'
							+ '</div>'

			bootbox.confirm({
				title: "<span class='fs20'>确认支付?</span>",
				message: remindMsg,
				buttons: {
					cancel: {
						label: '取消'
					},
					confirm: {
						label: '确认'
					}
				},
				callback: function (result) {
					result && self.pay(statementOrderNo, customerName, statementamount, statementPaidAmount,balanceAmount);
				}
			});
		},
		paySuccess:function(customerName, statementamount, statementPaidAmount,balanceAmount) {
			var self = this;
			var successMsg = '<div class="row mn">'
							+ '<div class="pull-left">' 
							+ '<p>客户名称：<span>' + customerName + '<span></p>'
							+ '<p>账户原余额：￥<span class="fs16">'+ (parseFloat(balanceAmount)).toFixed(2) + '<span></p>'
							+ '<p>已支付金额：￥<span class="text-danger fs16">'+ (parseFloat(statementamount)-parseFloat(statementPaidAmount)).toFixed(2) + '<span></p>'
							+ '<hr class="mn">'
							+ '<p style="margin-top:9.5px;">账户结余：￥<span class="text-success fs16">'+ (parseFloat(balanceAmount) - parseFloat(statementamount) + parseFloat(statementPaidAmount)).toFixed(2) + '<span></p>'
							+ '</div>'
							+ '</div>'

			bootbox.dialog({
				message: successMsg,
				title: "<span class='fs20'>支付成功</span>",
				buttons: {
					confirm: {
						label: '确认',
						className: 'btn-primary',
						callback: function () {
							self.doSearch(self.Pager.pagerData.currentPage);
						}
					}
				},
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		initDatePicker:function() {
			this.$createTimePicker.daterangepicker({
				"autoApply": true,
				format:'YYYY-MM-DD',
			}, function(start, end, label) {
				OrderManage.setSearchDate(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
			});
		},
		setSearchDate:function(start, end) {
			this.$createStartTime.val(new Date(start + " 00:00:00").getTime());
			this.$createEndTime.val(new Date(end + " 23:59:59").getTime());
			this.$createTimePickerInput.val("{0} - {1}".format(start,end));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, {
				statementOrderCustomerNo:self.state.statementOrderCustomerNo
			}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.statementOrder, searchData);
			
			Rental.ajax.submit("{0}statementOrder/page".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询结算单列表",response.description || '失败');
				}
			}, null ,"查询结算单列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.statementOrderDetail, this.statementOrderNo);
					},
					customerUrl:function() {
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
					},
					dataLimit:function() {
						return self.dateDiff(this.statementStartTime, this.statementEndTime)
					},
					rowActionButtons:function() {
						var _self = this, rowActionButtons = new Array();
						(self.rowActionButtons || []).forEach(function(button, index) {
							rowActionButtons.push(button);
							if((button.class == 'payButton') && (_self.statementStatus == Enum.statementOrderStatus.num.completed || _self.statementStatus == Enum.statementOrderStatus.num.noSttlement)) {
								rowActionButtons.splice(rowActionButtons.length - 1,1);
							}
						});
						return {
							buttons:rowActionButtons
						}
					},
					statementOrderStatusValue:function() {
						return Enum.statementOrderStatus.getValue(this.statementStatus);
					},
					statementOrderStatusClass:function() {
						return Enum.statementOrderStatus.getClass(this.statementStatus);
					}
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		pay:function(statementOrderNo, customerName, statementamount, statementPaidAmount,balanceAmount) {
			var self = this, des = '支付';
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'statementOrder/pay'), {statementOrderNo:statementOrderNo}, function(response){
				if(response.success) {
					self.paySuccess(customerName, statementamount, statementPaidAmount,balanceAmount)
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		dateDiff:function(startTime, endTime) {
			if(!startTime || !endTime) return;
			var startTime = new Date(startTime), endTime = new Date(endTime),
				year1 = startTime.getFullYear(),
				mon1 = startTime.getMonth(),
				day1 = startTime.getDate(),
				year2 = endTime.getFullYear(),
				mon2 = endTime.getMonth(),
				day2 = endTime.getDate(),
				diffYear = year2 - year1,
				diffMon = mon2 - mon1,
				diffDay = day2 - day1 + 1,
				md1 = [31,28,31,30,31,30,31,31,30,31,30,31],
				md2 = md1.slice();
			md2[1] += 1;

			var isLeap = (year2 % 4 == 0 && (year2 % 100 != 0 )) || (year2 % 400 == 0);
			var str = "";
			while (diffDay < 0) {
				mon2--;
				diffDay += isLeap ? md2[mon2] : md1[mon2];
				diffMon--;
			}
			while ((isLeap && diffDay == md2[mon2]) || (!isLeap && diffDay == md1[mon2])) {
				diffDay = 0;
				diffMon ++;
			}
			while (diffMon < 0) {
				diffMon += 12;
				diffYear--;
			}
			str = (diffYear > 0 ? diffYear + "年" : "") + (diffMon > 0 ? diffMon + "个月" : "") + (diffDay > 0 ? diffDay + "天" : "")
			return str;
		},
		exportList:{
			modal:function(prams) {
				this.props = _.extend({
					callBack:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "statement-order/list-export", type:'ajax', ajaxContentAdded:function(pModal) {
					self.initDom(pModal);
					self.initEvent();	
				}});
			},
			initDom:function(modal) {
				this.$exportModal = modal.container;
				this.$exportForm = $("#exportForm");
			},
			initEvent:function() {
				var self = this;
				Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#startTime"), $("#endTime"));

				self.$exportForm.on('click','.cancelButton',function(event) {
					event.preventDefault();
					Rental.modal.close();
				})

				self.$exportForm.on('click','.exportBtn',function(event) {
					event.preventDefault();
					self.exportDo();
				})

				var actionUrl = SitePath.base + "exportExcel/exportPageStatementOrder"
				self.$exportForm.prop({action:actionUrl});
			},
			exportDo:function() {
				var timeData = $('#createTimePickerInput').val();
				if(!!timeData) {
					dayList = timeData.split(" ");
					$('#exportForm [name="createStartTime"]').val(dayList[0])
					$('#exportForm [name="createEndTime"]').val(dayList[2])
				}
				$("#exportForm").submit();
				Rental.modal.close();
			},
		}
	};

	window.StatementOrderManage =  StatementOrderManage;

})(jQuery);