/**
* 结算单
**/
;(function($) {

	var StatementCorrectOrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statement_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_statement_correct_order_List];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_statement_correct_order_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_statement_correct_order_submit],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_statement_correct_order_cancel];

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));	
			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.statementCorrectOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor, this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});
			
			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));
			
			self.searchData();  //初始化列表数据

			self.renderCommonActionButton();
			
			Rental.ui.renderSelect({
				container:$('#statementOrderCorrectStatus'),
				data:Enum.array(Enum.statementOrderCorrectStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（冲正单状态）'
			});

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("#subCompanyId"),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});


			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
			
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.statementCorrectOrder, searchData);

			Rental.ajax.ajaxData('correct/page', searchData, '加载冲正单', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.statementCorrectOrderDetail, this.statementCorrectNo);
					},
					customerUrl:function() {
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
					},
					orderDetailUrl:function() {
						if(this.statementOrderCorrectType == 1) {
							return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
						} 
						if(this.statementOrderCorrectType == 2) {
							return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, this.orderNo);
						}
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
						return rowActionButtons;
					},
					statementOrderCorrectStatusValue:function() {
						return Enum.statementOrderCorrectStatus.getValue(this.statementOrderCorrectStatus);
					},
					statementOrderCorrectStatusClass:function() {
						return Enum.statementOrderCorrectStatus.getClass(this.statementOrderCorrectStatus);
					}
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.StatementCorrectOrderManage =  _.extend(StatementCorrectOrderManage, StatementCorrectOrderHanleMixin);

})(jQuery);