/*
* 结算单管理/对账单详情
* author: zhangsheng
* time: 2018-02-06
*/
;(function($) {
    var statementMonthlyDetail = {
        state:{
            no:null,
            month:null,
            order:null
        },
        init:function() {
            this.initAuthor();
            this.initDom();
            this.initEvent();
        },
        initAuthor:function() {
            this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statement_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_monthly_statement_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_monthly_statement_order_detail];
			this.initActionButtons();
        },
        initActionButtons:function() {
            this.actionButtons = new Array();

            if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;
        },
        initDom:function() {
            this.$form = $("#statementOrderDetailForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

            this.state.no = Rental.helper.getUrlPara('no');
            this.state.month = Rental.helper.getUrlPara('month');
        },
        initEvent:function() {
            var self = this;

            Layout.chooseSidebarMenu(AuthorCode.manage_monthly_statement_order_list); //激活选中menu菜单
            Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑
            
            self.loadData();
            // self.renderDetailList(new Object());
        },
        loadData:function() {
            var self = this;
            if(!self.state.no) {
                bootbox.alert('没找到客户编号');
                return;
            }
            Rental.ajax.ajaxData('statementOrder/queryStatementOrderMonthDetail', {statementOrderCustomerNo:self.state.no,monthTime:self.state.month}, '加载对账单详细', function(response) {
                self.state.order = response.resultMap.data;
                self.initData(response.resultMap.data);
            })
        },
        initData:function(data) {
            this.renderActionButton(data);
            this.renderBaseInfo(data);
            this.renderDetailList(data);
        },
        renderActionButton:function(order) {
            var self = this;
            var actionButtonsTpl = $('#actionButtonsTpl').html();
            var data = {
                actionButtons:self.actionButtons,
                no:self.state.no
            }
            Mustache.parse(actionButtonsTpl);
            $("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
        },
        renderBaseInfo:function(order) {
            this.renderHeaderInfo(order, $('#headerInfoTpl').html(), $('#headerInfo'))
            this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
        },
        renderHeaderInfo:function(order, tpl, container) {
            var data = _.extend(Rental.render, {
                "customerName":order.customerName,
                "customerNo":order.customerNo,
                "monthTime":order.monthTime,
                "customerDetailUrl":function() {
                    return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, order.customerNo);
                }
            });
            Mustache.parse(tpl);
            container.html(Mustache.render(tpl, data));
        },
        renderOrderInfo:function(order, tpl, container) {
            var data = _.extend(Rental.render, {
                order:order,
                statementOrderStatusValue:function() {
                    return Enum.statementOrderStatus.getValue(this.statementStatus);
                }
            });
            Mustache.parse(tpl);
            container.html(Mustache.render(tpl, data));
        },
        renderDetailList:function(order) {
            var self = this;
            var listData = order.hasOwnProperty("statementOrderDetailList") ? order.statementOrderDetailList : [];
            var data = {
                dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    orderDetailUrl:function() {
                        return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
                    },
                    statementDetailStatusValue:function() {
                        return Enum.statementOrderStatus.getValue(this.statementDetailStatus)
                    },
                    orderTypeValue:function() {
                        return Enum.orderType.getValue(this.orderType);
                    },
                    orderItemTypeValue:function() {
                        switch(this.orderItemType) {
                            case 1:
                                return "商品";
                            case 2:
                                return "物料";
                            default:
                                return "";
                        }
                    },
                    itemRentTypeValue:function() {
                        return Enum.rentType.getValue(this.itemRentType);
                    },
                    statementDetailTypeValue:function() {
                        return Enum.statementDetailType.getValue(this.statementDetailType);
                    }
                })
            }
            Mustache.parse(this.$dataListTpl);
            this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        }
    }
    window.statementMonthlyDetail = statementMonthlyDetail;
})(jQuery);