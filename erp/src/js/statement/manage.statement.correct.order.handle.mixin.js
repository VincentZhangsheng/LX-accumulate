;(function($) {

	var StatementCorrectOrderHanleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			self.$materialDataListTable && self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.correctOrder
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				self.cancel($(this).data('no'), handleCallBack);
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交冲正单', submitUrl = 'correct/commit';
			if(isAudit) {
				self.orderDo(submitUrl, {
					statementCorrectNo:prams.no,
					verifyUserId:prams.verifyUserId,
					remark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
				return;
			} else {
				bootbox.confirm('确认'+des+'？', function(result) {
					result && self.orderDo(submitUrl, {
						statementCorrectNo:prams.no
					}, des, callBack);
				});
			}
		},
		cancel:function(no, callBack) {
			var self = this;
			bootbox.confirm('确认取消冲正单？', function(result) {
				result && self.orderDo('correct/cancel', {statementCorrectNo:no}, '取消冲正单', callBack);
			});
		},
		orderDo:function(url, prams, des, callBack) {
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				button.no = order.statementCorrectNo; 
				rowActionButtons.push(button);
				if(_.indexOf(['submitButton', 'cancelButton'], button.class) > -1 && order.statementOrderCorrectStatus != Enum.statementOrderCorrectStatus.num.unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		}
	}

	window.StatementCorrectOrderHanleMixin = StatementCorrectOrderHanleMixin;

})(jQuery);