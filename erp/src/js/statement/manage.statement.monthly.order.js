/*
* 结算单管理/对账单模块
* author: zhangsheng
* time: 2018-02-06
*/
;(function($) {
    var statementMonthlyOrder = {
        state:{
            statementOrderCustomerNo:null,
            customerName:null
		},
        init:function() {
            this.initAuthor();
            this.initDom();
            this.initEvent();
        },
        initAuthor:function(){
            this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statement_order];
            this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_monthly_statement_order_list];
            this.initActionButtons();
        },
        initActionButtons:function() {
            this.commonActionButtons = new Array();
            this.rowActionButtons = new Array();

            if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

            var view = this.currentPageAuthor.children[AuthorCode.manage_monthly_statement_order_detail];
            view && this.rowActionButtons.push(AuthorUtil.button(view, 'viewButton', '', '查看'));
        },
        initDom:function() {
            this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
            this.$actionCommonButtons = $("#actionCommonButtons");

            this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.statementMonthlyOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});
            
            var statementOrderCustomerNo = Rental.helper.getUrlPara('customerNo');
            var customerName = Rental.helper.getUrlPara('customerName')
            this.state.statementOrderCustomerNo = statementOrderCustomerNo ? statementOrderCustomerNo : "";
            this.state.customerName = customerName ? decodeURI(customerName) : "";
            
            this.Pager = new Pager();
        },
        initEvent:function() {
            var self = this;

            self.render(new Object());

            Breadcrumb.init([this.currentManageAuthor, this.currentPageAuthor]); //面包屑
            self.renderCustomerName();

            Rental.ui.events.initMonthpicker($("#monthTime"));
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
            });
            
            self.searchData();
            self.renderCommonActionButton();

            ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("#subCompanyId"),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

            //绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
        },
        renderCommonActionButton:function() {
            var self = this;
            var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
            Mustache.parse(actionCommonButtonsTpl);
            self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'actionCommonButtons':self.commonActionButtons}));
        },
        renderCustomerName:function() {
			if(!!this.state.customerName) {
				$('[name="statementOrderCustomerName"]',this.$searchForm).val(this.state.customerName).attr("readonly","readonly");
			}
		},
        searchData:function(prams) {
            var self = this;
            var searchData = $.extend({
                pageNo:1,
                pageSize:self.Pager.defautPrams.pageSize,
                statementOrderCustomerName:'',
            }, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, {
                statementOrderCustomerNo:self.state.statementOrderCustomerNo
            }, prams || {});

            Rental.searchStorage.set(Rental.searchStorage.enum.statementMonthlyOrder, searchData);

            Rental.ajax.submit("{0}statementOrder/queryStatementOrderCheckParam".format(SitePath.service),searchData,function(response) {
                if(response.success) {
                    self.render(response.resultMap.data);
                } else {
                    Rental.notification.error("查询对账单列表",response.description || '失败');
                }
            }, null, "查询对账单列表", "listLoading");
        },
        doSearch:function(pageNo) {
            this.searchData({pageNo:pageNo || 1});
        },
        render:function(data) {
            var self = this;
            self.renderList(data);
            self.Pager.init(data,function(pageNo) {
                self.doSearch(pageNo);
            })
        },
        renderList:function(data) {
            var self = this;

            var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
                hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
                hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;
            
            var data = {
                hasCommonActionButtons:hasCommonActionButtons,
                hasRowActionButtons:hasRowActionButtons,
                dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    "rowActionButtons":self.rowActionButtons,
                    customerDetailUrl:function() {
                        return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
                    }
                })
            }
            Mustache.parse(this.$dataListTpl);
            self.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        }
    }
    window.statementMonthlyOrder = statementMonthlyOrder;
})(jQuery);