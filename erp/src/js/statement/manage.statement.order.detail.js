/**
* 结算单明细
*/
;(function($){

	var StatementOrderDetail = {
		state:{
			no:null,
			order:null,
			statementOrderDetailId: null,
			customerNo: null,
			customerName:null,
			statementamount:null,
			statementPaidAmount:null,
			balanceAmount:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_statement_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_statement_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function() {
			this.actionButtons = new Array();
			this.rowActionButtons = new Array();
			this.exportButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var pay = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_pay],
				createCorrectOrder = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_create_correct_order],
				useCoupon = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_user_coupon],
				payOrderItem = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_pary_orderItem],
				exportOrder = this.currentManageListAuthor.children[AuthorCode.manage_statement_order_export];
			
			pay && this.actionButtons.push(AuthorUtil.button(pay,'payButton','','支付'));

			exportOrder && this.exportButtons.push(AuthorUtil.button(exportOrder,'exportButton','','导出'))

			payOrderItem && this.rowActionButtons.push(AuthorUtil.button(payOrderItem,'orderPayButton','','支付'));
			createCorrectOrder && this.rowActionButtons.push(AuthorUtil.button(createCorrectOrder,'createCorrectOrderButton','','冲正'));
			useCoupon && this.rowActionButtons.push(AuthorUtil.button(useCoupon,'useCouponButton','','使用优惠券'));
		},
		initDom:function() {
			this.$form = $("#statementOrderDetailForm");
			this.$exportButton = $("#exportButton");
			this.$exportForm = $("#exportForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_statement_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.renderStatementOrderDetailList(new Object());
			self.renderExportButton();

			this.$form.on('click', '.payButton', function(event) {
				event.preventDefault();
				if(!self.state.order) {
					bootbox.alert('还没有加载订单信息')
					return;
				}
				var statementOrderNo = $(this).data('no');

				Rental.ajax.ajaxDataNoLoading('customer/detailCustomer', {customerNo:self.state.customerNo}, '查询客户详情', function(response) {
					self.state.balanceAmount = response.resultMap.data.customerAccount.hasOwnProperty("balanceAmount") ? response.resultMap.data.customerAccount.balanceAmount : 0;
					self.payConfirm(statementOrderNo);	
				});

				// bootbox.confirm({
				//     title: "确认支付?",
				//     message: "支付金额：<span class='text-danger'>￥"+ (parseFloat(self.state.order.statementAmount)-parseFloat(self.state.order.statementPaidAmount)).toFixed(2) +"<span>",
				//     buttons: {
				//         cancel: {
				//             label: '取消'
				//         },
				//         confirm: {
				//             label: '确认'
				//         }
				//     },
				//     callback: function (result) {
				//         result && self.pay({
				// 			statementOrderNo:statementOrderNo
				// 		});
				//     }
				// });
			});

			this.$dataListTable.on('click', '.orderPayButton', function(event) {
				event.preventDefault();
				var mergeStatementItemIdList = $(this).data("mergelist");
				self.orderItemPay(mergeStatementItemIdList,function(){
					self.loadData();
				})
			});

			this.$dataListTable.on('click', '.createCorrectOrderButton', function(event) {
				event.preventDefault();
				AddCorrectOrder.init({
					statementOrderId:$(this).data('statementorderid'),
					statementOrderReferId:$(this).data('orderid'),
					statementOrderItemId:$(this).data('orderitemreferid'),
					callBack:function() {
						self.loadData();
					}
				})
			});

			// 使用优惠券
			this.$dataListTable.on('click', '.useCouponButton', function(event) {
				event.preventDefault();
				self.state.statementOrderDetailId = $(this).attr('data-statementOrderDetailId');
				UseCouponManage.init({
					statementOrderDetailId: self.state.statementOrderDetailId,
					customerNo: self.state.customerNo,
					callBack: function() {
						self.loadData();
					}
				})
			})
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到结算单编号');
				return;
			}
			Rental.ajax.ajaxData('statementOrder/detail', {statementOrderNo:self.state.no}, '加载结算单详细', function(response) {
				self.state.order = response.resultMap.data;
				self.initData(response.resultMap.data);
				self.state.customerNo = response.resultMap.data.customerNo;
				self.state.customerName = response.resultMap.data.customerName;
				self.state.statementamount = response.resultMap.data.statementAmount;
				self.state.statementPaidAmount = response.resultMap.data.statementPaidAmount;
			});
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderBaseInfo(data);
			this.renderStatementOrderDetailList(data);
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.actionButtons,
				no:self.state.no,
				showButton:function() {
					var isShow = true;
					if((this.class == 'payButton') && order.statementStatus == Enum.statementOrderStatus.num.completed) {
						isShow = false;
					}
					if((this.class == 'useCouponButton') && order.statementStatus == Enum.statementOrderStatus.num.completed) {
						isShow = false;
					}
					return isShow;
				}
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderExportButton:function() {
			var self = this;
			var exportButtonsTpl = $('#exportButtonsTpl').html();
			var data ={
				'exportButtons':self.exportButtons,
				statementOrderNo:function() {
					return self.state.no;
				}
			}
			Mustache.parse(exportButtonsTpl);
			self.$exportButton.html(Mustache.render(exportButtonsTpl,data));
		},
		renderBaseInfo:function(order) {
			$("#statementOrderNo").html(order.statementOrderNo);
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				customerUrl:function() {
					return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
				},
				statementOrderStatusValue:function() {
					return Enum.statementOrderStatus.getValue(this.statementStatus);
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		renderStatementOrderDetailList:function(order) {
			var self = this;

			var listData = order.hasOwnProperty("statementOrderDetailList") ? order.statementOrderDetailList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;

			var data = {
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					orderDetailUrl:function() {
						return Enum.orderType.getUrl(this.orderType, this.orderNo);
						// return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
					},
					rowActionButtons:function() {
						var mergeListStr = JSON.stringify(this.mergeStatementItemIdList)
						var statementDetailStatus = this.statementDetailStatus
						var _self = this, rowActionButtons = new Array();
						(self.rowActionButtons || []).forEach(function(button, index) {
							button.no = order.statementOrderNo;
							button.mergeListStr = mergeListStr;
							rowActionButtons.push(button);
							if((button.class == 'createCorrectOrderButton') 
								&& !((order.statementStatus == Enum.statementOrderStatus.num.init || order.statementStatus == Enum.statementOrderStatus.num.part) 
								&& (_self.statementDetailStatus == Enum.statementOrderStatus.num.init || _self.statementDetailStatus == Enum.statementOrderStatus.num.part))) {
								rowActionButtons.splice(rowActionButtons.length - 1,1);
							}
							if(button.class === 'useCouponButton' && (order.statementStatus === Enum.statementOrderStatus.num.completed || 
								order.statementStatus === Enum.statementOrderStatus.num.noSttlement)) {
									rowActionButtons.splice(rowActionButtons.length - 1,1);
							}
							if(button.class == 'orderPayButton' && (statementDetailStatus == Enum.statementOrderStatus.num.completed || 
								statementDetailStatus == Enum.statementOrderStatus.num.noSttlement)) {
									rowActionButtons.splice(rowActionButtons.length - 1,1);
							}
						});
						return rowActionButtons;
					},
					statementDetailStatusValue:function() {
						return Enum.statementOrderStatus.getValue(this.statementDetailStatus);
					},
					statementDetailStatusClass:function() {
						return Enum.statementOrderStatus.getClass(this.statementDetailStatus);
					},
					orderTypeValue:function() {
						return Enum.orderType.getValue(this.orderType);
					},
					orderItemTypeValue:function() {
						switch(this.orderItemType) {
							case 1:
								return "商品";
							case 2:
								return "配件";
							case 3:
								return "商品";
							case 4:
								return "配件";
							case 5:
								return "商品";
							case 6:
								return "配件";
							case 7:
								return "其它费用";
							case 8:
								return "其它费用";
							case 9:
								return "其它费用";
							default:
								return "";
						}
					},
					itemRentTypeValue:function() {
						return Enum.rentType.getValue(this.itemRentType);
					},
					statementDetailTypeValue:function() {
						return Enum.statementDetailType.getValue(this.statementDetailType);
					}
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		payConfirm:function(statementOrderNo) {
			var self = this;
			var remindMsg = '<div class="row mn">'
							+ '<div class="pull-left">' 
							+ '<p>客户名称：<span>' + self.state.customerName + '<span></p>'
							+ '<p>账户余额：￥<span class="text-success fs16">'+ (parseFloat(self.state.balanceAmount)).toFixed(2) + '<span></p>'
							+ '<p>支付金额：￥<span class="text-danger fs16">'+ (parseFloat(self.state.statementamount)-parseFloat(self.state.statementPaidAmount)).toFixed(2) + '<span></p>'
							+ '</div>'
							+ '</div>'

			bootbox.confirm({
				title: "<span class='fs20'>确认支付?</span>",
				message: remindMsg,
				buttons: {
					cancel: {
						label: '取消'
					},
					confirm: {
						label: '确认'
					}
				},
				callback: function (result) {
					result && self.pay({statementOrderNo:statementOrderNo});
				}
			});
		},
		paySuccess:function() {
			var self = this;
			var successMsg = '<div class="row mn">'
							+ '<div class="pull-left">' 
							+ '<p>客户名称：<span>' + self.state.customerName + '<span></p>'
							+ '<p>账户原余额：￥<span class="fs16">'+ (parseFloat(self.state.balanceAmount)).toFixed(2) + '<span></p>'
							+ '<p>已支付金额：￥<span class="text-danger fs16">'+ (parseFloat(self.state.statementamount)-parseFloat(self.state.statementPaidAmount)).toFixed(2) + '<span></p>'
							+ '<hr class="mn">'
							+ '<p style="margin-top:9.5px;">账户结余：￥<span class="text-success fs16">'+ (parseFloat(self.state.balanceAmount) - parseFloat(self.state.statementamount) + parseFloat(self.state.statementPaidAmount)).toFixed(2) + '<span></p>'
							+ '</div>'
							+ '</div>'

			bootbox.dialog({
				message: successMsg,
				title: "<span class='fs20'>支付成功</span>",
				buttons: {
					confirm: {
						label: '确认',
						className: 'btn-primary',
						callback: function () {
							self.loadData();
						}
					}
				},
			});
		},
		pay:function(prams) {
			var self = this, des = '支付';
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'statementOrder/pay'), prams, function(response){
				if(response.success) {
					// Rental.notification.success(des,response.description || '成功');
					self.paySuccess();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		orderItemPay:function(mergeStatementItemList,callBack) {
			var self = this;
			bootbox.confirm('确认支付？', function(result) {
				result && self.orderPayDo(mergeStatementItemList, callBack);
			});
		},
		orderPayDo:function(mergeStatementItemList,callBack) {
			if(!mergeStatementItemList) {
				bootbox.alert('找不到订单详情id链表');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}statementOrder/payStatementOrderDetail".format(SitePath.service),{mergeStatementItemList:mergeStatementItemList},function(response){
				if(response.success) {
					Rental.notification.success("结算单项支付",response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error("结算单项支付",response.description || '失败');
				}
			}, null ,"结算单项支付",'dialogLoading');
		}
	};

	window.StatementOrderDetail = StatementOrderDetail;

})(jQuery);