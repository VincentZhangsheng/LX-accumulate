/**
 * author:liaojianming
 * date: 2018-4-24
 * decription: 结算单使用优惠券
 */
;(function($) {

	var UseCouponManage =  {
		init: function(prams) {
			this.props = _.extend({
				callBack:function() {},
				statementOrderDetailId: null,
				customerNo: null
			}, prams || {});

			this.showModal();
		},
		showModal: function(){
			var self = this;
			Rental.modal.open({
				src: SitePath.base + "statement-order/useCoupon", 
				type: 'ajax', 
				closeOnBgClick: false, 
				ajaxContentAdded: function() {
					self.initDom();
					self.initEvent();
				}	
			});
		},
		initDom: function() {
			var self = this;
			this.$dataListTpl = $('#dataListTpl').html();
			this.$dataListTable = $("#dataListTable");
		},
		initEvent: function() {
			var self = this;
			
			self.getCouponList();

			self.$dataListTable.on('click', '.useCouponButton', function(event) {
				event.preventDefault();
				var couponId = $(this).attr('data-couponId');
				self.useCouponBatch(couponId);
			})
		},
		getCouponList: function() {
			try {
				var self = this, 
					des = "加载优惠券列表";
				var customer = {
					customerNo: self.props.customerNo
				}

				Rental.ajax.ajaxDataNoLoading('coupon/findStatementCouponByCustomerNo', customer, des, function(response) {
					self.render(response.resultMap.data);
				});

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		useCouponBatch: function(couponId) {
			var des = "使用结算单优惠券",
				self = this,
				StatementCouponParam = {
					coupon: {
						couponId: couponId
					},
					statementOrderDetail: {
						statementOrderDetailId: self.props.statementOrderDetailId
					}
				}
			bootbox.confirm(des, function(result) {
				if(result) {
					try {
						Rental.ajax.ajaxData('coupon/useStatementCoupon', StatementCouponParam, des, function(response) {
							if(response.success) {
								Rental.notification.success(des,response.description || '成功');
								self.callBackFunc(response);
							} else {
								Rental.notification.error(des,response.description || '失败');
							}
						})
					} catch(e) {
						Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
					}
				}
			})
		},
		render: function(data) {
			var self = this;
			self.renderList(data);
		},
		renderList: function(data) {
			var self = this;
			var listData = data,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					"isOnLineValue": function() {
						return Enum.isOnline.getValue(this.isOnline);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack && self.props.callBack();
			}
			return false;
		}
	}

	window.UseCouponManage = UseCouponManage;

})(jQuery)