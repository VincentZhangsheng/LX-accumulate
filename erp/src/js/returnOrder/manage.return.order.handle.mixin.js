;(function($) {

	var ReturnOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			self.$materialDataListTable && self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('returnorderno'),
					workflowType:Enum.workflowType.num.returnOrder,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var returnorderno = $(this).data('returnorderno');
				self.cancel(returnorderno, handleCallBack);
			});

			container.on('click', '.completeButton', function(event) {
				event.preventDefault();
				var returnorderno = $(this).data('returnorderno');
				self.complete(returnorderno, handleCallBack);
			});

			container.on('click', '.returnEquipmentButton', function(event) {
				event.preventDefault();
				var returnorderno = $(this).data('returnorderno');
				self.returnEquipment(returnorderno, handleCallBack);
			});

			container.on('click', '.returnMaterialRowButton', function(event) {
				event.preventDefault();
				var returnOrderNo = $(this).data('returnorderno'), 
					materialno = $(this).data('materialno');
				self.rowReturnMaterial({
					returnOrderNo:$(this).data('returnorderno'), 
					materialNo:$(this).data('materialno'),
					returnCount:$(this).data('returncount'),
				}, handleCallBack);
			});

			container.on('click', '.viewReturnEquipment', function(event) {
				event.preventDefault();
				var $this = $(this);
				$this.closest('tr').next('.affixTr').find('.affixPanel').slideToggle('fast');
				self.getReturnEquipment($this)
			});

			container.on('click', '.viewReturnMaterial', function(event) {
				event.preventDefault();
				var $this = $(this);
				$this.closest('tr').next('.affixTr').find('.affixPanel').slideToggle('fast');
				self.getReturnMaterial($this)
			});

			container.on('click', '.hideAffixPanel', function(event) {
				event.preventDefault();
				$(this).closest('.affixPanel').slideUp('fast');
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交退货单', submitUrl = 'returnOrder/commit';
			if(!isAudit) {
				bootbox.confirm('确认'+des+'？', function(result) {
					result && self.orderDo(submitUrl, {
						returnOrderNo:prams.no,
					}, des, callBack);
				});
				return;
			} else {
				self.orderDo(submitUrl, {
					returnOrderNo:prams.no,
					verifyUserId:prams.verifyUserId,
					commitRemark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		returnEquipment:function(returnOrderNo, callBack) {
			var self = this;
			OrderPicking.init({
				title:'退还设备',
				callBack:function(res) {
					self.orderDo('returnOrder/doReturnEquipment', {
						returnOrderNo:returnOrderNo,
						equipmentNo:res.equipmentNo,
					}, '退还设备', callBack);
					return true;
				}
			});
		},
		rowReturnMaterial:function(prams, callBack) {
			this.returnMaterialSubmit(_.extend(prams, {
				title:'退还配件',
			}), callBack);
		},
		returnMaterialSubmit:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:prams.title,
				url:'input-return-material-info-modal/input',
				initFunc:function(modal) {
					$('[name=returnCount]', modal).val(prams.returnCount);
				},
				callBack:function(result) {
					self.orderDo('returnOrder/doReturnMaterial', {
						returnOrderNo:prams.returnOrderNo,
						materialNo:prams.materialNo,
						returnCount:result.returnCount,
					}, prams.title, callBack);
					return true;
				}
			});
		},
		orderDo:function(url, prams, des, callBack) {
			if(!prams.returnOrderNo) {
				bootbox.alert('找不到退货单编号');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		cancel:function(returnOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认取消该订单？', function(result) {
				result && self.orderDo('returnOrder/cancel', {returnOrderNo:returnOrderNo}, '取消退还单', callBack);
			});
		},
		complete:function(returnOrderNo, callBack) {	
			var self = this;
			InputReturnOrderEndInfo.init({
				callBack:function(info) {
					self.orderDo('returnOrder/end', _.extend(info, {returnOrderNo:returnOrderNo}), '结束退还单', callBack);
					return true;
				}
			})
		},
		getReturnEquipment:function($button) {
			var returnOrderProductId = $button.data('returnorderproductid'),
				container = $button.closest('tr').next('.affixTr').find('.table');
			var self = this; 
			Rental.ajax.ajaxData('returnOrder/pageReturnEquipment', {
				pageNo:1,
				pageSize:10000,
				returnOrderProductId:returnOrderProductId,
			}, '加载已退还设备信息', function(response) {
				self.renderReturnEquipment(response.resultMap.data, container);
			});
		},
		renderReturnEquipment:function(equipmentListData, container) {
			var data = _.extend(Rental.render, {
				equipmentList:equipmentListData.itemList,
			})
			var tpl = $('#returnEquipmentListTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
			container.closest('.affixPanel').find('.equimentListCount').html('共退{0}件'.format(equipmentListData.hasOwnProperty('itemList') ? equipmentListData.itemList.length : 0));
		},
		getReturnMaterial:function($button) {
			var returnOrderMaterialId = $button.data('returnordermaterialid'),
				container = $button.closest('tr').next('.affixTr').find('.table');
			var self = this; 
			Rental.ajax.ajaxData('returnOrder/pageReturnBulk', {
				pageNo:1,
				pageSize:10000,
				returnOrderMaterialId:returnOrderMaterialId,
			}, '加载退还单详细信息', function(response) {
				self.renderReturnMaterial(response.resultMap.data, container);
			});
		},
		renderReturnMaterial:function(bulkMaterialListData, container) {
			var data = {
				bulkMaterialList:bulkMaterialListData.itemList
			}
			var tpl = $('#returnBulkMaterialListTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data))
			container.closest('.affixPanel').find('.materialListCount').html('共退{0}件'.format(bulkMaterialListData.hasOwnProperty('itemList') ? bulkMaterialListData.itemList.length : 0));
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				button.returnOrderNo = order.returnOrderNo; //退还单编号
				rowActionButtons.push(button);
				var unSubmit = order.returnOrderStatus == Enum.returnOrderStatus.num.unSubmit,
					inHand = order.returnOrderStatus == Enum.returnOrderStatus.num.inHand,
					returnStatus = order.returnOrderStatus == Enum.returnOrderStatus.num.forPickup || order.returnOrderStatus == Enum.returnOrderStatus.num.inHand;

				if((button.class == 'submitButton' || button.class == 'editButton' || button.class == 'cancelButton') && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'completeButton') && !inHand) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'returnButton' || button.class == 'returnEquipmentButton' || button.class == 'returnMaterialRowButton') && !returnStatus) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		}
	}

	window.ReturnOrderHandleMixin = ReturnOrderHandleMixin;

})(jQuery);