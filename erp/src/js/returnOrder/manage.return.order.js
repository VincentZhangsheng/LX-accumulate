;(function($) {

	var ReturnOrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_renter_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_renter_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_renter_order_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_renter_order_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_renter_order_detail],
				returnEquipment = this.currentPageAuthor.children[AuthorCode.manage_renter_order_equipment_return],
				returnMaterial = this.currentPageAuthor.children[AuthorCode.manage_renter_order_material_return],
				submit = this.currentPageAuthor.children[AuthorCode.manage_renter_order_submit],
				end = this.currentPageAuthor.children[AuthorCode.manage_renter_order_end],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_renter_order_cancel];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			returnEquipment && this.rowActionButtons.push(AuthorUtil.button(view,'returnButton','','退还'));			
			// returnEquipment && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','退还设备'));
			// returnMaterial && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','退还配件'));
			end && this.rowActionButtons.push(AuthorUtil.button(end,'completeButton','','结束'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.returnOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function() {
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));
			
			self.searchData();  //初始化列表数据

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#returnOrderStatus'),
				data:Enum.array(Enum.returnOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.returnOrder, searchData);

			Rental.ajax.ajaxData('returnOrder/page', searchData, '加载退货单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource: _.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					isChargingStr:function() {
						return this.isCharging == 1 ? "是":"否";
					},
					returnOrderStatusValue:function(){
						return Enum.returnOrderStatus.getValue(this.returnOrderStatus);
					},
					returnOrderStatusClass:function() {
						return Enum.returnOrderStatus.getClass(this.returnOrderStatus);	
					}
				}),
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.ReturnOrderManage = _.extend(ReturnOrderManage, ReturnOrderHandleMixin);

})(jQuery);