;(function($) {

	ReturnOrderMixIn = {
		state:{
			customer:null,
			company:null,
			customerNo:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			returnOrderConsignInfo:null,
		},
		initCommonEvent:function() {
			var self = this;
			
			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});		

			$("#chooseOwnerName").on('click', function(event) {
				event.preventDefault();
				// var user = Rental.localstorage.getUser();
				ChooseUser.init({
					callBack:function(user) {
						// console.log(user);
						$('[name=ownerName]', self.$form).val(user.realName);
						$('[name=owner]', self.$form).val(user.userId);
					}
				});
			});
			
			Rental.ui.events.initDatePicker($('#returnTimePicker'), $("[name=returnTime]", self.$form));	
			
			Rental.ui.renderSelect({
				data:Enum.array(Enum.returnReasonType),
				container:$('[name=returnReasonType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			self.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			$("#chooseCustomer").click(function(event) {
				event.preventDefault();
				ChooseCustomer.init({
					callBack:function(customer) {
						self.setCustomer(customer,null);
					}
				});
			});

			$("#chooseBusinessCustomer").click(function(event) {
				event.preventDefault();
				ChooseBusinessCustomer.init({
					callBack:function(company) {
						self.setCustomer(null,company);
					}
				});
			});

			$('#addAddressButton').click(function(event) {
				event.preventDefault();
				InputAddress.init({
					callBack:function(address) {
						self.chooseAddress(address);
						return true;
					}
				})
			});

			$('#chooseCustomerAddressButton').click(function(event) {
				event.preventDefault();
				if(!self.state.customerNo) {
					bootbox.alert('请先选择客户');
					return;
				}
				ChooseCustomerAddress.init({
					customerNo:self.state.customerNo,
					callBack:function(address) {
						self.chooseAddress(address);
						return true;
					}
				});
			});

			$("#batchAddProduct").click(function(event) {
				event.preventDefault();
				if(!self.state.customerNo) {
					bootbox.alert('请先选择客户');
					return;
				}
				ChooseRentProduct.init({
					customerNo:self.state.customerNo,
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				event.preventDefault();
				self.batchDeleteProduct();
				self.renderProductList();
			});

			self.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
				self.renderProductList();
			});

			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				if(!self.state.customerNo) {
					bootbox.alert('请先选择客户');
					return;
				}
				ChooseCanReturnMaterial.init({
					btach:true,
					searchParms:{
						customerNo:self.state.customerNo,
					}, 
					callBack:function(material) {
						self.chooseMaterial(material);
					}
				});
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDeleteMaterial();
			});

			self.$materialDataListTable.on('click', '.delMaterialButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.deleteMaterial(materialNo);
			});

			self.$dataListTable.on('change', '.returnProductSkuCount', function(event) {
				event.preventDefault();
				var $skuRow = $(this).closest('.skuRow');
				self.changeProductEvent($skuRow);
			});

			self.$materialDataListTable.on('change', '.returnMaterialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeMaterialEvent($materialRow);
			});
		},
		setCustomer:function(customer, company) {
			this.state.customer = customer;
			this.state.company = company;
			if(!!customer) {
				this.state.customerNo = customer.customerNo;
				$('[name=customerName]', this.$form).val(customer.customerPerson.realName);
			}
			if(!!company) {
				this.state.customerNo = company.customerNo;
				$('[name=customerName]', this.$form).val(company.customerCompany.companyName);
			}
			//this.initAddressInfo(this.state.customerNo);
		},
		// initAddressInfo:function(customerNo, renderComplete) {
		// 	if(!customerNo) return;
		// 	CustomerAddressManage.init({
		// 		addAuthor: new Object(),
		// 		customerNo:customerNo,
		// 		checkbox:true,
		// 		renderComplete:renderComplete || function() {},
		// 		callBack:function(data) {
		// 			console.log(data);
		// 		}
		// 	});
		// },
		chooseAddress:function(address) {
			this.state.returnOrderConsignInfo = address;
			this.renderOrderConsignInfo();
		},
		renderOrderConsignInfo:function() {
			var self = this,
				data = {
					address: self.state.returnOrderConsignInfo,
				};
			var tpl = $('#orderConsignInfoTpl').html();
			Mustache.parse(tpl);
			$('#orderConsignInfo').html(Mustache.render(tpl, data));
		},
		renderProductList:function() {
			OrderManageItemRender.renderProductList(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable);
		},
		renderMaterialList:function(rowActionButtons) {
			OrderManageItemRender.renderMaterialList(this.state.chooseMaterialList, this.$materialDataListTpl, this.$materialDataListTable, rowActionButtons);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = OrderManageUtil.chooseProduct(this.state.chooseProductList, product);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = OrderManageUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		deleteSku:function($deleteButton) {
			this.state.chooseProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		changeProductEvent:function($skuRow) {
			this.state.chooseProductList = this.changeProduct(this.state.chooseProductList, $skuRow);
		},
		getSkuBySkuRow:function($skuRow) {
			return {
				productId:$('.productId', $skuRow).val(),
				productSkuId:$('.productSkuId', $skuRow).val(),
				returnProductSkuCount:$('.returnProductSkuCount', $skuRow).val(),
			};
		},
		changeProduct:function(chooseProductList, $skuRow) {
			var sku = this.getSkuBySkuRow($skuRow);
			var productIndex = _.findIndex(chooseProductList, {productId:parseInt(sku.productId)});
			var skuIndex = _.findIndex(chooseProductList[productIndex].chooseProductSkuList, {skuId:parseInt(sku.productSkuId)});
			chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(chooseProductList[productIndex].chooseProductSkuList[skuIndex], sku);
			return chooseProductList;
		},
		chooseMaterial:function(material) {
			this.state.chooseMaterialList = OrderManageUtil.chooseMaterial(this.state.chooseMaterialList, material);
			this.renderMaterialList();
		},
		batchDeleteMaterial:function() {
			this.state.chooseMaterialList = OrderManageUtil.batchDeleteMaterial(this.$materialDataListTable, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		deleteMaterial:function(materialNo) {
			var  index = _.findIndex(this.state.chooseMaterialList,{materialNo:materialNo});
			this.state.chooseMaterialList.splice(index,1);			
			this.renderMaterialList();
		},
		changeMaterialEvent:function($materialRow) {
			this.state.chooseMaterialList = this.changeMaterial(this.state.chooseMaterialList, $materialRow);
		},
		getMaterialByMaterialRow:function($materialRow) {
			return  {
				materialNo:$('.materialNo', $materialRow).val(),
				materialId:$('.materialId', $materialRow).val(),
				returnMaterialCount:$('.returnMaterialCount', $materialRow).val(),
			};
		},
		changeMaterial:function(chooseMaterialList, $materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var index = _.findIndex(chooseMaterialList, {materialNo:material.materialNo});
			chooseMaterialList[index] = _.extend(chooseMaterialList[index], material);
			return chooseMaterialList;
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.returnOrderProductList);
			this.renderProductList();
		},	
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('returnProductSkuSnapshot') ? JSON.parse(item.returnProductSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0], item)
					} 
					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					pre[item.productId].chooseProductSkuList.push(item);
					pre[item.productId] = _.extend(pre[item.productId], product);
			
					return pre;
				},{});

				chooseProductList = _.values(orderItemsMapByProductId);
			}
			return chooseProductList;
		},
		resolveChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.returnMaterialSnapshot);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
	};

	window.ReturnOrderMixIn = ReturnOrderMixIn;

})(jQuery);