/**
 * decription: 打印退货单
 * author:zhangsheng
 * time: 2018-4-12
 */

;(function($){
	
	var ReturnOrderPrint = {
		state:{
			chooseProductList:new Array(),
			no:null,
			pageData:null, //每页数据集合
			totalAmountInfoPageIndx:1, //统计数据所在页
			productRowNum:0,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_k3_manage];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_k3_manage_return_order];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_k3_manage_return_order_detial];
		},
		initDom:function() {
			this.$form = $("#orderDetailForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;
			this.loadData();

			this.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});

			$("#printButton").on("click",function(event) {
				event.preventDefault();
				self.savaPrintRecord(function() {
					window.print();
				})
			})
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到退货单编号');
				return;
			}
			Rental.ajax.submit("{0}k3/queryReturnOrderByNo".format(SitePath.service),{returnOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载退货单详细信息",response.description || '失败');
				}
			}, null ,"加载退货单详细信息");
		},
		initData:function(data) {
			this.initReturnProductList(data);
			this.initPrintPager(data);	
			this.renderOrderBaseInfo(data);
			this.renderReturnProduct();
			this.renderOrderAmountInfo(data);
			this.renderSignature();
			this.renderOwner(data);
			this.renderTotalPrice(data);
			this.renderRemark(data);
		},
		renderOrderBaseInfo:function(order) {
			var renderHtml = this.renderOrderInfo(order, $('#orderBaseInfoTpl').html());
			$("#subpage1").append(renderHtml);
		},
		renderOrderAmountInfo:function(order) {
			var totalCount = 0;
			order.hasOwnProperty("k3ReturnOrderDetailList") && order.k3ReturnOrderDetailList.forEach(function(item){
				totalCount += parseInt(item.productCount)
			})
			$("#totalCount").html(totalCount);
			$("#subpage"+ this.state.totalAmountInfoPageIndx).append($("#totalInfo").html());
		},
		renderSignature:function() {
			$("#subpage"+ this.state.pageSize).append($("#signature").html());
		},
		renderTotalPrice:function(data) {
			if(!data) return;
			var logisticsAmount	= data.hasOwnProperty("logisticsAmount") ? data.logisticsAmount : "";
			var serviceAmount = data.hasOwnProperty("serviceAmount") ? data.serviceAmount : "";
			totalMoney = parseFloat(logisticsAmount + serviceAmount);
			totalMoney = _.isNumber(totalMoney) ? totalMoney.toFixed(2) : "";
			moneyStr = !!totalMoney ? ("￥" + totalMoney + " " + "(" + this.moneyToChinese(totalMoney) + ")") : "";
			$("#totalMoney").html(moneyStr)
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				returnModeValue:function() {
					return Enum.returnMode.getValue(this.returnMode);
				},
				returnReasonValue:function() {
					return Enum.returnReasonType.getValue(this.returnReasonType);
				},
			});
			Mustache.parse(tpl);
			return Mustache.render(tpl, data);
		},
		savaPrintRecord:function(callBack) {
			var self = this;
			var commitData = {
				referNo:self.state.no,
				referType:Enum.printReferType.num.returnOrder
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'print/savePrintLog'), commitData ,function(response){
				if(response.success) {
					callBack && callBack();
				} else {
					Rental.notification.error("打印订单",response.description || '失败');
				}
			}, null ,"打印订单",'dialogLoading');
		},
		initReturnProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.k3ReturnOrderDetailList);
		},
		initPrintPager:function(order) {
			var self = this;
				productLength = self.state.chooseProductList.length,
				onePageshowCount = 18,
				firstShowAmountInfoMaxCount = 26,
				firstMaxCount = 33,
				normalCount = 36,
				lastCount = 21;

			self.state.pageData = new Array();

			if(productLength <= onePageshowCount) {
				self.state.pageSize = 1;
				self.state.totalAmountInfoPageIndx = 1;
				self.state.pageData.push({
					productList:self.state.chooseProductList
				});
			} else if(productLength > onePageshowCount && productLength <= firstMaxCount + lastCount){
				self.state.pageSize = 2;
				if(productLength <= firstShowAmountInfoMaxCount) {
					self.state.pageData.push({
						productList:self.state.chooseProductList
					});
					self.state.totalAmountInfoPageIndx = 1;
				} else {
					self.state.totalAmountInfoPageIndx = 2;
					if(productLength <=  firstMaxCount) {
						self.state.pageData.push({
							productList:self.state.chooseProductList
						});
					} else {
						self.state.pageData.push({
							productList: _.first(self.state.chooseProductList, firstMaxCount)
						});
						self.state.pageData.push({
							productList:_.last(self.state.chooseProductList, productLength - firstMaxCount)
						});
					}
				}
			} 
			else if (productLength > firstMaxCount + lastCount && productLength <= firstMaxCount + normalCount + lastCount) {
				self.state.pageSize = 3;
				if(productLength <= firstShowAmountInfoMaxCount + firstMaxCount + 3) {
					self.state.pageData.push({
						productList: _.first(self.state.chooseProductList, firstMaxCount)
					});
					self.state.pageData.push({
						productList:_.last(self.state.chooseProductList, productLength - firstMaxCount)
					});
					self.state.totalAmountInfoPageIndx = 2;
				} else {
					var fp = _.first(self.state.chooseProductList, firstMaxCount);
					var lp = _.last(self.state.chooseProductList, productLength - firstMaxCount);
					self.state.pageData.push({productList:fp})
					if(lp.length <= normalCount) {
						self.state.pageData.push({productList:lp})
					} else {
						self.state.pageData.push({
							productList: _.first(lp, normalCount)
						});
						self.state.pageData.push({
							productList:_.last(lp, lp.length - normalCount)
						});
					}
					self.state.totalAmountInfoPageIndx = 3;
				}
			}
			self.renderPrintPager();
		},
		renderPrintPager:function() {
			var self = this, pagerArray = new Array();
			for (var i = 1; i <= self.state.pageSize; i++) {
				pagerArray.push({pagerIndex:i});
			}
			var dataSource = {
				data:pagerArray,
				pageSize:self.state.pageSize
			}
			var tpl = $('#pagerTpl').html();
			Mustache.parse(tpl);
			$('#renturnOrderPagers').html(Mustache.render(tpl, dataSource));
		},
		renderReturnProduct:function() {
			var self = this;
			for (var i = 0; i < self.state.pageSize; i++) {
				var data = self.state.pageData[i];
				if(data) {
					data.hasOwnProperty('productList') && self.renderPrintProductList(i+1, data.productList);
				}
			}
		},
		renderPrintProductList:function(pagerIndex, data) {
			var renderHtml = this.renderProductList(data);
			$('#subpage'+pagerIndex).append(renderHtml);
		},
		// renderQcode:function(data) {
		// 	for (var i = 1; i <= this.state.pageSize; i++) {
		// 		var qrcode = new QRCode(document.getElementById("qrcodeCanvas"+i));
		// 		qrcode.makeCode(data.returnOrderNo);
		// 		$(".returnOrderNo").html(data.returnOrderNo);
		// 	}
		// },
		renderOwner:function(data) {
			ApiData.customerDetail({
				customerNo:data.k3CustomerNo,
				success:function(response) {
					if(response.hasOwnProperty('customerOwnerUser')) {
						$('.customerOwnerUser').html('{0} {1}'.format(response.customerOwnerUser.realName,response.customerOwnerUser.phone ))
					}
				}
			});
		},
		renderRemark:function(data) {
			remarkText = data.hasOwnProperty("remark") ? data.remark : "";
			$("#returnOrderRemark").html(remarkText);
		}
	};

	window.ReturnOrderPrint = _.extend(ReturnOrderPrint, ReturnOrderPrintMixIn);

})(jQuery);