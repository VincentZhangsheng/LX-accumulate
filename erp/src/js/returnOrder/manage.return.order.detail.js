;(function($){
	
	var ReturnOrderDetail = {
		state:{
			no:null,
			customer:null,
			company:null,
			customerNo:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			returnOrderConsignInfo:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_renter_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_renter_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_detail];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.returnEquipmentAcitonButtons = new Array();
			// this.returnMaterialAcitonButtons = new Array();
			this.returnMaterialRowAcitonButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_detail],
				returnEquipment = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_equipment_return],
				returnMaterial = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_material_return],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_submit],
				end = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_end],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_cancel];

			add && this.actionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			end && this.actionButtons.push(AuthorUtil.button(end,'completeButton','','结束'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));

			
			returnEquipment && this.returnEquipmentAcitonButtons.push(AuthorUtil.button(returnEquipment,'returnEquipmentButton','','退还设备'));
			// returnMaterial && this.returnMaterialAcitonButtons.push(AuthorUtil.button(returnMaterial,'returnMaterialButton','','退还配件'));
			returnMaterial && this.returnMaterialRowAcitonButtons.push(AuthorUtil.button(returnMaterial,'returnMaterialRowButton','','退还'));
		},
		initDom:function() {
			this.$form = $("#detailOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_renter_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.initHandleEvent(this.$form, function() {
				self.loadData();
			}); 

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到退还单编号');
				return;
			}
			Rental.ajax.ajaxData('returnOrder/detail', {returnOrderNo:self.state.no}, '加载退还单详细信息', function(response) {
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderReturnEquipmentButtons(data);
			this.renderReturnOrderBaseInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		renderActionButton:function(returnOrder) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, returnOrder),
				returnOrderNo:self.state.no,
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderReturnEquipmentButtons:function(returnOrder) {
			var self = this,
				actionButtonsTpl = $('#returnEquipmentButtonsTpl').html();
				actionButtons = self.filterAcitonButtons(self.returnEquipmentAcitonButtons, returnOrder)
			var data = {
				hasActionButtons:actionButtons.length > 0,
				acitonButtons:actionButtons,
			}
			Mustache.parse(actionButtonsTpl);
			$("#returnEquipmentButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderReturnOrderBaseInfo:function(returnOrder) {
			$("#returnOrderNo").html(returnOrder.returnOrderNo);
			var data = _.extend(Rental.render, {
				data:returnOrder,
				isChargingStr:function() {
					return this.isCharging == 1 ? '是':'否';
				},
				returnModeStr:function() {
					return this.returnMode == 1 ? '上门取件':'邮寄';
				},
				returnOrderStatusValue:function() {
					return Enum.returnOrderStatus.getValue(this.returnOrderStatus);
				},
				addressForamt:function() {
					return [this.provinceName, this.cityName, this.districtName, this.address].join('');
				},
				returnReasonTypeValue:function() {
					return Enum.returnReasonType.getValue(this.returnReasonType);
				}
			})
			var tpl = $("#returnOrderBaseInfoTpl").html();
			Mustache.parse(tpl);
			$('#returnOrderBaseInfo').html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.returnOrderProductList);
			this.renderProductList();
		},	
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('returnProductSkuSnapshot') ? JSON.parse(item.returnProductSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0], item)
					} 
					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					pre[item.productId].chooseProductSkuList.push(item);
					pre[item.productId] = _.extend(pre[item.productId], product);
			
					return pre;
				},{});

				chooseProductList = _.values(orderItemsMapByProductId);
			}
			return chooseProductList;
		},
		initChooseMaterialList:function(returnOrder) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(returnOrder.returnOrderMaterialList);
			this.renderMaterialList(this.filterAcitonButtons(this.returnMaterialRowAcitonButtons, returnOrder));
		},
	};

	window.ReturnOrderDetail = _.extend(ReturnOrderDetail, ReturnOrderMixIn, ReturnOrderHandleMixin);

})(jQuery);