;(function($){
	
	var InputReturnOrderEndInfo = {
		init:function(prams) {
			this.props = _.extend({
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "return-order-end-info-modal/input", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			var self = this;
			this.$modal = pModal.container;
			this.$form = $("#inputEndInfoForm");
			this.$cancelButton = $('.cancelButton', this.$form);
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(this.$form,function(form){
				self.inputInfo();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			console.log($('#returnTimePicker').size(), $('#returnTimePicker'))
			Rental.ui.events.initDatePicker($('#returnTimePicker'), $("[name=returnTime]", self.$form));
		},
		inputInfo:function() {
			try {
				var formData = Rental.form.getFormData(this.$form);
				formData.returnTime = new Date(formData['returnTime']).getTime();
				this.callBackFunc(formData)
			} catch (e) {
				Rental.notification.error('结束订单输入费用信息', '<br />' + e );
			}
		},
		callBackFunc:function(info) {
			if(this.props.hasOwnProperty('callBack') && this.props.callBack(info)) {
				Rental.modal.close();	
			} 
		}
	};

	window.InputReturnOrderEndInfo = InputReturnOrderEndInfo;

})(jQuery);