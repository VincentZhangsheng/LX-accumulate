;(function($){
	
	var EditReturnOrder = {
		state:{
			no:null,
			customer:null,
			company:null,
			customerNo:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			returnOrderConsignInfo:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_renter_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_renter_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_add];
		},
		initDom:function() {
			this.$form = $("#editOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_renter_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到退还单编号');
				return;
			}
			Rental.ajax.submit("{0}returnOrder/detail".format(SitePath.service),{returnOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载退还单详细信息",response.description || '失败');
				}
			}, null ,"加载退还单详细信息", 'listLoading');
		},
		initData:function(data) {
			this.initState(data);
			this.initFormData(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		initState:function(data) {
			this.state.customerNo = data.customerNo;
			this.state.returnOrderConsignInfo = data.returnOrderConsignInfo;
			this.renderOrderConsignInfo();
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
	  		Rental.ui.renderFormData(self.$form, data);
	  		$('[name=returnTime]', self.$form).val(new Date(data.returnTime).format('yyyy-MM-dd'));
		},
		initChooseMaterialList:function(returnOrder) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(returnOrder.returnOrderMaterialList);
			this.renderMaterialList();
		},
		edit:function() {
			var self = this;
			try {

				if(!self.state.no) {
					bootbox.alert('找不到退货单编号');
					return;
				}

				var formData = Rental.form.getFormData(self.$form);

				if(!self.state.customerNo) {
					bootbox.alert('请选择客户');
					return;
				}

				if(!self.state.returnOrderConsignInfo) {
					bootbox.alert('请选择收货地址');
					return;	
				}

				var commitData = {
					returnOrderNo:self.state.no,
					customerNo:self.state.customerNo,
					isCharging:formData['isCharging'],
					remark:formData['remark'],
					returnMode:formData['returnMode'],
					returnTime:new Date(formData['returnTime']).getTime(),
					returnReasonType:formData['returnReasonType'],
					owner:formData['owner'],
				}

				// var address = $('[name=customerConsignInfoId]').filter(':checked').data('address');
				commitData.returnOrderConsignInfo = {
					consigneeName:self.state.returnOrderConsignInfo.consigneeName,
					consigneePhone:self.state.returnOrderConsignInfo.consigneePhone,
					province:self.state.returnOrderConsignInfo.province,
					city:self.state.returnOrderConsignInfo.city,
					district:self.state.returnOrderConsignInfo.district,
					address:self.state.returnOrderConsignInfo.address,
				}

				var returnOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						returnOrderProductList.push({
							returnProductSkuId:sku.skuId,
							returnProductSkuCount:sku.returnProductSkuCount,
						})
					})
				});

				var returnOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					returnOrderMaterialList.push({
						returnMaterialNo:material.materialNo,
						returnMaterialCount:material.returnMaterialCount,
					})
				});

				if(returnOrderProductList.length == 0 && returnOrderMaterialList.length == 0) {
					bootbox.alert('请选择要退的商品或配件');
					return;
				}

				commitData.returnOrderProductList = returnOrderProductList;
				commitData.returnOrderMaterialList = returnOrderMaterialList;

				Rental.ajax.submit("{0}returnOrder/update".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success("修改退货单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("修改退货单",response.description || '失败');
					}
				}, null ,"修改退货单", 'dialogLoading');
			} catch(e) {
				Rental.notification.error("修改退货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建退货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form.find('.state-success').removeClass('state-success');
							self.$form[0].reset();
							self.state = new Object();
				        }
				    }
				});
			}
		}
	};


	window.EditReturnOrder = _.extend(EditReturnOrder, ReturnOrderMixIn);

})(jQuery);