;(function($){
	
	var AddReturnOrder = {
		state:{

		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_renter_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_renter_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_renter_order_add];
		},
		initDom:function() {
			this.$form = $("#addOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_renter_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			self.initCommonEvent(); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				if(!self.state.customerNo) {
					bootbox.alert('请选择客户');
					return;
				}

				if(!self.state.returnOrderConsignInfo) {
					bootbox.alert('请选择收货地址');
					return;	
				}

				var commitData = {
					customerNo:self.state.customerNo,
					isCharging:formData['isCharging'],
					remark:formData['remark'],
					returnMode:formData['returnMode'],
					returnTime:new Date(formData['returnTime']).getTime(),
					returnReasonType:formData['returnReasonType'],
					owner:formData['owner'],
				}

				// var address = $('[name=customerConsignInfoId]').filter(':checked').data('address');
				commitData.returnOrderConsignInfo = {
					consigneeName:self.state.returnOrderConsignInfo.consigneeName,
					consigneePhone:self.state.returnOrderConsignInfo.consigneePhone,
					province:self.state.returnOrderConsignInfo.province,
					city:self.state.returnOrderConsignInfo.city,
					district:self.state.returnOrderConsignInfo.district,
					address:self.state.returnOrderConsignInfo.address,
				}

				var returnOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						returnOrderProductList.push({
							returnProductSkuId:sku.skuId,
							returnProductSkuCount:sku.returnProductSkuCount,
						})
					})
				});

				var returnOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					returnOrderMaterialList.push({
						returnMaterialNo:material.materialNo,
						returnMaterialCount:material.returnMaterialCount,
					})
				});

				if(returnOrderProductList.length == 0 && returnOrderMaterialList.length == 0) {
					bootbox.alert('请选择要退的商品或配件');
					return;
				}

				commitData.returnOrderProductList = returnOrderProductList;
				commitData.returnOrderMaterialList = returnOrderMaterialList;

				Rental.ajax.submit("{0}returnOrder/add".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success("创建退货单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("创建退货单",response.description || '失败');
					}
				}, null ,"创建退货单", 'dialogLoading');
			} catch(e) {
				Rental.notification.error("创建退货单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建退货单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
		}
	};


	window.AddReturnOrder = _.extend(AddReturnOrder, ReturnOrderMixIn);

})(jQuery);