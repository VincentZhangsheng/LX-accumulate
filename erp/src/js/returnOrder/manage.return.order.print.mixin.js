/**
 * decription: 打印退货单
 * author:zhangsheng
 * time: 2018-5-7
 */
;(function($) {

	ReturnOrderPrintMixIn = {
		state:{
			chooseProductList:new Array(),
			customerNo:null,
			productRowNum:0,
		},
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				chooseProductList = data.map(function(item) {
					var orderProduct = item.hasOwnProperty("orderProduct") ? item.orderProduct : {};
					var product =  orderProduct.hasOwnProperty('productSkuSnapshot') ? JSON.parse(orderProduct.productSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0] || {},{productUnitAmount:item.orderProduct.productUnitAmount},{isNewProduct:orderProduct.isNewProduct}, item)
					}
					return item;
				})
			}
			return chooseProductList;
		},
		renderProductList:function(productList) {
			var self = this;
			productList = productList || new Array();
			var data = {
				hasListData:productList.length > 0,
				dataSource: _.extend(Rental.render, {
					"listData":productList,
					propertiesToStr:function() {
						var str = this.hasOwnProperty("productSkuPropertyList") ? this.productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					payPrice:function() {
						return (parseFloat(this.productCount) * parseFloat(this.productUnitAmount)).toFixed(2);
					},
					rowNum:function() {
						self.state.productRowNum = self.state.productRowNum + 1;
						return self.state.productRowNum;
					},
					isNewProductValue:function() {
						return this.isNewProduct == 1 ? "全新":"次新";
					},
					currentSkuPrice:function() {
						if(this.hasOwnProperty('isNewProduct') && parseInt(this.isNewProduct) == 1) {
							return this.hasOwnProperty('newSkuPrice') ? this.newSkuPrice.toFixed(2) : '';
						} else if(this.hasOwnProperty('skuPrice')) {
							return  this.skuPrice.toFixed(2);
						} else if(this.hasOwnProperty("orderMaterial") && this.orderMaterial.hasOwnProperty("materialSnapshot")) {
							var materialInfo = JSON.parse(this.orderMaterial.materialSnapshot)
							if(this.orderMaterial.hasOwnProperty('isNewMaterial') && parseInt(this.orderMaterial.isNewMaterial) == 1) {
								return materialInfo.hasOwnProperty('newMaterialPrice') ? materialInfo.newMaterialPrice.toFixed(2) : '';
							} else {
								return materialInfo.hasOwnProperty('materialPrice') ? materialInfo.materialPrice.toFixed(2) : '';
							}
						} else {
							return "";
						}
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			return Mustache.render(this.$dataListTpl, data)
		},
		moneyToChinese:function(money) {
			var cnNums = ["零","壹","贰","叁","肆","伍","陆","柒","捌","玖"];
			var cnIntRadice = ["","拾","佰","仟"];
			var cnIntUnits = ["","万","亿","兆"];
			var cnDeUnits = ["角","分"];
			var cnInteger = "整";
			var cnIntLast = "元";
			var maxNum = 999999999999999.99;

			var IntegerNum, DecimalNum, ChineseStr = "", parts;

			if(!money) {
				return "";
			}

			money = parseFloat(money)
			if(money >= maxNum) {
				return "";
			}
			if(money == 0) {
				ChineseStr = cnNums[0] + cnIntLast;
				return ChineseStr;
			}

			money = money.toString();
			if(money.indexOf(".") == -1) {
				IntegerNum = money;
				DecimalNum = "";
			} else {
				parts = money.split(".");
				IntegerNum = parts[0];
				DecimalNum = parts[1].substr(0,2);
			}

			if(parseInt(IntegerNum, 10) > 0) {
				zeroCount = 0;
				IntLen = IntegerNum.length;
				for(i=0; i<IntLen; i++) {
					n= IntegerNum.substr(i,1);
					p = IntLen - i -1;
					q = p / 4;
					m = p % 4;
					if(n == "0") {
						zeroCount++;
					} else {
						if(zeroCount > 0) {
							ChineseStr += cnNums[0];
						}
						zeroCount = 0;
						ChineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
					}
					if(m == 0 && zeroCount < 4) {
						ChineseStr += cnIntUnits[q];
					}
				}
				ChineseStr += cnIntLast;
			}

			if(DecimalNum != "") {
				deLen = DecimalNum.length;
				for(i=0; i<deLen; i++) {
					n = DecimalNum.substr(i,1);
					if(n != "0") {
						ChineseStr += cnNums[Number(n)] + cnDeUnits[i];
					}
				}
			}

			if(ChineseStr == "") {
				ChineseStr += cnNums[0] + cnIntLast;
			}

			return ChineseStr;
		},
	};

	window.ReturnOrderPrintMixIn = ReturnOrderPrintMixIn;

})(jQuery);