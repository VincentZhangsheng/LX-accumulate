;(function($){

	var DetailTransferInOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
			workflowType:null,
			transferOrderMode:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_transfer_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_transfer_order_in_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_detail];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			// this.rowActionButtons = new Array();
			this.rowProductActionuttons = new Array();
			this.rowMaterialActionuttons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_detail],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_submit],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_cancel],
				end = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_end];

			// add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			// view && this.actionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			end && this.actionButtons.push(AuthorUtil.button(end,'endButton','','结束'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));

			view && this.rowProductActionuttons.push(AuthorUtil.button(view,'viewEquimentButton','','查看设备'));
			view && this.rowMaterialActionuttons.push(AuthorUtil.button(view,'viewMaterialButton','','查看散料'));
		},
		initDom:function() {
			this.$form = $("#detailTransferInOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
			this.state.workflowType = Enum.workflowType.num.transferInOrder;
			this.state.transferOrderMode = Enum.transferOrderMode.num.in;
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_transfer_order_in_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.loadData();

			self.initCommonEvent(); // from order mixin

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到流入单编号');
				return;
			}
			Rental.ajax.submit("{0}transferOrder/detailTransferOrderByNo".format(SitePath.service),{transferOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载流入单详情",response.description || '失败');
				}
			}, null ,"加载流入单详情", 'listLoading');
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderOrderBaserInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		renderActionButton:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtons(self.actionButtons, order),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
				},
				actionButtonsTpl = $('#actionButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaserInfo:function(order) {
			$("#transferOrderNo").html(order.transferOrderNo);
			var data = _.extend(Rental.render, {
				data:order,
				transferOrderStatusValue:function(){
					return Enum.transferOrderStatus.getValue(this.transferOrderStatus);
				},
				transferOrderStatusClass:function() {
					return Enum.transferOrderStatus.getClass(this.transferOrderStatus);
				},
				transferOrderTypeValue:function() {
					return Enum.transferOrderType.getValue(this.transferOrderType);
				}
			});
			var tpl = $("#orderBaseInfoTpl").html();
			Mustache.parse(tpl);
			$('#orderBaseInfo').html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.transferOrderProductList);
			this.renderProductList(this.filterAcitonButtons(this.rowProductActionuttons, data));
		},	
		initChooseMaterialList:function(order) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(order.transferOrderMaterialList);
			this.renderMaterialList(this.filterAcitonButtons(this.rowMaterialActionuttons, order));
		}
	};

	window.DetailTransferInOrder = _.extend(DetailTransferInOrder, TransferOrderHandleMixin, TransferOrderMixIn);

})(jQuery);