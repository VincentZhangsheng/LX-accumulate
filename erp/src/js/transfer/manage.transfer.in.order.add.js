;(function($){

	var AddTransferInOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_transfer_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_transfer_order_in_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_in_add];
		},
		initDom:function() {
			this.$form = $("#addTransferInOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_transfer_order_in_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			Rental.ui.renderSelect({
				data:Enum.arrayByEnumObj(Enum.transferOrderType, Enum.transferOrderType.in),
				container:$('[name=transferOrderType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			self.initCommonEvent({isVerifyStock:false}); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					transferOrderName:formData['transferOrderName'],
					transferOrderType:formData['transferOrderType'],
					// warehouseId:formData['warehouseId'],
					remark:formData['remark'],
					transferOrderMode:Enum.transferOrderMode.num.in,
				}

				var transferOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						transferOrderProductList.push({
							productSkuId:sku.skuId,
							productCount:sku.productCount,
							isNew:sku.isNew,
						})
					})
				});

				var transferOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					transferOrderMaterialList.push({
						materialNo:material.materialNo,
						materialCount:material.materialCount,
						isNew:material.isNew,
					})
				});

				if(transferOrderProductList.length == 0 && transferOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(transferOrderProductList.length > 0) {
					commitData.transferOrderProductList = transferOrderProductList;
				}

				if(transferOrderMaterialList.length > 0) {
					commitData.transferOrderMaterialList = transferOrderMaterialList;
				}

				var description = "创建流入单";
				Rental.ajax.submit("{0}transferOrder/createTransferOrderInto".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null , description, 'dialogLoading');
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建流入单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.AddTransferInOrder = _.extend(AddTransferInOrder, TransferOrderMixIn);

})(jQuery);