;(function($) {

	TransferOrderMixIn = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			customerNo:null,
		},
		initCommonEvent:function(prams) {
			var self = this;

			// //选择仓库
			// $("#chooseWarehouseId").on('click',function(event) {
			// 	WarehouseChoose.init({
			// 		callBack:function(warehouse) {
			// 			console.log(warehouse)
			// 			$('[name=warehouseId]', self.$form).val(warehouse.warehouseId);
			// 			$('[name=warehouseName]', self.$form).val(warehouse.warehouseName);
			// 		}
			// 	});	
			// });

			self.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			
			$("#batchAddProduct").click(function(event) {
				ProductChoose.modal({
					isVerifyStock:prams || prams.hasOwnProperty('isVerifyStock') ? prams.isVerifyStock : true,
					showIsNewCheckBox:true,
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});
			});

			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms:new Object(), 
					showIsNewCheckBox:true,
					callBack:function(material) {
						self.chooseMaterial(material);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				self.batchDeleteProduct();
				self.renderProductList();
			});

			self.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
				self.renderProductList();
			});

			self.$dataListTable.on('change', '.productCount', function(event) {
				event.preventDefault();
				var $skuRow = $(this).closest('.skuRow');
				self.changeProductEvent($skuRow);
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDeleteMaterial();
			});

			self.$materialDataListTable.on('click', '.delMaterialButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.deleteMaterial(materialNo);
			});

			self.$materialDataListTable.on('change', '.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeMaterialEvent($materialRow);
			});
		},
		renderProductList:function(rowActionButtons){
			OrderManageItemRender.renderProductList(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable, rowActionButtons);
		},
		renderMaterialList:function(rowActionButtons) {
			OrderManageItemRender.renderMaterialList(this.state.chooseMaterialList, this.$materialDataListTpl, this.$materialDataListTable, rowActionButtons);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = this.chooseProductFunc(this.state.chooseProductList, product);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		chooseProductFunc:function(chooseProductList, product) {	
			if(!chooseProductList) {
				chooseProductList = new Array();
			}	
			if(chooseProductList.length > 0) {

				var currProductIndex = _.findIndex(chooseProductList, {productId:product.productId});
				var currProduct = currProductIndex > -1 ? chooseProductList[currProductIndex] : null;

				if(currProduct) {
					product.chooseProductSkuList.map(function(item) {
						var index = _.findIndex(currProduct.chooseProductSkuList, {skuId:item.skuId, isNew:item.isNew}); 
						if(index > -1) {
							currProduct.chooseProductSkuList[index] = _.extend(currProduct.chooseProductSkuList[index], item);
						} else {
							currProduct.chooseProductSkuList.push(item);
						}
					});
				} else {
					chooseProductList.push(product);
				}
			} else {
				chooseProductList.push(product);
			}
			return chooseProductList;
		},
		changeProductEvent:function($skuRow) {
			this.state.chooseProductList = this.changeProduct(this.state.chooseProductList, $skuRow);
			this.renderProductList();
		},
		getSkuBySkuRow:function($skuRow) {
			return {
				productId:$('.productId', $skuRow).val(),
				productSkuId:$('.productSkuId', $skuRow).val(),
				productCount:$('.productCount', $skuRow).val(),
				isNew:parseInt($('.isNewProduct', $skuRow).val()),
			};
		},
		changeProduct:function(chooseProductList, $skuRow) {
			var sku = this.getSkuBySkuRow($skuRow);
			var productIndex = _.findIndex(chooseProductList, {productId:parseInt(sku.productId)});
			var skuIndex = _.findIndex(chooseProductList[productIndex].chooseProductSkuList, {skuId:parseInt(sku.productSkuId), isNew:parseInt(sku.isNew)});
			chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(chooseProductList[productIndex].chooseProductSkuList[skuIndex], sku);
			return chooseProductList;
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = OrderManageUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		deleteSku:function($deleteButton) {
			this.state.chooseProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		chooseMaterial:function(material) {
			this.state.chooseMaterialList = this.chooseMaterialFunc(this.state.chooseMaterialList, material);
			this.renderMaterialList();
		},
		chooseMaterialFunc:function(chooseMaterialList, material) {
			try {
				if(chooseMaterialList.length > 0) {
					var index  = _.findIndex(chooseMaterialList, {materialNo:material.materialNo, isNew:material.isNew});
					if(index > -1) {
						chooseMaterialList[index] = _.extend(chooseMaterialList, material || {});
					} else {
						chooseMaterialList.push(material)
					}
				} else {
					chooseMaterialList.push(material)
				}
				Rental.notification.success("选择配件", '成功');

				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
		},
		changeMaterialEvent:function($materialRow) {
			this.state.chooseMaterialList = this.changeMaterial(this.state.chooseMaterialList, $materialRow);
			this.renderMaterialList();
		},
		getMaterialByMaterialRow:function($materialRow) {
			return  {
				materialNo:$('.materialNo', $materialRow).val(),
				materialId:$('.materialId', $materialRow).val(),
				materialCount:$('.materialCount', $materialRow).val(),
				isNew:parseInt($('.isNewMaterial', $materialRow).val()),
			};
		},
		changeMaterial:function(chooseMaterialList, $materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var index = _.findIndex(chooseMaterialList, {materialNo:material.materialNo, isNew:parseInt(material.isNew)});
			chooseMaterialList[index] = _.extend(chooseMaterialList[index], material);
			return chooseMaterialList;
		},
		batchDeleteMaterial:function() {
			this.state.chooseMaterialList = OrderManageUtil.batchDeleteMaterial(this.$materialDataListTable, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		deleteMaterial:function(materialNo) {
			var  index = _.findIndex(this.state.chooseMaterialList,{materialNo:materialNo});
			this.state.chooseMaterialList.splice(index,1);			
			this.renderMaterialList();
		},
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('productSkuSnapshot') ? JSON.parse(item.productSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0], item)
					} 
					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					pre[item.productId].chooseProductSkuList.push(item);
					pre[item.productId] = _.extend(pre[item.productId], product);
			
					return pre;
				},{});

				chooseProductList = _.values(orderItemsMapByProductId);
			}
			return chooseProductList;
		},
		resolveChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.materialSnapshot);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
	};

	window.TransferOrderMixIn = TransferOrderMixIn;

})(jQuery);