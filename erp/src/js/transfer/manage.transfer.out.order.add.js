/**
* 添加流出单
*/
;(function($){

	var AddTransferOutOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_transfer_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_transfer_order_out_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_add];
		},
		initDom:function() {
			this.$form = $("#addTransferOutOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_transfer_order_out_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			Rental.ui.renderSelect({
				data:Enum.arrayByEnumObj(Enum.transferOrderType, Enum.transferOrderType.out),
				container:$('[name=transferOrderType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			// self.initCommonEvent(); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					transferOrderName:formData['transferOrderName'],
					transferOrderType:formData['transferOrderType'],
					remark:formData['remark'],
					transferOrderMode:Enum.transferOrderMode.num.out,
				}

				var description = "创建流出单";
				Rental.ajax.submit("{0}transferOrder/createTransferOrderOut".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null , description, 'dialogLoading');
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建流出单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.AddTransferOutOrder = AddTransferOutOrder;//_.extend(AddTransferOutOrder, TransferOrderMixIn);

})(jQuery);