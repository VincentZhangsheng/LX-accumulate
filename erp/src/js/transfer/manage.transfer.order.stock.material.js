;(function($) {

	var TransferOrderStockMaterial = {
		init:function(prams) {
			this.props = $.extend({
				title:'',
				btach:true, //是否是批量
				searchParms:{},
				showIsNewCheckBox:true,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "order-transfer-out-manage/stock-up-material-modal", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			this.$chooseModal = pModal.container;
			this.$searchForm = $("#chooseMaterialSearchForm");
			this.$dataListTpl = $("#chooseMaterialModalDataListTpl").html();
			this.$dataListTable = $("#chooseMaterialDataListTable");

			$('.modalTitle', this.$chooseModal).html(this.props.title);

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Rental.ui.renderSelect({
				container:$('#materialType'),
				data:Enum.array(Enum.materialType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（配件类型）'
			});

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $this = $(this);
					$panel = $this.closest('.materialRow').next('.stockMatrialRow').find('.stockMatrialPanel');
				$panel.slideToggle('fast');
			});

			self.$dataListTable.on('click', '.cancelStockMaterialButton', function(event) {
				event.preventDefault();
				$(this).closest('.stockMatrialPanel').slideToggle('fast');
			});
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, self.props.searchParms || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('material/queryAllMaterial', searchData, '加载配件列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				showIsNewCheckBox:self.props.showIsNewCheckBox,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					materialJSONString:function() {
						return JSON.stringify(this);
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList, this.materialName);
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
			this.initStockMaterialFormEvent();
		},
		initStockMaterialFormEvent:function() {
			var self = this;
			
			$('.panelTitle', self.$chooseModal).html('添加'+self.props.title+'信息');

			$('.inputForm', self.$dataListTable).each(function(index, el) {
				Rental.form.initFormValidation($(el), function(form){
					self.confirmStockMaterial($(el));
				});
			});
		},
		confirmStockMaterial:function($form) {
			try {
				var formData = Rental.form.getFormData($form);
				this.props.callBack && this.props.callBack(formData);
				!this.props.btach && Rental.modal.close();
			} catch(e) {
				Rental.notification.error("配件备货失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		}
	};

	window.TransferOrderStockMaterial = TransferOrderStockMaterial;

})(jQuery)