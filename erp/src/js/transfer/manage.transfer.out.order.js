/**
* 流出单
*/
;(function($) {

	var TransferOutOrderManage = {
		state:{
			workflowType:null,
			transferOrderMode:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_transfer_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_transfer_order_out_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_submit],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_cancel],
				end = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_end],
				stock = this.currentPageAuthor.children[AuthorCode.manage_transfer_order_out_stock];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			stock && this.rowActionButtons.push(AuthorUtil.button(view,'stockButton','','备货'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			end && this.rowActionButtons.push(AuthorUtil.button(end,'endButton','','结束'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.orderTransferOut);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();

			this.state.workflowType = Enum.workflowType.num.transferOutOrder;
			this.state.transferOrderMode = Enum.transferOrderMode.num.out;
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据
			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#transferOrderType'),
				data:Enum.array(Enum.transferOrderType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（流转类型）'
			});

			Rental.ui.renderSelect({
				container:$('#transferOrderStatus'),
				data:Enum.array(Enum.transferOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（流转状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

			// self.renderWarehouse();
		},
		renderWarehouse:function() {
			var self = this;
			ApiData.warehouse({
				success:function(response) {
					self.renderWarehouseSelect($('#warehouseId'), '请选择流出仓库', response);
				}
			});
		},
		renderWarehouseSelect:function(container, defaultText, response) {
			console.log(response);
			var self = this;
			Rental.ui.renderSelect({
				container:container,
				data:response,
				func:function(opt, item) {
					return opt.format(item.warehouseId, item.warehouseName);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:defaultText
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				transferOrderMode:Enum.transferOrderMode.num.out,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.orderTransferOut, searchData);

			Rental.ajax.ajaxData('transferOrder/pageTransferOrder', searchData, '流出单查询', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.transferOutOrderDetail, this.transferOrderNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtonsOutOrder(self.rowActionButtons, this, 'out');
					},
					transferOrderStatusValue:function(){
						return Enum.transferOrderStatus.getValue(this.transferOrderStatus);
					},
					transferOrderStatusClass:function() {
						return Enum.transferOrderStatus.getClass(this.transferOrderStatus);
					},
					transferOrderTypeValue:function() {
						return Enum.transferOrderType.getValue(this.transferOrderType);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.TransferOutOrderManage = _.extend(TransferOutOrderManage, TransferOrderHandleMixin);

})(jQuery);