;(function($) {

	var TransferInOrderMaterialList = {
		init:function(prams) {
			this.props = _.extend({
				transferOrderMaterialId:null,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "order-transfer-manage/bulk-material-list", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$cancelButton  = $('.cancelButton', this.$modal);
			this.$dataListTpl = $(".dataListTpl", this.$modal).html();
			this.$dataListTable = $(".dataListTable", this.$modal);

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			self.$cancelButton.click(function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.searchData();
		},
		searchData:function(prams) {
			var self = this;

			if(!self.props.transferOrderMaterialId) {
				bootbox.alert('找不到流入单配件项ID');
				return;
			}

			var commitData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				transferOrderMaterialId:self.props.transferOrderMaterialId
			}, prams || {});
			
			Rental.ajax.submit("{0}transferOrder/detailTransferOrderMaterialBulkById".format(SitePath.service), commitData, function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载散料列表",response.description);
				}
			}, null ,"加载散料列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', self.$modal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			console.log(listData);

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					bulkMaterialTypeValue:function() {
						return Enum.materialType.getValue(this.bulkMaterialType);
					},
					bulkMaterialStatusValue:function() {
						return Enum.bulkMaterialStatus.getValue(this.bulkMaterialStatus);
					},
					bulkMaterialStatusClass:function() {
						return Enum.bulkMaterialStatus.getClass(this.bulkMaterialStatus);
					}
				})
			}
			console.log(this.$dataListTpl)
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.TransferInOrderMaterialList = TransferInOrderMaterialList;

})(jQuery)