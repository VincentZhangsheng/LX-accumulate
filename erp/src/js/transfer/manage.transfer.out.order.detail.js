;(function($){

	var DetailTransferOutOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
			workflowType:null,
			transferOrderMode:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_transfer_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_transfer_order_out_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_detail];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			
			this.stockProductActionButions = new Array();
			this.stockMaterialActionButions = new Array();

			this.rowProductActionuttons = new Array();
			this.rowMaterialActionuttons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_detail],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_submit],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_cancel],
				end = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_end],
				stock = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_stock],
				clearStock = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_clear_stock];

	
			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			end && this.actionButtons.push(AuthorUtil.button(end,'endButton','','结束'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));

			view && this.rowProductActionuttons.push(AuthorUtil.button(view,'viewEquimentButton','','查看设备'));
			view && this.rowMaterialActionuttons.push(AuthorUtil.button(view,'viewMaterialButton','','查看散料'));

			stock && this.stockProductActionButions.push(AuthorUtil.button(stock,'stockProductButton','','商品备货'));
			clearStock && this.stockProductActionButions.push(AuthorUtil.button(clearStock,'clearStockProductButton','','商品清货'));

			stock && this.stockMaterialActionButions.push(AuthorUtil.button(stock,'stockMaterialButton','','配件备货'));
			clearStock && this.stockMaterialActionButions.push(AuthorUtil.button(clearStock,'clearStockMaterialButton','','配件清货'));
		},
		initDom:function() {
			this.$form = $("#detailTransferOutOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
			this.state.workflowType = Enum.workflowType.num.transferInOrder;
			this.state.transferOrderMode = Enum.transferOrderMode.num.in;
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_transfer_order_out_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.loadData();

			self.initCommonEvent(); // from order mixin

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			});

			self.$form.on('click', '.stockProductButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.stockUpProduct(no);
			});
			self.$form.on('click', '.clearStockProductButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.clearStockUpProduct(no);
			});

			self.$form.on('click', '.stockMaterialButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.stockUpMaterial(no);
			});
			self.$form.on('click', '.clearStockMaterialButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.clearStockUpMaterial(no);
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到流入单编号');
				return;
			}
			Rental.ajax.submit("{0}transferOrder/detailTransferOrderByNo".format(SitePath.service),{transferOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载流入单详情",response.description || '失败');
				}
			}, null ,"加载流入单详情",'listLoading');
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderStockProductButtons(data);
			this.renderStockMaterialButtons(data);
			this.renderOrderBaserInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		renderActionButton:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtonsOutOrder(self.actionButtons, order, 'out'),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
				},
				actionButtonsTpl = $('#actionButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderStockProductButtons:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtonsOutOrder(self.stockProductActionButions, order),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
				},
				actionButtonsTpl = $('#stockUpProductButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#stockUpProductButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderStockMaterialButtons:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtonsOutOrder(self.stockMaterialActionButions, order),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
				},
				actionButtonsTpl = $('#stockUpMaterialButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#stockUpMaterialButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaserInfo:function(order) {
			$("#transferOrderNo").html(order.transferOrderNo);
			var data = _.extend(Rental.render, {
				data:order,
				transferOrderStatusValue:function(){
					return Enum.transferOrderStatus.getValue(this.transferOrderStatus);
				},
				transferOrderStatusClass:function() {
					return Enum.transferOrderStatus.getClass(this.transferOrderStatus);
				},
				transferOrderTypeValue:function() {
					return Enum.transferOrderType.getValue(this.transferOrderType);
				}
			});
			var tpl = $("#orderBaseInfoTpl").html();
			Mustache.parse(tpl);
			$('#orderBaseInfo').html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			console.log(data);
			this.state.chooseProductList = this.resolveChooseProductList(data.transferOrderProductList);
			this.renderProductList(this.filterAcitonButtonsOutOrder(this.rowProductActionuttons, data));
		},	
		initChooseMaterialList:function(order) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(order.transferOrderMaterialList);
			this.renderMaterialList(this.filterAcitonButtonsOutOrder(this.rowMaterialActionuttons, order));
		},
		stockUpProduct:function(no) {
			this.stockProductSubmit({
				title:'商品备货',
				transferOrderNo:no,
				operationType:Enum.operationType.num.stock,
			});
		},
		clearStockUpProduct:function(no) {
			this.stockProductSubmit({
				title:'商品清货',
				transferOrderNo:no,
				operationType:Enum.operationType.num.remove, 
			});
		},
		stockProductSubmit:function(prams) {
			var self = this;
			OrderPicking.init({
				title:prams.title,
				callBack:function(res) {
					var url = '';
					switch(prams.operationType) {
						case Enum.operationType.num.stock:
							url = 'transferOrder/transferOrderProductEquipmentOut';
							break;
						case Enum.operationType.num.remove:
							url = 'transferOrder/dumpTransferOrderProductEquipmentOut';
							break;
					}
					self.orderDo(url, { transferOrderNo:prams.transferOrderNo, productEquipmentNo:res.equipmentNo }, prams.title, function() {
						self.loadData();
					});
					return true;
				}
			});
		},
		stockUpMaterial:function(no) {
			this.stockMaterialSubmit( {
				title:'配件备货',
				transferOrderNo:no,
				operationType:Enum.operationType.num.stock,
			});
		},
		clearStockUpMaterial:function(no) {
			this.stockMaterialSubmit({
				title:'配件清货',
				transferOrderNo:no,
				operationType:Enum.operationType.num.remove,
			});
		},
		stockMaterialSubmit:function(prams) {
			var self = this;
			// InputModal.init({
			// 	title:prams.title,
			// 	url:'order-transfer-out-manage/stock-up-material-modal',
			// 	initFunc:function(modal) {
			// 		self.initStockMaterialModalEvent(modal);
			// 	},
			// 	callBack:function(result) {
			// 		var url = '';
			// 		switch(prams.operationType) {
			// 			case Enum.operationType.num.stock:
			// 				url = 'transferOrder/transferOrderMaterialOut';
			// 				break;
			// 			case Enum.operationType.num.remove:
			// 				url = 'dumpTransferOrderMaterialOut';
			// 				break;
			// 		}
			// 		self.orderDo(url, {
			// 			transferOrderNo:prams.transferOrderNo,
			// 			materialNo:prams.materialNo,
			// 			materialCount:result.materialCount,
			// 			isNew:result.isNew,
			// 			remark:result.remark,
			// 		}, prams.title, function() {
			// 			self.loadData();
			// 		});
			// 		return true;
			// 	}
			// });

			TransferOrderStockMaterial.init({
				title:prams.title,
				callBack:function(result) {
					console.log(result);
					var url = '';
					switch(prams.operationType) {
						case Enum.operationType.num.stock:
							url = 'transferOrder/transferOrderMaterialOut';
							break;
						case Enum.operationType.num.remove:
							url = 'transferOrder/dumpTransferOrderMaterialOut';
							break;
					}
					self.orderDo(url, {
						transferOrderNo:prams.transferOrderNo,
						materialNo:result.materialNo,
						materialCount:result.materialCount,
						isNew:result.isNew,
						remark:result.remark,
					}, prams.title, function() {
						self.loadData();
					});
					return true;
				}
			})
		},
		initStockMaterialModalEvent:function($modal) {
			$('.chooseMaterial', $modal).on('click', function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms:new Object(), 
					callBack:function(material) {
						// self.chooseMaterial(material);
						$('[name=materialNo]', $modal).val(material.materialNo);
						$('[name=materialName]', $modal).val(material.materialName);
					}
				});
			});
		}
	};

	window.DetailTransferOutOrder = _.extend(DetailTransferOutOrder, TransferOrderHandleMixin, TransferOrderMixIn);

})(jQuery);