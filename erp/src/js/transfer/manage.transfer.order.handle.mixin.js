;(function($) {

	var TransferOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack, prams) {
			var self = this;

			self.$materialDataListTable && self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:self.state.workflowType,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.cancel(no, handleCallBack);
			});

			container.on('click', '.endButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.complete(no, handleCallBack);
			});

			container.on('click', '.viewEquimentButton', function(event) {
				event.preventDefault();
				var transferOrderProductId = $(this).data('transferorderproductid');
				TransferInOrderEquimentList.init({
					transferOrderProductId:transferOrderProductId,
					callBack:function() {
						self.loadData();
					}
				});
			});

			container.on('click', '.viewMaterialButton', function(event) {
				event.preventDefault();
				var transferOrderMaterialId = $(this).data('transferordermaterialid');
				TransferInOrderMaterialList.init({
					transferOrderMaterialId:transferOrderMaterialId,
					callBack:function() {
						self.loadData();
					}
				});
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交流转单', submitUrl = 'transferOrder/commitTransferOrder';
			if(!isAudit) {
				bootbox.confirm('确认'+des+'？', function(result) {
					result && self.orderDo(submitUrl, {
						transferOrderNo:prams.no,
					}, des, callBack);
				});
				return;
			} else {
				self.orderDo(submitUrl, {
					transferOrderNo:prams.no,
					verifyUserId:prams.verifyUserId,
					remark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		orderDo:function(url, prams, des, callBack) {
			if(!prams.transferOrderNo) {
				bootbox.alert('找不到流转单编号');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		cancel:function(transferOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认取消该流转单？', function(result) {
				result && self.orderDo('transferOrder/cancelTransferOrder', {transferOrderNo:transferOrderNo}, '取消流转单', callBack);
			});
		},
		complete:function(transferOrderNo, callBack) {	
			var self = this;
			bootbox.confirm('确认结束该流转单？', function(result) {
				result && self.orderDo('transferOrder/endTransferOrder', {transferOrderNo:transferOrderNo}, '结束流转单', callBack);;
			});
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				button.no = order.transferOrderNo; //退还单编号
				rowActionButtons.push(button);
				var unSubmit = order.transferOrderStatus == Enum.transferOrderStatus.num.unSubmit,
					success = order.transferOrderStatus == Enum.transferOrderStatus.num.success;

				if(_.indexOf(['submitButton', 'editButton'], button.class) > -1 && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(button.class == 'cancelButton' && !(unSubmit || order.transferOrderStatus == Enum.transferOrderStatus.num.inReview)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				//结束操作废弃
				if(button.class == 'endButton') {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		},
		filterAcitonButtonsOutOrder:function(buttons, order) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				button.no = order.transferOrderNo; //退还单编号
				rowActionButtons.push(button);
				var unSubmit = order.transferOrderStatus == Enum.transferOrderStatus.num.unSubmit,
					stockUp = order.transferOrderStatus == Enum.transferOrderStatus.num.stockUp,	
					success = order.transferOrderStatus == Enum.transferOrderStatus.num.success;

				if(_.indexOf(['editButton','stockProductButton', 'clearStockProductButton',  'stockMaterialButton', 'clearStockMaterialButton', 'stockButton'], button.class) > -1 && !(unSubmit || stockUp)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(button.class == 'cancelButton' && !(unSubmit || stockUp || order.transferOrderStatus == Enum.transferOrderStatus.num.inReview)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'submitButton') && !stockUp) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				//结束操作废弃
				if(button.class == 'endButton') {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		}
	}

	window.TransferOrderHandleMixin = TransferOrderHandleMixin;

})(jQuery);