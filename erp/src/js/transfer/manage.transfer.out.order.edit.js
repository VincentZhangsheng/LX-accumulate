;(function($){

	var EditTransferOutOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_transfer_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_transfer_order_out_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_transfer_order_out_edit];
		},
		initDom:function() {
			this.$form = $("#editTransferOutOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_transfer_order_out_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			Rental.ui.renderSelect({
				data:Enum.arrayByEnumObj(Enum.transferOrderType, Enum.transferOrderType.out),
				container:$('[name=transferOrderType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			self.loadData();

			// self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到流入单编号');
				return;
			}
			Rental.ajax.submit("{0}transferOrder/detailTransferOrderByNo".format(SitePath.service),{transferOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载流出单详情",response.description || '失败');
				}
			}, null ,"加载流出单详情", 'listLoading');
		},
		initData:function(data) {
			this.initFormData(data);
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
	  		Rental.ui.renderFormData(self.$form, data);
		},
		edit:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					transferOrderNo:self.state.no,
					transferOrderName:formData['transferOrderName'],
					transferOrderType:formData['transferOrderType'],
					remark:formData['remark'],
					transferOrderMode:Enum.transferOrderMode.num.out,
				}

				var description = "编辑流出单";
				Rental.ajax.submit("{0}transferOrder/updateTransferOrderOut".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null , description, 'dialogLoading');
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑流入单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	// window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.EditTransferOutOrder =EditTransferOutOrder; // _.extend(EditTransferOutOrder, TransferOrderMixIn);

})(jQuery);