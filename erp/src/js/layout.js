var Layout = function() {

	function logout() {
		Rental.ajax.submit(SitePath.service + "/user/logout",{},function(response) {
			if(response.success) {
				Rental.localstorage.clearAll();
				window.location.href = SitePath.base + "login";	
				return true;
			} 
			return false;
		});
	}
	function initNavbar() {
		var user = Rental.localstorage.getUser();

		$("#navbarSpanUserName").html(user && user.realName);
		$("#userSubCompany").html(user && user.roleList[0].subCompanyName);
		$("#userDepartment").html(user && user.roleList[0].departmentName);

		$("#navbarLogout").unbind("click").bind("click",function(event){
			event.preventDefault();
			logout();
		});

		$("#navUpdatePassword").on('click', function(event) {
			event.preventDefault();
			user && UpdateUserPassword.init(user.userId);
		});

		if ($('#skin-toolbox').length) {
			$('#navBarShowSkinToolbox').on('click', function() {
	            $('#skin-toolbox').toggleClass('toolbox-open');
	         });
	         // Disable text selection
	         $('#navBarShowSkinToolbox').disableSelection();
		}
	}

	//头部未读消息提醒
	function renderMessage() {
		Rental.ajax.ajaxDataNoLoading('message/noReadCount', '', '未读信息', function(response) {
			if(response.resultMap.data > 0) {
				$("#unreadMessageNumber").html(response.resultMap.data);
			} else {
				$("#unreadMessageNumber").css('display','none');
			}
		});

		Rental.ajax.ajaxDataNoLoading('message/pageReceiveMessage', {
			pageNo:1,
			pageSize: 5,
			isRead: 0
		}, '查询收件列表', function(response) {
			var listData = response.resultMap.data.hasOwnProperty("itemList") ? response.resultMap.data.itemList : [];
			var data = _.extend(Rental.render, {
				"messageData":{
					messageCount:listData.length,
					messageList:listData
				}
			})
			var tpl = $("#navbarMessageListTpl").html();
			Mustache.parse(tpl);
			$("#navbarMessageList").html(Mustache.render(tpl,data));
		});
	};

	function initSideBar() {
		var author = Rental.localstorage.getAuthor();
		var tpl = document.getElementById("sidebarMenuTpl").innerHTML.trim();
		Mustache.parse(tpl);
		var data = {
			"menus":author,
			"icon":function() {
				return AuthorCodeIcon.hasOwnProperty(this.menuId) ?  AuthorCodeIcon[this.menuId] :"aaa";
			},
			show:function() {
				return this.menuId != AuthorCode.home;
			}
		}
        $("#sidebarMenuPosition").replaceWith(Mustache.render(tpl,data));

        //选择当前menu
        var crrentUrl = window.location.href;
        $(".authorLink").each(function(i){
        	var $this = $(this);
        	var authorUrl = $this.data("authorurl");
        	if(crrentUrl.indexOf(authorUrl) > -1) {
        		$this.parent("li").addClass("active");
        		$this.parents(".sub-nav").prev(".accordion-toggle").addClass("menu-open");
        	}
        });

        $(".authorLink").on('click', function(event) {
        	event.preventDefault();
        	Rental.searchStorage.clearAll();
        	window.location.href = $(this).attr('href');
        });
	}

	//根据权限code，展开对应的menu
	function chooseSidebarMenu(author) {
		var $menuLink = $("[author="+author+"]");
		$menuLink.parent("li").addClass("active");
        $menuLink.parents(".sub-nav").prev(".accordion-toggle").addClass("menu-open");
	}

	function initCommonEvent() {
		$('.goBack').click(function(event) {
			event.preventDefault();
			window.history.back();
		})
	}

	return {
		init:function() {
			initNavbar();
			initSideBar();
			initCommonEvent();
			renderMessage();
		},
		chooseSidebarMenu:chooseSidebarMenu,
	}

}();