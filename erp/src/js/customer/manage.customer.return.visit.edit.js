/**
 * decription: 编辑客户回访记录
 * author:zhangsheng
 * time: 2018-4-8
 */

;(function($){
	
	var EditReturnVisitRecord = {
		state:{
			dropzoneOnlyMany:'customerReturnVisitImageList',
			dropzoneOnlyManyName:'客户回访图片'
		},
		init:function(prams) {
			this.props = _.extend({
				customerNo:null,
                returnVisitId:null,
                returnListData:null,
				callBack:function() {},
			}, prams || {});
			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-manage/returnVisitModal", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			var self = this;
			this.$modal = pModal.container;
			this.$form = $("#returnVisitForm");
			this.$cancelButton = $('.cancelButton', this.$modal);
		},
		initEvent:function() {
			var self = this;

			$("#customerNo").html(self.props.customerNo);
			$("#customerName").html($("#customerHead .customerName").data("customername"));

            self.initDropzone();
            self.loadData();

			Rental.form.initFormValidation(self.$form, function(form){
				self.edit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initDropzone:function() {
			Rental.ui.events.initDropzone($('#'+this.state.dropzoneOnlyMany), 'image/upload'); 
		},
		getImageList:function(container, des) {
			var returnVisitImageList = new Array(), $preview = $('.dz-image-preview', container);
			$preview.each(function(index, el) {
				var img = $(el).data('img');
				if(img) returnVisitImageList.push({imgId:img.imgId});
			});
			if($preview.size() > 0 && $preview.size() != returnVisitImageList.length) {
				Rental.notification.error(des || '资料上传', '有未上传成功的图片，请检查');
				return false;
			}
			return returnVisitImageList;
        },
        initImgList:function(imgs, container) {
			var self = this;
			container.addClass('dz-started dz-demo');
			var tpl = $("#examplePreviewTpl").html();
			Mustache.parse(tpl);

			$.each(imgs, function(index, item) {
				self.renderImg(container, tpl, item);
			});
		},
		renderImg:function(container, tpl, item) {
			setTimeout(function() {
				var renderData = {
					returnImg:item,
					dataImg:function() {
						return JSON.stringify(this);
					}
				}
				var imgBox = Mustache.render(tpl,renderData);
				var $imgBox = $(imgBox);

                container.append($imgBox);
                $imgBox.addClass('animated fadeIn').removeClass('hidden');
				$imgBox.on('click','a.dz-remove',function(event) {
					event.preventDefault();		
					$(this).parent('.example-preview').remove();
                });
                // $("#customerReturnVisitImageList").find(".mfp-close").remove();
			},500);
        },
        intFormDataImgs:function(data) {
            var self = this;
            var imgList = data[self.state.dropzoneOnlyMany];
            if(imgList) {
                self.initImgList(imgList, $('#'+self.state.dropzoneOnlyMany));
            }
		},
        loadData:function() {
            var self = this;
            var returnVisitData = self.props.returnListData.hasOwnProperty("itemList") ? self.props.returnListData.itemList : [];
            var recordIndex = _.findIndex(returnVisitData,{returnVisitId:self.props.returnVisitId});
            Rental.ui.renderFormData(self.$form, returnVisitData[recordIndex]);
            self.intFormDataImgs(returnVisitData[recordIndex]);
		},
		edit:function() {
			try {
				var self = this;
				var formData = Rental.form.getFormData(self.$form);
				formData.returnVisitId = self.props.returnVisitId;

				if(!formData.returnVisitDescribe) {
					bootbox.alert('请填写回访描述');
					return;
				}
				
				var manyValidate = true;
				var result = self.getImageList($('#' + self.state.dropzoneOnlyMany), self.state.dropzoneOnlyManyName);
				if(_.isArray(result) && result.length > 0) {
					formData[self.state.dropzoneOnlyMany] = result;
				} else if(_.isBoolean(result)) {
					manyValidate = result;	
				}

				if(!manyValidate) {
					bootbox.alert('请检查有未上传成功的图片');
					return;
				}

				var des = '编辑客户回访';
				Rental.ajax.submit("{0}customer/updateCustomerReturnVisit".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null , des, "dialogLoading");

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.EditReturnVisitRecord = EditReturnVisitRecord;

})(jQuery);