/**
 * decription: 风控历史记录
 * author:zhangsheng
 * time: 2018-3-30
 */
;(function($) {

	riskHistory = {
		init:function(prams) {
			this.props = _.extend({
				no:null,
			}, prams);

			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.container = $("#riskHistory");
			this.$riskHistoryTpl = $("#riskHistoryTpl").html();
			this.$riskHistoryTable = $("#riskHistoryTable");
		},
		initEvent:function() {
			this.Pager = new Pager();
			this.searchData()
		},
		searchData:function() {
			var self = this;
			if(!self.props.no) {
				bootbox.alert('找不到客户编号');
				return;
			}
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				customerNo:self.props.no,
			}, self.props || {});
			Rental.ajax.ajaxDataNoLoading('customer/pageCustomerRiskManagementHistory', searchData, '查询客户风控历史记录', function(response) {
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderRiskHistory(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			}, $('.pagerContainer', self.container));
		},
		renderRiskHistory:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			$("#riskCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					dataStatusValue:function() {
						return self.getDataStatusValue(this.dataStatus);
					},
					isFullDepositValue:function() {
						return self.getRestrictValue(this.isFullDeposit);
					},
					isLimitAppleValue:function() {
						return self.getRestrictValue(this.isLimitApple);
					},
					isLimitNewValue:function() {
						return self.getRestrictValue(this.isLimitNew);
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
					applePayModeValue:function() {
						return Enum.payMode.getValue(this.applePayMode);
					},
					newPayModeValue:function() {
						return Enum.payMode.getValue(this.newPayMode);
					},
					singleLimitPriceValue:function() {
						return this.hasOwnProperty('singleLimitPrice')  && _.isNumber(this.singleLimitPrice) ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
					}
				})
			}
			Mustache.parse(self.$riskHistoryTpl);
			self.$riskHistoryTable.html(Mustache.render(self.$riskHistoryTpl, data));
		},
		getDataStatusValue:function(status) {
			switch(status) {
				case 0:
					return "不可用";
				case 1:
					return "可用";
				case 2:
					return "删除";
				default:
					return "";
			}
		},
		getRestrictValue:function(restrict) {
			switch(restrict) {
				case 0:
					return "否";
				case 1:
					return "是";
				default:
					return "";
			}
		},
	}

})(jQuery);