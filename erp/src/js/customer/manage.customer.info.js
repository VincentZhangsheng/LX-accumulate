;(function($) {

	var CustomerInfoManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_info];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) {
				return;
			}
			var auditAuthor = this.currentPageAuthor.children[AuthorCode.manage_customer_info_audit];

			if(auditAuthor) {
				this.rowActionButtons.push({
					class:"auditButton",
					status:Enum.verifyStatus.num.verified,
					text:'通过'	
				});
				this.rowActionButtons.push({
					class:"auditButton",
					status:Enum.verifyStatus.num.reject,
					text:'驳回'
				});
			}
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$customerInfoTpl = $("#customerInfoTpl").html();
			this.$customerInfoTbody = $("#customerInfoTbody");
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			this.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			this.$btnRefresh.on("click",function(){
				self.searchData();
			});


			this.$customerInfoTbody.on('click', '.open-img-popup', function(event) {
				event.preventDefault();
				var imgList = $(this).data("imgs");
				if(imgList) {
					$.magnificPopup.open({
						items:imgList,
						gallery: {
					      enabled: true
					    },
					    type: 'image'
					})
				}
				
			}); 

			this.$customerInfoTbody.on('click', '.auditButton', function(event) {
				event.preventDefault();
				var $this = $(this);
				var collegePersonInfoId = $this.data('infoid');
				var status = $this.data('status');
				bootbox.confirm("确认"+Enum.verifyStatus.getValue(status), function(result) {
				    if(!result)  return;
				    console.log(result)
					self.audit(collegePersonInfoId,status);
				});
			}); 
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}/customer/queryAllUserCollegePersonInfo".format(SitePath.service),searchData,function(response){

				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询客户列表失败",response.description);
				}
				
			}, null ,"查询客户列表");
		},
		doSarch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			Pager.init(data,this.doSarch);
		},
		renderList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				"listData":listData,
				"verifyTimeFormat":function() {
					return this.verifyTime && (new Date(this.verifyTime)).format("yyyy-MM-dd hh:mm:ss");
				},
				"validityTimeFormat":function() {
					return this.validityTime && (new Date(this.validityTime)).format("yyyy-MM-dd hh:mm:ss");
				},
				verifyStatusStr:function() {
					return Enum.verifyStatus.getValue(this.verifyStatus);
				},
				idCardFrontUrlUrl:function() {
					return this.idCardFrontUrl ? this.imgDomain + this.idCardFrontUrl : SitePath.staticManagement + '/img/no-img.png';
				},
				imgInofo:function() {
					var imgs = new Array();
					var noImgUrl = SitePath.staticManagement + '/img/no-img.png';
					imgs.push({
						src:this.idCardFrontUrl ? this.imgDomain + this.idCardFrontUrl:noImgUrl,
						title:'身份证正面'
					});
					imgs.push({
						src:this.idCardBackUrl ? this.imgDomain + this.idCardBackUrl :noImgUrl,
						title:'身份证反面'
					});
					imgs.push({
						src:this.otherIdCardUrl ? this.imgDomain + this.otherIdCardUrl : noImgUrl,
						title:'手持身份证'
					});
					imgs.push({
						src:this.studentCardFrontUrl ? this.imgDomain + this.studentCardFrontUrl :noImgUrl,
						title:'学生证正面'
					});
					imgs.push({
						src:this.studentCardBackUrl ? this.imgDomain + this.studentCardBackUrl : noImgUrl,
						title:'学生证内容页'
					});
					return JSON.stringify(imgs);
				},
				dataStatusStr:function() {
					switch(this.dataStatus){
						case 0:
							return "不可用";
						case 1:
							return "可用";
						case 2:
							return "删除";
						default:
							return "";
					}
				},
				acitonButtons:function() {
					var showBtn = this.verifyStatus == Enum.verifyStatus.num.commited;
					return {
						acitons:showBtn ? self.rowActionButtons : []
					}
				}
			}
			Mustache.parse(this.$customerInfoTpl);
			this.$customerInfoTbody.html(Mustache.render(this.$customerInfoTpl, data));
		},
		audit:function(id,status) {
			var self = this; 
			Rental.ajax.submit("{0}/customer/verifyUserCollegePersonInfo".format(SitePath.service),{collegePersonInfoId:id,verifyStatus:status},function(response){
				if(response.success) {
					Rental.notification.success(Enum.verifyStatus.getValue(status),response.description);
					self.callBackFunc(response);
				} else {
					Rental.notification.error(Enum.verifyStatus.getValue(status),response.description);
				}
			}, null ,"审核客户资料");
		},
		callBackFunc:function(response) {
			if(response.success) {
				Rental.modal.close();
				this.doSarch(Pager.pagerData.currentPage);
			}
			return false;
		},
	};

	window.CustomerInfoManage = CustomerInfoManage;

})(jQuery);