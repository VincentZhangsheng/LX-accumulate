;(function($){
	
	var EditCustomerAfterPass = {
		state:{
			no:null,
			customer:null,
		},
		init:function(prams) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_customer_edit];
		},
		initDom:function() {
			var self = this;
			this.$form = $("#editCustomerForm");
			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.initCommonEvent({
				isRender:false
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到客户编号');
				return;
			}
			Rental.ajax.submit("{0}customer/detailCustomerPerson".format(SitePath.service),{customerNo:self.state.no},function(response){
				if(response.success) {
					self.state.customer = response.resultMap.data;
					self.initFormData(response.resultMap.data);
				} else {
					Rental.notification.error("加载客户信息",response.description || '失败');
				}
			}, null ,"加载客户信息");
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$form, data);
			Rental.ui.renderFormData(self.$form, data.customerPerson);
		},
		edit:function() {
			try {
				
				var self = this;
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					customerNo:self.state.no,
					unionUser:formData['unionUser'],
					owner:formData['owner'],
				}


				Rental.ajax.submit("{0}customer/updateOwnerAndUnionUser".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success("编辑客户",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑客户",response.description || '失败');
					}
				}, null ,"编辑客户");

			} catch (e) {
				Rental.notification.error("编辑客户失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "编辑成功",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.EditCustomerAfterPass = _.extend(EditCustomerAfterPass, CustomerMixin);

})(jQuery);