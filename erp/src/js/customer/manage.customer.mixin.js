/**
* 客户公用类
*/
;(function() {
	CustomerMixin = {
		state:{
			dropzoneOnlyOne:['businessLicensePictureImage', 'legalPersonNoPictureFrontImage', 'legalPersonNoPictureBackImage','agentPersonNoPictureFrontImage','agentPersonNoPictureBackImage'],
			dropzoneOnlyOneName:['营业执照', '法人/股东身份证正面', '法人/股东身份证反','经办人身份证正面','经办人身份证反面'],
			dropzoneOnlyMany:['legalPersonCreditReportImageList', 
				'fixedAssetsProveImageList', 
				'publicAccountFlowBillImageList', 
				'socialSecurityRoProvidentFundImageList', 
				'cooperationAgreementImageList', 
				'legalPersonEducationImageList',
				'legalPersonPositionalTitleImageList',
				'localeChecklistsImageList',
				'managerPlaceRentContractImageList',
				'otherDateImageList'],
			dropzoneOnlyManyName:['法人个人征信报告或附（法人个人征信授权书）', 
				'固定资产证明', 
				'单位对公账户流水账单', 
				'公积金缴纳证明', 
				'战略协议或合作协议', 
				'法人学历证明',
				'法人职称证明',
				'现场核查表',
				'经营场所租赁合同',
				'其它材料'],
			customer:null,
			company:null,
			chooseProductList:new Array(),
			chooseLaterProductList:new Array(),
			customerConsignInfoList: new Array(),
			customerNo:null,
			order:new Object(),
			laterOrder:new Object()
		},
		initCommonEvent:function(prams) {
			prams = _.extend({
				isRender:true,
				isInitOwner:false
			}, prams || {})

			var self = this;

			Rental.ui.renderSelect({
				container:$("#statementDate"),
				data:Enum.array(Enum.settlementDate),
				func:function(opt, item) {
					return item.num == Enum.settlementDate.num.calendarMonth ? null : opt.format(item.num, item.value);
				},
				defaultText:'请选择'
			});

			Rental.ui.renderSelect({
				container:$("#deliveryMode"),
				data:Enum.array(Enum.deliveryMode),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					var $logisticsAmount = $('[name=logisticsAmount]');
					val == Enum.deliveryMode.num.pickup && $('[name=logisticsAmount]').val(0);
				},
				defaultText:'请选择'
			});

			$("#chooseUnionUserName").on('click', function(event) {
				event.preventDefault();
				ChooseUser.init({
					callBack:function(user) {
						$('[name=unionUserName]', self.$form).val(user.realName);
						$('[name=unionUser]', self.$form).val(user.userId);
					}
				});
			});

			$("#chooseOwnerName").on('click', function(event) {
				event.preventDefault();
				ChooseUser.init({
					callBack:function(user) {
						$('[name=ownerName]', self.$form).val(user.realName);
						$('[name=owner]', self.$form).val(user.userId);
					}
				});
			});

			$("#deleteOwnerName").on('click',function(event) {
				event.preventDefault();
				$('[name=ownerName]', self.$form).val("");
				$('[name=owner]', self.$form).val("");
			})

			$("#deleteUnionUserName").on('click',function(event) {
				event.preventDefault();
				$('[name=unionUserName]', self.$form).val("");
				$('[name=unionUser]', self.$form).val("");
			})

			Rental.ui.renderSelect({
				container:$("#customerOrigin"),
				data:Enum.array(Enum.customerOrigin),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				},
				defaultText:'请选择'
			});

			Rental.ui.renderSelect({
				container:$("#industry"),
				data:Enum.array(Enum.customerIndustiry),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				},
				defaultText:'请选择'
			});

			Rental.ui.events.initDatePicker($('#companyFoundTimePicker'), $("[name=companyFoundTime]", self.$form));
			
			self.initDropzone();

			if(prams.isRender) {
				self.renderProductList();
				self.renderLaterProductList();	

				Rental.ui.events.imgGallery(this.$dataListTable);
				self.$dataListTable.rtlCheckAll('checkAll','checkItem');

				$("#batchAddProduct").click(function(event) {
					ProductChoose.modal({
						showIsNewCheckBox:true,
						callBack:function(product) {
							self.chooseProduct(product);
							self.firstProductNeedPrice();
						}
					});
				});

				$("#laterAddProduct").click(function(event) {
					ProductChoose.modal({
						showIsNewCheckBox:true,
						callBack:function(product) {
							self.chooseLaterProduct(product);
							self.laterProductNeedPrice();
						}
					});
				})

				//首次所需设备
				$("#batchDeleteProduct").click(function(event) {
					self.batchDeleteProduct();
					self.firstProductNeedPrice();
				});

				self.$dataListTable.on('click', '.deleteSKUButton', function(event) {
					event.preventDefault();
					self.deleteSku($(this));
					self.renderProductList();
					self.firstProductNeedPrice();
				});

				self.$dataListTable.on('change', '.rentCount,.rentType,.rentTimeLength', function(event){
					event.preventDefault();
					var $skuRow = $(this).closest('.skuRow');
					self.changeProduct($skuRow);
					self.firstProductNeedPrice();
				})

				//后续所需设备
				$("#laterDeletProduct").click(function(event) {
					self.batchDeleteLaterProduct();
					self.laterProductNeedPrice();
				});

				self.$laterProductTable.on('click', '.deleteSKUButton', function(event) {
					event.preventDefault();
					self.deleteLaterSku($(this));
					self.renderLaterProductList();
					self.laterProductNeedPrice();
				});

				self.$laterProductTable.on('change', '.rentCount,.rentType,.rentTimeLength', function(event){
					event.preventDefault();
					var $skuRow = $(this).closest('.skuRow');
					self.changeLaterProduct($skuRow);
					self.laterProductNeedPrice();
				})
				Rental.helper.onOnlyNumber(self.$dataListTable, '.rentCount'); 
				Rental.helper.onOnlyNumber(self.$laterProductTable, '.rentCount'); 
				Rental.helper.onOnlyNumber(self.$form,'#shortLimitReceivableAmount');

				//是否由法人代表申请
				$('#isLegalPersonApple').on('change', function(event) {
					var isByLegalPerson =  parseInt($(this).val()) == 1;
					if(isByLegalPerson)  {
						$("#legalPersonContainer").hide();
					} else {
						$("#legalPersonContainer").show();
					}
				});

				/*
				* description: 添加地址
				* author: zhangsheng
				* time: 2018/03/21
				*/
				self.renderAddress();

				//新增地址
				self.$customerAddressPannel.on('click','.addAddressButton',function(event){
					event.preventDefault();
					InputAddress.init({
						initFunc:function() {
							$("#iaMainLabel").removeClass('hide')
						},
						callBack:function(address) {
							self.chooseAddress(address);
							return true;
						}
					})
				})

				//编辑地址
				self.$customerAddressPannel.on('click','.eidtAddressButton',function(event){
					event.preventDefault();
					var customerConsignInfoId = $(this).data('customerconsigninfoid');
					var guid = $(this).data('guid');
					var addressIndex = "";
					if(!customerConsignInfoId) {
						addressIndex = _.findIndex(self.state.customerConsignInfoList, {guid:guid});
					} else {
						addressIndex = _.findIndex(self.state.customerConsignInfoList, {customerConsignInfoId:customerConsignInfoId});
					}
					var ads = self.state.customerConsignInfoList[addressIndex];

					InputAddress.init({
						showIsMain:true,
						initFunc:function(modal) {
							Rental.ui.renderFormData($("form", modal), ads);
							InputAddress.Area.changeValue({
								province:ads.province,
								city:ads.city,
								district:ads.district,
							});
							$("[name=isMain]",modal).prop({'checked':ads.isMain == 1})
						},
						callBack:function(address) {
							self.state.customerConsignInfoList[addressIndex] =_.extend(ads, address);
							if(address.isMain == 1 && self.state.customerConsignInfoList.length > 0) {
								self.state.customerConsignInfoList.forEach(function(item) {
									item.isMain = 0;
								})
								self.state.customerConsignInfoList[addressIndex].isMain = 1;
							}
							self.renderAddress();

							self.renderManageAddress(self.state.customerConsignInfoList[addressIndex]);
							return true;
						}
					})
				})

				//删除地址
				self.$customerAddressPannel.on('click','.deleteAddressButton',function(event){
					event.preventDefault();
					var customerConsignInfoId = $(this).data('customerconsigninfoid');
					var guid = $(this).data('guid');
					var addressIndex = "";
					if(!customerConsignInfoId) {
						addressIndex = _.findIndex(self.state.customerConsignInfoList, {guid:guid});
					} else {
						addressIndex = _.findIndex(self.state.customerConsignInfoList, {customerConsignInfoId:customerConsignInfoId});
					}

					if(self.state.customerConsignInfoList[addressIndex].isBusinessAddress == 1) {
						var data = {
							isDefaultConsignAddress:0
						}
						Rental.ui.renderFormData(self.$form, data);
					}
					self.state.customerConsignInfoList.splice(addressIndex,1);
					self.renderAddress();
				})

				//将经营地址设为收货地址？
				self.$form.on('change','#isDefaultConsignAddress,#connectRealName,#connectPhone,#province,#city,#district,#address',function(){
					self.renderDefaultAddress();
				})
			}

			if(prams.isInitOwner) {
				self.renderOwner();
			}
		},
		renderOwner:function() {
			var user = Rental.localstorage.getUser();
			var ownerInfo = {
				ownerName:user.realName,
				owner:user.userId
			}
			Rental.ui.renderFormData(this.$form, ownerInfo);
		},
		firstProductNeedPrice:function(){
			var self = this;
			var firstLimit = 0;
			self.$dataListTable.find(".totalPrice").each(function() {
				firstLimit += parseFloat($(this).find(".num").text());
			})
			firstLimit = firstLimit.toFixed(2);
			$("#firstApplyAmount").val(firstLimit);
			$("#firstListTotalPrice").val(firstLimit);
			$("#firstListTotalPriceBox").html("￥" + firstLimit);
		},
		laterProductNeedPrice:function() {
			var self = this;
			var laterLimit = 0;
			self.$laterProductTable.find(".totalPrice").each(function() {
				laterLimit += parseFloat($(this).find(".num").text());
			})
			laterLimit = laterLimit.toFixed(2);
			$("#laterApplyAmount").val(laterLimit);
			$("#laterListTotalPrice").val(laterLimit);
			$("#laterListTotalPriceBox").html("￥" + laterLimit);
		},
		getCustomerSkuBySkuRow:function($skuRow) {
			return {
				skuId:parseInt($('.productSkuId', $skuRow).val()),
				productId:parseInt($.trim($('.productId', $skuRow).val())),
				rentCount:$.trim($('.rentCount', $skuRow).val()),
				isNew:parseInt($('.isNewProduct', $skuRow).val()),
				rentType:parseInt($('.rentType', $skuRow).val()),
				rentTimeLength: $.trim($('.rentTimeLength', $skuRow).val())
			};
		},
		changeCustomerProduct:function(chooseProductList, $skuRow) {
			var sku = this.getCustomerSkuBySkuRow($skuRow);
			var productIndex = _.findIndex(chooseProductList, {productId:sku.productId});
			var skuIndex = _.findIndex(chooseProductList[productIndex].chooseProductSkuList, {skuId:sku.skuId, isNew:sku.isNew});
			chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(chooseProductList[productIndex].chooseProductSkuList[skuIndex], sku);
			return chooseProductList;
		},
		initDropzone:function() {
			this.state.dropzoneOnlyOne.forEach(function(item) {
				Rental.ui.events.initDropzone($('#'+item), 'image/upload',1);
			});
			this.state.dropzoneOnlyMany.forEach(function(item) {
				Rental.ui.events.initDropzone($('#'+item), 'image/upload');
			}); 
		},
		getImageList:function(container, des) {
			var productImgList = new Array(), $preview = $('.dz-image-preview', container);
			$preview.each(function(index, el) {
				var img = $(el).data('img');
				if(img) productImgList.push({imgId:img.imgId});
			});
			if($preview.size() > 0 && $preview.size() != productImgList.length) {
				Rental.notification.error(des || '资料上传', '有未上传成功的图片，请检查');
				return false;
			}
			return productImgList;
		},
		initImgList:function(imgs, container) {
			var self = this;
			container.addClass('dz-started dz-demo');
			var tpl = $("#examplePreviewTpl").html();
			Mustache.parse(tpl);

			$.each(imgs, function(index, item) {
				self.renderImg(container, tpl, item);
			});
		},
		initImg:function(img, container) {
			var self = this;
			container.addClass('dz-started dz-demo');
			var tpl = $("#examplePreviewTpl").html();
			Mustache.parse(tpl);
			self.renderImg(container, tpl, img)
		},
		renderImg:function(container, tpl, item) {
			setTimeout(function() {
				var renderData = {
					productImg:item,
					dataImg:function() {
						return JSON.stringify(this);
					}
				}
				var imgBox = Mustache.render(tpl,renderData);
				var $imgBox = $(imgBox);

				container.append($imgBox);
				$imgBox.addClass('animated fadeIn').removeClass('hidden');
				$imgBox.on('click','a.dz-remove',function(event) {
					event.preventDefault();		
					$(this).parent('.example-preview').remove();
				});

				Rental.ui.events.initViewer()
			},500);
		},
		renderProductList:function(){
			OrderManageItemRender.renderProductList(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable, null, this.state.order);
		},
		renderLaterProductList:function() {
			OrderManageItemRender.renderProductList(this.state.chooseLaterProductList, this.$laterProductTpl, this.$laterProductTable, null, this.state.laterOrder);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = OrderManageUtil.chooseProductByIsNew(this.state.chooseProductList, product);
			this.renderProductList();
			this.chooseProductConfirm();
		},
		chooseLaterProduct:function(product) {
			this.state.chooseLaterProductList = OrderManageUtil.chooseProductByIsNew(this.state.chooseLaterProductList, product);
			this.renderLaterProductList();
			this.chooseProductConfirm();
		},
		chooseProductConfirm:function() {
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		changeProduct:function($skuRow) {
			this.state.chooseProductList = this.changeCustomerProduct(this.state.chooseProductList, $skuRow);
			this.renderProductList();
		},
		changeLaterProduct:function($skuRow) {
			this.state.chooseLaterProductList = this.changeCustomerProduct(this.state.chooseLaterProductList, $skuRow);
			this.renderLaterProductList();
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = OrderManageUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			this.renderProductList();	
		},
		batchDeleteLaterProduct:function() {
			this.state.chooseLaterProductList = OrderManageUtil.batchDelete(this.$laterProductTable, this.state.chooseLaterProductList);
			this.renderLaterProductList();	
		},
		deleteSku:function($deleteButton) {
			this.state.chooseProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseProductList);
			this.renderProductList();
		},
		deleteLaterSku:function($deleteButton) {
			this.state.chooseLaterProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseLaterProductList);
			this.renderLaterProductList();
		},
		/*
		* description: 添加地址
		* author: zhangsheng
		* time: 2018/03/21
		*/
		renderAddress:function() {
			var self = this;
			var data = {
				dataSource:{
					listData:self.state.customerConsignInfoList || [],
					addressJson:function() {
						return JSON.stringify(this);
					},
					isDefault:function() {
						return this.isMain == 1 ? true : false;
					},
					isBusiness: function() {
						return this.isBusinessAddress == 1 ? true : false;
					},
					verifyStatusValue:function() {
						return Enum.customerConsignInfoVerifyStatus.getValue(this.verifyStatus);
					},
					editButton:function() {
						return (this.verifyStatus == Enum.customerConsignInfoVerifyStatus.num.unSubmit && this.isBusinessAddress != 1);
					},
				}
			}
			var tpl = $("#customerAddressTpl").html();
			Mustache.parse(tpl);
			$("#customerAddressPannel").html(Mustache.render(tpl, data));
		},
		chooseAddress:function(address) {
			address = _.extend(address, {
				guid:Rental.helper.guid(),
				isBusinessAddress:0,
				verifyStatus:0,
			})
			if(address.isMain == 1 && this.state.customerConsignInfoList.length > 0) {
				this.state.customerConsignInfoList.forEach(function(item) {
					item.isMain = 0;
				})
			}
			this.state.customerConsignInfoList.push(address);
			this.renderAddress();
		},
		//经营地址与收货地址一致？
		renderDefaultAddress:function() {
			var self = this;
			var isDefaultAddress = parseInt($('#isDefaultConsignAddress').val()) == 1;
			try{
				var formData = Rental.form.getFormData(self.$form);
				if(isDefaultAddress) {
					// if($("option",$("#city")).length <= 1) {
					// 	if(!formData['connectRealName'] || !formData['connectPhone'] || !formData['province']  || !formData['address']) return;
					// } else if($("option",$("#district")).length <= 1) {
					// 	if(!formData['connectRealName'] || !formData['connectPhone'] || !formData['province']  || !formData['city'] || !formData['address']) return;
					// } else {
					// 	if(!formData['connectRealName'] || !formData['connectPhone'] || !formData['province']  || !formData['city'] || !formData['district'] || !formData['address']) return;
					// }

					if(!formData['connectRealName'] || !formData['connectPhone'] || !formData['province'] || !formData['address']) return;

					formData.provinceName = $('option[value='+formData['province']+']', $('[name=province]', this.$form)).html();
					formData.cityName =formData['city'] ?  $('option[value='+formData['city']+']', $('[name=city]', this.$form)).html() : "";
					formData.districtName = formData['district'] ? $('option[value='+formData['district']+']', $('[name=district]', this.$form)).html() : "";
					
					var address = {
						guid:Rental.helper.guid(),
						consigneeName:formData['connectRealName'],
						consigneePhone:formData['connectPhone'],
						province:formData['province'],
						provinceName:formData['provinceName'],
						city:formData['city'],
						cityName:formData['cityName'],
						district:formData['district'],
						districtName:formData['districtName'],
						address:formData['address'],
						isMain:0,
						isBusinessAddress:1,
						verifyStatus:0,
					}

					var addressIndex = _.findIndex(self.state.customerConsignInfoList, {isBusinessAddress:1});
					if(addressIndex >= 0) {
						address.isMain = self.state.customerConsignInfoList[addressIndex].isMain;
						self.state.customerConsignInfoList[addressIndex] = address;
					} else {
						self.state.customerConsignInfoList.push(address);
					}
					self.renderAddress();
				} else {
					var addressIndex = _.findIndex(self.state.customerConsignInfoList, {isBusinessAddress:1});
					if(!(addressIndex >= 0)) return;
					self.state.customerConsignInfoList.splice(addressIndex,1);
					self.renderAddress();
				}
			} catch(e) {}
		},
		//编辑收货地址，与经营地址一致时，同时改变经营地址
		// renderManageAddress:function(address) {
		// 	if(address.isBusinessAddress != 1) return;
		// 	var connectRealName = address.consigneeName;
		// 	var consigneePhone = address.consigneePhone;
		// 	address = _.extend(address, {
		// 		connectRealName:connectRealName,
		// 		connectPhone:consigneePhone,
		// 	})
		// 	Rental.ui.renderFormData(this.$form, address);

		// 	this.Area.changeValue({
		// 		province:address.province,
		// 		city:address.city,
		// 		district:address.district,
		// 	});
		// }
	}
	window.CustomerMixin = CustomerMixin;

})(jQuery);