;(function($){
	var CustomerDetail = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list_for400];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_customer_list_detail_for400];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			this.$form = $("#customerDetail");
			this.$customerRiskPanel = $("#customerRiskPanel");
			this.$riskHistoryTpl = $("#riskHistoryTpl").html();
			this.$riskHistoryTable = $("#riskHistoryTable");			

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_customer_list_for400); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.loadRentingInfo();	

			self.initAddressInfo();

			CustomerOrders.init({no:this.state.no});
			riskHistory.init({no:this.state.no});
			CustomerOrders.couponList.init({customerNo:self.state.no})
		},
		initAddressInfo:function() {
			var self = this, 
				addAddress = this.currentManageListAuthor.children[AuthorCode.manage_customer_address_add];

			CustomerAddressManage.init({
				addAuthor:addAddress,
				editAuthor:addAddress,
				deleteAuthor:addAddress,
				auditAuthor:addAddress,
				customerNo:self.state.no,
				isCustomer:true,
				callBack:function() {
					self.loadData();
				}
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				 bootbox.alert("没找到客户编号");
				 return;
			}
			Rental.ajax.submit("{0}/customer/detailCustomerPerson".format(SitePath.service),{customerNo:self.state.no},function(response){
				if(response.success) {
					self.state.customer = response.resultMap.data;
					self.render(response.resultMap.data);
				} else {
					Rental.notification.error("查询客户详细信息",response.description ||  '失败');
				}
			}, null ,"查询客户详细信息", 'listLoading');
		},
		render:function(customer) {
			this.renderCustomerHead(customer);
			this.renderCustomerRiskManagement(customer);
		},
		renderCustomerHead:function(customer) {
			this.renderCustomer(customer,  $("#customerHeadTpl").html(), $('#customerHead'));
		},
		renderCustomer:function(customer, tpl, container) {
			var self = this;
			Mustache.parse(tpl);
			var data = _.extend(Rental.render, {
				customer:customer,
				avatar:function() {
					return '';
				},
				isBadDebtCustomer:function() {
					return this.confirmBadAccountStatus == 1;
				},
				"cutomerTypeValue":function() {
					return Enum.customerType.getValue(this.customerType);
				},
				isDisabledValue:function() {
					return this.isDisabled == 0 ? "否" : "是";
				},
				customerStatusValue:function() {
					return Enum.customerStatus.getValue(this.customerStatus);
				},
				settlementDateValue:function() {
					return this.hasOwnProperty('statementDate')  && !!this.statementDate ? Enum.settlementDate.getValue(this.statementDate) : '月末最后一天';
				},
				shortLimitReceivableAmountValue:function() {
					return this.hasOwnProperty('shortLimitReceivableAmount')  && !!this.shortLimitReceivableAmount ? '￥'+parseFloat(this.shortLimitReceivableAmount).toFixed(2) : '无上限';
				}
			})
			container.html(Mustache.render(tpl, data));
		},
		renderCustomerRiskManagement:function(customer) {
			var self = this;
			var data = _.extend(Rental.render, {
				customerRiskManagement:customer.customerRiskManagement || new Object(),
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				applePayModeValue:function() {
					return Enum.payMode.getValue(this.applePayMode);
				},
				newPayModeValue:function() {
					return Enum.payMode.getValue(this.newPayMode);
				},
				customerno:function() {
					return customer.customerNo;
				},
				customerType:function() {
					return customer.customerType;
				},
				riskButton:function() {
					var risk = self.currentManageListAuthor.children[AuthorCode.manage_customer_risk];
					return risk ? AuthorUtil.button(risk,'riskButton','','风控授信') :  new Object();
				},
				singleLimitPriceValue:function() {
					return this.hasOwnProperty('singleLimitPrice')  && _.isNumber(this.singleLimitPrice) ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
				}
			})

			var customerStatusTpl = $("#customerRiskPanelTpl").html();
			Mustache.parse(customerStatusTpl);
			this.$customerRiskPanel.html(Mustache.render(customerStatusTpl, data));
		},
		loadRentingInfo:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('customer/queryRentCountByCustomerNo', {customerNo:self.state.no}, '查询客户在租信息', function(response) {
				self.renderRentingInfo(response.resultMap.data);
			});
		},
		renderRentingInfo:function(rentingData) {
			var self = this;
			var rentingInfoTpl = $("#rentingInfoTpl").html();
			var rentingData = (rentingData.hasOwnProperty("rentProductCount") || rentingData.hasOwnProperty("rentMaterialCount") || rentingData.hasOwnProperty("rentAmountMaterialCount")) ? rentingData : [];
			var data = {
				rentingData: rentingData
			}
			Mustache.parse(rentingInfoTpl);
			$("#rentingInfoTable").html(Mustache.render(rentingInfoTpl, data));
		},
	};
	
	window.CustomerDetail = CustomerDetail;

})(jQuery);