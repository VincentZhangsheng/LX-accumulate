;(function($) {

	var CustomerManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list_for400];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.customer);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			this.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			// self.renderList({});

			self.$dataListTable.on('click', '.viewWorkFlowButton', function(event) {
				event.preventDefault();
				var customer = $(this).closest('tr').data('rowdata');
				ViewWorkFlow.init({
					workflowType:Enum.workflowType.num.customer,
					workflowReferNo:customer.customerNo,
				});
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.customer, searchData);

			if(!searchData.customerNo && !searchData.phone && !searchData.realName) {
				self.renderList({})
			} else {
				Rental.ajax.ajaxData('customer/pageCustomerPerson', searchData, '查询个人客户', function(response) {
					self.renderList(response.resultMap.data);	
				});
			}
		},
		// doSearch:function(pageNo) {
		// 	this.searchData({pageNo:pageNo || 1});
		// },
		// render:function(data) {
			// var self = this;
			// self.renderList(data);
			// self.Pager.init(data,function(pageNo) {
			// 	self.doSearch(pageNo);
			// });
		// },
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					isBadDebtCustomer:function() {
						return this.confirmBadAccountStatus == 1;
					},
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.serviceCustomerDetail, this.customerNo);
					},
					rowData:function() {
						return JSON.stringify(this);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"cutomerTypeValue":function() {
						return Enum.customerType.getValue(this.customerType);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					addressForamt:function() {
						return (this.customerPerson.provinceName || '') + (this.customerPerson.cityName  || '') + (this.customerPerson.districtName  || '') + (this.customerPerson.address  || '');
					},
					customerStatusValue:function() {
						return Enum.customerStatus.getValue(this.customerStatus);
					},
					customerStatusClass:function() {
						return Enum.customerStatus.getClass(this.customerStatus);	
					},
					auditReason:function() {
						if(this.customerStatus == Enum.customerStatus.pass) {
							return this.passReason;
						} else if(this.customerStatus == Enum.customerStatus.reject) {
							return this.failReason;
						} else {
							return  "";
						}
					},
					isRiskValue:function() {
						return Enum.riskStatus.getValue(this.isRisk);
					},
					isRiskClass:function() {
						return this.isRisk == 1 ? 'text-success':'text-danger';
					},
					showViewWorkFlowButton:function() {
						return this.customerStatus == Enum.customerStatus.num.commited || this.customerStatus == Enum.customerStatus.num.reject;
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.CustomerManage = CustomerManage;

})(jQuery);