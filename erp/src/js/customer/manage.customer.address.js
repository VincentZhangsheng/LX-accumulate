;(function($) {

	CustomerAddressManage = {
		init:function(prams) {
			this.props = _.extend({
				customerNo:'',
				addAuthor:null,
				editAuthor:null,
				deleteAuthor:null,
				auditAuthor:null,
				checkbox:false,
				isCustomer:false,
				callBack:function() {},
				renderComplete:function() {}
			}, prams || {});

			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.$dataListTpl = $('#customerAddressTpl').html();
			this.$dataListTable = $('#customerAddressPannel');
		},
		initEvent:function() {
			var self = this;

			self.searchData(); 

			self.$dataListTable.on('click', '.eidtAddressButton', function(event) {
				event.preventDefault();
				self.bindButtonEvent($(this), EditCustomerAddress);
			});	

			$("#customerAddressPannel,#actionButtons").on('click', '.addAddressButton', function(event) {
				event.preventDefault();
				self.bindButtonEvent($(this), AddCustomerAddress);
			});	

			self.$dataListTable.on('click', '.deleteAddressButton', function(event) {
				event.preventDefault();
				var customerConsignInfoId = $(this).data('customerconsigninfoid');
				bootbox.confirm('确认删除该地址？', function(result) {
					result && self.del(customerConsignInfoId);
				});
			});	

			self.$dataListTable.on('click', '.auditAddressButton', function(event) {
				event.preventDefault();
				var customerConsignInfoId = $(this).data('customerconsigninfoid');
				
				ApiData.isNeedVerify({
					no:customerConsignInfoId,
					workflowType:Enum.workflowType.num.customerConsignInfo, //parseInt($(this).data('ownersubcompanyid')) == Enum.subCompany.num.telemarketing ?  Enum.workflowType.num.customerOwnerByTelSeller : Enum.workflowType.num.customer,
					verifyAttachment:false,
				}, function(result, isAudit) {
					// result.customerConsignInfoId = customerConsignInfoId;
					self.submitAudit(result, isAudit, function(){
						self.searchData(); 
					});
				})
			});	

			self.$dataListTable.on('click', '.viewAddressWorkFlowButton', function(event) {
				event.preventDefault();
				var customerConsignInfoId = $(this).data('customerconsigninfoid');
				ViewWorkFlow.init({
					workflowType:Enum.workflowType.num.customerConsignInfo,
					workflowReferNo:customerConsignInfoId,
				});
			});	
		},
		bindButtonEvent:function($this, eventClass) {
			var self = this, customerConsignInfoId = $this.data('customerconsigninfoid');
			eventClass.init({
				customerConsignInfoId:customerConsignInfoId || null,
				customerNo:self.props.customerNo,
				callBack:function(res) {
					self.searchData();
					self.props.callBack && self.props.callBack(res);
				},
			});
		},
		searchData:function(callBack) {
			var self = this;
			var searchData = {
				pageNo:1,
				pageSize:10000,
				customerNo:self.props.customerNo,
			};
			Rental.ajax.ajaxDataNoLoading('customer/pageCustomerConsignInfo', searchData, '加载客户收货地址信息', function(response) {
				self.renderList(response.resultMap.data);
				callBack && callBack();	
			});
		},
		renderList:function(data) {
			var self = this;
			var addressList = [];
			if(self.props.isCustomer == true && !!data.itemList && data.itemList.length > 0) {
				for(var i=0; i<data.itemList.length; i+=3) {
					addressList.push({"addressGroup":data.itemList.slice(i, i+3)});
				}
			} else if(!!data.itemList) {
				addressList = data.itemList;
			}

			var data = {
				dataSource:{
					listData:addressList,
					addressJson:function() {
						return JSON.stringify(this);
					},
					addButton:function() {
						return self.props.addAuthor ? AuthorUtil.button(self.props.addAuthor,'addAddressButton','','添加') : null;
					},
					editButton:function() {
						return self.props.editAuthor && (this.verifyStatus == Enum.customerConsignInfoVerifyStatus.num.unSubmit || this.verifyStatus == Enum.customerConsignInfoVerifyStatus.num.reject) ? AuthorUtil.button(self.props.editAuthor,'eidtAddressButton','','编辑') : null;
					},
					deleteAuthor:function() {
						return self.props.deleteAuthor ? AuthorUtil.button(self.props.deleteAuthor,'deleteAddressButton','','删除') : null;
					},
					auditAuthor:function() {
						// return self.props.auditAuthor && (this.verifyStatus == Enum.customerConsignInfoVerifyStatus.num.unSubmit || this.verifyStatus == Enum.customerConsignInfoVerifyStatus.num.reject) ? AuthorUtil.button(self.props.auditAuthor,'auditAddressButton','','提交审核') : null;
						return null;
					},
					viewWorkFlowAuthor:function() {
						// return self.props.auditAuthor && (this.verifyStatus != Enum.customerConsignInfoVerifyStatus.num.unSubmit) && this.workflowType == Enum.workflowType.num.customerConsignInfo  ? AuthorUtil.button(self.props.auditAuthor,'viewAddressWorkFlowButton','','查看审核流') : null;
						return null;
					},
					isDefault:function() {
						return this.isMain == 1 ? true : false;
					},
					checkbox:function() {
						return self.props.checkbox;
					},
					customerno:function() {
						return self.props.customerNo;
					},
					verifyStatusValue:function() {
						return Enum.customerConsignInfoVerifyStatus.getValue(this.verifyStatus);
					},
					verifyStatusClass:function() {
						return Enum.customerConsignInfoVerifyStatus.getClass(this.verifyStatus);
					}
				}
			}
			Mustache.parse(self.$dataListTpl);
			self.$dataListTable.html(Mustache.render(self.$dataListTpl, data));
			self.props.renderComplete && self.props.renderComplete();

			self.highlightAddressPanel();
		},
		highlightAddressPanel:function() {
			var consigninfoid = Rental.helper.getUrlPara('consigninfoid');
			if(!!consigninfoid) $('[data-consigninfoid='+consigninfoid+']').addClass('well-consigninfoid');
		},
		do:function(url, prams, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		del:function(customerConsignInfoId) {
			var self = this;
			if(!customerConsignInfoId) {
				bootbox.alert("没找到地址ID");
				return;
			}
			Rental.ajax.submit("{0}customer/deleteCustomerConsignInfo".format(SitePath.service), {customerConsignInfoId:customerConsignInfoId}, function(response){
				if(response.success) {
					Rental.notification.success("删除收货地址", response.description || '成功');
					self.searchData();
					self.props.callBack && self.props.callBack(response);
				} else {
					Rental.notification.error("删除收货地址", response.description || '失败');
				}
			}, null, "删除收货地址", 'dialogLoading');
		},
		submitAudit:function(prams, isAudit, callBack) {
			var self = this;
			if(isAudit) {
				self.do('customer/commitCustomerConsignInfo', { 
					customerConsignId:prams.no, 
					verifyUserId:prams.verifyUserId,
					remark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, '提交审核', function() {
					Rental.modal.close();
					callBack && callBack();
				});	
			} else {
				bootbox.confirm('确认提交审核？', function(result) {
					if(result) {
						self.do('customer/commitCustomerConsignInfo', { customerConsignId:prams.customerConsignId }, '提交审核', function() {
							callBack && callBack();
						});	
					}
				});
			}
		},
	}

	window.CustomerAddressManage = CustomerAddressManage;

})(jQuery);