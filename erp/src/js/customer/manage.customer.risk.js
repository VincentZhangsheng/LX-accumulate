;(function($){
	
	var RiskCustomer = {
		init:function(prams) {
			this.props = _.extend({
				customerType:null,
				customerNo:null,
				callBack:function() {},
			}, prams || {});
			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-manage/riskModal", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			var self = this;
			this.$modal = pModal.container;
			this.$form = $("#riskCustomerForm");
			this.$cancelButton = $('.cancelButton', this.$modal);
		},
		initEvent:function() {
			var self = this;

			self.loadData();

			Rental.form.initFormValidation(this.$form, function(form){
				self.edit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.renderPayModeSelect($('[name=payMode]', self.$modal));
			self.renderPayModeSelect($('[name=newPayMode]', self.$modal));
			self.renderPayModeSelect($('[name=applePayMode]', self.$modal));

			//全额押金
			self.$modal.on('change', '[name=isFullDeposit]', function(event) {
				event.preventDefault();
				self.changeRequired(parseInt($(this).val()) == 0, '.requiredWithFullDeposit');
			});

			self.$modal.on('change', '[name=isLimitApple]', function(event) {
				event.preventDefault();
				self.changeRequired(parseInt($(this).val()) == 0, '.appleControl');
			});

			self.$modal.on('change', '[name=isLimitNew]', function(event) {
				event.preventDefault();
				self.changeRequired(parseInt($(this).val()) == 0, '.newDepositControl');
			});

			Rental.helper.onOnlyNumber(self.$modal, '.checkInteger'); 
		},
		changeRequired:function(isReuired, className) {
			if(isReuired) {
				$(className, self.$modal).addClass('required');	
			} else {
				$(className, self.$modal).removeClass('required');	
			}
		},
		renderPayModeSelect:function(container) {
			Rental.ui.renderSelect({
				data:Enum.array(Enum.payMode),
				container:container,
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				}
			});
		},
		loadData:function() {
			var self = this;
			if(!self.props.customerNo) {
				bootbox.alert('没找到客户编号');
				return;
			}

			if(!self.props.customerType) {
				bootbox.alert('没找用户类型');
				return;
			}

			var url = null;
			switch(parseInt(self.props.customerType)) {
				case Enum.customerType.num.business:
					url = "customer/detailCustomerCompany";
					break;
				case Enum.customerType.num.personal:
					url = "customer/detailCustomerPerson";
					break;
				default:
					url = '';
					break;
			}

			var des = '查询客户详细信息';
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), { customerNo:self.props.customerNo }, function(response){
				if(response.success) {
					self.initFormData(response.resultMap.data);
				} else {
					Rental.notification.error(des, response.description || '失败');
				}
			}, null, des, 'listLoading');
		},
		initFormData:function(data) {
			if(!data) return;
			if(!data.hasOwnProperty('customerRiskManagement')) return;
	        Rental.ui.renderFormData(this.$form, data.customerRiskManagement);
	       
	       	var isFullDeposit = data.customerRiskManagement.isFullDeposit == 1;
	       	if(isFullDeposit) {
	       		this.changeRequired(data.customerRiskManagement.isFullDeposit != 1, '.requiredWithFullDeposit');
	       	} else {
	       		this.changeRequired(data.customerRiskManagement.isLimitApple != 1, '.appleControl');
	        	this.changeRequired(data.customerRiskManagement.isLimitNew != 1, '.newDepositControl');
	       	}
		},
		edit:function() {
			try {
				var self = this;
				if(!self.props.customerNo) {
					bootbox.alert('没找到客户编号');
					return;
				}
				var formData = Rental.form.getFormData(self.$form);
				formData.customerNo = self.props.customerNo;

				var des = '风控授信';
				Rental.ajax.submit("{0}customer/updateRisk".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null , des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.RiskCustomer = RiskCustomer;

})(jQuery);