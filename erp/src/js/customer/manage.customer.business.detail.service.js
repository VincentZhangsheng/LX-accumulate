;(function($){
	var BusinessCustomerDetail = {
		state:{
			no:null,
			customer:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_business_customer_list_for400];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_detail_list_for400];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			this.$form = $("#customerDetailForm");
			this.$customerRiskPanel = $("#customerRiskPanel");
			this.$productFirstListTpl = $("#productFirstNeedTpl").html();
			this.$productFirstListTable = $("#productFirstNeedTable");
			this.$productLaterListTpl = $("#productLaterNeedTpl").html();
			this.$productLaterListTable = $("#productLaterNeedTable");
			this.$customerAddressPannel = $('#customerAddressPannel');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_business_customer_list_for400); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.loadRentingInfo();	
			self.initAddressInfo();

			CustomerOrders.init({no:this.state.no});
			riskHistory.init({no:this.state.no});
			ReturnVisitRecordManage.init({no:this.state.no,customer:this.state.customer});
			CustomerOrders.couponList.init({customerNo:self.state.no})
		},
		initAddressInfo:function() {
			var self = this, 
				addAddress = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_address_add];
			CustomerAddressManage.init({
				addAuthor:addAddress,
				editAuthor:addAddress,
				deleteAuthor:addAddress,
				auditAuthor:addAddress,
				customerNo:self.state.no,
				isCustomer:true,
				callBack:function() {
					self.loadData();
				}
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert("没找到客户编号");
				return;
			}
			Rental.ajax.ajaxData('customer/detailCustomerCompany', {customerNo:self.state.no}, '查询客户详细信息', function(response) {
				self.state.customer = response.resultMap.data;
				self.render(response.resultMap.data);
				self.renderProductFirstNeed(response.resultMap.data.customerCompany);
				self.renderProductLaterNeed(response.resultMap.data.customerCompany);
				self.deliveryModeValue(response.resultMap.data);
			});
		},
		render:function(customer) {
			this.renderCustomerHead(customer);
			this.renderCustomerRiskManagement(customer);
			this.renderCustomerCompany(customer);
		},
		renderCustomerHead:function(customer) {
			this.renderCustomer(customer,  $("#customerHeadTpl").html(), $('#customerHead'))
		},
		renderCustomerCompany:function(customer) {
			this.renderCustomer(customer,  $("#customerCompanyInfoTpl").html(), $('#customerCompanyInfo'))
		},
		renderCustomer:function(customer, tpl, container) {
			var self = this;
			Mustache.parse(tpl);
			var data = _.extend(Rental.render, {
				customer:customer,
				avatar:function() {
					return '';
				},
				isBadDebtCustomer:function() {
					return this.confirmBadAccountStatus == 1;
				},
				"cutomerTypeValue":function() {
					return Enum.customerType.getValue(this.customerType);
				},
				isDisabledValue:function() {
					return this.isDisabled == 0 ? "否" : "是";
				},
				customerOriginValue:function() {
					return Enum.customerOrigin.getValue(this.customerCompany.customerOrigin);
				},
				customerStatusValue:function() {
					return Enum.customerStatus.getValue(this.customerStatus);
				},
				customerIndustryValue:function() {
					return function(val, render) {
			            return render(val) ? Enum.customerIndustiry.getValue(parseInt(render(val))) : '';
			        } 
				},
				isDefaultConsignAddressValue:function() {
					return this.isDefaultConsignAddress == 0 ? "否" : "是";
				},
				settlementDateValue:function() {
					return this.hasOwnProperty('statementDate')  && !!this.statementDate ? Enum.settlementDate.getValue(this.statementDate) : '月末最后一天';
				},
				shortLimitReceivableAmountValue:function() {
					return this.hasOwnProperty('shortLimitReceivableAmount')  && !!this.shortLimitReceivableAmount ? '￥'+parseFloat(this.shortLimitReceivableAmount).toFixed(2) : '无上限';
				}
			})
			container.html(Mustache.render(tpl, data));
		},
		renderCustomerRiskManagement:function(customer) {
			var self = this;
			var data = _.extend(Rental.render, {
				customerRiskManagement:customer.customerRiskManagement || new Object(),
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				applePayModeValue:function() {
					return Enum.payMode.getValue(this.applePayMode);
				},
				newPayModeValue:function() {
					return Enum.payMode.getValue(this.newPayMode);
				},
				customerno:function() {
					return customer.customerNo;
				},
				customerType:function() {
					return customer.customerType;
				},
				riskButton:function() {
					var risk = self.currentManageListAuthor.children[AuthorCode.manage_business_customer_risk];
					return risk ? AuthorUtil.button(risk,'riskButton','','风控授信') :  new Object();
				},
				singleLimitPriceValue:function() {
					return this.hasOwnProperty('singleLimitPrice')  && _.isNumber(this.singleLimitPrice) ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
				}
			})
			var customerStatusTpl = $("#customerRiskPanelTpl").html();
			Mustache.parse(customerStatusTpl);
			this.$customerRiskPanel.html(Mustache.render(customerStatusTpl, data));
		},
		renderCustomerAddress:function(customer) {
			var customerAddressTpl = $("#customerAddressTpl").html();
			Mustache.parse(customerAddressTpl);
			var data = {
				userConsignInfoList:customer.userConsignInfoList,
			}
			$('#customerAddress').html(Mustache.render(customerAddressTpl, data));
		},
		loadRentingInfo:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('customer/queryRentCountByCustomerNo', {customerNo:self.state.no}, '查询客户在租信息', function(response) {
				self.renderRentingInfo(response.resultMap.data);
			});
		},
		renderRentingInfo:function(rentingData) {
			var self = this;
			var rentingInfoTpl = $("#rentingInfoTpl").html();
			var rentingData = (rentingData.hasOwnProperty("rentProductCount") || rentingData.hasOwnProperty("rentMaterialCount") || rentingData.hasOwnProperty("rentAmountMaterialCount")) ? rentingData : [];
			var data = {
				rentingData: rentingData
			}
			Mustache.parse(rentingInfoTpl);
			$("#rentingInfoTable").html(Mustache.render(rentingInfoTpl, data));
		},
		renderProductFirstNeed:function(order) {
			var self = this;
			var listData = order.hasOwnProperty("customerCompanyNeedFirstList") ? order.customerCompanyNeedFirstList : [];
			$("#firstNeedCount").html(listData.length)
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					productName:function() {
						return this.product.productName
					},
					brandName:function() {
						return this.product.brandName
					},
					categoryName:function() {
						return this.product.categoryName
					},
					propertiesToStr:function() {
						var str = this.product.productSkuList[0].hasOwnProperty('productSkuPropertyList') ? this.product.productSkuList[0].productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType)
					},
					isNewIntValue:function() {
						if(this.hasOwnProperty('isNewProduct')) {
							return this.isNewProduct;
						} else if(this.hasOwnProperty('isNew')) {
							return this.isNew;
						}
					},
				})
			}
			Mustache.parse(self.$productFirstListTpl);
			self.$productFirstListTable.html(Mustache.render(self.$productFirstListTpl, data));
			
			if(order.hasOwnProperty("firstListTotalPrice") && order.firstListTotalPrice > 0) {
				$("#firstListTotalPriceBox").parents(".totalPriceRow").removeClass("hide");
				$("#firstListTotalPriceBox").html("￥" + order.firstListTotalPrice);
			}
		},
		renderProductLaterNeed:function(order) {
			var self = this;
			var listData = order.hasOwnProperty("customerCompanyNeedLaterList") ? order.customerCompanyNeedLaterList : [];
			$("#lastNeedCount").html(listData.length)
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					productName:function() {
						return this.product.productName
					},
					brandName:function() {
						return this.product.brandName
					},
					categoryName:function() {
						return this.product.categoryName
					},
					propertiesToStr:function() {
						var str = this.product.productSkuList[0].hasOwnProperty('productSkuPropertyList') ? this.product.productSkuList[0].productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType)
					},
					isNewIntValue:function() {
						if(this.hasOwnProperty('isNewProduct')) {
							return this.isNewProduct;
						} else if(this.hasOwnProperty('isNew')) {
							return this.isNew;
						}
					},
				})
			}
			Mustache.parse(self.$productLaterListTpl);
			self.$productLaterListTable.html(Mustache.render(self.$productLaterListTpl, data));
			
			if(order.hasOwnProperty("laterListTotalPrice") && order.laterListTotalPrice > 0) {
				$("#laterListTotalPriceBox").parents(".totalPriceRow").removeClass("hide");
				$("#laterListTotalPriceBox").html("￥" + order.laterListTotalPrice);
			}
		},
		deliveryModeValue:function(order) {
			if(order.deliveryMode) {
				function returndeliveryValue() {
					var deliveryMode = order.deliveryMode;
					switch(deliveryMode) {
						case 1:
							return "快递";
						case 2:
							return "自提";
						case 3:
							return "凌雄配送";
					}
				}
				$("#deliveryMode").html("(送货方式：" + returndeliveryValue() + ")");
			} else {
				$("#deliveryMode").css("display","none");
			}
		},
	};
	
	window.BusinessCustomerDetail = BusinessCustomerDetail;

})(jQuery);