;(function($) {

	var confirmStatement = {
		init:function(prams) {
			this.props = _.extend({
                customerNo:null,
                customerName:null,
				callBack:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
            var self = this;
			Rental.modal.open({src:SitePath.base + "customer-manage/confirm-statement", type:'ajax', 
                ajaxContentAdded:function() {
                    self.initDom();
                    self.initEvent();	
                }
            });
		},
		initDom:function(pModal) {
			this.$modal = $("#confirmStatementModal");
		},
		initEvent:function() {
            var self = this;

            self.renderConfirmInfo();
            
            self.$modal.on("click",".confirmBtn",function(event) {
                event.preventDefault();
                self.confirmFun()
            })

            self.$modal.on("click",".cancelButton",function(event) {
                event.preventDefault();
                Rental.modal.close();
            })
		},
		renderConfirmInfo:function() {
            $("#customerName").html(this.props.customerName);
            $("#customerNo").html(this.props.customerNo);

            var user = Rental.localstorage.getUser();
            $("#confirmPerson").html(user.realName);

            var statementUrl = SitePath.base + "statement-order/list?customerNo=" + this.props.customerNo + "&customerName=" + this.props.customerName
            $("#viewStatement").attr("href",statementUrl);
            
            var monStatementUrl = SitePath.base + "statement-monthly-order/list?customerNo=" + this.props.customerNo + "&customerName=" + this.props.customerName
            $("#viewMonStatement").attr("href",monStatementUrl);
		},
		confirmFun:function() {
            var self = this;
            var des = '结算单数据确认';
            Rental.ajax.submit("{0}customer/confirmStatement".format(SitePath.service),{customerNo:self.props.customerNo},function(response){
                if(response.success) {
                    Rental.notification.success(des,response.description || '成功');
                    Rental.modal.close();
                    self.props.callBack();
                } else {
                    Rental.notification.error(des,response.description || '失败');
                }
            }, null, des);
		}
	};

	window.confirmStatement = confirmStatement;

})(jQuery)