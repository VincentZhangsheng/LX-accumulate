;(function($) {

	var addSubCompanyManage = {
		state:{
            customerId:null,
			subCompanyList: new Array()
		},
		init:function(prams) {
			this.props = _.extend({
                chooseCompanyList:new Array(),
                customerId:null,
				callBack:function() {},
				complete:function() {},
				modalCloseCallBack:function() {},
				continueChooseCustomeFunc:function() {},
				changeContinueFunc:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
            var self = this;
			Rental.modal.open({src:SitePath.base + "customer-manage/add-subCompany", type:'ajax', 
			ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			},
			afterClose:function() {
				self.props.modalCloseCallBack && self.props.modalCloseCallBack();
				_.isFunction(self.props.continueChooseCustomeFunc) && self.props.continueChooseCustomeFunc();
			}
		});
		},
		initDom:function(pModal) {
			this.$modal = pModal.container;
			this.$subCompanyModal = $("#subCompanyModal");
			this.$form = $("form", this.$modal);
			this.$subCompanyTpl = $("#subCompanyTpl").html();
            this.$subCompanyTable = $("#subCompanyTable");
		},
		initEvent:function() {
            var self = this;
            
            //初始化
            self.renderCompanyData();
            self.renderCompanyList();
			
			self.$subCompanyTable.rtlCheckAll('checkAll','checkItem');

			$("#batchAddCompany").click(function(event) {
				self.props.continueChooseCustomeFunc = function() {
					_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
				}
				Rental.modal.close();
			});

			$("#batchDeleteCompany").click(function(event) {
				self.batchDeleteCompany();
				self.renderCompanyList();
			});

			//删除一条客户交易
			self.$subCompanyTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				self.deleteCompanyItem($(this));
				self.renderCompanyList();
			});

			Rental.form.initFormValidation(self.$form,function(form){
				self.confirm();
			});

			self.$form.on('click', '.cancelBtn', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		renderCompanyData:function() {
            this.state.customerId = this.props.customerId;
			this.state.subCompanyList = this.props.chooseCompanyList;
		},
		renderCompanyList:function() {
			var self = this;
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":self.state.subCompanyList,
				})
			}
			Mustache.parse(self.$subCompanyTpl);
			self.$subCompanyTable.html(Mustache.render(self.$subCompanyTpl, data));
		},
		deleteCompanyItem:function($deleteButton) {
			var self = this;
			var customerNo = $deleteButton.data("customerno");
			var _index = _.findIndex(self.state.subCompanyList, {customerNo:customerNo});
			self.state.subCompanyList.splice(_index,1);
		},
		batchDeleteCompany:function() {
			var self = this, 
				array = $('[name=checkItem]', self.$subCompanyTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', self.$subCompanyTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择客户');
				return self.state.subCompanyList;
			}

			filterChecked.each(function(i, item) {
				var customerNo = $(this).val();
				var _index = _.findIndex(self.state.subCompanyList, {customerNo:customerNo});
				self.state.subCompanyList.splice(_index,1);
			});
		},
		confirm:function() {
			try {
				var self = this;
                var customerIdList = self.state.subCompanyList.map(function(item) {
                    return item.customerId
                })

                var commitData = {
                    parentCustomerId:self.state.customerId,
                    customerIdList:customerIdList
                }
				var des = '添加子公司';
				Rental.ajax.submit("{0}customer/addParentCompany".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						Rental.modal.close();
						self.props.callBack();
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch (e) {
				Rental.notification.error("添加子公司", Rental.lang.commonJsError +  '<br />' + e );
			}
		}
	};

	window.addSubCompanyManage = addSubCompanyManage;

})(jQuery)