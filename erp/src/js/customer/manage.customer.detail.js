;(function($){
	var CustomerDetail = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_customer_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function() {
			this.actionButtons = new Array();
			this.manualActionButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var edit = this.currentManageListAuthor.children[AuthorCode.manage_customer_edit],
				editSalesman = this.currentManageListAuthor.children[AuthorCode.manage_customer_edit_salesman],
				risk = this.currentManageListAuthor.children[AuthorCode.manage_customer_risk],
				addAddress = this.currentManageListAuthor.children[AuthorCode.manage_customer_address_add],
				audit = this.currentManageListAuthor.children[AuthorCode.manage_customer_audit], 
				submitAudit = this.currentManageListAuthor.children[AuthorCode.manage_customer_submitAudit], 
				disabled = this.currentManageListAuthor.children[AuthorCode.manage_customer_disbaled],
				settlementDate = this.currentManageListAuthor.children[AuthorCode.manage_customer_settlement_date],
				shortRentalUpperLimit = this.currentManageListAuthor.children[AuthorCode.manage_customer_short_rental_upper_limit],
				setRiskCreditAmountUsed = this.currentManageListAuthor.children[AuthorCode.manage_customer_manage_set_risk_credit_amount_used],
				confirmStatement = this.currentManageListAuthor.children[AuthorCode.manage_customer_statement_order_confirm],
				confirmBadDebt = this.currentManageListAuthor.children[AuthorCode.manage_customer_confirm_baddebt],
				exportStatement = this.currentManageListAuthor.children[AuthorCode.manage_customer_statement_of_account_export];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			// edit && this.actionButtons.push(AuthorUtil.button(_.extend(edit, {menuUrl:'customer-manage/edit-after-pass'}),'editAfterPassButton','','编辑业务员&联合开发人'));
			editSalesman && this.actionButtons.push(AuthorUtil.button(editSalesman,'editAfterPassButton','','编辑业务员&联合开发人'));
			risk && this.actionButtons.push(AuthorUtil.button(risk,'riskButton','','风控授信'));
			addAddress && this.actionButtons.push(AuthorUtil.button(addAddress,'addAddressButton','','添加地址'));
			submitAudit && this.actionButtons.push(AuthorUtil.button(submitAudit,'submitAuditButton','','提交审核'));
			audit && this.actionButtons.push(AuthorUtil.button(audit,'auditRejectButton','','驳回'));
			disabled && this.actionButtons.push(AuthorUtil.button(disabled,'disabledButton','','禁用'));
			disabled && this.actionButtons.push(AuthorUtil.button(disabled,'enabledButton','','启用'));
			settlementDate && this.actionButtons.push(AuthorUtil.button(settlementDate,'settlementDate','','设置结算时间'));
			shortRentalUpperLimit && this.actionButtons.push(AuthorUtil.button(shortRentalUpperLimit, 'shortRentalUpperLimit', '', '设置短租上限金额'));
			setRiskCreditAmountUsed && this.actionButtons.push(AuthorUtil.button(setRiskCreditAmountUsed, 'setRiskCreditAmountUsed', '', '设置客户已用授信额度'));
			confirmStatement && this.actionButtons.push(AuthorUtil.button(confirmStatement, 'confirmStatement', '', '结算单数据确认'));
			confirmBadDebt && this.actionButtons.push(AuthorUtil.button(confirmBadDebt,'confirmBadDebt','','确认坏账'));
			exportStatement && this.actionButtons.push(AuthorUtil.button(exportStatement,'exportButton','fa fa-plus','导出对账单'));

			var manualCharge = this.currentManageListAuthor.children[AuthorCode.manage_customer_manual_charge], //加款
				manualDeduct = this.currentManageListAuthor.children[AuthorCode.manage_customer_manual_deduct]; //扣款

			manualCharge && this.manualActionButtons.push(AuthorUtil.button(manualCharge,'manualChargeButton','','加款'));
			manualDeduct && this.manualActionButtons.push(AuthorUtil.button(manualDeduct,'manualDeductButton','','扣款'));
		},
		initDom:function() {
			this.$form = $("#customerDetail");
			this.$customerRiskPanel = $("#customerRiskPanel");
			this.$riskHistoryTpl = $("#riskHistoryTpl").html();
			this.$riskHistoryTable = $("#riskHistoryTable");			

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.loadRentingInfo();

			// //授信
			// self.$customerRiskPanel.on('click', '.riskButton', function(event) {
			// 	event.preventDefault();
			// 	RiskCustomer.init({
			// 		customerNo:$(this).data('customerno'),
			// 		customerType:$(this).data('customertype'),
			// 		callBack:function() {
			// 			self.loadData();
			// 		},
			// 	})
			// });

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			}, {
				isInitAddressEvent:false,
			});		

			self.initAddressInfo();

			CustomerOrders.init({no:this.state.no});
			riskHistory.init({no:this.state.no});
			CustomerOrders.couponList.init({customerNo:self.state.no})
		},
		initAddressInfo:function() {
			var self = this, 
				addAddress = this.currentManageListAuthor.children[AuthorCode.manage_customer_address_add];

			CustomerAddressManage.init({
				addAuthor:addAddress,
				editAuthor:addAddress,
				deleteAuthor:addAddress,
				auditAuthor:addAddress,
				customerNo:self.state.no,
				isCustomer:true,
				callBack:function() {
					self.loadData();
				}
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				 bootbox.alert("没找到客户编号");
				 return;
			}
			Rental.ajax.submit("{0}/customer/detailCustomerPerson".format(SitePath.service),{customerNo:self.state.no},function(response){
				if(response.success) {
					self.state.customer = response.resultMap.data;
					self.render(response.resultMap.data);
				} else {
					Rental.notification.error("查询客户详细信息",response.description ||  '失败');
				}
			}, null ,"查询客户详细信息", 'listLoading');
		},
		render:function(customer) {
			this.renderCustomerHead(customer);
			this.renderActionButton(customer);
			this.renderCustomerRiskManagement(customer);
		},
		renderCustomerHead:function(customer) {
			this.renderCustomer(customer,  $("#customerHeadTpl").html(), $('#customerHead'));
		},
		renderActionButton:function(customer) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, customer),
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderCustomer:function(customer, tpl, container) {
			var self = this;
			Mustache.parse(tpl);
			var data = _.extend(Rental.render, {
				customer:customer,
				manualActionButtons:self.manualActionButtons,
				avatar:function() {
					return '';
				},
				isBadDebtCustomer:function() {
					return this.confirmBadAccountStatus == 1;
				},
				"cutomerTypeValue":function() {
					return Enum.customerType.getValue(this.customerType);
				},
				isDisabledValue:function() {
					return this.isDisabled == 0 ? "否" : "是";
				},
				customerStatusValue:function() {
					return Enum.customerStatus.getValue(this.customerStatus);
				},
				settlementDateValue:function() {
					return this.hasOwnProperty('statementDate')  && !!this.statementDate ? Enum.settlementDate.getValue(this.statementDate) : '月末最后一天';
				},
				shortLimitReceivableAmountValue:function() {
					return this.hasOwnProperty('shortLimitReceivableAmount')  && !!this.shortLimitReceivableAmount ? '￥'+parseFloat(this.shortLimitReceivableAmount).toFixed(2) : '无上限';
				}
			})
			container.html(Mustache.render(tpl, data));
		},
		renderCustomerRiskManagement:function(customer) {
			var self = this;
			var data = _.extend(Rental.render, {
				customerRiskManagement:customer.customerRiskManagement || new Object(),
				// creditAmountFormat:function() {
				// 	return this.creditAmount ? this.creditAmount.toFixed(2) : '0.00';
				// },
				// creditAmountUsedFormat:function() {
				// 	return this.creditAmountUsed ? this.creditAmountUsed.toFixed(2) : '0.00';
				// },
				// applePayModeValue:function() {
				// 	return Enum.payMode.getValue(this.applePayMode);
				// },
				// newPayModeValue:function() {
				// 	return Enum.payMode.getValue(this.newPayMode);
				// },
				// riskButton:function() {
				// 	var risk = self.currentManageListAuthor.children[AuthorCode.manage_customer_risk];
				// 	return risk ? AuthorUtil.button(risk,'riskButton','','风控授信') :  new Object();
				// },
				// customerno:function() {
				// 	return customer.customerNo;
				// },
				// customerType:function() {
				// 	return customer.customerType;
				// },
				// singleLimitPriceValue:function() {
				// 	return this.hasOwnProperty('singleLimitPrice')  && !!this.singleLimitPrice ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
				// }
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				applePayModeValue:function() {
					return Enum.payMode.getValue(this.applePayMode);
				},
				newPayModeValue:function() {
					return Enum.payMode.getValue(this.newPayMode);
				},
				customerno:function() {
					return customer.customerNo;
				},
				customerType:function() {
					return customer.customerType;
				},
				riskButton:function() {
					var risk = self.currentManageListAuthor.children[AuthorCode.manage_customer_risk];
					return risk ? AuthorUtil.button(risk,'riskButton','','风控授信') :  new Object();
				},
				singleLimitPriceValue:function() {
					return this.hasOwnProperty('singleLimitPrice')  && _.isNumber(this.singleLimitPrice) ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
				}
			})

			var customerStatusTpl = $("#customerRiskPanelTpl").html();
			Mustache.parse(customerStatusTpl);
			this.$customerRiskPanel.html(Mustache.render(customerStatusTpl, data));
		},
		loadRentingInfo:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('customer/queryRentCountByCustomerNo', {customerNo:self.state.no}, '查询客户在租信息', function(response) {
				self.renderRentingInfo(response.resultMap.data);
			});
		},
		renderRentingInfo:function(rentingData) {
			var self = this;
			var rentingInfoTpl = $("#rentingInfoTpl").html();
			var rentingData = (rentingData.hasOwnProperty("rentProductCount") || rentingData.hasOwnProperty("rentMaterialCount") || rentingData.hasOwnProperty("rentAmountMaterialCount")) ? rentingData : [];
			var data = {
				rentingData: rentingData
			}
			Mustache.parse(rentingInfoTpl);
			$("#rentingInfoTable").html(Mustache.render(rentingInfoTpl, data));
		},
	};
	
	window.CustomerDetail = _.extend(CustomerDetail, CustomerHandleMixin, CustomerMixin);

})(jQuery);