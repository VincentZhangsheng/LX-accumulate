;(function($){
	
	var EditBusinessCustomerAfterPass = {
		state:{
			no:null,
			customer:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_business_customer_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_edit];
		},
		initDom:function() {
			var self = this;
			this.$form = $("#editCustomerForm");
			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_business_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.loadData();
			self.initCommonEvent();
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到企业客户编号');
				return;
			}
			Rental.ajax.ajaxData('customer/detailCustomerCompany', {customerNo:self.state.no}, '获取企业客户详细信息', function(response) {
				self.state.customer = response.resultMap.data;
				self.initFormData(response.resultMap.data);
			});
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$form, data);
			Rental.ui.renderFormData(self.$form, data.customerCompany);
		},
		edit:function() {
			try {
				var self = this,
					formData = Rental.form.getFormData(self.$form);

				var commitData = {
					customerNo:self.state.no,
					unionUser:formData['unionUser'],
					owner:formData['owner']
				}

				var des = "编辑企业客户";
				Rental.ajax.submit("{0}customer/updateOwnerAndUnionUser".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null ,des);

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑企业客户",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } 
				    }
				});
			}
			return false;
		}
	};

	window.EditBusinessCustomerAfterPass = _.extend(EditBusinessCustomerAfterPass, CustomerMixin);

})(jQuery);