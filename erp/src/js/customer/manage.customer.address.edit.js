;(function($){

	var EditCustomerAddress = {
		init:function(prams) {
			this.props = _.extend({
				customerConsignInfoId:null,
				customerNo:null,
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/customer-consign-info/edit", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			var self = this;

			this.$form = $("#editCustomerAddressForm");
			this.$cancelButton = $('.cancelButton', this.$form);

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});
		},
		initEvent:function() {
			var self = this;

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.props.customerConsignInfoId) {
				bootbox.alert('没找地址编号');
				return;
			}
			Rental.ajax.submit("{0}customer/detailCustomerConsignInfo".format(SitePath.service),{customerConsignInfoId:self.props.customerConsignInfoId},function(response){
				if(response.success) {
					self.initFormData(response.resultMap.data);
				} else {
					Rental.notification.error("编辑客户地址",response.description || '失败');
				}
			}, null ,"编辑客户地址", 'listLoading');
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			$.each(array,function(i,item) {
				if(data.hasOwnProperty(item.name)) {
					var dom = self.$form.find("[name="+item.name+"]");
	            	dom.val(data[item.name]);
	            	dom.data('defaultvalue', data[item.name]);
				}
	        });
	        
	        $('[name=isMain]', self.$form).prop('checked',data.isMain == 1 ? true:false);

			// self.Area.setDefaultValue({
			// 	province:data.province,
			// 	city:data.city,
			// 	district:data.district,
			// });

			self.Area.changeValue({
				province:data.province,
				city:data.city,
				district:data.district,
			});
		},
		edit:function() {
			try {
				
				var self = this,
					tip = '修改客户收货地址',
					isMain = $('[name=isMain]', self.$form).prop('checked'),
					formData = Rental.form.getFormData(self.$form);

				formData.isMain = isMain ? 1 : 0;
				formData.customerConsignInfoId = self.props.customerConsignInfoId;

				Rental.ajax.submit("{0}customer/updateCustomerConsignInfo".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(tip,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(tip,response.description || '失败');
					}
				}, null ,tip, 'dialogLoading');

			} catch (e) {
				Rental.notification.error(tip, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack(response.resultMap.data);
			}
			return false;
		}
	};

	window.EditCustomerAddress = EditCustomerAddress;

})(jQuery);