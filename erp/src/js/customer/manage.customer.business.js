;(function($) {

	var BusinessCustomerManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_business_customer_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_business_customer_detail],
				add = this.currentPageAuthor.children[AuthorCode.manage_business_customer_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_business_customer_edit],
				editSalesman = this.currentPageAuthor.children[AuthorCode.manage_business_customer_edit_salesman],
				addSubCompany = this.currentPageAuthor.children[AuthorCode.manage_business_customer_add_subCompany],
				risk = this.currentPageAuthor.children[AuthorCode.manage_business_customer_risk],
				addAddress = this.currentPageAuthor.children[AuthorCode.manage_business_customer_address_add],
				manualCharge = this.currentPageAuthor.children[AuthorCode.manage_business_customer_manual_charge], //加款
				manualDeduct = this.currentPageAuthor.children[AuthorCode.manage_business_customer_manual_deduct], //扣款
				audit = this.currentPageAuthor.children[AuthorCode.manage_business_customer_audit], //审核
				submitAudit = this.currentPageAuthor.children[AuthorCode.manage_business_customer_submitAudit], //提交审核
				disabled = this.currentPageAuthor.children[AuthorCode.manage_business_customer_disabled],
				settlementDate = this.currentPageAuthor.children[AuthorCode.manage_business_customer_settlement_date],
				shortRentalUpperLimit = this.currentPageAuthor.children[AuthorCode.manage_business_customer_short_rental_upper_limit],
				setRiskCreditAmountUsed = this.currentPageAuthor.children[AuthorCode.manage_business_customer_set_risk_credit_amount_used],
				addReturnVisit = this.currentPageAuthor.children[AuthorCode.manage_business_customer_return_visit_record_add],//添加客户回访
				confirmStatement = this.currentPageAuthor.children[AuthorCode.manage_business_customer_statement_order_confirm],
				confirmBadDebt = this.currentPageAuthor.children[AuthorCode.manage_business_customer_confirm_baddebt],
				exportStatement = this.currentPageAuthor.children[AuthorCode.manage_business_customer_statement_of_account_export];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			// editSalesman && this.rowActionButtons.push(AuthorUtil.button(_.extend(editSalesman, {menuUrl:'customer-business-manage/edit-after-pass'}),'editAfterPassButton','','编辑业务员&联合开发人'));
			editSalesman && this.rowActionButtons.push(AuthorUtil.button(editSalesman,'editAfterPassButton','','编辑业务员&联合开发人'));
			addSubCompany && this.rowActionButtons.push(AuthorUtil.button(addSubCompany,'addSubCompany','','添加子公司'));
			risk && this.rowActionButtons.push(AuthorUtil.button(risk,'riskButton','','风控授信'));
			addAddress && this.rowActionButtons.push(AuthorUtil.button(addAddress,'addAddressButton','','添加地址'));
			submitAudit && this.rowActionButtons.push(AuthorUtil.button(submitAudit,'submitAuditButton','','提交审核'));
			// audit && this.rowActionButtons.push(AuthorUtil.button(audit,'auditPassButton','','通过'));
			audit && this.rowActionButtons.push(AuthorUtil.button(audit,'auditRejectButton','','驳回'));
			disabled && this.rowActionButtons.push(AuthorUtil.button(disabled,'disabledButton','','禁用'));
			disabled && this.rowActionButtons.push(AuthorUtil.button(disabled,'enabledButton','','启用'));
			settlementDate && this.rowActionButtons.push(AuthorUtil.button(settlementDate,'settlementDate','','设置结算时间'));
			shortRentalUpperLimit && this.rowActionButtons.push(AuthorUtil.button(shortRentalUpperLimit, 'shortRentalUpperLimit', '', '设置短租上限金额'));
			setRiskCreditAmountUsed && this.rowActionButtons.push(AuthorUtil.button(setRiskCreditAmountUsed, 'setRiskCreditAmountUsed', '', '设置客户已用授信额度'));
			addReturnVisit && this.rowActionButtons.push(AuthorUtil.button(addReturnVisit, 'addReturnVisit', '', '添加客户回访'));
			confirmStatement && this.rowActionButtons.push(AuthorUtil.button(confirmStatement, 'confirmStatement', '', '结算单数据确认'));
			confirmBadDebt && this.rowActionButtons.push(AuthorUtil.button(confirmBadDebt,'confirmBadDebt','','确认坏账'));
			exportStatement && this.rowActionButtons.push(AuthorUtil.button(exportStatement,'exportButton','fa fa-plus','导出对账单'));

			// this.rowActionButtons.push(AuthorUtil.button(new Object(), 'viewWorkFlowButton', '', '查看审核工作流', null, true));
			this.viewDisabled = this.currentPageAuthor.children[AuthorCode.manage_business_customer_view_disabled];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.businessCustomer);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.renderIsDisabledButton();
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			Rental.ui.renderSelect({
				container: $('#customerStatus'),
				data:Enum.array(Enum.customerStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '全部 （客户状态）'
			})

			Rental.ui.renderSelect({
				container: $('#isRisk'),
				data:Enum.array(Enum.riskStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '全部 （是否授信）'
			})

			Rental.ui.renderSelect({
				container: $('#confirmBadAccountStatus'),
				data:Enum.array(Enum.badDebtStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '全部 （是否为坏账客户）'
			})

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("#ownerSubCompanyId"),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

			self.renderStatus();
			this.searchData();  //初始化列表数据
			self.render({});

			self.initHandleEvent(self.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			})

			//是否禁用按钮
			self.renderIsDisabledClass();
			self.$searchForm.on("click","#isDisabledBtn",function(){
				if($("#isDisabled", self.$searchForm).val() == 1) {
					$("#isDisabled", self.$searchForm).val("0");
					$(this).removeClass("btn-primary").addClass("btn-default")
				} else {
					$("#isDisabled", self.$searchForm).val("1");
					$(this).removeClass("btn-default").addClass("btn-primary")
				}
				self.searchData();
			})
		},
		bindButtonEvent:function($this, eventClass) {
			var customerNo = $this.data('customerno'),
				customerName = $this.data('customername'),
				customerType = $this.data('customertype');
				 
			eventClass.init({
				customerNo:customerNo,
				customerType:customerType,
				callBack:function() {
					self.doSearch(self.Pager.pagerData.currentPage);
				},
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		renderStatus:function() {
			var customerStatus = Rental.helper.getUrlPara('customerStatus')
			if(!!customerStatus) {
				Rental.ui.renderFormData(this.$searchForm, {customerStatus:customerStatus});
			}
		},
		renderIsDisabledButton:function() {
			var self = this;
			var isDisabledTpl = $('#isDisabledTpl').html();
			Mustache.parse(isDisabledTpl);
			$("#isDisabledWrapper").html(Mustache.render(isDisabledTpl,{'isDisabled':self.viewDisabled}));
		},
		renderIsDisabledClass:function() {
			if(this.viewDisabled && $('#isDisabled').val() == 1) {
				$('#isDisabledBtn').removeClass("btn-default").addClass("btn-primary")
			}
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.businessCustomer, searchData);

			Rental.ajax.ajaxData('customer/pageCustomerCompany', searchData, '查询企业客户', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				// rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					isBadDebtCustomer:function() {
						return this.confirmBadAccountStatus == 1;
					},
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
					},
					rowData:function() {
						return JSON.stringify(this);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"cutomerTypeValue":function() {
						return Enum.customerType.getValue(this.customerType);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					addressForamt:function() {
						return (this.customerCompany.provinceName || '') + (this.customerCompany.cityName  || '') + (this.customerCompany.districtName  || '') + (this.customerCompany.address  || '');
					},
					customerStatusValue:function() {
						return Enum.customerStatus.getValue(this.customerStatus);
					},
					customerStatusClass:function() {
						return Enum.customerStatus.getClass(this.customerStatus);	
					},
					auditReason:function() {
						if(this.customerStatus == Enum.customerStatus.num.pass) {
							return this.passReason;
						} else if(this.customerStatus == Enum.customerStatus.num.reject) {
							return this.failReason;
						} else {
							return "";
						}
					},
					isRiskValue:function() {
						// return this.hasOwnProperty('customerRiskManagement') ? '已授信':'未授信';
						return Enum.riskStatus.getValue(this.isRisk);
					},
					isRiskClass:function() {
						return this.isRisk == 1 ? 'text-success':'text-danger';
					},
					showViewWorkFlowButton:function() {
						return this.customerStatus == Enum.customerStatus.num.commited || this.customerStatus == Enum.customerStatus.num.reject;
					},
					subsidiary:function() {
						return this.customerCompany.subsidiary == true ? "是" : "否";
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.BusinessCustomerManage = _.extend(BusinessCustomerManage, CustomerHandleMixin);

})(jQuery);