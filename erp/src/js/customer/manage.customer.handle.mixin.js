;(function($) {

	var CustomerHandleMixin = {
		state:{
			customerId:null,
			chooseCompanyList:new Array()
		},
		initHandleEvent:function(container, handleCallBack, prams) {
			var self = this;
			prams = _.extend({
				isInitAddressEvent:true
			}, prams || {});

			// //查看审核工作流
			// container.on('click', '.viewWorkFlowButton', function(event) {
			// 	event.preventDefault();
			// 	var customerNo = $(this).data('customerno');
			// 	Rental.searchStorage.set(Rental.searchStorage.enum.auditList, {
			// 		workflowReferNo:customerNo
			// 	});
			// 	window.location.href = PageUrl.auditManageList;
			// });


			//加款
			container.on('click', '.manualChargeButton', function(event) {
				event.preventDefault();
				self.manualCharge({
					businessCustomerNo:$(this).data('customerno')
				}, function() {
					handleCallBack &&　handleCallBack();
					if(self.hasOwnProperty('state') && self.state.no) {
						CustomerOrders.accountList.init({customerNo:self.state.no})
					}
				});
			});

			//扣款
			container.on('click', '.manualDeductButton', function(event) {
				event.preventDefault();
				self.manualDeduct({
					businessCustomerNo:$(this).data('customerno')
				}, function() {
					handleCallBack &&　handleCallBack();
					if(self.hasOwnProperty('state') && self.state.no) {
						CustomerOrders.accountList.init({customerNo:self.state.no})
					}
				});
			});

			container.on('click', '.riskButton', function(event) {
				event.preventDefault();
				self.bindButtonEvent($(this), RiskCustomer, function() {
					handleCallBack &&　handleCallBack();
					if(self.hasOwnProperty('state') && self.state.no) {
						riskHistory.init({no:self.state.no});
					}
				});
			});

			if(prams.isInitAddressEvent) {
				container.on('click', '.addAddressButton', function(event) {
					event.preventDefault();
					self.bindButtonEvent($(this), AddCustomerAddress, handleCallBack);
				});	
			}

			container.on('click', '.auditPassButton', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno');
				self.pass(customerno, handleCallBack);
			});			

			container.on('click', '.auditRejectButton', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno');
				self.reject(customerno, handleCallBack)
			});			

			container.on('click', '.submitAuditButton', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno');
				// self.submitAuditFunc(customerno, handleCallBack);
				ApiData.isNeedVerify({
					no:$(this).data('customerno'),
					workflowType:Enum.workflowType.num.customer, //parseInt($(this).data('ownersubcompanyid')) == Enum.subCompany.num.telemarketing ?  Enum.workflowType.num.customerOwnerByTelSeller : Enum.workflowType.num.customer,
					verifyAttachment:false,
				}, function(result, isAudit) {
					self.submitAudit(result, handleCallBack, isAudit);
				})
			});			

			container.on('click', '.disabledButton', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno');
				self.disabled(customerno, handleCallBack);
			});			

			container.on('click', '.enabledButton', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno');
				self.enabled(customerno, handleCallBack);
			});			

			//设置结算时间
			container.on('click', '.settlementDate', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno'), statementdate = $(this).data('statementdate')
				self.setSettlementDate(customerno, statementdate, handleCallBack);
			});			

			container.on('click', '.shortRentalUpperLimit', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno'), shortLimitReceivableAmount = $(this).data('shortlimitreceivableamount');
				self.setShortRentalUpperLimit(customerno, shortLimitReceivableAmount, handleCallBack);
			});			

			container.on('click', '.setRiskCreditAmountUsed', function(event) {
				event.preventDefault();
				var customerno = $(this).data('customerno');
				self.setCreditAmountUsed(customerno, handleCallBack);
			});			

			container.on('click', '.viewWorkFlowButton', function(event) {
				event.preventDefault();
				var customer = $(this).closest('tr').data('rowdata');
				ViewWorkFlow.init({
					workflowType:Enum.workflowType.num.customer,
					workflowReferNo:customer.customerNo,
				});
			});

			//添加客户回访
			container.on('click', '.addReturnVisit', function(event) {
				event.preventDefault();
				self.bindButtonEvent($(this), AddReturnVisitRecord, function() {
					handleCallBack &&　handleCallBack();
					if(self.hasOwnProperty('state') && self.state.no) {
						AddReturnVisitRecord.init({
							customerNo:customerNo,
							customerName:customerName
						});
					}
				});
			});

			//结算单数据确认
			container.on('click', '.confirmStatement', function(event) {
				event.preventDefault();
				var customerNo = $(this).data("customerno");
				var customerName = $(this).data("customername");
				confirmStatement.init({
					customerNo:customerNo,
					customerName:customerName,
					callBack:function() {
						handleCallBack && handleCallBack();
					} 
				})
			});

			//确认为坏账客户
			container.on('click', '.confirmBadDebt', function(event) {
				event.preventDefault();
				var customerNo = $(this).data("customerno");
				var customerName = $(this).data("customername");
				self.confirnBadDebetCustomer.init({
					customerNo:customerNo,
					customerName:customerName,
					callBack:function() {
						handleCallBack && handleCallBack();
					} 
				})
			});

			//导出对账单
			container.on('click', '.exportButton', function(event) {
				event.preventDefault();
				var customerNo = $(this).data("customerno");
				self.exportStatement.modal({customerNo:customerNo})
				// self.export(customerNo);
			});

			//添加子公司
			container.on('click', '.addSubCompany', function(event) {
				event.preventDefault();
				var customerId = $(this).data("id");
				self.state.customerId = customerId;
				self.state.chooseCompanyList = new Array();
				self.addSubCompany()
			});
		},
		bindButtonEvent:function($this, eventClass, handleCallBack) {
			var customerNo = $this.data('customerno'), 
				customerName = $this.data('customername'), 
				customerType = $this.data('customertype');

			eventClass.init({
				customerNo:customerNo,
				customerName:customerName,
				customerType:customerType,
				isCustomer:true,
				callBack:function() {
					handleCallBack && handleCallBack();
				},
			});
		},
		do:function(url, prams, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		manualCharge:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'加款',
				url:'customer-manual-account/modal',
				initFunc:function(modal) {
					// Rental.helper.onOnlyDecmailNum($('[name=amount]',modal),2);
				},
				callBack:function(result, $modal) {
					self.do('payment/manualCharge', {
						businessCustomerNo:prams.businessCustomerNo,
						chargeAmount:result.amount,
						chargeRemark:result.remark
					}, '加款', function() {
						Rental.modal.close();
						callBack && callBack();
					});
				}
			});
		},
		manualDeduct:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'扣款',
				url:'customer-manual-account/modal',
				initFunc:function(modal) {},
				callBack:function(result, $modal) {
					self.do('payment/manualDeduct', {
						businessCustomerNo:prams.businessCustomerNo,
						deductAmount:result.amount,
						deductRemark:result.remark
					}, '扣款', function() {
						Rental.modal.close();
						callBack && callBack();
					});
				}
			});
		},
		pass:function(customerNo, callBack) {
			var self = this;
			bootbox.prompt({
			    title: "审核通过，输入备注信息",
			    inputType: 'textarea',
			    callback: function (result) {
			        if(null != result) {
			        	self.do('customer/verifyCustomer', {customerNo:customerNo, customerStatus:Enum.customerStatus.num.pass, verifyRemark:result }, '审核', function() {
							callBack && callBack();
						});
			        }
			    }
			});
		},
		reject:function(customerNo, callBack) {
			var self = this;
			bootbox.prompt({
			    title: "驳回，输入备注信息",
			    inputType: 'textarea',
			    callback: function (result) {
			    	if(null != result) {
			    		self.do('customer/rejectCustomer', { customerNo:customerNo, remark:result }, '驳回', function() {
							callBack && callBack();
						});
			    	}
			    }
			});
		},
		submitAuditFunc:function(customerNo, callBack) {
			var self = this;
			bootbox.confirm('确认提交审核？', function(result) {
				if(result) {
					self.do('customer/commitCustomer', { customerNo:customerNo }, '提交审核', function() {
						callBack && callBack();
					});	
				}
			});
		},
		submitAudit:function(prams, callBack, isAudit) {			
			var self = this, des = '提交审核客户', submitUrl = 'customer/commitCustomerToWorkflow';

			self.do(submitUrl, {
				customerNo:prams.no,
				verifyUserId:prams.verifyUserId,
				remark:prams.commitRemark,
				imgIdList:prams.imgIdList,
			}, des, function() {
				Rental.modal.close();
				callBack && callBack();
			});
		},
		disabled:function(customerNo, callBack) {
			var self = this;
			bootbox.confirm('确认禁用该客户？', function(result) {
				if(result) {
					self.do('customer/disabledCustomer', { customerNo:customerNo }, '禁用客户', function() {
						callBack && callBack();
					});	
				}
			});
		},
		enabled:function(customerNo, callBack) {
			var self = this;
			bootbox.confirm('确认启用该用户？', function(result) {
				if(result) {
					self.do('customer/enableCustomer', { customerNo:customerNo }, '启用客户', function() {
						callBack && callBack();
					});	
				}
			});
		},		
		setSettlementDate:function(customerNo, statementdate, callBack) {
			var self = this;
			InputModal.init({
				title:'设置结算时间',
				url:'customer/set-settlement-date-modal',
				initFunc:function(modal) {
					var statementdateInput = $('[name=statementDate]', modal);
					Rental.ui.renderSelect({
						container:statementdateInput,
						data:Enum.array(Enum.settlementDate),
						func:function(opt, item) {
							return opt.format(item.num, item.value);
						},
						defaultText:'请选择'
					});
					statementdateInput.val(statementdate);
				},
				callBack:function(result) {
					self.do('customer/addStatementDate', { customerNo:customerNo,statementDate:result.statementDate }, '设置结算时间', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			});
		},
		setShortRentalUpperLimit:function(customerNo, shortLimitReceivableAmount, callBack) {
			var self = this;
			InputModal.init({
				title:'设置短租上限金额',
				url:'customer/set-short-rental-upper-limit-modal',
				initFunc:function(modal) {
					$('[name=shortLimitReceivableAmount]', modal).val(shortLimitReceivableAmount);
				},
				callBack:function(result) {
					self.do('customer/addShortReceivableAmount', { customerNo:customerNo, shortLimitReceivableAmount:result.shortLimitReceivableAmount }, '设置短租上限金额', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			})
		},
		setCreditAmountUsed:function(customerNo, callBack) {
			var self = this;
			InputModal.init({
				title:'设置客户已用授信额度',
				url:'customer/set-risk-credit-amount-used',
				initFunc:function(modal) {
					self.customerDetail({
						customerNo:customerNo,
						success:function(resuslt) {
							$('[name=creditAmountUsed]', modal).val(resuslt.hasOwnProperty('customerRiskManagement') ? resuslt.customerRiskManagement.creditAmountUsed : '');
						}
					});
				},
				callBack:function(result) {
					self.do('customer/updateRiskCreditAmountUsed', { customerNo:customerNo, creditAmountUsed:result.creditAmountUsed }, '设置客户已用授信额度', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			})
		},
		customerDetail:function(prams) {
			Rental.ajax.ajaxDataNoLoading('customer/detailCustomerCompany', {customerNo:prams.customerNo}, '加载客户详细', function(response) {
				prams.success && prams.success(response.resultMap.data);
			}, function() {
				prams.error && prams.error();
			});
		},
		addSubCompany:function() {
			var self = this;
			ChooseBusinessCustomer.init({
				batch:true,
				callBack:function(customer) {
					self.chooseCustomer(self.state.chooseCompanyList,customer);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {
					if(self.state.chooseCompanyList.length > 0) {
						addSubCompanyManage.init({
							customerId:self.state.customerId,
							chooseCompanyList:self.state.chooseCompanyList,
							continueChooseCustomeFunc:null,
							changeContinueFunc:function() {
								_.isObject(CustomerHandleMixin) &&  _.isFunction(CustomerHandleMixin.addSubCompany) && CustomerHandleMixin.addSubCompany();
							},
							callBack:function() {
								self.doSearch(self.Pager.pagerData.currentPage);
							}
						});
					}
				}
			})
		},
		chooseCustomer:function(chooseCompanyList, customer) {
			if(!chooseCompanyList) {
				chooseCompanyList = new Array();
			}	
			if(chooseCompanyList.length > 0) {
				var _Index = _.findIndex(chooseCompanyList, {customerNo:customer.customerNo});
				if(_Index > -1) {
					return chooseCompanyList;
				} else {
					chooseCompanyList.push({
						customerName:customer.customerName,
						customerNo:customer.customerNo,
						customerId:customer.customerId
					})
				}
			} else {
				chooseCompanyList.push({
					customerName:customer.customerName,
					customerNo:customer.customerNo,
					customerId:customer.customerId
				})
			}
			return chooseCompanyList;
		},
		filterAcitonButtons:function(buttons, customer) {
			var rowActionButtons = new Array(),
				unSubmit = customer.customerStatus == Enum.customerStatus.num.unSubmit,
				commited = customer.customerStatus == Enum.customerStatus.num.commited,
				commitedOrPass = customer.customerStatus == Enum.customerStatus.num.commited || customer.customerStatus == Enum.customerStatus.num.pass,
				isBadDebtCustomer = customer.confirmBadAccountStatus == 1,
				isConfirmStatement = customer.confirmStatementStatus == 1;
				subsidiary =  customer.customerCompany.subsidiary == true;

			(buttons || []).forEach(function(button, index) {
				button.customerNo = customer.customerNo;
				button.customerId = customer.customerId;
				button.customerName = customer.customerName;
				button.statementDate = customer.statementDate;
				button.shortLimitReceivableAmount = customer.shortLimitReceivableAmount;
				button.customerType = customer.customerType;
				button.ownerSubCompanyId = customer.ownerSubCompanyId;
				rowActionButtons.push(button);

				if((button.class == 'submitAuditButton') && !(unSubmit || customer.customerStatus == Enum.customerStatus.num.reject)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}				
				if((button.class == 'auditPassButton') && !commited) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'auditRejectButton') && customer.customerStatus != Enum.customerStatus.num.pass) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				// if((button.class == 'auditRejectButton') && !commitedOrPass) {
				// 	rowActionButtons.splice(rowActionButtons.length - 1,1);
				// }
				if((button.class == 'editButton') && commitedOrPass) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				// if((button.class == 'editAfterPassButton') && !commitedOrPass) {
				// 	rowActionButtons.splice(rowActionButtons.length - 1,1);
				// }
				if((button.class == 'disabledButton') && customer.isDisabled == 1) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'enabledButton') && customer.isDisabled == 0) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'confirmBadDebt') && isBadDebtCustomer) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'confirmStatement') && isConfirmStatement) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'addSubCompany') && subsidiary) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		},
		exportStatement:{
			modal:function(prams) {
				this.props = _.extend({
					customerNo:null,
					callBack:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "customer-statement/list-export", type:'ajax', ajaxContentAdded:function(pModal) {
					self.initDom(pModal);
					self.initEvent();	
				}});
			},
			initDom:function(modal) {
				this.$exportModal = modal.container;
				this.$exportForm = $("#exportForm");
				this.$chooseForm = $("#chooseForm");
			},
			initEvent:function() {
				var self = this;
				Rental.ui.events.initMonthpicker($("#startTime"), moment().subtract(3,'month'));
				Rental.ui.events.initMonthpicker($("#endTime"),moment());

				self.$chooseForm.on('click','.cancelButton',function(event) {
					event.preventDefault();
					Rental.modal.close();
				})

				self.$chooseForm.on('click','.exportBtn',function(event) {
					event.preventDefault();
					self.exportDo();
				})
			},
			exportDo:function() {
				var self = this; 
				if(!$("#endTime").val()) {
					bootbox.alert('请选择结束时间');
					return;
				}
				var startTime = $("#startTime").val()
				var endTime = $("#endTime").val()

				if(!startTime) {
					bootbox.alert('请选择开始时间')
					return;
				}
				if(!endTime) {
					bootbox.alert('请选择结束时间')
					return;
				}

				var startTimeStamp = new Date(startTime + "-01").getTime();
				var endTimeStamp = new Date(endTime + "-01").getTime();

				var requestJson = JSON.stringify({
					"statementOrderCustomerNo":self.props.customerNo,
					"statementOrderStartTime":startTimeStamp,
					"statementOrderEndTime":endTimeStamp
				})

				var commitData = {
					taskType: 1,
					requestJson: requestJson
				}

				Rental.ajax.submit("{0}delayedTask/addDelayedTask".format(SitePath.service), commitData, function(response){
					if(response.success) {
						if(response.resultMap.data.taskStatus == Enum.taskStatus.num.complete && response.resultMap.data.sync == true) {
							$("#openNewTab").attr("href",response.resultMap.data.fileUrl)
							$("#triggerLink").trigger("click");
							Rental.notification.success("导出对账单成功",'<a href="' + response.resultMap.data.fileUrl +'" target="_blank" class="text-muted">下载excel文件</a>');
						} else if(response.resultMap.data.taskStatus == Enum.taskStatus.num.failure && response.resultMap.data.sync == true) {
							Rental.notification.error(response.resultMap.data.remark,'');
						} else if(response.resultMap.data.sync == false) {
							Rental.notification.success("添加导出对账单任务成功",'<a href="' + SitePath.base +'delayed-task/list" style="color:#333;">前往下载</a>');
						}
					} else {
						Rental.notification.error("添加导出对账单任务",response.description || '失败');
					}
				}, null, "添加导出对账单任务", 'dialogLoading');
				Rental.modal.close();
			},
		},
		confirnBadDebetCustomer:{
			init:function(prams) {
				this.props = _.extend({
					customerNo:null,
					customerName:null,
					callBack:function() {},
				}, prams || {});
	
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "customer-manage/bad-debt-customer", type:'ajax', 
					ajaxContentAdded:function() {
						self.initDom();
						self.initEvent();	
					}
				});
			},
			initDom:function(pModal) {
				this.$modal = $("#badDebtCustomerModal");
			},
			initEvent:function() {
				var self = this;
	
				self.renderConfirmInfo();
				
				self.$modal.on("click",".confirmBtn",function(event) {
					event.preventDefault();
					self.confirmFun()
				})
	
				self.$modal.on("click",".cancelButton",function(event) {
					event.preventDefault();
					Rental.modal.close();
				})
			},
			renderConfirmInfo:function() {
				$("#customerName").html(this.props.customerName);
				$("#customerNo").html(this.props.customerNo);
	
				var user = Rental.localstorage.getUser();
				$("#confirmPerson").html(user.realName);
			},
			confirmFun:function() {
				var self = this;
				var des = '确认为坏账客户';
				Rental.ajax.submit("{0}customer/confirmBadAccount".format(SitePath.service),{customerNo:self.props.customerNo},function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						Rental.modal.close();
						self.props.callBack();
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des);
			}
		}
	}

	window.CustomerHandleMixin = CustomerHandleMixin;

})(jQuery);