/**
 * decription: 客户回访记录
 * author:zhangsheng
 * time: 2018-4-4
 */
;(function($) {

	ReturnVisitRecordManage = {
		state:{
			returnListData:null
		},
		init:function(prams) {
			this.props = _.extend({
				no:null,
				customer:null,
			}, prams);

			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.container = $("#returnVisitRecord");
			this.$returnVisitTpl = $("#returnVisitTpl").html();
			this.$returnVisitTable = $("#returnVisitTable");
		},
		initEvent:function() {
			var self = this;
			//添加客户回访
            $("#addReturnVisit").on('click',function() {
                AddReturnVisitRecord.init({
					customerNo:self.props.no,
				    callBack:function() {
						self.doSearch(self.Pager.pagerData.currentPage);
					},
                })
			})
			
			//编辑客户回访
			self.container.on('click','.editReturnVisit',function(event) {
				event.preventDefault();
				var returnVisitId = $(this).data("returnvisitid");
				EditReturnVisitRecord.init({
					customerNo:self.props.no,
					returnVisitId:returnVisitId,
					returnListData:self.state.returnListData,
				    callBack:function() {
						self.doSearch(self.Pager.pagerData.currentPage);
					},
                })
			})

			//删除客户回访
			self.container.on('click','.delReturnVisit',function(event) {
				event.preventDefault();
				var returnVisitId = $(this).data("returnvisitid");
				self.delete(returnVisitId, function(){
					self.doSearch(self.Pager.pagerData.currentPage);
				});
			})

			this.Pager = new Pager();
			this.searchData();
			Rental.ui.events.imgGallery(this.$returnVisitTable);
        },
		searchData:function() {
			var self = this;
			if(!self.props.no) {
				bootbox.alert('找不到客户编号');
				return;
			}
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				customerNo:self.props.no,
			}, {});
			Rental.ajax.ajaxDataNoLoading('customer/pageCustomerReturnVisit', searchData, '查询客户回访记录', function(response) {
				self.state.returnListData = response.resultMap.data;
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderReturnVisitRecord(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			}, $('.pagerContainer', self.container));
		},
		renderReturnVisitRecord:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			$("#returnVistCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.customerReturnVisitImageList);
					},
					"returnVisitImgJSON":function() {
						return Rental.helper.imgListToJSONStringify(this.customerReturnVisitImageList);
					}
				})
			}
			Mustache.parse(self.$returnVisitTpl);
			self.$returnVisitTable.html(Mustache.render(self.$returnVisitTpl, data));
		},
		delete:function(returnVisitId, callBack) {
			var self = this;
			bootbox.confirm('确认删除该回访记录？', function(result) {
				result && self.returnVisitDo(returnVisitId, "删除回访记录", callBack);
			});
		},
		returnVisitDo:function(returnVisitId, des, callBack) {
			if(!returnVisitId) {
				bootbox.alert('找不到回访记录ID');
				return;
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'customer/deleteCustomerReturnVisit'),{returnVisitId:returnVisitId},function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		}
	}

})(jQuery);