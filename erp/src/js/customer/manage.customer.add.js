/**
* 添加个人客户
*/
;(function($){
	var AddCustomer = {
		init:function(callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();	
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_customer_add];
		},
		initDom:function() {
			var self = this;

			this.$form = $("#addCustomerForm");

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {
				}
			});
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});

			self.initCommonEvent({
				isRender:false,
				isInitOwner:true
			});
		},
		add:function() {
			try {
				
				var self = this;
				var formData = Rental.form.getFormData(self.$form);

				// if(formData['personNo'].length > 0 && !Rental.helper.checkIDCard(formData['personNo'])) {
				// 	bootbox.alert('输入身份证号码格式不对，请核对后重新输入');
				// 	return;
				// }

				var commitData = {
					// isDisabled:formData['isDisabled'],
					unionUser:formData['unionUser'],
					owner:formData['owner'],
					firstApplyAmount:formData['firstApplyAmount'],
					laterApplyAmount:formData['laterApplyAmount'],
					statementDate:formData['statementDate'],
					customerPerson:formData,
				}

				Rental.ajax.submit("{0}customer/addPerson".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success("添加客户",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("添加客户",response.description || '失败');
					}
				}, null ,"添加客户", 'dialogLoading');

			} catch (e) {
				Rental.notification.error("添加客户失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加个人客户",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.AddCustomer = _.extend(AddCustomer, CustomerMixin);

})(jQuery);