;(function($){
	
	var EditCustomer = {
		state:{
			no:null,
			customer:null,
		},
		init:function(prams) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_customer_edit];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			var self = this;

			this.$form = $("#editCustomerForm");

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.initCommonEvent({
				isRender:false
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到客户编号');
				return;
			}
			Rental.ajax.submit("{0}customer/detailCustomerPerson".format(SitePath.service),{customerNo:self.state.no},function(response){
				if(response.success) {
					self.state.customer = response.resultMap.data;
					self.initFormData(response.resultMap.data);
				} else {
					Rental.notification.error("加载客户信息",response.description || '失败');
				}
			}, null ,"加载客户信息");
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$form, data);
			Rental.ui.renderFormData(self.$form, data.customerPerson);

			$('[name=realName]', self.$form).prop({disabled:true});

			// self.Area.setDefaultValue({
			// 	province:data.customerPerson.province,
			// 	city:data.customerPerson.city,
			// 	district:data.customerPerson.district,
			// });
			
			self.Area.changeValue({
				province:data.customerPerson.province,
				city:data.customerPerson.city,
				district:data.customerPerson.district,
			});
		},
		edit:function() {
			try {
				
				var self = this;
				var formData = Rental.form.getFormData(self.$form);

				// if(formData['personNo'].length > 0 && !Rental.helper.checkIDCard(formData['personNo'])) {
				// 	bootbox.alert('输入身份证号码格式不对，请核对后重新输入');
				// 	return;
				// }

				formData['realName'] = self.state.customer.customerPerson.realName;

				var commitData = {
					customerNo:self.state.no,
					// isDisabled:formData['isDisabled'],
					unionUser:formData['unionUser'],
					owner:formData['owner'],
					firstApplyAmount:formData['firstApplyAmount'],
					laterApplyAmount:formData['laterApplyAmount'],
					statementDate:formData['statementDate'],
					customerPerson:formData,
				}


				Rental.ajax.submit("{0}customer/updatePerson".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success("编辑客户",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑客户",response.description || '失败');
					}
				}, null ,"编辑客户");

			} catch (e) {
				Rental.notification.error("编辑客户失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑个人客户",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.EditCustomer = _.extend(EditCustomer, CustomerMixin);

})(jQuery);