;(function($){
	var BusinessCustomerDetail = {
		state:{
			no:null,
			customer:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_business_customer_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.manualActionButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var edit = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_edit],
				editSalesman = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_edit_salesman],
				addSubCompany = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_add_subCompany],
				risk = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_risk],
				addAddress = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_address_add],
				manualCharge = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_manual_charge],
				manualDeduct = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_manual_deduct],
				audit = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_audit],
				submitAudit = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_submitAudit],
				disabled = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_disabled],
				settlementDate = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_settlement_date],
				shortRentalUpperLimit = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_short_rental_upper_limit],
				setRiskCreditAmountUsed = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_set_risk_credit_amount_used],
				addReturnVisit = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_return_visit_record_add],
				confirmStatement = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_statement_order_confirm],
				confirmBadDebt = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_confirm_baddebt],
				exportStatement = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_statement_of_account_export];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			// edit && this.actionButtons.push(AuthorUtil.button(_.extend(edit, {menuUrl:'customer-business-manage/edit-after-pass'}),'editAfterPassButton','','编辑业务员&联合开发人'));
			editSalesman && this.actionButtons.push(AuthorUtil.button(editSalesman,'editAfterPassButton','','编辑业务员&联合开发人'));
			addSubCompany && this.actionButtons.push(AuthorUtil.button(addSubCompany,'addSubCompany','','添加子公司'));
			risk && this.actionButtons.push(AuthorUtil.button(risk,'riskButton','','风控授信'));
			addAddress && this.actionButtons.push(AuthorUtil.button(addAddress,'addAddressButton','','添加地址'));
			submitAudit && this.actionButtons.push(AuthorUtil.button(submitAudit,'submitAuditButton','','提交审核'));
			audit && this.actionButtons.push(AuthorUtil.button(audit,'auditRejectButton','','驳回'));
			disabled && this.actionButtons.push(AuthorUtil.button(disabled,'disabledButton','','禁用'));
			disabled && this.actionButtons.push(AuthorUtil.button(disabled,'enabledButton','','启用'));
			settlementDate && this.actionButtons.push(AuthorUtil.button(settlementDate,'settlementDate','','设置结算时间'));
			shortRentalUpperLimit && this.actionButtons.push(AuthorUtil.button(shortRentalUpperLimit, 'shortRentalUpperLimit', '', '设置短租上限金额'));
			setRiskCreditAmountUsed && this.actionButtons.push(AuthorUtil.button(setRiskCreditAmountUsed, 'setRiskCreditAmountUsed', '', '设置客户已用授信额度'));
			addReturnVisit && this.actionButtons.push(AuthorUtil.button(addReturnVisit, 'addReturnVisit', '', '添加客户回访'));
			confirmStatement && this.actionButtons.push(AuthorUtil.button(confirmStatement, 'confirmStatement', '', '结算单数据确认'));
			confirmBadDebt && this.actionButtons.push(AuthorUtil.button(confirmBadDebt,'confirmBadDebt','','确认坏账'));
			exportStatement && this.actionButtons.push(AuthorUtil.button(exportStatement,'exportButton','fa fa-plus','导出对账单'));


			var manualCharge = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_manual_charge], //加款
				manualDeduct = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_manual_deduct]; //扣款

			manualCharge && this.manualActionButtons.push(AuthorUtil.button(manualCharge,'manualChargeButton','','加款'));
			manualDeduct && this.manualActionButtons.push(AuthorUtil.button(manualDeduct,'manualDeductButton','','扣款'));
		},
		initDom:function() {
			this.$form = $("#customerDetailForm");
			this.$customerRiskPanel = $("#customerRiskPanel");
			this.$productFirstListTpl = $("#productFirstNeedTpl").html();
			this.$productFirstListTable = $("#productFirstNeedTable");
			this.$productLaterListTpl = $("#productLaterNeedTpl").html();
			this.$productLaterListTable = $("#productLaterNeedTable");
			this.$customerAddressPannel = $('#customerAddressPannel');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_business_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.loadRentingInfo();

			self.initHandleEvent(this.$form, function() {
				self.loadData();
				ReturnVisitRecordManage.init({no:self.state.no,customer:self.state.customer});
			}, {
				isInitAddressEvent:false,
			});		

			self.initAddressInfo();

			CustomerOrders.init({no:this.state.no});
			riskHistory.init({no:this.state.no});
			ReturnVisitRecordManage.init({no:this.state.no,customer:this.state.customer});
			CustomerOrders.couponList.init({customerNo:self.state.no})
		},
		initAddressInfo:function() {
			var self = this, 
				addAddress = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_address_add];
			CustomerAddressManage.init({
				addAuthor:addAddress,
				editAuthor:addAddress,
				deleteAuthor:addAddress,
				auditAuthor:addAddress,
				customerNo:self.state.no,
				isCustomer:true,
				callBack:function() {
					self.loadData();
				}
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert("没找到客户编号");
				return;
			}
			Rental.ajax.ajaxData('customer/detailCustomerCompany', {customerNo:self.state.no}, '查询客户详细信息', function(response) {
				self.state.customer = response.resultMap.data;
				self.render(response.resultMap.data);
				self.renderProductFirstNeed(response.resultMap.data.customerCompany);
				self.renderProductLaterNeed(response.resultMap.data.customerCompany);
				self.deliveryModeValue(response.resultMap.data);
			});
		},
		render:function(customer) {
			this.renderCustomerHead(customer);
			this.renderActionButton(customer);
			this.renderCustomerRiskManagement(customer);
			this.renderCustomerCompany(customer);
			this.renderOrderImgs(customer);
		},
		renderActionButton:function(customer) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, customer),
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderCustomerHead:function(customer) {
			this.renderCustomer(customer,  $("#customerHeadTpl").html(), $('#customerHead'))
		},
		renderCustomerCompany:function(customer) {
			this.renderCustomer(customer,  $("#customerCompanyInfoTpl").html(), $('#customerCompanyInfo'))
		},
		renderCustomer:function(customer, tpl, container) {
			var self = this;
			Mustache.parse(tpl);
			var data = _.extend(Rental.render, {
				customer:customer,
				manualActionButtons:self.manualActionButtons,
				avatar:function() {
					return '';
				},
				isBadDebtCustomer:function() {
					return this.confirmBadAccountStatus == 1;
				},
				"cutomerTypeValue":function() {
					return Enum.customerType.getValue(this.customerType);
				},
				isDisabledValue:function() {
					return this.isDisabled == 0 ? "否" : "是";
				},
				customerOriginValue:function() {
					return Enum.customerOrigin.getValue(this.customerCompany.customerOrigin);
				},
				customerStatusValue:function() {
					return Enum.customerStatus.getValue(this.customerStatus);
				},
				customerIndustryValue:function() {
					return function(val, render) {
			            return render(val) ? Enum.customerIndustiry.getValue(parseInt(render(val))) : '';
			        } 
				},
				isDefaultConsignAddressValue:function() {
					return this.isDefaultConsignAddress == 0 ? "否" : "是";
				},
				settlementDateValue:function() {
					return this.hasOwnProperty('statementDate')  && !!this.statementDate ? Enum.settlementDate.getValue(this.statementDate) : '月末最后一天';
				},
				shortLimitReceivableAmountValue:function() {
					return this.hasOwnProperty('shortLimitReceivableAmount')  && !!this.shortLimitReceivableAmount ? '￥'+parseFloat(this.shortLimitReceivableAmount).toFixed(2) : '无上限';
				},
				subsidiary:function() {
					return this.customerCompany.subsidiary == true ? "是" : "否";
				},
				parentCompanyUrl:function() {
					return this.customerCompany.hasOwnProperty('parentCustomerNo') ? '{0}?no={1}'.format(PageUrl.commonCustomerDetail,  this.customerCompany.parentCustomerNo) : "";
				}
			})
			container.html(Mustache.render(tpl, data));
		},
		renderCustomerRiskManagement:function(customer) {
			var self = this;
			var data = _.extend(Rental.render, {
				customerRiskManagement:customer.customerRiskManagement || new Object(),
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				applePayModeValue:function() {
					return Enum.payMode.getValue(this.applePayMode);
				},
				newPayModeValue:function() {
					return Enum.payMode.getValue(this.newPayMode);
				},
				customerno:function() {
					return customer.customerNo;
				},
				customerType:function() {
					return customer.customerType;
				},
				riskButton:function() {
					var risk = self.currentManageListAuthor.children[AuthorCode.manage_business_customer_risk];
					return risk ? AuthorUtil.button(risk,'riskButton','','风控授信') :  new Object();
				},
				singleLimitPriceValue:function() {
					return this.hasOwnProperty('singleLimitPrice')  && _.isNumber(this.singleLimitPrice) ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
				}
			})
			var customerStatusTpl = $("#customerRiskPanelTpl").html();
			Mustache.parse(customerStatusTpl);
			this.$customerRiskPanel.html(Mustache.render(customerStatusTpl, data));
		},
		renderCustomerAddress:function(customer) {
			var customerAddressTpl = $("#customerAddressTpl").html();
			Mustache.parse(customerAddressTpl);
			var data = {
				userConsignInfoList:customer.userConsignInfoList,
			}
			$('#customerAddress').html(Mustache.render(customerAddressTpl, data));
		},
		renderOrderImgs:function(customer) {
			var self = this;
			self.state.dropzoneOnlyOne.forEach(function(item, index) {
				var img = customer.customerCompany[item];
				if(img) {
					self.renderImgBox(img, self.state.dropzoneOnlyOneName[index]);
				}
			});
			self.state.dropzoneOnlyMany.forEach(function(item, index) {
				var imgList = customer.customerCompany[item];
				if(imgList) {
					imgList.forEach(function(item) {
						self.renderImgBox(item, self.state.dropzoneOnlyManyName[index]);
					})
				}
			});

			$('#customerImgNano').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true,
                // scrollTo: $('input[type=checkbox]',$this).filter(':checked').eq(0),
            });

            // $('.custom-img-link').magnificPopup({
			// 	type: 'image'
			// 	// other options
			// });
			$("#mix-container").viewer()
		},
		renderImgBox:function(img, des) {
			var self = this,
				renderData = {
					img:img,
					url:function() {
						return this.imgDomain + this.imgUrl;
					},
					des:function() {
						return des;
					}
				};

			var tpl = $("#imgBoxTpl").html();
			Mustache.parse(tpl);
			var imgBox = Mustache.render(tpl, renderData);
			$('#mix-container').append(imgBox)
			$("#noimgbox").hide();
		},
		loadRentingInfo:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('customer/queryRentCountByCustomerNo', {customerNo:self.state.no}, '查询客户在租信息', function(response) {
				self.renderRentingInfo(response.resultMap.data);
			});
		},
		renderRentingInfo:function(rentingData) {
			var self = this;
			var rentingInfoTpl = $("#rentingInfoTpl").html();
			var rentingData = (rentingData.hasOwnProperty("rentProductCount") || rentingData.hasOwnProperty("rentMaterialCount") || rentingData.hasOwnProperty("rentAmountMaterialCount")) ? rentingData : [];
			var data = {
				rentingData: rentingData
			}
			Mustache.parse(rentingInfoTpl);
			$("#rentingInfoTable").html(Mustache.render(rentingInfoTpl, data));
		},
		renderProductFirstNeed:function(order) {
			var self = this;
			var listData = order.hasOwnProperty("customerCompanyNeedFirstList") ? order.customerCompanyNeedFirstList : [];
			var hasFirstTotalPrice = order.hasOwnProperty("firstListTotalPrice") && order.firstListTotalPrice > 0;
			$("#firstNeedCount").html(listData.length)
			var data = {
				dataSource:_.extend(Rental.render, {
					"hasFirstTotalPrice":hasFirstTotalPrice,
					"firstTotalPrice":order.firstListTotalPrice,
					"listData":listData,
					productName:function() {
						return this.product.productName
					},
					brandName:function() {
						return this.product.brandName
					},
					categoryName:function() {
						return this.product.categoryName
					},
					propertiesToStr:function() {
						var product = this.hasOwnProperty("product") ? this.product : "";
						var skuList = !!product && product.hasOwnProperty("productSkuList") ? product.productSkuList : "";
						var str = !!skuList && skuList[0].hasOwnProperty('productSkuPropertyList') ? skuList[0].productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType)
					},
					isNewIntValue:function() {
						if(this.hasOwnProperty('isNewProduct')) {
							return this.isNewProduct;
						} else if(this.hasOwnProperty('isNew')) {
							return this.isNew;
						}
					},
				})
			}
			Mustache.parse(self.$productFirstListTpl);
			self.$productFirstListTable.html(Mustache.render(self.$productFirstListTpl, data));
		},
		renderProductLaterNeed:function(order) {
			var self = this;
			var listData = order.hasOwnProperty("customerCompanyNeedLaterList") ? order.customerCompanyNeedLaterList : [];
			var hasLaterTotalPrice = order.hasOwnProperty("firstListTotalPrice") && order.laterListTotalPrice > 0;
			$("#lastNeedCount").html(listData.length)
			var data = {
				dataSource:_.extend(Rental.render, {
					"hasLaterTotalPrice":hasLaterTotalPrice,
					"laterTotalPrice":order.laterListTotalPrice,
					"listData":listData,
					productName:function() {
						return this.product.productName
					},
					brandName:function() {
						return this.product.brandName
					},
					categoryName:function() {
						return this.product.categoryName
					},
					propertiesToStr:function() {
						var product = this.hasOwnProperty("product") ? this.product : "";
						var skuList = !!product && product.hasOwnProperty("productSkuList") ? product.productSkuList : "";
						var str = !!skuList && skuList[0].hasOwnProperty('productSkuPropertyList') ? skuList[0].productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType)
					},
					isNewIntValue:function() {
						if(this.hasOwnProperty('isNewProduct')) {
							return this.isNewProduct;
						} else if(this.hasOwnProperty('isNew')) {
							return this.isNew;
						}
					},
				})
			}
			Mustache.parse(self.$productLaterListTpl);
			self.$productLaterListTable.html(Mustache.render(self.$productLaterListTpl, data));
		},
		deliveryModeValue:function(order) {
			if(order.deliveryMode) {
				function returndeliveryValue() {
					var deliveryMode = order.deliveryMode;
					switch(deliveryMode) {
						case 1:
							return "快递";
						case 2:
							return "自提";
						case 3:
							return "凌雄配送";
					}
				}
				$("#deliveryMode").html("(送货方式：" + returndeliveryValue() + ")");
			} else {
				$("#deliveryMode").css("display","none");
			}
		},
	};
	
	window.BusinessCustomerDetail = _.extend(BusinessCustomerDetail, CustomerHandleMixin, CustomerMixin);

})(jQuery);