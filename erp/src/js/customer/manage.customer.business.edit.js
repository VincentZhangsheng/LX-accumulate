;(function($){
	
	var EditBusinessCustomer = {
		state:{
			no:null,
			customer:null,
			chooseProductList:new Array(),
			order:new Object(),
			chooseLaterProductList:new Array(),
			laterOrder:new Object(),
			customerConsignInfoList: new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_business_customer_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_business_customer_edit];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			var self = this;
			this.$form = $("#editBusinessCustomerForm");
			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.$dataListTpl = $("#dataListTpl").html(); 
			this.$dataListTable = $("#dataListTable"); 
			this.$laterProductTpl = $("#productLaterNeedTpl").html();
			this.$laterProductTable = $("#productLaterNeedTable");

			this.$customerAddressPannel = $('#customerAddressPannel');

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_business_customer_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.initCommonEvent();
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到企业客户编号');
				return;
			}
			Rental.ajax.ajaxData('customer/detailCustomerCompany', {customerNo:self.state.no}, '获取企业客户详细信息', function(response) {
				self.state.customer = response.resultMap.data;
				self.initFormData(response.resultMap.data);
				self.intFormDataImgs(response.resultMap.data);
				self.initChooseProductList(response.resultMap.data);
				self.initChooseLaterProductList(response.resultMap.data);
				self.renderFirstTotalPrice(response.resultMap.data);
				self.renderLaterTotalPrice(response.resultMap.data);
				self.initAddressList(response.resultMap.data.customerCompany);
			});
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$form, data);
			Rental.ui.renderFormData(self.$form, data.customerCompany);
			
			data.customerCompany.hasOwnProperty("companyFoundTime") && $('[name=companyFoundTime]', self.$form).val(new Date(data.customerCompany.companyFoundTime).format('yyyy-MM-dd'));

			$('[name=isLegalPersonApple]', self.$form).val(data.customerCompany.isLegalPersonApple);
			if(data.customerCompany.isLegalPersonApple == 1) {
				$("#legalPersonContainer").hide();
			} 

			$('[name=companyName]').prop({disabled:true});

			$('[name=isDefaultConsignAddress]', self.$form).prop('checked',data.isDefaultConsignAddress == 1 ? true:false);

			self.Area.changeValue({
				province:data.customerCompany.province,
				city:data.customerCompany.city,
				district:data.customerCompany.district,
			});
		},
		intFormDataImgs:function(data) {
			var self = this;
			self.state.dropzoneOnlyOne.forEach(function(item) {
				var img = data.customerCompany[item];
				if(img) {
					self.initImg(img, $('#'+item));
				}
			});
			self.state.dropzoneOnlyMany.forEach(function(item) {
				var imgList = data.customerCompany[item];
				if(imgList) {
					self.initImgList(imgList, $('#'+item));
				}
			});
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.initProductList(data.customerCompany.customerCompanyNeedFirstList)
			this.renderProductList();
		},
		initProductList:function(data) {
			if(!data) return;
			var  orderItemsMapByProductId = data.reduce(function(pre,item) {
				var product =  _.extend(item, item.product, item.product.productSkuList[0]);
				if(!pre[item.productId]) {
					pre[item.productId] = {
						chooseProductSkuList:new Array()
					};
				}
				pre[item.productId].chooseProductSkuList.push(product);
				pre[item.productId] = _.extend(pre[item.productId], product);
		
				return pre;
			},{});

			var chooseProductList = _.values(orderItemsMapByProductId);
			return chooseProductList;
		},
		initAddressList:function(data) {
			if(!data.customerConsignInfoList) return;
			this.state.customerConsignInfoList = data.customerConsignInfoList;
			this.renderAddress();
		},
		initChooseLaterProductList:function(data) {
			this.state.chooseLaterProductList = this.initProductList(data.customerCompany.customerCompanyNeedLaterList);
			this.renderLaterProductList();
		},
		edit:function() {
			try {
				var self = this,
					formData = Rental.form.getFormData(self.$form),
					isDefaultConsignAddress = formData['isDefaultConsignAddress'];
					// isDefaultConsignAddress = $('[name=isDefaultConsignAddress]', self.$form).prop('checked');

				// formData['isDefaultConsignAddress'] = isDefaultConsignAddress ? 1 : 0;

				// if(formData['legalPersonNo'].length > 0 && !Rental.helper.checkIDCard(formData['legalPersonNo'])) {
				// 	bootbox.alert('输入法人身份证号码格式不对，请核对后重新输入');
				// 	return;
				// }

				// if(formData['agentPersonNo'].length > 0 && !Rental.helper.checkIDCard(formData['agentPersonNo'])) {
				// 	bootbox.alert('输入经办人身份证号码格式不对，请核对后重新输入');
				// 	return;	
				// }

				if(formData['connectRealName'] == formData['agentPersonName']  || formData['agentPersonPhone'] == formData['connectPhone']) {
					bootbox.alert('紧急联系人方式和经办人联系方式不能相同');
					return;		
				}

				if(formData['isLegalPersonApple'].toString() == "1") {
					formData['legalPerson'] = formData['agentPersonName'];
					formData['legalPersonNo'] = formData['agentPersonNo'];
					formData['legalPersonPhone'] = formData['agentPersonPhone'];
				} else {
					if(formData['legalPerson'] == formData['agentPersonName'] || formData['legalPersonNo'] == formData['agentPersonNo'] || formData['legalPersonPhone'] == formData['agentPersonPhone']) {
						bootbox.alert('经办人联系方式和法人信息不能相同');
						return;
					}
				}

				//选择经营地址与收货地址一致时，联系人信息必须填写完整
				if(formData['isDefaultConsignAddress'] == 1) {
					if(!formData['connectRealName'] || !formData['connectPhone']) {
						bootbox.alert('请完善紧急联系人信息');
						return;
					}
					if(!formData['province'] || !formData['address']) {
						bootbox.alert('请完善地址信息');
						return;
					}
				}

				//收货地址必填
				var addressList = self.state.customerConsignInfoList && self.state.customerConsignInfoList.length > 0;
				if(!addressList) {
					bootbox.alert('请填写收货地址');
					return;
				}

				formData['companyName'] = self.state.customer.hasOwnProperty("customerCompany") ? self.state.customer.customerCompany.companyName : "";

				var commitData = {
					customerNo:self.state.no,
					unionUser:formData['unionUser'],
					owner:formData['owner'],
					firstApplyAmount:formData['firstApplyAmount'],
					laterApplyAmount:formData['laterApplyAmount'],
					deliveryMode:formData['deliveryMode'],
					shortLimitReceivableAmount:formData['shortLimitReceivableAmount'],
					isDefaultConsignAddress:formData['isDefaultConsignAddress'],
					statementDate:formData['statementDate'],
					customerCompany:formData,
				}

				var onlyValidate = true;
				self.state.dropzoneOnlyOne.forEach(function(item, index) {
					var result = self.getImageList($('#' + item), self.state.dropzoneOnlyOneName[index]);
					if(_.isArray(result) && result.length > 0) {
						commitData.customerCompany[item] = result[0];
					} else if(_.isBoolean(result)) {
						onlyValidate = result;	
					}
				});

				if(!onlyValidate) {
					bootbox.alert('请检查有未上传成功的图片');
					return;
				}

				var manyValidate = true;
				self.state.dropzoneOnlyMany.forEach(function(item, index) {
					var result = self.getImageList($('#' + item), self.state.dropzoneOnlyManyName[index]);
					if(_.isArray(result) && result.length > 0) {
						commitData.customerCompany[item] = result;
					} else if(_.isBoolean(result)) {
						manyValidate = result;	
					}
				});

				if(!manyValidate) {
					bootbox.alert('请检查有未上传成功的图片');
					return;
				}

				//首次所需商品
				commitData.customerCompany.customerCompanyNeedFirstList = new Array();
				if(!!self.state.chooseProductList && self.state.chooseProductList.length > 0) {
					self.state.chooseProductList.forEach(function(product) {
						_.isArray(product.chooseProductSkuList) && product.chooseProductSkuList.forEach(function(sku) {
							commitData.customerCompany.customerCompanyNeedFirstList.push({
								skuId:sku.skuId,
								rentCount:sku.rentCount,
								isNew:sku.isNew,
								rentType:sku.rentType,
								rentTimeLength:sku.rentTimeLength
							})
						})
					})
				}

				//后续所需商品
				commitData.customerCompany.customerCompanyNeedLaterList = new Array();
				if(!!self.state.chooseLaterProductList && self.state.chooseLaterProductList.length > 0) {
					self.state.chooseLaterProductList.forEach(function(product){
						_.isArray(product.chooseProductSkuList) &&	product.chooseProductSkuList.forEach(function(sku){
							commitData.customerCompany.customerCompanyNeedLaterList.push({
								skuId:sku.skuId,
								rentCount:sku.rentCount,
								isNew:sku.isNew,
								rentType:sku.rentType,
								rentTimeLength:sku.rentTimeLength
							})
						})
					})
				}

				//收货地址
				commitData.customerCompany.customerConsignInfoList = new Array();
				if(self.state.customerConsignInfoList.length > 0) {
					commitData.customerCompany.customerConsignInfoList = self.state.customerConsignInfoList
				}

				var des = "编辑企业客户";
				Rental.ajax.submit("{0}customer/updateCompany".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null ,des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑企业客户",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } 
				    }
				});
			}
			return false;
		},
		renderFirstTotalPrice:function(data) {
			$("#firstListTotalPriceBox").html("￥" + (data.customerCompany.hasOwnProperty("firstListTotalPrice") ? parseFloat(data.customerCompany.firstListTotalPrice).toFixed(2) : "0.00"));
			$("#firstApplyAmount").val(data.hasOwnProperty("firstApplyAmount") ? data.firstApplyAmount : "0")
		},
		renderLaterTotalPrice:function(data) {
			$("#laterListTotalPriceBox").html("￥" + (data.customerCompany.hasOwnProperty("laterListTotalPrice") ? parseFloat(data.customerCompany.laterListTotalPrice).toFixed(2) : "0.00"));
			$("#laterApplyAmount").val(data.hasOwnProperty("laterApplyAmount") ? data.laterApplyAmount : "0")
		}
	};

	window.EditBusinessCustomer = _.extend(EditBusinessCustomer, CustomerMixin);

})(jQuery);