;(function($) {

	var RechargeList = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_recharge_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function() {
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#timePicker'), $('#timePickerInput'),  $("#queryStartTime"), $("#queryEndTime"));

			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#chargeType'),
				data:Enum.array(Enum.chargeType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（支付类型）'
			});

			Rental.ui.renderSelect({
				container:$('#chargeStatus'),
				data:Enum.array(Enum.chargeStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（充值状态）'
			});

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("#subCompanyId"),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

			self.searchData();  //初始化列表数据

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			Rental.ajax.ajaxData('payment/queryChargeRecordParamPage', searchData, '加载收入列表', function(response) {
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				data = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						isErpCustomerVal:function() {
							return this.hasOwnProperty("erpCustomerNo") && !!this.erpCustomerNo;
						},
						customerDetailUrl:function() {
							var customerNo = this.hasOwnProperty("erpCustomerNo") && !!this.erpCustomerNo ? this.erpCustomerNo : "";
							return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, customerNo);
						},
						chargeTypeValue:function(){
							return Enum.chargeType.getValue(this.chargeType)
						},
						chargeStatusValue:function(){
							return Enum.chargeStatus.getValue(this.chargeStatus)
						}
					})
				}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.RechargeList = RechargeList;

})(jQuery);