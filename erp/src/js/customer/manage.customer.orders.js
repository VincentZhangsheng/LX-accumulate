/**
* 客户
* 1. 销售订单
* 2. 退货单
*/
;(function($) {

	CustomerOrders = {
		init:function(prams) {
			this.props = _.extend({
				no:null,
			}, prams);

			this.initDom();
			this.initEvent();
		},
		initDom:function() {
		},
		initEvent:function() {
			this.order.init({customerNo:this.props.no});
			this.returnOrder.init({customerNo:this.props.no});
			this.rechargeList.init({customerNo:this.props.no});
			this.accountList.init({customerNo:this.props.no});
			this.subsidiaryList.init({customerNo:this.props.no});
		},
		order:{
			init:function(prams) {
				this.no = prams.customerNo;
				this.initDom();
				this.initEvent();
			},
			initDom:function() {
				this.container = $("#orderList");
				this.$dataListTpl = $("#orderListTpl").html();
				this.$dataListTable = $("#orderListTable");
			},
			initEvent:function() {
				this.Pager = new Pager();
				this.searchData()
			},
			searchData:function(prams) {
				var self = this;
				if(!self.no) {
					bootbox.alert('找不到客户编号');
					return;
				}
				var searchData = $.extend({
					pageNo:1,
					pageSize:self.Pager.defautPrams.pageSize,
					buyerCustomerNo:self.no,
				}, prams || {});
				Rental.ajax.ajaxDataNoLoading('order/queryAllOrder', searchData, '加载订单列表', function(response) {
					self.render(response.resultMap.data);	
				});
			},
			doSearch:function(pageNo) {
				this.searchData({pageNo:pageNo || 1});
			},
			render:function(data) {
				var self = this;
				self.renderList(data);
				self.Pager.init(data,function(pageNo) {
					self.doSearch(pageNo)
				}, $('.pagerContainer', self.container));
			},
			renderList:function(data) {
				var self = this;
				
				var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
					hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
					hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

				$("#orderCount").html(listData.length)
				var data = {
					hasCommonActionButtons:hasCommonActionButtons,
					hasRowActionButtons:hasRowActionButtons,
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						orderDetailUrl:function() {
							return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
						},
						rowActionButtons:function() {
							return self.filterAcitonButtons(self.rowActionButtons, this);
						},
						"orderStatusStr":function(){
							return Enum.orderStatus.getValue(this.orderStatus);
						},
						orderStatusClass:function() {
							return Enum.orderStatus.getClass(this.orderStatus);
						},
						payStatusValue:function() {
							return Enum.payStatus.getValue(this.payStatus);
						},
						payStatusClass:function() {
							return Enum.payStatus.getClass(this.payStatus);
						},
						"rentTypeStr":function() {
							return Enum.rentType.getValue(this.rentType);
						},
					})
				}
				Mustache.parse(this.$dataListTpl);
				this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
			},		
		},
		returnOrder:{
			init:function(prams) {
				this.no = prams.customerNo;
				this.initDom();
				this.initEvent();
			},
			initDom:function() {
				this.container = $("#returnOrder");
				this.$dataListTpl = $("#returnOrderListTpl").html();
				this.$dataListTable = $("#returnOrderListTable");
			},
			initEvent:function() {
				this.Pager = new Pager();
				this.searchData()
			},
			searchData:function(prams) {
				var self = this;
				if(!self.no) {
					bootbox.alert('找不到客户编号');
					return;
				}
				var searchData = $.extend({
					pageNo:1,
					pageSize:self.Pager.defautPrams.pageSize,
					k3CustomerNo:self.no,
				}, prams || {});
				Rental.ajax.ajaxDataNoLoading('k3/queryReturnOrder', searchData, '加载退货单列表', function(response) {
					self.render(response.resultMap.data);	
				});
			},
			doSearch:function(pageNo) {
				this.searchData({pageNo:pageNo || 1});
			},
			render:function(data) {
				var self = this;
				self.renderList(data);
				self.Pager.init(data,function(pageNo) {
					self.doSearch(pageNo)
				}, $('.pagerContainer', self.container));
			},
			renderList:function(data) {
				var self = this;
				
				var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
					hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
					hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

				$("#returnOrderCount").html(listData.length)
				var data = {
					hasCommonActionButtons:hasCommonActionButtons,
					hasRowActionButtons:hasRowActionButtons,
					dataSource: _.extend(Rental.render, {
						"listData":listData,
						returnOrderUrl:function() {
							return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, this.returnOrderNo);
						},
						rowActionButtons:function() {
							return self.filterAcitonButtons(self.rowActionButtons, this);
						},
						isChargingStr:function() {
							return this.isCharging == 1 ? "是":"否";
						},
						returnOrderStatusValue:function(){
							return Enum.returnOrderStatus.getValue(this.returnOrderStatus);
						},
						returnOrderStatusClass:function() {
							return Enum.returnOrderStatus.getClass(this.returnOrderStatus);	
						}
					}),
				}
				Mustache.parse(this.$dataListTpl);
				this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
			},	
		},
		rechargeList:{
			init:function(prams) {
				this.no = prams.customerNo;
				this.initDom();
				this.initEvent();
			},
			initDom:function() {
				this.container = $("#rechargeList");
				this.$dataListTpl = $("#rechargeListTpl").html();
				this.$dataListTable = $("#rechargeListTable");
			},
			initEvent:function() {
				this.Pager = new Pager();
				this.searchData()
			},
			searchData:function(prams) {
				var self = this;
				if(!self.no) {
					bootbox.alert('找不到客户编号');
					return;
				}
				var searchData = $.extend({
					pageNo:1,
					pageSize:self.Pager.defautPrams.pageSize,
					businessCustomerNo:self.no,
				}, prams || {});
				Rental.ajax.ajaxDataNoLoading('payment/queryChargeRecordParamPage', searchData, '加载客户充值记录', function(response) {
					self.render(response.resultMap.data);	
				});
			},
			doSearch:function(pageNo) {
				this.searchData({pageNo:pageNo || 1});
			},
			render:function(data) {
				var self = this;
				self.renderList(data);
				self.Pager.init(data,function(pageNo) {
					self.doSearch(pageNo)
				}, $('.pagerContainer', self.container));
			},
			renderList:function(data) {
				var self = this,
					listData = data.hasOwnProperty("itemList") ? data.itemList : [];

				$("#rechargeCount").html(listData.length)
				var data = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						chargeTypeValue:function(){
							return Enum.chargeType.getValue(this.chargeType)
						},
						chargeStatusValue:function(){
							return Enum.chargeStatus.getValue(this.chargeStatus)
						}
					})
				};

				Mustache.parse(this.$dataListTpl);
				this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
			},	
		},
		accountList:{
			init:function(prams) {
				this.no = prams.customerNo;
				this.initDom();
				this.initEvent();
			},
			initDom:function() {
				this.container = $("#accountList");
				this.$totalAccountTpl = $("#totalAccountTpl").html();
				this.$totalAccount = $("#totalAccount");
				this.$dataListTpl = $("#accountListTpl").html();
				this.$dataListTable = $("#accountListTable");
			},
			initEvent:function() {
				this.Pager = new Pager();
				this.searchData();
				
			},
			searchData:function(prams) {
				var self = this;
				if(!self.no) {
					bootbox.alert('找不到客户编号');
					return;
				}
				var searchData = $.extend({
					pageNo:1,
					pageSize:self.Pager.defautPrams.pageSize,
					businessCustomerNo:self.no,
				}, prams || {});
				Rental.ajax.ajaxDataNoLoading('payment/queryCustomerAccountLogPage', searchData, '加载订单列表', function(response) {
					self.renderTotalAccount(response.resultMap.data);
					self.render(response.resultMap.data.customerAccountLogPage);
				});
			},
			doSearch:function(pageNo) {
				this.searchData({pageNo:pageNo || 1});
			},
			renderTotalAccount:function(account) {
				var self = this;
				var totalAccount = account.hasOwnProperty("customerAccountLogSummary") ? account.customerAccountLogSummary : {};
				var hasTotalAccount = account.hasOwnProperty("customerAccountLogSummary") ? true : false;

				var data = _.extend(Rental.render, {
					hasTotalAccount:hasTotalAccount,
					totalAccount:totalAccount,
					recordType:function() {
						return Enum.customerAccountLogType.getValue(this.customerAccountLogType);
					}
				})
				Mustache.parse(this.$totalAccountTpl);
				this.$totalAccount.html(Mustache.render(this.$totalAccountTpl, data));
			},
			render:function(data) {
				var self = this;
				self.renderList(data);
				self.Pager.init(data,function(pageNo) {
					self.doSearch(pageNo)
				}, $('.pagerContainer', self.container));
			},
			renderList:function(data) {
				var self = this;
				var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
				var	hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

				$("#accountCount").html(listData.length)
				var data = {
					hasCommonActionButtons:hasCommonActionButtons,
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						operation:function() {
							if(this.customerAccountLogType == Enum.customerAccountLogType.num.charge || this.customerAccountLogType == Enum.customerAccountLogType.num.balancePaid) {
								return "-";
							}
							return "";
						},
						recordType:function() {
							return Enum.customerAccountLogType.getValue(this.customerAccountLogType);
						},
					})
				}
				Mustache.parse(this.$dataListTpl);
				this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
			},	
		},
		couponList:{
			init:function(prams) {
				this.no = prams.customerNo;
				this.initDom();
				this.initEvent();
			},
			initDom:function() {
				this.container = $("#couponList");
				this.$dataListTpl = $("#couponListTpl").html();
				this.$dataListTable = $("#couponListTable");
			},
			initEvent:function() {
				this.Pager = new Pager();
				this.searchData();
			},
			searchData:function(prams) {
				var self = this;
				if(!self.no) {
					bootbox.alert('找不到客户编号');
					return;
				}
				var searchData = $.extend({
					pageNo:1,
					pageSize:self.Pager.defautPrams.pageSize,
					customer:{customerNo:self.no},
				}, prams || {});
				Rental.ajax.ajaxDataNoLoading('coupon/pageCouponByCustomerNo', searchData, '加载优惠券', function(response) {
					self.render(response.resultMap.data);
				});
			},
			doSearch:function(pageNo) {
				this.searchData({pageNo: pageNo || 1})
			},
			render:function(data) {
				var self = this;
				self.renderList(data);
				self.Pager.init(data,function(pageNo) {
					self.doSearch(pageNo)
				}, $('.pagerContainer', self.container));
			},
			renderList:function(data) {
				var self = this;
				var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

				$("#couponCount").html(listData.length);
				var couponData = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						couponStatusValue:function() {
							return self.getCouponStatus(this.couponStatus)
						},
						isOnlineValue:function() {
							return self.getIsOnline(this.isOnline)
						},
						productNameValue:function() {
							return this.hasOwnProperty("productName") ? this.productName : "";
						},
						orderNoValue:function() {
							return this.hasOwnProperty("orderNo") ? this.orderNo : "";
						}
					})
				}
				Mustache.parse(this.$dataListTpl);
				this.$dataListTable.html(Mustache.render(this.$dataListTpl, couponData));
			},
			getIsOnline:function(str){
				switch(str) {
					case 0:
						return "否"
					case 1:
						return "是";
					default:
						return "";
				}
			},
			getCouponStatus:function(str){
				switch(str) {
					case 0:
						return "未领取";
					case 4:
						return "可用";
					case 8:
						return "已用";
					default:
						return "";
				}
			}
		},
		subsidiaryList:{
			init:function(prams) {
				this.no = prams.customerNo;
				this.initDom();
				this.initEvent();
			},
			initDom:function() {
				this.container = $("#subsidiaryList");
				this.$dataListTpl = $("#subsidiaryTpl").html();
				this.$dataListTable = $("#subsidiaryTable");
			},
			initEvent:function() {
				this.Pager = new Pager();
				this.searchData();
			},
			searchData:function(prams) {
				var self = this;
				if(!self.no) {
					bootbox.alert('找不到客户编号');
					return;
				}
				var searchData = $.extend({
					pageNo:1,
					pageSize:self.Pager.defautPrams.pageSize,
					customerNo:self.no,
				}, prams || {});
				Rental.ajax.ajaxDataNoLoading('customer/queryParentCompanyPage', searchData, '加载子公司', function(response) {
					self.render(response.resultMap.data);
				});
			},
			doSearch:function(pageNo) {
				this.searchData({pageNo: pageNo || 1})
			},
			render:function(data) {
				var self = this;
				self.renderList(data);
				self.Pager.init(data,function(pageNo) {
					self.doSearch(pageNo)
				}, $('.pagerContainer', self.container));
			},
			renderList:function(data) {
				var self = this;
				var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

				$("#subsidiaryCount").html(data.totalCount);
				var couponData = {
					dataSource:_.extend(Rental.render, {
						"listData":listData,
						isBadDebtCustomer:function() {
							return this.confirmBadAccountStatus == 1;
						},
						detailUrl:function() {
							return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.customerNo);
						},
						"isDisabledStr":function() {
							return this.isDisabled == 1 ? "是":"否";
						},
						addressForamt:function() {
							return (this.customerCompany.provinceName || '') + (this.customerCompany.cityName  || '') + (this.customerCompany.districtName  || '') + (this.customerCompany.address  || '');
						},
						customerStatusValue:function() {
							return Enum.customerStatus.getValue(this.customerStatus);
						},
					})
				}
				Mustache.parse(this.$dataListTpl);
				this.$dataListTable.html(Mustache.render(this.$dataListTpl, couponData));
			},
		}
	}

})(jQuery);