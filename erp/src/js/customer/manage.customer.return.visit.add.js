/**
 * decription: 添加客户回访记录
 * author:zhangsheng
 * time: 2018-4-4
 */

;(function($){
	
	var AddReturnVisitRecord = {
		state:{
			dropzoneOnlyMany:'customerReturnVisitImageList',
			dropzoneOnlyManyName:'客户回访图片'
		},
		init:function(prams) {
			this.props = _.extend({
				customerNo:null,
				customerName:null,
				callBack:function() {},
			}, prams || {});
			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-manage/returnVisitModal", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			var self = this;
			this.$modal = pModal.container;
			this.$form = $("#returnVisitForm");
			this.$cancelButton = $('.cancelButton', this.$modal);
		},
		initEvent:function() {
			var self = this;

			$("#customerNo").html(self.props.customerNo);
			$("#customerName").html(self.props.customerName || $("#customerHead .customerName").data("customername"));

			self.initDropzone();

			Rental.form.initFormValidation(self.$form, function(form){
				self.add();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initDropzone:function() {
			Rental.ui.events.initDropzone($('#'+this.state.dropzoneOnlyMany), 'image/upload'); 
		},
		getImageList:function(container, des) {
			var returnVisitImageList = new Array(), $preview = $('.dz-image-preview', container);
			$preview.each(function(index, el) {
				var img = $(el).data('img');
				if(img) returnVisitImageList.push({imgId:img.imgId});
			});
			if($preview.size() > 0 && $preview.size() != returnVisitImageList.length) {
				Rental.notification.error(des || '资料上传', '有未上传成功的图片，请检查');
				return false;
			}
			return returnVisitImageList;
		},
		add:function() {
			try {
				var self = this;
				var formData = Rental.form.getFormData(self.$form);
				formData.customerNo = self.props.customerNo;

				if(!formData.returnVisitDescribe) {
					bootbox.alert('请填写回访描述');
					return;
				}

				var manyValidate = true;
				var result = self.getImageList($('#' + self.state.dropzoneOnlyMany), self.state.dropzoneOnlyManyName);
				if(_.isArray(result) && result.length > 0) {
					formData[self.state.dropzoneOnlyMany] = result;
				} else if(_.isBoolean(result)) {
					manyValidate = result;	
				}

				if(!manyValidate) {
					bootbox.alert('请检查有未上传成功的图片');
					return;
				}

				var des = '添加客户回访';
				Rental.ajax.submit("{0}customer/addCustomerReturnVisit".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null , des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.AddReturnVisitRecord = AddReturnVisitRecord;

})(jQuery);