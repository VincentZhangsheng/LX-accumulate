;(function($){
	
	var AddCustomerAddress = {
		init:function(prams) {
			this.props = _.extend({
				customerConsignInfoId:null,
				customerNo:null,
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/customer-consign-info/add", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			var self = this;

			this.$form = $("#addCustomerAddressForm");
			this.$cancelButton = $('.cancelButton', this.$form);

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		add:function() {
			try {
				
				var self = this,
					tip = '添加客户收货地址',
					isMain = $('[name=isMain]', self.$form).prop('checked'),
					formData = Rental.form.getFormData(self.$form);

				formData.isMain = isMain ? 1 : 0;
				formData.customerNo = self.props.customerNo;

				Rental.ajax.submit("{0}customer/addCustomerConsignInfo".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(tip,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(tip,response.description || '失败');
					}
				}, null ,tip, "dialogLoading");

			} catch (e) {
				Rental.notification.error(tip, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack(response.resultMap.data);
			}
			return false;
		}
	};

	window.AddCustomerAddress = AddCustomerAddress;

})(jQuery);