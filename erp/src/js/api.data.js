;(function($) {

	var ApiData = {
		/**
		* author:weblee
		* update date:2017-12-30
		* description:获取公司列表
		**/
		company:function(prams) {
			var self = this;

			var searchData = _.extend({
				pageNo:1,
				pageSize:10000,
			}, prams.searchData || {});

			Rental.ajax.ajaxDataNoLoading('company/pageSubCompany', searchData, '加载公司列表', function(response) {
				prams.success && prams.success(response.resultMap.data.itemList);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2017-12-30
		* description:获取仓库列表
		**/
		warehouse:function(prams) {
			var self = this;

			var searchData = _.extend({
				pageNo:1,
				pageSize:10000,
			}, prams.searchData || {});

			Rental.ajax.ajaxDataNoLoading('warehouse/getWarehousePage', searchData, '加载仓库列表', function(response) {
				prams.success && prams.success(response.resultMap.data.itemList);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2017-12-30
		* description:加载品牌列表
		**/
		brand:function(prams) {
			var self = this;

			var searchData = _.extend({
				pageNo:1,
				pageSize:10000,
			}, prams.searchData || {});

			Rental.ajax.ajaxDataNoLoading('basic/queryAllBrand', searchData, '加载品牌列表', function(response) {
				prams.success && prams.success(response.resultMap.data.itemList);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2017-12-30
		* description:加载类别列表
		**/
		category:function(prams) {
			var self = this;

			var searchData = _.extend({
				
			}, prams.searchData || {});

			Rental.ajax.ajaxDataNoLoading('product/queryAllProductCategory', searchData, '加载类别列表', function(response) {
				prams.success && prams.success(response.resultMap.data);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2017-12-30
		* description:加载客户详细
		**/
		customer:function(prams) {
			var self = this, url = '';
			if(prams.customerType == Enum.customerType.num.business) {
				url = 'customer/detailCustomerCompany';
			} else if(prams.customerType == Enum.customerType.num.personal) {
				url = 'customer/detailCustomerPerson';
			}
			Rental.ajax.ajaxDataNoLoading(url, {customerNo:prams.customerNo}, '加载客户详细', function(response) {
				prams.success && prams.success(response.resultMap.data);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2017-12-30
		* description:加载客户详细
		**/
		customerDetail:function(prams) {
			Rental.ajax.ajaxDataNoLoading('customer/detailCustomer', {customerNo:prams.customerNo}, '加载客户详细', function(response) {
				prams.success && prams.success(response.resultMap.data);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2018-01-18
		* description:加载同行详情
		**/
		peerSupplierDetial:function(prams) {
			Rental.ajax.ajaxDataNoLoading('peer/queryDetail', {peerNo:prams.peerNo}, '加载同行信息', function(response) {
				prams.success && prams.success(response.resultMap.data);
			}, function() {
				prams.error && prams.error();
			});
		},
		/**
		* author:weblee
		* update date:2018-01-18
		* description:加载配件类型
		**/
		queryMaterialType:function(prams) {
			var searchData = _.extend({
				pageNo:1,
				pageSize:10000,
				// materialTypeId:null,
				// materialTypeName:null,
				// isMainMaterial:1,
				// isCapacityMaterial:1,
			}, prams.searchData || {});

			Rental.ajax.ajaxDataNoLoading('material/queryType', searchData, '加载配件类型', function(response) {
				prams.success && prams.success(response.resultMap.data.itemList);
			}, function() {
				prams.error && prams.error();
			}, 'none');
		},
		/**
		*  提交审核
		**/
		isNeedVerify:function(prams, callBack) {
			if(!prams.no) {
				bootbox.alert('找不到审核单编号');
				return;
			}
			Rental.ajax.submit("{0}workflow/isNeedVerify".format(SitePath.service),{workflowType:prams.workflowType},function(response){
				if(response.success) {
					if(response.resultMap.data) {

						SubmitAudit.init({
							workflowType:prams.workflowType,
							workflowReferNo:prams.no,
							verifyAttachment:prams.hasOwnProperty('verifyAttachment') ? prams.verifyAttachment : true,
							callBack:function(res) {
								var commitData = {
									no:prams.no,
									verifyUserId:res.verifyUser,
									commitRemark:res.commitRemark,
									imgIdList:res.imgIdList,
								}
								callBack && callBack(commitData, true);
								// Rental.modal.close();
							}
						});
					} else {
						callBack && callBack({no:prams.no}, false)
					}
				} else {
					Rental.notification.error('请求提交', response.description || '失败');
				}
			}, null ,'请求提交');
		}

	}

	window.ApiData = ApiData;

})(jQuery);