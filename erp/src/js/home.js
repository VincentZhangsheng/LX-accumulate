(function($) {

	 // Define chart color patterns
    var highColors = [bgWarning, bgPrimary, bgInfo, bgAlert,bgDanger, bgSuccess, bgSystem, bgDark];

    // Color Library we used to grab a random color
    var sparkColors = {
        "primary": [bgPrimary, bgPrimaryLr, bgPrimaryDr],
        "info": [bgInfo, bgInfoLr, bgInfoDr],
        "warning": [bgWarning, bgWarningLr, bgWarningDr],
        "success": [bgSuccess, bgSuccessLr, bgSuccessDr],
        "alert": [bgAlert, bgAlertLr, bgAlertDr]
    };

	var Home = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();	
		},
		initAuthor:function() {
			this.currentPageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.home];
			if(!!this.currentPageAuthor == false) return ;
			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;
			this.homeDataAnalysisAuthor = this.currentPageAuthor.children[AuthorCode.home_data_analysis];
			this.businessWorkbenchAuthor = this.currentPageAuthor.children[AuthorCode.home_business_workbench];
			this.salesWorkbenchAuthor = this.currentPageAuthor.children[AuthorCode.home_sales_workbench];
		},
		initDom:function() {
		},
		initEvent:function() {
			Breadcrumb.init(); //面包屑

			//权限控制
			if(this.hasOwnProperty('homeDataAnalysisAuthor') && !!this.homeDataAnalysisAuthor && false) {
				$(".dataAnalysis").removeClass('hide');
				this.loadStatistics();
				this.statisticsUnReceivable.init();
				this.longRentByTime.init()
				this.shortRentByTime.init();	
			} 
			else if((this.hasOwnProperty('salesWorkbenchAuthor') && !!this.salesWorkbenchAuthor) && (!this.hasOwnProperty('businessWorkbenchAuthor') || !this.businessWorkbenchAuthor)) {
				$(".sales-panel").removeClass('hide');
				this.salesWorkbench.init()
			} else if((this.hasOwnProperty('businessWorkbenchAuthor') && !!this.businessWorkbenchAuthor) && (!this.hasOwnProperty('salesWorkbenchAuthor') || !this.salesWorkbenchAuthor)) {
				$(".business-panel").removeClass('hide');
				this.businessWorkbench.init()
			} else if((this.hasOwnProperty('businessWorkbenchAuthor') && !!this.businessWorkbenchAuthor) && (this.hasOwnProperty('salesWorkbenchAuthor') && !!this.salesWorkbenchAuthor)) {
				$(".multiple-panel").removeClass('hide');
				this.multipleWorkbench.init()
				// $(".warehouse-panel").removeClass('hide');
				// this.multipleWorkbench.init()
			} 
			else {
				$(".welcome").removeClass('hide');
			}
			this.systemNotice.init();
		},
		loadStatistics:function() {
			var self = this;
			Rental.ajax.ajaxData('statistics/queryIndexInfo', {}, '查询统计信息', function(response) {
				self.renderStatistics(response.resultMap.data);
			});
		},
		renderStatistics:function(info) {
			var data = _.extend(Rental.render, {
				statistics:info
			}),
			tpl = $("#statisticsTpl").html();

			Mustache.parse(tpl);
			$("#statistics").html(Mustache.render(tpl, data));
		},

	};

	/**
	* 未收汇总
	**/
	Home.statisticsUnReceivable = { 
		state: {
		},
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
		},
		initEvent:function() {
			this.loadData();
			this.initHighcharts();
		},
		loadData:function() {
			var self = this, loading = $('#statisticsUnReceivableForSubCompanyLoading');
			loading.removeClass('hide');
			Rental.ajax.ajaxData('statistics/queryStatisticsUnReceivableForSubCompany', {}, '加载未收汇总信息', function(response) {
				self.resolveData(response.resultMap.data);
				loading.addClass('hide');
			}, function() {
				loading.addClass('hide');
			});
		},
		resolveData:function(dataSource) {
			var self = this;

			if(!dataSource.hasOwnProperty('statisticsUnReceivableDetailForSubCompanyList')) return;
			
			var categories = _.pluck(dataSource.statisticsUnReceivableDetailForSubCompanyList, 'subCompanyName');

			var series = [
				{
					name:'上月租金',
					data:_.pluck(dataSource.statisticsUnReceivableDetailForSubCompanyList, 'lastMonthRent'),
					totalAmount: dataSource.totalLastMonthRent,
					rowIndex:0,
					className:'text-warning'
				},
				{
					// visible: false,
					name:'长租未收',
					data:_.pluck(dataSource.statisticsUnReceivableDetailForSubCompanyList, 'unReceivableLong'),
					totalAmount:dataSource.totalUnReceivableLong,
					rowIndex:1,
					className:'text-primary'
				},
				{
					 // visible: false,
					name:'短租未收',
					data:_.pluck(dataSource.statisticsUnReceivableDetailForSubCompanyList, 'unReceivableShort'),
					totalAmount:dataSource.totalUnReceivableShort,
					rowIndex:2,
					className:'text-info'
				},
				{
					name:'合计未收',
					data:_.pluck(dataSource.statisticsUnReceivableDetailForSubCompanyList, 'unReceivable'),
					totalAmount:dataSource.totalUnReceivable,
					rowIndex:3,
					className:'text-alert'
				},
			];

			self.rendeTable(categories, series);
			self.initHighcharts(categories, series);
			self.initButtonCharts();
		},
		initHighcharts:function(categories, series) {
			
			var line1 = $('#high-line');
            if (line1.length) {
                $('#high-line').highcharts({
                    credits: false,
                    colors: highColors,
                    chart: {
                        type: 'column',
                        zoomType: 'x',
                        panning: true,
                        panKey: 'shift',
                        marginRight: 50,
                        marginTop: -5,
                    },
                    title: {
                        text: null
                    },
                    xAxis: {
                        gridLineColor: '#EEE',
                        lineColor: '#EEE',
                        tickColor: '#EEE',
                        categories: categories,
                    },
                    yAxis: {
                        min: -2,
                        // tickInterval: 5,
                        gridLineColor: '#EEE',
                        title: {
                            text: '柱状图',
                            style: {
                                color: bgInfo,
                                fontWeight: '600'
                            }
                        }
                    },
                    plotOptions: {
                        spline: {
                            lineWidth: 3,
                        },
                        area: {
                            fillOpacity: 0.2
                        }
                    },
                    legend: {
                        enabled: false,
                    },
                    series:series
                });
            }
		},
		initButtonCharts:function() {
            var tableLegend = $('.table-legend');
                 
            if (tableLegend.length) {

                $('.table-legend').each(function(i, e) {
                    var legendID = $(e).data('chart-id');
                    $(e).find('input.legend-switch').each(
                        function(i, e) {
                            var This = $(e);
                            var itemID = This.val();
                            
                            var legend = $(legendID).highcharts()
                                .series[itemID];
                            
                            var legendName = legend.name;
                            This.html(legendName);
                            
                            if (legend.visible) {
                                This.attr('checked', true);
                            } else {
                                This.attr('checked', false);
                            }
                            
                            This.on('click', function(i, e) {
                                if (legend.visible) {
                                    legend.hide();
                                    This.attr('checked',false);
                                } else {
                                    legend.show();
                                    This.attr('checked',true);
                                }
                            });
                    });
                });
            }
		},
		rendeTable:function(categories, series) {
			var self = this,
				index = -1;
				data = {
					tableHead:{
						categories:categories,
						name:function() {
							return this;
						}
					},
					dataSource:_.extend(Rental.render, {
						"listData":series,
						amountList:function() {
							var al = this.data;
							return _.extend(Rental.render, {
								amountData:al,
								amount:function() {
									return this;
								}
							})
						},
					}),
				};

			var tpl = $('#statisticsUnReceivableTpl').html();
			Mustache.parse(tpl);
			$("#statisticsUnReceivableContainer").html(Mustache.render(tpl, data));
		}
	}

	/*
	* 长租统计信息
	*/
	Home.longRentByTime = {
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
		},
		initEvent:function() {
			var self = this;
			self.loadData(2); //1:当月，2：当年
			$(".longRentByTimeBtn").on('click', function(event) {
				event.preventDefault();
				var timeDimensionType = $(this).data('timedimensiontype');
				self.loadData(timeDimensionType);
				$(".longRentByTimeBtn").removeClass('btn-info');
				$(this).addClass('btn-info');
			});
		},
		loadData:function(timeDimensionType) {
			var self = this, loading = $("#longRentByTimeLoading");
			loading.removeClass('hide');
			Rental.ajax.ajaxData('statistics/queryLongRentByTime', {timeDimensionType:timeDimensionType}, '加载长租统计信息', function(response) {
				loading.addClass('hide');
				self.resolveData(response.resultMap.data, timeDimensionType);
			}, function() {
				loading.addClass('hide');
			});
		},
		resolveData:function(dataSource, timeDimensionType) {
			var self = this;

			var categories = _.pluck(dataSource, 'timeNode');
			if(categories && categories.length > 0) {
				if(timeDimensionType == 1) {
					categories = categories.map(function(item) {
						return  (new Date(item)).getDate();
					}); 
				} else {
					categories = categories.map(function(item) {
						return ((new Date(item)).getMonth() + 1) + "月";
					}); 
				}
			}

			var series = [
				{
		            name: '租金收入',
		            type: 'column',
		            yAxis: 1,
		           	data:_.pluck(dataSource, 'rentIncome'),	
		            tooltip: {
		                valueSuffix: '元'
		            }
		        },
		        {
		            name: '下单老客户数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'oldCustomerCount'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '下单新增客户数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'newCustomerCount'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '老客户订单数量',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'orderCountByOldCustomer'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '新客户订单数量',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'orderCountByNewCustomer'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '老客户下单台数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'productCountByOldCustomer'),	
		            tooltip: {
		                valueSuffix: '台'
		            }
		        },
		        {
		            name: '新客户下单台数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'productCountByNewCustomer'),	
		            tooltip: {
		                valueSuffix: '台'
		            }
		        },
			];


			self.initHighcharts(categories, series);
		},
		initHighcharts:function(categories, series) {
			$('#longRentByTime').highcharts({
				credits: false,
                colors: highColors,
		        chart: {
		            zoomType: 'xy'
		        },
		        title: {
		            text: '长租统计信息'
		        },
		        subtitle: {
		            text: '租金收入、订单增长、客户增长'
		        },
		        xAxis: [{
		            // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		            categories:categories,
		            crosshair: true
		        }],
		        yAxis: [{ // Secondary yAxis
		            title: {
		                text: '数量',
		                style: {
		                    color: Highcharts.getOptions().colors[0]
		                }
		            },
		            labels: {
		                format: '{value}',
		                style: {
		                    color: Highcharts.getOptions().colors[0]
		                }
		            },
		            opposite: true
		        },{ // Primary yAxis
		            labels: {
		                format: '{value}元',
		                style: {
		                    color: Highcharts.getOptions().colors[1]
		                }
		            },
		            title: {
		                text: '金额',
		                style: {
		                    color: Highcharts.getOptions().colors[1]
		                }
		            }
		        }],
		        tooltip: {
		            shared: true
		        },
		        legend: {
		            layout: 'vertical',
		            align: 'left',
		            x: 120,
		            verticalAlign: 'top',
		            y: 100,
		            floating: true,
		            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		        },
		        series:series,
		    });
		}
	}

	/*
	* 短租统计信息
	*/
	Home.shortRentByTime = {
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
		},
		initEvent:function() {
			var self = this;
			self.loadData(2); //1:当月，2：当年
			$(".shortRentByTimeBtn").on('click', function(event) {
				event.preventDefault();
				var timeDimensionType = $(this).data('timedimensiontype');
				self.loadData(timeDimensionType);
				$(".shortRentByTimeBtn").removeClass('btn-info');
				$(this).addClass('btn-info');
			});
		},
		loadData:function(timeDimensionType) {
			var self = this, loading = $("#shortRentByTimeLoading");
			loading.removeClass('hide');
			Rental.ajax.ajaxData('statistics/queryShortRentByTime', {timeDimensionType:timeDimensionType}, '加载短租统计信息', function(response) {
				loading.addClass('hide');
				self.resolveData(response.resultMap.data, timeDimensionType);
			}, function() {
				loading.addClass('hide');
			});
		},
		resolveData:function(dataSource, timeDimensionType) {
			var self = this;

			var categories = _.pluck(dataSource, 'timeNode');
			if(categories && categories.length > 0) {
				if(timeDimensionType == 1) {
					categories = categories.map(function(item) {
						return  (new Date(item)).getDate();
					}); 
				} else {
					categories = categories.map(function(item) {
						return ((new Date(item)).getMonth() + 1) + "月";
					}); 
				}
			}

			var series = [
				{
		            name: '租金收入',
		            type: 'column',
		            yAxis: 1,
		           	data:_.pluck(dataSource, 'rentIncome'),	
		            tooltip: {
		                valueSuffix: '元'
		            }
		        },
		        {
		            name: '下单老客户数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'oldCustomerCount'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '下单新增客户数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'newCustomerCount'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '老客户订单数量',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'orderCountByOldCustomer'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '新客户订单数量',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'orderCountByNewCustomer'),	
		            tooltip: {
		                valueSuffix: '个'
		            }
		        },
		        {
		            name: '老客户下单台数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'productCountByOldCustomer'),	
		            tooltip: {
		                valueSuffix: '台'
		            }
		        },
		        {
		            name: '新客户下单台数',
		            type: 'spline',
		           	data:_.pluck(dataSource, 'productCountByNewCustomer'),	
		            tooltip: {
		                valueSuffix: '台'
		            }
		        },
			];


			self.initHighcharts(categories, series);
		},
		initHighcharts:function(categories, series) {
			$('#shortRentByTime').highcharts({
				credits: false,
                colors: highColors,
		        chart: {
		            zoomType: 'xy'
		        },
		        title: {
		            text: '短租统计信息'
		        },
		        subtitle: {
		            text: '租金收入、订单增长、客户增长'
		        },
		        xAxis: [{
		            categories:categories,
		            crosshair: true
		        }],
		        yAxis: [{ // Secondary yAxis
		            title: {
		                text: '数量',
		                style: {
		                    color: Highcharts.getOptions().colors[0]
		                }
		            },
		            labels: {
		                format: '{value}',
		                style: {
		                    color: Highcharts.getOptions().colors[0]
		                }
		            },
		            opposite: true
		        },{ // Primary yAxis
		            labels: {
		                format: '{value}元',
		                style: {
		                    color: Highcharts.getOptions().colors[1]
		                }
		            },
		            title: {
		                text: '金额',
		                style: {
		                    color: Highcharts.getOptions().colors[1]
		                }
		            }
		        }],
		        tooltip: {
		            shared: true
		        },
		        legend: {
		            layout: 'vertical',
		            align: 'left',
		            x: 120,
		            verticalAlign: 'top',
		            y: 100,
		            floating: true,
		            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		        },
		        series:series,
		    });
		}
	}

	/**
	* 商务工作台
	**/
	Home.businessWorkbench = { 
		init:function() {
			this.initEvent();
		},
		initEvent:function() {
			this.loadWorkbench();
		},
		loadWorkbench:function() {
			var self = this;
			var commitData = {
				isRecycleBin:0,
				isDisabled:0,
				workbenchName:1,
			}
			Rental.ajax.ajaxDataNoLoading('workbench/queryWorkbenchCount', commitData, '加载工作台', function(response) {
				self.renderWorkbench(response.resultMap.data)
			});
		},
		renderWorkbench:function(data) {
			Home.renderWorkbenchList(data.workflowBusinessAffairsListMap,$("#businessAuditTpl").html(),$("#businessAuditRow"),"工作流")
			Home.renderWorkbenchList(data.bankSlipDetailBusinessAffairsListMap,$("#businessFinanceTpl").html(),$("#businessFinanceRow"),"资金流水")
			Home.renderWorkbenchList(data.statementOrderBusinessAffairsListMap,$("#statementTpl").html(),$("#statementRow"),"结算单")
			Home.renderWorkbenchList(data.orderListMap,$("#businessOrdeTpl").html(),$("#businessOrderRow"),"租赁订单")
			Home.renderWorkbenchList(data.k3ReturnOrderListMap,$("#businessReturnOrderTpl").html(),$("#returnOrderRow"),"退货单")
		}
	}

	/**
	* 业务工作台
	**/
	Home.salesWorkbench = {
		init:function() {
			this.initEvent();
		},
		initEvent:function() {
			this.loadWorkbench();
		},
		loadWorkbench:function() {
			var self = this;
			var commitData = {
				isRecycleBin:0,
				isDisabled:0,
				workbenchName:0,
			}
			Rental.ajax.ajaxDataNoLoading('workbench/queryWorkbenchCount', commitData, '加载业务工作台', function(response) {
				self.renderWorkbench(response.resultMap.data)
			});
		},
		renderWorkbench:function(data) {
			Home.renderWorkbenchList(data.orderListMap,$("#salesOrdeTpl").html(),$("#salesOrderRow"),"租赁订单")
			Home.renderWorkbenchList(data.k3ReturnOrderListMap,$("#salesReturnOrderTpl").html(),$("#salesReturnOrderRow"),"退货单")
			Home.renderWorkbenchList(data.customerListMap,$("#salesCustomerTpl").html(),$("#salesCustomerRow"),"客户")
			Home.renderWorkbenchList(data.workflowListMap,$("#salesAuditTpl").html(),$("#salesAuditRow"),"工作流")
		}
	}

	/**
	* 商务+业务工作台
	**/
	Home.multipleWorkbench = {
		init:function() {
			this.initEvent();
		},
		initEvent:function() {
			this.loadWorkbench();
		},
		loadWorkbench:function() {
			var self = this;
			var commitData = {
				isRecycleBin:0,
				isDisabled:0,
				workbenchName:2,
			}
			Rental.ajax.ajaxDataNoLoading('workbench/queryWorkbenchCount', commitData, '加载待审核工作流数量', function(response) {
				self.renderWorkbench(response.resultMap.data)
			});
		},
		renderWorkbench:function(data) {
			Home.renderWorkbenchList(data.workflowBusinessAffairsListMap,$("#waitToAuditTpl").html(),$("#waitToAuditRow"),"工作流")
			Home.renderWorkbenchList(data.workflowListMap,$("#multipleAuditTpl").html(),$("#multipleAuditRow"),"")
			Home.renderWorkbenchList(data.bankSlipDetailBusinessAffairsListMap,$("#multipleFinanceTpl").html(),$("#multipleFinanceRow"),"资金流水")
			Home.renderWorkbenchList(data.statementOrderBusinessAffairsListMap,$("#multipleStatementTpl").html(),$("#multipleStatementRow"),"结算单")
			Home.renderWorkbenchList(data.orderListMap,$("#multipleOrdeTpl").html(),$("#multipleOrderRow"),"租赁订单")
			Home.renderWorkbenchList(data.k3ReturnOrderListMap,$("#multipleReturnOrderTpl").html(),$("#multipleReturnOrderRow"),"退货单")
			Home.renderWorkbenchList(data.customerListMap,$("#multipleCustomerTpl").html(),$("#multipleCustomerRow"),"客户")
		}
	}

	Home.renderWorkbenchList=function(workData,tpl,container,tplTitle) {
		var self = this;
		var listData = workData;
		if(listData.length > 0) {
			listData[0].title = tplTitle
		}
		
		var data = {
			dataSource:_.extend(Rental.render, {
				"listData":listData,
				returnColTitle:function() {
					return Home.returnWorkbenchTitle.getValue(this.workbenchType);
				},
				orderColTitle:function() {
					return Home.orderWorkbenchTitle.getValue(this.workbenchType);
				},
				auditColTitle:function() {
					return Home.auditWorkbenchTitle.getValue(this.workbenchType);
				},
				statementColTitle:function() {
					return Home.statementWorkbenchTitle.getValue(this.workbenchType);
				},
				customerColTitle:function() {
					return Home.customerWorkbenchTitle.getValue(this.workbenchType);
				},
				customerListUrl:function() {
					return (this.workbenchType == 10 || this.workbenchType == 11) ? SitePath.base + "customer-business-manage/list" : SitePath.base + "customer-manage/list"
				},
				hasdata:function() {
					return this.count > 0;
				},
			}),
		}

		Mustache.parse(tpl);
		container.html(Mustache.render(tpl, data));
	}

	Home.returnWorkbenchTitle={
		num:{
			inReview:6,
			handle:8,
			reject:7,
			unsubmit:9
		},
		getValue:function(num) {
			switch(num) {
				case this.num.inReview:
					return "审核中的退货单";
				case this.num.reject:
					return "被驳回的退货单";
				case this.num.handle:
					return "处理中的退货单";
				case this.num.unsubmit:
					return "未提交的退货单";
				default:
					return "";
			}
		},
	}

	Home.orderWorkbenchTitle={
		num:{
			inReview:1,
			unhandle:3,
			canRelet:2,
			waitDelivery:4,
			notPay:5
		},
		getValue:function(num) {
			switch(num) {
				case this.num.inReview:
					return "审核中的订单";
				case this.num.unhandle:
					return "到期未处理的订单";
				case this.num.canRelet:
					return "可续租的订单";
				case this.num.waitDelivery:
					return "待发货的订单";
				case this.num.notPay:
					return "未支付的订单";
				default:
					return "";
			}
		},
	}

	Home.customerWorkbenchTitle={
		num:{
			handleCompany:10,
			rejectCompany:11,
			handlePerson:12,
			rejectPerson:13,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.handleCompany:
					return "审核中的企业客户";
				case this.num.rejectCompany:
					return "被驳回的企业客户";
				case this.num.handlePerson:
					return "审核中的个人客户";
				case this.num.rejectPerson:
					return "被驳回的个人客户";
				default:
					return "";
			}
		},
	}

	Home.auditWorkbenchTitle={
		num:{
			handle:14,
			reject:15,
			waitToAudit:18
		},
		getValue:function(num) {
			switch(num) {
				case this.num.handle:
					return "审核中的工作流";
				case this.num.reject:
					return "被驳回的工作流";
				case this.num.waitToAudit:
					return "待审核的工作流";
				default:
					return "";
			}
		},
	}

	Home.statementWorkbenchTitle={
		num:{
			unpaid:16,
			partPaid:17
		},
		getValue:function(num) {
			switch(num) {
				case this.num.unpaid:
					return "未支付的结算单";
				case this.num.partPaid:
					return "部分支付的结算单";
				default:
					return "";
			}
		},
	}

	Home.systemNotice = {
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
		},
		initEvent:function() {
			var self = this;
			this.loadNoticeData();

			$("#systemNoticeList").on("click",".notice-item",function(event) {
				event.preventDefault();
				var noticeObj = $(this).data("notice");
				self.noticeDialog.init({noticeObj:noticeObj})
			})
		},
		loadNoticeData:function() {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('announcement/list', {}, '加载系统公告', function(response) {
				$("#systemNotice").removeClass("hide")
				self.renderNoticeList(response.resultMap.data)
			});
		},
		renderNoticeList:function(data) {
			var self = this;
			var dataListTpl = $("#systemNoticeTpl").html()
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			if(listData.length > 6) {
				listData = listData.splice(0,6)
			}
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					noticeStr:function() {
						return JSON.stringify(this);
					}			
				})
			}
			Mustache.parse(dataListTpl);
			$("#systemNoticeList").html(Mustache.render(dataListTpl, data));
		},
		noticeDialog:{
			init:function(prams) {
				this.props = _.extend({
					noticeObj:null,
					callBack:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "home/system-notice", type:'ajax', ajaxContentAdded:function(pModal) {
					self.initDom();
					self.initEvent();	
				}});
			},
			initDom:function() {
			},
			initEvent:function() {
				this.renderNoticeInfo();
			},
			renderNoticeInfo:function() {
				var self = this;
				var noticeDetailTpl = $("#noticeDetailTpl").html();
				var data = _.extend(Rental.render, {
					noticeObj:self.props.noticeObj
				})
				Mustache.parse(noticeDetailTpl);
				$("#noticeDetail").html(Mustache.render(noticeDetailTpl, data));

				$("#noticeContent").html(self.props.noticeObj.content);
			},
		},
	}

	window.Home = Home;

})(jQuery)