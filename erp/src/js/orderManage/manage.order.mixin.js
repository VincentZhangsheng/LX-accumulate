;(function($) {

	OrderMixIn = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			chooseGroupList:new Array(),
			customerNo:null,
			order:new Object(),
		},
		initCommonEvent:function() {
			var self = this;

			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});

			Rental.ui.renderSelect({
				container:$("#deliveryMode"),
				data:Enum.array(Enum.deliveryMode),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					var $logisticsAmount = $('[name=logisticsAmount]');
					val == Enum.deliveryMode.num.pickup && $('[name=logisticsAmount]').val(0);
					self.getFirstPayAmount();
				},
				defaultText:'请选择'
			});

			Rental.ui.renderSelect({
				container:$("#rentType"),
				data:Enum.array(Enum.rentType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.changeRentType(val);
					self.getFirstPayAmount();
				},
				defaultText:'请选择'
			});

			self.$form.on('change', '#rentTimeLength', function(event) {
				event.preventDefault();
				self.changeRentType();
				self.getFirstPayAmount();
			});

			self.$form.on('change', '#highTaxRate,#lowTaxRate', function(event) {
				event.preventDefault();
				self.changTaxRate($(this));
			});

			self.initDatePicker($('#createTimePicker'), $("[name=rentStartTime]", self.$form),function(date){
				self.getFirstPayAmount();
			});
			self.initDatePicker($('#createExpectDeliveryTimePicker'), $("[name=expectDeliveryTime]", self.$form));

			self.renderProductList();
			self.renderMaterialList();
			self.renderGroupProductList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			$("#chooseCustomer").click(function(event) {
				event.preventDefault();
				ChooseCustomer.init({
					callBack:function(customer) {
						self.setCustomer(customer,null);
					}
				});
			});

			$("#chooseBusinessCustomer").click(function(event) {
				event.preventDefault();
				ChooseBusinessCustomer.init({
					callBack:function(company) {
						self.setCustomer(null,company);
					}
				});
			});

			$("#chooseSeller").click(function(event) {
				event.preventDefault();
				ChooseUser.init({
					callBack:function(seller) {
						self.setSeller(seller);
					}
				});
			});

			ApiData.company({
				success:function(response) {
					var filterCompany = _.filter(response, function(item) {
						return item.subCompanyId  != Enum.subCompany.num.headOffice && item.subCompanyId != Enum.subCompany.num.telemarketing && item.subCompanyId != Enum.subCompany.num.channelBigCustomer;
					});
					Rental.ui.renderSelect({
						container:$("#deliverySubCompanyId"),
						data: filterCompany, //response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							// self.searchData();
						},
						defaultText:'请选择发货公司'
					});
				}
			});

			// $("#chooseSubCompany").click(function(event) {
			// 	event.preventDefault();
			// 	ChooseCompany.init({
			// 		callBack:function(subCompany) {
			// 			self.setSubCompany(subCompany);
			// 		}
			// 	});
			// });

			$("#batchAddProduct").click(function(event) {
				event.preventDefault();
				self.chooseProductFunc();
			});

			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms:new Object(), 
					showIsNewCheckBox:true,
					callBack:function(material) {
						self.chooseMaterial(material);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				self.batchDeleteProduct();
				self.getFirstPayAmount();
			});

			self.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
				self.renderProductList();
				self.getFirstPayAmount();
			});

			self.$dataListTable.on('change', '.productUnitAmount,.productCount,.depositAmount,.payMode,.insuranceAmount', function(event) {
				event.preventDefault();
				var $skuRow = $(this).closest('.skuRow');
				self.changeProduct($skuRow);
				self.getFirstPayAmount();
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDeleteMaterial();
				self.getFirstPayAmount();
			});

			self.$materialDataListTable.on('click', '.delMaterialButton', function(event) {
				event.preventDefault();
				self.deleteMaterial($(this));
				self.getFirstPayAmount();
			});

			self.$materialDataListTable.on('change', '.materialUnitAmount,.materialCount,.depositAmount,.payMode,.insuranceAmount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeMaterial($materialRow);
				self.getFirstPayAmount();
			});

			self.$dataListTable.on('click', '.chooseProductUnitAmount', function(event) {
				event.preventDefault();
				self.chooseProductUnitAmount($(this));
			});

			self.$materialDataListTable.on('click', '.chooseMaterialUnitAmount', function(event) {
				event.preventDefault();
				self.chooseMaterialUnitAmount($(this));
			});

			self.$form.on('change','#rentTimeLength,#deliveryMode,#logisticsAmount,#rentStartTime', function(){
				self.getFirstPayAmount();
			})

			self.$dataListTable.on('mouseout', '.fa-info-circle', function(event) {
				event.preventDefault();
				$(this).popover('hide');
			});
			self.$dataListTable.on('mouseover', '.fa-info-circle', function(event) {
				event.preventDefault();
				$(this).popover('show');
			});

			self.$materialDataListTable.on('mouseover', '.fa-info-circle', function(event) {
				event.preventDefault();
				$(this).popover('show');
			});
			self.$materialDataListTable.on('mouseout', '.fa-info-circle', function(event) {
				event.preventDefault();
				$(this).popover('hide');
			});

			//组合商品
			$("#batchAddGroup").click(function(event) {
				ChooseGroupProduct.modal({
					callBack:function(groupProduct) {
						self.chooseGroupProduct(groupProduct);
					}
				});
			});

			$("#batchDeleteGroup").click(function(event) {
				event.preventDefault();
				self.batchDeleteGroup();
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('click', '.deleteGroup', function(event) {
				event.preventDefault();
				self.deleteGroup($(this));
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('change', '.jointProductCount', function(event) {
				event.preventDefault();
				var $groupRow = $(this).closest('.groupRow');
				self.changeGroupProduct($groupRow);
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('click', '.chooseProductUnitAmount', function(event) {
				event.preventDefault();
				self.chooseGroupProductUnitAmount($(this));
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('click', '.chooseMaterialUnitAmount', function(event) {
				event.preventDefault();
				self.chooseGroupMaterialUnitAmount($(this));
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('change', '.productUnitAmount,.productDeposit,.productPayMode', function(event) {
				event.preventDefault();
				var $productRow = $(this).closest('.productRow');
				self.changeGroupProductItem($productRow);
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('change', '.materialUnitAmount,.materialCount,.materialDeposit,.materialPayMode', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeGroupMaterial($materialRow);
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on('click', '.delGroupMaterial', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.deleteGroupMaterial($materialRow);
				self.getFirstPayAmount();
			});

			self.$groupDataListListTable.on("click",'.orderItemTabs li a',function(event) {
				event.preventDefault();
				var jointid = $(this).parents("li").data("id");
				var isNew = $(this).parents("li").data("isnew");
				var tabIndex = $(this).parents("li").index();
				self.changeTabIndex(jointid,isNew,tabIndex);
			})

			Rental.helper.onOnlyDecmailNum(self.$materialDataListTable, '.materialUnitAmount', 4); 
			Rental.helper.onOnlyDecmailNum(self.$dataListTable, '.productUnitAmount', 4); 
			Rental.helper.onOnlyNumber(self.$groupDataListListTable, '.jointProductCount'); 
			Rental.helper.onOnlyDecmailNum(self.$groupDataListListTable, '.materialUnitAmount', 4); 
			Rental.helper.onOnlyDecmailNum(self.$groupDataListListTable, '.productUnitAmount', 4); 
		},
		chooseProductFunc:function() {
			var self = this;
			ProductChoose.modal({
				showIsNewCheckBox:true,
				callBack:function(product) {
					self.chooseProduct(product);
				},
				complete:function() {
					Rental.modal.close();
				},
				changeContinueFunc:null,
				modalCloseCallBack:function(product) {
					confirmProductStock.init({
						isChooseProduct:true,
						productName:product.productName,
						k3ProductNo:product.k3ProductNo,
						changeContinueFunc:null,
						continueFunc:function() {
							_.isObject(OrderMixIn) &&  _.isFunction(OrderMixIn.chooseProductFunc) && OrderMixIn.chooseProductFunc();
						},
					})
				}
			});
		},
		/**
		* description: 获取首付金额、首付押金,
		* author：zhangsheng
		* time: 2018/03/20 
		*/
		getFirstPayAmount: function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);
				var rentStartTime = new Date(formData['rentStartTime']).getTime();

				// if(!formData['deliveryMode'] || !formData['rentStartTime'] || !formData['rentType'] || !formData['rentTimeLength'] || !formData['logisticsAmount']) return;
				if(!formData['deliveryMode'] || !formData['rentStartTime'] || !formData['rentType'] || !formData['rentTimeLength']) return;
				
				var commitData = {
					buyerCustomerNo:self.state.customerNo,
					deliveryMode:formData['deliveryMode'],
					rentStartTime:rentStartTime,
					rentType:formData['rentType'],
					rentTimeLength:formData['rentTimeLength'],
					// logisticsAmount:formData['logisticsAmount'],
					deliverySubCompanyId:formData['deliverySubCompanyId']
				}

				// if(self.isTelSeller()) {
				// 	if(!formData['deliverySubCompanyId']) return;
				// 	commitData.deliverySubCompanyId = formData['deliverySubCompanyId'];
				// }

				var orderProductList = OrderManageUtil.getSkuList(self.$dataListTable);
				var orderMaterialList = OrderManageUtil.getMaterialList(self.$materialDataListTable);
				var orderJointProductList = OrderManageUtil.getGroupCommitData(self.state.chooseGroupList,self.$groupDataListListTable);

				if(formData['rentType'] == 1) {
					orderProductList = orderProductList.filter(function(item) {
						return !!item.productUnitAmount && !!item.productCount && !!item.depositAmount && !!item.payMode
					})
					orderMaterialList = orderMaterialList.filter(function(item) {
						return !!item.materialUnitAmount && !!item.materialCount && !!item.depositAmount && !!item.payMode
					})

					orderJointProductList.forEach(function(group) {
						group.orderProductList = group.orderProductList.filter(function(item) {
							return !!item.productUnitAmount && !!item.productCount && !!item.depositAmount && !!item.payMode
						})
						group.orderMaterialList = group.orderMaterialList.filter(function(item) {
							return !!item.materialUnitAmount && !!item.materialCount && !!item.depositAmount && !!item.payMode
						})
					})
				} else if(formData['rentType'] == 2) {
					orderProductList = orderProductList.filter(function(item) {
						return !!item.productUnitAmount && !!item.productCount
					})
					orderMaterialList = orderMaterialList.filter(function(item) {
						return !!item.materialUnitAmount && !!item.materialCount
					})

					orderJointProductList.forEach(function(group) {
						group.orderProductList = group.orderProductList.filter(function(item) {
							return !!item.productUnitAmount && !!item.productCount
						})
						group.orderMaterialList = group.orderMaterialList.filter(function(item) {
							return !!item.materialUnitAmount && !!item.materialCount
						})
					})
				}
				
				if(!!orderJointProductList) {
					orderJointProductList = orderJointProductList.filter(function(item) {
						return !!item.jointProductCount && (item.orderProductList.length > 0 || item.orderMaterialList.length > 0)
					})
				}

				if(orderProductList.length == 0 && orderMaterialList.length == 0 && (!orderJointProductList || orderJointProductList.length == 0)) return;

				if(orderProductList.length > 0) {
					commitData.orderProductList = orderProductList.map(function(item) {
						delete item.rowid;
						return item;
					});
				}
				
				if(orderMaterialList.length > 0) {
					commitData.orderMaterialList = orderMaterialList.map(function(item) {
						delete item.rowid;
						return item;
					});
				}

				if(!!orderJointProductList && orderJointProductList.length > 0) {
					commitData.orderJointProductList = orderJointProductList
				}

				Rental.ajax.submit("{0}order/createOrderFirstPayAmount".format(SitePath.service), commitData, function(response){
					if(response.success) {
						self.resolvePayAmount(response.resultMap.data)
					} else {
						Rental.notification.error("获取首付金额失败",response.description || '失败');
					}
				}, null ,"");
			} catch(e) {
				Rental.notification.error("获取首付金额失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		resolvePayAmount:function(data) {
			this.resolveProductFirstPayAmount(data.orderProductList);
			this.renderProductList();
			this.resolveMaterialFirstPayAmount(data.orderMaterialList);
			this.renderMaterialList();
			this.resolveGroupFirstPayAmount(this.state.chooseGroupList,data.orderJointProductList);
			this.renderGroupProductList();
			this.renderEachPrice(data);
		},
		resolveProductFirstPayAmount:function(productList) {
			var self = this;
			if(self.state.chooseProductList.length < 1) return;
			productList.forEach(function(item) {
				var productIndex = _.findIndex(self.state.chooseProductList, {productId:parseInt(item.productId)});
				if(productIndex > -1) {
					var skuIndex = _.findIndex(self.state.chooseProductList[productIndex].chooseProductSkuList, {serialNumber:item.serialNumber});
					self.state.chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(self.state.chooseProductList[productIndex].chooseProductSkuList[skuIndex], self.getProductRow(item));
				}
			})
		},
		resolveMaterialFirstPayAmount:function(materialList) {
			var self = this;
			if(self.state.chooseMaterialList.length < 1) return;
			materialList.forEach(function(item) {
				itemObj = JSON.parse(item.materialSnapshot);
				var materialNo = itemObj.materialNo;
				var index = _.findIndex(self.state.chooseMaterialList, {serialNumber:item.serialNumber});
				self.state.chooseMaterialList[index] = _.extend(self.state.chooseMaterialList[index], self.getProductRow(item));
			})
		},
		resolveGroupFirstPayAmount:function(chooseGroupList,groupList) {
			if(!groupList) return;
			var self = this;

			groupList.forEach(function(item) {
				var groupIndex = _.findIndex(chooseGroupList, {jointProductId:item.jointProductId,isNew:item.isNew});
				var itemIndex = _.findIndex(groupList, {jointProductId:item.jointProductId,isNew:item.isNew});
				
				groupList[itemIndex].orderProductList.forEach(function(productItem) {
					var proIndex = _.findIndex(chooseGroupList[groupIndex].chooseGroupProductList, {productId:productItem.productId});
					chooseGroupList[groupIndex].chooseGroupProductList[proIndex] = _.extend(chooseGroupList[groupIndex].chooseGroupProductList[proIndex], self.getProductRow(productItem));
				})
	
				groupList[itemIndex].orderMaterialList.forEach(function(matItem) {
					var matIndex = _.findIndex(chooseGroupList[groupIndex].chooseMaterialList, {materialId:matItem.materialId});
					chooseGroupList[groupIndex].chooseMaterialList[matIndex] = _.extend(chooseGroupList[groupIndex].chooseMaterialList[matIndex], self.getProductRow(matItem));
				})
			})
			return chooseGroupList;
		},
		getProductRow:function(productRow) {
			return {
				firstNeedPayRentAmount:productRow.firstNeedPayRentAmount,
				firstNeedPayDepositAmount:productRow.firstNeedPayDepositAmount,
				rentType:productRow.rentType,
				depositCycle:productRow.depositCycle,
				paymentCycle:productRow.paymentCycle,
				payMode:productRow.payMode,
			}
		},
		renderTotalFirstNeedPay:function(totalContainer, totalPrice) {
			if(!!totalPrice == false) {
				totalContainer.addClass('hide');
				return;
			}
			totalContainer.removeClass('hide');
			$(".num", totalContainer).html("￥"+parseFloat(totalPrice).toFixed(2));
		},
		renderEachPrice:function(priceData) {
			this.renderTotalFirstNeedPay($("#totalNeedPrice"), priceData.firstNeedPayAmount);
			this.renderTotalFirstNeedPay($("#totalProductFirstNeedPay"), priceData.totalProductFirstNeedPayAmount);
			this.renderTotalFirstNeedPay($("#totalMaterialFirstNeedPay"), priceData.totalMaterialFirstNeedPayAmount);
		},
		/**
		* 是否是电话销部门员工,
		* 电销：subCompanyId:10 
		*/
		isTelSeller:function() {
			var user = Rental.localstorage.getUser();
			var role = _.find(user.roleList, {subCompanyId:Enum.subCompany.num.telemarketing});
			// var bidCustomer = _.find(user.roleList, {subCompanyId:Enum.subCompany.num.channelBigCustomer});
			return !!role;
		},
		/**
		* 渠道大客户,
		* 渠道大客户：subCompanyId:11 
		*/
		isChannelBigCustomer:function() {
			var user = Rental.localstorage.getUser();
			var bidCustomer = _.find(user.roleList, {subCompanyId:Enum.subCompany.num.channelBigCustomer});
			return !!bidCustomer;
		},
		showOrderSubCompany:function() {
			if(this.isTelSeller() || this.isChannelBigCustomer()) {
				$('#orderSubCompanyNameFormControl').removeClass('hide');
			} else {
				$('#orderSubCompanyNameFormControl').remove();
			}
		},
		changTaxRate:function($input) {
			var idArray = ['highTaxRate','lowTaxRate'],
				inputId = $input.attr('id'),
				inputVal = parseFloat($.trim($input.val())),
				antherInputVal = 100 - inputVal;

			idArray.forEach(function(item) {
				if(item != inputId) {
					$('#'+item).val(antherInputVal);
				}
			});
		},
		initAddressInfo:function(customerNo, renderComplete) {
			if(!customerNo) return;
			CustomerAddressManage.init({
				addAuthor: null, //new Object(),
				customerNo:customerNo,
				checkbox:true,
				renderComplete:renderComplete || function() {},
				callBack:function(data) {
				}
			});
		},
		initDatePicker:function($picker, $input, callBack) {
			var self = this , defaultStart = moment().subtract('days', 29), defaultEnd = moment();
			$picker.daterangepicker({
			    "singleDatePicker": true,
			    "showDropdowns": true,
			    "showWeekNumbers": true,
			    "startDate": defaultEnd,
			    "endDate": defaultEnd
			}, function(start, end, label) {
			  	// console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
				  $input.val(start.format('YYYY-MM-DD'));
				  callBack && callBack(start.format('YYYY-MM-DD'));
			});
		},
		setCustomer:function(customer, company) {
			this.state.customer = customer;
			this.state.company = company;
			var customerType = null;
			if(!!customer) {
				this.state.customerNo = customer.customerNo;
				$('[name=buyerCustomerName]', this.$form).val(customer.customerPerson.realName);
				customerType = Enum.customerType.num.personal;
			}
			if(!!company) {
				this.state.customerNo = company.customerNo;
				$('[name=buyerCustomerName]', this.$form).val(company.customerCompany.companyName);
				customerType = Enum.customerType.num.business;
			}

			this.initAddressInfo(this.state.customerNo);
			this.loadRisk({customerNo:this.state.customerNo, customerType:customerType});
		},
		setSeller:function(seller) {
			if(!seller) {
				bootbox.alert('没找到销售员，请重新选择');
				return;
			}
			$("[name=orderSellerName]", this.$form).val(seller.realName);
			this.state.orderSeller = seller;
		},
		setSubCompany:function(subCompany) {
			if(!!subCompany) {
				$('[name=orderSubCompanyName]', this.$form).val(subCompany.subCompanyName);
			}
			this.state.orderSubCompany = subCompany;
		},
		changeRentType:function() {
			var self = this;
			if(_.isObject(self.state.order)) self.state.order = new Object();
			var formData = Rental.form.getFormData(self.$form);
			self.state.order = _.extend(self.state.order, formData);
			self.renderProductList();
			self.renderMaterialList();
			self.renderGroupProductList();
			self.statementDateStatus(formData['rentType']);
		},
		renderProductList:function(){
			var self = this;
			OrderManageItemRender.renderProductList(this.state.chooseProductList, $("#dataListTpl").html(), $("#dataListTable"), null, this.state.order, this.showK3No);
		},
		renderMaterialList:function(rowActionButtons) {
			OrderManageItemRender.renderMaterialList(this.state.chooseMaterialList, this.$materialDataListTpl, this.$materialDataListTable, rowActionButtons, this.state.order, this.showK3No);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = OrderManageUtil.chooseProductSupportSome(this.state.chooseProductList, product);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		changeProduct:function($skuRow) {
			this.state.chooseProductList = OrderManageUtil.changeProduct(this.state.chooseProductList, $skuRow);
			this.renderProductList();
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = OrderManageUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			this.renderProductList();	
		},
		deleteSku:function($deleteButton) {
			this.state.chooseProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseProductList);
			this.renderProductList();
		},
		chooseMaterial:function(material) {
			// this.state.chooseMaterialList = OrderManageUtil.chooseMaterial(this.state.chooseMaterialList, material);
			this.state.chooseMaterialList = OrderManageUtil.chooseMaterialSupportSome(this.state.chooseMaterialList, material, true);
			this.renderMaterialList();
		},
		changeMaterial:function($materialRow) {
			this.state.chooseMaterialList = OrderManageUtil.changeMaterial(this.state.chooseMaterialList, $materialRow);
			this.renderMaterialList();
		},
		batchDeleteMaterial:function() {
			this.state.chooseMaterialList = OrderManageUtil.batchDeleteMaterial(this.$materialDataListTable, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		deleteMaterial:function($button) {
			var materialNo = $button.data('materialno'), isNew = $button.data('isnew'), rowid = $button.data('rowid');
			this.state.chooseMaterialList = OrderManageUtil.deleteMaterial(materialNo, isNew, this.state.chooseMaterialList, rowid);
			this.renderMaterialList();
		},

		//组合商品
		chooseGroupProduct:function(groupProduct) {
			this.state.chooseGroupList = OrderManageUtil.chooseGroupProduct(this.state.chooseGroupList, groupProduct);
			this.renderGroupProductList();
			bootbox.confirm({
			    message: "成功选择组合商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		renderGroupProductList:function(){
			OrderManageItemRender.renderGroupProductList(this.state.chooseGroupList, this.$groupDataListTpl, this.$groupDataListListTable, null, this.state.order);
		},
		changeGroupProduct:function($groupRow) {
			this.state.chooseGroupList = OrderManageUtil.changeGroupProduct(this.state.chooseGroupList, $groupRow);
			this.renderGroupProductList();
		},
		batchDeleteGroup:function() {
			this.state.chooseGroupList = OrderManageUtil.batchDeleteGroup(this.$groupDataListListTable, this.state.chooseGroupList);
			this.renderGroupProductList();	
		},
		deleteGroup:function($deleteButton) {
			this.state.chooseGroupList = OrderManageUtil.deleteGroupProduct($deleteButton, this.state.chooseGroupList);
			this.renderGroupProductList();
		},
		changeGroupProductItem:function($productRow) {
			this.state.chooseGroupList = OrderManageUtil.changeGroupProductItem(this.state.chooseGroupList, $productRow);
			this.renderGroupProductList();
		},
		changeGroupMaterial:function($materialRow) {
			this.state.chooseGroupList = OrderManageUtil.changeGroupMaterial(this.state.chooseGroupList, $materialRow);
			// this.renderGroupProductList();
		},
		deleteGroupMaterial:function($materialRow) {
			this.state.chooseGroupList = OrderManageUtil.deleteGroupMaterial(this.state.chooseGroupList, $materialRow);
			this.renderGroupProductList();
		},
		changeTabIndex:function(jointid,isNew,tabIndex) {
			var groupIndex = _.findIndex(this.state.chooseGroupList,{jointProductId:jointid,isNew:parseInt(isNew)})
			this.state.chooseGroupList[groupIndex] = _.extend(this.state.chooseGroupList[groupIndex], {tabIndex:tabIndex});
		},

		//选择单价
		chooseProductUnitAmount:function($button) {
			var self = this;
			var productSkuId = $button.data('productskuid');
			self.queryLastPrice({productSkuId:productSkuId}, function(res) {
				$button.closest('.productUnitAmountTd').find('.productUnitAmount').val(res);
				self.changeProduct($button.closest('.skuRow'));
				self.getFirstPayAmount();
			})
		},
		chooseMaterialUnitAmount:function($button) {
			var self = this;
			var materialId = $button.data('materialid');
			self.queryLastPrice({materialId:materialId}, function(res) {
				$button.closest('.materialUnitAmountTd').find('.materialUnitAmount').val(res);
				self.changeMaterial($button.closest('.materialRow'));
				self.getFirstPayAmount();
			})
		},
		chooseGroupProductUnitAmount:function($button) {
			var self = this;
			var productSkuId = $button.data('productskuid');
			self.queryLastPrice({productSkuId:productSkuId}, function(res) {
				$button.closest('.productUnitAmountTd').find('.productUnitAmount').val(res);
				self.changeGroupProductItem($button.closest('.productRow'));
				self.getFirstPayAmount();
			})
		},
		chooseGroupMaterialUnitAmount:function($button) {
			var self = this;
			var materialId = $button.data('materialid');
			self.queryLastPrice({materialId:materialId}, function(res) {
				$button.closest('.materialUnitAmountTd').find('.materialUnitAmount').val(res);
				self.changeGroupMaterial($button.closest('.materialRow'));
				self.getFirstPayAmount();
			})
		},
		queryLastPrice:function(prams, callBack) {
			var self = this;
			if(!self.state.customerNo) {
				bootbox.alert('请先选择客户');
				return;
			}
			prams.customerNo = self.state.customerNo;
			Rental.ajax.submit("{0}order/queryLastPrice".format(SitePath.service), prams, function(response){
				if(response.success) {
					self.showLastPriceDialog(response.resultMap.data, callBack);	
				} else {
					Rental.notification.error("查询单价信息",response.description || '失败');
				}
			}, null ,"查询单价信息");
		},
		showLastPriceDialog:function(data, callBack) {
			var priceTextMap = {
				productSkuLastDayPrice:'客户上次按天租',
				productSkuLastMonthPrice:'客户上次按月租',
				productSkuDayPrice:'当前按天租',
				productSkuMonthPrice:'当前按月租',

				materialLastDayPrice:'客户上次按天租',
				materialLastMonthPrice:'客户上次按月租',
				materialDayPrice:'当前按天租',
				materialMonthPrice:'当前按月租',
			}

			var keys= _.keys(data), opts = new Array();
			keys.forEach(function(item) {
				priceTextMap.hasOwnProperty(item) && opts.push({
					text: priceTextMap[item] + "：￥"+ data[item].toFixed(2),
		            value: data[item].toString(),
				});
			})

			bootbox.prompt({
			    title: "单价列表",
			    inputType: 'checkbox',
			    inputOptions: opts,
			    callback: function (result) {
			    	if(!!result) {
			    		if(result.length > 1) {
			    			bootbox.alert('只能选择一个价格');
			    			return;
			    		}
			    		_.isFunction(callBack) && callBack(result[0]);	
			    	}
			    }
			});
		},
		loadRisk:function(prams) {
			var self = this;
			ApiData.customerDetail(_.extend(prams, {
				success: function(response) {
					self.renderCustomerRiskManagement(response);
					self.renderCustomerInfoMixin(response);
				}
			}))
		},
		renderCustomerInfoMixin:function(customer) {
			$("#statementDateFormGroup").removeClass('hide');
			$("#statementDateValue").html(customer.hasOwnProperty('statementDate')  && !!customer.statementDate ? Enum.settlementDate.getValue(customer.statementDate) : '月末最后一天');
			this.statementDateStatus();
		},
		statementDateStatus:function() {
			var rentType = parseInt($('[name=rentType]', this.$form).val());
			if(!!this.state.customerNo) {
				if(rentType == Enum.rentType.num.byDay) {
					$("#statementDateFormGroup").addClass('hide');
				} else if(rentType == Enum.rentType.num.byMonth) {
					$("#statementDateFormGroup").removeClass('hide');
				}
			}
		},
		renderCustomerRiskManagement:function(customer) {
			var self = this;
			var data = _.extend(Rental.render, {
				customerRiskManagement:customer.customerRiskManagement || new Object(),
				subsidiary:function() {
					return customer.customerCompany.hasOwnProperty("subsidiary") ? customer.customerCompany.subsidiary : false;
				},
				creditAmountFormat:function() {
					return this.creditAmount ? this.creditAmount.toFixed(2) : '0.00';
				},
				creditAmountUsedFormat:function() {
					return this.creditAmountUsed ? this.creditAmountUsed.toFixed(2) : '0.00';
				},
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				applePayModeValue:function() {
					return Enum.payMode.getValue(this.applePayMode);
				},
				newPayModeValue:function() {
					return Enum.payMode.getValue(this.newPayMode);
				},
				riskButton:function() {
					return null;
				},
				singleLimitPriceValue:function() {
					return this.hasOwnProperty('singleLimitPrice')  && _.isNumber(this.singleLimitPrice) ? '￥'+parseFloat(this.singleLimitPrice).toFixed(2) : '不限';
				}
			})
			var customerStatusTpl = $("#customerRiskPanelTpl").html();
			Mustache.parse(customerStatusTpl);
			$("#customerRiskPanel").html(Mustache.render(customerStatusTpl, data));
		},
	};

	window.OrderMixIn = OrderMixIn;

})(jQuery);