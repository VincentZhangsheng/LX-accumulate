;(function($) {
	var ChangeUnitPrice = {
		modal:function(prams) {
			this.props = _.extend({
				orderNo:null,
                callBack:function() {},
                complete:function() {},
                modalCloseCallBack:function() {},
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
            Rental.modal.open({src:SitePath.base + "order-manage/change-price", type:'ajax', 
                ajaxContentAdded:function(pModal) {
                    self.initDom(pModal);
                    self.initEvent();	
                }
            });
		},
		initDom:function(modal) {
            this.$changePriceModal = $("#changePriceModal");
            this.$changePriceForm = $("#changePriceForm");
			this.$productListTpl = $("#orderProductListTpl").html();
            this.$productListTable = $("#orderProductListTable");
            this.$materialListTpl = $("#orderMaterialListTpl").html();
			this.$materialListTable = $("#orderMaterialListTable");
		},
		initEvent:function() {
			var self = this;
			self.loadData();

			Rental.form.initFormValidation(self.$changePriceForm, function(form){
				self.confirmDo();
			});

			self.$changePriceModal.on('click','.cancelButton',function(event) {
				event.preventDefault();
				Rental.modal.close();
			})

			Rental.helper.onOnlyDecmailNum(self.$changePriceModal, '.productUnitAmount', 4); 
			Rental.helper.onOnlyDecmailNum(self.$changePriceModal, '.materialUnitAmount', 4); 
		},
        //订单商品
		loadData:function() {
            var self = this;
            if(!self.props.orderNo) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('order/queryOrderByNo', {orderNo:self.props.orderNo}, '加载订单详细信息', function(response) {
                self.initOrderData(response.resultMap.data)
			});
		},
		initOrderData:function(data) {
			$("#rentType").val(data.rentType).prop({disabled:true})

			var productListData = data.hasOwnProperty("orderProductList") ? data.orderProductList : [];
			this.initProductList(productListData);

			var materialListData = data.hasOwnProperty("orderMaterialList") ? data.orderMaterialList : [];
			this.initMaterialList(materialListData);
        },
        initProductList:function(listData) {
			var self = this;
            $("#orderItemProductCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					productInfo:function() {
						if(this.hasOwnProperty('productSkuSnapshot')) {
							return JSON.parse(this.productSkuSnapshot);
						}
					},
					propertiesToStr:function() {
						var str = "";
						if(this.hasOwnProperty('productSkuPropertyList')) {
							str = this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ");
						} else if(this.hasOwnProperty('productSkuSnapshot')) {
							var productInfo = JSON.parse(this.productSkuSnapshot);
							str = productInfo.hasOwnProperty("productSkuList") ? productInfo.productSkuList.length > 0 ? (productInfo.productSkuList[0].hasOwnProperty("productSkuPropertyList") ?
							productInfo.productSkuList[0].productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : "") : "" : ""; 
						}
                        return str;
					},
					currentSkuPrice:function() {
						if((this.hasOwnProperty('isNewProduct') && parseInt(this.isNewProduct)) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							if(this.hasOwnProperty('newSkuPrice')) {
								return this.newSkuPrice.toFixed(2)
							} else if(this.hasOwnProperty('productSkuSnapshot')) {
								var productInfo = JSON.parse(this.productSkuSnapshot);
								return productInfo.productSkuList.length > 0 ? productInfo.productSkuList[0].newSkuPrice.toFixed(2) : "";
							}
						} else {
							if(this.hasOwnProperty('skuPrice')) {
								return this.skuPrice.toFixed(2)
							} else if(this.hasOwnProperty('productSkuSnapshot')) {
								var productInfo = JSON.parse(this.productSkuSnapshot);
								return productInfo.productSkuList.length > 0 ? productInfo.productSkuList[0].skuPrice.toFixed(2) : "";
							}
						}
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
				})
			}
			Mustache.parse(this.$productListTpl);
			this.$productListTable.html(Mustache.render(this.$productListTpl, data));
        },
        initMaterialList:function(listData) {
            var self = this;
            $("#orderItemMaterialCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    materialInfo:function() {
						if(this.hasOwnProperty('materialSnapshot')) {
							return JSON.parse(this.materialSnapshot);
						}
                    },
					materialTypeStr:function() {
                        if(this.hasOwnProperty("materialType")) {
                            return Enum.materialType.getValue(this.materialType);
                        } else if(this.hasOwnProperty('materialSnapshot')) {
                            return Enum.materialType.getValue((JSON.parse(this.materialSnapshot)).materialType);
                        }
					},
					currentMaterialPrice:function() {
						if((this.hasOwnProperty('isNewMaterial') && parseInt(this.isNewMaterial) == 1) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							if(this.hasOwnProperty('newMaterialPrice')) {
								return this.newMaterialPrice.toFixed(2)
							} else if(this.hasOwnProperty('materialSnapshot')) {
								var meterialInfo = JSON.parse(this.materialSnapshot);
								return meterialInfo.newMaterialPrice.toFixed(2)
							}
						} else {
							if(this.hasOwnProperty('materialPrice')) {
								return this.materialPrice.toFixed(2)
							} else if(this.hasOwnProperty('materialSnapshot')) {
								var meterialInfo = JSON.parse(this.materialSnapshot);
								return meterialInfo.materialPrice.toFixed(2)
							} 
						}
					},
				})
			}
			Mustache.parse(this.$materialListTpl);
			this.$materialListTable.html(Mustache.render(this.$materialListTpl, data));
        },
		getOrderProduct:function($productRow) {
			return {
				orderProductId:parseInt($productRow.data("id")),
				productUnitAmount:$('.productUnitAmount', $productRow).val(),
			}
		},
		getOrderProductList:function($dataListTable) {
			var self = this;
			var orderProductList = $('tr.productRow', $dataListTable).map(function() {
				return self.getOrderProduct($(this));
			}).toArray();
			return orderProductList;
		},
		getOrderMaterial:function($materialRow) {
			return {
				orderMaterialId:parseInt($materialRow.data("id")),
				materialUnitAmount:$('.materialUnitAmount', $materialRow).val(),
			}
		},
		getOrderMaterialList:function($dataListTable) {
			var self = this;
			var orderMaterial = $('tr.materialRow', $dataListTable).map(function() {
				return self.getOrderMaterial($(this));
			}).toArray();
			return orderMaterial;
		},
		confirmDo:function() {
			var self=this;
			try {
				var formData = Rental.form.getFormData(self.$changePriceForm);
				var orderProductList = self.getOrderProductList(self.$productListTable)
				var orderMaterialList = self.getOrderMaterialList(self.$materialListTable)
				
				var commitData = {
					orderNo:self.props.orderNo,
					orderProductList:orderProductList,
					orderMaterialList:orderMaterialList
				}

				var des = '修改订单商品项单价';
				Rental.ajax.submit("{0}order/updateOrderPrice".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.modal.close();
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("修改订单商品项单价", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
                Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.ChangeUnitPrice = ChangeUnitPrice;

})(jQuery)