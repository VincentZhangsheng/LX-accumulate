;(function($) {
	var ReceiptConfirm = {
		state:{
			dropzoneOnlyMany:'upLoadSignPager',
			dropzoneOnlyManyName:'客户回访图片',
		},
		modal:function(prams) {
			this.props = _.extend({
				orderNo:null,
				receiptType:1,
				isOrderList:null,
				detUrl:null,
				callBack:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "receipt-confirm/modal", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
            this.$groupProductModal = $("#receiptConfirmModal");
            this.$receiptForm = $("#receiptForm");
			this.$productListTpl = $("#productListTpl").html();
            this.$productListTable = $("#productListTable");
            this.$materialListTpl = $("#materialListTpl").html();
			this.$materialListTable = $("#materialListTable");
		},
		initEvent:function() {
			var self = this;

			self.loadData();
			self.initDropzone();

			Rental.form.initFormValidation(self.$receiptForm, function(form){
				self.receipt();
			});

			Rental.ui.renderSelect({
				container:$('#changeReasonType'),
				data:Enum.array(Enum.changeReasonType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					if(val == 3) {
						$("#changeReasonCol").removeClass("hide")
					} else {
						if(!$("#changeReasonCol").hasClass("hide")) {
							$("#changeReasonCol").addClass("hide")
						}
					}
				},
				defaultText:'请选择退货原因'
			});

			self.$receiptForm.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.$receiptForm.on('change', '.itemCount', function() {
				var orderRow = $(this).closest("tr");
				self.changeItemCount(orderRow);
			});

			Rental.helper.onOnlyNumber(self.$receiptForm, '.itemCount'); 
		},
		initDropzone:function() {
			Rental.ui.events.initDropzone($('#'+this.state.dropzoneOnlyMany), 'image/upload', 1); 
		},
		getImageList:function(container, des) {
			var productImgList = new Array(), $preview = $('.dz-image-preview', container);
			$preview.each(function(index, el) {
				var img = $(el).data('img');
				if(img) productImgList.push({imgId:img.imgId});
			});
			if($preview.size() > 0 && $preview.size() != productImgList.length) {
				Rental.notification.error(des || '资料上传', '有未上传成功的图片，请检查');
				return false;
			}
			return productImgList;
		},
        //订单商品
		loadData:function() {
            var self = this;
            if(!self.props.orderNo) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('order/queryOrderByNo', {orderNo:self.props.orderNo}, '加载订单详细信息', function(response) {
                self.initProductList(response.resultMap.data);
                self.initMaterialList(response.resultMap.data);
			});
        },
		initProductList:function(data) {
			var self = this;
            var listData = data.hasOwnProperty("orderProductList") ? data.orderProductList : [];
            $("#orderItemProductCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    productInfo:function() {
                        return this.hasOwnProperty('productSkuSnapshot') ? JSON.parse(this.productSkuSnapshot) : "";
                    },
					propertiesToStr:function() {
                        var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
                            return item.propertyValueName;
                        }).join(" | ") : '';
                        return str;
                    },
				})
			}
			Mustache.parse(this.$productListTpl);
			this.$productListTable.html(Mustache.render(this.$productListTpl, data));
        },
        initMaterialList:function(data) {
            var self = this;
            var listData = data.hasOwnProperty("orderMaterialList") ? data.orderMaterialList : [];
            $("#orderItemMaterialCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    materialInfo:function() {
                        return this.hasOwnProperty('returnMaterialSnapshot') ? JSON.parse(this.returnMaterialSnapshot) : "";
                    },
					materialTypeStr:function() {
                        if(this.hasOwnProperty("materialType")) {
                            return Enum.materialType.getValue(this.materialType);
                        } else if(this.hasOwnProperty('materialSnapshot')) {
                            return Enum.materialType.getValue((JSON.parse(this.materialSnapshot)).materialType);
                        }
					},
				})
			}
			Mustache.parse(this.$materialListTpl);
			this.$materialListTable.html(Mustache.render(this.$materialListTpl, data));
        },
		changeItemCount:function(orderRow) {
			var self = this;
			var orderitem = self.getOrderItem(orderRow);
			var changeItemList = new Array();
			if(orderitem.itemCount > orderitem.orderCount) {
				bootbox.alert('收货数量不能大于订单数量');
				orderRow.find(".itemCount").val("");
				orderitem.itemCount == "";
			}

			var orderItemList = self.getOrderItemList(self.$receiptForm);
			orderItemList = orderItemList.filter(function(item) {
				return item.itemCount != "";
			})
			orderItemList.forEach(function(item) {
				if( item.orderCount != item.itemCount) {
					changeItemList.push(item);
				}
			})

			if(changeItemList.length > 0) {
				$("#reasonTypeRow").removeClass("hide");
			}
			if(changeItemList.length == 0) {
				$("#reasonTypeRow").addClass("hide");
			}
		},
		getOrderItem:function($orderRow) {
			return {
				itemType:parseInt($orderRow.data("itemtype")),
				itemId:parseInt($orderRow.data("id")),
				orderCount:parseInt($('.orderCount', $orderRow).val()),
				itemCount:$('.itemCount', $orderRow).val(),
			}
		},
		getOrderItemList:function($dataListTable) {
			var self = this;
			var orderItemList = $('tr.orderItemRow', $dataListTable).map(function() {
				return self.getOrderItem($(this));
			}).toArray();
			return orderItemList;
		},
		receipt:function() {
			var self=this;
			try {
				var formData = Rental.form.getFormData(self.$receiptForm);
				var OrderConfirmChangeParam = new Array();

				$('tr.orderItemRow', self.$receiptForm).each(function() {
					var orderItem = self.getOrderItem($(this));
					var newItem = null;
					if(orderItem.itemCount == "") {
						newItem = _.extend({},{itemCount:orderItem.orderCount,itemType:orderItem.itemType,itemId:orderItem.itemId})
					} else {
						newItem = _.extend({},{itemCount:parseInt(orderItem.itemCount),itemType:orderItem.itemType,itemId:orderItem.itemId})
					}
					OrderConfirmChangeParam.push(newItem);
				})

				var manyValidate = true;
				var result = self.getImageList($('#' + self.state.dropzoneOnlyMany), self.state.dropzoneOnlyManyName);
				if(_.isArray(result) && result.length > 0) {
					formData[self.state.dropzoneOnlyMany] = result;
				} else if(_.isBoolean(result)) {
					manyValidate = result;	
				}
				if(!manyValidate) {
					bootbox.alert('请检查有未上传成功的图片');
					return;
				}
				if(!formData["upLoadSignPager"]) {
					bootbox.alert('请上传客户签字确认的交货单');
					return;
				}
				
				var commitData = {
					orderNo:self.props.orderNo,
					orderItemParamList:OrderConfirmChangeParam,
					deliveryNoteCustomerSignImg:formData["upLoadSignPager"][0]
				}
				
				var orderItemList = self.getOrderItemList(self.$receiptForm);
				orderItemList = orderItemList.filter(function(item) {
					return item.itemCount != "";
				})

				var filterItemList = orderItemList.filter(function(item) {
					return item.itemCount == item.orderCount;
				})

				if(orderItemList.length > 0 && orderItemList.length != filterItemList.length) {
					if(!formData.changeReasonType) {
						bootbox.alert('请选择退货原因');
						return;
					}
					if(formData.changeReasonType == 3 && !formData.changeReason) {
						bootbox.alert('请填写退货具体原因');
						return;
					}
					commitData.changeReasonType = formData["changeReasonType"]
					commitData.changeReason = formData["changeReason"]
				}

				var des = '确认收货';
				Rental.ajax.submit("{0}order/confirmChangeOrder".format(SitePath.service),commitData,function(response){
					if(response.success) {
						_.isFunction(OrderHandleMixin.receiptSuccess) && OrderHandleMixin.receiptSuccess(self.props.orderNo, self.props.isOrderList, self.props.detUrl);
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, "dialogLoading");
			} catch(e) {
				Rental.notification.error("确认收货失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.ReceiptConfirm = ReceiptConfirm;

})(jQuery)