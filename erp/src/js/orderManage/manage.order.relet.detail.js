;(function($){
	
	var reletOrderDetail = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
			customerNo:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_order_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var submitRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_commit],
				editRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_edit],
				recalRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_recalculation],
				cancelRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_cancel];

			editRelet && this.actionButtons.push(AuthorUtil.button(editRelet,'editRelet','','编辑'));
			submitRelet && this.actionButtons.push(AuthorUtil.button(submitRelet,'submitRelet','','提交'));
			cancelRelet && this.actionButtons.push(AuthorUtil.button(cancelRelet,'cancelRelet','','取消'));
			recalRelet && this.actionButtons.push(AuthorUtil.button(recalRelet,'recalRelet','','续租重算'));
		},
		initDom:function() {
			this.$form = $("#orderDetailForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.Pager = new Pager();
			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			self.initHandleEvent(this.$form, {isOrderList:false,isReletDetail:true}, function() {
				self.loadData();
            });
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('reletOrder/queryReletOrderByNo', {reletOrderNo:self.state.no}, '加载订单详细信息', function(response) {
				self.initData(response.resultMap.data);
			}, null, 'dialogLoading');
		},
		initData:function(data) {
			this.state.order = data;
			this.renderOrderBaseInfo(data);
			this.renderOrderDetailInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.renderActionButton(data);
			this.renderCustomerInfo(data.buyerCustomerNo);
		},
		doSearch: function(pageNo) {
			this.getReletList({pageNo: pageNo || 1})
		},

		isNewProduct: function(num) {
			switch(num) {
				case 0:
					return "次新";
				case 1:
					return "全新";
				default:
					return "";
			}
		},
		renderActionButton:function(order) {
            var self = this;
            self.actionButtons.forEach(function(item) {
                item.reletOrderNo = self.state.no
            }) 

			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterReletButtons(self.actionButtons, order),
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#orderNo").html(order.orderNo);
			order.isK3Order == 1 && $("#spanIsK3Order").removeClass('hide');
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderDetailInfo:function(order) {
			this.renderOrderInfo(order, $('#orderDetailInfoTpl').html(), $('#orderDetailInfo'));
			$('#statementDate').html(order.hasOwnProperty('statementDate')  && !!order.statementDate ? Enum.settlementDate.getValue(order.statementDate) : '月末最后一天');
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				orderStatusValue:function() {
					return Enum.orderStatus.getValue(order.orderStatus);
				},
				// orderTimeAxisTree:function() {
				// 	var list = this.orderTimeAxisList;
				// 	return list.map(function(item, index) {
				// 		item.isLastLine = index == list.length-1;
				// 		item.inlineClass = item.isLastLine ? 'Rental-timeline-item-head-green' : 'Rental-timeline-item-head-blue';
				// 		item.orderStatusValue = Enum.orderTimeAxisStatus.getValue(item.orderStatus);
				// 		item.createTimeFormat = Rental.helper.timestampFormat(item.createTime);
				// 		return item;
				// 	});
				// },
				totalRentalAmoutFont:function() {
					var totalProductAmount = this.hasOwnProperty('totalProductAmount') ? parseFloat(this.totalProductAmount) : 0;
					var totalMaterialAmount = this.hasOwnProperty('totalMaterialAmount') ? parseFloat(this.totalMaterialAmount) : 0;
					return (totalProductAmount + totalMaterialAmount).toFixed(2);
				},
				deliveryModeValue:function() {
					return Enum.deliveryMode.getValue(this.deliveryMode);
				},
				rentTypeValue:function() {
					return Enum.rentType.getValue(this.rentType);
				},
				rentTypeUnit:function() {
					return Enum.rentType.getUnit(this.rentType);
				},
				rentTypeByMonth:function() {
					return this.rentType == Enum.rentType.num.byMonth;
				},
				cancelReasonValue:function() {
					return Enum.cancelReason.getValue(this.cancelOrderReasonType);
				},
				canceled:function() {
					return this.hasOwnProperty("cancelOrderReasonType") ? true : false;
				},
				orderMessageData:function() {
					return this.hasOwnProperty("orderMessage") ? (!!this.orderMessage && JSON.parse(this.orderMessage)) : [];
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = OrderManageUtil.initChooseProductList(data.reletOrderProductList);
			this.renderProductList();
		},	
		initChooseMaterialList:function(data) {
			this.state.chooseMaterialList = OrderManageUtil.initChooseMaterialList(data.reletOrderMaterialList);
			this.renderMaterialList();
		},
		defaultTab:function() {
			if(this.state.chooseProductList.length == 0) {
				$('.orderItemTabs li').removeClass('active').eq(1).addClass('active');
				$('.tab-pane-order').removeClass('active').eq(1).addClass('active');
			} 
		},
		renderCustomerInfo:function(customerNo) {
			var self = this;
			ApiData.customerDetail({
				customerNo:customerNo,
				success:function(response) {
					var customerRiskManagement = response.customerRiskManagement || {};
					var creditAmount = customerRiskManagement.hasOwnProperty('creditAmount') ? parseFloat(customerRiskManagement.creditAmount) : 0;
					var creditAmountUsed = customerRiskManagement.hasOwnProperty('creditAmountUsed') ?parseFloat(customerRiskManagement.creditAmountUsed) : 0;
					$('#creditAmount').html("￥" + creditAmount.toFixed(2));
					$('#creditAmountUsed').html("￥" + creditAmountUsed.toFixed(2));
					$('#surplusCreditAmount').html("￥" + (creditAmount - creditAmountUsed).toFixed(2));
				}
			})
		},
	};

	window.reletOrderDetail = _.extend(reletOrderDetail, OrderMixIn, OrderHandleMixin);

})(jQuery);