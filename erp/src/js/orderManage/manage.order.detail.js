;(function($){
	
	var OrderDetail = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			chooseGroupList:new Array(),
			no:null,
			customerNo:null
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_order_detail];
				this.initActionButtons();
			} catch (e) {
				// bootbox.alert('您没有此页面相关权限，请联系系统管理员');
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.pickingProductButtons = new Array;
			this.pickingMaterialButtons = new Array;
			this.reletButtons = new Array;
			this.showK3No = null;

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var edit = this.currentManageListAuthor.children[AuthorCode.manage_order_edit],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_order_submit],
				confirmReceipt = this.currentManageListAuthor.children[AuthorCode.manage_order_confirm_receipt],
				confirmReceiptNew = this.currentManageListAuthor.children[AuthorCode.manage_order_confirm_receipt_new],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_order_cancel],
				picking = this.currentManageListAuthor.children[AuthorCode.manage_order_picking],
				deliverGoods = this.currentManageListAuthor.children[AuthorCode.manage_order_deliver_goods],
				print = this.currentManageListAuthor.children[AuthorCode.manage_order_print],
				pickPrint = this.currentManageListAuthor.children[AuthorCode.manage_order_pick_print],
				strongCancel = this.currentManageListAuthor.children[AuthorCode.manage_order_strong_cancel],
				again = this.currentManageListAuthor.children[AuthorCode.manage_order_again],
				addRemark = this.currentManageListAuthor.children[AuthorCode.manage_order_add_remark],
				relet = this.currentManageListAuthor.children[AuthorCode.manage_order_relet],
				recalculation = this.currentManageListAuthor.children[AuthorCode.manage_order_recalculation],
				changeSettlement = this.currentManageListAuthor.children[AuthorCode.manage_order_statement_change],
				splitSettlement = this.currentManageListAuthor.children[AuthorCode.manage_order_statement_split],
				editOrderItemPrice = this.currentManageListAuthor.children[AuthorCode.manage_order_edit_order_item_price];
				

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			deliverGoods && this.actionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			confirmReceipt && this.actionButtons.push(AuthorUtil.button(confirmReceipt,'confirmReceiptButton','','确认收货'));
			confirmReceiptNew && this.actionButtons.push(AuthorUtil.button(confirmReceiptNew,'confirmReceiptButtonNew','','确认收货'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
			strongCancel && this.actionButtons.push(AuthorUtil.button(strongCancel,'strongCancel','','强制取消'));
			addRemark && this.actionButtons.push(AuthorUtil.button(addRemark,'addRemark','','添加备注'));
			print && this.actionButtons.push(AuthorUtil.button(print,'printButton','','打印交货单',true,true));
			pickPrint && this.actionButtons.push(AuthorUtil.button(_.extend(pickPrint,{menuUrl:"order-manage/pick-list-print"}),'pickPrintButton','','打印备货单',true,true));
			again && this.actionButtons.push(AuthorUtil.button(again,'againButton','fa fa-plus','再来一单'));
			relet && this.actionButtons.push(AuthorUtil.button(relet,'reletButton','','续租'));
			recalculation && this.actionButtons.push(AuthorUtil.button(recalculation,'recalButton','','订单重算'));
			changeSettlement && this.actionButtons.push(AuthorUtil.button(changeSettlement,'changeSettlement','','修改结算日'));
			splitSettlement && this.actionButtons.push(AuthorUtil.button(splitSettlement,'sectionSettlement','','分段结算'));
			editOrderItemPrice && this.actionButtons.push(AuthorUtil.button(editOrderItemPrice,'changePrice','','修改商品项单价'));

			picking && this.pickingProductButtons.push(AuthorUtil.button(picking,'pickingProductButton','','配货'));
			picking && this.pickingProductButtons.push(AuthorUtil.button(picking,'removePickingProductButton','','清货'));
			picking && this.pickingMaterialButtons.push(AuthorUtil.button(picking,'pickingMaterialButton','','配货'));
			picking && this.pickingMaterialButtons.push(AuthorUtil.button(picking,'removePickingMaterialButton','','清货'));

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;
			var viewRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_view],
				submitRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_commit],
				cancelRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_cancel],
				editRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_edit],
				recalRelet = this.currentPageAuthor.children[AuthorCode.manage_order_relet_recalculation];

			editRelet && this.reletButtons.push(AuthorUtil.button(editRelet,'editRelet','','编辑'));
			submitRelet && this.reletButtons.push(AuthorUtil.button(submitRelet,'submitRelet','','提交'));
			cancelRelet && this.reletButtons.push(AuthorUtil.button(cancelRelet,'cancelRelet','','取消'));
			recalRelet && this.reletButtons.push(AuthorUtil.button(recalRelet,'recalRelet','','续租重算'));
			viewRelet && this.reletButtons.push(AuthorUtil.button(viewRelet,'viewRelet','','详情'));

			this.showK3No = this.currentPageAuthor.children[AuthorCode.manage_order_detail_showK3No];
		},
		initDom:function() {
			this.$form = $("#orderDetailForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
			this.$reletOrderLink = $("#modalList");
			this.$reletOrderListTpl = $("#reletOrderTpl").html();
			this.$reletOrderListTable = $("#reletOrderTable");
			this.$groupDataListTpl = $('#groupListTpl').html();
			this.$groupDataListListTable = $('#groupListTable');

			this.Pager = new Pager();
			this.printPager = new Pager();

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			self.getReletList();

			self.initHandleEvent(this.$form, {isOrderList:false}, function() {
				self.loadData();
				self.getReletList();
			});

			//打印记录
			orderPrintRecord.init({
				referNo:self.state.no,
                referType:Enum.printReferType.num.order
			});

			self.$reletOrderListTable.on('click', '.viewWorkFlowBtn', function(event) {
				event.preventDefault();
				var reletOrderNo = $(this).data('no');
				ViewWorkFlow.init({
					workflowType:Enum.workflowType.num.reletOrder,
					workflowReferNo:reletOrderNo,
				});
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('order/queryOrderByNo', {orderNo:self.state.no}, '加载订单详细信息', function(response) {
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.state.order = data;
			this.renderOrderBaseInfo(data);
			this.renderOrderDetailInfo(data);
			this.renderOrderMessageInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.renderActionButton(data);
			this.renderPickingProductButtons(data);
			this.defaultTab();
			this.renderStatementOrderDetailList(data.statementOrder);
			this.renderReturnOrderList(data);
			this.renderReceiptRecord(data);
			this.renderImgBox(data);
			this.renderStatementDateSplit(data);
		},
		getReletList: function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
				orderNo: this.state.no
			}, prams || {});
			Rental.ajax.ajaxDataNoLoading('reletOrder/page', searchData, '加载续租单', function(response) {
				self.renderReletOrder(response.resultMap.data);
			});
		},
		renderReletOrder: function(data) {
			var self = this;
			self.renderReletOrderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			}, $('.pagerContainer', self.container));
		},
		doSearch: function(pageNo) {
			this.getReletList({pageNo: pageNo || 1})
		},
		renderReletOrderList: function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var hasReletButtons = self.reletButtons && self.reletButtons.length > 0;
			$("#orderReletCount").html(listData.length);
			var data = {
				hasReletButtons:hasReletButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.reletDetail, this.reletOrderNo);
					},
					reletButtons:function() {
						return self.filterReletButtons(self.reletButtons, this);
					},
					"orderStatusValue": function() {
						return Enum.reletOrderStatus.getValue(this.reletOrderStatus);
					},
					"payStatusValue": function() {
						return Enum.payStatus.getValue(this.payStatus);
					},
					"rentLengthTypeValue": function() {
						return Enum.rentType.getValue(this.rentType);
					},
					"rentTypeUnit": function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					rentTypeByMonth:function() {
						return this.rentType == Enum.rentType.num.byMonth;
					},
					statementDateValue:function() {
						return this.hasOwnProperty('statementDate')  && !!this.statementDate ? Enum.settlementDate.getValue(this.statementDate) : '月末最后一天';
					},
					showViewWorkFlowButton:function() {
						return this.reletOrderStatus == Enum.reletOrderStatus.num.inAudit;
					},
					reletOrderStatusClass:function() {
						return Enum.reletOrderStatus.getClass(this.reletOrderStatus);
					},
				})
			}
			Mustache.parse(this.$reletOrderListTpl);
			this.$reletOrderListTable.html(Mustache.render(this.$reletOrderListTpl, data));
		},

		isNewProduct: function(num) {
			switch(num) {
				case 0:
					return "次新";
				case 1:
					return "全新";
				default:
					return "";
			}
		},
		renderActionButton:function(order) {
			var self = this;
			var actionButtonsTpl = $('#actionButtonsTpl').html();
			var data = {
				acitonButtons:self.filterAcitonButtons(self.actionButtons, order),
			}
			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderPickingProductButtons:function(order) {
			var self = this,
				actionButtonsTpl = $('#pickingProductButtonsTpl').html(),
				actionButtons = self.filterAcitonButtons(self.pickingProductButtons, order);
			var data = {
				hasActionButtons:actionButtons.length > 0,
				acitonButtons:actionButtons
			}
			Mustache.parse(actionButtonsTpl);
			$("#pickingProductButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#orderNo").html(order.orderNo);
			order.isK3Order == 1 && $("#spanIsK3Order").removeClass('hide');
			this.renderOrderInfo(order, $('#orderBaseInfoTpl').html(), $('#orderBaseInfo'));
		},
		renderOrderDetailInfo:function(order) {
			this.renderOrderInfo(order, $('#orderDetailInfoTpl').html(), $('#orderDetailInfo'));
		},
		renderStatementDateSplit:function(order) {
			var hasStatementDateSplit = order.hasOwnProperty("orderStatementDateSplit") ? true : false;
			var statementDateSplit = order.hasOwnProperty("orderStatementDateSplit") ? order.orderStatementDateSplit : {};
			var data = _.extend(Rental.render, {
				hasStatementDateSplit:hasStatementDateSplit,
				statementDateSplit : statementDateSplit,
				changeTypeVal:function() {
					return this.changeType == 1 ? "结算到下一结算日" : (this.changeType == 0 ? "结算到本月底" : "");
				},
				beforeStatementDateVal:function() {
					return Enum.settlementDate.getValue(this.beforeStatementDate);
				},
				afterStatementDateVal:function() {
					return Enum.settlementDate.getValue(this.afterStatementDate);
				}
			})
			var orderStatementDateSplitTpl = $("#orderStatementDateSplitTpl").html();
			Mustache.parse(orderStatementDateSplitTpl);
			$("#orderStatementDateSplit").html(Mustache.render(orderStatementDateSplitTpl, data));
		},
		renderOrderMessageInfo:function(order) {
			this.renderOrderInfo(order, $('#orderMessageTpl').html(), $('#orderMessage'));
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				customerUrl:function() {
					return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.buyerCustomerNo);
				},
				orderStatusValue:function() {
					return Enum.orderStatus.getValue(order.orderStatus);
				},
				orderTimeAxisTree:function() {
					var list = this.orderTimeAxisList;
					return list.map(function(item, index) {
						item.isLastLine = index == list.length-1;
						item.inlineClass = item.isLastLine ? 'Rental-timeline-item-head-green' : 'Rental-timeline-item-head-blue';
						item.orderStatusValue = Enum.orderTimeAxisStatus.getValue(item.orderStatus);
						item.realName = item.hasOwnProperty("operationType") && item.operationType == 2 ? item.updateUserRealName : item.createUserRealName;
						item.createTimeFormat = Rental.helper.timestampFormat(item.createTime);
						item.operationVal = Enum.orderOperationType.getValue(item.operationType);
						return item;
					});
				},
				totalRentalAmoutFont:function() {
					var totalProductAmount = this.hasOwnProperty('totalProductAmount') && this.totalProductAmount ? parseFloat(this.totalProductAmount) : 0;
					var totalMaterialAmount = this.hasOwnProperty('totalMaterialAmount') && this.totalMaterialAmount ? parseFloat(this.totalMaterialAmount) : 0;
					return (totalProductAmount + totalMaterialAmount).toFixed(2);
				},
				deliveryModeValue:function() {
					return Enum.deliveryMode.getValue(this.deliveryMode);
				},
				rentTypeValue:function() {
					return Enum.rentType.getValue(this.rentType);
				},
				rentTypeUnit:function() {
					return Enum.rentType.getUnit(this.rentType);
				},
				rentTypeByMonth:function() {
					return this.rentType == Enum.rentType.num.byMonth;
				},
				cancelReasonValue:function() {
					return Enum.cancelReason.getValue(this.cancelOrderReasonType);
				},
				canceled:function() {
					return this.hasOwnProperty("cancelOrderReasonType") ? true : false;
				},
				orderMessageData:function() {
					return this.hasOwnProperty("orderMessage") ? (!!this.orderMessage && JSON.parse(this.orderMessage)) : [];
				},
				surplusCreditAmount:function() {
					var creditAmount = order.hasOwnProperty("customerRiskManagement") ? (order.customerRiskManagement.hasOwnProperty("creditAmount") ? order.customerRiskManagement.creditAmount : "") : "";
					var creditAmountUsed = order.hasOwnProperty("customerRiskManagement") ? (order.customerRiskManagement.hasOwnProperty("creditAmountUsed") ? order.customerRiskManagement.creditAmountUsed : "") : "";
					if(creditAmount !== "" && creditAmountUsed !== "") {
						return (creditAmount - creditAmountUsed).toFixed(2);
					}
				},
				statementDateValue:function() {
					return this.hasOwnProperty('statementDate')  && !!this.statementDate ? Enum.settlementDate.getValue(this.statementDate) : '月末最后一天';
				},
				parentCompany:function() {
					return order.hasOwnProperty("customerCompanyDTO") ? (order.customerCompanyDTO.hasOwnProperty("parentCustomerName") ? order.customerCompanyDTO.parentCustomerName : "") : "";
				},
				parentCompanyUrl:function() {
					return order.hasOwnProperty("customerCompanyDTO") ? (order.customerCompanyDTO.hasOwnProperty("parentCustomerNo") ? '{0}?no={1}'.format(PageUrl.commonCustomerDetail, order.customerCompanyDTO.parentCustomerNo) : "") : "";
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = OrderManageUtil.initChooseProductList(data.orderProductList);
			this.renderProductList();
		},	
		initChooseMaterialList:function(data) {
			this.state.chooseMaterialList = OrderManageUtil.initChooseMaterialList(data.orderMaterialList);
			this.renderMaterialList(this.filterAcitonButtons(this.pickingMaterialButtons, data));
		},
		initTotalFirstNeedPay:function(totalContainer, totalPrice) {
			if(!!totalPrice == false) {
				totalContainer.addClass('hide');
				return;
			}
			totalContainer.removeClass('hide');
			$(".num", totalContainer).html("￥"+parseFloat(totalPrice).toFixed(2));
		},
		defaultTab:function() {
			if(this.state.chooseProductList.length == 0) {
				$('.orderItemTabs li').removeClass('active').eq(1).addClass('active');
				$('.tab-pane-order').removeClass('active').eq(1).addClass('active');
			} 
		},
		renderStatementOrderDetailList:function(order) {
			var self = this;
			var listData = order.hasOwnProperty("statementOrderDetailList") ? order.statementOrderDetailList : [];
			$("#orderStatementCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					statementUrl:function() {
						return '{0}?no={1}'.format(PageUrl.statementOrderDetail, this.statementOrderNo);
					},
					statementDetailStatusValue:function() {
						return Enum.statementOrderStatus.getValue(this.statementDetailStatus);
					},
					orderTypeValue:function() {
						return Enum.orderType.getValue(this.orderType);
					},
					orderItemTypeValue:function() {
						switch(this.orderItemType) {
							case 1:
								return "商品";
							case 2:
								return "配件";
							default:
								return "";
						}
					},
					itemRentTypeValue:function() {
						return Enum.rentType.getValue(this.itemRentType);
					},
					statementDetailTypeValue:function() {
						return Enum.statementDetailType.getValue(this.statementDetailType);
					}
				}),
			}
			var tpl = $('#statementOrderDetailListTpl').html();
			Mustache.parse(tpl);
			$("#statementOrderDetailList").html(Mustache.render(tpl, data));
		},
		renderReturnOrderList:function(order) {
			var self = this;
			var listData = order.hasOwnProperty("k3ReturnOrderDetailList") ? order.k3ReturnOrderDetailList : [];
			$("#orderReturnCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					returnOrderUrl:function() {
						return '{0}?no={1}'.format(PageUrl.k3ReturnOrderDetail, this.returnOrderNo);
					},
				}),
			}
			var tpl = $('#returnOrderTpl').html();
			Mustache.parse(tpl);
			$("#returnOrderTable").html(Mustache.render(tpl, data));
		},
		renderReceiptRecord:function(order) {
			var self = this;
			var receipt = null;
			if(order.orderStatus == 20 || order.orderStatus == 22 || order.orderStatus == 24 || order.orderStatus == 44 || order.orderStatus == 48) {
				receipt = order
			}
			var hasProductList = order.hasOwnProperty("orderProductList") && order.orderProductList.length > 0;
			var hasMaterialList = order.hasOwnProperty("orderMaterialList") && order.orderMaterialList.length > 0;
			var hasChangeReason = order.hasOwnProperty("changeReason") && !!order.changeReason;

			var data = _.extend(Rental.render, {
				receipt:receipt,
				hasProductList:hasProductList,
				hasMaterialList:hasMaterialList,
				hasChangeReason:hasChangeReason,
				orderProduct:function() {
					var productList = this.orderProductList;
					return _.extend(Rental.render, {
						productList:productList,
						isNewProduct:function() {
							if(this.hasOwnProperty('isNewProduct')) {
								return this.isNewProduct;
							} else if(this.hasOwnProperty('isNew')) {
								return this.isNew;
							}
						},
						propertiesToStr:function() {
							var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
					})
				},
				orderMaterial:function() {
					var materialList = this.orderMaterialList;
					return _.extend(Rental.render, {
						materialList:materialList,
						isNewMaterial:function() {
							if(this.hasOwnProperty('isNewProduct')) {
								return this.isNewProduct;
							} else if(this.hasOwnProperty('isNew')) {
								return this.isNew;
							}
						},
						materialTypeStr:function() {
							if(this.hasOwnProperty("materialTypeName")) {
								return this.materialTypeName
							} 
							if(!this.hasOwnProperty("materialType") && this.hasOwnProperty("materialSnapshot")) {
								materialInfo = JSON.parse(this.materialSnapshot)
								return materialInfo.hasOwnProperty("materialType") ? materialInfo.materialTypeName : ""
							}
						},
					})
				}
			});
			var tpl = $('#receiptRecordTpl').html();
			Mustache.parse(tpl);
			$("#receiptRecordContent").html(Mustache.render(tpl, data));
		},
		renderImgBox:function(order) {
			var self = this;
			var img = order.hasOwnProperty("deliveryNoteCustomerSignImg") ? order.deliveryNoteCustomerSignImg : "";
			var	renderData = {
					img:img,
					url:function() {
						return this.imgDomain + this.imgUrl;
					},
				};

			var tpl = $("#imgBoxTpl").html();
			Mustache.parse(tpl);
			var imgBox = Mustache.render(tpl, renderData);
			$('#mix-container').append(imgBox)

			$("#mix-container").viewer()
		},
	};

	window.OrderDetail = _.extend(OrderDetail, OrderMixIn, OrderHandleMixin);

})(jQuery);