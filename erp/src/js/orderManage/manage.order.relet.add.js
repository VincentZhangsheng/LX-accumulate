;(function($) {
	var ReletOrderAdd = {
        state:{
			isNeed:false,
			reletOrderNo:null
        },
		modal:function(prams) {
			this.props = _.extend({
				orderNo:null,
				reletOrderNo:null,
                callBack:function() {},
                complete:function() {},
                modalCloseCallBack:function() {},
                continueFunc:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
            Rental.modal.open({src:SitePath.base + "relet-order/modal", type:'ajax', 
                ajaxContentAdded:function(pModal) {
                    self.initDom(pModal);
                    self.initEvent();	
                },
                afterClose:function() {
                    if(self.state.isNeed) {
                        _.isFunction(self.props.continueFunc) && self.props.continueFunc(self.state.reletOrderNo);
                    }
                }
            });
		},
		initDom:function(modal) {
            this.$groupProductModal = $("#reletOrderModal");
            this.$reletOrderForm = $("#reletOrderForm");
			this.$productListTpl = $("#productListTpl").html();
            this.$productListTable = $("#productListTable");
            this.$materialListTpl = $("#materialListTpl").html();
			this.$materialListTable = $("#materialListTable");
		},
		initEvent:function() {
			var self = this;
			self.loadData();

			Rental.form.initFormValidation(self.$reletOrderForm, function(form){
				self.relet();
			});

			self.initCommonEvent()
		},
        //订单商品
		loadData:function() {
            var self = this;
            if(!self.props.orderNo) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('order/queryOrderByNo', {orderNo:self.props.orderNo}, '加载订单详细信息', function(response) {
                self.initOrderData(response.resultMap.data)
			});
		},
		initOrderData:function(data) {
			$("#rentType").val(data.rentType).prop({disabled:true})

			var productListData = data.hasOwnProperty("orderProductList") ? data.orderProductList : [];
			this.initProductList(productListData);

			var materialListData = data.hasOwnProperty("orderMaterialList") ? data.orderMaterialList : [];
			this.initMaterialList(materialListData);
		},
		relet:function() {
			var self=this;
			try {
				var formData = Rental.form.getFormData(self.$reletOrderForm);
				var orderProductList = self.getReletProductList(self.$productListTable)
				var orderMaterialList = self.getReletMaterialList(self.$materialListTable)
				
				var commitData = {
					orderNo:self.props.orderNo,
					rentTimeLength:parseInt(formData["rentTimeLength"]),
					orderProductList:orderProductList,
					orderMaterialList:orderMaterialList
				}

				var des = '订单续租';
				Rental.ajax.submit("{0}reletOrder/create".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.modal.close();
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);

						self.state.isNeed = response.resultMap.data.isNeedVerify;
						self.state.reletOrderNo = response.resultMap.data.reletOrderNo;
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("订单续租", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
                Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.ReletOrderAdd = _.extend(ReletOrderAdd,ReletOrderMixin);

})(jQuery)