;(function($) {

	OrderPrintMixIn = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			customerNo:null,
			productRowNum:0,
			materialRowNum:0,
		},
		initCommonEvent:function() {
			var self = this;

			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});
		},
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				chooseProductList = data.map(function(item) {
					var product =  item.hasOwnProperty('productSkuSnapshot') ? JSON.parse(item.productSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0] || {}, item)
					} 
					return _.extend(product || {}, item);
				})
			}
			return chooseProductList;
		},
		resolveChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.materialSnapshot);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
		renderProductList:function(productList) {
			var self = this;
			productList = productList || new Array();
			var data = {
				hasListData:productList.length > 0,
				dataSource: _.extend(Rental.render, {
					"listData":productList,
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '在租':"下架";
					},
					propertiesToStr:function() {
						var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					currentSkuPrice:function() {
						if((this.hasOwnProperty('isNewProduct') && parseInt(this.isNewProduct) == 1) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							return this.hasOwnProperty('newSkuPrice') ? this.newSkuPrice : '';
						} else {
							return this.hasOwnProperty('skuPrice') ? this.skuPrice : '';
						}
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
					payPrice:function() {
						return (parseFloat(this.productCount) * parseFloat(this.productUnitAmount)).toFixed(2);
					},
					rentalWay:function() {
						return '押{0}付{1}'.format(this.depositCycle, this.paymentCycle);
					},
					rowNum:function() {
						self.state.productRowNum = self.state.productRowNum + 1;
						return self.state.productRowNum;
					},
					isRentTypeByMonth:function() {
						return this.rentType == Enum.rentType.num.byMonth;
					},
					isNewProductValue:function() {
						return this.isNewProduct == 1 ? "全新":"次新";
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			// this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
			return Mustache.render(this.$dataListTpl, data)
		},
		renderMaterialList:function(materialList) {
			var self = this;
			materialList = materialList || new Array();
			var data = {
				hasListData:materialList.length > 0,
				dataSource:_.extend(Rental.render, {
					"listData":materialList,
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					currentMaterialPrice:function() {
						if((this.hasOwnProperty('isNewMaterial') && parseInt(this.isNewMaterial) == 1) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							return this.hasOwnProperty('newMaterialPrice') ? this.newMaterialPrice : '';
						} else {
							return this.hasOwnProperty('materialPrice') ? this.materialPrice : '';
						}
					},
					rentTypeList:function() {
						return Enum.array(Enum.rentType, this.rentType);
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					payModeList:function() {
						return Enum.array(Enum.payMode, this.payMode);	
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
					payPrice:function() {
						return (parseFloat(this.materialCount) * parseFloat(this.materialUnitAmount)).toFixed(2);
					},
					rowNum:function() {
						self.state.materialRowNum = self.state.materialRowNum + 1;
						return self.state.materialRowNum;
					},
					isRentTypeByMonth:function() {
						return this.rentType == Enum.rentType.num.byMonth;
					},
					isNewMaterialValue:function() {
						return this.isNewMaterial == 1 ? "全新":"次新";
					}
				})
			}
			Mustache.parse(this.$materialDataListTpl);
			// this.$materialDataListTable.html(Mustache.render(this.$materialDataListTpl, data));
			return Mustache.render(this.$materialDataListTpl, data);

		},
	};

	window.OrderPrintMixIn = OrderPrintMixIn;

})(jQuery);