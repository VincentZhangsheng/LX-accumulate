;(function($) {
	var ReletOrderEdit = {
        state:{
            isNeed:false
        },
		modal:function(prams) {
			this.props = _.extend({
				reletOrderNo:null,
                callBack:function() {},
                complete:function() {},
                modalCloseCallBack:function() {},
                continueFunc:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
            Rental.modal.open({src:SitePath.base + "relet-order/modal", type:'ajax', 
                ajaxContentAdded:function(pModal) {
                    self.initDom(pModal);
                    self.initEvent();	
                },
                afterClose:function() {
                    if(self.state.isNeed) {
                        _.isFunction(self.props.continueFunc) && self.props.continueFunc(self.props.reletOrderNo);
                    }
                }
            });
		},
		initDom:function(modal) {
            this.$groupProductModal = $("#reletOrderModal");
            this.$reletOrderForm = $("#reletOrderForm");
			this.$productListTpl = $("#productListTpl").html();
            this.$productListTable = $("#productListTable");
            this.$materialListTpl = $("#materialListTpl").html();
			this.$materialListTable = $("#materialListTable");
		},
		initEvent:function() {
			var self = this;
			self.loadData();

			Rental.form.initFormValidation(self.$reletOrderForm, function(form){
				self.relet();
			});

			self.initCommonEvent()
		},
        //订单商品
		loadData:function() {
			var self = this;
            if(!self.props.reletOrderNo) {
				bootbox.alert('没找到续租单编号');
				return;
			}
			Rental.ajax.ajaxData('reletOrder/queryReletOrderByNo', {reletOrderNo:self.props.reletOrderNo}, '加载续租单详细信息', function(response) {
				self.initReletData(response.resultMap.data);
			});
		},
		initReletData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$reletOrderForm, data);

			$("#rentType").val(data.rentType).prop({disabled:true})

			var productListData = data.hasOwnProperty("reletOrderProductList") ? data.reletOrderProductList : [];
			this.initProductList(productListData);
			
			var materialListData = data.hasOwnProperty("reletOrderMaterialList") ? data.reletOrderMaterialList : [];
			this.initMaterialList(materialListData);
		},
		relet:function() {
			var self=this;
			try {
				var formData = Rental.form.getFormData(self.$reletOrderForm);
				var reletOrderProductList = self.getReletProductList(self.$productListTable)
				var reletOrderMaterialList = self.getReletMaterialList(self.$materialListTable)

				var commitData = {
					reletOrderNo:self.props.reletOrderNo,
					rentTimeLength:parseInt(formData["rentTimeLength"]),
					reletOrderProductList:reletOrderProductList,
					reletOrderMaterialList:reletOrderMaterialList
				}

				var des = '编辑续租单';
				Rental.ajax.submit("{0}reletOrder/update".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.modal.close();
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);

						self.state.isNeed = response.resultMap.data.isNeedVerify;
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch(e) {
				Rental.notification.error("编辑续租单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
                Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
			return false;
		}
	};

	window.ReletOrderEdit = _.extend(ReletOrderEdit,ReletOrderMixin);

})(jQuery)