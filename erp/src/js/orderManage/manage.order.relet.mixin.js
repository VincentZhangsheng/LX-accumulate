;(function($) {
	var ReletOrderMixin = {
		initCommonEvent:function() {
			var self = this;
			Rental.ui.renderSelect({
				container:$("#rentType"),
				data:Enum.array(Enum.rentType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {},
				defaultText:'请选择'
			});

			self.$groupProductModal.on('click','.cancelButton',function(event) {
				event.preventDefault();
				Rental.modal.close();
			})

			Rental.helper.onOnlyDecmailNum(self.$groupProductModal, '.productUnitAmount', 4); 
			Rental.helper.onOnlyDecmailNum(self.$groupProductModal, '.materialUnitAmount', 4); 
			Rental.helper.onOnlyNumber(self.$groupProductModal, '#rentTimeLength'); 
		},
        initProductList:function(listData) {
			var self = this;
            $("#orderItemProductCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					productInfo:function() {
						if(this.hasOwnProperty('productSkuSnapshot')) {
							return JSON.parse(this.productSkuSnapshot);
						}
					},
					propertiesToStr:function() {
						var str = "";
						if(this.hasOwnProperty('productSkuPropertyList')) {
							str = this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ");
						} else if(this.hasOwnProperty('productSkuSnapshot')) {
							var productInfo = JSON.parse(this.productSkuSnapshot);
							console.log(productInfo);
							str = productInfo.hasOwnProperty("productSkuList") ? productInfo.productSkuList.length > 0 ? (productInfo.productSkuList[0].hasOwnProperty("productSkuPropertyList") ?
							productInfo.productSkuList[0].productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : "") : "" : ""; 
						}
                        return str;
					},
					currentSkuPrice:function() {
						if((this.hasOwnProperty('isNewProduct') && parseInt(this.isNewProduct)) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							if(this.hasOwnProperty('newSkuPrice')) {
								return this.newSkuPrice.toFixed(2)
							} else if(this.hasOwnProperty('productSkuSnapshot')) {
								var productInfo = JSON.parse(this.productSkuSnapshot);
								return productInfo.productSkuList.length > 0 ? productInfo.productSkuList[0].newSkuPrice.toFixed(2) : "";
							}
						} else {
							if(this.hasOwnProperty('skuPrice')) {
								return this.skuPrice.toFixed(2)
							} else if(this.hasOwnProperty('productSkuSnapshot')) {
								var productInfo = JSON.parse(this.productSkuSnapshot);
								return productInfo.productSkuList.length > 0 ? productInfo.productSkuList[0].skuPrice.toFixed(2) : "";
							}
						}
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
				})
			}
			Mustache.parse(this.$productListTpl);
			this.$productListTable.html(Mustache.render(this.$productListTpl, data));
        },
        initMaterialList:function(listData) {
            var self = this;
            $("#orderItemMaterialCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    materialInfo:function() {
						if(this.hasOwnProperty('materialSnapshot')) {
							return JSON.parse(this.materialSnapshot);
						}
                    },
					materialTypeStr:function() {
                        if(this.hasOwnProperty("materialType")) {
                            return Enum.materialType.getValue(this.materialType);
                        } else if(this.hasOwnProperty('materialSnapshot')) {
                            return Enum.materialType.getValue((JSON.parse(this.materialSnapshot)).materialType);
                        }
					},
					currentMaterialPrice:function() {
						if((this.hasOwnProperty('isNewMaterial') && parseInt(this.isNewMaterial) == 1) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							if(this.hasOwnProperty('newMaterialPrice')) {
								return this.newMaterialPrice.toFixed(2)
							} else if(this.hasOwnProperty('materialSnapshot')) {
								var meterialInfo = JSON.parse(this.materialSnapshot);
								return meterialInfo.newMaterialPrice.toFixed(2)
							}
						} else {
							if(this.hasOwnProperty('materialPrice')) {
								return this.materialPrice.toFixed(2)
							} else if(this.hasOwnProperty('materialSnapshot')) {
								var meterialInfo = JSON.parse(this.materialSnapshot);
								return meterialInfo.materialPrice.toFixed(2)
							} 
						}
					},
				})
			}
			Mustache.parse(this.$materialListTpl);
			this.$materialListTable.html(Mustache.render(this.$materialListTpl, data));
        },
		getReletProduct:function($productRow) {
			return {
				orderProductId:parseInt($productRow.data("id")),
				originAmount:$productRow.data("amount"),
				newAmount:$('.productUnitAmount', $productRow).val(),
			}
		},
		getReletProductList:function($dataListTable) {
			var self = this;
			var reletProductList = $('tr.productRow', $dataListTable).map(function() {
				return self.getReletProduct($(this));
			}).toArray();
			reletProductList.forEach(function(item) {
				if(item.newAmount == "") {
					item.productUnitAmount = item.originAmount
				} else {
					item.productUnitAmount = parseFloat(item.newAmount)
				}
			})
			reletProductList = reletProductList.map(function(item) {
				return {
					orderProductId:item.orderProductId,
					productUnitAmount:item.productUnitAmount,
				}
			})
			return reletProductList;
		},
		getReletMaterial:function($materialRow) {
			return {
				orderMaterialId:parseInt($materialRow.data("id")),
				originAmount:$materialRow.data("amount"),
				newAmount:$('.materialUnitAmount', $materialRow).val(),
			}
		},
		getReletMaterialList:function($dataListTable) {
			var self = this;
			var reletMaterial = $('tr.materialRow', $dataListTable).map(function() {
				return self.getReletMaterial($(this));
			}).toArray();
			reletMaterial.forEach(function(item) {
				if(item.newAmount == "") {
					item.materialUnitAmount = item.originAmount
				} else {
					item.materialUnitAmount = parseFloat(item.newAmount)
				}
			})
			reletMaterial = reletMaterial.map(function(item) {
				return {
					orderMaterialId:item.orderMaterialId,
					materialUnitAmount:item.materialUnitAmount,
				}
			})
			return reletMaterial;
		},
	}
	window.ReletOrderMixin = ReletOrderMixin;

})(jQuery);