/**
 * decription: 待审核订单列表
 * author:zhangsheng
 * time: 2018-4-25
 */
;(function($) {

	var OrderToAuditManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
			this.orderListPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_add_waite_for_audit];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.orderListPageAuthor.hasOwnProperty('children') == false) return;

			var edit = this.orderListPageAuthor.children[AuthorCode.manage_order_edit],
				view = this.orderListPageAuthor.children[AuthorCode.manage_order_detail],
				submit = this.orderListPageAuthor.children[AuthorCode.manage_order_submit],
				// pay = this.orderListPageAuthor.children[AuthorCode.manage_order_pay],
				confirmReceipt = this.orderListPageAuthor.children[AuthorCode.manage_order_confirm_receipt],
				cancel = this.orderListPageAuthor.children[AuthorCode.manage_order_cancel],
				picking = this.orderListPageAuthor.children[AuthorCode.manage_order_picking],
				deliverGoods = this.orderListPageAuthor.children[AuthorCode.manage_order_deliver_goods],
				print = this.orderListPageAuthor.children[AuthorCode.manage_order_print],
				strongCancel = this.orderListPageAuthor.children[AuthorCode.manage_order_strong_cancel],
				again = this.orderListPageAuthor.children[AuthorCode.manage_order_again],
				addRemark = this.orderListPageAuthor.children[AuthorCode.manage_order_add_remark];

			// add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));

			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			// pay && this.rowActionButtons.push(AuthorUtil.button(pay,'payButton','','模拟支付'));
			picking && this.rowActionButtons.push(AuthorUtil.button(view,'pickingButton','','配货')); //列表页面配货跳转至详细页面配货
			deliverGoods && this.rowActionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			confirmReceipt && this.rowActionButtons.push(AuthorUtil.button(confirmReceipt,'confirmReceiptButton','','确认收货'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
			strongCancel && this.rowActionButtons.push(AuthorUtil.button(strongCancel,'strongCancel','','强制取消'));
			addRemark && this.rowActionButtons.push(AuthorUtil.button(addRemark,'addRemark','','添加备注'));
			print && this.rowActionButtons.push(AuthorUtil.button(print,'printButton','','打印',true,true));
			again && this.rowActionButtons.push(AuthorUtil.button(again,'againButton','fa fa-plus','再来一单'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.orderToAudit);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			self.searchData();  //初始化列表数据

			self.renderCommonActionButton();

			var orderStatuList = Enum.array(Enum.orderStatus).filter(function(statu){
				return statu.num != Enum.orderStatus.num.cancel;
			})

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

			Rental.ui.renderSelect({
				container:$('#rentType'),
				data:Enum.array(Enum.rentType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（租赁类型）'
            });
            
            // Rental.ui.renderSelect({
			// 	container:$('#deliveryMode'),
			// 	data:Enum.array(Enum.deliveryMode),
			// 	func:function(opt, item) {
			// 		return opt.format(item.num.toString(), item.value.toString());
			// 	},
			// 	change:function(val) {
			// 		self.searchData();
			// 	},
			// 	defaultText:'全部（送货方式）'
			// });

			ApiData.company({
				success:function(response) {

					var companyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.headOffice;
					});

					var deliverySubCompanyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.headOffice && company.subCompanyId != Enum.subCompany.num.telemarketing && company.subCompanyId != Enum.subCompany.num.channelBigCustomer;
					});

					function renderCompany(container, defaultText, companyData) {
						Rental.ui.renderSelect({
							container:container,
							data:companyData,
							func:function(opt, item) {
								return opt.format(item.subCompanyId, item.subCompanyName);
							},
							change:function(val) {
								self.searchData();
							},
							defaultText:defaultText
						});
					}

					renderCompany($("#subCompanyId"), '请选择订单所属公司', companyList);
					renderCompany($("#deliverySubCompanyId"), '请选择订单发货公司', deliverySubCompanyList);
				}
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.orderToAudit, searchData);
			
			Rental.ajax.ajaxData('order/queryVerifyOrder', searchData, '加载待审核订单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"orderStatusStr":function(){
						return Enum.orderStatus.getValue(this.orderStatus);
					},
					orderStatusClass:function() {
						return Enum.orderStatus.getClass(this.orderStatus);
					},
					payStatusValue:function() {
						return Enum.payStatus.getValue(this.payStatus);
					},
					payStatusClass:function() {
						return Enum.payStatus.getClass(this.payStatus);
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					orderItemName:function() {
						if(this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0) {
							return this.orderProductList[0].productName;
						}
						if(this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0) {
							return this.orderMaterialList[0].materialName;
						}
					},
					showViewWorkFlowButton:function() {
						return this.orderStatus == Enum.orderStatus.num.inReview;
					},
					isK3Order:function() {
						return this.isK3Order == 1;
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.OrderToAuditManage = _.extend(OrderToAuditManage,OrderHandleMixin);

})(jQuery);