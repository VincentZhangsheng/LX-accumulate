// 订单配货
;(function($) {

	var OrderPicking = {
		init:function(prams) {
			this.props = _.extend({
				title:'扫描设备号',
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "order-manage/picking", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$modal = $("#pickingModal");
			this.$form = $("#pickingForm");
			this.$cancelButton = $('.cancelButton', this.$form);
			this.equipmentNoInput = $('[name=equipmentNo]', this.$form);
			
			$('.modalTitle', this.$modal).html(this.props.title);

			this.equipmentNoInput.focus();
		},
		initEvent:function() {
			var self = this;
			Rental.form.initFormValidation(this.$form, function(form){
				self.submit();
			});
			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		submit:function() {
			try {
				this.callBackFunc(Rental.form.getFormData(this.$form));
			} catch (e) {
				Rental.notification.error("系统错误", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			this.props.hasOwnProperty('callBack') && this.props.callBack(response)
			this.equipmentNoInput.val('').focus();
		},
	};

	window.OrderPicking = OrderPicking;

})(jQuery)