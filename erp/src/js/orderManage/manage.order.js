;(function($) {

	var OrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_order_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_order_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_order_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_order_submit],
				// pay = this.currentPageAuthor.children[AuthorCode.manage_order_pay],
				confirmReceipt = this.currentPageAuthor.children[AuthorCode.manage_order_confirm_receipt],
				confirmReceiptNew = this.currentPageAuthor.children[AuthorCode.manage_order_confirm_receipt_new],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_order_cancel],
				picking = this.currentPageAuthor.children[AuthorCode.manage_order_picking],
				deliverGoods = this.currentPageAuthor.children[AuthorCode.manage_order_deliver_goods],
				print = this.currentPageAuthor.children[AuthorCode.manage_order_print],
				pickPrint = this.currentPageAuthor.children[AuthorCode.manage_order_pick_print],
				strongCancel = this.currentPageAuthor.children[AuthorCode.manage_order_strong_cancel],
				again = this.currentPageAuthor.children[AuthorCode.manage_order_again],
				addRemark = this.currentPageAuthor.children[AuthorCode.manage_order_add_remark],
				relet = this.currentPageAuthor.children[AuthorCode.manage_order_relet],
				recalculation = this.currentPageAuthor.children[AuthorCode.manage_order_recalculation],
				changeSettlement = this.currentPageAuthor.children[AuthorCode.manage_order_statement_change],
				splitSettlement = this.currentPageAuthor.children[AuthorCode.manage_order_statement_split],
				editOrderItemPrice = this.currentPageAuthor.children[AuthorCode.manage_order_edit_order_item_price];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));

			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			// pay && this.rowActionButtons.push(AuthorUtil.button(pay,'payButton','','模拟支付'));
			picking && this.rowActionButtons.push(AuthorUtil.button(view,'pickingButton','','配货')); //列表页面配货跳转至详细页面配货
			deliverGoods && this.rowActionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			confirmReceipt && this.rowActionButtons.push(AuthorUtil.button(view,'confirmReceiptButton','','确认收货'));
			confirmReceiptNew && this.rowActionButtons.push(AuthorUtil.button(view,'confirmReceiptButtonNew','','确认收货'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
			strongCancel && this.rowActionButtons.push(AuthorUtil.button(strongCancel,'strongCancel','','强制取消'));
			addRemark && this.rowActionButtons.push(AuthorUtil.button(addRemark,'addRemark','','添加备注'));
			print && this.rowActionButtons.push(AuthorUtil.button(print,'printButton','','打印交货单',true,true));
			pickPrint && this.rowActionButtons.push(AuthorUtil.button(_.extend(pickPrint,{menuUrl:"order-manage/pick-list-print"}),'pickPrintButton','','打印备货单',true,true));
			again && this.rowActionButtons.push(AuthorUtil.button(again,'againButton','fa fa-plus','再来一单'));
			relet && this.rowActionButtons.push(AuthorUtil.button(relet,'reletButton','','续租'));
			recalculation && this.rowActionButtons.push(AuthorUtil.button(recalculation,'recalButton','','订单重算'));

			changeSettlement && this.rowActionButtons.push(AuthorUtil.button(changeSettlement,'changeSettlement','','修改结算日'));
			splitSettlement && this.rowActionButtons.push(AuthorUtil.button(splitSettlement,'sectionSettlement','','分段结算'));
			editOrderItemPrice && this.rowActionButtons.push(AuthorUtil.button(editOrderItemPrice,'changePrice','','修改商品项单价'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			if(!Rental.helper.getUrlPara('orderStatus') && !Rental.helper.getUrlPara('payStatus') && !Rental.helper.getUrlPara('isReturnOverDue') && !Rental.helper.getUrlPara('isCanReletOrder')) {
				this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.order);
			}
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));
			Rental.ui.events.initRangeDatePicker($('#rentTimePicker'), $('#rentTimePickerInput'),  $("#startRentStartTime"), $("#endRentStartTime"));
			Rental.ui.events.initRangeDatePicker($('#deliveryTimePicker'), $('#deliveryTimePickerInput'),  $("#startExpectDeliveryTime"), $("#endExpectDeliveryTime"));

			self.renderCommonActionButton();

			var orderStatuList = Enum.array(Enum.orderStatus).filter(function(statu){
				return statu.num != Enum.orderStatus.num.cancel;
			})
			Rental.ui.multiselect({
				container:$('#orderStatusList'),
				optionsContainer:$('#orderStatusList optgroup'),
				data:orderStatuList,
				func:function(opt, item) {
					return item.num == Rental.helper.getUrlPara('orderStatus') ? 
					'<option value="{0}" selected>{1}</option>'.format(item.num.toString(), item.value.toString()) :
					opt.format(item.num.toString(), item.value.toString());
				},
				selectClass:"multiselect dropdown-toggle btn btn-sm btn-default back-white bradius",
				defaultText:'全部（订单状态）',
				nonSelectedText: '订单状态',
				nSelectedText: '种状态',
				allSelectedText: '选中所有',
				change:function(value) {
					self.searchData();
				}
			});

			Rental.ui.renderSelect({
				container:$('#isReturnOverDue'),
				data:Enum.array(Enum.isReturnOverDue),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'到期情况'
			});

			Rental.ui.renderSelect({
				container:$('#orderStatus'),
				data:Enum.array(Enum.orderStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（订单状态）'
			});

			Rental.ui.renderSelect({
				container:$('#payStatus'),
				data:Enum.array(Enum.payStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（支付状态）'
			});

			Rental.ui.renderSelect({
				container:$('#rentType'),
				data:Enum.array(Enum.rentType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（租赁类型）'
			});

			$('#isReletOrder').on("change",function() {
				self.searchData();
			})

			ApiData.company({
				success:function(response) {

					var companyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.headOffice;
					});

					var deliverySubCompanyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.headOffice && company.subCompanyId != Enum.subCompany.num.telemarketing && company.subCompanyId != Enum.subCompany.num.channelBigCustomer;
					});

					function renderCompany(container, defaultText, companyData) {
						Rental.ui.renderSelect({
							container:container,
							data:companyData,
							func:function(opt, item) {
								return opt.format(item.subCompanyId, item.subCompanyName);
							},
							change:function(val) {
								self.searchData();
							},
							defaultText:defaultText
						});
					}

					renderCompany($("#subCompanyId"), '请选择所属公司', companyList);
					renderCompany($("#deliverySubCompanyId"), '请选择发货公司', deliverySubCompanyList);
				}
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCanRelet();
			self.renderReturnOverDue();
			self.renderPayStatus();

			self.render(new Object());
			self.searchData();  //初始化列表数据

			//回收站
			self.renderRecycle();
			self.$searchForm.on("click","#recycleBtn",function(){
				if($("#isRecycleBin", self.$searchForm).val() == 1) {
					$("#isRecycleBin", self.$searchForm).val("0");
					$(this).removeClass("btn-primary").addClass("btn-default")
				} else {
					$("#isRecycleBin", self.$searchForm).val("1");
					$(this).removeClass("btn-default").addClass("btn-primary")
				}
				self.searchData();
			})

			self.initHandleEvent(this.$dataListTable, {isOrderList:true}, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

			self.$searchForm.on("click",":reset",function() {
				$(".multiselect", self.$searchForm).html('订单状态<b class="caret"></b>');
				$(".multiselect", self.$searchForm).attr("title","订单状态");
				$(".multiselect-container li", self.$searchForm).each(function(){
					$(this).removeClass("active");
				})
				$(".multiselect-container :checkbox", self.$searchForm).each(function(){
					$(this).prop("checked",false);
				})
				$("#isRecycleBin", self.$searchForm).val("0");
				self.searchStorage = Rental.searchStorage.clearAll();
			}) 
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		renderPayStatus:function() {
			var payStatus = Rental.helper.getUrlPara('payStatus')
			if(!!payStatus) {
				Rental.ui.renderFormData(this.$searchForm, {payStatus:payStatus});
			}
		},
		renderReturnOverDue:function() {
			var isReturnOverDue = Rental.helper.getUrlPara('isReturnOverDue')
			if(!!isReturnOverDue) {
				Rental.ui.renderFormData(this.$searchForm, {isReturnOverDue:isReturnOverDue});
			}
		},
		renderCanRelet:function() {
			var isCanReletOrder = Rental.helper.getUrlPara('isCanReletOrder')
			if(!!isCanReletOrder) {
				Rental.ui.renderFormData(this.$searchForm, {isCanReletOrder:isCanReletOrder});
			}
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.order, searchData);
			
			Rental.ajax.ajaxData('order/queryAllOrder', searchData, '加载订单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var user = Rental.localstorage.getUser();

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.orderDetail, this.orderNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					customerUrl:function() {
						return '{0}?no={1}'.format(PageUrl.commonCustomerDetail, this.buyerCustomerNo);
					},
					"orderStatusStr":function(){
						return Enum.orderStatus.getValue(this.orderStatus);
					},
					orderStatusClass:function() {
						return Enum.orderStatus.getClass(this.orderStatus);
					},
					payStatusValue:function() {
						return Enum.payStatus.getValue(this.payStatus);
					},
					payStatusClass:function() {
						return Enum.payStatus.getClass(this.payStatus);
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					orderItemName:function() {
						if(this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0) {
							return this.orderProductList[0].productName;
						}
						if(this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0) {
							return this.orderMaterialList[0].materialName;
						}
					},
					showViewWorkFlowButton:function() {
						return this.orderStatus == Enum.orderStatus.num.inReview;
					},
					isK3Order:function() {
						return this.isK3Order == 1;
					},
					isReletOrder:function() {
						return this.isReletOrder == 1;
					},
					trClass:function() {
						var nowTime = new Date();
						var year = nowTime.getFullYear();
						var month = nowTime.getMonth();
						var day = nowTime.getDate();
						var midnight = new Date(year,month,day).getTime();
						return ((midnight > this.expectReturnTime) && (this.orderStatus == Enum.orderStatus.num.confirmReceipt || this.orderStatus == Enum.orderStatus.num.delivered || this.orderStatus == Enum.orderStatus.num.partRestitution)) ? "text-danger" : "";
					},
					workflowCurrentNodeName:function() {
						return this.hasOwnProperty('workflowLink') ? this.workflowLink.workflowCurrentNodeName : "";
					},
					getCurrentVerifyUserName:function() {
						if(this.hasOwnProperty('workflowLink')) {
							if(!(_.isArray(this.workflowLink.workflowVerifyUserGroupList) && this.workflowLink.workflowVerifyUserGroupList.length > 0)) {
								return this.workflowLink.currentVerifyUserName;
							}
							var currentVerifyUserInfo = _.findWhere(this.workflowLink.workflowVerifyUserGroupList, {verifyUser:user.userId}) || {};
							if(currentVerifyUserInfo.hasOwnProperty('verifyUserName')) {
								return  currentVerifyUserInfo.verifyUserName;
							} else {
								return _.pluck(this.workflowLink.workflowVerifyUserGroupList, 'verifyUserName').join('、');
							}
						}
						return "";
					},
					
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		renderRecycle:function() {
			if($('#isRecycleBin').val() == 1) {
				$('#recycleBtn').removeClass("btn-default").addClass("btn-primary")
			}
		}
	};

	window.OrderManage = _.extend(OrderManage, OrderHandleMixin);

})(jQuery);