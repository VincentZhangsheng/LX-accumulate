;(function($){
	
	var EditOrder = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			chooseGroupList:new Array(),
			no:null,
			customerNo:null,
			customerConsignId:null,
			order:new Object(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_order_edit];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			this.$form = $("#editOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.$groupDataListTpl = $('#groupListTpl').html();
			this.$groupDataListListTable = $('#groupListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(self.$form, function() {
				self.edit();
			});

			self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.submit("{0}order/queryOrderByNoNew".format(SitePath.service),{orderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载订单详细信息",response.description || '失败');
				}
			}, null ,"加载订单详细信息", 'listLoading');
		},
		initData:function(data) {
			this.state.order = data;
			this.initState(data);
			this.initFormData(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.initChooseGroupList(data);
			this.renderEachPrice(data);
		},
		initState:function(data) {
			this.state.customerConsignId = data.orderConsignInfo.customerConsignId;
			this.state.customerNo = data.buyerCustomerNo;
			this.state.orderSeller = {
				userId:data.orderSellerId
			}
			this.state.orderSubCompany = {
				subCompanyId:data.orderSubCompanyId
			}
			this.loadRisk({customerNo:this.state.customerNo});
		},
		initFormData:function(data) {
			if(!data) return;

			var self = this;

	  		Rental.ui.renderFormData(self.$form, data);
	  		$('[name=rentStartTime]', self.$form).val(new Date(data.rentStartTime).format('yyyy-MM-dd'));
	  		$('[name=expectDeliveryTime]', self.$form).val(new Date(data.expectDeliveryTime).format('yyyy-MM-dd'));
			
			self.initAddressInfo(data.buyerCustomerNo, function() {
				$('[name=customerConsignInfoId]', self.$form).prop("checked", false);
				$('[name=customerConsignInfoId][value='+ data.orderConsignInfo.customerConsignId +']', self.$form).prop('checked', true);
			});
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = OrderManageUtil.initChooseProductList(data.orderProductList);
			this.renderProductList();
		},	
		initChooseMaterialList:function(data) {
			this.state.chooseMaterialList = OrderManageUtil.initChooseMaterialList(data.orderMaterialList);
			this.renderMaterialList();
		},
		initChooseGroupList:function(data) {
			this.state.chooseGroupList = OrderManageUtil.initChooseGroupList(data.orderJointProductList);
			this.renderGroupProductList();
		},
		initTotalFirstNeedPay:function(totalContainer, totalPrice) {
			if(!!totalPrice == false) {
				totalContainer.addClass('hide');
				return;
			}
			totalContainer.removeClass('hide');
			$(".num", totalContainer).html("￥"+parseFloat(totalPrice).toFixed(2));
		},
		edit:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				if(!self.state.customerNo) {
					bootbox.alert('请选择客户');
					return;
				}

				if(!formData['customerConsignInfoId']) {
					bootbox.alert('请选择收货地址');
					return;	
				}

				// if(!self.state.orderSeller) {
				// 	bootbox.alert('请选择销售员');
				// 	return;
				// }

				// if(!self.state.orderSubCompany) {
				// 	bootbox.alert('请选择订单所属公司');
				// 	return;
				// }

				var rentStartTime = new Date(formData['rentStartTime']).getTime(),
					expectDeliveryTime = new Date(formData['expectDeliveryTime']).getTime();

				if(expectDeliveryTime > rentStartTime) {
					bootbox.alert('交货日期不能大于起租日期');
					return;
				}

				var commitData = {
					orderNo:self.state.no,
					buyerCustomerNo:self.state.customerNo,
					rentStartTime:rentStartTime,
					expectDeliveryTime:expectDeliveryTime,
					payMode:formData['payMode'],
					logisticsAmount:formData['logisticsAmount'],
					// orderSellerId:self.state.orderSeller.userId,
					// orderSubCompanyId:self.state.orderSubCompany.subCompanyId,
					buyerRemark:formData['buyerRemark'],
					customerConsignId:formData['customerConsignInfoId'],
					highTaxRate:formData['highTaxRate'],
					lowTaxRate:formData['lowTaxRate'],
					deliveryMode:formData['deliveryMode'],
					// owner:formData['owner'],
					rentType:formData['rentType'],
					rentTimeLength:formData['rentTimeLength'],
					isPeer:formData['isPeer'],
					deliverySubCompanyId:formData['deliverySubCompanyId']
				}

				// if(self.isTelSeller()) {
				// 	if(!formData['deliverySubCompanyId']) {
				// 		bootbox.alert('请选择订单所属公司');
				// 		return;
				// 	}
				// 	commitData.deliverySubCompanyId = formData['deliverySubCompanyId'];
				// }

				var orderProductList = OrderManageUtil.getSkuList(self.$dataListTable);
				var orderMaterialList = OrderManageUtil.getMaterialList(self.$materialDataListTable);
				var orderJointProductList = OrderManageUtil.getJointProductList(self.state.chooseGroupList,self.$groupDataListListTable);

				if(orderProductList.length == 0 && orderMaterialList.length == 0  && orderJointProductList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(orderProductList.length > 0) {
					commitData.orderProductList = orderProductList.map(function(item) {
						delete item.rowid;
						return item;
					});
				}

				if(orderMaterialList.length > 0) {
					commitData.orderMaterialList = orderMaterialList.map(function(item) {
						delete item.rowid;
						return item;
					});
				}

				if(orderJointProductList.length > 0) {
					commitData.orderJointProductList = orderJointProductList
				}

				Rental.ajax.submit("{0}order/updateNew".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success("编辑订单",response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error("编辑订单",response.description || '失败');
					}
				}, null ,"编辑订单",'dialogLoading');
			} catch(e) {
				Rental.notification.error("编辑订单失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑订单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							self.loadData();
				        }
				    }
				});
			}
		}
	};


	window.EditOrder = _.extend(EditOrder, OrderMixIn);

})(jQuery);