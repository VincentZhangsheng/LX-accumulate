;(function($) {

	OrderManageUtil = {
		chooseProduct:function(chooseProductList, product) {
			if(!chooseProductList) {
				chooseProductList = new Array();
			}	
			if(chooseProductList.length > 0) {
				var currProductIndex = _.findIndex(chooseProductList, {productId:product.productId});
				var currProduct = currProductIndex > -1 ? chooseProductList[currProductIndex] : null;

				if(currProduct) {
					product.chooseProductSkuList.map(function(item) {
						var findIndexPrams = item.hasOwnProperty('isNewProduct') ? {skuId:item.skuId, isNewProduct:item.isNewProduct} : {skuId:item.skuId},
							index = _.findIndex(currProduct.chooseProductSkuList, findIndexPrams); //如果新旧不一样，为两条数据
						if(index > -1) {
							currProduct.chooseProductSkuList[index] = _.extend(currProduct.chooseProductSkuList[index], item);
						} else {
							currProduct.chooseProductSkuList.push(item);
						}
					});
				} else {
					chooseProductList.push(product);
				}
			} else {
				chooseProductList.push(product);
			}
			return chooseProductList;
		},
		chooseProductSupportSome:function(chooseProductList, product) {
			try {

				if(!chooseProductList) {
					chooseProductList = new Array();
				}	

				product.chooseProductSkuList = product.chooseProductSkuList.map(function(item) {
					if(!item.hasOwnProperty('serialNumber')) {
						item.serialNumber = Rental.helper.randomString(8);
					}
					return item;
				});

				if(chooseProductList.length > 0) {
					var currProductIndex = _.findIndex(chooseProductList, {productId:product.productId});
					var currProduct = currProductIndex > -1 ? chooseProductList[currProductIndex] : null;

					if(currProduct) {
						currProduct.chooseProductSkuList = (currProduct.chooseProductSkuList || []).concat(product.chooseProductSkuList);
						chooseProductList[currProductIndex] = currProduct;
					} else {
						chooseProductList.push(product);
					}
				} else {
					chooseProductList.push(product);
				}
				return chooseProductList;

			} catch (e) {
				Rental.notification.error("选择商品失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseProductList;
			}
			
		},
		chooseProductByIsNew:function(chooseProductList, product) {	
			if(!chooseProductList) {
				chooseProductList = new Array();
			}	
			if(chooseProductList.length > 0) {

				var currProductIndex = _.findIndex(chooseProductList, {productId:product.productId});
				var currProduct = currProductIndex > -1 ? chooseProductList[currProductIndex] : null;

				if(currProduct) {
					product.chooseProductSkuList.map(function(item) {
						var index = _.findIndex(currProduct.chooseProductSkuList, {skuId:item.skuId, isNew:item.isNewProduct}); //如果新旧不一样，为两条数据
						if(index > -1) {
							currProduct.chooseProductSkuList[index] = _.extend(currProduct.chooseProductSkuList[index], item);
						} else {
							currProduct.chooseProductSkuList.push(item);
						}
					});
				} else {
					chooseProductList.push(product);
				}
			} else {
				chooseProductList.push(product);
			}
			return chooseProductList;
		},
		getSkuList:function($dataListTable) {
			var self = this;
			var orderProductList = $('tr.skuRow', $dataListTable).map(function() {
				return self.getSkuBySkuRow($(this));
			}).toArray();
			return orderProductList;
		},
		getSkuBySkuRow:function($skuRow) {
			return {
				serialNumber:$('.serialNumber', $skuRow).val(),
				productId:parseInt($('.productId', $skuRow).val()),
				productSkuId:parseInt($('.productSkuId', $skuRow).val()),
				productUnitAmount:$.trim($('.productUnitAmount', $skuRow).val()),
				productCount:$.trim($('.productCount', $skuRow).val()),
				rentType:$('.rentType', $skuRow).val(),
				rentTimeLength:$.trim($('.rentTimeLength', $skuRow).val()),
				insuranceAmount:$.trim($('.insuranceAmount', $skuRow).val()),
				payMode:$('.payMode', $skuRow).val(),
				isNewProduct:parseInt($('.isNewProduct', $skuRow).val()),
				depositAmount:$('.depositAmount', $skuRow).length > 0 ? $('.depositAmount', $skuRow).val():0,
			};
		},
		changeProduct:function(chooseProductList, $skuRow) {
			var sku = this.getSkuBySkuRow($skuRow);
			var productIndex = _.findIndex(chooseProductList, {productId:sku.productId});
			var findPrams = !!sku.serialNumber ? {serialNumber:sku.serialNumber}:{skuId:sku.productSkuId, isNewProduct:sku.isNewProduct};
			var skuIndex = _.findIndex(chooseProductList[productIndex].chooseProductSkuList, findPrams);
			chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(chooseProductList[productIndex].chooseProductSkuList[skuIndex], sku);
			return chooseProductList;
		},
		chooseMaterial:function(chooseMaterialList, material) {
			try {
				if(chooseMaterialList.length > 0) {
					var findIndexPrams = material.hasOwnProperty('isNewMaterial') ? {materialNo:material.materialNo, isNewMaterial:material.isNewMaterial} :  {materialNo:material.materialNo},
						index  = _.findIndex(chooseMaterialList, findIndexPrams);
					if(index > -1) {
						chooseMaterialList[index] = _.extend(chooseMaterialList, material || {});
					} else {
						chooseMaterialList.push(material)
					}
				} else {
					chooseMaterialList.push(material)
				}
				Rental.notification.success("选择配件", '成功');

				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
		},
		chooseMaterialSupportSome:function(chooseMaterialList, material, showNotice) {
			try {
				// if(chooseMaterialList.length > 0) {
				// 	var findIndexPrams = material.hasOwnProperty('isNewMaterial') ? {materialNo:material.materialNo, isNewMaterial:material.isNewMaterial} :  {materialNo:material.materialNo},
				// 		index  = _.findIndex(chooseMaterialList, findIndexPrams);
				// 	if(index > -1) {
				// 		chooseMaterialList[index] = _.extend(chooseMaterialList, material || {});
				// 	} else {
				// 		chooseMaterialList.push(material)
				// 	}
				// } else {
				// 	chooseMaterialList.push(material)
				// }
				material.serialNumber = Rental.helper.randomString(8)
				chooseMaterialList.push(material);
				if(showNotice == true) {
					Rental.notification.success("选择配件", '成功');
				}
				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
		},
		chooseMaterialByIsNew:function(chooseMaterialList, material) {
			try {
				if(chooseMaterialList.length > 0) {
					var index  = _.findIndex(chooseMaterialList, {materialNo:material.materialNo, isNew:material.isNewMaterial});
					if(index > -1) {
						chooseMaterialList[index] = _.extend(chooseMaterialList, material || {});
					} else {
						chooseMaterialList.push(material)
					}
				} else {
					chooseMaterialList.push(material)
				}
				Rental.notification.success("选择配件", '成功');

				return chooseMaterialList;
			} catch (e) {
				Rental.notification.error("选择配件失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseMaterialList;
			}
		},
		batchDelete:function($dataListTable, chooseProductList) {
			var self = this, 
				array = $('[name=checkItem]', $dataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $dataListTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择商品');
				return chooseProductList;
			}

			filterChecked.each(function(i, item) {
				var skuId = $(this).data('skuid'), 
					productId = $(this).val(),
					isNew = $(this).data('isnew'),
					serialNumber = $(this).data('serialnumber');
				chooseProductList = self.deleteSkuFunc(productId, skuId, isNew, chooseProductList, serialNumber);
			});

			return chooseProductList;
		},
		deleteSku:function($deleteButton, chooseProductList) {
			var skuId = $deleteButton.data('skuid'), 
				productId = $deleteButton.data('productid'),
				isNew = $deleteButton.data('isnew'),
				serialNumber = $deleteButton.data('serialnumber');
			return this.deleteSkuFunc(productId, skuId, isNew, chooseProductList, serialNumber);
		},
		deleteSkuFunc:function(productId, skuId, isNew, chooseProductList, serialNumber) {
			var currProductIndex = _.findIndex(chooseProductList, {productId:parseInt(productId)}),
				skuList = chooseProductList[currProductIndex].chooseProductSkuList;
				
			var skuIndex = _.findIndex(skuList, {skuId:parseInt(skuId)});
			if(!!serialNumber) {
				skuIndex = _.findIndex(skuList, {serialNumber:serialNumber});
			} else if(skuList[0].hasOwnProperty('isNew')) {
				skuIndex = _.findIndex(skuList, {skuId:parseInt(skuId), isNew:parseInt(isNew)});
			} else if(skuList[0].hasOwnProperty('isNewProduct')) {
				skuIndex = _.findIndex(skuList, {skuId:parseInt(skuId), isNewProduct:parseInt(isNew)});
			}

			skuList.splice(skuIndex, 1);
			if(skuList.length == 0) {
				chooseProductList.splice(currProductIndex,1);
			} else {
				chooseProductList[currProductIndex].chooseProductSkuList = skuList;
			}
			return chooseProductList;
		},
		batchDeleteMaterial:function($materialDataListTable, chooseMaterialList) {
			var self = this,
				array = $('[name=checkItem]', $materialDataListTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $materialDataListTable).filter(':checked');
			if(filterChecked.length == 0) {
				bootbox.alert('请选择配件');
				return chooseMaterialList;
			}

			filterChecked.each(function(index, el) {
				var materialNo = $(this).val(), 
					isNew = $(this).data('isnew'),
					serialNumber = $(this).data('serialnumber');
				chooseMaterialList = self.deleteMaterial(materialNo, isNew, chooseMaterialList, serialNumber);
			});

			return chooseMaterialList;
		},
		deleteMaterial:function(materialNo, isNew, chooseMaterialList, serialNumber) {
			if(chooseMaterialList.length == 0) return chooseMaterialList;
			var index = _.findIndex(chooseMaterialList, {materialNo:materialNo});
			if(!!serialNumber) {
				index = _.findIndex(chooseMaterialList, {serialNumber:serialNumber})	
			} else if(chooseMaterialList[0].hasOwnProperty('isNew')) {
				index = _.findIndex(chooseMaterialList, {materialNo:materialNo, isNew: parseInt(isNew)})	
			} else if(chooseMaterialList[0].hasOwnProperty('isNewMaterial')){
				index = _.findIndex(chooseMaterialList, {materialNo:materialNo, isNewMaterial: parseInt(isNew)})	
			}
			chooseMaterialList.splice(index, 1);
			return chooseMaterialList;
		},
		getMaterialList:function($materialDataListTable) {
			var self = this;
			var materialList = $('tr.materialRow', $materialDataListTable).map(function() {
				return self.getMaterialByMaterialRow($(this));
			}).toArray();
			return materialList;
		},
		getMaterialByMaterialRow:function($materialRow) {
			return  {
				serialNumber:$('.serialNumber', $materialRow).val(),
				materialNo:$('.materialNo', $materialRow).val(),
				materialId:parseInt($('.materialId', $materialRow).val()),
				materialUnitAmount:$.trim($('.materialUnitAmount', $materialRow).val()),
				materialCount:$.trim($('.materialCount', $materialRow).val()),
				productCount:$.trim($('.productCount', $materialRow).val()),
				rentType:$('.rentType', $materialRow).val(),
				rentTimeLength:$.trim($('.rentTimeLength', $materialRow).val()),
				productUnitAmount:$.trim($('.productUnitAmount', $materialRow).val()),
				insuranceAmount:$.trim($('.insuranceAmount', $materialRow).val()),
				payMode:$('.payMode', $materialRow).val(),
				isNewMaterial:parseInt($('.isNewMaterial', $materialRow).val()),
				depositAmount:$('.depositAmount', $materialRow).length > 0 ? $('.depositAmount', $materialRow).val():0,
			};
		},
		changeMaterial:function(chooseMaterialList, $materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var findPrams = !!material.serialNumber ? {serialNumber:material.serialNumber} : {materialNo:material.materialNo, isNewMaterial:material.isNewMaterial};
			var index = _.findIndex(chooseMaterialList, findPrams);
			chooseMaterialList[index] = _.extend(chooseMaterialList[index], material);
			return chooseMaterialList;
		},
		initChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('productSkuSnapshot') ? JSON.parse(item.productSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0] || {}, item)
					} 
					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					item.serialNumber = Rental.helper.randomString(8);
					pre[item.productId].chooseProductSkuList.push(item);
					pre[item.productId] = _.extend(pre[item.productId], product);
			
					return pre;
				},{});

				chooseProductList = _.values(orderItemsMapByProductId);
			}
			return chooseProductList;
		},
		initChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.materialSnapshot);
					material.serialNumber = Rental.helper.randomString(8);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
		initChooseGroupList:function(data) {
			var chooseGroupList = new Array();
			if(data) {
				chooseGroupList = data;
				chooseGroupList = chooseGroupList.map(function(item) {
					if(!item.hasOwnProperty('serialNumber')) {
						item.serialNumber = Rental.helper.randomString(8);
					}

					item.chooseGroupProductList = item.orderProductList.map(function(productItem) {
						var product = productItem.hasOwnProperty("productSkuSnapshot") ? JSON.parse(productItem.productSkuSnapshot) : null;

						productItem.chooseProductSkuList = product.productSkuList;
						productItem.categoryName = product.categoryName
						productItem.brandName = product.brandName
						productItem.productName = product.productName
						productItem.chooseProductSkuList.forEach(function(skuItem) {
							skuItem.isNewProduct = productItem.isNewProduct
						}) 

						if(!productItem.hasOwnProperty('serialNumber')) {
							productItem.serialNumber = Rental.helper.randomString(8);
						}

						return productItem;
					});

					item.chooseMaterialList = item.orderMaterialList.map(function(materialItem) {
						var material = materialItem.hasOwnProperty("materialSnapshot") ? JSON.parse(materialItem.materialSnapshot) : null;

						materialItem.material = material;
						materialItem.jointProductId = item.jointProductId;
						materialItem.materialName = material.materialName;

						if(!materialItem.hasOwnProperty('serialNumber')) {
							materialItem.serialNumber = Rental.helper.randomString(8);
						}
						return materialItem;
					});
					item.tabIndex = 0;
					return item;
				})
			}
			return chooseGroupList;
		},
		chooseGroupProduct:function(chooseGroupProductList, groupProduct) {
			try {
				if(!chooseGroupProductList) {
					chooseGroupProductList = new Array();
				}	

				if(!groupProduct.hasOwnProperty('serialNumber')) {
					groupProduct.serialNumber = Rental.helper.randomString(8);
				}

				groupProduct.chooseGroupProductList = groupProduct.chooseGroupProductList.map(function(item) {
					if(!item.hasOwnProperty('serialNumber')) {
						item.serialNumber = Rental.helper.randomString(8);
					}
					return item;
				});

				if(chooseGroupProductList.length > 0) {
					var currProductIndex = _.findIndex(chooseGroupProductList, {jointProductId:groupProduct.jointProductId,isNew:groupProduct.isNew});
					var currProduct = currProductIndex > -1 ? chooseGroupProductList[currProductIndex] : null;

					if(currProduct) {
						return chooseGroupProductList;
					} else {
						chooseGroupProductList.push(groupProduct);
					}
				} else {
					chooseGroupProductList.push(groupProduct);
				}
				return chooseGroupProductList;

			} catch (e) {
				Rental.notification.error("选择组合商品失败", Rental.lang.commonJsError +  '<br />' + e );
				return chooseGroupProductList;
			}
		},
		getGroupList:function($groupTable) {
			var self = this;
			var orderGroupProductList = $('tr.groupRow', $groupTable).map(function() {
				return self.getGroupRow($(this));
			}).toArray();
			return orderGroupProductList;
		},
		getGroupRow:function($groupRow) {
			return {
				isNew:parseInt($('.isNew', $groupRow).val()),
				jointProductId:parseInt($('.jointProductId', $groupRow).val()),
				jointProductCount:$('.jointProductCount', $groupRow).val()
			}
		},
		changeGroupProduct:function(chooseGroupList, $groupRow) {
			var groupRow = this.getGroupRow($groupRow);
			var groupIndex = _.findIndex(chooseGroupList, {jointProductId:groupRow.jointProductId,isNew:groupRow.isNew});
			chooseGroupList[groupIndex] = _.extend(chooseGroupList[groupIndex], groupRow);

			chooseGroupList[groupIndex].chooseGroupProductList.forEach(function(item) {
				var proIndex = _.findIndex(chooseGroupList[groupIndex].jointProductProductList, {productId:parseInt(item.productId)});
				if(!!groupRow.jointProductCount) {
					var nowCount = parseInt(groupRow.jointProductCount) * chooseGroupList[groupIndex].jointProductProductList[proIndex].productCount;
					item.productCount = nowCount;
				} else {
					item.productCount = chooseGroupList[groupIndex].jointProductProductList[proIndex].productCount;
				}
			})

			chooseGroupList[groupIndex].chooseMaterialList.forEach(function(item) {
				var matIndex = _.findIndex(chooseGroupList[groupIndex].jointMaterialList, {materialId:item.materialId});
				if(!!groupRow.jointProductCount) {
					var nowCount = parseInt(groupRow.jointProductCount) * chooseGroupList[groupIndex].jointMaterialList[matIndex].materialCount;
					item.materialCount = nowCount;
				} else {
					item.materialCount = chooseGroupList[groupIndex].jointMaterialList[matIndex].materialCount;
				}
			})
			return chooseGroupList;
		},
		batchDeleteGroup:function($groupTable, chooseGroupProductList) {
			var self = this, 
				array = $('[name=checkItem]', $groupTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', $groupTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择组合商品');
				return chooseGroupProductList;
			}

			filterChecked.each(function(i, item) {
				var jointProductId = parseInt($(this).val());
				var isNew = parseInt($(this).data("isnew"));
				chooseGroupProductList = self.deleteGroupFunc(jointProductId, isNew, chooseGroupProductList);
			});

			return chooseGroupProductList;
		},
		deleteGroupProduct:function($deleteButton, chooseGroupProductList) {
			var jointProductId = parseInt($deleteButton.data("id"));
			var isNew = parseInt($deleteButton.data("isnew"));
			return this.deleteGroupFunc(jointProductId, isNew, chooseGroupProductList);
		},
		deleteGroupFunc:function(jointProductId, isNew, chooseGroupProductList) {
			var curIndex = _.findIndex(chooseGroupProductList, {jointProductId:jointProductId,isNew:isNew});
			if(chooseGroupProductList.length > 0) {
				chooseGroupProductList.splice(curIndex,1);
			} 
			return chooseGroupProductList;
		},
		changeGroupProductItem:function(chooseGroupList,$groupProductRow) {
			var groupProductSku = this.getSkuBySkuRow($groupProductRow);
			var jointid = $groupProductRow.data("jointid");
			var groupIndex = _.findIndex(chooseGroupList,{jointProductId:jointid,isNew:parseInt(groupProductSku.isNewProduct)});
			var productIndex = _.findIndex(chooseGroupList[groupIndex].chooseGroupProductList,{productId:groupProductSku.productId})
			chooseGroupList[groupIndex].chooseGroupProductList[productIndex] = _.extend(chooseGroupList[groupIndex].chooseGroupProductList[productIndex],{
				productUnitAmount:groupProductSku.productUnitAmount,
				payMode:groupProductSku.payMode,
				depositAmount:groupProductSku.depositAmount
			})
			return chooseGroupList;
		},
		changeGroupMaterial:function(chooseGroupList,$materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var rowData = $materialRow.data("rowdata");
			var groupIndex = _.findIndex(chooseGroupList,{jointProductId:rowData.jointProductId,isNew:rowData.isNewMaterial});
			var matIndex = _.findIndex(chooseGroupList[groupIndex].chooseMaterialList,{materialId:material.materialId});
			
			chooseGroupList[groupIndex].chooseMaterialList[matIndex] = _.extend(chooseGroupList[groupIndex].chooseMaterialList[matIndex],{
				materialUnitAmount: material.materialUnitAmount,
				payMode:material.payMode,
				depositAmount:material.depositAmount,
				materialCount: material.materialCount,
			})
			return chooseGroupList;
		},
		deleteGroupMaterial:function(chooseGroupList,$materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var rowData = $materialRow.data("rowdata");
			var groupIndex = _.findIndex(chooseGroupList,{jointProductId:rowData.jointProductId,isNew:rowData.isNewMaterial});
			var matIndex = _.findIndex(chooseGroupList[groupIndex].chooseMaterialList,{materialId:material.materialId});
			chooseGroupList[groupIndex].chooseMaterialList.splice(matIndex,1)
			return chooseGroupList;
		},
		getJointProductList:function(chooseGroupList, $dataTable) {
			var self = this;
			var orderJointProductList = self.getGroupList($dataTable);
			chooseGroupList.forEach(function(item) {
				var orderProductList = item.chooseGroupProductList.map(function(product) {
					return {
						productId:product.productId,
						jointProductProductId:product.jointProductProductId,
						productSkuId:product.chooseProductSkuList[0].skuId,
						productCount:product.productCount,
						payMode:product.payMode ? product.payMode : "",
						insuranceAmount:"",
						productUnitAmount:product.productUnitAmount,
						depositAmount:product.depositAmount,
						isNewProduct:product.chooseProductSkuList[0].isNewProduct
					}
				})

				var orderMaterialList = item.chooseMaterialList.map(function(material) {
					return {
						materialId:material.materialId,
						jointMaterialId:material.jointMaterialId,
						materialCount:material.materialCount,
						payMode:material.payMode ? material.payMode : "",
						insuranceAmount:"",
						materialUnitAmount:material.materialUnitAmount,
						depositAmount:material.depositAmount,
						isNewMaterial:material.isNewMaterial
					}
				})
				var groupIndex = _.findIndex(orderJointProductList,{jointProductId:item.jointProductId,isNew:item.isNew})
				orderJointProductList[groupIndex].orderProductList = orderProductList;
				orderJointProductList[groupIndex].orderMaterialList = orderMaterialList;
			})
			return orderJointProductList;
		},
		getGroupCommitData:function(chooseGroupList, $dataTable) {
			var self = this;
			var orderJointProductList = self.getGroupList($dataTable);

			chooseGroupList.forEach(function(item) {
				var orderProductList = item.chooseGroupProductList.map(function(product) {
					return {
						productId:product.productId,
						productSkuId:product.chooseProductSkuList[0].skuId,
						productCount:product.productCount,
						payMode:product.payMode ? product.payMode : "",
						productUnitAmount:product.productUnitAmount,
						depositAmount:product.depositAmount,
						isNewProduct:product.chooseProductSkuList[0].isNewProduct,
						serialNumber:product.serialNumber
					}
				})

				var orderMaterialList = item.chooseMaterialList.map(function(material) {
					return {
						materialId:material.materialId,
						materialCount:material.materialCount,
						payMode:material.payMode ? material.payMode : "",
						materialUnitAmount:material.materialUnitAmount,
						depositAmount:material.depositAmount,
						isNewMaterial:material.isNewMaterial,
						serialNumber:material.serialNumber
					}
				})
				var groupIndex = _.findIndex(orderJointProductList,{jointProductId:item.jointProductId,isNew:item.isNew})
				orderJointProductList[groupIndex].orderProductList = orderProductList;
				orderJointProductList[groupIndex].orderMaterialList = orderMaterialList;
			})
			return orderJointProductList;
		},
	}

	window.OrderManageUtil = OrderManageUtil;

})(jQuery);