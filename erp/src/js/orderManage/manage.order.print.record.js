/**
* 客户
* 1. 销售订单
* 2. 退货单
*/
;(function($) {

	orderPrintRecord = {
		init:function(prams) {
			this.props = _.extend({
                referNo:null,
                referType:null
			}, prams);
            this.initDom();
			this.initEvent();
        },
        initDom:function() {
            this.$container = $("#orderPrintList")
            this.Pager = new Pager();
        },
		initEvent:function() {
            this.renderPrintRecord({})
            this.searchPrintRecord();
        },
        getPrintRecord: function(prams) {
            var self = this;
            var searchData = $.extend({
                pageNo:1,
                pageSize:self.Pager.defautPrams.pageSize,
                referNo:this.props.referNo,
                referType:this.props.referType,
            }, prams || {});
            Rental.ajax.ajaxDataNoLoading('print/pagePrintLog', searchData, '加载打印记录', function(response) {
                self.renderPrintRecord(response.resultMap.data);
                self.renderPrintCount(response.resultMap.data);
            });
        },
        renderPrintRecord: function(data) {
        	var self = this;
        	self.renderPrintRecordList(data);
        	self.Pager.init(data,function(pageNo) {
        		self.searchPrintRecord(pageNo)
        	}, $('.pagerContainer', self.$container));
        },
        searchPrintRecord: function(pageNo) {
        	this.getPrintRecord({pageNo: pageNo || 1})
        },
        renderPrintRecordList: function(data) {
            var self = this;
            var printRecordTpl = $("#printRecordTpl").html();
            var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

            var data = {
                dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    referTypeVal:function() {
                        return Enum.referType.getValue(this.referType)
                    }
                })
            }
            Mustache.parse(printRecordTpl);
            $("#printRecordTable").html(Mustache.render(printRecordTpl, data));
        },
        renderPrintCount:function(data) {
            var totalCount = data.hasOwnProperty("totalCount") ? data.totalCount : 0;
            $("#printRecordCount").html(totalCount);
        }	
	}

})(jQuery);