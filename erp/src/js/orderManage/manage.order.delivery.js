/**
* 待发货列表
*/
;(function($) {

	var OrderManageWaiteForDelivery = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
			this.currentPageAuthorForDelivery = this.currentManageAuthor.children[AuthorCode.manage_order_watie_for_delivery_list]; 
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_order_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_order_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_order_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_order_submit],
				// pay = this.currentPageAuthor.children[AuthorCode.manage_order_pay],
				confirmReceipt = this.currentPageAuthor.children[AuthorCode.manage_order_confirm_receipt],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_order_cancel],
				picking = this.currentPageAuthor.children[AuthorCode.manage_order_picking],
				deliverGoods = this.currentPageAuthor.children[AuthorCode.manage_order_deliver_goods],
				print = this.currentPageAuthor.children[AuthorCode.manage_order_print];

			// add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));

			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			// pay && this.rowActionButtons.push(AuthorUtil.button(pay,'payButton','','模拟支付'));
			picking && this.rowActionButtons.push(AuthorUtil.button(view,'pickingButton','','配货')); //列表页面配货跳转至详细页面配货
			deliverGoods && this.rowActionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			confirmReceipt && this.rowActionButtons.push(AuthorUtil.button(confirmReceipt,'confirmReceiptButton','','确认收货'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
			print && this.rowActionButtons.push(AuthorUtil.button(print,'printButton','','打印'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.orderWaiteForDelivery);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthorForDelivery]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			self.searchData();  //初始化列表数据

			self.renderCommonActionButton();

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				// orderStatus:Enum.orderStatus.num.waitForDelivery,
				isPendingDelivery:1,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.orderWaiteForDelivery, searchData);

			Rental.ajax.ajaxData('order/queryAllOrder', searchData, '加载订单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"orderStatusStr":function(){
						return Enum.orderStatus.getValue(this.orderStatus);
					},
					orderStatusClass:function() {
						return Enum.orderStatus.getClass(this.orderStatus);
					},
					payStatusValue:function() {
						return Enum.payStatus.getValue(this.payStatus);
					},
					payStatusClass:function() {
						return Enum.payStatus.getClass(this.payStatus);
					},
					"rentTypeStr":function() {
						return Enum.rentType.getValue(this.rentType);
					},
					orderItemName:function() {
						if(this.hasOwnProperty('orderProductList') && this.orderProductList.length > 0) {
							return this.orderProductList[0].productName;
						}
						if(this.hasOwnProperty('orderMaterialList') && this.orderMaterialList.length > 0) {
							return this.orderMaterialList[0].materialName;
						}
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.OrderManageWaiteForDelivery = _.extend(OrderManageWaiteForDelivery, OrderHandleMixin);

})(jQuery);