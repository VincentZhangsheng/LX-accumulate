;(function($) {

	var OrderHandleMixin = {
		initHandleEvent:function(container, prams, handleCallBack) {
			var self = this;
			prams = _.extend({
				isOrderList:true,
				isReletDetail:false,
			}, prams || {});

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.isNeedVerify(orderNo, handleCallBack);
			})
			
			// container.on('click', '.submitButton', function(event) {
			// 	event.preventDefault();
			// 	ApiData.isNeedVerify({
			// 		no:$(this).data('orderno'),
			// 		workflowType:Enum.workflowType.num.order,
			// 	}, function(result, isAudit) {
			// 		self.submitOrder(result, handleCallBack, isAudit);
			// 	})
			// });

			container.on('click', '.deliverGoodsButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.deliverGoods(orderNo, handleCallBack);
			});

			container.on('click', '.confirmReceiptButtonNew', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				var detUrl = $(this).prop('href');
				ReceiptConfirm.modal({
					orderNo:orderNo,
					receiptType:1,
					isOrderList:prams.isOrderList,
					detUrl:detUrl,
					callBack: handleCallBack ||function() {}
				});
			});

			container.on('click', '.confirmReceiptButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				var detUrl = $(this).prop('href');
				self.confirmReceipt(orderNo, prams.isOrderList, detUrl, function(){
					handleCallBack && handleCallBack();
				});
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.cancel(orderNo, handleCallBack);
			});

			container.on('click', '.strongCancel', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.strongCancel(orderNo, handleCallBack);
			});

			// 添加备注
			container.on('click', '.addRemark', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.addRemarkFunc(orderNo, handleCallBack);
			});

			// 续租
			container.on('click', '.reletButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				var canRelet = $(this).data('canrelet');
				var isShortOrder = true;
				self.reletOrder(orderNo, canRelet, isShortOrder, handleCallBack)
			});

			container.on('click', '.submitRelet', function(event) {
				event.preventDefault();
				var reletOrderNo = $(this).data('no');
				self.reletIsNeedVerify(reletOrderNo, handleCallBack)
			});

			//续租单重算
			container.on('click', '.recalRelet', function(event) {
				event.preventDefault();
				var reletOrderNo = $(this).data('no');
				self.recalculationReletOrder(reletOrderNo, handleCallBack)
			});

			container.on('click', '.cancelRelet', function(event) {
				event.preventDefault();
				var reletOrderNo = $(this).data('no');
				self.cancelReletOrder(reletOrderNo, prams.isReletDetail, handleCallBack);
			})

			container.on('click', '.editRelet', function(event) {
				event.preventDefault();
				var reletOrderNo = $(this).data('no');
				self.editReletOrder(reletOrderNo, handleCallBack);
			})

			// 重算
			container.on('click', '.recalButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.recalculationOrder(orderNo, handleCallBack);
			});

			// 拆单
			container.on('click', '.splitButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				OrderSplit.init({})
			});

			container.on('click', '.pickingProductButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.pickingProduct({orderNo:orderNo, operationType:Enum.operationType.num.stock, title:'配货'}, handleCallBack);
			});

			container.on('click', '.removePickingProductButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.pickingProduct({orderNo:orderNo, operationType:Enum.operationType.num.remove, title:'清货'}, handleCallBack);
			});

			container.on('click', '.pickingMaterialButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno'), 
					materialId = $(this).data('materialid'), 
					materialCount = $(this).data('materialcount'),
					isNewMaterial = $(this).data('isnewmaterial');
				self.pickingMaterial({
					orderNo:orderNo, 
					materialId:materialId, 
					materialCount:materialCount, 
					operationType:Enum.operationType.num.stock, 
					title:'配货',
					isNewMaterial:isNewMaterial,
				}, handleCallBack);
			});

			container.on('click', '.removePickingMaterialButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno'), 
					materialId = $(this).data('materialid'), 
					materialCount = $(this).data('materialcount'),
					isNewMaterial = $(this).data('isnewmaterial');
				self.pickingMaterial({
					orderNo:orderNo, 
					materialId:materialId, 
					materialCount:materialCount, 
					operationType:Enum.operationType.num.remove, 
					title:'清货',
					isNewMaterial:isNewMaterial,
				}, handleCallBack);
			});

			container.on('click', '.viewAffix', function(event) {
				event.preventDefault();
				$(this).closest('tr').next('.affixTr').find('.affixPanel').slideToggle('fast');
			});

			container.on('click', '.hideAffixPanel', function(event) {
				event.preventDefault();
				$(this).closest('.affixPanel').slideUp('fast');
			});

			container.on('click', '.payButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.pay(orderNo, handleCallBack);
			});

			container.on('click', '.viewWorkFlowButton', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				ViewWorkFlow.init({
					workflowType:Enum.workflowType.num.order,
					workflowReferNo:orderNo,
				});
			});

			//修改结算日
			container.on('click', '.changeSettlement', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				var statementDate = $(this).data('statement');
				self.changeSettlement(orderNo, statementDate, handleCallBack);
			});	

			//分段结算
			container.on('click', '.sectionSettlement', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.sectionSettlement(orderNo, handleCallBack);
			});	

			//修改商品项单价
			container.on('click', '.changePrice', function(event) {
				event.preventDefault();
				var orderNo = $(this).data('orderno');
				self.changeOrderItemPrice(orderNo, handleCallBack);
			});	
		},
		orderDo:function(url, commitData, des, callBack) {
			if(!commitData.orderNo) {
				bootbox.alert('找不到订单编号');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url),commitData,function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
		},
		deliverGoods:function(orderNo, callBack) {
			var self = this;
			bootbox.confirm('确认发货？', function(result) {
				result && self.orderDo('order/delivery', {orderNo:orderNo}, '订单发货', callBack);
			});
		},
		confirmReceipt:function(orderNo, isOrderList, detUrl, callBack) {
			var self = this;
			bootbox.confirm('确认收货？', function(result) {
				result && self.receiptDo(orderNo, isOrderList, detUrl, callBack);
			});
		},
		receiptDo:function(orderNo, isOrderList, detUrl, callBack) {
			if(!orderNo) {
				bootbox.alert('找不到订单编号');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'order/confirm'),{orderNo:orderNo},function(response){
				if(response.success) {
					self.receiptSuccess(orderNo, isOrderList, detUrl)
					callBack && callBack()
				} else {
					Rental.notification.error('确认收货',response.description || '失败');
				}
			}, null ,'确认收货','dialogLoading');
		},
		receiptSuccess:function(orderNo, isOrderList, detUrl) {
			var buttons = null;
			if(isOrderList) {
				buttons = {
					cancel: {
						label: '知道了',
						className: 'text-primary'
					},
					confirm: {
						label: '查看订单详情',
						className: 'btn-primary',
						callback: function () {
							location.href = detUrl;
						}
					}
				}
			} else {
				buttons = {
					cancel: {
						label: '知道了',
						className: 'text-primary'
					}
				}
			}
			bootbox.dialog({
			    message: "订单 "+ orderNo +" 确认收货成功",
			    buttons: buttons,
			});
		},
		cancel:function(orderNo, callBack) {
			var self = this;
			self.cancelReasonFunc(orderNo, 'order/cancel', '取消订单', callBack);
		},
		strongCancel:function(orderNo, callBack) {
			var self = this;
			self.cancelReasonFunc(orderNo, 'order/forceCancel', '强制取消订单', callBack);
		},
		cancelReasonFunc:function(orderNo, url, des, callBack) {
			var self = this;
			InputModal.init({
				url:'cancel-reason/modal',
				initFunc:function(modal) {
					var cancelReasonInput = $('[name=cancelOrderReasonType]', modal);
					Rental.ui.renderSelect({
						container:cancelReasonInput,
						data:Enum.array(Enum.cancelReason),
						func:function(opt, item) {
							return opt.format(item.num, item.value);
						},
						defaultText:'请选择取消原因'
					});
				},
				callBack:function(result) {
					self.orderDo(url, {orderNo:orderNo,cancelOrderReasonType:result.cancelOrderReasonType}, des, callBack);
					Rental.modal.close();
				}
			})
		},
		//添加备注
		addRemarkFunc:function(orderNo, callBack) {
			var self = this;
			self.addRemark.init({
				orderNo:orderNo,
				callBack:function(result) {
					Rental.modal.close();
					callBack && callBack();
				}
			})
		},
		//续租
		reletOrder:function(orderNo, canRelet, isShortOrder, callBack) {
			var self = this;
			if(canRelet == 2) {
				bootbox.alert('存在待处理的续租单，请先处理');
				return;
			}
			if(canRelet == 3) {
				bootbox.alert('已存在续租单,待执行后方可再次续租');
				return;
			}
			ReletOrderAdd.modal({
				orderNo:orderNo,
				callBack: callBack ||function() {},
				complete:function() {
					Rental.modal.close();
				},
				continueFunc:function(reletOrderNo) {
					self.reletIsNeedVerify(reletOrderNo, callBack);
				}
			});
			// if(isShortOrder) {
			// 	self.shortRentalRelet.init({
			// 		orderNo:orderNo,
			// 		callBack:function(product) {},
			// 		complete:function() {
			// 			Rental.modal.close();
			// 		},
			// 		changeContinueFunc:null,
			// 		continueFunc:function() {
			// 			ReletOrderAdd.modal({
			// 				orderNo:orderNo,
			// 				callBack: callBack ||function() {},
			// 				complete:function() {
			// 					Rental.modal.close();
			// 				},
			// 				continueFunc:function(reletOrderNo) {
			// 					self.reletIsNeedVerify(reletOrderNo, callBack);
			// 				}
			// 			});
			// 		},
			// 	})
			// } else {
			// 	ReletOrderAdd.modal({
			// 		orderNo:orderNo,
			// 		callBack: callBack ||function() {},
			// 		complete:function() {
			// 			Rental.modal.close();
			// 		},
			// 		continueFunc:function(reletOrderNo) {
			// 			self.reletIsNeedVerify(reletOrderNo, callBack);
			// 		}
			// 	});
			// }
		},
		editReletOrder:function(reletOrderNo, callBack) {
			var self = this;
			ReletOrderEdit.modal({
				reletOrderNo:reletOrderNo,
				callBack: callBack ||function() {},
				complete:function() {
					Rental.modal.close();
				},
				continueFunc:function(reletOrderNo) {
					self.reletIsNeedVerify(reletOrderNo, callBack);
				}
			});
		},
		reletIsNeedVerify:function(reletOrderNo, callBack) {
			if(!reletOrderNo) {
				bootbox.alert('找不到续租单编号');
				return;
			}
			var self = this;
			Rental.ajax.submit("{0}reletOrder/isNeedVerify".format(SitePath.service),{reletOrderNo:reletOrderNo},function(response){
				if(response.success) {
					if(response.resultMap.data) {
						self.reletSubmitAudit({reletOrderNo:reletOrderNo}, callBack);
					} else {
						self.submitReletOrder({reletOrderNo:reletOrderNo}, callBack, false);
					}
				} else {
					Rental.notification.error('请求提交',response.description || '失败');
				}
			}, null ,'请求提交');
		},
		reletSubmitAudit:function(prams, callBack) {
			var self = this;
			SubmitAudit.init({
				workflowType:Enum.workflowType.num.order,
				workflowReferNo:prams.reletOrderNo,
				callBack:function(res) {
					var commitData = {
						reletOrderNo:prams.reletOrderNo,
						verifyUser:res.verifyUser,
						commitRemark:res.commitRemark,
						imgIdList:res.imgIdList,
					}
					self.submitReletOrder(commitData, callBack, true);
				}
			})
		},
		submitReletOrder:function(prams, callBack, isAudit) {
			var self = this;
			if(isAudit) {
				self.submitReletOrderFunc({
					reletOrderNo:prams.reletOrderNo,
					verifyUser:prams.verifyUser,
					commitRemark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, function() {
					Rental.modal.close();
					callBack && callBack();
				});
				return;
			} else {
				bootbox.confirm('确认提交续租单？', function(result) {
					result && self.submitReletOrderFunc({
						reletOrderNo:prams.reletOrderNo,
					}, callBack);
				});
			}
		},
		submitReletOrderFunc:function(prams, callBack) { 
			var  des = '提交续租单';
			Rental.ajax.submit("{0}reletOrder/commit".format(SitePath.service), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des, response.description || '成功');
					callBack && callBack();
					Rental.modal.close();
				} else {
					Rental.notification.error(des, response.description || '失败');
				}
			}, null, des);
		},
		cancelReletOrder:function(reletOrderNo, isReletDetail, callBack) {
			var self = this;
			bootbox.confirm('确认取消续租？', function(result) {
				result && self.cancelReletOrderDo(reletOrderNo, isReletDetail, callBack);
			});
		},
		cancelReletOrderDo:function(reletOrderNo, isReletDetail, callBack) {
			if(!reletOrderNo) {
				bootbox.alert('找不到续租单编号');
				return;
			}
			var self = this;
			Rental.ajax.submit("{0}reletOrder/cancelReletOrderByNo".format(SitePath.service),{reletOrderNo:reletOrderNo},function(response){
				if(response.success) {
					Rental.notification.success("取消续租", response.description || '成功');
					if(isReletDetail) {
						window.history.go(-1);
					} else {
						callBack && callBack();
					}
				} else {
					Rental.notification.error('取消续租',response.description || '失败');
				}
			}, null ,'取消续租');
		},
		recalculationReletOrder:function(reletOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认重算？', function(result) {
				result && self.recalReletOrderDo(reletOrderNo, callBack);
			});
		},
		recalReletOrderDo:function(reletOrderNo, callBack) {
			var self = this; 
			if(!reletOrderNo) {
				bootbox.alert('找不到续租单编号');
				return;
			}
			
			var des = "确认重算续租单"
			Rental.ajax.submit("{0}statementOrder/reCreateReletOrderStatement".format(SitePath.service),{reletOrderNo:reletOrderNo},function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
		},
		filterReletButtons:function(buttons, relet) {
			var reletButtons = new Array(),
				toSubmit = relet.reletOrderStatus == Enum.reletOrderStatus.num.toSubmit;
		
			(buttons || []).forEach(function(button, index) {
				button.reletOrderNo = relet.reletOrderNo;
				reletButtons.push(button);

				if((button.class == 'submitRelet' || button.class == 'editRelet' || button.class == 'cancelRelet') && !toSubmit) {
					reletButtons.splice(reletButtons.length - 1,1);
				}
			});
			return reletButtons;
		},

		//重算
		recalculationOrder:function(orderNo, callBack) {
			var self = this;
			bootbox.confirm('确认重算？', function(result) {
				result && self.orderDo('statementOrder/reCreateOrderStatement', {orderNo:orderNo}, '确认重算', callBack);
			});
		},

		//修改结算日
		changeSettlement:function(orderNo, statementDate, callBack) {
			var self = this;
			InputModal.init({
				title:'修改结算时间',
				url:'customer/set-settlement-date-modal',
				initFunc:function(modal) {
					var statementdateInput = $('[name=statementDate]', modal);
					Rental.ui.renderSelect({
						container:statementdateInput,
						data:Enum.array(Enum.settlementDate),
						func:function(opt, item) {
							return opt.format(item.num, item.value);
						},
						defaultText:'请选择'
					});
					statementdateInput.val(statementDate);
				},
				callBack:function(result) {
					if(!result.statementDate) {
						bootbox.alert('请选择结算日');
						return;
					}
					self.orderDo('statementOrder/reCreateOrderStatement', {orderNo:orderNo,statementDate:result.statementDate }, '修改结算时间', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			});
		},

		//分段结算
		sectionSettlement:function(orderNo, callBack) {
			var self = this;
			Rental.ajax.ajaxDataNoLoading('order/queryOrderByNo', {orderNo:orderNo}, '加载订单详细信息', function(response) {
				InputModal.init({
					title:'分段结算',
					url:'order-manage/section-settlement',
					initFunc:function(modal) {
						Rental.ui.renderSelect({
							container:$("#changeType"),
							data:Enum.array(Enum.sectionSettlementType),
							func:function(opt, item) {
								return opt.format(item.num, item.value);
							},
							defaultText:'请选择截止类型'
						});
	
						$(".settlementSelect").each(function() {
							Rental.ui.renderSelect({
								container:$(this),
								data:Enum.array(Enum.settlementDate),
								func:function(opt, item) {
									return opt.format(item.num.toString(), item.value.toString());
								},
								defaultText:'请选择结算日'
							});
						})
						
						self.initDatePicker();
					},
					callBack:function(result) {
						if(!result.afterStatementDate || !result.beforeStatementDate || !result.statementDateChangeTime || !result.changeType) {
							bootbox.alert('请完善分段结算信息');
							return;
						}
	
						var commitData = {
							orderNo:orderNo,
							afterStatementDate:result.afterStatementDate,
							beforeStatementDate:result.beforeStatementDate,
							statementDateChangeTime:result.statementDateChangeTime,
							changeType:result.changeType,
						}
	
						self.orderDo('statementOrder/reCreateOrderStatementSplit', commitData, '分段结算', function() {
							callBack && callBack();
							Rental.modal.close();
						});	
					}
				});
			});
		},
		//修改商品项单价
		changeOrderItemPrice:function(orderNo, callBack) {
			var self = this;
			ChangeUnitPrice.modal({
				orderNo:orderNo,
				callBack: callBack ||function() {},
				complete:function() {
					Rental.modal.close();
				},
			});
		},
		initDatePicker:function() {
			var self = this , defaultStart = moment().subtract('days', 29), defaultEnd = moment();
			$('#changeTimePicker').daterangepicker({
			    "singleDatePicker": true,
			    "showDropdowns": true,
			    "showWeekNumbers": true,
			    "startDate": defaultEnd,
			    "endDate": defaultEnd
			}, function(start, end, label) {
				$("#changeTimeInput").val(start.format('YYYY-MM-DD'));
				$("#statementDateChangeTime").val(new Date(start).getTime())
			});
		},

		//目前是模拟支付，只是改变订单状态
		pay:function(orderNo, callBack) {
			var self = this;
			bootbox.confirm('确认支付订单？', function(result) {
				result && self.orderDo('order/pay', {orderNo:orderNo}, '支付订单', callBack);
			});
		},
		isNeedVerify:function(orderNo, callBack) {
			if(!orderNo) {
				bootbox.alert('找不到订单编号');
				return;
			}
			var self = this;
			Rental.ajax.submit("{0}order/isNeedVerify".format(SitePath.service),{orderNo:orderNo},function(response){
				if(response.success) {
					if(response.resultMap.data) {
						self.submittoAudit({orderNo:orderNo}, callBack);
					} else {
						self.submitOrder({orderNo:orderNo}, callBack, false);
					}
				} else {
					Rental.notification.error('请求提交',response.description || '失败');
				}
			}, null ,'请求提交', 'dialogLoading');
		},
		submittoAudit:function(prams, callBack) {
			var self = this;
			SubmitAudit.init({
				workflowType:Enum.workflowType.num.order,
				workflowReferNo:prams.orderNo,
				callBack:function(res) {
					var commitData = {
						orderNo:prams.orderNo,
						verifyUser:res.verifyUser,
						commitRemark:res.commitRemark,
						imgIdList:res.imgIdList,
					}
					self.submitOrder(commitData, callBack, true);
				}
			})
		},
		// submitOrder:function(prams, callBack) {
		// 	var self = this;
		// 	Rental.ajax.submit("{0}order/commit".format(SitePath.service),prams,function(response) {
		// 		if(response.success) {
		// 			Rental.notification.success('请求提交',response.description || '成功');
		// 			// self.doSearch(self.Pager.pagerData.currentPage);
		// 			callBack && callBack();
		// 			Rental.modal.close();
		// 		} else {
		// 			Rental.notification.error('请求提交',response.description || '失败');
		// 		}
		// 	}, null ,'请求提交');
		// },
		submitOrder:function(prams, callBack, isAudit) {
			var self = this;
			if(isAudit) {
				self.submitOrderFunc({
					orderNo:prams.orderNo,
					verifyUser:prams.verifyUser,
					commitRemark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, function() {
					Rental.modal.close();
					callBack && callBack();
				});
				return;
			} else {
				bootbox.confirm('确认提交订单？', function(result) {
					result && self.submitOrderFunc({
						orderNo:prams.orderNo,
					}, callBack);
				});
			}
		},
		submitOrderFunc:function(prams, callBack) { 
			var  des = '提交订单';
			Rental.ajax.submit("{0}order/commit".format(SitePath.service), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des, response.description || '成功');
					callBack && callBack();
					Rental.modal.close();
				} else {
					Rental.notification.error(des, response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		pickingProduct:function(prams, callBack) {
			var self = this;
			OrderPicking.init({
				title:prams.title,
				callBack:function(res) {
					self.pickingSubmit({
						equipmentNo:res.equipmentNo,
						orderNo:prams.orderNo,
						operationType:prams.operationType,
					}, callBack);
				}
			});
		},
		pickingMaterial:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:'配件'+prams.title,
				url:'order-manage-input-picking-material/modal',
				initFunc:function(modal) {
					self.getWarehouseByCurrentCompany(modal);
					$('[name=materialCount]', modal).val(prams.materialCount);
				},
				callBack:function(result) {
					self.pickingSubmit({
						orderNo:prams.orderNo,
						operationType:prams.operationType,
						materialId:prams.materialId,
						materialCount:result.materialCount,
						warehouseId:result.warehouseId,
						isNewMaterial:prams.isNewMaterial, //result.isNewMaterial,
					}, callBack);
					return true;
				}
			})
		},
		getWarehouseByCurrentCompany:function(modal) {
			var self = this;
			Rental.ajax.submit("{0}warehouse/getWarehouseByCurrentCompany".format(SitePath.service), {}, function(response) {
				if(response.success) {
						Rental.ui.renderSelect({
							container:$('[name=warehouseId]', modal),
							data:response.resultMap.data,
							func:function(opt, item) {
								return opt.format(item.warehouseId, item.warehouseName);
							}
						});
				} 
			}, null ,'加载仓库列表');
		},
		pickingSubmit:function(prams, callBack) {
			var des = prams.operationType == 1 ? '配货':'清货';
			Rental.ajax.submit("{0}order/process".format(SitePath.service),prams,function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
					$('[name=equipmentNo]').val('');
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des, 'dialogLoading');
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array(),
				unSubmit = order.orderStatus == Enum.orderStatus.num.unSubmit,
				picking = order.orderStatus == Enum.orderStatus.num.waitForDelivery || order.orderStatus == Enum.orderStatus.num.inHand, //配货条件
				deliverGoods = order.orderStatus == Enum.orderStatus.num.inHand, //发货条件
				confirmReceipt = order.orderStatus == Enum.orderStatus.num.delivered, //确认收货条件
				isK3Order = order.isK3Order == 1,
				canReletOrder = order.canReletOrder == 1 || order.canReletOrder == 2 || order.canReletOrder == 3,
				rentByday = order.rentType == 1;
		
			(buttons || []).forEach(function(button, index) {
				button.orderNo = order.orderNo;
				button.canReletOrder = order.canReletOrder;
				button.statementDate = order.statementDate;
				rowActionButtons.push(button);

				if((button.class == 'submitButton' || button.class == 'editButton' || button.class == 'cancelButton') && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(button.class == 'strongCancel' && !(
					order.orderStatus == Enum.orderStatus.num.unSubmit || 
					order.orderStatus == Enum.orderStatus.num.inReview || 
					order.orderStatus == Enum.orderStatus.num.waitForDelivery ||
					deliverGoods ||
					confirmReceipt
					)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'pickingButton' || button.class == 'pickingProductButton' || button.class == 'removePickingProductButton'  || button.class == 'pickingMaterialButton' || button.class == 'removePickingMaterialButton') && !picking) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'deliverGoodsButton') && !deliverGoods) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'confirmReceiptButton' || button.class == 'confirmReceiptButtonNew') && !confirmReceipt) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'againButton') && isK3Order) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'reletButton') && !canReletOrder) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'changeSettlement' || button.class == 'sectionSettlement') && rentByday) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		},
		addRemark:{
			init:function(prams) {
				this.props = _.extend({
					orderNo:null,
					callBack:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "add-remark/modal", type:'ajax', ajaxContentAdded:function(pModal) {
					self.initDom();
					self.initEvent();	
				}});
			},
			initDom:function() {
				this.$form = $("#addRemarkForm");
				this.$cancelButton = $('.cancelButton', this.$form);
			},
			initEvent:function() {
				var self = this;
				
				Rental.form.initFormValidation(self.$form, function(form){
					self.submit();
				});
	
				self.$cancelButton.on('click', function(event) {
					event.preventDefault();
					Rental.modal.close();
				});
			},
			submit:function() {
				try {
					var self = this;
					var formData = Rental.form.getFormData(self.$form);
					if(!formData.remark) {
						bootbox.alert('请输入备注信息');
						return;
					}
					var commitData = {
						orderNo: self.props.orderNo,
						remark:formData.remark
					}
					Rental.ajax.submit("{0}{1}".format(SitePath.service,"order/addOrderMessage"),commitData,function(response){
						if(response.success) {
							Rental.notification.success("添加备注",response.description || '成功');
							self.callBackFunc();
						} else {
							Rental.notification.error("添加备注",response.description || '失败');
						}
					}, null ,"添加备注",'dialogLoading');
									
				} catch (e) {
					Rental.notification.error("添加备注失败", Rental.lang.commonJsError +  '<br />' + e );
				}
			},
			callBackFunc:function(response) {
				this.props.hasOwnProperty('callBack') && this.props.callBack(response);
			},
		},
		shortRentalRelet:{
			init:function(prams) {
				this.props = _.extend({
					orderNo:null,
					callBack:function() {},
					complete:function() {},
					continueFunc:function() {},
					modalCloseCallBack:function() {},
					changeContinueFunc:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "order-manage/choose-relet-type", type:'ajax', 
					ajaxContentAdded:function(pModal) {
						self.initDom();
						self.initEvent();	
					},
					afterClose:function() {
						_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
					}
				});
			},
			initDom:function() {
				this.$modal = $("#reletModal");
			},
			initEvent:function() {
				var self = this;
	
				$("#modelToRelet").attr("href",SitePath.base + "order-manage/test-relet-add?no=" + self.props.orderNo)

				self.$modal.on('click', '.shortToRelet', function(event) {
					event.preventDefault();
					self.shortTorelet();
				});
			},
			shortTorelet:function() {
				var self = this;
				self.props.changeContinueFunc = function() {
					self.props.modalCloseCallBack && self.props.modalCloseCallBack();
					_.isFunction(self.props.continueFunc) && self.props.continueFunc();
				}
				Rental.modal.close();
			},
		},	
	}

	window.OrderHandleMixin = OrderHandleMixin;

})(jQuery);