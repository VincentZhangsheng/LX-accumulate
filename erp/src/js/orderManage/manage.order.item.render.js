;(function($) {

	OrderManageItemRender = {
		renderProductList:function(listData, tpl, container, rowActionButtons, order, showK3No) {
			var skuList = new Array();
			if(listData) {
				listData.forEach(function(item) {
					skuList = skuList.concat(item.chooseProductSkuList);
				});
			}
			$("#orderItemProductCount").html(skuList.length);
			rowActionButtons = rowActionButtons || new Array();
			var data = _.extend(Rental.render, {
				listData:skuList,
				rowActionButtons:rowActionButtons,
				productInfo:function() {
					if(this.hasOwnProperty('categoryName')) {
						return this;
					}
					if(this.hasOwnProperty('productSkuSnapshot')) {
						return JSON.parse(this.productSkuSnapshot);
					}
					if(this.hasOwnProperty('returnProductSkuSnapshot')) {
						return JSON.parse(this.returnProductSkuSnapshot);
					}
					if(this.hasOwnProperty('deploymentProductSkuSnapshot')) {
						return JSON.parse(this.deploymentProductSkuSnapshot);	
					}
				},
				propertiesToStr:function() {
					var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
						return item.propertyValueName;
					}).join(" | ") : '';
					return str;
				},
				showK3ProductNo:function() {
					return !!showK3No ? true : false;
				},
				k3ProductNo:function() {
					var productSku = this.hasOwnProperty('productSkuSnapshot') ? JSON.parse(this.productSkuSnapshot) : "";
					return !!productSku && productSku.hasOwnProperty("k3ProductNo") ? productSku.k3ProductNo : "";
				},
				rowKey:function() {
					return parseInt(Math.random()*(100+1),10); //0-100随机数
				},
				rentTypeList:function() {
					return Enum.array(Enum.rentType, this.rentType);
				},
				rentTypeValue:function() {
					return Enum.rentType.getValue(this.rentType);
				},
				rentTypeUnit:function() {
					return Enum.rentType.getUnit(this.rentType);
				},
				isRentTypeByMonth:function() {
					return this.rentType == Enum.rentType.num.byMonth;
				},
				isNewProductBool:function() {
					return this.isNewProduct == 1;
				},
				isNewIntValue:function() {
					if(this.hasOwnProperty('isNewProduct')) {
						return this.isNewProduct;
					} else if(this.hasOwnProperty('isNew')) {
						return this.isNew;
					}
				},
				currentSkuPrice:function() {
					if((this.hasOwnProperty('isNewProduct') && parseInt(this.isNewProduct)) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
						return this.hasOwnProperty('newSkuPrice') ? this.newSkuPrice.toFixed(2) : '';
					} else {
						return this.hasOwnProperty('skuPrice') ? this.skuPrice.toFixed(2) : '';
					}
				},
				totalPrice:function() {
					if(this.hasOwnProperty('totalPrice')) {
						return this.totalPrice
					} else {
						return 0
					}
				},
				isNewProductBadgeClass:function() {
					return this.isNewProduct == 1 ? 'badge-primary':'badge-default';
				},
				showDepositAmount:function() {
					//小于90天需要设备押金，和选择支付方式
					// return this.rentType == Enum.rentType.num.byDay && parseInt(this.rentTimeLength) < 90; 
					return order.rentType == Enum.rentType.num.byDay && parseInt(order.rentTimeLength) < 90;
				},
				isByDay:function() {
					return order.rentType == Enum.rentType.num.byDay;
				},
				payModeList:function() {
					var array = Enum.array(Enum.payMode, this.payMode);	
					if(order.rentType == Enum.rentType.num.byDay) {
						array = Enum.arrayByEnumObj(Enum.payMode, Enum.payMode.numForOrder, this.payMode);	
					}
					return array;
				},
				payModeValue:function() {
					return Enum.payMode.getValue(this.payMode);
				},
				"mainImg":function() {
					return Rental.helper.mainImgUrlFormat(this.productImgList);
				},
				'productImgJSON':function() {
					return Rental.helper.imgListToJSONStringify(this.productImgList);
				},
				isRentStr:function() {
					return this.isRent == 1 ? '在租':"下架";
				},
				rowspan:function() {
					return this.hasOwnProperty('chooseProductSkuList') ? this.chooseProductSkuList.length : 0;
				},
				orderProductEquipmentListCount:function() {
					return this.hasOwnProperty('orderProductEquipmentList') ? this.orderProductEquipmentList.length : 0;
				},
				/*
				* description:是否显示首付金额、首付押金
				* author: zhangsheng
				* time : 2018/03/20
				*/
				showFirstPayRentAmount:function() {
					return this.hasOwnProperty('firstNeedPayRentAmount');
				},
				showFirstDepositAmount:function() {
					return this.hasOwnProperty('firstNeedPayDepositAmount');
				},
				/**
				* 订单详细配货设备列信息
				*/
				orderProductEquipmentListData:function() {
					var orderProductEquipmentList = this.orderProductEquipmentList;
					return _.extend(Rental.render, {
						list:orderProductEquipmentList,
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				},
				/**
				* 调拨单详细配货设备列信息
				*/
				deploymentOrderProductEquipmentListData:function() {
					var deploymentOrderProductEquipmentList = this.deploymentOrderProductEquipmentList;
					return _.extend(Rental.render, {
						deploymentOrderProductEquipmentListSource:deploymentOrderProductEquipmentList,
						sourcceLength:deploymentOrderProductEquipmentList.length,
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				},
				/**
				* 调拨单 添加、编辑商品项小计联动计算
				*/
				deploymentProductAmountTotal:function() {
					var deploymentProductAmount = 0;
					if(this.hasOwnProperty('deploymentProductUnitAmount') && this.hasOwnProperty('deploymentProductSkuCount')) {
						deploymentProductAmount = parseFloat(this.deploymentProductUnitAmount)*parseFloat(this.deploymentProductSkuCount);
					} else if(this.hasOwnProperty('deploymentProductAmount')) {
						deploymentProductAmount = this.deploymentProductAmount;
					}
					return deploymentProductAmount ? deploymentProductAmount.toFixed(2) : 0;
				},
				/*
				* 换货单更换商品信息
				*/
				destChangeProductInfo:function() {
					var self = this;
					if(self.hasOwnProperty('destChangeProduct')) {
						return {
							destChangeProductData:self.destChangeProduct,
							propertiesToStr:function() {
								var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
									return item.propertyValueName;
								}).join(" | ") : '';
								return str;
							},
							productInfo:function() {
								return this;
							},
							isNewIntValue:function() {
								if(this.hasOwnProperty('isNewProduct')) {
									return this.isNewProduct;
								} else if(this.hasOwnProperty('isNew')) {
									return this.isNew;
								} else if(self.hasOwnProperty('isNewProduct')) {
									return self.isNewProduct;
								} else if(self.hasOwnProperty('isNew')) {
									return self.isNew;
								} 
							},
						}
					} else {
						return null;
					}
				},
				/**
				* 客户模块，客户所需设备
				*/
				totalRowCurrentSkuPrice:function() {
					var currentSkuPrice = 0;
					if((this.hasOwnProperty('isNewProduct') && this.isNewProduct) || (this.hasOwnProperty('isNew') && this.isNew)) {
						currentSkuPrice = this.hasOwnProperty('newSkuPrice') ? this.newSkuPrice.toFixed(2) : 0;
					} else {
						currentSkuPrice = this.hasOwnProperty('skuPrice') ? this.skuPrice.toFixed(2) : 0;
					}
					if(this.hasOwnProperty('rentCount') && !!this.rentCount) {
						return (parseInt(this.rentCount) * parseFloat(currentSkuPrice)).toFixed(2);
					}
					return "0.00"
				},
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
			$('[data-toggle="tooltip"]', container).tooltip();
			// this.renderHead(container, listData, order);
		},
		renderMaterialList:function(listData, tpl, container, rowActionButtons, order, showK3No) {
			rowActionButtons = rowActionButtons || new Array();
			$("#orderItemMaterialCount").html(listData.length);
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:rowActionButtons,
					materialInfo:function() {
						if(this.hasOwnProperty('brandName')) {
							return this;
						}
						if(this.hasOwnProperty('returnMaterialSnapshot')) {
							return JSON.parse(this.returnMaterialSnapshot);
						}
						if(this.hasOwnProperty('deploymentProductMaterialSnapshot')) {
							return JSON.parse(this.deploymentProductMaterialSnapshot);	
						}
					},
					materialJSONString:function() {
						return JSON.stringify(this);
					},
					showK3MaterialNo:function() {
						return !!showK3No ? true : false;
					},
					k3MaterialNo:function() {
						var materialInfo = this.hasOwnProperty('materialSnapshot') ? JSON.parse(this.materialSnapshot) : "";
						return !!materialInfo && materialInfo.hasOwnProperty("k3MaterialNo") ? materialInfo.k3MaterialNo : "";
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList, this.materialName);
					},
					rentTypeList:function() {
						return Enum.array(Enum.rentType, this.rentType);
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					isRentTypeByMonth:function() {
						return this.rentType == Enum.rentType.num.byMonth;
					},
					isNewMaterialBool:function() {
						return this.isNewMaterial == 1;
					},
					isNewIntValue:function() {
						if(this.hasOwnProperty('isNewMaterial')) {
							return this.isNewMaterial;
						} else if(this.hasOwnProperty('isNew')) {
							return this.isNew;
						}
					},
					currentMaterialPrice:function() {
						if((this.hasOwnProperty('isNewMaterial') && parseInt(this.isNewMaterial) == 1) || (this.hasOwnProperty('isNew') && parseInt(this.isNew))) {
							return this.hasOwnProperty('newMaterialPrice') ? this.newMaterialPrice.toFixed(2) : '';
						} else {
							return this.hasOwnProperty('materialPrice') ? this.materialPrice.toFixed(2) : '';
						}
					},
					isNewMaterialBadgeClass:function() {
						return this.isNewMaterial == 1 ? 'badge-primary':'badge-default';
					},
					isNewMaterialValue:function() {
						return this.isNewMaterial == 1 ? '全新':'次新';
					},
					showDepositAmount:function() {
						//小于90天需要设备押金，和选择支付方式
						// return this.rentType == Enum.rentType.num.byDay && parseInt(this.rentTimeLength) < 90;
						return order.rentType == Enum.rentType.num.byDay && parseInt(order.rentTimeLength) < 90;
					},
					payModeList:function() {
						var array = Enum.array(Enum.payMode, this.payMode);	
						if(order.rentType == Enum.rentType.num.byDay) {
							array = Enum.arrayByEnumObj(Enum.payMode, Enum.payMode.numForOrder, this.payMode);	
						}
						return array;
					},
					payModeValue:function() {
						return Enum.payMode.getValue(this.payMode);
					},
					/*
					* description:是否显示首付金额、首付押金
					* author: zhangsheng
					* time : 2018/03/20
					*/
					showFirstPayRentAmount:function() {
						return this.hasOwnProperty('firstNeedPayRentAmount');
					},
					showFirstDepositAmount:function() {
						return this.hasOwnProperty('firstNeedPayDepositAmount');
					},
					/**
					* 订单配货配件列表
					*/
					orderMaterialBulkListData:function() {
						var orderMaterialBulkList = this.orderMaterialBulkList;
						return _.extend(Rental.render, {
							orderMaterialBulkListSource:orderMaterialBulkList,
							sourcceLength:orderMaterialBulkList.length
						})
					},
					/**
					* 调拨备货配件列表
					*/
					deploymentOrderMaterialBulkListData:function() {
						var deploymentOrderMaterialBulkList = this.deploymentOrderMaterialBulkList;
						return _.extend(Rental.render, {
							deploymentOrderMaterialBulkListSource:deploymentOrderMaterialBulkList,
							sourcceLength:deploymentOrderMaterialBulkList.length
						})
					},
					/**
					* 调拨单 添加、编辑配件项小计联动计算
					*/
					deploymentMaterialAmountTotal:function() {
						var deploymentMaterialAmount = 0;
						if(this.hasOwnProperty('deploymentMaterialUnitAmount') && this.hasOwnProperty('deploymentProductMaterialCount')) {
							deploymentMaterialAmount =  parseFloat(this.deploymentMaterialUnitAmount)*parseFloat(this.deploymentProductMaterialCount);
						} else if(this.hasOwnProperty('deploymentMaterialAmount')) {
							deploymentMaterialAmount = this.deploymentMaterialAmount;
						}
						return deploymentMaterialAmount ? deploymentMaterialAmount.toFixed(2) : 0;
					},
					/**
					*  换货单更换配件信息
					**/
					destChangeMaterialInfo:function() {
						var self = this;
						if(self.hasOwnProperty('destChangeMaterial')) {
							// return this.destChangeMaterial;
							return {
								destChangeMaterialData:self.destChangeMaterial,
								isNewIntValue:function() {
									if(this.hasOwnProperty('isNewMaterial')) {
										return this.isNewMaterial;
									} else if(this.hasOwnProperty('isNew')) {
										return this.isNew;
									} else if(self.hasOwnProperty('isNewMaterial')) {
										return self.isNewMaterial;
									} else if(self.hasOwnProperty('isNew')) {
										return self.isNew;
									}
								},
								materialInfo:function() {
									if(this.hasOwnProperty('brandName')) {
										return this;
									}
									if(this.hasOwnProperty('returnMaterialSnapshot')) {
										return JSON.parse(this.returnMaterialSnapshot);
									}
									if(this.hasOwnProperty('deploymentProductMaterialSnapshot')) {
										return JSON.parse(this.deploymentProductMaterialSnapshot);	
									}
								},
								materialTypeStr:function() {
									return Enum.materialType.getValue(this.materialType);
								},
							}
						} else {
							return null;
						}
					},
				})
			}
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));

			$('[data-toggle="tooltip"]', container).tooltip('show');
			// this.renderHead(container, listData, order);
		},
		renderHead:function(container, listData, order) {
			if(!order.hasOwnProperty('rentType')) return;
			var th = $('.rentalPriceTh', container), text = '单价';
			if(order.rentType == Enum.rentType.num.byDay) {
				if(order.hasOwnProperty('rentTimeLength')) {
					$('.text', th).html('{0}{1}{2}'.format(order.rentTimeLength, Enum.rentType.getUnit(parseInt(order.rentType)), text));
				} else {
					$('.text', th).html(text);
				}
				$('.fa-info-circle', th).removeClass('hide')
			} else {
				$('.text', th).html(text);
				$('.fa-info-circle', th).addClass('hide')
			}
		},
		renderGroupProductList:function(listData, tpl, container, rowActionButtons, order) {
			var groupList = new Array();
			if(listData) {
				listData.forEach(function(item) {
					groupList = groupList.concat(item);
				});
			}
			$("#orderGroupProductCount").html(groupList.length);

			rowActionButtons = rowActionButtons || new Array();
			var data = _.extend(Rental.render, {
				listData:groupList,
				rowActionButtons:rowActionButtons,
				groupActive:function() {
					return this.tabIndex == 0 ? "active" : ""
				},
				materialActive:function() {
					return this.tabIndex == 1 ? "active" : ""
				},
				groupedProductTostr:function() {
					var str = this.hasOwnProperty('chooseGroupProductList') ? this.chooseGroupProductList.map(function(item) {
						return item.productName
					}).join(' | ') : ''; 
					return str;
				},
				groupedMaterialTostr:function() {
					var str = this.hasOwnProperty('chooseMaterialList') ? this.chooseMaterialList.map(function(item) {
						return item.materialName
					}).join(' | ') : '';
					return str;
				},
				groupProduct:function() {
					var chooseGroupProductList = this.chooseGroupProductList;
					return _.extend(Rental.render, {
						chooseGroupProductList:chooseGroupProductList,
						productJSONString:function() {
							return JSON.stringify(this);
						},
						propertiesToStr:function() {
							var str = this.chooseProductSkuList[0].hasOwnProperty('productSkuPropertyList') ? this.chooseProductSkuList[0].productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						},
						rentTypeList:function() {
							return Enum.array(Enum.rentType, this.rentType);
						},
						rentTypeValue:function() {
							return Enum.rentType.getValue(this.rentType);
						},
						skuId:function() {
							return this.hasOwnProperty("chooseProductSkuList") ? (this.chooseProductSkuList[0].hasOwnProperty('skuId') ? this.chooseProductSkuList[0].skuId : "") : "";
						},
						rentTypeUnit:function() {
							return Enum.rentType.getUnit(this.rentType);
						},
						isRentTypeByMonth:function() {
							return this.rentType == Enum.rentType.num.byMonth;
						},
						isNewIntValue:function() {
							if(this.chooseProductSkuList[0].hasOwnProperty('isNewProduct')) {
								return this.chooseProductSkuList[0].isNewProduct;
							} else if(this.chooseProductSkuList[0].hasOwnProperty('isNew')) {
								return this.chooseProductSkuList[0].isNew;
							}
						},
						currentSkuPrice:function() {
							if(this.chooseProductSkuList[0].hasOwnProperty('isNewProduct') && parseInt(this.chooseProductSkuList[0].isNewProduct) == 1) {
								return this.chooseProductSkuList[0].hasOwnProperty('newSkuPrice') ? this.chooseProductSkuList[0].newSkuPrice.toFixed(2) : '';
							} else {
								return this.chooseProductSkuList[0].hasOwnProperty('skuPrice') ? this.chooseProductSkuList[0].skuPrice.toFixed(2) : '';
							}
						},
						isNewProductBadgeClass:function() {
							return this.isNewProduct == 1 ? 'badge-primary':'badge-default';
						},
						showDepositAmount:function() { 
							return order.rentType == Enum.rentType.num.byDay && parseInt(order.rentTimeLength) < 90;
						},
						isByDay:function() {
							return order.rentType == Enum.rentType.num.byDay;
						},
						payModeList:function() {
							var array = Enum.array(Enum.payMode, this.payMode);	
							if(order.rentType == Enum.rentType.num.byDay) {
								array = Enum.arrayByEnumObj(Enum.payMode, Enum.payMode.numForOrder, this.payMode);	
							}
							return array;
						},
						payModeValue:function() {
							return Enum.payMode.getValue(this.payMode);
						},
						showFirstPayRentAmount:function() {
							return this.hasOwnProperty('firstNeedPayRentAmount');
						},
						showFirstDepositAmount:function() {
							return this.hasOwnProperty('firstNeedPayDepositAmount');
						},
					})
				},
				groupMaterial:function() {
					var chooseMaterialList = this.chooseMaterialList;
					return _.extend(Rental.render, {
						chooseMaterialList:chooseMaterialList,
						materialJSONString:function() {
							return JSON.stringify(this);
						},
						materialTypeStr:function() {
							return Enum.materialType.getValue(this.material.materialType);
						},
						serialNumber:function() {
							return Rental.helper.randomString(8);
						},
						rentTypeList:function() {
							return Enum.array(Enum.rentType, this.rentType);
						},
						rentTypeValue:function() {
							return Enum.rentType.getValue(this.rentType);
						},
						rentTypeUnit:function() {
							return Enum.rentType.getUnit(this.rentType);
						},
						isRentTypeByMonth:function() {
							return this.rentType == Enum.rentType.num.byMonth;
						},
						isNewIntValue:function() {
							if(this.hasOwnProperty('isNewMaterial')) {
								return this.isNewMaterial;
							} else if(this.hasOwnProperty('isNew')) {
								return this.isNew;
							}
						},
						currentMaterialPrice:function() {
							if(this.hasOwnProperty('isNewMaterial') && parseInt(this.isNewMaterial) == 1) {
								return this.hasOwnProperty("material") && this.material.hasOwnProperty('newMaterialPrice') ? this.material.newMaterialPrice.toFixed(2) : '';
							} else {
								return this.hasOwnProperty("material") &&  this.material.hasOwnProperty('materialPrice') ? this.material.materialPrice.toFixed(2) : '';
							}
						},
						isNewMaterialBadgeClass:function() {
							return this.isNewMaterial == 1 ? 'badge-primary':'badge-default';
						},
						isNewMaterialValue:function() {
							return this.isNewMaterial == 1 ? '全新':'次新';
						},
						showDepositAmount:function() {
							return order.rentType == Enum.rentType.num.byDay && parseInt(order.rentTimeLength) < 90;
						},
						payModeList:function() {
							var array = Enum.array(Enum.payMode, this.payMode);	
							if(order.rentType == Enum.rentType.num.byDay) {
								array = Enum.arrayByEnumObj(Enum.payMode, Enum.payMode.numForOrder, this.payMode);	
							}
							return array;
						},
						payModeValue:function() {
							return Enum.payMode.getValue(this.payMode);
						},
						showFirstPayRentAmount:function() {
							return this.hasOwnProperty('firstNeedPayRentAmount');
						},
						showFirstDepositAmount:function() {
							return this.hasOwnProperty('firstNeedPayDepositAmount');
						},
					})
				}
			});
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		}
	}

	window.OrderManageItemRender = OrderManageItemRender;

})(jQuery);