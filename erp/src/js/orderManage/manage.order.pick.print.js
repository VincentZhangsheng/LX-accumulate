;(function($){
	
	var OrderPickListPrint = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
			customerNo:null,
			pageData:null, //每页数据集合
			totalAmountInfoPageIndx:1, //统计数据所在页
			productRowNum:0,
			materialRowNum:0,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_order_detail];
		},
		initDom:function() {
			this.$form = $("#orderDetailForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;
			self.loadData();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});

			$("#printButton").on("click",function(event) {
				event.preventDefault();
				self.savaPrintRecord(function() {
					window.print();
				})
			})
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到订单编号');
				return;
			}
			Rental.ajax.ajaxData('order/queryOrderByNo', {orderNo:self.state.no}, '加载订单详细信息', function(response) {
				self.initData(response.resultMap.data);
			});
		},
		initData:function(data) {
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.initPrintPager(data);
			this.renderQcode(data);
			this.renderOrderBaseInfo(data);
            this.renderProductMaterial();
			this.renderOrderAmountInfo(data);
			this.initCustomerInfo(data);
			this.renderSignature();
		},
		savaPrintRecord:function(callBack) {
			var self = this;
			var commitData = {
				referNo:self.state.no,
				referType:Enum.printReferType.num.order
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service,'print/savePrintLog'), commitData ,function(response){
				if(response.success) {
					callBack && callBack();
				} else {
					Rental.notification.error("打印订单",response.description || '失败');
				}
			}, null ,"打印订单",'dialogLoading');
		},
		renderOrderBaseInfo:function(order) {
			var renderHtml = this.renderOrderInfo(order, $('#orderBaseInfoTpl').html());
			$("#subpage1").append(renderHtml);
		},
		renderOrderAmountInfo:function(order) {
			var renderHtml = this.renderOrderInfo(order, $('#orderAmountInfoTpl').html());
			$("#orderAmountInfo").html(renderHtml);
			$("#subpage"+ this.state.totalAmountInfoPageIndx).append($("#totalInfo").html());
		},
		renderSignature:function() {
			$("#subpage"+ this.state.pageSize).append($("#signature").html());
		},
		renderOrderInfo:function(order, tpl, container) {
			var data = _.extend(Rental.render, {
				order:order,
				orderStatusValue:function() {
					return Enum.orderStatus.getValue(order.orderStatus);
				},
				totalRentalAmoutFont:function() {
					var totalProductAmount = this.hasOwnProperty('totalProductAmount') ? parseFloat(this.totalProductAmount) : 0;
					var totalMaterialAmount = this.hasOwnProperty('totalMaterialAmount') ? parseFloat(this.totalMaterialAmount) : 0;
					return (totalProductAmount + totalMaterialAmount).toFixed(2);
				},
				totalEquiumentDepositAmount:function() {
					return this.totalDepositAmount + this.totalCreditDepositAmount;
				},
				totalCount:function() {
					return this.totalProductCount + this.totalMaterialCount;
				},
				deliveryModeValue:function() {
					return Enum.deliveryMode.getValue(this.deliveryMode);
				},
				//总押金
				deliveryModeValue:function() {
					return Enum.deliveryMode.getValue(this.deliveryMode);
				},
				orderotalDepositAmount:function() {
					var totalDepositAmount = 0, totalRentDepositAmount = 0;
					if(this.hasOwnProperty('totalDepositAmount')) {
						totalDepositAmount = parseFloat(this.totalDepositAmount)
					}
					if(this.hasOwnProperty('totalRentDepositAmount')) {
						totalRentDepositAmount = parseFloat(this.totalRentDepositAmount)
					}
					return totalDepositAmount + totalRentDepositAmount;
				}
			});
			Mustache.parse(tpl);
			return Mustache.render(tpl, data);
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.orderProductList);
		},	
		initChooseMaterialList:function(data) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(data.orderMaterialList);
		},
		initCustomerInfo:function(order) {
			if(order.hasOwnProperty('orderSellerName') && order.hasOwnProperty('orderSellerPhone')) {
				$('.customerOwnerUser').html('{0} {1}'.format(order.orderSellerName,order.orderSellerPhone))
			}
			if(order.hasOwnProperty('orderUnionSellerName') && order.hasOwnProperty('orderUnionSellerPhone')) {
				$('.customerUnionUser').html('{0} {1}'.format(order.orderUnionSellerName,order.orderUnionSellerPhone))
			}
		},
		initPrintPager:function(order) {
			var self = this;
				productLength = this.state.chooseProductList.length,
				materialLength = this.state.chooseMaterialList.length,
				totalCount = productLength + materialLength,

				onePageshowCount = 10,
				firstShowAmountInfoMaxCount = 14,
				finstMaxCount = 17,
				normalCount = 20,
				lastCount = 13;

			if(order.rentType == Enum.rentType.num.byDay) {
				var onePageshowCount = 14;
				var firstShowAmountInfoMaxCount = 18;
				var finstMaxCount = 21;
				var normalCount = 24;
				var lastCount = 17;
			}

			self.state.pageData = new Array();

			if(totalCount <= onePageshowCount) {
				self.state.pageSize = 1;
				self.state.totalAmountInfoPageIndx = 1;
				self.state.pageData.push({
					productList:self.state.chooseProductList,
					materialList:self.state.chooseMaterialList,
				});
			} else if(totalCount > onePageshowCount && totalCount <= finstMaxCount + lastCount){
				self.state.pageSize = 2;
				if(totalCount <= firstShowAmountInfoMaxCount) {
					self.state.pageData.push({
						productList:self.state.chooseProductList,
						materialList:self.state.chooseMaterialList,
					});
					self.state.totalAmountInfoPageIndx = 1;
				} else {
					self.state.totalAmountInfoPageIndx = 2;
					var p = finstMaxCount - productLength;
					if(p >= 3) {
						self.state.pageData.push({
							productList:self.state.chooseProductList,
							materialList:_.first(self.state.chooseMaterialList, p-2),
						});
						self.state.pageData.push({
							productList:null,
							materialList:_.last(self.state.chooseMaterialList,materialLength - ( p-2)),
						});
					} else {
						if(productLength <=  finstMaxCount) {
							self.state.pageData.push({
								productList:self.state.chooseProductList,
								materialList:null,
							});
							self.state.pageData.push({
								productList:null,
								materialList:self.state.chooseMaterialList,
							});
						} else {
							self.state.pageData.push({
								productList: _.first(self.state.chooseProductList, finstMaxCount),
								materialList:null,
							});
							self.state.pageData.push({
								productList:_.last(self.state.chooseProductList, productLength - finstMaxCount),
								materialList:self.state.chooseMaterialList,
							});
						}
					}
				}
			} else if (totalCount >= finstMaxCount + lastCount && totalCount <= finstMaxCount + normalCount + lastCount) {
				self.state.pageSize = 3;
				self.state.totalAmountInfoPageIndx = 3;
				var p = finstMaxCount - productLength;
				if(p >= 3) {
					var firstMaterial = _.first(self.state.chooseMaterialList, p-2);
					var lastMaterial = _.last(self.state.chooseMaterialList,materialLength - ( p-2));
					self.state.pageData.push({
						productList:self.state.chooseProductList,
						materialList:firstMaterial,
					});
					var seconPageFirstMaterial = _.first(lastMaterial, normalCount);
					if(lastMaterial.length <= normalCount){
						if(normalCount >= lastMaterial.length + 3) {
							self.state.totalAmountInfoPageIndx = 2;
						} else {
							self.state.totalAmountInfoPageIndx = 3;
						}
						self.state.pageData.push({
							productList:null,
							materialList:seconPageFirstMaterial || new Array(),
						});
					} else {
						self.state.totalAmountInfoPageIndx = 3;
						var secondLastMaterial = _.last(lastMaterial, lastMaterial.length - normalCount);
						self.state.pageData.push({
							productList:null,
							materialList:seconPageFirstMaterial,
						});
						self.state.pageData.push({
							productList:null,
							materialList:secondLastMaterial || new Array(),
						});
					}
				} else {
					if(productLength <=  finstMaxCount) {
						self.state.totalAmountInfoPageIndx = 3;
						self.state.pageData.push({
							productList:self.state.chooseProductList,
							materialList:null,
						});
						self.state.pageData.push({
							productList:null,
							materialList:_.first(self.state.chooseMaterialList, normalCount),
						});
						self.state.pageData.push({
							productList:null,
							materialList:_.last(self.state.chooseMaterialList, materialLength - normalCount),
						});
					} else {
						var fp = _.first(self.state.chooseProductList, finstMaxCount);
						var lp = _.last(self.state.chooseProductList, productLength - finstMaxCount);
						self.state.pageData.push({
							productList: fp,
							materialList:null,
						});
						var sfp = _.first(lp, normalCount);
						if(lp.length <= normalCount) {
							self.state.pageData.push({
								productList:sfp,
								materialList:_.first(self.state.chooseMaterialList,normalCount - sfp.length-2),
							});
							self.state.pageData.push({
								productList:null,
								materialList:_.last(self.state.chooseMaterialList,materialLength - (normalCount - sfp.length-2)),
							});
						} else {
							var slp = _.first(lp, lp.length - normalCount);
							self.state.pageData.push({
								productList:sfp,
								materialList:null,
							});
							self.state.pageData.push({
								productList:slp,
								materialList:self.state.chooseMaterialList,
							});
						}
					}
				}
			}
			self.renderPrintPager(order);
		},
		renderPrintPager:function(order) {
			var self = this, pagerArray = new Array();
			for (var i = 1; i <= self.state.pageSize; i++) {
				pagerArray.push({pagerIndex:i});
			}
			var dataSource = {
				data:pagerArray,
				printTitle:function() {
					return order.rentType == Enum.rentType.num.byDay ? '技术服务交货单' : '租赁商品交货单';
				},
				pageSize:self.state.pageSize
			}
			var tpl = $('#pagerTpl').html();
			Mustache.parse(tpl);
			$('#orderPagers').html(Mustache.render(tpl, dataSource));
		},
		renderOrderBaseInfo:function(order) {
			var renderHtml = this.renderOrderInfo(order, $('#orderBaseInfoTpl').html());
			$("#subpage1").append(renderHtml);
		},
		renderProductMaterial:function() {
			var self = this;
			for (var i = 0; i < self.state.pageSize; i++) {
				var data = self.state.pageData[i];
				if(data) {
					data.hasOwnProperty('productList') && self.renderPrintProductList(i+1, data.productList);	
					data.hasOwnProperty('materialList') && self.renderPrintMaterialList(i+1, data.materialList);	
				}
			}
		},
		renderPrintProductList:function(pagerIndex, data) {
			var renderHtml = this.renderProductList(data);
			$('#subpage'+pagerIndex).append(renderHtml);
		},
		renderPrintMaterialList:function(pagerIndex, data) {
			var renderHtml = this.renderMaterialList(data);
			$('#subpage'+pagerIndex).append(renderHtml);
		},
		renderQcode:function(data) {
			for (var i = 1; i <= this.state.pageSize; i++) {
				var qrcode = new QRCode(document.getElementById("qrcodeCanvas"+i));
				qrcode.makeCode(data.orderNo);
				// JsBarcode("#qrcodeCanvas" + i, data.orderNo, {
				// 	format: "CODE39",
				// 	displayValue:false,
				// });
				
				$(".orderNo").html(data.orderNo);
			}
        },
        resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				chooseProductList = data.map(function(item) {
					var product =  item.hasOwnProperty('productSkuSnapshot') ? JSON.parse(item.productSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0] || {}, item)
					} 
					return _.extend(product || {}, item);
				})
			}
			return chooseProductList;
		},
		resolveChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.materialSnapshot);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
		renderProductList:function(productList) {
			var self = this;
			productList = productList || new Array();
			var data = {
				hasListData:productList.length > 0,
				dataSource: _.extend(Rental.render, {
					"listData":productList,
					propertiesToStr:function() {
						var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
							return item.propertyValueName;
						}).join(" | ") : '';
						return str;
					},
					rowNum:function() {
						self.state.productRowNum = self.state.productRowNum + 1;
						return self.state.productRowNum;
					},
					isNewProductValue:function() {
						return this.isNewProduct == 1 ? "全新":"次新";
					},
					k3Number:function() {
						var productSku = this.hasOwnProperty("productSkuSnapshot") ? JSON.parse(this.productSkuSnapshot) : "";
						return productSku.hasOwnProperty("k3ProductNo") ? productSku.k3ProductNo : "";
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			return Mustache.render(this.$dataListTpl, data)
		},
		renderMaterialList:function(materialList) {
			var self = this,
				materialList = materialList || new Array();

			var data = {
				hasListData:materialList.length > 0,
				dataSource:_.extend(Rental.render, {
					"listData":materialList,
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					rowNum:function() {
						self.state.materialRowNum = self.state.materialRowNum + 1;
						return self.state.materialRowNum;
					},
					isNewMaterialValue:function() {
						return this.isNewMaterial == 1 ? "全新":"次新";
					},
					k3Number:function() {
						var materialInfo = this.hasOwnProperty("materialSnapshot") ? JSON.parse(this.materialSnapshot) : "";
						return materialInfo.hasOwnProperty("k3MaterialNo") ? materialInfo.k3MaterialNo : "";
					}
				})
			}
			Mustache.parse(this.$materialDataListTpl);
			return Mustache.render(this.$materialDataListTpl, data);

		},
	};

	window.OrderPickListPrint = OrderPickListPrint;

})(jQuery);