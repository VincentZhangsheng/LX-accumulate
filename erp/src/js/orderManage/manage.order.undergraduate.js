;(function($) {

	var UndergraduateOrder = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_order_for_undergraduate_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			Rental.ui.events.initRangeDatePicker($('#timePicker'), $('#timePickerInput'),  $("#orderStartTime"), $("#orderEndTime"));

			Rental.ui.renderSelect({
				container:$('#status'),
				data:Enum.array(Enum.undergraduateOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（订单状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			this.$dataListTable.on('click', '.order-remark', function(event) {
				event.preventDefault();
				var remark = $(this).closest("td").data("remark");
				$('#messageContent').html(remark + '<button title="Close (Esc)" type="button" class="mfp-close">×</button>');
				Layout.init();
				$.magnificPopup.open({
                    removalDelay: 500,
                    items: {
                        src: "#messageContent"
                    },
                    midClick: true 
                });
			});

			self.render(new Object());
			self.searchData();  //初始化列表数据
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.ajaxData('activity/getActivityOrder', searchData, '加载大学生订单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var user = Rental.localstorage.getUser();

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					statusClass:function() {
						return Enum.undergraduateOrderStatus.getClass(this.status);
					},
					statusValue:function() {
						return Enum.undergraduateOrderStatus.getValue(this.status);
					},
					monthRentPrice:function() {
						return this.monthRent / this.rentTimeLength
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.UndergraduateOrder = UndergraduateOrder;

})(jQuery);