var Login = {
	init:function() {
		Rental.localstorage.clearAll();
		this.initDom();
		this.initEvent();
	},
	initDom:function() {
		this.$form = $("#loginForm");
	},
	initEvent:function() {
		Rental.form.initFormValidation(this.$form);
	},
	callBakcFunc:function(response) {
		if(response.success) {
			Rental.localstorage.setUser(response.resultMap.data);
			var ru =  Rental.helper.getUrlPara("ru")
			window.location.href = !!ru ? decodeURIComponent(ru) : SitePath.base + "home";
		} else {
			if(response.code == 'J100020') {
				window.location.href = SitePath.base + "uppwd";
			}
			return false;
		}
		return true;
	}
}
