/**
* 修改登录密码
*/
(function($) {

	var LoginUpdatePassword = {
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.$form = $("#updatePasswordForm");
		},
		initEvent:function() {
			Rental.form.initFormValidation(this.$form);
		},
		callBakcFunc:function(response) {
			if(response.success) {
				bootbox.confirm("您已成功修改密码，前往登录？", function(result) {
					result && (window.location.href = SitePath.base + "login");
				});
			} else {
				return false;
			}
			return true;
		}
	}

	window.LoginUpdatePassword = LoginUpdatePassword;

})(jQuery)