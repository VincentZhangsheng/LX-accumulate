/**
* 打印条形码
**/
;(function($) {
	var BarcodePrint = {
		state:{
			dataSource:null,
			classsName:'barcode50MM30MM',
			barcodeWHArray:[
				{
					classsName:'barcode50MM30MM',
					key:'50mm*30mm'
				},
				{
					classsName:'barcode40MM30MM',
					key:'40mm*30mm'
				}
			],
		},
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
		},
		initEvent:function() {
			var self = this;

			this.initBarcode();

			Rental.ui.multiselect({
				container:$('#barcodeWH'),
				selectClass:'multiselect dropdown-toggle btn btn-sm btn-default light',
				data:self.state.barcodeWHArray,
				func:function(opt, item) {
					return opt.format(item.classsName.toString(), item.key.toString());
				},
				change:function(val) {
					// self.createBarcode(val);
					self.createQcode(val);
				},
				defaultText:'请选择比例'
			});
		},
		initBarcode:function() {
			var list = sessionStorage.getItem(Rental.common.key.print_equiment); 
			console.log(list)
			if(list) {
				this.state.dataSource = JSON.parse(list); 
			}
			// !!this.state.dataSource && this.createBarcode();
			!!this.state.dataSource && this.createQcode();
		},
		createBarcode:function(prams) {
			var self = this,
				container = $("#printContainer"),
				className = prams || self.state.classsName;

			container.html('');
			this.state.dataSource.forEach(function(item, index) {
				container.append('<img src="" class="img-responsive pull-left '+className+'" id="barcode'+ index +'" alt="凌雄租赁" />');
				JsBarcode("#barcode"+index, item, {font: "OCR-B", textPosition: "bottom", fontSize:12, height: 100, width:1});
			});
		},
		createQcode:function(prams) {
			var self = this,
				container = $("#printContainer"),
				className = prams || self.state.classsName;

			container.html('');
			this.state.dataSource.forEach(function(item, index) {
				container.append('<div class="qcodeItem page '+className+'"><p class="qrcodeTitle">凌雄租赁</p><div class="code"><div class="qrcodeCanvas pull-left" id="qrcodeCanvas-'+item+'"></div><div class="qrcodeCanvas pull-right" id="linkQrcodeCanvas-'+item+'"></div></div><p class="equimentno">'+item+'</p></div>')
			});
			console.log(this.state.dataSource)
			this.state.dataSource.forEach(function(item, index) {
				var qrcode = new QRCode(document.getElementById("qrcodeCanvas-"+item));
				qrcode.makeCode(item);
				var qrcodelink = new QRCode(document.getElementById("linkQrcodeCanvas-"+item));
				qrcodelink.makeCode('http://support.52rental.com/?idCode='+item);
			});
		}
	}

	window.BarcodePrint = BarcodePrint;

})(jQuery);