// 审核通过
;(function($) {

	var AuditPass = {
		init:function(prams) {
			this.props = _.extend({
				workflowType:null,
				workflowReferNo:null,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "audit-manage/modal/pass", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#auditPassForm");
			this.$cancelButton = $('.cancelButton', this.$form);
		},
		initEvent:function() {
			var self = this;
			
			if(self.props.lastStep) {
				$('[name=nextVerifyUser]', this.$form).prop({disabled:true}).closest('.section').addClass('hide');
			} else {
				self.queryNextVerifyUsers();	
			}

			Rental.ui.events.initDropzone($('#auditImgIdList'), 'image/upload');
			
			Rental.form.initFormValidation(this.$form, function(form){
				self.pass();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		queryNextVerifyUsers:function(prams) {
			var self = this;
			var commitData = {
				workflowType:self.props.workflowType,
				workflowReferNo:self.props.workflowReferNo
			};
			Rental.ajax.submit("{0}workflow/queryNextVerifyUsers".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.renderNextVerifyUserSelect(response.resultMap.data);	
				} else {
					Rental.notification.error("查询审核人信息",response.description || '查询失败');
				}
			}, null ,"查询审核人信息",'listLoading');
		},
		renderNextVerifyUserSelect:function(data) {
			var input = $('[name=nextVerifyUser]', this.$form);
			if(!(_.isArray(data) && data.length > 0)) {
				input.prop({disabled:true}).closest('.section').addClass('hide');
				return;
			}
			var opt = '<option value="{0}">{1}</option>';
			var opts = (data || []).map(function(item) {
				return opt.format(item.userId, item.realName);
			});
			opts.unshift(opt.format('','请选择下一步审核人'));
			input.html(opts.join(''));
		},
		pass:function() {
			try {
				var self = this;
				var formData = Rental.form.getFormData(self.$form);
				var imgIdList = Rental.ui.events.getImageList($('#auditImgIdList'));
				if(_.isArray(imgIdList)) {
					imgIdList = _.pluck(imgIdList, 'imgId');
				}
				
				var commitData = {
					workflowLinkNo:self.props.workflowLinkNo,
					verifyStatus:Enum.workflowVerifyStatus.num.pass,
					verifyOpinion:formData['verifyOpinion'],
					imgIdList:imgIdList
				}

				if(!self.props.lastStep) {
					commitData.nextVerifyUser = formData['nextVerifyUser'];
				}

				var des = '审核';
				Rental.ajax.submit("{0}workflow/verifyWorkFlow".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '审核通过');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '审核失败');
					}
				}, null, des, 'dialogLoading');
			} catch (e) {
				Rental.notification.error("审核失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				this.props.hasOwnProperty('callBack') && this.props.callBack();
			}
		},
	};

	window.AuditPass = AuditPass;

})(jQuery)