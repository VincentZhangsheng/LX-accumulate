// 驳回工作流
;(function($) {

	var AuditReject = {
		init:function(prams) {
			this.props = _.extend({
				workflowType:null,
				workflowReferNo:null,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "audit-manage/modal/reject", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
			this.$form = $("#auditRejectForm");
			this.$cancelButton = $('.cancelButton', this.$form);
		},
		initEvent:function() {
			var self = this;

			self.initReturnType();

			Rental.ui.events.initDropzone($('#auditImgIdList'), 'image/upload');
			
			Rental.form.initFormValidation(this.$form, function(form){
				self.reject();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initReturnType:function() {
			var self = this, returnTypeArray = [{num:1, value:'驳回到上一级'}, {num:0, value:'驳回到发起人 '}];
			if(self.props.workflowType == Enum.workflowType.num.customer || self.props.workflowType == Enum.workflowType.num.channelBigCustomer) {
				returnTypeArray = [{num:0, value:'驳回到发起人 '}];
			}
			Rental.ui.renderSelect({
				container:$('[name=returnType]', self.$form),
				data:returnTypeArray,
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
				},
				defaultText:'请选择驳回层级'
			});
		},
		reject:function() {
			try {
				var self = this;
				var formData = Rental.form.getFormData(self.$form);
				var imgIdList = Rental.ui.events.getImageList($('#auditImgIdList'));
				if(_.isArray(imgIdList)) {
					imgIdList = _.pluck(imgIdList, 'imgId');
				}
				
				var commitData = {
					workflowLinkNo:self.props.workflowLinkNo,
					verifyStatus:Enum.workflowVerifyStatus.num.reject,
					verifyOpinion:formData['verifyOpinion'],
					returnType:formData['returnType'],
					imgIdList:imgIdList,
				}

				var des = '驳回';
				Rental.ajax.submit("{0}workflow/verifyWorkFlow".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '驳回');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch (e) {
				Rental.notification.error("驳回失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.hasOwnProperty('callBack') && self.props.callBack();
			}
		},
	};

	window.AuditReject = AuditReject;

})(jQuery)