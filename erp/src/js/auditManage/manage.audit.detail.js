;(function($) {

	var AuditDetail = {
		state:{
			workflowLinkNo:Rental.helper.getUrlPara('no'),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_audit];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_audit_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_audit_detail];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.actionButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var audit = this.currentManageListAuthor.children[AuthorCode.manage_audit_do_audit];

			audit && this.actionButtons.push(AuthorUtil.button(audit,'btn-danger rejectButton','','驳回'));
			audit && this.actionButtons.push(AuthorUtil.button(audit,'btn-primary passButton','','通过'));
		},
		initDom:function() {
			this.$content = $('#content');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_audit_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor, self.currentManageListAuthor, this.currentPageAuthor]); //面包

			self.loadData();

			self.$content.on('click', '.passButton', function(event) {
				event.preventDefault();
				// var rowData = $(this).closest('.rowTr').data('rowdata');
				AuditPass.init(_.extend(self.state.data, {
					callBack:function() {
						self.loadData();
					}
				}));
			});

			self.$content.on('click', '.rejectButton', function(event) {
				event.preventDefault();
				// var rowData = $(this).closest('.rowTr').data('rowdata');
				AuditReject.init(_.extend(self.state.data, {
					callBack:function() {
						self.loadData();
					}
				}));
			});

			Rental.ui.events.imgGallery(self.$content);
		},
		loadData:function() {
			var self = this;
			if(!self.state.workflowLinkNo) {
				bootbox.alert('没有找到工作流编号');
				return;
			}
			Rental.ajax.submit("{0}workflow/queryWorkflowLinkDetail".format(SitePath.service), {workflowLinkNo:self.state.workflowLinkNo}, function(response) {
				if(response.success) {
					self.state.data = response.resultMap.data;
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载工作流详细信息",response.description || "失败");
				}
			}, null ,"加载工作流详细信息",'listLoading');
		},
		initData:function(data) {
			this.renderBaseInfo(data);
			this.renderWorkflowTimeline(data);
			// this.renderAction(data);
		},
		renderBaseInfo:function(workflow) {
			$("#workflowLinkNo").html(workflow.workflowLinkNo)
			var data = {
				dataSource:workflow,
				workflowTypeStr:function() {
					return Enum.workflowType.getValue(this.workflowType);
				},
				workflowVerifyStatusStr:function() {
					return Enum.workflowVerifyStatus.getValue(this.currentVerifyStatus);
				},
				labelClass:function() {
					return 'label-' + Enum.workflowVerifyStatus.getClass(this.currentVerifyStatus);
				},
				lastStepStr:function() {
					return this.lastStep ? "是":"否";
				},
				workflowReferUrl:function() {
					return Enum.workflowType.getUrl(this.workflowType, this.workflowReferNo);
				},
				getCurrentVerifyUserName:function() {
					if(!(_.isArray(this.workflowVerifyUserGroupList) && this.workflowVerifyUserGroupList.length > 0)) {
						return this.currentVerifyUserName;
					}
					return _.pluck(this.workflowVerifyUserGroupList, 'verifyUserName').join('、');
				},
			}
			var dataTpl = $("#baserInfoTpl").html();
			Mustache.parse(dataTpl);
			$("#baseInfo").html(Mustache.render(dataTpl, data));
		},
		renderWorkflowTimeline:function(workflow) {
			workflow.lastStep && $('#timelineEndLabel').html('结束');
			var self = this,
				user = Rental.localstorage.getUser(),
				data = _.extend(Rental.render, {
					dataSource:workflow.workflowLinkDetailList || new Array(),
					verifyUserGroup:function() {
						var _this = this, _dataSource = _this.workflowVerifyUserGroupList || [];

						if(_dataSource.length > 1) {
							var _currentVerifyIndex = _.findIndex(_dataSource, {verifyUser:user.userId});
							if(_currentVerifyIndex > 0) {
								var _currentVerifyUserInfo = _dataSource[_currentVerifyIndex];
								_dataSource.splice(_currentVerifyIndex, 1);
								_dataSource.unshift(_currentVerifyUserInfo);
							}
						}

						return _.extend(Rental.render, {
							verifyUserGroupDataSource:_dataSource,
							// workflowCurrentNodeName:function() {
							// 	console.log(_this.workflowCurrentNodeName)
							// 	return _this.workflowCurrentNodeName;
							// },
							verifyOpinionText:function() {
								return this.verifyOpinion;
							},
							verifyStatusValue:function() {
								return Enum.workflowVerifyStatus.getValue(this.verifyStatus);
							},
							textClass:function() {
								return 'text-' + Enum.workflowVerifyStatus.getClass(this.verifyStatus);
							},
							showAction:function() {
								var showAction = user.userId == this.verifyUser && (this.verifyStatus == Enum.workflowVerifyStatus.num.inReview) && (_this.verifyStatus == Enum.workflowVerifyStatus.num.inReview);
								return {
									show:showAction,
									actionButtons:self.actionButtons,
								}
							},
							imageListSource:function() {
								var imageList = this.imageList;
								return {
									imageListData:imageList,
									imgurl:function() {
										return this.imgDomain + this.imgUrl;
									},
									imgListToJSONStringify:function() {
										return Rental.helper.imgListToJSONStringify(imageList, '审核附件');
									}
								}
							}
						})
					},
					verifyStatusValue:function() {
						return Enum.workflowVerifyStatus.getValue(this.verifyStatus);
					},
					textClass:function() {
						return 'text-' + Enum.workflowVerifyStatus.getClass(this.verifyStatus);
					},
					timeLinkClass:function() {
						var enum = Enum.workflowVerifyStatus.num; 
						switch(this.verifyStatus) {
							case enum.unSubmit:
								return "fa fa-ellipsis-h";
							case enum.inReview:
								return "fa fa-ellipsis-h";
							case enum.pass:
								return "glyphicons glyphicons-circle_ok";
							case enum.reject:
								return "glyphicons glyphicons-circle_remove";
							case enum.cancel:
								return "glyphicons glyphicons-circle_remove";
							default:
								return '';
						}
					},
					// verifyTimeFormat:function() {
					// 	return Rental.helper.timestampFormat(this.verifyTime);
					// },
					// showAction:function() {
					// 	var showAction = (workflow.currentVerifyUser == user.userId) && (user.userId == this.verifyUser) && (this.verifyStatus == Enum.workflowVerifyStatus.num.inReview);
					// 	return {
					// 		show:showAction,
					// 		actionButtons:self.actionButtons,
					// 	}
					// 	return showAction;
					// },
					// imageListSource:function() {
					// 	var imageList = this.imageList;
					// 	return {
					// 		imageListData:imageList,
					// 		imgurl:function() {
					// 			return this.imgDomain + this.imgUrl;
					// 		},
					// 		imgListToJSONStringify:function() {
					// 			return Rental.helper.imgListToJSONStringify(imageList, '审核附件');
					// 		}
					// 	}
					// }
				});
			var dataTpl = $('#workflowTimelineTpl').html();
			Mustache.parse(dataTpl);
			$("#workflowTimeline").html(Mustache.render(dataTpl, data));
		},
		renderAction:function(workflow) {
			workflow.lastStep && $('#timelineEndLabel').html('结束');

			var user = Rental.localstorage.getUser(), showAction = !workflow.lastStep && (workflow.currentVerifyUser == user.userId);
			if(!showAction) return;
			
			var data = {
				dataSource:workflow,
			}
			var dataTpl = $('#workflowTimelineActionTpl').html();
			Mustache.parse(dataTpl);
			$("#workflowTimelineAction").html(Mustache.render(dataTpl, data));	
		}
	};

	window.AuditDetail = AuditDetail;

})(jQuery);