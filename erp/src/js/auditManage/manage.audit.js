;(function($) {

	var AuditManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_audit];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_audit_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_audit_detail];
			var audit = this.currentPageAuthor.children[AuthorCode.manage_audit_do_audit];

			// audit && this.commonActionButtons.push(AuthorUtil.button(audit,'passButton','','通过'));
			// audit && this.commonActionButtons.push(AuthorUtil.button(audit,'rejectButton','','驳回'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			audit && this.rowActionButtons.push(AuthorUtil.button(audit,'passButton','','通过'));
			audit && this.rowActionButtons.push(AuthorUtil.button(audit,'rejectButton','','驳回'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.auditList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包
		
			Rental.ui.renderSelect({
				container:$('[name=workflowType]', self.searchForm),
				data:Enum.array(Enum.workflowType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（工作流类型）'
			});

			Rental.ui.renderSelect({
				container:$('[name=verifyStatus]', self.searchForm),
				data:Enum.array(Enum.workflowVerifyStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（审核状态）'
			});

			self.render(new Object());

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});
			self.renderVerifyStatus();

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			self.$dataListTable.on('click', '.passButton', function(event) {
				event.preventDefault();
				var rowData = $(this).closest('.rowTr').data('rowdata');
				AuditPass.init(_.extend(rowData, {
					callBack:function() {
						self.doSearch(self.Pager.pagerData.currentPage);
					}
				}));
			});

			self.$dataListTable.on('click', '.rejectButton', function(event) {
				event.preventDefault();
				var rowData = $(this).closest('.rowTr').data('rowdata');
				AuditReject.init(_.extend(rowData, {
					callBack:function() {
						self.doSearch(self.Pager.pagerData.currentPage);
					}
				}));
			});

			self.$dataListTable.on('mouseover mouseout', '.verifyMatters', function(event) {
				event.preventDefault();
				$(this).popover('toggle');
			});

			// self.$dataListTable.on('click', '.workflowReferCustomer', function(event) {
			// 	event.preventDefault();
			// 	var no = $(this).data('no')
			// 	self.forwordReferNoForCustomer(no);
			// });
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		renderVerifyStatus:function() {
			var verifyStatus = Rental.helper.getUrlPara('status')
			var isWorkbench = Rental.helper.getUrlPara('isWorkbench')
			if(!!verifyStatus) {
				Rental.ui.renderFormData(this.$searchForm, {verifyStatus:verifyStatus,isWorkbench:isWorkbench});
			}
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.auditList, searchData);
			
			Rental.ajax.submit("{0}workflow/queryWorkflowLinkPage".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询工作流",response.description || '失败');
				}
			}, null ,"查询工作流",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var user = Rental.localstorage.getUser();

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.workflowDetail, this.workflowLinkNo);
					},
					rowActionButtons:function() {
						var rowActionButtons = new Array(), _this = this;
						var currentVerifyUserInfo = _.findWhere(_this.workflowVerifyUserGroupList, {verifyUser:user.userId}) || {};

						(self.rowActionButtons || []).forEach(function(button, index) {
							rowActionButtons.push(button);
							if((button.class == 'passButton' || button.class == 'rejectButton') && 
								((currentVerifyUserInfo.verifyUser != user.userId) || (currentVerifyUserInfo.verifyStatus != Enum.workflowVerifyStatus.num.inReview) || (_this.currentVerifyStatus != Enum.workflowVerifyStatus.num.inReview)) 
							) {
								rowActionButtons.splice(rowActionButtons.length - 1,1);	
							}
						});
						return {
							buttons:rowActionButtons
						}
					},
					getCurrentVerifyUserName:function() {
						if(!(_.isArray(this.workflowVerifyUserGroupList) && this.workflowVerifyUserGroupList.length > 0)) {
							return this.currentVerifyUserName;
						}
						var currentVerifyUserInfo = _.findWhere(this.workflowVerifyUserGroupList, {verifyUser:user.userId}) || {};
						if(currentVerifyUserInfo.hasOwnProperty('verifyUserName')) {
							return  currentVerifyUserInfo.verifyUserName;
						} else {
							return _.pluck(this.workflowVerifyUserGroupList, 'verifyUserName').join('、');
						}
						return "";
					},
					rowData:function() {
						return JSON.stringify(this);
					},
					workflowTypeStr:function() {
						return Enum.workflowType.getValue(this.workflowType);
					},
					workflowVerifyStatusValue:function() {
						return Enum.workflowVerifyStatus.getValue(this.currentVerifyStatus);
					},
					workflowVerifyStatusClass:function() {
						return Enum.workflowVerifyStatus.getClass(this.currentVerifyStatus);
					},
					workflowReferUrl:function() {
						var enum = Enum.workflowType.num;
						return Enum.workflowType.getUrl(this.workflowType, this.workflowReferNo);
					},
					isCustomer:function() {
						return Enum.workflowType.num.customer == this.workflowType;
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		forwordReferNoForCustomer:function(workflowReferNo) {
			ApiData.customerDetail({
				customerNo:workflowReferNo,
				success:function(response) {
					var url = Enum.customerType.getUrl(response.customerType, workflowReferNo);
					window.location.href = url;
				}
			});
		}
	};

	window.AuditManage = AuditManage;

})(jQuery);