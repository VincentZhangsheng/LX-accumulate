//调拨单
;(function($) {

	var DeploymentOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack) {
			var self = this;

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.deploymentOrder,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.cancel(no, handleCallBack);
			});

			container.on('click', '.deliverGoodsButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.deliverGoods(no, handleCallBack);
			});

			container.on('click', '.endButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.complete(no, handleCallBack);
			});

			container.on('click', '.stcokUpProductButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.stockUpProduct(no, handleCallBack);
			});

			container.on('click', '.clearStcokUpProductButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.clearStockUpProduct(no, handleCallBack);
			});

			container.on('click', '.stcokUpMaterialButton', function(event) {
				event.preventDefault();
				self.stockUpMaterial({
					deploymentOrderNo:$(this).data('no'),
					materialId:$(this).data('materialid'), 
					materialCount:$(this).data('materialcount')
				}, handleCallBack)
			});

			container.on('click', '.clearStcokUpMaterialButton', function(event) {
				event.preventDefault();
				self.clearStockUpMaterial({
					deploymentOrderNo:$(this).data('no'),
					materialId:$(this).data('materialid'), 
					materialCount:$(this).data('materialcount')
				}, handleCallBack)
			});

			container.on('click', '.viewAffix', function(event) {
				event.preventDefault();
				$(this).closest('tr').next('.affixTr').find('.affixPanel').slideToggle('fast');
			});

			container.on('click', '.hideAffixPanel', function(event) {
				event.preventDefault();
				$(this).closest('.affixPanel').slideUp('fast');
			});
		},
		submitOrder:function(prams, callBack, isAudit) {
			var self = this, des = '提交调拨单', submitUrl = 'deploymentOrder/commit';
			if(!isAudit) {
				bootbox.confirm('确认{0}？'.format(des), function(result) {
					result && self.orderDo(submitUrl, {
						deploymentOrderNo:prams.no,
					}, des, callBack);
				});
				return;
			} else {
				self.orderDo(submitUrl, {
					deploymentOrderNo:prams.no,
					verifyUser:prams.verifyUserId,
					commitRemark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		orderDo:function(url, prams, des, callBack) {
			if(!prams.deploymentOrderNo) {
				bootbox.alert('找不到调拨单编号');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		deliverGoods:function(deploymentOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认发货？', function(result) {
				result && self.orderDo('deploymentOrder/delivery', {deploymentOrderNo:deploymentOrderNo}, '调拨单发货', callBack);
			});
		},
		cancel:function(deploymentOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认取消该调拨单？', function(result) {
				result && self.orderDo('deploymentOrder/cancel', {deploymentOrderNo:deploymentOrderNo}, '取消调拨单', callBack);
			});
		},
		complete:function(deploymentOrderNo, callBack) {	
			var self = this;
			bootbox.confirm('确定结束调拨单？',function(result) {
				result && self.orderDo('deploymentOrder/confirm', {deploymentOrderNo:deploymentOrderNo}, '结束调拨单', callBack);	
			});
		},	
		stockUpProduct:function(deploymentOrderNo, callBack) {
			this.stockProductSubmit({
				title:'商品备货',
				deploymentOrderNo:deploymentOrderNo,
				operationType:Enum.operationType.num.stock,
			}, callBack);
		},
		clearStockUpProduct:function(deploymentOrderNo, callBack) {
			this.stockProductSubmit({
				title:'商品清货',
				deploymentOrderNo:deploymentOrderNo,
				operationType:Enum.operationType.num.remove, 
			}, callBack);
		},
		stockProductSubmit:function(prams, callBack) {
			var self = this;
			OrderPicking.init({
				title:prams.title,
				callBack:function(res) {
					self.orderDo('deploymentOrder/process', {
						operationType:prams.operationType, 
						deploymentOrderNo:prams.deploymentOrderNo,
						equipmentNo:res.equipmentNo,
					}, prams.title, callBack);
					return true;
				}
			});
		},
		stockUpMaterial:function(prams, callBack) {
			this.stockMaterialSubmit(_.extend(prams, {
				title:'配件备货',
				operationType:Enum.operationType.num.stock,
			}), callBack);
		},
		clearStockUpMaterial:function(prams, callBack) {
			this.stockMaterialSubmit(_.extend(prams, {
				title:'配件清货',
				operationType:Enum.operationType.num.remove,
			}), callBack);
		},
		stockMaterialSubmit:function(prams, callBack) {
			var self = this;
			InputModal.init({
				title:prams.title,
				url:'order-deployment-manage/stock-up-material-modal',
				initFunc:function(modal) {
					$('[name=materialCount]', modal).val(prams.materialCount);
				},
				callBack:function(result) {
					self.orderDo('deploymentOrder/process', {
						operationType:prams.operationType, 
						deploymentOrderNo:prams.deploymentOrderNo,
						materialId:prams.materialId,
						materialCount:result.materialCount,
						isNewMaterial:result.isNewMaterial,
					}, prams.title, callBack);
					return true;
				}
			});
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array(),
				unSubmit = order.deploymentOrderStatus == Enum.deploymentOrderStatus.num.unSubmit,
				inHand = order.deploymentOrderStatus == Enum.deploymentOrderStatus.num.inHand,
				delivered = order.deploymentOrderStatus == Enum.deploymentOrderStatus.num.delivered;

			(buttons || []).forEach(function(button, index) {
				button.no = order.deploymentOrderNo;
				rowActionButtons.push(button);
				if((button.class == 'submitButton' || button.class == 'editButton' || button.class == 'cancelButton') && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				if((button.class == 'stockButton' || button.class == 'deliverGoodsButton' 
					|| button.class == 'stcokUpProductButton' || button.class == 'clearStcokUpProductButton' 
					|| button.class == 'stcokUpMaterialButton' || button.class == 'clearStcokUpMaterialButton') && !inHand) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);	
				}
				if(button.class == 'endButton' && !delivered) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);	
				}
			});

			return rowActionButtons;
		},
	}

	window.DeploymentOrderHandleMixin = DeploymentOrderHandleMixin;

})(jQuery);