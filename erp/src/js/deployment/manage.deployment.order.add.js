;(function($){

	var AddDeploymentOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_deployment_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_add];
		},
		initDom:function() {
			this.$form = $("#addDeploymentOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_deployment_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			self.initCommonEvent(); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				if(formData['srcWarehouseId'].toString() == formData['targetWarehouseId'].toString()) {
					bootbox.alert('同库房之间不需要调拨');
					return;
				}

				var commitData = {
					deploymentType:formData['deploymentType'],
					srcWarehouseId:formData['srcWarehouseId'],
					targetWarehouseId:formData['targetWarehouseId'],
					expectReturnTime:new Date(formData['expectReturnTime']).getTime(),
				}

				var deploymentOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						deploymentOrderProductList.push({
							deploymentProductSkuId:sku.skuId,
							deploymentProductSkuCount:sku.deploymentProductSkuCount,
							deploymentProductUnitAmount:sku.deploymentProductUnitAmount,
							isNew:sku.isNew,
						})
					})
				});

				var deploymentOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					deploymentOrderMaterialList.push({
						deploymentMaterialId:material.materialId,
						deploymentProductMaterialCount:material.deploymentProductMaterialCount,
						deploymentMaterialUnitAmount:material.deploymentMaterialUnitAmount,
						isNew:material.isNew,
					})
				});

				if(deploymentOrderProductList.length == 0 && deploymentOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(deploymentOrderProductList.length > 0) {
					commitData.deploymentOrderProductList = deploymentOrderProductList;
				}

				if(deploymentOrderMaterialList.length > 0) {
					commitData.deploymentOrderMaterialList = deploymentOrderMaterialList;
				}

				var description = "创建调拨单";
				Rental.ajax.submit("{0}deploymentOrder/create".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null , description);
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建调拨单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.AddDeploymentOrder = _.extend(AddDeploymentOrder, DeploymentOrderMixIn);

})(jQuery);