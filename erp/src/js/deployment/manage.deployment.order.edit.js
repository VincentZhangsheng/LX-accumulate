;(function($){

	var EditDeploymentOrder = {
		state:{
			no:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_deployment_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_edit];
		},
		initDom:function() {
			this.$form = $("#editDeploymentOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_deployment_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();
			
			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到调拨单编号');
				return;
			}
			Rental.ajax.submit("{0}deploymentOrder/queryDetail".format(SitePath.service),{deploymentOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载调拨单详细信息",response.description || '失败');
				}
			}, null ,"加载调拨单详细信息");
		},
		initData:function(data) {
			// this.initState(data);
			this.initFormData(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		// initState:function(data) {
		// 	this.state.customerNo = data.customerNo;
		// 	this.state.returnOrderConsignInfo = data.returnOrderConsignInfo;
		// 	this.renderOrderConsignInfo();
		// },
		initFormData:function(data) {
			if(!data) return;
			var self = this;
	  		Rental.ui.renderFormData(self.$form, data);
	  		$('[name=expectReturnTime]', self.$form).val(new Date(data.expectReturnTime).format('yyyy-MM-dd'));
		},
		initChooseMaterialList:function(returnOrder) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(returnOrder.deploymentOrderMaterialList);
			this.renderMaterialList();
		},
		edit:function() {
			var self = this;
			try {
				if(!self.state.no) {
					bootbox.alert('找不到调拨单号');
					return;
				}

				var formData = Rental.form.getFormData(self.$form);

				if(formData['srcWarehouseId'].toString() == formData['targetWarehouseId'].toString()) {
					bootbox.alert('同库房之间不需要调拨');
					return;
				}

				var commitData = {
					deploymentOrderNo:self.state.no,
					deploymentType:formData['deploymentType'],
					srcWarehouseId:formData['srcWarehouseId'],
					targetWarehouseId:formData['targetWarehouseId'],
					expectReturnTime:new Date(formData['expectReturnTime']).getTime(),
				}

				var deploymentOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						deploymentOrderProductList.push({
							deploymentProductSkuId:sku.skuId,
							deploymentProductSkuCount:sku.deploymentProductSkuCount,
							deploymentProductUnitAmount:sku.deploymentProductUnitAmount,
							isNew:sku.isNew,
						})
					})
				});

				var deploymentOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					deploymentOrderMaterialList.push({
						deploymentMaterialId:material.materialId,
						deploymentProductMaterialCount:material.deploymentProductMaterialCount,
						deploymentMaterialUnitAmount:material.deploymentMaterialUnitAmount,
						isNew:material.isNew,
					})
				});

				if(deploymentOrderProductList.length == 0 && deploymentOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(deploymentOrderProductList.length > 0) {
					commitData.deploymentOrderProductList = deploymentOrderProductList;
				}

				if(deploymentOrderMaterialList.length > 0) {
					commitData.deploymentOrderMaterialList = deploymentOrderMaterialList;
				}

				var description = "编辑调拨单";
				Rental.ajax.submit("{0}deploymentOrder/update".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null , description);
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑调拨单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form.find('.state-success').removeClass('state-success');
							self.$form[0].reset();
							self.state = new Object();
				        }
				    }
				});
			}
		}
	};

	window.EditDeploymentOrder = _.extend(EditDeploymentOrder, DeploymentOrderMixIn);

})(jQuery);