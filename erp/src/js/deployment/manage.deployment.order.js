/**
* 调拨单
*/
;(function($) {

	var DeploymentOrderManage = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_deployment_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_submit],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_cancel],
				stockUp = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_stock_up],
				end = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_end],
				deliverGoods = this.currentPageAuthor.children[AuthorCode.manage_deployment_order_deliver_goods];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));

			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			stockUp && this.rowActionButtons.push(AuthorUtil.button(view,'stockButton','','备货'));
			deliverGoods && this.rowActionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			end && this.rowActionButtons.push(AuthorUtil.button(end,'endButton','','结束'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$createStartTime = $("#createStartTime");
			this.$createEndTime = $("#createEndTime");
			this.$createTimePicker = $('#createTimePicker');
			this.$createTimePickerInput = $('#createTimePickerInput');
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.deploymentOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据
			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#deploymentOrderStatus'),
				data:Enum.array(Enum.deploymentOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（调拨单状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

			self.renderWarehouse();
		},
		renderWarehouse:function() {
			var self = this;
			ApiData.warehouse({
				success:function(response) {
					console.log(response)
					self.renderWarehouseSelect($('#srcWarehouseId'), '源仓库', response);
					self.renderWarehouseSelect($('#targetWarehouseId'), '目标仓库', response);
				}
			});
		},
		renderWarehouseSelect:function(container, defaultText, response) {
			var self = this;
			Rental.ui.renderSelect({
				container:container,
				data:response,
				func:function(opt, item) {
					return opt.format(item.warehouseId, item.warehouseName);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:defaultText
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.deploymentOrder, searchData);

			Rental.ajax.ajaxData('deploymentOrder/queryPage', searchData, '查询调拨单列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.deplymentOrderDetail, this.deploymentOrderNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					deploymentTypeValue:function() {
						return Enum.deploymentType.getValue(this.deploymentType);
					},
					deploymentOrderStatusValue:function() {
						return Enum.deploymentOrderStatus.getValue(this.deploymentOrderStatus);
					},
					deploymentOrderStatusClass:function() {
						return Enum.deploymentOrderStatus.getClass(this.deploymentOrderStatus);	
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.DeploymentOrderManage = _.extend(DeploymentOrderManage, DeploymentOrderHandleMixin);

})(jQuery);