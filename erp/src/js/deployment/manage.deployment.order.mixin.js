;(function($) {

	DeploymentOrderMixIn = {
		state:{
			customer:null,
			company:null,
			orderSeller:null,
			orderSubCompany:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			customerNo:null,
		},
		initCommonEvent:function() {
			var self = this;

			Rental.ui.renderSelect({
				data:Enum.array(Enum.deploymentType),
				container:$('[name=deploymentType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					if(parseInt(val) == Enum.deploymentType.num.borrow) {
						$("#expectReturnTimeControl").removeClass('hide');	
					} else {
						$("#expectReturnTimeControl").addClass('hide');	
					}
				}
			});

			self.$form.on('click', '.goback', function(event) {
				event.preventDefault();
				window.history.back();
			});

			self.initDatePicker();

			//选择仓库
			$("#chooseSrcWarehouseId").on('click',function(event) {
				WarehouseChoose.init({
					callBack:function(warehouse) {
						$('[name=srcWarehouseId]', self.$form).val(warehouse.warehouseId);
						$('[name=srcWarehouseName]', self.$form).val(warehouse.warehouseName);
					}
				});	
			});

			//选择仓库
			$("#chooseTargetWarehouseId").on('click',function(event) {
				WarehouseChoose.init({
					callBack:function(warehouse) {
						$('[name=targetWarehouseId]', self.$form).val(warehouse.warehouseId);
						$('[name=targetWarehouseName]', self.$form).val(warehouse.warehouseName);
					}
				});	
			});

			self.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			
			$("#batchAddProduct").click(function(event) {
				ProductChoose.modal({
					showIsNewCheckBox:true,
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});
			});

			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms:new Object(), 
					showIsNewCheckBox:true,
					callBack:function(material) {
						self.chooseMaterial(material);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				self.batchDeleteProduct();
				self.renderProductList();
			});

			self.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
				self.renderProductList();
			});

			self.$dataListTable.on('change', '.deploymentProductSkuCount,.deploymentProductUnitAmount', function(event) {
				event.preventDefault();
				var $skuRow = $(this).closest('.skuRow');
				self.changeProductEvent($skuRow);
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDeleteMaterial();
			});

			self.$materialDataListTable.on('click', '.delMaterialButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.deleteMaterial(materialNo);
			});

			self.$materialDataListTable.on('change', '.deploymentProductMaterialCount,.deploymentMaterialUnitAmount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeMaterialEvent($materialRow);
			});
		},
		initDatePicker:function() {
			var self = this , defaultStart = moment().subtract('days', 29), defaultEnd = moment();
			$('#expectReturnTime').daterangepicker({
			    "singleDatePicker": true,
			    "showDropdowns": true,
			    "showWeekNumbers": true,
			    "startDate": defaultEnd,
			    "endDate": defaultEnd
			}, function(start, end, label) {
			  	// console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
			  	$("[name=expectReturnTime]", self.$form).val(start.format('YYYY-MM-DD'));
			});
		},
		renderProductList:function(){
			OrderManageItemRender.renderProductList(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable);
		},
		renderMaterialList:function(rowActionButtons) {
			OrderManageItemRender.renderMaterialList(this.state.chooseMaterialList, this.$materialDataListTpl, this.$materialDataListTable, rowActionButtons);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = OrderManageUtil.chooseProductByIsNew(this.state.chooseProductList, product);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		changeProductEvent:function($skuRow) {
			this.state.chooseProductList = this.changeProduct(this.state.chooseProductList, $skuRow);
			this.renderProductList();
			console.log(this.state.chooseProductList)
		},
		getSkuBySkuRow:function($skuRow) {
			return {
				productId:$('.productId', $skuRow).val(),
				productSkuId:parseInt($('.productSkuId', $skuRow).val()),
				deploymentProductSkuCount:$('.deploymentProductSkuCount', $skuRow).val(),
				deploymentProductUnitAmount:$('.deploymentProductUnitAmount', $skuRow).val(),
				isNew:parseInt($('.isNew', $skuRow).val()),
			};
		},
		changeProduct:function(chooseProductList, $skuRow) {
			var sku = this.getSkuBySkuRow($skuRow);
			var productIndex = _.findIndex(chooseProductList, {productId:parseInt(sku.productId)});
			var skuIndex = _.findIndex(chooseProductList[productIndex].chooseProductSkuList, {skuId:sku.productSkuId, isNew:sku.isNew});
			chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(chooseProductList[productIndex].chooseProductSkuList[skuIndex], sku);
			return chooseProductList;
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = OrderManageUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		deleteSku:function($deleteButton) {
			this.state.chooseProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		chooseMaterial:function(material) {
			this.state.chooseMaterialList = OrderManageUtil.chooseMaterialByIsNew(this.state.chooseMaterialList, material);
			this.renderMaterialList();
		},
		changeMaterialEvent:function($materialRow) {
			this.state.chooseMaterialList = this.changeMaterial(this.state.chooseMaterialList, $materialRow);
			this.renderMaterialList();
		},
		getMaterialByMaterialRow:function($materialRow) {
			return  {
				materialNo:$('.materialNo', $materialRow).val(),
				materialId:$('.materialId', $materialRow).val(),
				deploymentProductMaterialCount:$('.deploymentProductMaterialCount', $materialRow).val(),
				deploymentMaterialUnitAmount:$('.deploymentMaterialUnitAmount', $materialRow).val(),
				isNew:parseInt($('.isNew', $materialRow).val()),
			};
		},
		changeMaterial:function(chooseMaterialList, $materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var index = _.findIndex(chooseMaterialList, {materialNo:material.materialNo, isNew:material.isNew});
			chooseMaterialList[index] = _.extend(chooseMaterialList[index], material);
			return chooseMaterialList;
		},
		batchDeleteMaterial:function() {
			this.state.chooseMaterialList = OrderManageUtil.batchDeleteMaterial(this.$materialDataListTable, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		deleteMaterial:function(materialNo) {
			var  index = _.findIndex(this.state.chooseMaterialList,{materialNo:materialNo});
			this.state.chooseMaterialList.splice(index,1);			
			this.renderMaterialList();
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.deploymentOrderProductList);
			this.renderProductList();
		},	
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('deploymentProductSkuSnapshot') ? JSON.parse(item.deploymentProductSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0], item)
					} 
					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					pre[item.productId].chooseProductSkuList.push(item);
					pre[item.productId] = _.extend(pre[item.productId], product);
			
					return pre;
				},{});

				chooseProductList = _.values(orderItemsMapByProductId);
			}
			return chooseProductList;
		},
		resolveChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.deploymentProductMaterialSnapshot);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
	};

	window.DeploymentOrderMixIn = DeploymentOrderMixIn;

})(jQuery);