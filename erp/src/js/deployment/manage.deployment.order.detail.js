;(function($){

	var DeploymentOrderDetails = {
		state:{
			no:null,
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_deployment_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_edit];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			this.stockProductActionButtons = new Array();
			this.stockMaterialActionButtons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_detail],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_submit],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_cancel],
				stockUp = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_stock_up],
				end = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_end],
				deliverGoods = this.currentManageListAuthor.children[AuthorCode.manage_deployment_order_deliver_goods];

			add && this.actionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			deliverGoods && this.actionButtons.push(AuthorUtil.button(deliverGoods,'deliverGoodsButton','','发货'));
			end && this.actionButtons.push(AuthorUtil.button(end,'endButton','','结束'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));

			stockUp && this.stockProductActionButtons.push(AuthorUtil.button(stockUp,'stcokUpProductButton','','备货'));
			stockUp && this.stockProductActionButtons.push(AuthorUtil.button(stockUp,'clearStcokUpProductButton','','清货'));

			stockUp && this.stockMaterialActionButtons.push(AuthorUtil.button(stockUp,'stcokUpMaterialButton','','备货'));
			stockUp && this.stockMaterialActionButtons.push(AuthorUtil.button(stockUp,'clearStcokUpMaterialButton','','清货'));
		},
		initDom:function() {
			this.$form = $("#editDeploymentOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_deployment_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			}); 

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到调拨单编号');
				return;
			}
			Rental.ajax.submit("{0}deploymentOrder/queryDetail".format(SitePath.service),{deploymentOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载调拨单详细信息",response.description || '失败');
				}
			}, null ,"加载调拨单详细信息");
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderStockProductButtons(data);
			this.renderOrderBaseInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
		},
		renderActionButton:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtons(self.actionButtons, order),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
					// no:self.state.no,
				},
				actionButtonsTpl = $('#actionButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderStockProductButtons:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtons(self.stockProductActionButtons, order),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
				},
				actionButtonsTpl = $('#stockUpProductButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#stockUpProductButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaseInfo:function(order) {
			$("#deploymentOrderNo").html(order.deploymentOrderNo);
			var data = _.extend(Rental.render, {
				data:order,
				deploymentTypeValue:function() {
					return Enum.deploymentType.getValue(this.deploymentType);
				},
				deploymentOrderStatusValue:function() {
					return Enum.deploymentOrderStatus.getValue(this.deploymentOrderStatus); 
				},
				showExpectReturnTime:function() {
					return Enum.deploymentType.num.borrow == this.deploymentType;
				}
			});
			var tpl = $("#orderBaseInfoTpl").html();
			Mustache.parse(tpl);
			$('#orderBaseInfo').html(Mustache.render(tpl, data));
		},
		initChooseMaterialList:function(order) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(order.deploymentOrderMaterialList);
			this.renderMaterialList(this.filterAcitonButtons(this.stockMaterialActionButtons, order));
		},
	};

	window.DeploymentOrderDetails = _.extend(DeploymentOrderDetails, DeploymentOrderMixIn, DeploymentOrderHandleMixin);

})(jQuery);