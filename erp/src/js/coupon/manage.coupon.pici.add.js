/**
 * author:liaojianming
 * date: 2018-4-4
 * decription: 批次增券
 */
;(function($){	
	var CouponPiCiAddManage = {
		init: function(prams) {
			this.props = _.extend({
				couponBatchId:null,
				callBack:function() {}
			}, prams || {});
			this.showModal();
		},
		showModal: function(){
			var self = this;
			Rental.modal.open({
				src: SitePath.base + "coupon-manage/add-coupon-pici", 
				type: 'ajax', 
				closeOnBgClick: false, 
				ajaxContentAdded: function() {
					self.initDom();
					self.initEvent();	
				}
			});
		},
		initDom: function() {
			var self = this;
			this.$form = $("#addPiCiCouponBatch");
			this.$cancelButton = $('.cancelButton', this.$form);
			this.$couponTotalCount = $('input[name=couponTotalCount]', this.$form);
		},
		initEvent: function() {
			var self = this;

			self.getOnLineStatus();

			self.checkIsNumber();

			//初始化时间控件
			Rental.ui.events.initDatePicker($("#effectiveStartTimePicker", this.$form), $("[name=effectiveStartTime]", this.$form));
			Rental.ui.events.initDatePicker($("#effectiveEndTimePicker", this.$form), $("[name=effectiveEndTime]", this.$form));
			
			Rental.form.initFormValidation(self.$form,function(form){
				self.addCouponBatch();
			});
			
			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
			Rental.helper.onOnlyDecmailNum(this.$form, '.faceValue', 2); 
		},
		getOnLineStatus: function() {
			Rental.ui.renderSelect({
				container:$("#isOnline"),
				data:Enum.array(Enum.isOnline),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					// self.searchData({pageNo: 1});
				},
				defaultText:'请选择'
			});
		},
		checkIsNumber: function() {
			// 替换非数字字符   
			this.$couponTotalCount.on('keyup', function() {  
				if(/[^\d]/.test($(this).val())){ 
					var res = $(this).val().replace(/[^\d]/g, '');    
					$(this).val(res);    
				} 
			})
		},
		addCouponBatch: function() {
			try {
				var self = this, des = "批次增券";
				if(!this.props.couponBatchId){
					bootbox.alert('找不到批次ID');
					return false;
				}
				var commitData = Rental.form.getFormData(self.$form);
				commitData.couponBatchId = this.props.couponBatchId;

				Rental.ajax.submit("{0}coupon/addCouponBatchDetail".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null ,des,'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack && self.props.callBack();
			}
			return false;
		}
	};

	window.CouponPiCiAddManage = CouponPiCiAddManage;

})(jQuery);