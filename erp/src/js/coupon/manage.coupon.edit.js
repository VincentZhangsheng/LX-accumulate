/**
 * author:liaojianming
 * date: 2018-4-9
 * decription: 优惠券批次修改
 */
;(function($){	
	var CouponEditManage = {
		init: function(prams) {
			this.props = _.extend({
				couponBatchId: null,
				$obj: null,
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal: function(){
			var self = this;
			Rental.modal.open({
				src:SitePath.base + "coupon-manage/edit-coupon", 
				type: 'ajax', 
				closeOnBgClick: false, 
				ajaxContentAdded: function() {
					self.initDom();
					self.initEvent();	
				}
			});
		},
		initDom: function() {
			var self = this;
			this.$form = $("#editCouponBatch");
			this.$cancelButton = $('.cancelButton', this.$form);
			this.$couponBatchName = $('input[name=couponBatchName]', this.$form);
			this.$couponBatchDescribe = $('input[name=couponBatchDescribe]', this.$form)
			this.$couponTypeValue = $('select[name=couponType]', this.$form);
			this.$effectiveStartTime = $('input[name=effectiveStartTime]', this.$form);
			this.$effectiveEndTime = $('input[name=effectiveEndTime]', this.$form);
			this.$remark = $('textarea[name=remark]', this.$form);
		},
		initEvent: function() {
			var self = this;

			// 获取优惠券类型
			self.getCouponType();

			// 获取修改前优惠券详情
			self.getCouponDetailData();

			//初始化时间控件
			Rental.ui.events.initDatePicker($("#effectiveStartTimePicker", this.$form), $("[name=effectiveStartTime]", this.$form));
			Rental.ui.events.initDatePicker($("#effectiveEndTimePicker", this.$form), $("[name=effectiveEndTime]", this.$form));
			
			Rental.form.initFormValidation(self.$form,function(form){
				self.updateCouponBatch();
			});
			
			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		formatDate: function(dateValue) {
			if(!dateValue) {
				dateValue = 0;
				return;
			}
			var date = new Date(parseInt(dateValue));
			return date.getFullYear() + '-' + (this.addZero(date.getMonth() + 1)) + '-' + this.addZero(date.getDate());
		},
		addZero: function(num) {
			if(num < 10) {
				return '0' + num;
			} else {
				return num;
			}
		},
		getCouponDetailData: function() {
			this.$couponBatchNameVal = this.props.$obj.parents('td.text-right').siblings('td.couponBatchName').attr('data-couponBatchName');
			this.$couponBatchDescribeVal = this.props.$obj.parents('td.text-right').siblings('td.couponBatchDescribe').attr('data-couponBatchDescribe');
			this.$couponTypeValueVal = this.props.$obj.parents('td.text-right').siblings('td.couponTypeValue').attr('data-couponTypeValue');
			this.$effectiveStartTimeVal = this.props.$obj.parents('td.text-right').siblings('td.effectiveStartTime').attr('data-effectiveStartTime');
			this.$effectiveEndTimeVal = this.props.$obj.parents('td.text-right').siblings('td.effectiveEndTime').attr('data-effectiveEndTime');
			this.$remarkVal = this.props.$obj.parents('td.text-right').siblings('td.remark').attr('data-remark');

			this.$couponBatchName.val(this.$couponBatchNameVal);
			this.$couponBatchDescribe.val(this.$couponBatchDescribeVal);
			this.$couponTypeValue.find("option:contains(" + this.$couponTypeValueVal + ")").attr('selected', true);
			this.$effectiveStartTime.val(this.formatDate(this.$effectiveStartTimeVal));
			this.$effectiveEndTime.val(this.formatDate(this.$effectiveEndTimeVal));
			this.$remark.val(this.$remarkVal);
		},
		getCouponType: function() {
			Rental.ui.renderSelect({
				container: $("#couponType"),
				data: Enum.array(Enum.couponType),
				func: function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change: function(val) {
					// self.searchData({pageNo: 1});
				},
				defaultText: '请选择优惠券类型'
			});
		},
		updateCouponBatch: function() {
			try {
				var self = this, des = "批次修改";
				if(!this.props.couponBatchId){
					bootbox.alert('找不到批次ID');
					return;
				}
				var commitData = Rental.form.getFormData(self.$form);
				commitData.couponBatchId = this.props.couponBatchId;
				Rental.ajax.submit("{0}coupon/updateCouponBatch".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				},null ,des ,'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack && self.props.callBack();
			}
			return false;
		}
	};

	window.CouponEditManage = CouponEditManage;

})(jQuery);