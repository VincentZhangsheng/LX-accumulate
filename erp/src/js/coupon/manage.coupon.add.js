/**
 * author:liaojianming
 * date: 2018-4-4
 * decription: 添加优惠券批次
 */
;(function($){	
	var CouponAddManage = {
		init: function(prams) {
			this.props = _.extend({
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal: function(){
			var self = this;
			Rental.modal.open({
				src: SitePath.base + "coupon-manage/add-coupon", 
				type: 'ajax', 
				closeOnBgClick: false, 
				ajaxContentAdded: function() {
					self.initDom();
					self.initEvent();
				}	
			});
		},
		initDom: function() {
			var self = this;
			this.$form = $("#addCouponBatch");
			this.$cancelButton = $('.cancelButton', this.$form);
		},
		initEvent: function() {
			var self = this;

			self.getCouponType();

			//初始化时间控件
			Rental.ui.events.initDatePicker($("#effectiveStartTimePicker", this.$form), $("[name=effectiveStartTime]", this.$form));
			Rental.ui.events.initDatePicker($("#effectiveEndTimePicker", this.$form), $("[name=effectiveEndTime]", this.$form));
			
			Rental.form.initFormValidation(self.$form,function(form){
				self.addCouponBatch();
			});
			
			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		getCouponType: function() {
			Rental.ui.renderSelect({
				container:$("#couponType"),
				data:Enum.array(Enum.couponType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					// self.searchData({pageNo: 1});
				},
				defaultText:'请选择优惠券类型'
			});
		},
		addCouponBatch: function() {
			try {
				var self = this, des = "批次增券";
				self.formData = Rental.form.getFormData(self.$form);
				Rental.ajax.submit("{0}coupon/addCouponBatch".format(SitePath.service), self.formData, function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null ,des,'dialogLoading');

			} catch (e) {
				Rental.notification.error(des, Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack && self.props.callBack();
			}
			return false;
		}
	};

	window.CouponAddManage = CouponAddManage;

})(jQuery);