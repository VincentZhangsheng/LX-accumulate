/**
 * author:liaojianming
 * date: 2018-4-9
 * decription: 优惠券分页列表
 */
;(function($) {

	var CouponCardListManage = {
		init: function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor: function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_activities]; //活动管理
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_activities_coupon_card]; //活动管理优惠券
			this.initActionButtons();
		},
		initActionButtons: function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var abolish = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_abolish], //优惠券批量作废
				singalAbolish = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_abolish]; // 单条作废

			abolish && this.commonActionButtons.push(AuthorUtil.button(abolish,'abolishButton','', '批量作废'));
			singalAbolish && this.rowActionButtons.push(AuthorUtil.button(singalAbolish,'singalAbolishButton', '', '作废'));
		},
		initDom: function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.couponCardList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});
			this.Pager = new Pager();
		},
		initEvent: function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			//初始化列表数据
			this.searchData();

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			// 优惠券状态
            Rental.ui.renderSelect({
				container: $("#couponStatus"),
				data: Enum.array(Enum.couponStatus),
				func: function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change: function(val) {
					self.searchData({pageNo: 1});
				},
				defaultText: '请选择优惠券状态'
			});

			// 批量作废优惠券
			this.$actionCommonButtons.on('click', '.abolishButton', function(event) {
				event.preventDefault();
				var array = $('input[name=checkItem]', self.$dataListTable).rtlCheckArray(),
					filterChecked = $('input[name=checkItem]', self.$dataListTable).filter(':checked'),
					CouponDeleteParam = {
						couponList: new Array()
					};
				if(array.length === 0) {
					bootbox.alert('请选择需要作废的优惠券!');
					return;
				}
				filterChecked.each(function(i, item) {
					var couponId = $(item).val(),
						dataObj = {
							couponId: couponId
						}
					CouponDeleteParam.couponList.push(dataObj);
				});
				self.abolishCouponBanch(CouponDeleteParam);
			})

			// 作废当前选中优惠券
			this.$dataListTable.on('click', '.singalAbolishButton', function(event) {
				event.preventDefault();
				var couponId = $(this).attr('data-couponId');
				var CouponDeleteParam = {
					couponList: [
						{
							couponId: couponId
						}
					]
				}
				self.abolishCouponBanch(CouponDeleteParam);
			})

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll', 'checkItem');
			
			self.render({});
		},
		abolishCouponBanch: function(data) {
			var self = this;
			bootbox.confirm('确认作废优惠券？', function(result) {
				if(result) {
					self.do('coupon/deleteCoupon', data, '作废优惠券', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage});
					});	
				}
			});
		},
		do: function(url, prams, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		renderCommonActionButton: function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData: function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.couponCardList, searchData);
			Rental.ajax.ajaxData('coupon/pageCoupon', searchData, '优惠券', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch: function(pageNo) {
			this.searchData({pageNo: pageNo || 1});
		},
		render: function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList: function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons: function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"couponTypeValue": function() {
						return Enum.couponType.getValue(this.couponType);
					},
					"couponStatusValue": function() {
						return Enum.couponStatus.getValue(this.couponStatus);
					},
					"isOnLineValue": function() {
						return Enum.isOnline.getValue(this.isOnline);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		filterAcitonButtons: function(buttons, couponData) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
			});
			return rowActionButtons;
		}
	};

	window.CouponCardListManage = CouponCardListManage;

})(jQuery);