/*
* author: liaojianming
* date: 2018-04-09
* decription: 批次优惠券详情
*/
;(function($) {
    var CouponDetailManage = {
        state: {
            couponBatchId: null,
            couponData: null,
            chooseCustomerList:new Array(),
            couponCanReceivedCount: null,
            couponBatchDetailId: null
        },
        init: function() {
            this.initAuthor();
            this.initDom();
            this.initEvent();
        },
        initAuthor: function() {
            try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_activities]; // 活动管理
                this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_activities_coupon_group_list]; // 批次优惠券
                this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_activities_coupon_group_detail]; // 批次优惠券详情
                this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
        },
        initActionButtons: function(){
			this.rowActionButtons = new Array();

            if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var groupDelete = this.currentManageListAuthor.children[AuthorCode.manage_activities_coupon_group_detele], //批次删除
				provideCoupon = this.currentManageListAuthor.children[AuthorCode.manage_activities_coupon_give_out]; //发放优惠券

			groupDelete && this.rowActionButtons.push(AuthorUtil.button(groupDelete, 'abolishButton', '', '作废'));
            provideCoupon && this.rowActionButtons.push(AuthorUtil.button(provideCoupon, 'provideButton', '', '发放优惠券'));
		},
        initDom: function() {
            this.$form = $("#couponBanchDetailForm");
			this.$dataListTpl = $("#dataListTpl").html();
            this.$dataListTable = $("#dataListTable");
            this.$detailTable = $("#couponBaseInfo");
            this.$detailTpl = $("#couponBaseInfoTpl").html();
            this.$couponBatchId = $(".couponBatchId small", this.$form);
            this.state.couponBatchId = Rental.helper.getUrlPara('couponBatchId');
            this.Pager = new Pager();
        },
        initEvent: function() {
            var self = this;
            this.$couponBatchId.html(this.state.couponBatchId);
            Breadcrumb.init([self.currentManageAuthor, self.currentManageListAuthor, self.currentPageAuthor]); //面包屑
            self.loadCouponDetailInfo();
            self.searchData();

            // 作废优惠券
            self.$dataListTable.on('click', '.abolishButton', function(event) {
                event.preventDefault();
                var couponBatchDetailId = $(this).attr('data-couponBatchDetailId');
                self.abolishCouponBanch(couponBatchDetailId);
            })

            // 发放优惠券
            self.$dataListTable.on('click', '.provideButton', function(event) {
                event.preventDefault();
                self.state.couponCanReceivedCount = parseInt($(this).attr('data-couponCanReceivedCount'));
                self.state.couponBatchDetailId = $(this).attr('data-couponBatchDetailId');
                self.claimChooseCustomerByAll();
                self.state.chooseCustomerList = new Array();
            })
        },
        //选择客户弹窗
		claimChooseCustomerByAll:function() {
			var self = this;
			CouponProvideManage.init({
				batch:true,
				callBack:function(customer) {
					self.chooseCustomer(self.state.chooseCustomerList,customer);
				},
				complete:function() {
					Rental.modal.close();
				},
				modalCloseCallBack:function() {
					if(self.state.chooseCustomerList.length > 0) {
						CouponTradeDetail.init({
							chooseCustomerList:self.state.chooseCustomerList,
							couponBatchDetailId:self.state.couponBatchDetailId,
							couponCanReceivedCount:self.state.couponCanReceivedCount,
							continueChooseCustomeFunc:null,
							callBack:function() {
								self.searchData();
							}
						});
					}
				}
			});
		},
        // 选择客户
        chooseCustomer: function(chooseCustomerList, customer) {
            if(!chooseCustomerList) {
				chooseCustomerList = new Array();
			}	
			if(chooseCustomerList.length > 0) {
				var _Index = _.findIndex(chooseCustomerList, {customerNo:customer.customerNo});
				if(_Index > -1) {
					return chooseCustomerList;
				} else {
					chooseCustomerList.push({
						customerName:customer.customerName,
						customerNo:customer.customerNo
					})
				}
			} else {
				chooseCustomerList.push({
					customerName:customer.customerName,
					customerNo:customer.customerNo
				})
			}
			return chooseCustomerList;
        },
        abolishCouponBanch: function(couponBatchDetailId) {
            var self = this,
				data = {
					couponBatchDetailId: couponBatchDetailId
				};
			bootbox.confirm('确认作废该批次？', function(result) {
				if(result) {
					self.do('coupon/cancelCouponByCouponBatchDetail', data, '作废批次？', function() {
						self.searchData();
					});	
				}
			});
        },
        do: function(url, prams, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
        loadCouponDetailInfo: function() {
            var self = this,
                data = {
                    couponBatchId: self.state.couponBatchId
                };
            if(!self.state.couponBatchId) {
                bootbox.alert('没找到批次ID');
                return;
            }
            Rental.ajax.ajaxData('coupon/findCouponBatchByID', data, '加载批次详情', function(response) {
                self.state.couponData = response.resultMap.data;
                self.renderCouponDetailInfo(response.resultMap.data);
            })
        },
        searchData: function(prams) {
            var self = this,
                data = $.extend({
                    couponBatchId: self.state.couponBatchId,
                    pageNo:1,
                    pageSize:this.Pager.defautPrams.pageSize,
                }, prams || {});
            Rental.ajax.ajaxData('coupon/pageCouponBatchDetail', data, '加载增券列表数据', function(response) {
                self.render(response.resultMap.data);
            })
        },
        renderCouponDetailInfo: function(couponData) {
            var self = this;
            var data = _.extend(Rental.render, {
                couponData: couponData,
                "getCouponTypeValue": function() {
                    return Enum.couponType.getValue(this.couponType);
                }
            });
            Mustache.parse(this.$detailTpl);
            this.$detailTable.html(Mustache.render(this.$detailTpl, data));
        },
        doSearch: function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
        render: function(data) {
            var self = this;
            self.renderList(data);
            self.Pager.init(data, function(pageNo) {
                self.doSearch(pageNo);
			});
        },
        renderList: function(data) {
            var self = this;
            var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
                hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;
            var data = {
                hasRowActionButtons: hasRowActionButtons,
                dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
                    "isOnLineValue":function() {
						return Enum.isOnline.getValue(this.isOnline);
					}
                })
            }
            Mustache.parse(this.$dataListTpl);
            this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        },
        filterAcitonButtons: function(buttons, couponData) {
            var self = this;
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
                rowActionButtons.push(button);
                if(button.class == 'abolishButton' && (couponData.couponTotalCount == couponData.couponCancelCount)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
                }
                if(button.class == 'provideButton' && (couponData.couponTotalCount == couponData.couponReceivedCount ||
                    couponData.couponTotalCount == couponData.couponReceivedCount + couponData.couponCancelCount ||
                    couponData.couponTotalCount == couponData.couponCancelCount || couponData.isOnline == Enum.isOnline.num.offline)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
                }
            });
			return rowActionButtons;
		},
        getCouponTypeValue: function(value) {
            switch(value) {
                case value:
                    return "设备优惠券";
                default:
                    return "";
            }
        }
    }
    window.CouponDetailManage = CouponDetailManage;
})(jQuery);