/**
 * decription: 发放优惠券
 * author:liaojianming
 * time: 2018-4-11
 */
;(function($) {

	var CouponTradeDetail = {
		state:{
			tradeDetailList: new Array()
		},
		init:function(prams) {
			this.props = _.extend({
                chooseCustomerList:new Array(),
                couponBatchDetailId:null,
                couponCanReceivedCount:null,
				callBack:function() {},
				complete:function() {},
				modalCloseCallBack:function() {},
				continueChooseCustomeFunc:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
            var self = this;
			Rental.modal.open({src:SitePath.base + "coupon-manage/coupon-trade", type:'ajax', 
			ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			},
			afterClose:function() {
				self.props.modalCloseCallBack && self.props.modalCloseCallBack();
				_.isFunction(self.props.continueChooseCustomeFunc) && self.props.continueChooseCustomeFunc();
			}
		});
		},
		initDom:function(pModal) {
			this.$modal = pModal.container;
			this.$tradeDetailModal = $("#tradeDetail");
			this.$tradeAmount = $("#tradeAmountNum");
			this.$form = $("form", this.$modal);
			this.$tradeDetailTpl = $("#tradeDetailTpl").html();
			this.$tradeDetailTable = $("#tradeDetailTable");
			this.$tradeDetailForm = $('#tradeDetailForm');
		},
		initEvent:function() {
            var self = this;
            
			//初始化
			self.renderTradeAmount();
			self.renderTradeDetailData();
            self.renderTradeDetailList();
			
			self.$tradeDetailTable.rtlCheckAll('checkAll','checkItem');

			$("#batchAddCustomer").click(function(event) {
				self.props.continueChooseCustomeFunc = function() {
					_.isObject(CouponDetailManage) && _.isFunction(CouponDetailManage.claimChooseCustomerByAll) && CouponDetailManage.claimChooseCustomerByAll();
				}
				Rental.modal.close();
			});

			$("#batchDeleteCustomer").click(function(event) {
				self.batchDeleteTrade();
				self.renderTradeDetailList();
			});

			self.$tradeDetailTable.on('keyup', '.claimAmount', function(event) {
				// 替换非数字字符   
				if(!/^\+?[1-9][0-9]*$/.test($(this).val())){ 
					var res = $(this).val().replace(/[^\d]/g, '');    
					$(this).val(res);    
				}
			})

			self.$tradeDetailTable.on('change', '.claimAmount', function(event) {
				event.preventDefault();
				var $tradeRow = $(this).closest('.tradeRow');
				self.changeTradeItem($tradeRow);
				self.renderTradeDetailList();
			});

			//删除
			self.$tradeDetailTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				self.deleteTradeItem($(this));
				self.renderTradeDetailList();
			});

			Rental.form.initFormValidation(self.$form,function(form){
				self.claim();
			});

			self.$form.on('click', '.cancelBtn', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

			self.checkIsNumber();
		},
		renderTradeAmount:function() {
			this.$tradeAmount.html("线上可发放优惠券总数：" + (parseInt(this.props.couponCanReceivedCount)) + "张");
		},
		renderTradeDetailData:function() {
			this.state.tradeDetailList = this.props.chooseCustomerList;
		},
		renderTradeDetailList:function() {
			var self = this;
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":self.state.tradeDetailList,
				})
			}
			Mustache.parse(this.$tradeDetailTpl);
			this.$tradeDetailTable.html(Mustache.render(self.$tradeDetailTpl, data));
		},
		getTradeRow:function($tradeRow) {
			return {
				customerNo:$.trim($('.customerNo', $tradeRow).val()),
				claimAmount:$.trim($('.claimAmount', $tradeRow).val())
			}
		},
		changeTradeItem:function($tradeRow) {
			var self = this;
			var trade = self.getTradeRow($tradeRow);
			var tradeIndex = _.findIndex(self.state.tradeDetailList, {customerNo:trade.customerNo});

			self.state.tradeDetailList[tradeIndex] = _.extend(self.state.tradeDetailList[tradeIndex],{claimAmount:trade.claimAmount});
		},
		deleteTradeItem:function($deleteButton) {
			var self = this;
			var customerNo = $deleteButton.data("customerno");
			var tradeIndex = _.findIndex(self.state.tradeDetailList, {customerNo:customerNo});
			self.state.tradeDetailList.splice(tradeIndex,1);
		},
		batchDeleteTrade:function() {
			var self = this, 
				array = $('[name=checkItem]', self.$tradeDetailTable).rtlCheckArray(),
				filterChecked = $('[name=checkItem]', self.$tradeDetailTable).filter(':checked');

			if(array.length == 0) {
				bootbox.alert('请选择客户');
				return self.state.tradeDetailList;
			}

			filterChecked.each(function(i, item) {
				var customerNo = $(this).val();
				var tradeIndex = _.findIndex(self.state.tradeDetailList, {customerNo:customerNo});
				self.state.tradeDetailList.splice(tradeIndex,1);
			});
		},
		claim:function() {
			try {
				var self = this,
					formData = Rental.form.getFormData(self.$form);

				var CouponProvideParam = {
					couponBatchDetailId: self.props.couponBatchDetailId
				}

				// 分配给客户的优惠券总和
				var totalCouponNum = 0;
				self.state.tradeDetailList.forEach(function(trade){
					totalCouponNum += parseInt(trade.claimAmount);
				})
				
				if(totalCouponNum > self.props.couponCanReceivedCount) {
					bootbox.alert('客户发放优惠券总数超出可发放优惠券总数，请重新发放！');
					return;
				}
				
				CouponProvideParam.customerProvideList = new Array();
				if(self.state.tradeDetailList.length > 0) {
					self.state.tradeDetailList.forEach(function(trade){
						CouponProvideParam.customerProvideList.push({
							customerNo: trade.customerNo,
							provideCount: trade.claimAmount
						})
					})
				}

				var des = '发放优惠券';
				Rental.ajax.submit("{0}coupon/provideCoupon".format(SitePath.service),CouponProvideParam,function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						Rental.modal.close();
						self.props.callBack();
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');
			} catch (e) {
				Rental.notification.error("发放优惠券失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		}
	};

	window.CouponTradeDetail = CouponTradeDetail;

})(jQuery)