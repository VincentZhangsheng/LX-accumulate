/**
 * author:liaojianming
 * date: 2018-4-4
 * decription: 优惠券批次列表
 */
;(function($) {
	var CouponListManage = {
		init: function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor: function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_activities]; //活动管理
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_activities_coupon_group_list]; //活动管理-优惠券批次
			this.initActionButtons();
		},
		initActionButtons: function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var view = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_group_detail], //批次优惠券-查看详情
				add = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_group_add], //添加优惠券批次
				edit = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_group_edit], //编辑
				groupadd = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_group_to_add], //批次增券
				groupDelete = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_group_detele], //批次删除
				groupAbolish = this.currentPageAuthor.children[AuthorCode.manage_activities_coupon_group_cancellation]; //批次作废

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','详情'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			groupadd && this.rowActionButtons.push(AuthorUtil.button(groupadd,'groupAddButton','','批次增券'));
			groupDelete && this.rowActionButtons.push(AuthorUtil.button(groupDelete, 'groupDeleteButton', '', '批次删除'));
			groupAbolish && this.rowActionButtons.push(AuthorUtil.button(groupAbolish, 'groupAbolishButton', '', '批次作废'));
		},
		initDom: function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.couponBatchList);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});
			this.Pager = new Pager();
		},
		initEvent: function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			//初始化列表数据
			this.searchData();

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			// 设备类型
            Rental.ui.renderSelect({
				container:$("#couponType"),
				data:Enum.array(Enum.couponType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData({pageNo: 1});
				},
				defaultText:'请选择设备类型'
			});

			// 添加优惠券批次
			self.$actionCommonButtons.on("click", '.addButton', function(event) {
				event.preventDefault();
				CouponAddManage.init({
					callBack: function(){
						self.searchData();
					}
				});
			});

			// 编辑
			this.$dataListTable.on('click', '.editButton', function(event) {
				event.preventDefault();
				var couponBatchId = $(this).attr('data-couponBatchId'),
					$obj = $(this);
				CouponEditManage.init({
					couponBatchId: couponBatchId,
					$obj: $obj,
					callBack: function() {
						self.searchData();
					}
				})
			})

			// 批次增券
			self.$dataListTable.on('click', '.groupAddButton', function(event) {
				event.preventDefault();
				var couponBatchId = $(this).attr('data-couponBatchId');
				CouponPiCiAddManage.init({
					couponBatchId: couponBatchId,
					callBack: function() {
						self.searchData();
					}
				})
			})

			// 批次删除
			self.$dataListTable.on('click', '.groupDeleteButton', function(event) {
				event.preventDefault();
				var couponBatchId = $(this).attr('data-couponBatchId');
				self.deleteCouponBanch(couponBatchId);
			})

			// 批次作废
			self.$dataListTable.on('click', '.groupAbolishButton', function(event) {
				event.preventDefault();
				var couponBatchId = $(this).attr('data-couponBatchId');
				self.abolishCouponBanch(couponBatchId);
			})

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.render({});
		},
		deleteCouponBanch: function(couponBatchId) {
			var self = this,
				data = {
					couponBatchId: couponBatchId
				};
			bootbox.confirm('确认删除批次？', function(result) {
				if(result) {
					self.do('coupon/deleteCouponBatch', data, '删除批次', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage});
					});	
				}
			});
		},
		abolishCouponBanch: function(couponBatchId) {
			var self = this,
				data = {
					couponBatchId: couponBatchId
				};
			bootbox.confirm('确认作废该优惠券批次？', function(result) {
				if(result) {
					self.do('coupon/cancelCouponByCouponBatch', data, '确认作废该优惠券批次', function() {
						self.searchData({pageNo: self.Pager.pagerData.currentPage});
					});	
				}
			});
		},
		do: function(url, prams, des, callBack) {
			var self = this;
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		renderCommonActionButton: function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData: function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.couponBatchList, searchData);
			Rental.ajax.ajaxData('coupon/pageCouponBatch', searchData, '优惠券批次', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch: function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render: function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList: function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					"couponTypeValue":function() {
						return Enum.couponType.getValue(this.couponType);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		filterAcitonButtons: function(buttons, couponData) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
				if(button.class == 'editButton' && (_.isNumber(couponData.couponBatchTotalCount) && couponData.couponBatchTotalCount > 0)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
				}
				if(button.class == 'groupDeleteButton' && (_.isNumber(couponData.couponBatchTotalCount) && couponData.couponBatchTotalCount > 0)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
				}
				if(button.class == 'groupAbolishButton' && (couponData.couponBatchTotalCount == couponData.couponBatchCancelCount)) {
					rowActionButtons.splice(rowActionButtons.length - 1, 1);
				}
			});
			return rowActionButtons;
		}
	};

	window.CouponListManage = CouponListManage;

})(jQuery);