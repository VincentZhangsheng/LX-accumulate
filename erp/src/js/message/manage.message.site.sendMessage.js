/*
* 站内信/发送信息
* author: zhangsheng
* time: 2018-02-02
*/
;(function(){
    var sendMessage = {
        initCommonEvent: function(){
            var self = this;
            //点击写信激活写信弹框
			self.messageMenu.on('click','#quick-compose',function(){
				self.sendWrapper.addClass("active");
			})

			//关闭写信弹框
			self.sendWrapper.on('click','.close-btn',function(){
				self.sendWrapper.removeClass('active');
			})

			//选择收件人
			$("#chooseSrcReceiveId").on('click', function(event) {
				event.preventDefault();
				ChooseUser.init({
					callBack:function(user) {
						$('[name=srcReceiveName]', self.$form).val(user.realName);
						$('[name=srcReceiveId]', self.$form).val(user.userId);
					}
				});
			});

			//发送信息
			$("#sendMessageBtn").on('click',function(){
				var receiverUserIdList = new Array();
				var receiveUserId = $('[name=srcReceiveId]').val(),
					messageTitle = $('[name=messageTitle]').val(),
					messageText = $('[name=messageText]').val();

				if(receiveUserId == '') {
					bootbox.alert('请选择收件人');
					return;
				}
				if(messageText == '') {
					bootbox.alert('填输入主题');
					return;
				}
				if(messageTitle == '') {
					bootbox.alert('请输入内容');
					return;
				}

				receiverUserIdList.push(receiveUserId)
				var parameterData = $.extend({
					title: messageTitle,
					messageText: messageText,
					receiverUserIdList: receiverUserIdList
				},{})
				
				Rental.ajax.ajaxData('message/sendMessage', parameterData, '发送信息', function(response) {
					self.sendWrapper.removeClass('active');
					setTimeout(function(){
						$('[name=srcReceiveId]').val('');
						$('[name=srcReceiveName]').val('');
						$('[name=messageTitle]').val('');
						$('[name=messageText]').val('');
					},20)	
					window.location.href = PageUrl.SiteMessageOutboxList;
				});
			})
        }
    }
    window.sendMessage = sendMessage;
})(jQuery)