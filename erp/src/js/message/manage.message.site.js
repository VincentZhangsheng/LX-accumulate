/*
* 站内信
* author: zhangsheng
* time: 2018-02-01
*/
;(function($) {

	var SiteMessageManage = {
		init:function() {
			window.location.href = PageUrl.SiteMessageInboxList;
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_message];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_message_site];
			this.initActionButtons();
		},
		/**
		 * author:zhangsheng
		 * update data:2018-02-01
		*/
		initActionButtons: function(){
			this.sendButtons = new Array();
			this.menuButtonList = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;
 
			var sendMessage = this.currentPageAuthor.children[AuthorCode.manage_message_site_sedmessage],
				inbox = this.currentPageAuthor.children[AuthorCode.manage_message_site_inbox],
				outbox = this.currentPageAuthor.children[AuthorCode.manage_message_site_outbox];
			
			sendMessage && this.sendButtons.push({"menuName":"发送消息","text":"写信"})
			inbox && this.menuButtonList.push({"menuName":"收件箱","menuUrl":"site-message/inbox-list","iClass":"text-dark"})
			outbox && this.menuButtonList.push({"menuName":"发件箱","menuUrl":"site-message/outbox-list","iClass":"text-dark"})
		},
		initDom:function() {
			this.messageMenuTpl = $("#messageMenuTpl").html();
			this.messageMenu = $("#messageMenu");
			this.sendWrapper = $(".quick-compose-form");
		},
		initEvent:function() {
			var self = this;
			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			self.renderMenu();
			self.initCommonEvent();
		},
		renderMenu:function() {
			var self = this;
			var hasSendMessage = self.sendButtons && this.sendButtons.length > 0,
				hasMenuButtons = self.menuButtonList && self.menuButtonList.length > 0;
			var data = {
				hasSendMessage: hasSendMessage,
				hasMenuButtons:hasMenuButtons,
				buttonSource:_.extend(Rental.render, {
					"buttonList": self.menuButtonList
				})
			}
			Mustache.parse(this.messageMenuTpl);
			self.messageMenu.html(Mustache.render(this.messageMenuTpl,data))
		}
	};

	window.SiteMessageManage = _.extend(SiteMessageManage, sendMessage);

})(jQuery);