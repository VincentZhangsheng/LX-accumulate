/*
* 站内信/发件箱
* author: zhangsheng
* time: 2018-02-02
*/
;(function($) {

	var outboxManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_message];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_message_site];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_message_site_outbox];
			this.initActionButtons();
		},
		/**
		 * author:zhangsheng
		 * update data:2018-02-01
		*/
		initActionButtons: function(){
			this.sendButtons = new Array();
			this.menuButtonList = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;
 
			var sendMessage = this.currentManageListAuthor.children[AuthorCode.manage_message_site_sedmessage],
				inbox = this.currentManageListAuthor.children[AuthorCode.manage_message_site_inbox],
				outbox = this.currentManageListAuthor.children[AuthorCode.manage_message_site_outbox];
			
			sendMessage && this.sendButtons.push({"menuName":"发送消息","text":"写信"})
			inbox && this.menuButtonList.push({"menuName":"收件箱","menuUrl":"site-message/inbox-list","iClass":"text-dark"})
			outbox && this.menuButtonList.push({"menuName":"发件箱","menuUrl":"site-message/outbox-list","iClass":""})
		},
		initDom:function() {
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.messageMenuTpl = $("#messageMenuTpl").html();
			this.messageMenu = $("#messageMenu");
			this.sendWrapper = $(".quick-compose-form");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;
			Layout.chooseSidebarMenu(AuthorCode.manage_message_site); //激活选中menu菜单
			Breadcrumb.init([this.currentManageAuthor,this.currentManageListAuthor,this.currentPageAuthor]); //面包屑

			self.searchData()

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderMenu();
			self.initCommonEvent();
		},
		renderMenu:function() {
			var self = this;
			var hasSendMessage = self.sendButtons && self.sendButtons.length > 0,
				hasMenuButtons = self.menuButtonList && self.menuButtonList.length > 0;
			var data = {
				hasSendMessage: hasSendMessage,
				hasMenuButtons:hasMenuButtons,
				buttonSource:_.extend(Rental.render, {
					"buttonList": self.menuButtonList
				})
			}
			Mustache.parse(this.messageMenuTpl);
			self.messageMenu.html(Mustache.render(this.messageMenuTpl,data))
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, prams || {});

			Rental.ajax.ajaxData('message/pageSendMessage', searchData, '查询收件列表', function(response) {
				self.render(response.resultMap.data)
			});

			this.$dataListTable.on('click', '.viewContent', function(event) {
				event.preventDefault();
				$('#messageContent').html($(this).data('messagetext'))
				$.magnificPopup.open({
                    removalDelay: 500, //delay removal by X to allow out-animation,
                    items: {
                        src: "#messageContent"
                    },
                    // overflowY: 'hidden', // 
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = $("#animation-switcher").find('.active-animation').attr('data-effect');
                            this.st.mainClass = Animation;
                        }
                    },
                    midClick: true 
                });
			});
		},
		doSearch:function(pageNo) {
			outboxManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			this.renderList(data);
			this.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			
			var data = {
				dataSource: _.extend(Rental.render, {
					"listData":listData
				})
			}

			Mustache.parse(this.$dataListTpl)
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.outboxManage = _.extend(outboxManage, sendMessage);

})(jQuery);