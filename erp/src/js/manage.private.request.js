;(function($) {

	ManagePrivateRequest = {
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.$form = $("#requestForm");
			this.$pramContrls = $("#pramContrls");
			this.$pramTpl = $("#pramTpl");
			this.$addPramButton = $("#addPramButton");
			this.$sendRequest = $("#sendRequest");
		},
		initEvent:function() {
			var self = this;

			this.$addPramButton.on('click', function(event) {
				event.preventDefault();
				self.$pramContrls.append(self.$pramTpl.html());
			});

			this.$form.on('click', '.delPramButton', function(event) {
				event.preventDefault();
				$(this).closest('.section').remove();
			});

			Rental.form.initFormValidation(self.$form, function() {
				self.sendRequest();
			});

			// this.$sendRequest.on('click', function(event) {
			// 	event.preventDefault();
			// 	self.sendRequest();
			// });
		},
		sendRequest:function() {
			var url = $.trim($('[name=reqUrl]').val());
			var commitData = new Object();
			$('.pramControl').each(function(index, el) {
				var pram = $.trim($("[name=reqPramName]", $(el)).val()),
					value = $.trim($("[name=reqPramValue]", $(el)).val());
				commitData[pram] = value;
			});

			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), commitData, function(response){
	           $("#resultContainer").html(JSON.stringify(response, null, 4)).removeClass('hide');
	        }, null , '', 'dialogLoading');

		}
	}

	window.ManagePrivateRequest = ManagePrivateRequest;

})(jQuery)