// 提交审核
;(function($) {

	var ViewWorkFlow = {
		init:function(prams) {
			this.props = _.extend({
				workflowType:null,
				workflowReferNo:null,
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "view-work-flow/modal", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(pModal) {
			this.$modal = pModal.container;
			this.$cancelButton = $('.cancelButton', this.$modal);
		},
		initEvent:function() {
			var self = this;

			self.loadData();

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

		},
		loadData:function() {
			var self = this;
			if(!self.props.workflowReferNo) {
				bootbox.alert('没有找到工作流编号');
				return;
			}
			Rental.ajax.submit("{0}workflow/queryWorkflowLinkDetailByType".format(SitePath.service), {
				workflowReferNo:self.props.workflowReferNo,
				workflowType:self.props.workflowType
			}, function(response) {
				if(response.success) {
					// self.state.data = response.resultMap.data;
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载工作流详细信息",response.description || "失败");
				}
			}, null ,"加载工作流详细信息", 'dialogLoading');
		},
		initData:function(data) {
			this.renderBaseInfo(data);
			this.renderWorkflowTimeline(data);
			this.imgViewer()
		},
		renderBaseInfo:function(workflow) {
			$(".workflowLinkNo", this.$modal).html(workflow.workflowLinkNo)
			var data = {
				dataSource:workflow,
				workflowTypeStr:function() {
					return Enum.workflowType.getValue(this.workflowType);
				},
				workflowVerifyStatusStr:function() {
					return Enum.workflowVerifyStatus.getValue(this.currentVerifyStatus);
				},
				labelClass:function() {
					return 'label-' + Enum.workflowVerifyStatus.getClass(this.currentVerifyStatus);
				},
				lastStepStr:function() {
					return this.lastStep ? "是":"否";
				},
				workflowReferUrl:function() {
					return Enum.workflowType.getUrl(this.workflowType, this.workflowReferNo);
				},
				getCurrentVerifyUserName:function() {
					if(!(_.isArray(this.workflowVerifyUserGroupList) && this.workflowVerifyUserGroupList.length > 0)) {
						return this.currentVerifyUserName;
					}
					return _.pluck(this.workflowVerifyUserGroupList, 'verifyUserName').join('、');
				},
			}
			var dataTpl = $(".baserInfoTpl", this.$modal).html();
			Mustache.parse(dataTpl);
			$(".baseInfo", this.$modal).html(Mustache.render(dataTpl, data));
		},
		renderWorkflowTimeline:function(workflow) {
			workflow.lastStep && $('.timelineEndLabel', this.$modal).html('结束');
			var self = this,
				user = Rental.localstorage.getUser(),
				data = _.extend(Rental.render, {
					dataSource:workflow.workflowLinkDetailList || new Array(),
					verifyUserGroup:function() {
						var _this = this, _dataSource = _this.workflowVerifyUserGroupList || [];

						if(_dataSource.length > 1) {
							var _currentVerifyIndex = _.findIndex(_dataSource, {verifyUser:user.userId});
							if(_currentVerifyIndex > 0) {
								var _currentVerifyUserInfo = _dataSource[_currentVerifyIndex];
								_dataSource.splice(_currentVerifyIndex, 1);
								_dataSource.unshift(_currentVerifyUserInfo);
							}
						}

						return _.extend(Rental.render, {
							verifyUserGroupDataSource:_dataSource,
							verifyOpinionText:function() {
								return this.verifyOpinion;
							},
							verifyStatusValue:function() {
								return Enum.workflowVerifyStatus.getValue(this.verifyStatus);
							},
							textClass:function() {
								return 'text-' + Enum.workflowVerifyStatus.getClass(this.verifyStatus);
							},
							showAction:function() {
								var showAction = user.userId == this.verifyUser && (this.verifyStatus == Enum.workflowVerifyStatus.num.inReview) && (_this.verifyStatus == Enum.workflowVerifyStatus.num.inReview);
								return {
									show:showAction,
									actionButtons:self.actionButtons,
								}
							},
							imageListSource:function() {
								var imageList = this.imageList;
								return {
									imageListData:imageList,
									imgurl:function() {
										return this.imgDomain + this.imgUrl;
									},
									imgListToJSONStringify:function() {
										return Rental.helper.imgListToJSONStringify(imageList, '审核附件');
									}
								}
							}
						})
					},
					verifyStatusValue:function() {
						return Enum.workflowVerifyStatus.getValue(this.verifyStatus);
					},
					textClass:function() {
						return 'text-' + Enum.workflowVerifyStatus.getClass(this.verifyStatus);
					},
					timeLinkClass:function() {
						var enum = Enum.workflowVerifyStatus.num; 
						switch(this.verifyStatus) {
							case enum.unSubmit:
								return "fa fa-ellipsis-h";
							case enum.inReview:
								return "fa fa-ellipsis-h";
							case enum.pass:
								return "glyphicons glyphicons-circle_ok";
							case enum.reject:
								return "glyphicons glyphicons-circle_remove";
							case enum.cancel:
								return "glyphicons glyphicons-circle_remove";
							default:
								return '';
						}
					},
				});
			var dataTpl = $('.workflowTimelineTpl', this.$modal).html();
			Mustache.parse(dataTpl);
			$(".workflowTimeline", this.modal).html(Mustache.render(dataTpl, data));
		},
		imgViewer:function() {
			$(".workFlowImg").each(function() {
				$(this).viewer();
			})
		},
	};

	window.ViewWorkFlow = ViewWorkFlow;

})(jQuery)