;(function($) {

	var MaterialChoose = {
		init:function(prams) {
			this.props = $.extend({
				btach:false, //是否是批量
				searchParms:{},
				showIsNewCheckBox:false,
				isVerifyStock:true, //是否验证库存
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "material/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseMaterialModal");
			this.$searchForm = $("#chooseMaterialSearchForm");
			this.$dataListTpl = $("#chooseMaterialModalDataListTpl").html();
			this.$dataListTable = $("#chooseMaterialDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '[name=isNewMaterial]', function(event) {
				var isChecked = $(this).prop('checked'), $materialRow = $(this).closest('.materialRow');
				if(isChecked) {
					$(".isNew", $materialRow).removeClass('hide');
					$(".isUsed", $materialRow).addClass('hide');
				} else {
					$(".isUsed", $materialRow).removeClass('hide');
					$(".isNew", $materialRow).addClass('hide');
				}
			});

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var material = $(this).data('material'),
					isOldMaterial = $(this).closest('.materialRow').find('.oldMaterial').prop('checked'),
					isNewMaterial = $(this).closest('.materialRow').find('.newMaterial').prop('checked');

				if(self.props.showIsNewCheckBox == true && !isOldMaterial && !isNewMaterial) {
					bootbox.alert("请选择配件新旧属性");
					return false;
				}
				if(self.props.showIsNewCheckBox == true && isOldMaterial) {
					material.isNewMaterial = 0;
					material.isNew = 0;
				}
				if(self.props.showIsNewCheckBox == true && isNewMaterial) {
					material.isNewMaterial = 1;
					material.isNew = 1;
				}
				self.chooseMaterial(_.extend({}, material));
			});
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				isRent:1, //在租
			}, self.props.searchParms || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			Rental.ajax.ajaxData('material/queryAllMaterial', searchData, '加载配件列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				showIsNewCheckBox:self.props.showIsNewCheckBox,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					materialJSONString:function() {
						return JSON.stringify(this);
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList, this.materialName);
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		chooseMaterial:function(material) {
			// if(this.props.isVerifyStock) {
			// 	if((material.isNew && material.newMaterialCount == 0) || (!material.isNew && material.oldMaterialCount == 0)){
			// 		bootbox.alert('此配件无库存，请重选');
			// 		return;
			// 	}
			// }
			this.props.hasOwnProperty('callBack') && this.props.callBack(material);
			!this.props.btach && Rental.modal.close();
		}
	};

	window.MaterialChoose = MaterialChoose;

})(jQuery)