;(function($) {

	var ChooseCustomer = {
		init:function(prams) {
			this.props = _.extend({
				batch:false,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-modal/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseCustomerModal");
			this.$searchForm = $(".searchForm", this.$chooseModal);
			this.$dataListTpl = $("#chooseCustomerDataListTpl").html();
			this.$dataListTable = $("#chooseCustomerDataListTable");
			this.$ownerName = $("#ownerName");
			this.$owner = $("#owner");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("[name=ownerSubCompanyId]", self.$chooseModal),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

			self.searchData();

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm, function(){
				self.searchData();
			});

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);

				self.$ownerName.val(rowData.ownerName);
				self.$owner.val(rowData.owner);
			});

			self.render({});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				isDisabled:0,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('customer/pageCustomerPerson', searchData, '查询客户列表失败', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowData:function() {
						return JSON.stringify(this);
					},
					"cutomerTypeValue":function() {
						return Enum.customerType.getValue(this.customerType);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					addressForamt:function() {
						return (this.customerPerson.provinceName || '') + (this.customerPerson.cityName  || '') + (this.customerPerson.districtName  || '') + (this.customerPerson.address  || '');
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function(rowData) {
			var self = this;
			self.props.hasOwnProperty('callBack') && self.props.callBack(rowData);	
			if(this.props.batch) {
				Rental.notification.success("选择客户", '成功');
			} else {
				Rental.modal.close();
			}
		}
	};

	window.ChooseCustomer = ChooseCustomer;

})(jQuery)