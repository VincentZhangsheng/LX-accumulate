;(function($) {

	var WarehouseChoose = {
		init:function(prams) {
			this.props = _.extend({
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "warehouse/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseWarehouseModal = $("#chooseWarehouseModal");
			this.$searchForm = $("#chooseWarehouseSearchForm");
			this.$dataListTpl = $("#chooseWarehouseModalDataListTpl").html();
			this.$dataListTable = $("#chooseWarehouseModalDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			//选择仓库
			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(WarehouseChoose.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}warehouse/getWarehousePage".format(SitePath.service),searchData,function(response){
				if(response.success) {
					WarehouseChoose.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载仓库",response.description || '失败');
				}
			}, null ,"加载仓库",'listLoading');
		},
		doSearch:function(pageNo) {
			WarehouseChoose.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSearch,$('.pagerContainer',this.$chooseWarehouseModal));
		},
		renderList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : []
			var data = {
				dataSource:{
					"listData":listData,
					"rowData":function() {
						return JSON.stringify(this);
					}
				}
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function(rowData) {
			// var self = this;
			// var $bringContainer = self.$bringButton.closest('.bringContainer');
			// $("[bringFiled=warehouseNo]",$bringContainer).val(rowData.warehouseNo);
			// $("[bringFiled=warehouseName]",$bringContainer).val(rowData.warehouseName);
			// Rental.modal.close();
			_.isFunction(this.props.callBack) && this.props.callBack(rowData);
			Rental.modal.close();
		}
	};

	window.WarehouseChoose = WarehouseChoose;

})(jQuery)