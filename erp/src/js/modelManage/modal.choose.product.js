;(function($) {
	var ProductChoose = {
		state:{
			product:null,
		},
		init:function(callBack) {
			this.props = {
				productId:null, //商品ID,用户过滤商品信息
				showIsNewCheckBox:false, //是否显示新旧商品，供选择
				isVerifyStock:true, //是否验证库存
				callBack:callBack,
			};
			this.show();
		},
		modal:function(prams) {
			this.props = _.extend({
				productId:null,
				isVerifyStock:true,
				showIsNewCheckBox:false,
				callBack:function() {},
				complete:function() {},
				continueFunc:function() {},
				modalCloseCallBack:function() {},
				changeContinueFunc:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "product/choose", type:'ajax', 
				ajaxContentAdded:function(pModal) {
					self.initDom(pModal);
					self.initEvent();	
				},
				afterClose:function() {
					_.isFunction(self.props.changeContinueFunc) && self.props.changeContinueFunc();
				}
			});
		},
		initDom:function(modal) {
			this.$chooseModal = modal.container;//$("#chooseProductModal");
			this.$searchForm = $("#chooseProductSearchForm");
			this.$dataListTpl = $("#chooseProductModalDataListTpl").html();
			this.$dataListTable = $("#chooseProductDataListTable");

			!!this.props.productId && $('[name=productId]', this.$searchForm).val(this.props.productId).addClass('hide');

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '.oldProduct', function(event) {
				var isChecked = $(this).prop('checked'), $skuTr = $(this).closest('.skuTr');
				if(isChecked) {
					$(".isNew", $skuTr).addClass('hide');
					$(".isUsed", $skuTr).removeClass('hide');
				}
			});

			self.$dataListTable.on('click', '.newProduct', function(event) {
				var isChecked = $(this).prop('checked'), $skuTr = $(this).closest('.skuTr');
				if(isChecked) {
					$(".isNew", $skuTr).removeClass('hide');
					$(".isUsed", $skuTr).addClass('hide');
				}
			});

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $this =  $(this), productId = $this.data('productid');
				self.choose($this,productId);
			});

			self.$dataListTable.on('click', '.confirmChoose', function(event) {
				event.preventDefault();
				self.confirm($(this));
			});

			self.$dataListTable.on('click', '.cancelChoose', function(event) {
				event.preventDefault();
				$(this).closest('.productDetial').slideUp();
			});

			self.$dataListTable.on('click', '.cancelChoose', function(event) {
				event.preventDefault();
				$(this).closest('.productDetial').slideUp();
			});

			self.$dataListTable.on('click', '.confirmStock', function(event) {
				event.preventDefault();
				self.confirmStock($(this));
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = _.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				isRent:1, //在租
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('product/queryAllProduct', searchData, '查询商品列表失败', function(response) {
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', self.$chooseModal));
		},
		renderList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '在租':"下架";
					},
					totalStock:function() {
						return _.reduce(this.productSkuList, function(pre, item) {
							return pre + parseInt(item.stock);
						}, 0);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function($button,id) {
			var detailTr =  $button.closest('tr').next('tr'), productDetial = $('.productDetial',detailTr);
			if(productDetial.size() > 0) {
				productDetial.slideDown(); 
			} else {
				this.getProductById($('td',detailTr),id);	
			}
		},
		getProductById:function(container,id) {
			var self = this;
			Rental.ajax.ajaxData('product/queryProductById', {productId:id}, '加载商品详细信息', function(response) {
				self.renderRowProduct(container, response.resultMap.data);
			});
		},
		renderRowProduct:function(container,product) {
			var self = this;
			var data  = {
				product:product,
				productJSONString:function() {
					return JSON.stringify(this);
				},
				skuListSource:function() {
					var skuList = this.productSkuList;
					return _.extend(Rental.render, {
						showIsNewCheckBox:self.props.showIsNewCheckBox,
						skuList:skuList,
						propertiesToStr:function() {
							var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
								return item.propertyValueName;
							}).join(" | ") : '';
							return str;
						},
						rowKey:function() {
							return parseInt(Math.random()*(100+1),10); //0-100随机数
						}
					})
				}
			}
			var tpl = $('#chooseProductModalDetialTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		confirm:function($confirmButton) {
			var self = this;
			var $productDetial = $confirmButton.closest('.productDetial');
			var product = $productDetial.data('product');
			var checkedSku = $('[name=sku]',$productDetial).filter(':checked');
			if(checkedSku.length == 0) {
				bootbox.alert("请选择SKU");
				return false;
			}
			var skuList = new Array();
			checkedSku.each(function(i,item) {
				var skuId = $(item).val(),
					isNewProduct = $(item).closest('.skuTr').find('.newProduct').prop('checked'),
					isOldProduct = $(item).closest('.skuTr').find('.oldProduct').prop('checked'),
					sku = product.productSkuList.filter(function(filterItem) {
						return filterItem.skuId == skuId;
					})[0];

				var newSku = _.extend(_.extend({}, product), sku);

				//由于各个功能模块后端给的字段不统一，这里加多一个字段。
				if(self.props.showIsNewCheckBox == true && isNewProduct) {
					newSku.isNewProduct = 1; 
					newSku.isNew = 1;
				}
				if(self.props.showIsNewCheckBox == true && isOldProduct) {
					newSku.isNewProduct = 0; 
					newSku.isNew = 0;
				}

				skuList.push(newSku);
			});
			var filterSkuList = skuList.filter(function(item) {
				return item.hasOwnProperty("isNewProduct")
			})
			if(filterSkuList.length != skuList.length) {
				bootbox.alert("请选择商品新旧属性");
				return false;
			}
			product.chooseProductSkuList = skuList;

			// if(this.props.isVerifyStock) {
			// 	var noStock = _.filter(product.chooseProductSkuList, function(item) {
			// 		return (item.isNew && item.newProductSkuCount == 0) || (!item.isNew && item.oldProductSkuCount == 0);
			// 	});
			// 	if(noStock && noStock.length > 0) {
			// 		bootbox.alert('您选择了无库存SKU，请重选');
			// 		return;
			// 	}
			// }

			self.props.callBack && self.props.callBack(_.extend({}, product));
		},
		confirmStock:function($button) {
			var self = this;
			var $productDetial = $button.closest('.productDetial');
			var product = $productDetial.data('product');
			self.state.product = _.extend({}, product);
			self.props.changeContinueFunc = function() {
				self.props.modalCloseCallBack && self.props.modalCloseCallBack(self.state.product);
				_.isFunction(self.props.continueFunc) && self.props.continueFunc();
			}
			Rental.modal.close();
		}
	};

	window.ProductChoose = ProductChoose;

})(jQuery)