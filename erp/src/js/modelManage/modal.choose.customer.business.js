;(function($) {

	var ChooseBusinessCustomer = {
		init:function(prams) {
			this.props = _.extend({
				batch:false,
				continue:false,
				chooseCustomerList:new Array(),
				callBack:function() {},
                complete:function() {},
				modalCloseCallBack:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "business-customer-modal/choose", type:'ajax', 
				ajaxContentAdded:function(pModal) {
					self.initDom();
					self.initEvent();	
				},
				afterClose:function() {
					if(self.props.batch) {
						self.props.modalCloseCallBack && self.props.modalCloseCallBack();
					}
				}
			});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseBusinessCustomerModal");
			this.$searchForm = $(".searchForm", this.$chooseModal);
			this.$dataListTpl = $("#chooseBusinessCustomerModalDataListTpl").html();
			this.$dataListTable = $("#chooseBusinessCustomerModalDataListTable");
			this.$ownerName = $("#ownerName");
			this.$owner = $("#owner");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.searchData();

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("[name=ownerSubCompanyId]", self.$chooseModal),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm, function(){
				self.searchData();
			});

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);

				self.$ownerName.val(rowData.ownerName);
				self.$owner.val(rowData.owner);
			});

			self.render({});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				isDisabled:0,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('customer/pageCustomerCompany', searchData, '查询企业客户', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowData:function() {
						return JSON.stringify(this);
					},
					"cutomerTypeValue":function() {
						return Enum.customerType.getValue(this.customerType);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					addressForamt:function() {
						return (this.customerCompany.provinceName || '') + (this.customerCompany.cityName  || '') + (this.customerCompany.districtName  || '') + (this.customerCompany.address  || '');
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function(rowData) {
			var self = this;
			self.props.hasOwnProperty('callBack') && self.props.callBack(rowData);	
			if(this.props.batch) {
				self.confirm();
			} else {
				Rental.modal.close();
			}
		},
		confirm:function() {
            var self = this;
            bootbox.confirm({
				message: "成功选择客户",
				buttons: {
					confirm: {
						label: '完成',
						className: 'btn-primary'
					},
					cancel: {
						label: '继续选择',
						className: 'btn-default'
					}
				},
				callback: function (result) {
					if(result) {
						Rental.modal.close();
					} 
				}
			});
        }
	};

	window.ChooseBusinessCustomer = ChooseBusinessCustomer;

})(jQuery)