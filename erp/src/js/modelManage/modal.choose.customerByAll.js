/**
 * decription: 选择客户（所有）
 * author:zhangsheng
 * time: 2018-3-27
 */
;(function($) {

	var ChooseCustomerByAll = {
		init:function(prams) {
			this.props = _.extend({
				chooseCustomerList:new Array(),
				batch:false,
                callBack:function() {},
                complete:function() {},
				modalCloseCallBack:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "all-customer-modal/choose", type:'ajax', 
				ajaxContentAdded:function(pModal) {
					self.initDom();
					self.initEvent();	
				},
				afterClose:function() {
					self.props.modalCloseCallBack && self.props.modalCloseCallBack();
				}
			});
		},
		initDom:function() {
            this.$chooseModal = $("#chooseCustomerModal");
            this.$businessCustomerModal = $("#customerBusiness");
            this.$businessCustomerSearchForm = $(".businessCustomerSearchForm", this.$businessCustomerModal);
            this.$personalCustomerModal = $("#customerPersonal");
            this.$personalCustomerSearchForm = $(".personalCustomerSearchForm", this.$personalCustomerModal);
			this.$businessCustomerListTpl = $("#chooseBusinessCustomerTpl").html();
            this.$businessCustomerListTable = $("#chooseBusinessCustomerTable");
            this.$personalCustomerListTpl = $("#chooseCustomerPersonalTpl").html();
			this.$personalCustomerListTable = $("#chooseCustomerPersonalTable");

            this.businessPager = new Pager();
			this.personalPager = new Pager();
		},
		initEvent:function() {
			var self = this;

			ApiData.company({
				success:function(response) {
					Rental.ui.renderSelect({
						container:$("[name=ownerSubCompanyId]", self.$chooseModal),
						data:response,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {
							self.searchBusinessCustomerData();
							self.searchPersonalCustomerData();
						},
						defaultText:'请选择客户所属公司'
					});
				}
			});

            //初始化客户
            self.searchBusinessCustomerData();
            self.searchPersonalCustomerData();

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$businessCustomerSearchForm, function(){
				self.searchBusinessCustomerData();
            });
            Rental.form.initSearchFormValidation(this.$personalCustomerSearchForm, function(){
				self.searchPersonalCustomerData();
			});

			self.$chooseModal.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});

            self.renderBusinessCustomer({});
            self.renderPersonalCustomer({});
        },
        //企业客户
		searchBusinessCustomerData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.businessPager.defautPrams.pageSize,
				isDisabled:0,
			}, Rental.form.getFormData(self.$businessCustomerSearchForm) || {}, prams || {});

			Rental.ajax.ajaxData('customer/pageCustomerCompany', searchData, '查询企业客户列表失败', function(response) {
				self.renderBusinessCustomer(response.resultMap.data);	
			});
		},
		doBusinessCustomerSearch:function(pageNo) {
			this.searchBusinessCustomerData({pageNo:pageNo || 1});
		},
		renderBusinessCustomer:function(data) {
			var self = this;
			self.renderBusinessCustomerList(data);
			self.businessPager.init(data,function(pageNo) {
				self.doBusinessCustomerSearch(pageNo);
			}, $('.pagerContainer',self.$businessCustomerModal));
		},
		renderBusinessCustomerList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowData:function() {
						return !!this && _.isObject(this) && JSON.stringify(this);
					},
					"cutomerTypeValue":function() {
						return Enum.customerType.getValue(this.customerType);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					addressForamt:function() {
						return (this.customerCompany.provinceName || '') + (this.customerCompany.cityName  || '') + (this.customerCompany.districtName  || '') + (this.customerCompany.address  || '');
					}
				})
			}

			Mustache.parse(this.$businessCustomerListTpl);
			this.$businessCustomerListTable.html(Mustache.render(this.$businessCustomerListTpl, data));
        },
        //个人客户
        searchPersonalCustomerData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.personalPager.defautPrams.pageSize,
				isDisabled:0,
			}, Rental.form.getFormData(self.$personalCustomerSearchForm) || {}, prams || {});

			Rental.ajax.ajaxData('customer/pageCustomerPerson', searchData, '查询个人客户列表失败', function(response) {
				self.renderPersonalCustomer(response.resultMap.data);	
			});
		},
		doPersonalCustomerSearch:function(pageNo) {
			this.searchPersonalCustomerData({pageNo:pageNo || 1});
		},
		renderPersonalCustomer:function(data) {
			var self = this;
			self.renderPersonalCustomerList(data);
			self.personalPager.init(data,function(pageNo) {
				self.doPersonalCustomerSearch(pageNo);
			}, $('.pagerContainer',self.$personalCustomerModal));
		},
		renderPersonalCustomerList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowData:function() {
						return !!this && _.isObject(this) && JSON.stringify(this);
					},
					"cutomerTypeValue":function() {
						return Enum.customerType.getValue(this.customerType);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					addressForamt:function() {
						return (this.customerPerson.provinceName || '') + (this.customerPerson.cityName  || '') + (this.customerPerson.districtName  || '') + (this.customerPerson.address  || '');
					}
				})
			}

			Mustache.parse(this.$personalCustomerListTpl);
			this.$personalCustomerListTable.html(Mustache.render(this.$personalCustomerListTpl, data));
		},
		choose:function(rowData) {
			var self = this;
			self.props.hasOwnProperty('callBack') && self.props.callBack(rowData);	
			if(this.props.batch) {
				self.confirm();
			} else {
				Rental.modal.close();
			}
        },
        confirm:function() {
            var self = this;
            bootbox.confirm({
				message: "成功选择客户",
				buttons: {
					confirm: {
						label: '完成',
						className: 'btn-primary'
					},
					cancel: {
						label: '继续选择',
						className: 'btn-default'
					}
				},
				callback: function (result) {
					if(result) {
						self.props.complete &&  self.props.complete();
					} 
				}
			});
        }
	};

	window.ChooseCustomerByAll = ChooseCustomerByAll;

})(jQuery)