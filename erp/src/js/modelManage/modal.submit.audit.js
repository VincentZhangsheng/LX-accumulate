// 提交审核
;(function($) {

	var SubmitAudit = {
		init:function(prams) {
			this.props = _.extend({
				workflowType:null,
				workflowReferNo:null,
				verifyAttachment:true,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "submit-audit/modal", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#submitAuditForm");
			this.$cancelButton = $('.cancelButton', this.$form);
		},
		initEvent:function() {
			var self = this;
			
			// if(self.props.lastStep) {
			// 	$('[name=nextVerifyUser]', this.$form).prop({disabled:true}).closest('.section').addClass('hide');
			// } else {
			// 	self.queryNextVerifyUsers();	
			// }

			Rental.ui.events.initDropzone($('#auditImgIdList'), 'image/upload');

			self.queryNextVerifyUsers();
			
			Rental.form.initFormValidation(this.$form, function(form){
				self.submit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		queryNextVerifyUsers:function(prams) {
			var self = this;
			var commitData = {
				workflowType:self.props.workflowType,
				workflowReferNo:self.props.workflowReferNo
			};
			Rental.ajax.submit("{0}workflow/queryNextVerifyUsers".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.renderNextVerifyUserSelect(response.resultMap.data);	
				} else {
					Rental.notification.error("查询审核人信息",response.description || '查询失败');
				}
			}, null ,"查询审核人信息",'listLoading');
		},
		renderNextVerifyUserSelect:function(data) {
			var input = $('[name=verifyUser]', this.$form);
			if(!(_.isArray(data) && data.length > 0)) {
				input.hide();
				input.closest('.section').hide();
				return;
			}
			var opt = '<option value="{0}">{1}</option>';
			var opts = (data || []).map(function(item) {
				return opt.format(item.userId, item.realName);
			});
			opts.unshift(opt.format('','请选择审核人'));
			$('[name=verifyUser]', this.$form).html(opts.join(''));
		},
		submit:function() {
			try {
				var self = this;
				var formData = Rental.form.getFormData(self.$form);
				var imgIdList = Rental.ui.events.getImageList($('#auditImgIdList'));
				if(_.isArray(imgIdList)) {
					formData.imgIdList = _.pluck(imgIdList, 'imgId');
				}
				if(self.props.verifyAttachment && !(_.isArray(formData.imgIdList) && formData.imgIdList.length > 0)) {
					bootbox.alert('请上传附件');
					return;
				}
				self.callBackFunc(formData);				
			} catch (e) {
				Rental.notification.error("提交审核失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			// Rental.modal.close();
			this.props.hasOwnProperty('callBack') && this.props.callBack(response);
		},
	};

	window.SubmitAudit = SubmitAudit;

})(jQuery)