;(function($) {

	var ChooseRentProduct = {
		init:function(prams) {
			this.props = _.extend({
				callBack:function() {},
				customerNo:null,
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-rent-product-modal/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseProductModal");
			this.$searchForm = $("#chooseProductSearchForm");
			this.$dataListTpl = $("#chooseProductModalDataListTpl").html();
			this.$dataListTable = $("#chooseProductDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $this =  $(this), productId = $this.data('productid');
				$('.productDetial', $this.closest('tr').next('tr')).slideToggle('fast');
			});

			self.$dataListTable.on('click', '.cancelChoose', function(event) {
				event.preventDefault();
				$(this).closest('.productDetial').slideUp();
			});

			self.$dataListTable.on('click', '.confirmChoose', function(event) {
				event.preventDefault();
				self.confirm($(this));
			});
		},
		searchData:function(prams) {
			var self = this;
			if(!self.props.customerNo) {
				bootbox.alert('没找到用户编号');
				return;
			}
			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
				customerNo:self.props.customerNo,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}customerOrder/pageRentProduct".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询用户可退换商品列表",response.description);
				}
			}, null , "查询用户可退换商品列表", 'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer', this.$chooseModal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:{
					"listData":listData,
					"listPriceFixed":function() {
						return !!this.listPrice ? this.listPrice.toFixed(2) : "0.00";
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.productImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.productImgList);
					},
					isRentStr:function() {
						return this.isRent == 1 ? '在租':"下架";
					},
					productJSONString:function() {
						return JSON.stringify(this);	
					},
					skuListSource:function() {
						var skuList = this.productSkuList;
						return {
							skuList:skuList,
							propertiesToStr:function() {
								var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
									return item.propertyValueName;
								}).join(" | ") : '';
								return str;
							},
							rowKey:function() {
								return parseInt(Math.random()*(100+1),10); //0-100随机数
							}
						}
					}
				}
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		confirm:function($confirmButton) {
			var $productDetial = $confirmButton.closest('.productDetial');
			var product = $productDetial.data('product');
			var checkedSku = $('[name=sku]',$productDetial).filter(':checked');
			if(checkedSku.length == 0) {
				bootbox.alert("请选择SKU");
				return false;
			}
			var skuList = new Array();
			checkedSku.each(function(i,item) {
				var skuId = $(item).val();
				var sku = product.productSkuList.filter(function(filterItem) {
					return filterItem.skuId == skuId;
				})[0];
				skuList.push(sku);
			});
			product.chooseProductSkuList = skuList;

			this.props.callBack && this.props.callBack(product);
		}
	};

	window.ChooseRentProduct = ChooseRentProduct;

})(jQuery)