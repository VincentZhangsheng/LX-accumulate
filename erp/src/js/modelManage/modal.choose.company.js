;(function($) {

	var ChooseCompany = {
		init:function(prams) {
			this.props = _.extend({
				batch:false,
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "company-modal/chooseCompany", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseCompanyModal");
			this.$searchForm = $(".searchForm", this.$chooseModal);
			this.$dataListTpl = $("#chooseCompanyDataListTpl").html();
			this.$dataListTable = $("#chooseCompanyDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.searchData();

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm, function(){
				self.searchData();
			});

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});

			self.render({});
		},
		searchData:function(prams) {
			var self = this;

			self.commitData = _.extend(self.commitData || {}, {
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}company/pageSubCompany".format(SitePath.service),self.commitData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询用户信息",response.description || '查询失败');
				}
			}, null ,"查询用户信息",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data  = {
				dataSource:{
					"listData":listData,
					rowData:function() {
						return JSON.stringify(this);
					},
					subCompanyTypeValue:function() {
						return Enum.companyType.getValue(this.subCompanyType);
					},
					address:function() {
						return '{0}{1}{2}'.format(this.provinceName,this.cityName,this.districtName);
					}
				}
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl,data));
		},
		choose:function(rowData) {
			var self = this;
			self.props.hasOwnProperty('callBack') && self.props.callBack(rowData);	
			if(!this.props.batch) {
				Rental.notification.success("选择客户", '成功');
				Rental.modal.close();
			}
		}
	};

	window.ChooseCompany = ChooseCompany;

})(jQuery)