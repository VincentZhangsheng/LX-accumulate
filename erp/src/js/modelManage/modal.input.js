;(function($) {

	var InputModal = {
		init:function(prams) {
			this.props = _.extend({
				title:'输入信息',
				url:null,
				initFunc:function() {},
				callBack:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + self.props.url, type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();
				self.props.initFunc(pModal.container);	
			}});
		},
		initDom:function(pModal) {
			this.$modal = pModal.container;
			this.$form = $(".inputForm", this.$modal);
			this.$cancelButton = $('.cancelButton', this.$form);
			$('.modalTitle', this.$modal).html(this.props.title);
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(this.$form, function(form){
				self.submit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		submit:function() {
			try {
				this.callBackFunc(Rental.form.getFormData(this.$form));
			} catch (e) {
				Rental.notification.error("系统错误", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			if(this.props.hasOwnProperty('callBack') && this.props.callBack(response, this.$modal)) {
				Rental.modal.close();
			}
		},
	};

	window.InputModal = InputModal;

})(jQuery)