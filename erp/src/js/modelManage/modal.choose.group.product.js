;(function($) {
	var ChooseGroupProduct = {
		init:function(callBack) {
			this.props = {
				jointProductId:null, //商品ID,用户过滤商品信息
				callBack:callBack,
			};
			this.show();
		},
		modal:function(prams) {
			this.props = _.extend({
				jointProductId:null,
				callBack:function() {}
			}, prams || {});
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "group-product/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();	
			}});
		},
		initDom:function(modal) {
            this.$groupProductModal = $("#chooseGroupProduct");
            this.$searchForm = $("#groupProductSearchForm");
			this.$groupProductListTpl = $("#groupProductListTpl").html();
			this.$groupProductListTable = $("#groupProductListTable");

			this.pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			//绑定查询事件
            Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchGroupProduct();
			});

			self.searchGroupProduct();

			self.$groupProductListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
                var jointProductId = $(this).data('id');
                self.chooseGroupProuduct($(this),jointProductId);
				// self.props.hasOwnProperty('callBack') && self.props.callBack(groupProduct);
			});

			self.$groupProductListTable.on('click', '.confirmChoose', function(event) {
				event.preventDefault();
				self.confirmChoose($(this));
			});

			self.$groupProductListTable.on('click', '.cancelChoose', function(event) {
				event.preventDefault();
				$(this).closest('.groupDetial').slideUp();
			});
        },
        //组合商品
		searchGroupProduct:function(prams) {
			var self = this;

			var searchData = _.extend({
				pageNo:1,
				pageSize:self.pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('jointProduct/page', searchData, '查询组合商品列表失败', function(response) {
				self.renderGroupProduct(response.resultMap.data);
			});
		},
		doGroupSearch:function(pageNo) {
			this.searchGroupProduct({pageNo:pageNo || 1});
		},
		renderGroupProduct:function(data) {
			var self = this;
			self.renderGroupProductList(data);
			self.pager.init(data, function(pageNo) {
				self.doGroupSearch(pageNo);
			}, $('.pagerContainer', self.$groupProductModal));
		},
		renderGroupProductList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];
			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					productJSONString:function() {
						return JSON.stringify(this);
					},
					dataStatusValue:function() {
                        switch(this.dataStatus) {
                            case 0:
                                return "不可用";
                            case 1:
                                return "可用";
                            case 2:
                                return "删除";
                            default:
                                return "";
                        }
					},
					groupedProductTostr:function() {
						var str = this.hasOwnProperty('jointProductProductList') ? this.jointProductProductList.map(function(item) {
							return item.hasOwnProperty("product") ? (item.product.hasOwnProperty("productName") ? item.product.productName : "") : ""
						}).join(' | ') : ''; 
						return str;
					},
					groupedMaterialTostr:function() {
						var str = this.hasOwnProperty('jointMaterialList') ? this.jointMaterialList.map(function(item) {
							return item.hasOwnProperty("material") ? (item.material.hasOwnProperty("materialName") ? item.material.materialName : "") : ""
						}).join(' | ') : '';
						return str;
					}
				})
			}
			Mustache.parse(this.$groupProductListTpl);
			this.$groupProductListTable.html(Mustache.render(this.$groupProductListTpl, data));
		},
		chooseGroupProuduct:function($button,id) {
			var detailTr =  $button.closest('tr').next('tr'), groupDetial = $('.groupDetial',detailTr);
			if(groupDetial.size() > 0) {
				groupDetial.slideDown(); 
			} else {
				this.getGroupDetail($('td',detailTr),id);	
			}
        },
        getGroupDetail:function(container,id) {
			var self = this;
			Rental.ajax.ajaxData('jointProduct/query', {jointProductId:id}, '加载组合商品详细信息', function(response) {
				self.renderRowGroupProduct(container, response.resultMap.data);
			});
        },
        renderRowGroupProduct:function(container,group) {
			var self = this;
			var data  = {
				group:group,
				groupJSONString:function() {
					return JSON.stringify(this);
				},
				groupMaterialJSONString:function() {
					return JSON.stringify(this.jointMaterialList);
				},
				groupProduct:function() {
					var productList = this.jointProductProductList;
					return _.extend(Rental.render, {
						productList:productList,
						productJSONString:function() {
							return JSON.stringify(this);
						},
						serialNumber:function() {
							return Rental.helper.randomString(8);
						},
						skuListSource:function() {
							var skuList = this.product.productSkuList;
							return _.extend(Rental.render, {
								skuList:skuList,
								propertiesToStr:function() {
									var str = this.hasOwnProperty('productSkuPropertyList') ? this.productSkuPropertyList.map(function(item) {
										return item.propertyValueName;
									}).join(" | ") : '';
									return str;
								}
							}) 
						}
					})
				}
			}
			var tpl = $('#groupProductDetailTpl').html();
			Mustache.parse(tpl);
			container.html(Mustache.render(tpl, data));
		},
		confirmChoose:function($confirmButton) {
			var $groupDetial = $confirmButton.closest('.groupDetial');
			var group = _.extend({}, $groupDetial.data('group'));
			var groupMaterial = $groupDetial.data('material');
			var skuRow = $('.skuRow',$groupDetial);
			var isNewGroup = $groupDetial.parents("tr").prev().find('.newGroup').prop('checked');
			var isOldGroup = $groupDetial.parents("tr").prev().find('.oldGroup').prop('checked');

			if(!isNewGroup && !isOldGroup) {
				bootbox.alert("请选择组合商品新旧属性");
				return false;
			}

			var chooseProductList = new Array();
			skuRow.each(function() {
				var checkedSku = $('[name=sku]',$(this)).filter(':checked');
				var jointProduct = $(this).data("product");
				var product = jointProduct.hasOwnProperty("product") ? jointProduct.product : {};
				var skuList = new Array();
				
				checkedSku.each(function(i,item) {
					var skuId = $(item).val(),
						sku = product.productSkuList.filter(function(filterItem) {
							return filterItem.skuId == skuId;
						})[0];
	
					var newSku = _.extend({},product, sku);
					if(isNewGroup) {
						newSku.isNewProduct = 1;
					}
					if(isOldGroup) {
						newSku.isNewProduct = 0;
					}
					skuList.push(newSku);
				});
				product.chooseProductSkuList = skuList;
				jointProduct = _.extend({}, jointProduct, product);

				chooseProductList.push(jointProduct);
			})

			chooseProductList = chooseProductList.filter(function(item) {
				return item.chooseProductSkuList.length == 1;
			})
			if(chooseProductList.length != group.jointProductProductList.length) {
				bootbox.alert("每个商品项请选择一条SKU");
				return false;
			}

			group.chooseGroupProductList = chooseProductList;
			group.tabIndex = 0;
			group.chooseMaterialList = new Array();

			groupMaterial.forEach(function(item) {
				if(isNewGroup) {
					item.isNewMaterial = 1;
				}
				if(isOldGroup) {
					item.isNewMaterial = 0;
				}
				item.materialName = item.hasOwnProperty("material") ? (item.material.hasOwnProperty("materialName") ? item.material.materialName : "") : "";
				group.chooseMaterialList.push(_.extend({},item));
			})
			if(isNewGroup) {
				group.isNew = 1;
			}
			if(isOldGroup) {
				group.isNew = 0;
			}
			this.props.callBack && this.props.callBack(_.extend({}, group));
		},
	};

	window.ChooseGroupProduct = ChooseGroupProduct;

})(jQuery)