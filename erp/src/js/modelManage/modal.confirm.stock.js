;(function($) {

	var confirmProductStock = {
		init:function(prams) {
			this.props = _.extend({
                isChooseProduct:false,
				productName:null,
				k3ProductNo:null,
                callBack:function() {},
                complete:function() {},
                continueFunc:function() {},
                changeContinueFunc:function() {},
				modalCloseCallBack:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "order-manage/confirm-stock", type:'ajax', 
				ajaxContentAdded:function(pModal) {
                    self.initAuthor();
					self.initDom();
					self.initEvent();	
				},
				afterClose:function() {
                    if(!!self.props.isChooseProduct) {
                        self.props.changeContinueFunc = function() {
                            self.props.modalCloseCallBack && self.props.modalCloseCallBack();
                            _.isFunction(self.props.continueFunc) && self.props.continueFunc();
                        }
                        self.props.changeContinueFunc();
                    }
				}
			});
        },
        initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_product];
            this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_product_list];
            this.modalPageAuthor = this.currentPageAuthor.children[AuthorCode.manage_product_stock];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.rowActionButtons = new Array();

			if(this.modalPageAuthor.hasOwnProperty('children') == false) return;

			var confirmStock = this.modalPageAuthor.children[AuthorCode.manage_product_confirm_stock],
				queryStock = this.modalPageAuthor.children[AuthorCode.manage_product_query_stock];

            confirmStock && this.rowActionButtons.push(AuthorUtil.button(confirmStock,'confirmBtn','btn-primary','确认库存'));
            queryStock && this.rowActionButtons.push(AuthorUtil.button(queryStock,'queryBtn','btn-default','查询库存'));
		},
		initDom:function() {
            this.$modal = $("#confirmStockModal");
            this.$modalForm = $("#modalForm");
		},
		initEvent:function() {
			var self = this;

            self.initProductInfo();
            self.renderStockButton();

			ApiData.company({
				success:function(response) {

                    var companyList = response.filter(function(company) {
						return company.subCompanyId != Enum.subCompany.num.telemarketing && company.subCompanyId != Enum.subCompany.num.channelBigCustomer;
                    });
                    
					Rental.ui.renderSelect({
						container:$("[name=subCompanyId]", self.$chooseModal),
						data:companyList,
						func:function(opt, item) {
							return opt.format(item.subCompanyId, item.subCompanyName);
						},
						change:function(val) {},
						defaultText:'请选择分公司'
					});
				}
            });

            Rental.helper.onOnlyNumber(self.$modalForm, '.productCount');
            
            $("#confirmBtn").on("click",function(event) {
                event.preventDefault();
                self.confirmStock();
            })

            $("#queryBtn").on("click",function(event) {
                event.preventDefault();
                self.queryStock();
            })

            $("#cancelButton").on("click",function(event) {
                event.preventDefault();
                Rental.modal.close();
            })
        },
        renderStockButton:function() {
			var self = this;
			var stockButtonsTpl = $('#stockuttonsTpl').html();
			Mustache.parse(stockButtonsTpl);
			$("#stockButtons").html(Mustache.render(stockButtonsTpl,{'stockButtons':self.rowActionButtons}));
		},
        initProductInfo:function() {
            if(!!this.props.productName) {
                $("[name=productName]",this.$modalForm).val(this.props.productName)
            }
            if(!!this.props.k3ProductNo) {
                $("[name=k3Code]",this.$modalForm).val(this.props.k3ProductNo)
            }
        },
        confirmStock:function(){
            var self = this;
            if(!$("#querySection").hasClass("hide")) {
                $("#querySection").addClass("hide")
            }
            if(!$("#confirmSection").hasClass("hide")) {
                $("#confirmSection").addClass("hide")
            }
            var formData = Rental.form.getFormData(self.$modalForm);
            if(!formData["k3Code"]) {
                bootbox.alert('商品编号不能为空');
				return;
            }
            if(!formData["productCount"]) {
                bootbox.alert('请输入客户需求');
				return;
            }
            var commitData = _.extend(formData,{
                queryType:1,
                warehouseType:1
            })
            Rental.ajax.submit("{0}k3/queryK3Stock".format(SitePath.service),commitData,function(response){
				if(response.success) {
                    self.renderStockList(response.resultMap.data,$("#confirmStockTpl").html(),$("#confirmStockTable"))
				} else {
					Rental.notification.error("确认库存",response.description || '失败');
				}
			}, null ,"确认库存",'dialogLoading');
        },
        queryStock:function() {
            var self = this;
            if(!$("#confirmSection").hasClass("hide")) {
                $("#confirmSection").addClass("hide")
            }
            if(!$("#querySection").hasClass("hide")) {
                $("#querySection").addClass("hide")
            }
            var formData = Rental.form.getFormData(self.$modalForm);
            if(!formData["k3Code"]) {
                bootbox.alert('商品编号不能为空');
				return;
            }
            var commitData = _.extend(formData,{
                queryType:0,
                warehouseType:1
            })
            Rental.ajax.submit("{0}k3/queryK3Stock".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.renderStockList(response.resultMap.data,$("#productStockTpl").html(),$("#productStockTable"))
				} else {
					Rental.notification.error("查询库存",response.description || '失败');
				}
			}, null ,"查询库存",'dialogLoading');
        },
        renderStockList:function(StockData,tpl,listTable) {
            var self = this;
            listTable.closest(".panel-body").removeClass("hide")
            var data = {
                dataSource:_.extend(Rental.render, {
					"listData":StockData,
					isEnough:function() {
						return this.isStockEnough == 1 ? "需求库存充足" : (this.isStockEnough == 0 ? "需求库存不足" : "")
                    },
                    isEnoughClass:function() {
                        return this.isStockEnough == 1 ? "text-success" : (this.isStockEnough == 0 ? "text-danger" : "")
                    },
				})
            }
            Mustache.parse(tpl);
			listTable.html(Mustache.render(tpl, data));
        },
	};

	window.confirmProductStock = confirmProductStock;

})(jQuery)