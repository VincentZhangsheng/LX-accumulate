;(function($) {

	var SupplierChoose = {
		init:function(prams) {
			this.props = _.extend({
				callBack:function() {}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "supplier/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseWarehouseModal = $("#chooseWarehouseModal");
			this.$searchForm = $("#chooseSupplierSearchForm");
			this.$dataListTpl = $("#chooseSupplierModalDataListTpl").html();
			this.$dataListTable = $("#chooseSupplierModalDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(SupplierChoose.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}supplier/getSupplier".format(SitePath.service),searchData,function(response){
				if(response.success) {
					SupplierChoose.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载供应商",response.description || '失败');
				}
			}, null ,"加载供应商",'listLoading');
		},
		doSearch:function(pageNo) {
			SupplierChoose.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSearch,$('.pagerContainer',this.$chooseWarehouseModal));
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				data = {
				dataSource:{
					"listData":listData,
					"rowData":function() {
						return JSON.stringify(this);
					}
				}
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function(rowData) {
			_.isFunction(this.props.callBack) && this.props.callBack(rowData);
			Rental.modal.close();
		}
	};

	window.SupplierChoose = SupplierChoose;

})(jQuery)