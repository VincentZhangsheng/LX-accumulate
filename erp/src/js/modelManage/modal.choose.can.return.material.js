;(function($) {

	var ChooseCanReturnMaterial = {
		init:function(prams) {
			var self = this;
			
			self.props = new Object();
			
			self.props = $.extend({
				btach:false, //是否是批量
				searchParms:{},
				callBack:function() {}
			}, prams);

			self.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-can-return-material-modal/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseMaterialModal");
			this.$searchForm = $("#chooseMaterialSearchForm");
			this.$dataListTpl = $("#chooseMaterialModalDataListTpl").html();
			this.$dataListTable = $("#chooseMaterialDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var material = $(this).data('material');
				self.chooseMaterial(material);
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize:this.Pager.defautPrams.pageSize,
			}, self.props.searchParms || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}customerOrder/pageRentMaterialCanReturn".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询客户可退配件列表",response.description || '失败');
				}
			}, null ,"查询客户可退配件列表",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			//var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					materialJSONString:function() {
						return JSON.stringify(this);
					},
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					"mainImg":function() {
						return Rental.helper.mainImgUrlFormat(this.materialImgList);
					},
					'productImgJSON':function() {
						return Rental.helper.imgListToJSONStringify(this.materialImgList);
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		chooseMaterial:function(material) {
			if(material.hasOwnProperty('canProcessCount') && (parseInt(material.canProcessCount) <= 0)) {
				bootbox.alert('可退数量必须大于0');
				return;
			}
			this.props.hasOwnProperty('callBack') &&this.props.callBack(material);
			!this.props.btach && Rental.modal.close();
		}
	};

	window.ChooseCanReturnMaterial = ChooseCanReturnMaterial;

})(jQuery)