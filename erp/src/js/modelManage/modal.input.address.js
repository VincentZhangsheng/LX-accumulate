;(function($){
	
	var InputAddress = {
		init:function(prams) {
			this.props = _.extend({
				showIsMain: false,
				initFunc:function() {},
				callBack:function() {},
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "address-modal/input", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
				self.props.initFunc(pModal.container);	
			}});
		},
		initDom:function() {
			var self = this;

			this.$form = $("#inputAddressForm");
			this.$cancelButton = $('.cancelButton', this.$form);

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});

			if(self.props.showIsMain) {
				$("#iaMainLabel").removeClass('hide');
			}
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		add:function() {
			try {
				var formData = Rental.form.getFormData(this.$form);
				formData.provinceName = $('option[value='+formData['province']+']', $('[name=province]', this.$form)).html();
				formData.cityName =formData['city'] ? $('option[value='+formData['city']+']', $('[name=city]', this.$form)).html() : "";
				formData.districtName =formData['district'] ? $('option[value='+formData['district']+']', $('[name=district]', this.$form)).html() : "";
				formData.isMain = formData.hasOwnProperty('isMain') && parseInt(formData.isMain) == 1 ? 1 : 0;
				this.callBackFunc(formData)
			} catch (e) {
				Rental.notification.error(tip, '输入地址' +  '<br />' + e );
			}
		},
		callBackFunc:function(address) {
			
			if(this.props.hasOwnProperty('callBack') && this.props.callBack(address)) {
				Rental.notification.success("选择客户地址", '成功');
				Rental.modal.close();	
			} else {
				Rental.notification.success("选择客户地址", '失败');
			}
		}
	};

	window.InputAddress = InputAddress;

})(jQuery);