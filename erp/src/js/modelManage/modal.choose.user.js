;(function($) {

	var ChooseUser = {
		init:function(prams) {
			this.props = _.extend({
				batch:false,
				callBack:function() {},
				complate:function(){}
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "user-modal/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseUserModal");
			this.$searchForm = $(".searchForm", this.$chooseModal);
			this.$dataListTpl = $("#chooseUserDataListTpl").html();
			this.$dataListTable = $("#chooseUserDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			self.searchData();

			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm, function(){
				self.searchData();
			});

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});

			self.render({});
		},
		searchData:function(prams) {
			var self = this;

			self.commitData = _.extend(self.commitData || {}, {
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
				isDisabled:0,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}/user/page".format(SitePath.service),self.commitData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询用户信息",response.description || '查询失败');
				}
			}, null ,"查询用户信息",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',this.$chooseModal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data  = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					rowData:function() {
						return JSON.stringify(this);
					},
					roleListData:function() {
						return this.roleList.map(function(item){
							return {
								role:item.subCompanyName + " " + item.departmentName + " " + item.roleName,
							}
						})
					},
					department:function() {
						return (this.roleList || []).map(function(item){
							return item.departmentName;
						}).join(', ');
					},
					role:function() {
						return (this.roleList || []).map(function(item){
							return item.roleName;
						}).join(', ');
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl,data));
		},
		choose:function(rowData) {
			var self = this;
			self.props.hasOwnProperty('callBack') && self.props.callBack(rowData);	
			if(this.props.batch) {
				// Rental.notification.success("选择客户", '成功');
				self.confirm();
			} else {
				Rental.modal.close();
			}
		},
		confirm:function() {
			bootbox.confirm({
				message: "成功选择客户",
				buttons: {
					confirm: {
						label: '完成',
						className: 'btn-primary'
					},
					cancel: {
						label: '继续选择',
						className: 'btn-default'
					}
				},
				callback: function (result) {
					if(result) {
						self.props.complate &&  self.props.complate();
					} 
				}
			});
		}
	};

	window.ChooseUser = ChooseUser;

})(jQuery)