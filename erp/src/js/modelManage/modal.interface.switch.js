/**
 * decription: 功能开关列表
 * author:zhangsheng
 * time: 2018-4-10
 */
;(function($) {

	var SwitchModal = {
		init:function(prams) {
			this.props = _.extend({
                title:'添加开关',
                showIsOpen:false,
				initFunc:function() {},
				callBack:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "system-manage/switch-modal", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom(pModal);
				self.initEvent();
				self.props.initFunc($("#interfaceSwitchForm", this.$modal));	
			}});
		},
		initDom:function(pModal) {
			this.$modal = pModal.container;
            this.$form = $("#interfaceSwitchForm", this.$modal);
            this.isOpenRow = $("#isOpenRow", this.$modal);
			this.$cancelButton = $('.cancelButton', this.$form);
			$('.modalTitle', this.$modal).html(this.props.title);
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(self.$form, function(form){
				self.submit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
            });
            
            if(self.props.showIsOpen) {
                self.isOpenRow.removeClass("hidden");
            }
		},
		submit:function() {
			try {
				this.callBackFunc(Rental.form.getFormData(this.$form));
			} catch (e) {
				Rental.notification.error("系统错误", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			if(this.props.hasOwnProperty('callBack') && this.props.callBack(response, this.$modal)) {
				Rental.modal.close();
			}
		},
	};

	window.SwitchModal = SwitchModal;

})(jQuery)