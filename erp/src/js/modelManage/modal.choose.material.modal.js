;(function($) {

	var MaterialModalChoose = {
		init:function(searchPrams, callBack, continueFunc) {
			this.searchPrams = searchPrams;
			this.callBack = callBack;
			this.continueFunc = continueFunc;
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "material-modal/choose", type:'ajax', 
				ajaxContentAdded:function(pModal) {
					self.initDom(pModal);
					self.initEvent();	
				},
				afterClose:function() {
					_.isFunction(self.continueFunc) && self.continueFunc();
				}
			});
		},
		initDom:function(modal) {
			this.$modal = modal.container;
			this.$searchForm = $("#chooseMaterialModalSearchForm");
			this.$dataListTpl = $("#chooseMaterialModalDataListTpl").html();
			this.$dataListTable = $("#chooseMaterialModalDataListTable");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();

			//选择仓库
			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: this.Pager.defautPrams.pageSize,
			}, this.searchPrams || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}material/queryModel".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("加载配件列表",response.description || '失败');
				}
			}, null ,"加载配件列表",'listLoading');
		},
		doSearch:function(pageNo) {
			MaterialModalChoose.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			this.renderList(data);
			this.Pager.init(data, function(pageNo) {
				self.doSearch(pageNo);
			} ,$('.pagerContainer', this.$modal));
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:{
					"listData":listData,
					materialTypeStr:function() {
						return Enum.materialType.getValue(this.materialType);
					},
					rowData:function() {
						return JSON.stringify(this);
					}
				}
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function(rowData) {
			var self = this;
			this.hasOwnProperty('callBack') && this.callBack(rowData)
			Rental.modal.close();
		}
	};

	window.MaterialModalChoose = MaterialModalChoose;

})(jQuery)