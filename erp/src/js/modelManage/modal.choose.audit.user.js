;(function($) {

	var AuditUserChoose = {
		init:function(prams) {
			this.props = {
				workflowType:prams.workflowType,
				workflowReferNo:prams.workflowReferNo,
				callBack:prams.callBack
			}
			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "audit-user/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseAuditUserModal");
			this.$searchForm = $("#chooseAuditUserSearchForm");
			this.$dataListTpl = $("#chooseAuditUserModalDataListTpl").html();
			this.$dataListTable = $("#chooseAuditUserModalDataListTable");
		},
		initEvent:function() {
			var self = this;

			self.searchData();

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var $tr = $(this).closest('tr');
				var rowData = $tr.data('rowdata');
				self.choose(rowData);
			});

			self.render({});
		},
		searchData:function(prams) {
			var self = this;
			var commitData = {
				workflowType:self.props.workflowType,
				workflowReferNo:self.props.workflowReferNo
			};
			Rental.ajax.submit("{0}workflow/queryNextVerifyUsers".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询审核人信息",response.description || '查询失败');
				}
			}, null ,"查询审核人信息",'listLoading');
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
		},
		renderList:function(data) {
			var self = this;
			var listData = data || []
			var data  = {
				dataSource:{
					"listData":listData,
					"rowData":function() {
						return JSON.stringify(this);
					},
					"isActivatedStr":function() {
						return this.isActivated == 1 ? "是":"否";
					},
					"isDisabledStr":function() {
						return this.isDisabled == 1 ? "是":"否";
					},
					"registerTimeFormat":function() {
						return this.registerTime && (new Date(this.registerTime)).format("yyyy-MM-dd hh:mm:ss")
					},
					"lastLoginTimeFormat":function() {
						return this.lastLoginTime && (new Date(this.lastLoginTime)).format("yyyy-MM-dd hh:mm:ss")
					},
					roleListData:function() {
						return (this.roleList || []).map(function(item){
							return {
								role:item.subCompanyName + " " + item.departmentName + " " + item.roleName,
							}
						})
					},
					department:function() {
						return (this.roleList || []).map(function(item){
							return item.departmentName;
						}).join(', ');
					},
					role:function() {
						return (this.roleList || []).map(function(item){
							return item.roleName;
						}).join(', ');
					}
				}
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		choose:function(rowData) {
			var self = this;
			self.props.hasOwnProperty('callBack') && self.props.callBack(rowData);
		}
	};

	window.AuditUserChoose = AuditUserChoose;

})(jQuery)