;(function($) {

	var ChooseCustomerAddress = {
		init:function(prams) {
			this.props = _.extend({
				customerNo:null,
				callBack:function() {},
				renderComplete:function() {},
			}, prams || {});

			this.show();
		},
		show:function() {
			var self = this;
			Rental.modal.open({src:SitePath.base + "customer-address-modal/choose", type:'ajax', ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$chooseModal = $("#chooseCustomerAddressModal");
			this.$dataListTpl = $("#chooseCustomerAddressTpl").html();
			this.$dataListTable = $("#chooseCustomerAddressPannel");
		},
		initEvent:function() {
			var self = this;

			self.searchData();

			self.$dataListTable.on('click', '.chooseButton', function(event) {
				event.preventDefault();
				var address = $(this).data('address');
				self.choose(address);
			});

			self.renderList({});
		},
		searchData:function() {
			var self = this;
			var searchData = {
				pageNo:1,
				pageSize:100,
				customerNo:self.props.customerNo,
			};
			Rental.ajax.submit("{0}customer/pageCustomerConsignInfo".format(SitePath.service), searchData, function(response){
				if(response.success) {
					self.renderList(response.resultMap.data);	
				} else {
					Rental.notification.error("加载客户收货地址信息",response.description);
				}
			}, null ,"加载客户收货地址信息",'listLoading');
		},
		renderList:function(data) {
			var self = this;
			var data = {
				dataSource:{
					listData:data.itemList || [],
					addressJson:function() {
						return JSON.stringify(this);
					},
					addButton:function() {
						return self.props.addAuthor ? AuthorUtil.button(self.props.addAuthor,'addAddressButton','','添加') : null;
					},
					editButton:function() {
						return self.props.editAuthor ? AuthorUtil.button(self.props.editAuthor,'eidtAditressButton','','编辑') : null;
					},
					deleteAuthor:function() {
						return self.props.deleteAuthor ? AuthorUtil.button(self.props.deleteAuthor,'deleteAditressButton','','删除') : null;
					},
					isDefault:function() {
						return this.isMain == 1 ? true:false;
					},
					checkbox:function() {
						return self.props.checkbox;
					}
				}
			}
			Mustache.parse(self.$dataListTpl);
			self.$dataListTable.html(Mustache.render(self.$dataListTpl, data));
			self.props.renderComplete && self.props.renderComplete();
		},
		choose:function(address) {
			var self = this;
			if(self.props.hasOwnProperty('callBack') && self.props.callBack(address)) {
				Rental.notification.success("选择客户地址", '成功');
				Rental.modal.close();	
			} else {
				Rental.notification.success("选择客户地址", '失败');
			}
			
		}
	};

	window.ChooseCustomerAddress  = ChooseCustomerAddress ;

})(jQuery)