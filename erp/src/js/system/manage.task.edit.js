;(function($){
	
	var EditTaskManage = {
		state:{
			taskInfo:null,
			id:null,
			dateList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_task_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_task_list_edit];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			var self = this;
			this.$form = $("#editTaskForm");

			this.state.id = Rental.helper.getUrlPara('id');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_task_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.initCommonEvent();
		},
		loadData:function() {
			var self = this;
			if(!self.state.id) {
				bootbox.alert('没找到任务id');
				return;
			}
			Rental.ajax.ajaxData('taskScheduler/detailTaskExecutor', {id:self.state.id}, '获取定时任务详细信息', function(response) {
				self.state.taskInfo = response.resultMap.data;
				self.initFormData(response.resultMap.data);
				if(response.resultMap.data.hasOwnProperty("holidayDTOList")) {
					self.state.dateList = response.resultMap.data.holidayDTOList
				}
				self.renderDateList()
			});
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
			Rental.ui.renderFormData(self.$form, data);
			$("#triggerName,#triggerGroup").prop("disabled",true)
		},
		initProductList:function(data) {
			if(!data) return;
			var  orderItemsMapByProductId = data.reduce(function(pre,item) {
				var product =  _.extend(item, item.product, item.product.productSkuList[0]);
				if(!pre[item.productId]) {
					pre[item.productId] = {
						chooseProductSkuList:new Array()
					};
				}
				pre[item.productId].chooseProductSkuList.push(product);
				pre[item.productId] = _.extend(pre[item.productId], product);
		
				return pre;
			},{});

			var chooseProductList = _.values(orderItemsMapByProductId);
			return chooseProductList;
		},
		edit:function() {
			try {
				var self = this,
                    formData = Rental.form.getFormData(self.$form);

				if(!!formData['holidayName'] && self.state.dateList.length == 0) {
					bootbox.alert('请指定不需要指定定时任务的日期');
					return;
				}

				if(self.state.dateList.length > 0 && !formData['holidayName']) {
					bootbox.alert('请输入假期名称');
					return;
				}

				var commitData = {
					triggerName:self.state.taskInfo.triggerName,
					triggerGroup:self.state.taskInfo.triggerGroup,
					cronExpression:formData['cronExpression'],
					holidayName:formData['holidayName'],
					description:formData['description'],
					holidayDTOList:self.state.dateList
				}

				var des = '编辑定时任务';
				Rental.ajax.submit("{0}taskScheduler/updateTaskScheduler".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("编辑定时任务失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑定时任务",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } 
				    }
				});
			}
			return false;
		},
	};

	window.EditTaskManage = _.extend(EditTaskManage, TaskMixin);

})(jQuery);