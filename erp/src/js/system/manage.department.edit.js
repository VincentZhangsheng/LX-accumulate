;(function($){
	var EditDepartment = {
		init:function(prams) {
			this.props =_.extend({
				departmentId:null,
				callBack:function () {}
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "user-manage/departmentEdit", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#editDepartmentForm");		
			this.$cancelButton = $("#cancelEditDeparmentBtn");
		},
		initEvent:function() {
			var self = this;

			self.findDataByType();
			self.getDepartmentById();
			
			Rental.form.initFormValidation(this.$form,function(form) {
				self.edit();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		findDataByType:function() {
			var self = this;
			Rental.ajax.submit("{0}data/findDataByType".format(SitePath.service),{dataType:1},function(response){
				if(response.success) {
					self.renderDepartmentType(response.resultMap.data);
				} else {
					Rental.notification.error("加载部门类型",response.description || '失败');
				}
			}, null , "加载部门类型");			
		},
		renderDepartmentType:function(data) {
			var self = this;
			Rental.ui.renderSelect({
				data:data,
				container:$('[name=departmentType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.value, item.label);
				}
			});
		},
		getDepartmentById:function() {
			var self = this;
			if(!self.props.departmentId) {
				bootbox.alert('没有找到部门ID');
				return;
			}
			Rental.ajax.submit("{0}company/getDepartmentById".format(SitePath.service),{departmentId:self.props.departmentId},function(response){
				if(response.success) {
					self.initFormData(response.resultMap.data);
				} else {
					Rental.notification.error("加载部门详细",response.description || '失败');
				}
			}, null , "加载部门详细", 'listLoading');		
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
			var array = self.$form.serializeArray();  
			$.each(array,function(i,item){
				if(data.hasOwnProperty(item.name)) {
					var dom = self.$form.find("[name="+item.name+"]");
	            	dom.val(data[item.name]);
	            	dom.data('defaultvalue',data[item.name]);
				}
	        });
		},
		edit:function() {
			var self = this;

			var formData = Rental.form.getFormData(self.$form);

			var commmitData = {
				departmentId:self.props.departmentId,
				departmentName:formData['departmentName'],
				departmentType:formData['departmentType'],
				parentDepartmentId:formData['parentDepartmentId'],
				subCompanyId:formData['subCompanyId'],
			}

			if(!commmitData.departmentId) {
				bootbox.alert('找不到部门ID');
				return ;
			}

			Rental.ajax.submit("{0}company/updateDepartment".format(SitePath.service),commmitData,function(response){
				if(response.success) {
					Rental.notification.success("编辑部门",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("编辑部门",response.description || '失败');
				}
			}, null , "编辑部门", 'dialogLoading');
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack();
			}
			return false;
		},
	};
	window.EditDepartment = EditDepartment;
})(jQuery);