;(function($){
	
	var AddRole = {
		init:function(callBack) {
			this.calllBack = callBack;
			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/addRole", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#addRoleForm");
			this.$companyDepartmentTree = $("#companyDepartmentTree");
			this.$cancelAddRoleBtn = $("#cancelAddRoleBtn");
		},
		initEvent:function() {
			var self = this;

			self.getCompanyDepartmentTree();

			Rental.form.initFormValidation(this.$form,function(form){
				self.addRole();
			});

			self.$cancelAddRoleBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox:"radio",
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:1,
            });
			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getCompanyDepartmentTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/company/getCompanyDepartmentTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyDepartmentTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("加载组织架构",response.description || '查询失败');
				}
			}, null ,"加载组织架构");
		},
		initCompanyDepartmentTree:function(data) {
			var tree = this.toTree(data);
			this.initTree(this.$companyDepartmentTree,tree);
		},
		toTree:function(data) {

			var tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						expanded:true,
						//checkbox: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
					};
					tr.push(item);

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					checkbox: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.departmentId,
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		addRole:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);
			var roleTree = this.$companyDepartmentTree.fancytree('getTree');

			var departmentList = (roleTree.getSelectedNodes() || []).map(function(item){
				return {
					departmentId:item.key,
				};
			});

			if(departmentList.length == 0) {
				bootbox.alert("请选择部门");
				return false;
			}

			formData.departmentId = departmentList[0].departmentId;

			Rental.ajax.submit("{0}/userRole/add".format(SitePath.service),formData,function(response){
				if(response.success) {
					Rental.notification.success("添加角色",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("添加角色",response.description || '失败');
				}
			}, null ,"添加用户", 'dialogLoading');
		},
		callBackFunc:function(response) {
			if(response.success) {
				Rental.modal.close();
				AddRole.hasOwnProperty('calllBack') && AddRole.calllBack();
				AddRole.$form.find('.state-success').removeClass('state-success');
				AddRole.$form[0].reset();
			}
			return false;
		},
	};

	window.AddRole = AddRole;

})(jQuery);