;(function($) {

	var delayedTaskManage = {
		init:function() {
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;
			self.render(new Object());

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
            });
            
            Rental.ui.events.initRangeDatePicker($('#timePicker'), $('#timePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
            
            Rental.ui.renderSelect({
				container:$('#taskType'),
				data:Enum.array(Enum.taskType),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（任务类型）'
            });
            
            Rental.ui.renderSelect({
				container:$('#taskStatus'),
				data:Enum.array(Enum.taskStatus),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString());
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（任务状态）'
			});
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}delayedTask/pageDelayedTask".format(SitePath.service),searchData,function(response){
				if(response.success) {
					self.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询延时任务",response.description || '失败');
				}
			}, null ,"查询延时任务",'listLoading');
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;

			var user = Rental.localstorage.getUser();

			var data = {
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.workflowDetail, this.workflowLinkNo);
					},
					"rowActionButtons":self.rowActionButtons,
					taskTypeVal:function() {
						return Enum.taskType.getValue(this.taskType);
					},
					taskStatusVal:function() {
						return Enum.taskStatus.getValue(this.taskStatus);
					},
					progressRateVal:function() {
						return parseFloat(this.progressRate) * 100 + "%";
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		forwordReferNoForCustomer:function(workflowReferNo) {
			ApiData.customerDetail({
				customerNo:workflowReferNo,
				success:function(response) {
					var url = Enum.customerType.getUrl(response.customerType, workflowReferNo);
					window.location.href = url;
				}
			});
		}
	};

	window.delayedTaskManage = delayedTaskManage;

})(jQuery);