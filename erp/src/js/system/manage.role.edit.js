;(function($){
	
	var EditRole = {
		init:function(roleId,callBack) {
			this.calllBack = callBack;
			this.showModal(roleId);
		},
		showModal:function(roleId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/editRole", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.loadData(roleId);
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#editRoleForm");
			this.$companyDepartmentTree = $("#companyDepartmentTree");
			this.$cancelEditRoleBtn = $("#cancelEditRoleBtn");
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(this.$form,function(form){
				self.eidtRole();
			});

			self.$cancelEditRoleBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},	
		loadData:function(roleId) {
			var self = this;
			
			var commitData = {
				pageNo:1,
				pageSize:1,
				roleId:roleId,
			}

			console.log(commitData)

			Rental.ajax.submit("{0}/userRole/page".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.role = response.resultMap.data.itemList[0];
					self.initFormData(self.role)
					self.getCompanyDepartmentTree();
				} else {
					Rental.notification.error("加载角色信息",response.description);
				}
			}, null ,"加载角色信息");
		},
		initFormData:function(role) {
			if(!role) return;
			var self = this;
			var array = this.$form.serializeArray();  
			$.each(array,function(i,item){
	            role.hasOwnProperty(item.name) && self.$form.find("[name="+item.name+"]").val(role[item.name]);
	        });
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox:"radio",
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:1,
            });

			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getCompanyDepartmentTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/company/getCompanyDepartmentTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyDepartmentTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("加载组织架构",response.description || '查询失败');
				}
			}, null ,"加载组织架构");
		},
		initCompanyDepartmentTree:function(data) {
			var tree = this.toTree(data);
			this.initTree(this.$companyDepartmentTree,tree);
		},
		toTree:function(data) {

			var self = this;

			var tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						expanded:true,
						//checkbox: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
						selected:self.role && self.role.hasOwnProperty('departmentId') && self.role.departmentId == department[i].departmentId,
					};
					tr.push(item);

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					checkbox: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.departmentId,
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		eidtRole:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);

			var roleTree = this.$companyDepartmentTree.fancytree('getTree');

			var departmentList = (roleTree.getSelectedNodes() || []).map(function(item){
				return {
					departmentId:item.key,
				};
			});

			if(departmentList.length == 0) {
				bootbox.alert("请选择部门");
				return false;
			}

			formData.departmentId = departmentList[0].departmentId;

			Rental.ajax.submit("{0}/userRole/update".format(SitePath.service),formData,function(response){
				if(response.success) {
					Rental.notification.success("编辑角色",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("编辑角色",response.description || '失败');
				}
			}, null ,"编辑角色");
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
				/*EditRole.$form.find('.state-success').removeClass('state-success');
				EditRole.$form[0].reset();*/
			}
			return false;
		},
	};

	window.EditRole = EditRole;

})(jQuery);