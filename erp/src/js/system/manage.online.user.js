;(function($) {

	var onlineUesrManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_online_user_list];
		},
		initDom:function() {
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});
			self.searchData();  //初始化列表数据
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, prams || {});

			Rental.ajax.ajaxData('user/onLineUserInfo', searchData, '加载在线人数列表', function(response) {
                self.render(response.resultMap.data);	
                $("#totalOnlineUeser").html(response.resultMap.data.totalCount + "人")
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.onlineUesrManage = onlineUesrManage;

})(jQuery);