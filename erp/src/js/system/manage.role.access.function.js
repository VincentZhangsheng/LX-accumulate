;(function($){
	
	var RoleAccessFunction = {
		init:function(roleId,callBack) {
			this.calllBack = callBack;
			this.showModal(roleId);
		},
		showModal:function(roleId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/roleAccessFunction", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.loadData(roleId);
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#roleAccessFunctionForm");
			this.$roleFunctionTree = $("#roleFunctionTree");
			this.$cancelRoleAccessFuncBtn = $("#cancelRoleAccessFuncBtn");
			this.$roleAcccessFuncBtn = $("#roleAcccessFuncBtn");
		},
		initEvent:function() {
			var self = this;

			self.$roleAcccessFuncBtn.on('click', function(event) {
				event.preventDefault();
				self.accessRole();
			});

			self.$cancelRoleAccessFuncBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},	
		loadData:function(roleId) {
			var self = this;
			
			var commitData = {
				pageNo:1,
				pageSize:1,
				roleId:roleId,
			}

			Rental.ajax.submit("{0}/userRole/getRoleMenuList".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.role = response.resultMap.data;
					self.initData(self.role.role)
					self.getRoleFunctonTree();
				} else {
					Rental.notification.error("加载角色信息",response.description);
				}
			}, null ,"加载角色信息");
		},
		initData:function(role) {
			if(!role) return;
			var roleAccessFuncInfoTpl = $('#roleAccessFuncInfoTpl').html();
			Mustache.parse(roleAccessFuncInfoTpl);
			$("#roleAccessFuncInfo").html(Mustache.render(roleAccessFuncInfoTpl, {"role":role}));
		},
		resolveCurrentRoleMap:function() {
			var self = this;
			var map = new Object();
			function resolve(roleList) {
				for (var i in roleList) {
					var curr = roleList[i];

					map[curr.menuId]  = {
						menuId:curr.menuId,
						menuName:curr.menuName,
					}

					if(curr.hasOwnProperty('children')) {
						resolve(curr.children);
					}
				}

			}

			resolve(self.role.menuList);
			return map; 
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox:true,
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:2, //
            });

			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getRoleFunctonTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/menu/getAllMenu".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initRoleFuncitonTree(response.resultMap.data);
				} else {
					Rental.notification.error("加载功能权限树",response.description || '加载失败');
				}
			}, null ,"加载功能权限树");
		},
		initRoleFuncitonTree:function(data) {
			var tree = this.toTree(data);
			this.initTree(this.$roleFunctionTree,tree);
		},
		toTree:function(data) {

			var self = this;

			var tree = new Array();
			var icon = ['rental/img/tree/role/folder.png','rental/img/tree/role/list.png','rental/img/tree/role/btn.png','rental/img/tree/role/btn.png'];

			var currentRoleMap = self.resolveCurrentRoleMap();
		
			var level =  0;
			function dt(tr, rolelist) {
				
				for(var i in rolelist) {
					var curr = rolelist[i];
					var item = {
						expanded:true,
						folder: true,
						icon:SitePath.staticCommon + icon[level],
						key:curr.menuId,
						title:curr.menuName,
						selected:!!currentRoleMap[curr.menuId],
					};
					tr.push(item);

					if(curr.hasOwnProperty('children')) {
						level++;
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}

				level--;
			}

			dt(tree, data);

			return tree;
		},
		accessRole:function() {
			var self = this;
			//var formData = Rental.form.getFormData(self.$form);

			var roleTree = this.$roleFunctionTree.fancytree('getTree');

			var menuList = (roleTree.getSelectedNodes() || []).map(function(item){
				return {
					menuId:item.key,
				};
			});

			if(menuList.length == 0) {
				bootbox.alert("请选择功能权限");
				return false;
			}

			var commitData = {
				menuList:menuList,
				roleId:self.role.role.roleId,
			}

			Rental.ajax.submit("{0}/userRole/saveRoleMenu".format(SitePath.service),commitData,function(response){
				if(response.success) {
					Rental.notification.success("功能授权",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("功能授权",response.description || '失败');
				}
			}, null ,"功能授权", 'dialogLoading');
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
				// self.$form.find('.state-success').removeClass('state-success');
				// self.$form[0].reset();
			}
			return false;
		},
	};

	window.RoleAccessFunction = RoleAccessFunction;

})(jQuery);