;(function($){
	
	var AddTaskManage = {
		state:{
			id:null,
			dateList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_task_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_task_list_add];
		},
		initDom:function() {
			var self = this;
			this.$form = $("#addTaskForm");
		},
		initEvent:function() {
			var self = this;
			
			Layout.chooseSidebarMenu(AuthorCode.manage_task_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
            });
            
			self.initCommonEvent();
			
			$("#executorRow").removeClass("hide")
		},
		add:function() {
			try {
				var self = this,
                    formData = Rental.form.getFormData(self.$form);

				if(!!formData['holidayName'] && self.state.dateList.length == 0) {
					bootbox.alert('请指定不需要指定定时任务的日期');
					return;
				}

				if(self.state.dateList.length > 0 && !formData['holidayName']) {
					bootbox.alert('请输入假期名称');
					return;
				}
				
				var commitData = {
					triggerName:formData['triggerName'],
					triggerGroup:formData['triggerGroup'],
					cronExpression:formData['cronExpression'],
					holidayName:formData['holidayName'],
					description:formData['description'],
					taskExecutor:{
                        requestUrl:formData['requestUrl'],
                        systemType:formData['systemType'],
                        jobType:formData['jobType'],
                        requestBody:formData['requestBody'],
                    },
					holidayDTOList:self.state.dateList
                }

				var des = '添加定时任务';
				Rental.ajax.submit("{0}taskScheduler/initTaskScheduler".format(SitePath.service),commitData,function(response){
					if(response.success) {
						Rental.notification.success(des,response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des,response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("添加定时任务失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加定时任务",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		},
	};

	window.AddTaskManage = _.extend(AddTaskManage, TaskMixin);

})(jQuery);