;(function($) {
	var AddSystemNoticeManage = {
        state:{
            content:null,
        },
		init:function() {
			this.initAuthor();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
            this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_notice_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_notice_list_add];
		},
		initEvent:function() {
			var self = this;
            
			Layout.chooseSidebarMenu(AuthorCode.manage_notice_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑
            
            $('.summernote').summernote({
                lang:"zh-CN",
                height: 400,
                focus: false,
                oninit: function() {
                    $('.note-insert').css("display","none")
                },
                onChange: function(contents, $editable) {},
            });

            $("#actionButtons").on("click",".confirmBtn",function(event) {
                event.preventDefault();
                self.submit();
            })

            $("#actionButtons").on("click",".goBack",function(event) {
                event.preventDefault();
                window.history.go(-1);
            })
        },
        submit:function() {
            try {
                var self = this;
                var title = $("#title").val();
                var content = $('.note-editable').html();
                var remark = $("#remark").val();
                if(!title) {
                    bootbox.alert('请输入公告标题');
				    return;
                }
                if(!content) {
                    bootbox.alert('请输入公告内容');
				    return;
                }
                var commitData = {
                    title:title,
                    content:content,
                    remark:remark
                }
                Rental.ajax.submit("{0}announcement/create".format(SitePath.service), commitData, function(response){
                    if(response.success) {
                        Rental.notification.success("发布系统公告",response.description || '成功');
                        self.callBackFunc(response);
                    } else {
                        Rental.notification.error("发布系统公告",response.description || '失败');
                    }
                }, null ,"发布系统公告",'dialogLoading');				
            } catch (e) {
                Rental.notification.error("发布系统公告失败", Rental.lang.commonJsError +  '<br />' + e );
            }
        },
        callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.AddSystemNoticeManage = AddSystemNoticeManage;

})(jQuery);