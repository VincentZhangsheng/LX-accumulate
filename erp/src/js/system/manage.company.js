// 分公司管理
;(function($) {
	var CompanyManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_company_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var addShortReceivableAmount = this.currentPageAuthor.children[AuthorCode.manage_company_add_short_receivable_amount];

			addShortReceivableAmount && this.rowActionButtons.push(AuthorUtil.button(addShortReceivableAmount,'addShortReceivableAmountButton','fa fa-plus','设置短租应收上限'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			self.$dataListTable.on('click', '.addShortReceivableAmountButton', function(event) {
				event.preventDefault();
				var subCompanyId = $(this).data('subcompanyid'), shortLimitReceivableAmount = $(this).data('shortlimitreceivableamount');
				self.addShortReceivableAmount(subCompanyId, shortLimitReceivableAmount);
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this,
				searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.ajax.ajaxData('company/pageSubCompany', searchData, '加载分公司', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					subCompanyTypeValue:function() {
						switch(this.subCompanyType) {
							case 1:
								return '总公司';
							case 2:
								return '分公司';
							default:
								return "";
						}
					},
					shortLimitReceivableAmountValue:function() {
						return !this.hasOwnProperty('shortLimitReceivableAmount')  && !this.shortLimitReceivableAmount ? '无上限': '￥'+parseFloat(this.shortLimitReceivableAmount).toFixed(2);
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		addShortReceivableAmount:function(subCompanyId, shortLimitReceivableAmount) {
			var self = this;
			InputModal.init({
				title:'设置短租应收上限',
				url:'add-short-receivable-amount/modal',
				initFunc:function(modal) {
					$('[name=shortLimitReceivableAmount]', modal).val(shortLimitReceivableAmount);
				},
				callBack:function(result) {
					self.addShortReceivableAmountCommit({
						subCompanyId:subCompanyId,
						shortLimitReceivableAmount:result.shortLimitReceivableAmount,
					});
					// return true;
				}
			})
		},
		addShortReceivableAmountCommit:function(prams) {
			var self = this;
			Rental.ajax.submit("{0}company/addShortReceivableAmount".format(SitePath.service), prams,function(response){
				if(response.success) {
					Rental.notification.success("设置短租应收上限",response.description || '成功');
					self.doSearch(self.Pager.pagerData.currentPage);
					Rental.modal.close();
				} else {
					Rental.notification.error("设置短租应收上限",response.description || '失败');
				}
			}, null ,"设置短租应收上限", 'dialogLoading');
		}
	};

	window.CompanyManage = CompanyManage;

})(jQuery);