;(function($){
	
	var RoleAccessData = {
		init:function(roleId,callBack) {
			this.calllBack = callBack;
			this.showModal(roleId);
		},
		showModal:function(roleId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/roleAccessData", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.loadData(roleId);
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#roleAccessDataForm");
			this.$roleDataTree = $("#roleDataTree");
			this.$cancelRoleAccessDataBtn = $("#cancelRoleAccessDataBtn");
			this.$roleAcccessDataBtn = $("#roleAcccessDataBtn");
		},
		initEvent:function() {
			var self = this;

			self.$roleAcccessDataBtn.on('click', function(event) {
				event.preventDefault();
				self.accessRole();
			});

			self.$cancelRoleAccessDataBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},	
		loadData:function(roleId) {
			var self = this;
			
			var commitData = {
				pageNo:1,
				pageSize:1,
				roleId:roleId,
			}

			Rental.ajax.submit("{0}/userRole/getRoleMenuList".format(SitePath.service),commitData,function(response){
				if(response.success) {
					self.role = response.resultMap.data;
					self.initData(self.role.role);
					self.getRoleDepartmentDataListByRole();
				} else {
					Rental.notification.error("加载角色信息",response.description);
				}
			}, null ,"加载角色信息");
		},
		initData:function(role) {
			if(!role) return;
			var roleAccessInfoTpl = $('#roleAccessInfoTpl').html();
			Mustache.parse(roleAccessInfoTpl);
			$("#roleAccessInfo").html(Mustache.render(roleAccessInfoTpl, {"role":role}));
		},
		getRoleDepartmentDataListByRole:function() {
			var self = this;
			Rental.ajax.submit("{0}/userRole/getRoleDepartmentDataListByRole".format(SitePath.service),{roleId:self.role.role.roleId},function(response){
				if(response.success) {
					self.currentRoleDepartmentList = new Array();
					if(response.resultMap.hasOwnProperty('data') && response.resultMap.data.hasOwnProperty('departmentList')) {
						self.currentRoleDepartmentList = response.resultMap.data.departmentList;
					}
					self.getCompanyDepartmentTree();

				} else {
					Rental.notification.error("加载角色可看部门数据信息",response.description || '加载失败');
				}
			}, null ,"加载角色可看部门数据信息");
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox:true,
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:2,
            });

			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getCompanyDepartmentTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/company/getCompanyDepartmentTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyDepartmentTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("加载组织架构",response.description || '加载失败');
				}
			}, null ,"加载组织架构");
		},
		initCompanyDepartmentTree:function(data) {
			var tree = this.toTree(data);
			this.initTree(this.$roleDataTree,tree);
		},
		toTree:function(data) {

			var self = this;
			var tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
					/*	data:{
							a:'a',
							b:'b'
						},*/
						expanded:true,
						//checkbox: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
						selected:self.currentRoleDepartmentList && self.currentRoleDepartmentList.length > 0 && self.currentRoleDepartmentList.some(function(item){ return curr.departmentId == item.departmentId }),
					};
					tr.push(item);

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					checkbox: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.departmentId,
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		accessRole:function() {
			var self = this;
			//var formData = Rental.form.getFormData(self.$form);

			var roleTree = this.$roleDataTree.fancytree('getTree');

			var departmentList = (roleTree.getSelectedNodes() || []).map(function(item){
				return {
					departmentId:item.key,
				};
			});

			// if(departmentList.length == 0) {
			// 	bootbox.alert("请选择数据权限");
			// 	return false;
			// }

			var commitData = {
				departmentList:departmentList,
				roleId:self.role.role.roleId,
			}

			Rental.ajax.submit("{0}/userRole/updateRoleDepartmentData".format(SitePath.service),commitData,function(response){
				if(response.success) {
					Rental.notification.success("数据授权",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("数据授权",response.description || '失败');
				}
			}, null ,"数据授权");
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
				// self.$form.find('.state-success').removeClass('state-success');
				// self.$form[0].reset();
			}
			return false;
		},
	};

	window.RoleAccessData = RoleAccessData;

})(jQuery);