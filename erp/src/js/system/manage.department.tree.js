;(function($){

	var DepartmentTree = {
		state:{},
		nodeType:{
			company:1,
			department:2,
		},
		init:function(prams) {
			this.props = _.extend({
				$tree:$('.companyDepartmentTree'),
			}, prams || {});
			this.initDom();
			this.initEvent();
		},
		initDom:function() {
		},
		initEvent:function() {
			var self = this;
			self.getCompanyDepartmentTree();
		},
		getCompanyDepartmentTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/company/getCompanyDepartmentTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyDepartmentTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("加载组织架构",response.description || '查询失败');
				}
			}, null ,"加载组织架构");
		},
		initCompanyDepartmentTree:function(data) {
			var tree = this.toTree(data);
			this.state.departmentTree = tree;
			this.initTree(this.props.$tree, tree);
		},
		initTree:function($tree,source,prams) {
			var self = this;
			$tree.fancytree(_.extend({
				source:source,
                checkbox:false,
                clickFolderMode: 3, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:1,
                activate: function(event, data){
			        var node = data.node;
		         	if(node.data.nodeType == self.nodeType.department) {
			        	self.props.callBack && self.props.callBack(node.data.department, null);	     
			        } else if(node.data.nodeType == self.nodeType.company) {	
						self.props.callBack && self.props.callBack(null, node.data.company);	     
			        }
			    }
            }, prams || {}));

			//初始化滚动条
            // $tree.parents('.panel-body').nanoScroller({
            //     preventPageScrolling: true,
            //     alwaysVisible: true
            // });
		},
		toTree:function(data) {

			var self = this, tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						// expanded:true,
						//checkbox: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
						data:{
							department:curr,
							nodeType:self.nodeType.department
						}
					};
					tr.push(item);

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					// expanded:true,
					// checkbox: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.subCompanyId,
					data:{
						company:sub,
						nodeType:self.nodeType.company
					}
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}


			return tree;
		},
	};

	window.DepartmentTree = DepartmentTree;

})(jQuery);