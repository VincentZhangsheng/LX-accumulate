;(function($) {

	var TaskExecutor = {
		state:{},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_task_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_task_list_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_task_list_edit],
				pause = this.currentPageAuthor.children[AuthorCode.manage_task_list_pause],
				restart = this.currentPageAuthor.children[AuthorCode.manage_task_list_restart],
				del = this.currentPageAuthor.children[AuthorCode.manage_task_list_delete],
				updateExecutor = this.currentPageAuthor.children[AuthorCode.manage_task_list_update_executor];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

            edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','更新任务调度者'));
			pause && this.rowActionButtons.push(AuthorUtil.button(pause,'pauseButton','','暂停任务触发器'));
			restart && this.rowActionButtons.push(AuthorUtil.button(restart,'restartButton','','重启任务触发器'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'deleteButton','','删除任务触发器'));
			updateExecutor && this.rowActionButtons.push(AuthorUtil.button(updateExecutor,'updatePerformer','','更新执行者信息'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;
			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.renderCommonActionButton();

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.render(new Object());
            self.searchData();  //初始化列表数据
            
            self.$dataListTable.on("click",".updatePerformer",function(event) {
                event.preventDefault();
                var rowData = $(this).closest("tr").data("rowdata");
                self.updatePerformer.init({
                        performerInfo:rowData,
                        callBack:function() {
                        self.searchData()
                    }
                })
            })

            self.$dataListTable.on("click",".pauseButton",function(event) {
                event.preventDefault();
                var commitData = {
                    triggerName:$(this).data("name"),
                    triggerGroup:$(this).data("group")
                }
                self.pauseTask(commitData,function() {
                    self.searchData();
                })
            })

            self.$dataListTable.on("click",".restartButton",function(event) {
                event.preventDefault();
                var commitData = {
                    triggerName:$(this).data("name"),
                    triggerGroup:$(this).data("group")
                }
                self.restartTask(commitData,function() {
                    self.searchData();
                })
            })

            self.$dataListTable.on("click",".deleteButton",function(event) {
                event.preventDefault();
                var commitData = {
					taskExecutorId:$(this).data("id"),
                    triggerName:$(this).data("name"),
                    triggerGroup:$(this).data("group")
                }
                self.deleteTask(commitData,function() {
                    self.searchData();
                })
            })
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.ajaxData('taskScheduler/pageTaskExecutor', searchData, '加载任务执行者列表', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this;
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;

			var data = {
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    rowData:function() {
                        return JSON.stringify(this)
                    },
					rowActionButtons:function() {
						return self.filterRowButtons(self.rowActionButtons, this);
					},
					jobTypeVal:function() {
						return Enum.taskJobType.getValue(this.jobType);
					},
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        },
        taskDo:function(url, commitData, des, callBack) {
            var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url),commitData,function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
        },
        pauseTask:function(commitData, callBack) {
            var self = this;
			bootbox.confirm('确认暂停触发器？', function(result) {
				result && self.taskDo('taskScheduler/pauseTaskTrigger', commitData, '暂停触发器', callBack);
			});
        },
        restartTask:function(commitData, callBack) {
            var self = this;
			bootbox.confirm('确认重启触发器？', function(result) {
				result && self.taskDo('taskScheduler/resumeTaskTrigger', commitData, '重启触发器', callBack);
			});
        },
        deleteTask:function(commitData, callBack) {
            var self = this;
			bootbox.confirm('可能会影响到其它执行者正常执行，确认删除触发器？', function(result) {
				result && self.taskDo('taskScheduler/deleteTaskTrigger', commitData, '删除触发器', callBack);
			});
		},
		filterRowButtons:function(buttons, task) {
			var rowActionButtons = new Array();
		
			(buttons || []).forEach(function(button, index) {
				rowActionButtons.push(button);

				if((button.class == 'restartButton') && task.triggerState != "PAUSED") {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if((button.class == 'pauseButton') && task.triggerState == "PAUSED") {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		},
        updatePerformer:{
			init:function(prams) {
				this.props = _.extend({
					performerInfo:null,
					callBack:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "timed-manage/update-performer", type:'ajax', ajaxContentAdded:function(pModal) {
					self.initDom();
					self.initEvent();	
				}});
			},
			initDom:function() {
				this.$form = $("#editPerformerForm");
				this.$cancelButton = $('.cancelButton', this.$form);
			},
			initEvent:function() {
				var self = this;
				
				Rental.form.initFormValidation(self.$form, function(form){
					self.edit();
				});
	
				self.$cancelButton.on('click', function(event) {
					event.preventDefault();
					Rental.modal.close();
                });

                self.initFormData(self.props.performerInfo)
            },
            initFormData:function(performerInfo) {
                var self = this;
                Rental.ui.renderFormData(self.$form, performerInfo);
                if(performerInfo.asyn == 1) {
                    $(".synchronization",self.$form).prop("checked",true)
                }
                if(performerInfo.asyn == 0) {
                    $(".asychronous",self.$form).prop("checked",true)
                }
            },
			edit:function() {
				try {
					var self = this;
					var formData = Rental.form.getFormData(self.$form);
                    var synchronization = $(".synchronization", self.$form).prop('checked');
                    var asychronous = $(".asychronous", self.$form).prop('checked');

					var commitData = {
						id: self.props.performerInfo.id,
                        retryCount:formData["retryCount"],
                        retryInterval:formData["retryInterval"],
                        requestUrl:formData["requestUrl"],
                        requestBody:formData["requestBody"],
                        jobCallbackClassName:formData["jobCallbackClassName"],
                    }
                    if(!!synchronization) {
                        commitData.asyn = 1;
                    }
                    if(!!asychronous) {
                        commitData.asyn = 0;
                    }
					Rental.ajax.submit("{0}{1}".format(SitePath.service,"taskScheduler/updateTaskExecutor"),commitData,function(response){
						if(response.success) {
							Rental.notification.success("修改任务执行者信息",response.description || '成功');
							self.callBackFunc();
						} else {
							Rental.notification.error("修改任务执行者信息",response.description || '失败');
						}
					}, null ,"修改任务执行者信息",'dialogLoading');
									
				} catch (e) {
					Rental.notification.error("修改任务执行者信息失败", Rental.lang.commonJsError +  '<br />' + e );
				}
			},
			callBackFunc:function(response) {
				Rental.modal.close();
				this.props.hasOwnProperty('callBack') && this.props.callBack(response);
			},
		},
	};

	window.TaskExecutor = TaskExecutor;

})(jQuery);