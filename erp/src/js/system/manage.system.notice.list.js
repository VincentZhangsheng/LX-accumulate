;(function($) {
	var SystemNoticeManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_notice_list];
            this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

            var add = this.currentPageAuthor.children[AuthorCode.manage_notice_list_add],
                view = this.currentPageAuthor.children[AuthorCode.manage_notice_list_detail],
				edit = this.currentPageAuthor.children[AuthorCode.manage_notice_list_edit],
				del = this.currentPageAuthor.children[AuthorCode.manage_notice_list_delete];

			add && this.commonActionButtons.push(AuthorUtil.button(_.extend(add,{menuUrl:"system-manage/notice-add-page"}),'addButton','fa fa-plus','添加'));

            view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(_.extend(edit,{menuUrl:"system-manage/notice-edit"}),'editButton','','编辑'));
			del && this.rowActionButtons.push(AuthorUtil.button(del,'deleteButton','','删除'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

            Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
            });
            
			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

            self.renderCommonActionButton();
			self.render(new Object());
			self.searchData();  //初始化列表数据

            self.$dataListTable.on("click",".viewButton",function(event) {
				event.preventDefault();
				var noticeObj = $(this).closest("tr").data("notice");
				self.noticeDialog.init({noticeObj:noticeObj})
			})
            
            self.$dataListTable.on('click', '.deleteButton', function(event) {
                event.preventDefault();
                var noticeId = $(this).data("id");
				self.delete(noticeId)
			});
        },
        renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
			
			Rental.ajax.ajaxData("announcement/list",searchData, "查询系统公告列表", function(response){
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(sqlData) {
			var self = this;
			
			var listData = sqlData.hasOwnProperty("itemList") ? sqlData.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0;

			var data = {
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
                    "listData":listData,
                    noticeStr:function() {
						return JSON.stringify(this);
					},
                    rowActionButtons:self.rowActionButtons
				})
			}
			Mustache.parse(this.$dataListTpl);
            this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
            
            $(".notice-content").each(function() {
                var _index = $(this).closest("tr").index();
                $(this).html(listData[_index].content);
            })
        },
        do:function(url, commitData, des, callBack) {
            Rental.ajax.submit("{0}{1}".format(SitePath.service, url),commitData,function(response){
                if(response.success) {
                    Rental.notification.success(des,response.description || '成功');
                    callBack && callBack();
                } else {
                    Rental.notification.error(des,response.description || '失败');
                }
            }, null, des, "dialogLoading");
        },
		delete:function(id) {
			var self = this;
			if(!id) {
				bootbox.alert('没找到公告id');
				return;
            }
			bootbox.confirm('确认删除该系统公告？',function(result) {
				result && self.do("announcement/delete", {id:id}, "删除该系统公告", function() {
                    self.doSearch(self.Pager.pagerData.currentPage);
                });
			});
        },
		filterRowButtons:function(dynamicSqlData) {
			var self = this;
			var rowActionButtons = new Array();
			(self.rowActionButtons || []).forEach(function(button, index) {
				rowActionButtons.push(button);
			});
			return rowActionButtons;
        },
        noticeDialog:{
			init:function(prams) {
				this.props = _.extend({
					noticeObj:null,
					callBack:function() {}
				}, prams || {});
				this.show();
			},
			show:function() {
				var self = this;
				Rental.modal.open({src:SitePath.base + "home/system-notice", type:'ajax', ajaxContentAdded:function(pModal) {
					self.initDom();
					self.initEvent();	
				}});
			},
			initDom:function() {
			},
			initEvent:function() {
				this.renderNoticeInfo();
			},
			renderNoticeInfo:function() {
				var self = this;
				var noticeDetailTpl = $("#noticeDetailTpl").html();
				var data = _.extend(Rental.render, {
					noticeObj:self.props.noticeObj
				})
				Mustache.parse(noticeDetailTpl);
                $("#noticeDetail").html(Mustache.render(noticeDetailTpl, data));
                
                $("#noticeContent").html(self.props.noticeObj.content);
			},
        },
	};

	window.SystemNoticeManage = SystemNoticeManage;

})(jQuery);