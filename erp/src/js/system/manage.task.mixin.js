/**
* 客户公用类
*/
;(function() {
	TaskMixin = {
		state:{
            dateList:new Array()
        },
		initCommonEvent:function() {
			var self = this;

            Rental.ui.renderSelect({
				container:$("#systemType"),
				data:Enum.array(Enum.taskSystemType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				defaultText:'系统类型'
            });

			Rental.ui.renderSelect({
				container:$("#jobType"),
				data:Enum.array(Enum.taskJobType),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				defaultText:'Job类型'
            });
            
            self.renderDateSelect();
            self.renderDateList();

            $("#chooseDateTable").rtlCheckAll('checkAll','checkItem');

            $("#batchAddDate").on("click",function(event) {
                event.preventDefault();
                self.addHolidayDto();
                self.renderDateList();
            })

            $("#batchDeleteDate").on("click",function(event) {
                event.preventDefault();
                self.deleteHolidayDto();
                self.renderDateList();
            })
        },
        addHolidayDto:function() {
            var self = this;
            var year = $("#yearSelect").val();
            var month = $("#monthSelect").val();
            var day = $("#daySelect").val();
            if(!year || !month || !day) {
                bootbox.alert('请选择完整的日期');
				return;
            }
            var dateObj = {
                year:year,
                month:month,
                day:day
            }
            var _index = _.findIndex(self.state.dateList,{year:dateObj.year,month:dateObj.month,day:dateObj.day})
            if(_index > -1) {
                return self.state.dateList
            } else {
                self.state.dateList.push(dateObj)
            }
            return self.state.dateList
        },
        deleteHolidayDto:function() {
            var self = this,
                array = $('[name=checkItem]', ("#chooseDateTable")).rtlCheckArray(),
                filterChecked = $('[name=checkItem]', ("#chooseDateTable")).filter(':checked');

            if(array.length == 0) {
                bootbox.alert('请选择要删除的日期');
                return self.state.dateList;
            }

            filterChecked.each(function(i, item) {
                var dateObj = $(this).closest(".date-item ").data("time");
                var _index = _.findIndex(self.state.dateList,{year:dateObj.year,month:dateObj.month,day:dateObj.day})
                if(self.state.dateList.length > 0) {
                    self.state.dateList.splice(_index,1)
                }
            });
            
            return self.state.dateList;
        },
        renderDateSelect:function() {
            var $YearSelector = $("#yearSelect");
            var $MonthSelector = $("#monthSelect");
            var $DaySelector = $("#daySelect");

            for(var i = 2000; i<=2025; i++) {
                var $option = $('<option value="' + i + '">' + i + '</option>').val(i)
                $YearSelector.append($option)
            }
            for(var i = 1; i <=12; i++){
                var $option = $('<option value="' + i + '">' + i + '</option>').val(i)
                $MonthSelector.append($option)   
            }
            for(var i = 1; i <=31; i++){
                var $option = $('<option value="' + i + '">' + i + '</option>').val(i)
                $("#daySelect").append($option)     
            }
            function buildDay() {
                $DaySelector.html('<option value="">日</option>');
                var year = parseInt($YearSelector.val());
                var month = parseInt($MonthSelector.val());
                var dayCount = 0;
                switch (month) {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        dayCount = 31;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        dayCount = 30;
                        break;
                    case 2:
                        dayCount = 28;
                        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
                            dayCount = 29;
                        }
                        break;
                    default:
                        break;
                }
                for (var i = 1; i <= dayCount; i++) {
                    var dayStr = $('<option value="' + i + '">' + i + '</option>').val(i);
                    $DaySelector.append(dayStr);
                }
            }
            $MonthSelector.change(function () {
                buildDay();
            }); 
            $YearSelector.change(function () {
                buildDay();
            }); 
        },
        renderDateList:function() {
            var self = this;
            var hasDate = self.state.dateList.length > 0;
            if(hasDate) {
                $("#dateRow").removeClass("hide")
            } else {
                if(!$("#dateRow").hasClass("hide")) {
                    $("#dateRow").addClass("hide")
                }
            }
            var data = _.extend(Rental.render, {
                dateList:self.state.dateList,
                dateObj:function() {
                    return JSON.stringify(this)
                }
            })

            var $chooseDateTpl = $("#chooseDateTpl").html();
            Mustache.parse($chooseDateTpl );
			$("#chooseDateTable").html(Mustache.render($chooseDateTpl , data));
        }
	}
	window.TaskMixin = TaskMixin;

})(jQuery);