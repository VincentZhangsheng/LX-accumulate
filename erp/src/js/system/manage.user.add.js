;(function($){
	
	var AddUser = {
		init:function(callBack) {
			this.calllBack = callBack;
			this.showModal();
			// this.initDom();
			// this.initEvent();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/addUser", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#addUserForm");
			this.$cancelAddUserBtn = $('#cancelAddUserBtn');
			this.$companyRoleTree = $("#companyRoleTree");
		},
		initEvent:function() {
			var self = this;

			this.getCompanyRoleTree();

			Rental.form.initFormValidation(this.$form,function(form){
				self.addUser();
			});

			self.$cancelAddUserBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox: true, // Show checkboxes.
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:3,
            });

			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getCompanyRoleTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/userRole/getCompanyRoleTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyRoleTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("查询组织架构",response.description || '查询失败');
				}
			}, null ,"查询组织架构");
		},
		initCompanyRoleTree:function(data) {
			var tree = this.toTree(data);
			this.initTree(this.$companyRoleTree,tree);
		},
		toTree:function(data) {

			var tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						expanded:true,
						checkbox: false,
						unselectableStatus: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
					};
					tr.push(item);

					if(curr.hasOwnProperty('roleList')) {
						item.children = item.children ||  new Array();
						for(var j in curr.roleList) {
							item.children.push({
								expanded: true,
								icon:SitePath.staticCommon + 'rental/img/tree/role.png',
								key:curr.roleList[j].roleId,
								title:curr.roleList[j].roleName,
							});
						}
					}

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					checkbox: false,
					unselectableStatus: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.departmentId,
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		addUser:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);

			var roleTree = this.$companyRoleTree.fancytree('getTree');

			var roleList = (roleTree.getSelectedNodes() || []).map(function(item){
				return {
					roleId:item.key,
				};
			});

			if(roleList.length == 0) {
				bootbox.alert("请选择职位");
				return false;
			}

			formData.roleList = roleList;

			Rental.ajax.submit("{0}/user/add".format(SitePath.service),formData,function(response){
				if(response.success) {
					Rental.notification.success("添加用户",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("添加用户",response.description || '失败');
				}
			}, null ,"添加用户", 'dialogLoading');
		},
		callBackFunc:function(response) {
			if(response.success) {
				Rental.modal.close();
				AddUser.hasOwnProperty('calllBack') && AddUser.calllBack();
				AddUser.$form.find('.state-success').removeClass('state-success');
				AddUser.$form[0].reset();
			}
			return false;
		}
	};

	window.AddUser = AddUser;

})(jQuery);