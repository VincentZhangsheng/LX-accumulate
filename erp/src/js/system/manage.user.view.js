;(function($){
	
	var ViewUser = {
		init:function(userId) {
			this.showModal(userId);
		},
		initDom:function() {
			this.$form = $("#viewUserForm");
		},
		showModal:function(userId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/view", type:'ajax', ajaxContentAdded:function(pModal) {
				self.loadUser(userId);
				self.initDom();	
			}});
		},
		loadUser:function(userId) {
			var self = this;
			Rental.ajax.submit("{0}/user/getUserById".format(SitePath.service),{userId:userId},function(response){
				if(response.success) {
					self.setFormData(response.resultMap.data);
					self.setRole(response.resultMap.data);
				} else {
					Rental.notification.error("查询用户详细信息失败",response.description);
				}
			}, null ,"查询用户详细信息");
		},
		setFormData:function(user) {
			if(!user) return;

			var self = this, 
				array = self.$form.serializeArray();  

			$.each(array,function(i,item){
	            user.hasOwnProperty(item.name) && self.$form.find("[name="+item.name+"]").val(user[item.name]).prop({disabled:true});
	        });
		},
		setRole:function(user) {
			var viewUserRoleTpl = $('#viewUserRoleTpl').html();
			Mustache.parse(viewUserRoleTpl);
			$('#viewUserRole').html(Mustache.render(viewUserRoleTpl,{
				roleList:user.roleList,
				role:function() {
					return this.subCompanyName + " " + this.departmentName + " " + this.roleName;
				}
			}));
		},
	};

	window.ViewUser = ViewUser;

})(jQuery);