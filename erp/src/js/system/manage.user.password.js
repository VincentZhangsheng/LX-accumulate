;(function($){
	
	var UpdateUserPassword = {
		init:function(userId,callBack) {
			this.showModal(userId);
			this.calllBack = callBack;
			//this.currentPage = currentPage;
		},
		initDom:function(userId) {
			this.$form = $("#updateUserPasswordForm");
			this.$form.attr({'action':SitePath.base+'/user/updatePassword'});
			this.$form.find("[name=userId]").val(userId);
		},
		showModal:function(userId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/password",type: 'ajax',ajaxContentAdded:function(pModal) {
				self.initDom(userId);
				self.initEvent();	
			}});
		},
		initEvent:function() {
			Rental.form.initFormValidation(this.$form);
		},
		callBackFunc:function(response) {
			if(response.success) {
				Rental.modal.close();
				UpdateUserPassword.hasOwnProperty('calllBack') && UpdateUserPassword.calllBack && UpdateUserPassword.calllBack();
				UpdateUserPassword.$form.find('.state-success').removeClass('state-success');
				UpdateUserPassword.$form[0].reset();
			}
			return false;
		}
	};

	window.UpdateUserPassword = UpdateUserPassword;

})(jQuery);