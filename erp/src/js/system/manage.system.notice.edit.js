;(function($) {
	var EditSystemNoticeManage = {
        state:{
            id:null,
        },
		init:function() {
            this.initAuthor();
            this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
            this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_notice_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_notice_list_edit];
        },
        initDom:function() {
            this.state.id = Rental.helper.getUrlPara('id');
        },
		initEvent:function() {
			var self = this;
            
			Layout.chooseSidebarMenu(AuthorCode.manage_notice_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

            self.loadData();

            $("#actionButtons").on("click",".confirmBtn",function(event) {
                event.preventDefault();
                self.submit();
            })

            $("#actionButtons").on("click",".goBack",function(event) {
                event.preventDefault();
                window.history.go(-1);
            })
        },
        initData:function(data) {
            $("#title").val(data.title);
            $("#remark").val(data.remark);
            $('.summernote').summernote({
                lang:"zh-CN",
                height: 400,
                focus: false,
                oninit: function() {
                    $('.note-insert').css("display","none");
                    $('.note-editable').html(data.content);
                },
                onChange: function(contents, $editable) {},
            });
        },
        loadData:function() {
            var self = this;
            Rental.ajax.submit("{0}announcement/list".format(SitePath.service),{id:self.state.id},function(response){
				if(response.success) {
					self.initData(response.resultMap.data.itemList[0]);
				} else {
					Rental.notification.error("加载公告详细信息",response.description || '失败');
				}
			}, null ,"加载公告详细信息", 'listLoading');
        },
        submit:function() {
            try {
                var self = this;
                var title = $("#title").val();
                var content = $('.note-editable').html();
                var remark = $("#remark").val();
                if(!title) {
                    bootbox.alert('请输入公告标题');
				    return;
                }
                if(!content) {
                    bootbox.alert('请输入公告内容');
				    return;
                }
                var commitData = {
                    id:self.state.id,
                    title:title,
                    content:content,
                    remark:remark
                }
                Rental.ajax.submit("{0}announcement/update".format(SitePath.service), commitData, function(response){
                    if(response.success) {
                        Rental.notification.success("编辑公告",response.description || '成功');
                        self.callBackFunc(response);
                    } else {
                        Rental.notification.error("编辑公告",response.description || '失败');
                    }
                }, null ,"编辑公告",'dialogLoading');				
            } catch (e) {
                Rental.notification.error("编辑公告失败", Rental.lang.commonJsError +  '<br />' + e );
            }
        },
        callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.EditSystemNoticeManage = EditSystemNoticeManage;

})(jQuery);