;(function($){
	
	var UserAccessData = {
		init:function(userId,callBack) {
			this.calllBack = callBack;
			this.showModal(userId);
		},
		showModal:function(userId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "user-manage/userAccessData", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.loadData(userId);
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$companyDepartmentUserTree = $("#companyDepartmentUserTree");
			this.$cancelUserAccessDataBtn = $("#cancelUserAccessDataBtn");
			this.$userAcccessDataBtn = $("#userAcccessDataBtn");
		},
		initEvent:function() {
			var self = this;

			self.$userAcccessDataBtn.on('click', function(event) {
				event.preventDefault();
				self.accessRole();
			});

			self.$cancelUserAccessDataBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},	
		loadData:function(userId) {
			var self = this;
			Rental.ajax.submit("{0}/user/getUserById".format(SitePath.service),{userId:userId},function(response){
				if(response.success) {
					self.user = response.resultMap.data;
					self.initData(self.user);
					self.getRoleUserDataListByUser();
				} else {
					Rental.notification.error("加载用户详细信息",response.description || '失败');
				}
			}, null ,"加载用户详细信息");
		},
		initData:function(user) {
			if(!user) return;
			var userAccessInfoTpl = $('#userAccessInfoTpl').html();
			Mustache.parse(userAccessInfoTpl);
			$("#userAccessInfo").html(Mustache.render(userAccessInfoTpl, {
				"user":user,
				roleListData:function() {
					return this.roleList.map(function(item) {
						return {
							role:item.subCompanyName + " " +item.departmentName + " " + item.roleName,
						}
					})
				}
			}));
		},
		getRoleUserDataListByUser:function() {
			var self = this;
			Rental.ajax.submit("{0}userRole/getRoleUserDataListByUser".format(SitePath.service),{activeUserId:self.user.userId},function(response){
				if(response.success) {
					self.passiveUserList = new Array();
					if(response.resultMap.hasOwnProperty('data') && response.resultMap.data.hasOwnProperty('passiveUserList')) {
						self.passiveUserList = response.resultMap.data.passiveUserList;
					}
					self.getCompanyDepartmentUserTree();
				} else {
					Rental.notification.error("加载用户可看其它用户列表",response.description || '加载失败');
				}
			}, null ,"加载用户可看其它用户列表");
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox:true,
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:2,
            });

			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getCompanyDepartmentUserTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/company/getCompanyDepartmentUserTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyDepartmentUserTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("加载组织架构",response.description || '加载失败');
				}
			}, null ,"加载组织架构");
		},
		initCompanyDepartmentUserTree:function(data) {
			var tree = this.toTree(data);
			this.initTree(this.$companyDepartmentUserTree,tree);
		},
		toTree:function(data) {

			var self = this;
			var tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						expanded:true,
						checkbox: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
					};
					tr.push(item);

					if(curr.hasOwnProperty('userList')) {
						item.children = item.children ||  new Array();
						for(var j in curr.userList) {
							item.children.push({
								expanded: true,
								icon:SitePath.staticCommon + 'rental/img/tree/role.png',
								key:curr.userList[j].userId,
								title:curr.userList[j].realName,
								selected:self.passiveUserList && self.passiveUserList.length > 0 && self.passiveUserList.some(function(user){ return curr.userList[j].userId == user.userId; }),
							});
						}
					}

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					checkbox: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.departmentId,
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		accessRole:function() {
			var self = this;

			var roleTree = this.$companyDepartmentUserTree.fancytree('getTree');

			var passiveUserList = (roleTree.getSelectedNodes() || []).map(function(item){
				console.log(item.key);
				return {
					userId:item.key,
				};
			});

			// if(passiveUserList.length == 0) {
			// 	bootbox.alert("请选择需要观察的用户");
			// 	return false;
			// }

			var commitData = {
				passiveUserList:passiveUserList,
				activeUserId:self.user.userId,
			}

			Rental.ajax.submit("{0}userRole/updateRoleUserData".format(SitePath.service),commitData,function(response){
				if(response.success) {
					Rental.notification.success("数据授权",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("数据授权",response.description || '失败');
				}
			}, null ,"数据授权");
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
			}
			return false;
		},
	};

	window.UserAccessData = UserAccessData;

})(jQuery);