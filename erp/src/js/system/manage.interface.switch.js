/**
 * decription: 功能开关列表
 * author:zhangsheng
 * time: 2018-4-10
 */
;(function($) {

	var InterfaceSwitch = {
		state:{
			switchId:null,
			switchList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_customer];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_customer_list];
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$btnRefresh = $("#btnRefresh");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			var self = this;

			//Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
            });
            
            Rental.ui.events.initRangeDatePicker($('#createTimePicker'), $('#createTimePickerInput'),  $("#createStartTime"), $("#createEndTime"));

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.searchData();  //初始化列表数据
			self.renderCommonActionButton(); //渲染操作按钮及事件

			Rental.ui.renderSelect({
				container: $('#isOpen'),
				data:Enum.array(self.isOpen),
				func:function(opt, item) {
					return opt.format(item.num.toString(), item.value.toString())
				},
				change:function(val) {
					self.searchData();
				},
				defaultText: '是否开启'
			})

            //改变开关
			self.$dataListTable.on('change', '.switchCheckbox', function(event) {
                event.preventDefault();
                var rowData = $(this).closest('tr').data('rowdata');
                var isChecked = $(this).prop("checked");
                var isOpen = isChecked ? 1 : 0;
                var updateData = {
                    switchId:rowData.switchId,
                    interfaceUrl:rowData.interfaceUrl,
                    remark:rowData.remark,
                    isOpen:isOpen
                }
                self.changeIsOpen(updateData,function(){
                    // self.searchData();
                });
            })
            
            //添加
			self.$actionCommonButtons.on('click', '.addButton', function(event) {
				event.preventDefault();
				self.addSwitch(function(){
                    self.doSearch(self.Pager.pagerData.currentPage);
                });
            })
            
			//编辑
			self.$dataListTable.on('click', '.editButton', function(event) {
                event.preventDefault();
                var rowData = $(this).closest('tr').data('rowdata');
				self.editSwitch(rowData,function(){
                    self.doSearch(self.Pager.pagerData.currentPage);
                });
            })
            
            //删除
			self.$dataListTable.on('click', '.delButton', function(event) {
                event.preventDefault();
                var rowData = $(this).closest('tr').data('rowdata');
				self.deleteSwitch(rowData.switchId,function(){
                    self.doSearch(self.Pager.pagerData.currentPage);
                });
			})
        },
        do:function(url, prams, des, callBack) {
			Rental.ajax.submit("{0}{1}".format(SitePath.service, url), prams, function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null, des, 'dialogLoading');
		},
		//添加
		addSwitch:function(callBack) {
			var self = this;
			SwitchModal.init({
				callBack:function(result) {
					self.do('interfaceSwitch/add',{
                        interfaceUrl:result.interfaceUrl,
                        remark:result.remark
                    }, '添加功能开关', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			})
        },
        //编辑
		editSwitch:function(rowData,callBack) {
			var self = this;
			SwitchModal.init({
                showIsOpen:true,
                initFunc:function(modalForm) {
                    Rental.ui.renderFormData(modalForm, rowData);
                    $("[name=isOpen]",modalForm).prop({'checked':rowData.isOpen == 1})
                },
				callBack:function(result) {
					var isOpen = result.hasOwnProperty("isOpen") ? result.isOpen : 0;
					self.do('interfaceSwitch/update',{
                        switchId:result.switchId,
                        interfaceUrl:result.interfaceUrl,
                        isOpen:isOpen,
                        remark:result.remark
                    }, '添加功能开关', function() {
						callBack && callBack();
						Rental.modal.close();
					});	
				}
			})
		},
		//删除
		deleteSwitch:function(switchId, callBack) {
			var self = this;
			bootbox.confirm('确认删除该功能开关？', function(result) {
				result && self.deleteDo('interfaceSwitch/delete', switchId, '删除功能开关', callBack);
			});
		},
		deleteDo:function(url, switchId, des, callBack) {
			if(!switchId) {
				bootbox.alert('找不到功能开关ID');
				return;
			}
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url),{switchId:switchId},function(response){
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null ,des,'dialogLoading');
        },
        //修改
		changeIsOpen:function(updateData, callBack) {
			Rental.ajax.submit("{0}interfaceSwitch/update".format(SitePath.service),updateData,function(response){
				if(response.success) {
					// Rental.notification.success("修改开关",response.description || '成功');
					callBack && callBack();
				} else {
					// Rental.notification.error("修改开关",response.description || '失败');
				}
			}, null ,"修改开关");
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
            var formData = Rental.form.getFormData(self.$searchForm);
            var filterData = {
                isOpen:formData.isOpen,
                interfaceUrl:formData.interfaceUrl,
                createStartTime:formData.createStartTime,
                createEndTime:formData.createEndTime
            }

			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, filterData || {}, prams || {});

			Rental.ajax.ajaxData('interfaceSwitch/page', searchData, '查询功能开关', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			}, $('.pagerContainer',$("#content")));
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [];

			var data = {
				dataSource:_.extend(Rental.render, {
					"listData":listData,
                    rowData:function() {
						return !!this && _.isObject(this) && JSON.stringify(this);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
        },
        isOpen:{
			num:{
				open:1,
				notOpen:0,
			},
			getValue:function(num) {
				switch(num) {
					case this.num.open:
						return "是";
					case this.num.notOpen:
						return "否";
				}
				return "";
			}
		},
	};

	window.InterfaceSwitch = InterfaceSwitch;

})(jQuery);		