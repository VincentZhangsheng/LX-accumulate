;(function($){
	
	var EditUser = {
		init:function(userId,callBack) {
			this.calllBack = callBack;
			this.showModal(userId);
		},
		showModal:function(userId){
			var self = this;
			Rental.modal.open({src:SitePath.base + "/user-manage/editUser", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.loadUser(userId);
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#editUserForm");
			this.$cancelEditUserBtn = $('#cancelEditUserBtn');
			this.$companyRoleTree = $("#companyRoleTree");
		},
		initEvent:function() {
			var self = this;

			Rental.form.initFormValidation(this.$form,function(form){
				self.editUser();
			});

			self.$cancelEditUserBtn.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});

		},
		loadUser:function(userId) {
			var self = this;
			Rental.ajax.submit("{0}/user/getUserById".format(SitePath.service),{userId:userId},function(response){
				if(response.success) {
					self.user = response.resultMap.data;
					EditUser.initFormData(response.resultMap.data);
					self.getCompanyRoleTree();
				} else {
					Rental.notification.error("查询用户详细信息失败",response.description);
				}
			}, null ,"查询用户详细信息");
		},
		initFormData:function(user) {
			if(!user) return;
			var _this = this;
			var array = this.$form.serializeArray();  
			$.each(array,function(i,item){
	            user.hasOwnProperty(item.name) && _this.$form.find("[name="+item.name+"]").val(user[item.name]);
	        });
		},
		initTree:function($tree,source) {
			$tree.fancytree({
				source:source,
                checkbox: true, // Show checkboxes.
                clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                autoCollapse: false,
                selectMode:3,
            });

			//初始化滚动条
            $tree.parents('.panel-body').nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true
            });
		},
		getCompanyRoleTree:function() {
			var self = this;
			Rental.ajax.submit("{0}/userRole/getCompanyRoleTree".format(SitePath.service),{},function(response){
				if(response.success) {
					self.initCompanyRoleTree(response.resultMap.data.subCompanyList);
				} else {
					Rental.notification.error("查询组织架构",response.description || '查询失败');
				}
			}, null ,"查询组织架构");
		},
		initCompanyRoleTree:function(data) {
			var tree = this.toTree(data);
			console.log(tree)
			this.initTree(this.$companyRoleTree,tree);
		},
		toTree:function(data) {

			var self = this;

			var tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						expanded:true,
						checkbox: false,
						unselectableStatus: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
					};
					tr.push(item);

					if(curr.hasOwnProperty('roleList')) {
						item.children = item.children ||  new Array();
						for(var j in curr.roleList) {
							item.children.push({
								expanded: true,
								icon:SitePath.staticCommon + 'rental/img/tree/role.png',
								key:curr.roleList[j].roleId,
								title:curr.roleList[j].roleName,
								selected:self.user && self.user.hasOwnProperty('roleList') && self.user.roleList.some(function(item) {
									return item.roleId == curr.roleList[j].roleId;
								}),
							});
						}
					}

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					checkbox: false,
					unselectableStatus: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.departmentId,
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		editUser:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);

			var roleTree = this.$companyRoleTree.fancytree('getTree');

			var roleList = (roleTree.getSelectedNodes() || []).map(function(item){
				return {
					roleId:item.key,
				};
			});

			if(roleList.length == 0) {
				bootbox.alert("请选择职位");
				return false;
			}

			formData.roleList = roleList;

			Rental.ajax.submit("{0}/user/update".format(SitePath.service),formData,function(response){
				if(response.success) {
					Rental.notification.success("编辑用户",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("编辑用户",response.description || '失败');
				}
			}, null ,"编辑用户", 'dialogLoading');
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.hasOwnProperty('calllBack') && self.calllBack();
				self.$form.find('.state-success').removeClass('state-success');
				self.$form[0].reset();
			}
			return false;
		}
	};

	window.EditUser = EditUser;

})(jQuery);