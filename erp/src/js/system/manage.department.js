;(function($){

	var DepartmentManage = {
		nodeType:{
			company:1,
			department:2,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_department];
			this.initActionButtons();
		},
		initActionButtons:function() {
			// this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();
			this.rowActionButtonsForCompanyNode = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var author = this.currentPageAuthor.children, 
				add = author[AuthorCode.manage_department_add],
				edit = author[AuthorCode.manage_department_edit],
				delte = author[AuthorCode.manage_department_delete];

			// add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			add && this.rowActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));							
			delte && this.rowActionButtons.push(AuthorUtil.button(delte,'delteButton','','删除'));	

			add && this.rowActionButtonsForCompanyNode.push(AuthorUtil.button(add,'addButtonByCompany','fa fa-plus','添加'));		
		},
		initDom:function() {
			this.$treeContainer = $("#departmentTreeContainer");
			this.$dataListTpl = $('#dataListTableTpl');
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");
		},
		initEvent:function() {
			var self = this;
			
			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			this.searchData();  
			this.renderCommonActionButton();

			this.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			this.$treeContainer.on('click', '.addButtonByCompany', function(event) {
				event.preventDefault();
				var subCompanyId = $(this).data('companyid');
				AddDepartment.init({
					subCompanyId:subCompanyId,
					callBack:function() {
						self.searchData();
					}
				})
			});

			this.$treeContainer.on('click', '.addButton', function(event) {
				event.preventDefault();
				event.preventDefault();
				var parentDepartmentId = $(this).data('departmentid'), subCompanyId = $(this).data('companyid');
				AddDepartment.init({
					parentDepartmentId:parentDepartmentId,
					subCompanyId:subCompanyId,
					callBack:function() {
						self.searchData();
					}
				})
			});

			this.$treeContainer.on('click', '.editButton', function(event) {
				event.preventDefault();
				var departmentid = $(this).data('departmentid');
				EditDepartment.init({
					departmentId:departmentid,
					callBack:function() {
						self.searchData();
					}
				})
			});

			this.$treeContainer.on('click', '.delteButton', function(event) {
				event.preventDefault();
				var departmentid = $(this).data('departmentid');
				bootbox.confirm('确认删除该部门？',function(result) {
					result && self.del(departmentid);
				})
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			this.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function() {
			var self = this;
			Rental.ajax.ajaxData('company/getCompanyDepartmentTree', {}, '加载组织架构', function(response) {
				self.initCompanyDepartmentTree(response.resultMap.data.subCompanyList);
			});
		},
		initCompanyDepartmentTree:function(data) {
			var tree = this.toTree(data);

			var tpl = this.$dataListTpl.html();
			this.$treeContainer.html(tpl);

			this.initTree($('.table', this.$treeContainer), tree);
		},
		initTree:function($tree, source) {
			var self = this;

			$tree.fancytree({
			      extensions: ["table"],
			      //checkbox: true,
			      table: {
			        indentation: 20,      // indent 20px per node level
			        nodeColumnIdx:1,     // render the node title into the 2nd column
			        checkboxColumnIdx: 0 // render the checkboxes into the 1st column
			      },
			      source: source,
			      renderColumns: function(event, data) {
			        var node = data.node;
		          	$tdList = $(node.tr).find(">td");
			        $tdList.eq(0).text(node.getIndexHier()).addClass("alignRight");
			        $tdList.eq(2).text(node.key);

			        if(node.data.nodeType == self.nodeType.department) {
			        	$tdList.eq(3).html(self.renderRowButtonsForDeparmentNode(node.key, node.data.department.subCompanyId));	
			        } else if(node.data.nodeType == self.nodeType.company) {
			        	$tdList.eq(3).html(self.renderRowButtonsForCompanyNode(node.key));	
			        }
			      }
		    });
		},
		renderRowButtonsForDeparmentNode:function(departmentid, companyid) {
			var self = this, btnGroup = '<div class="btn-group text-right">{0}</div>';
			var btns = self.rowActionButtons.map(function(item) {
				return ' <a href="{0}{1}" type="button" class="btn btn-default btn-xs {2}" data-departmentid="{3}" data-companyid="{5}">{4}</a>'.format(SitePath.base, item.menuUrl, item.class, departmentid, item.text, companyid);
			});
			return btnGroup.format(btns.join(' '));
		},
		renderRowButtonsForCompanyNode:function(companyid) {
			var self = this, btnGroup = '<div class="btn-group text-right">{0}</div>';
			var btns = self.rowActionButtonsForCompanyNode.map(function(item) {
				return ' <a href="{0}{1}" type="button" class="btn btn-default btn-xs {2}" data-companyid="{3}">{4}</a>'.format(SitePath.base, item.menuUrl, item.class, companyid, item.text);
			});
			return btnGroup.format(btns.join(' '));
		},
		toTree:function(data) {

			var self = this, tree = new Array();

			function dt(tr, department) {
				for(var i in department) {
					var curr = department[i];
					var item = {
						expanded:true,
						//checkbox: false,
						folder: true,
						icon:SitePath.staticCommon + 'rental/img/tree/department.png',
						key:curr.departmentId,
						title:curr.departmentName,
						data:{
							department:curr,
							nodeType:self.nodeType.department
						}
					};
					tr.push(item);

					if(curr.hasOwnProperty('children')) {
						item.children = item.children || new Array();
						dt(item.children,curr.children);
					}
				}
			}

			for(var i in data) {
				var sub = data[i];
				var item = {
					expanded:true,
					// checkbox: false,
					folder: true,
					icon:SitePath.staticCommon + 'rental/img/tree/company.png',
					title:sub.subCompanyName,
					key:sub.subCompanyId,
					data:{
						company:sub,
						nodeType:self.nodeType.company
					}
				}

				if(sub.hasOwnProperty('departmentList')) {
					item.children = new Array();
					dt(item.children, sub.departmentList);
				}

				tree.push(item);
			}

			return tree;
		},
		del:function(departmentId) {
			var self = this;
			if(!departmentId) {
				bootbox.alert('找不到部门ID');
				return;
			}
			Rental.ajax.submit("{0}company/deleteDepartment".format(SitePath.service),{departmentId:departmentId},function(response){
				if(response.success) {
					Rental.notification.success("删除部门",response.description || '成功');
					self.searchData();
				} else {
					Rental.notification.error("删除部门",response.description || '失败');
				}
			}, null , "删除部门");
		}
	};

	window.DepartmentManage = DepartmentManage;

})(jQuery);