;(function($){
	var AddDepartment = {
		init:function(prams) {
			this.props =_.extend({
				parentDepartmentId:null,
				subCompanyId:null,
				callBack:function () {}
			}, prams || {});

			this.showModal();
		},
		showModal:function(){
			var self = this;
			Rental.modal.open({src:SitePath.base + "user-manage/departmentAdd", type:'ajax', closeOnBgClick:false, ajaxContentAdded:function(pModal) {
				self.initDom();
				self.initEvent();	
			}});
		},
		initDom:function() {
			this.$form = $("#addDepartmentForm");		
			this.$cancelButton = $("#cancelAddDeparmentBtn");
		},
		initEvent:function() {
			var self = this;

			self.findDataByType();
			
			Rental.form.initFormValidation(this.$form,function(form) {
				self.add();
			});

			self.$cancelButton.on('click', function(event) {
				event.preventDefault();
				Rental.modal.close();
			});
		},
		findDataByType:function() {
			var self = this;
			Rental.ajax.submit("{0}data/findDataByType".format(SitePath.service),{dataType:1},function(response){
				if(response.success) {
					self.renderDepartmentType(response.resultMap.data);
				} else {
					Rental.notification.error("加载部门类型",response.description || '失败');
				}
			}, null , "加载部门类型");			
		},
		renderDepartmentType:function(data) {
			var self = this;
			Rental.ui.renderSelect({
				data:data,
				container:$('[name=departmentType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.value, item.label);
				}
			});
		},
		add:function() {
			var self = this;
			var formData = Rental.form.getFormData(self.$form);

			var commmitData = {
				departmentName:formData['departmentName'],
				departmentType:formData['departmentType'],
			}

			if(!self.props.parentDepartmentId && !self.props.subCompanyId) {
				bootbox.alert('找不到部门所属上级');
				return;
			}

			!!self.props.parentDepartmentId && (commmitData.parentDepartmentId = self.props.parentDepartmentId);
			!!self.props.subCompanyId && (commmitData.subCompanyId = self.props.subCompanyId);

			Rental.ajax.submit("{0}company/addDepartment".format(SitePath.service),commmitData,function(response){
				if(response.success) {
					Rental.notification.success("添加部门",response.description || '成功');
					self.callBackFunc(response);
				} else {
					Rental.notification.error("添加部门",response.description || '失败');
				}
			}, null , "添加部门", 'dialogLoading');
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				Rental.modal.close();
				self.props.callBack();
			}
			return false;
		},
	};
	window.AddDepartment = AddDepartment;
})(jQuery);