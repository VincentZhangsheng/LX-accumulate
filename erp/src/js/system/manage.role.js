;(function($){

	var RoleManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_role_list];
			this.initActionButtons();
		},
		initActionButtons:function() {
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var author = this.currentPageAuthor.children, 
				add = author[AuthorCode.manage_role_list_add],
				edit = author[AuthorCode.manage_role_list_eidt],
				delte = author[AuthorCode.manage_role_list_delete],
				accessFunction = author[AuthorCode.manage_role_list_access_function], //功能授权
				accessData = author[AuthorCode.manage_role_list_access_data]; //数据授权

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			accessFunction && this.commonActionButtons.push(AuthorUtil.button(accessFunction,'accessFunctionButton','fa fa-shield','功能授权'));
			accessData && this.commonActionButtons.push(AuthorUtil.button(accessData,'accessDataButton','fa fa-shield','数据授权'));

			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));			
			accessFunction && this.rowActionButtons.push(AuthorUtil.button(accessFunction,'accessFunctionButton','','功能授权'));			
			accessData && this.rowActionButtons.push(AuthorUtil.button(accessData,'accessDataButton','','数据授权'));			
			delte && this.rowActionButtons.push(AuthorUtil.button(delte,'delteButton','','删除'));			
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {
			
			var self = this;
			
			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

			DepartmentTree.init({
				callBack:function(department, company) {
					var prams = new Object();
					self.searchData({
						departmentId: department ? department.departmentId : '',
						subCompanyId: company ? company.subCompanyId : '',
					});
				}
			});
			
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			this.searchData();  

			this.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			this.$actionCommonButtons.on('click', '.addButton', function(event) {
				event.preventDefault();
				AddRole.init(function() {
					self.doSearch();
				});
			});

			this.$actionCommonButtons.on('click', '.accessFunctionButton', function(event) {
				event.preventDefault();
				var array = $('[name=checkItem]').rtlCheckArray();
				if(array.length == 0) {
					bootbox.alert('请选择角色');
					return;
				}
				if(array.length > 1) {
					bootbox.alert('只能选择一个');
					return;
				}
				RoleAccessFunction.init(array[0],function() {
					self.doSearch(self.Pager.pagerData.currentPage);
				});
			});

			this.$actionCommonButtons.on('click', '.accessDataButton', function(event) {
				event.preventDefault();
				var array = $('[name=checkItem]').rtlCheckArray();
				if(array.length == 0) {
					bootbox.alert('请选择角色');
					return;
				}
				if(array.length > 1) {
					bootbox.alert('只能选择一个');
					return;
				}
				RoleAccessData.init(array[0],function() {
					self.doSearch(self.Pager.pagerData.currentPage);
				});
			});

			$("#submitSearchForm").on("click",function() {
				self.searchData();
			})

			this.$dataListTable.on('click', '.editButton', function(event) {
				event.preventDefault();
				var roleId = $(this).data("roleid");
				EditRole.init(roleId,self.doSearch,self.Pager.pagerData.currentPage);
			});

			this.$dataListTable.on('click', '.delteButton', function(event) {
				event.preventDefault();
				var roleId = $(this).data("roleid");
				bootbox.confirm("确定需要删除该角色？", function(result){ 
					if(!result) return;
					self.deleteRole(roleId);
				});
			});

			this.$dataListTable.on('click', '.accessFunctionButton', function(event) {
				event.preventDefault();
				var roleId = $(this).data("roleid");
				RoleAccessFunction.init(roleId,self.doSearch,self.Pager.pagerData.currentPage);
			});

			this.$dataListTable.on('click', '.accessDataButton', function(event) {
				event.preventDefault();
				var roleId = $(this).data("roleid");
				RoleAccessData.init(roleId,self.doSearch,self.Pager.pagerData.currentPage);
			});

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

			this.renderCommonActionButton();

		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			this.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			self.commitData = _.extend(self.commitData || {}, {
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(RoleManage.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}/userRole/page".format(SitePath.service),self.commitData,function(response){
				if(response.success) {
					RoleManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询角色列表",response.description || '查询失败');
				}
			}, null ,"查询角色列表",'listLoading');
		},
		doSearch:function(pageNo) {
			RoleManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSearch);
		},
		renderList:function(data) {
			var self = this;

			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:{
					"listData":listData,
					"isSuperAdmin":function() {
						return this.isSuperAdmin == 1 ? "是":"否";
					},
					department:function() {
						return this.subCompanyName + " " + this.departmentName;
					}
				}
			}
			
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
		deleteRole:function(roleId) {
			var self = this;
			Rental.ajax.submit("{0}/userRole/delete".format(SitePath.service),{roleId:roleId},function(response){
				if(response.success) {
					Rental.notification.success("删除角色",response.description || '成功');
					self.doSearch(self.Pager.pagerData.currentPage);
				} else {
					Rental.notification.error("删除角色",response.description || '失败');
				}
			}, null ,"删除角色", 'dialogLoading');
		}
	};

	window.RoleManage = RoleManage;

})(jQuery);