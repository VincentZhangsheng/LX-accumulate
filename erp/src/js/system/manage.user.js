var UserManage = {
	init:function() {
		this.initAuthor();
		this.initDom();
		this.initEvent();
	},
	initAuthor:function() {
		this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_user];
		this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_user_list];
		this.initActionButtons();
	},
	initActionButtons:function() {
		this.commonActionButtons = new Array();
		this.rowActionButtons = new Array();

		if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

		var author = this.currentPageAuthor.children, 
			add = author[AuthorCode.manage_user_list_add],
			edit = author[AuthorCode.manage_user_list_eidt],
			view = author[AuthorCode.manage_user_list_view],
			uppwd = author[AuthorCode.manage_user_list_uppassword],
			accessData = author[AuthorCode.manage_user_list_access_data];

		add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
		accessData && this.commonActionButtons.push(AuthorUtil.button(accessData,'accessDataButton','fa fa-shield','数据授权'));

		view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
		edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
		uppwd && this.rowActionButtons.push(AuthorUtil.button(uppwd,'uppwdButton','','修改密码'));
		accessData && this.rowActionButtons.push(AuthorUtil.button(accessData,'accessDataButton','','数据授权'));
	},
	initDom:function() {
		this.$searchForm = $("#searchForm");
		this.$dataListTpl = $("#dataListTpl").html();
		this.$dataListTable = $("#dataListTable");
		this.$actionCommonButtons = $("#actionCommonButtons");

		this.Pager = new Pager();
	},
	initEvent:function() {
		
		var self = this;
		
		Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑

		DepartmentTree.init({
			callBack:function(department, company) {
				var prams = new Object();
				self.searchData({
					departmentId: department ? department.departmentId : '',
					subCompanyId: company ? company.subCompanyId : '',
				});
			}
		});
		
		this.searchData();  

		Rental.form.initSearchFormValidation(this.$searchForm,function(){
			self.searchData();
		});

		$("#submitSearchForm").on('click',function(event){
			event.preventDefault();
			self.searchData();  
		});

		this.$actionCommonButtons.on("click", '.refreshButton', function(event) {
			event.preventDefault();
			self.searchData();
		});

		self.$searchForm.on('change', '[name=isDisabled]', function(event) {
			event.preventDefault();
			self.searchData();
		});

		this.$actionCommonButtons.on('click', '.addButton', function(event) {
			event.preventDefault();
			AddUser.init(function() {
				self.doSearch();
			});
		});

		this.$actionCommonButtons.on('click', '.accessDataButton', function(event) {
			event.preventDefault();
			var array = $('[name=checkItem]').rtlCheckArray();
				if(array.length == 0) {
					bootbox.alert('请选择用户');
					return;
				}
				if(array.length > 1) {
					bootbox.alert('只能选择一个');
					return;
				}
			UserAccessData.init(array[0],function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		});

		this.$dataListTable.on('click', '.viewButton', function(event) {
			event.preventDefault();
			var userId = $(this).data("userid");
			ViewUser.init(userId);
		});

		this.$dataListTable.on('click', '.editButton', function(event) {
			event.preventDefault();
			var userId = $(this).data("userid");
			EditUser.init(userId,function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});
		});

		this.$dataListTable.on('click', '.uppwdButton', function(event) {
			event.preventDefault();
			var userId = $(this).data("userid");
			UpdateUserPassword.init(userId,UserManage.doSearch,self.Pager.pagerData.currentPage);
		});

		this.$dataListTable.on('click', '.accessDataButton', function(event) {
			event.preventDefault();
			var userId = $(this).data("userid");
			UserAccessData.init(userId,function() {
				self.doSearch(self.Pager.pagerData.currentPage)
			});
		});

		self.$dataListTable.rtlCheckAll('checkAll','checkItem');

		self.renderCommonActionButton();

	},
	renderCommonActionButton:function() {
		var self = this;
		var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
		Mustache.parse(actionCommonButtonsTpl);
		this.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
	},
	searchData:function(prams) {
		var self = this;

		self.commitData = _.extend(self.commitData || {}, {
			pageNo:1,
			pageSize:self.Pager.defautPrams.pageSize,
		}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});
		
		Rental.ajax.submit("{0}/user/page".format(SitePath.service),self.commitData,function(response){
			if(response.success) {
				UserManage.render(response.resultMap.data);	
			} else {
				Rental.notification.error("查询用户信息",response.description || '查询失败');
			}
		}, null ,"查询用户信息",'listLoading');
	},
	doSearch:function(pageNo) {
		UserManage.searchData({pageNo:pageNo || 1});
	},
	render:function(data) {
		this.renderList(data);
		this.Pager.init(data,this.doSearch);
	},
	renderList:function(data) {
		var self = this;
		
		var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
			hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
			hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

		var data  = {
			hasCommonActionButtons:hasCommonActionButtons,
			hasRowActionButtons:hasRowActionButtons,
			rowActionButtons:self.rowActionButtons,
			dataSource:_.extend(Rental.render, {
				"listData":listData,
				roleListData:function() {
					return this.roleList.map(function(item){
						return {
							role:item.subCompanyName + " " + item.departmentName + " " + item.roleName,
						}
					})
				},
				department:function() {
					return (this.roleList || []).map(function(item){
						return item.departmentName;
					}).join(', ');
				},
				role:function() {
					return (this.roleList || []).map(function(item){
						return item.roleName;
					}).join(', ');
				}
			})
		}
		Mustache.parse(this.$dataListTpl);
		this.$dataListTable.html(Mustache.render(this.$dataListTpl,data));
	}
};