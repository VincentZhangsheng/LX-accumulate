;(function($) {

	var PeerOrderHandleMixin = {
		initHandleEvent:function(container, handleCallBack, prams) {
			var self = this;

			self.$materialDataListTable && self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			// container.on('click', '.submitButton', function(event) {
			// 	event.preventDefault();
			// 	var no = $(this).data('no');
			// 	self.isNeedVerify(no, Enum.workflowType.num.peerBorrowIn, handleCallBack);
			// });

			container.on('click', '.submitButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.peerBorrowIn,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit, Enum.workflowType.num.peerBorrowIn);
				})
			});

			container.on('click', '.cancelButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.cancel(no, handleCallBack);
			});

			container.on('click', '.confirmButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.confirmReceipt(no, handleCallBack);
			});

			// container.on('click', '.returnButton', function(event) {
			// 	event.preventDefault();
			// 	var no = $(this).data('no');
			// 	self.isNeedVerify(no, Enum.workflowType.num.peerReturn, handleCallBack);
			// });

			container.on('click', '.returnButton', function(event) {
				event.preventDefault();
				ApiData.isNeedVerify({
					no:$(this).data('no'),
					workflowType:Enum.workflowType.num.peerReturn,
				}, function(result, isAudit) {
					self.submitOrder(result, handleCallBack, isAudit, Enum.workflowType.num.peerReturn);
				})
			});

			container.on('click', '.confirmReturnButton', function(event) {
				event.preventDefault();
				var no = $(this).data('no');
				self.confirmReturn(no, handleCallBack);
			});

			container.on('click', '.viewEquimentButton', function(event) {
				event.preventDefault();
				var peerDeploymentOrderProductId = $(this).data('itemid');
				PeerOrderEquimentList.init({
					id:peerDeploymentOrderProductId,
					callBack:function() {
						self.loadData();
					}
				});
			});

			container.on('click', '.viewMaterialButton', function(event) {
				event.preventDefault();
				var peerDeploymentOrderMaterialId = $(this).data('itemid');
				PeerOrderMaterialList.init({
					id:peerDeploymentOrderMaterialId,
					callBack:function() {
						self.loadData();
					}
				});
			});
		},
		// isNeedVerify:function(no, workflowType, callBack) {
		// 	if(!no) {
		// 		bootbox.alert('找不到流转单编号');
		// 		return;
		// 	}
		// 	var self = this;
		// 	Rental.ajax.submit("{0}workflow/isNeedVerify".format(SitePath.service), {workflowType:workflowType}, function(response){
		// 		if(response.success) {
		// 			var url = '', des = '提交审核';
		// 			if(workflowType ==  Enum.workflowType.num.peerBorrowIn) {
		// 				url = 'peerDeploymentOrder/commitPeerDeploymentOrderInto';
		// 				des = '提交同行调拨单';
		// 			} else if(workflowType ==  Enum.workflowType.num.peerReturn) {
		// 				url = 'peerDeploymentOrder/commitPeerDeploymentOrderReturn';
		// 				des = '归还同行调拨单提交审核';
		// 			}
		// 			if(response.resultMap.data) {
		// 				self.submittoAudit({ peerDeploymentOrderNo:no, workflowType:workflowType, url: url, description:des}, callBack);
		// 			} else {
		// 				self.submitOrder({peerDeploymentOrderNo:no}, url, des, callBack);
		// 			}
		// 		} else {
		// 			Rental.notification.error('请求提交', response.description || '失败');
		// 		}
		// 	}, null ,'请求提交');
		// },
		// submittoAudit:function(prams, callBack) {
		// 	var self = this;
		// 	SubmitAudit.init({
		// 		workflowType:prams.workflowType,
		// 		workflowReferNo:prams.peerDeploymentOrderNo,
		// 		callBack:function(res) {
		// 			var commitData = {
		// 				peerDeploymentOrderNo:prams.peerDeploymentOrderNo,
		// 				verifyUserId:res.verifyUser,
		// 				remark:res.commitRemark,
		// 			}
		// 			self.submitOrder(commitData, prams.url, prams.description, callBack, true);
		// 		}
		// 	});
		// },
		submitOrder:function(prams, callBack, isAudit, workflowType) {
			var self = this, url = '', des = '提交审核';
			if(workflowType ==  Enum.workflowType.num.peerBorrowIn) {
				url = 'peerDeploymentOrder/commitPeerDeploymentOrderInto';
				des = '提交同行调拨单';
			} else if(workflowType ==  Enum.workflowType.num.peerReturn) {
				url = 'peerDeploymentOrder/commitPeerDeploymentOrderReturn';
				des = '归还同行调拨单提交审核';
			}

			if(!isAudit) {
				bootbox.confirm('确认{0}？'.format(des), function(result) {
					result && self.orderDo(url, prams, des, callBack);
				});
				return;
			} else {
				self.orderDo(url, {
					peerDeploymentOrderNo:prams.no,
					verifyUserId:prams.verifyUserId,
					remark:prams.commitRemark,
					imgIdList:prams.imgIdList,
				}, des, function() {
					Rental.modal.close();
					callBack && callBack();
				});
			}
		},
		orderDo:function(url, prams, des, callBack) {
			if(!prams.peerDeploymentOrderNo) {
				bootbox.alert('找不到调拨单编号');
				return;
			}
			var self = this; 
			Rental.ajax.submit("{0}{1}".format(SitePath.service,url), prams, function(response) {
				if(response.success) {
					Rental.notification.success(des,response.description || '成功');
					callBack && callBack();
				} else {
					Rental.notification.error(des,response.description || '失败');
				}
			}, null , des, 'dialogLoading');
		},
		cancel:function(peerDeploymentOrderNo, callBack) {
			var self = this;
			bootbox.confirm('确认取消同行调拨单？', function(result) {
				result && self.orderDo('peerDeploymentOrder/cancel', {peerDeploymentOrderNo:peerDeploymentOrderNo}, '取消取消调拨单', callBack);
			});
		},
		confirmReceipt:function(peerDeploymentOrderNo, callBack) {	
			var self = this;
			bootbox.confirm('确认收货？', function(result) {
				result && self.orderDo('peerDeploymentOrder/confirmPeerDeploymentOrderInto', {peerDeploymentOrderNo:peerDeploymentOrderNo}, '确认收货', callBack);;
			});
		},
		confirmReturn:function(peerDeploymentOrderNo, callBack) {	
			var self = this;
			bootbox.confirm('确认归还？', function(result) {
				result && self.orderDo('peerDeploymentOrder/endPeerDeploymentOrderOut', {peerDeploymentOrderNo:peerDeploymentOrderNo}, '确认归还', callBack);;
			});
		},
		filterAcitonButtons:function(buttons, order) {
			var rowActionButtons = new Array();
			(buttons || []).forEach(function(button, index) {
				button.no = order.peerDeploymentOrderNo; //退还单编号
				rowActionButtons.push(button);
				var unSubmit = order.peerDeploymentOrderStatus == Enum.peerDeploymentOrderStatus.num.unSubmit,
					inHand = order.peerDeploymentOrderStatus == Enum.peerDeploymentOrderStatus.num.inHand,
					confirm = order.peerDeploymentOrderStatus == Enum.peerDeploymentOrderStatus.num.confirm;


				if(_.indexOf(['submitButton', 'editButton'], button.class) > -1 && !unSubmit) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf(['confirmButton'], button.class) > -1 && !inHand) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(_.indexOf(['returnButton'], button.class) > -1 && !confirm) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
				
				if(_.indexOf(['confirmReturnButton'], button.class) > -1 && order.peerDeploymentOrderStatus != Enum.peerDeploymentOrderStatus.num.returnInHand) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}

				if(button.class == 'cancelButton' && !(unSubmit || order.peerDeploymentOrderStatus == Enum.peerDeploymentOrderStatus.num.inReview)) {
					rowActionButtons.splice(rowActionButtons.length - 1,1);
				}
			});
			return rowActionButtons;
		},
	}

	window.PeerOrderHandleMixin = PeerOrderHandleMixin;

})(jQuery);