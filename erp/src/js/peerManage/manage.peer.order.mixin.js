;(function($) {

	PeerOrderMixIn = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			peerDeploymentOrderConsignInfo:null,
		},
		initCommonEvent:function() {
			var self = this;

			$("#choosePeerSupplier").on('click', function(event) {
				event.preventDefault();
				PeerSupplierChoose.init({
					callBack:function(peerSupplier) {
						console.log(peerSupplier);
						$('[name=peerNo]', self.$form).val(peerSupplier.peerNo);
						$('[name=peerName]', self.$form).val(peerSupplier.peerName);
					}
				})
			});

			//选择仓库
			$("#chooseWarehouseId").on('click',function(event) {
				WarehouseChoose.init({
					callBack:function(warehouse) {
						console.log(warehouse)
						$('[name=warehouseNo]', self.$form).val(warehouse.warehouseNo);
						$('[name=warehouseName]', self.$form).val(warehouse.warehouseName);
					}
				});	
			});

			Rental.ui.events.initDatePicker($("#rentStartTimePicker"), $("#rentStartTime"));

			Rental.ui.renderSelect({
				data:Enum.deliveryMode.withOutLX(),
				container:$('[name=deliveryMode]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			Rental.ui.renderSelect({
				data:Enum.array(Enum.rentType),
				container:$('[name=rentType]', self.$form),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
				}
			});

			$("#editAddressButton").click(function(event) {
				event.preventDefault();
				InputAddress.init({
					initFunc:function(modal) {
						Rental.ui.renderFormData($('.inputAddressForm', modal), _.extend(self.state.peerDeploymentOrderConsignInfo, {
							consigneeName:self.state.peerDeploymentOrderConsignInfo.contactName,
							consigneePhone:self.state.peerDeploymentOrderConsignInfo.contactPhone,
						}));
						InputAddress.Area.setDefaultValue({
							province:self.state.peerDeploymentOrderConsignInfo.province,
							city:self.state.peerDeploymentOrderConsignInfo.city,
							district:self.state.peerDeploymentOrderConsignInfo.district,
						});
					},
					callBack:function(address) {
						self.chooseAddress(address);
						return true;
					}
				})
			});

			$("#userPeerDefaultAddress").click(function(event) {
				event.preventDefault();
				var peerNo = $("[name=peerNo]").val();
				if(!peerNo) {
					bootbox.alert('请选择同行');
					return;
				}
				self.userPeerDefaultAddress(peerNo);
			}); 

			self.renderProductList();
			self.renderMaterialList();

			Rental.ui.events.imgGallery(this.$dataListTable);
			Rental.ui.events.imgGallery(this.$materialDataListTable);

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
			self.$materialDataListTable.rtlCheckAll('checkAll','checkItem');

			$("#batchAddProduct").click(function(event) {
				ProductChoose.modal({
					isVerifyStock:false, //是否验证库存
					showIsNewCheckBox:true,
					callBack:function(product) {
						self.chooseProduct(product);
					}
				});
			});

			$('#batchAddMaterial').click(function(event) {
				event.preventDefault();
				MaterialChoose.init({
					btach:true,
					searchParms:new Object(), 
					showIsNewCheckBox:true,
					isVerifyStock:false, //是否验证库存
					callBack:function(material) {
						self.chooseMaterial(material);
					}
				});
			});

			$("#batchDeleteProduct").click(function(event) {
				self.batchDeleteProduct();
				self.renderProductList();
			});

			self.$dataListTable.on('click', '.deleteSKUButton', function(event) {
				event.preventDefault();
				self.deleteSku($(this));
				self.renderProductList();
			});

			self.$dataListTable.on('change', '.productUnitAmount,.productSkuCount', function(event) {
				event.preventDefault();
				var $skuRow = $(this).closest('.skuRow');
				self.changeProductEvent($skuRow);
			});

			$("#batchDeleteMaterial").on('click', function(event) {
				event.preventDefault();
				self.batchDeleteMaterial();
			});

			self.$materialDataListTable.on('click', '.delMaterialButton', function(event) {
				event.preventDefault();
				var materialNo = $(this).data('materialno');
				self.deleteMaterial(materialNo);
			});

			self.$materialDataListTable.on('change', '.materialUnitAmount,.materialCount', function(event) {
				event.preventDefault();
				var $materialRow = $(this).closest('.materialRow');
				self.changeMaterialEvent($materialRow);
			});
		},
		getAddress:function() {
			if(!this.state.peerDeploymentOrderConsignInfo) {
				bootbox.alert('请选择归还地址');
				return;
			}
			var address = this.state.peerDeploymentOrderConsignInfo;
			return {
				contactName:address.contactName,
				contactPhone:address.contactPhone,
				province:address.province,
				city:address.city,
				district:address.district,
				address:address.address,
			}
		},
		chooseAddress:function(address) {
			console.log(address)
			this.state.peerDeploymentOrderConsignInfo = _.extend(address, {
				contactName:address.consigneeName,
				contactPhone:address.consigneePhone,
			});
			this.renderPeerDeploymentOrderConsignInfo();
		},
		userPeerDefaultAddress:function(no) {
			var self = this;
			ApiData.peerSupplierDetial({
				peerNo:no,
				success:function(response) {
					self.state.peerDeploymentOrderConsignInfo = response;
					self.renderPeerDeploymentOrderConsignInfo();
				}
			});
		},
		renderPeerDeploymentOrderConsignInfo:function() {
			var self = this,
				data = {
					address: self.state.peerDeploymentOrderConsignInfo,
				};
			var tpl = $('#orderConsignInfoTpl').html();
			Mustache.parse(tpl);
			$('#orderConsignInfo').html(Mustache.render(tpl, data));
		},
		renderProductList:function(rowActionButtons){
			OrderManageItemRender.renderProductList(this.state.chooseProductList, this.$dataListTpl, this.$dataListTable, rowActionButtons);
		},
		renderMaterialList:function(rowActionButtons) {
			OrderManageItemRender.renderMaterialList(this.state.chooseMaterialList, this.$materialDataListTpl, this.$materialDataListTable, rowActionButtons);
		},
		chooseProduct:function(product) {
			this.state.chooseProductList = OrderManageUtil.chooseProductByIsNew(this.state.chooseProductList, product);
			this.renderProductList();
			bootbox.confirm({
			    message: "成功选择商品",
			    buttons: {
			        confirm: {
			            label: '完成',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: '继续',
			            className: 'btn-default'
			        }
			    },
			    callback: function (result) {
			        if(result) {
			        	Rental.modal.close();
			        }
			    }
			});
		},
		changeProductEvent:function($skuRow) {
			this.state.chooseProductList = this.changeProduct(this.state.chooseProductList, $skuRow);
			this.renderProductList();
		},
		getSkuBySkuRow:function($skuRow) {
			return {
				productId:$('.productId', $skuRow).val(),
				productSkuId:$('.productSkuId', $skuRow).val(),
				productUnitAmount:$('.productUnitAmount', $skuRow).val(),
				productSkuCount:$('.productSkuCount', $skuRow).val(),
				isNew:parseInt($('.isNewProduct', $skuRow).val()),
			};
		},
		changeProduct:function(chooseProductList, $skuRow) {
			var sku = this.getSkuBySkuRow($skuRow);
			var productIndex = _.findIndex(chooseProductList, {productId:parseInt(sku.productId)});
			var skuIndex = _.findIndex(chooseProductList[productIndex].chooseProductSkuList, {skuId:parseInt(sku.productSkuId), isNew:parseInt(sku.isNew)});
			chooseProductList[productIndex].chooseProductSkuList[skuIndex] = _.extend(chooseProductList[productIndex].chooseProductSkuList[skuIndex], sku);
			return chooseProductList;
		},
		batchDeleteProduct:function() {
			this.state.chooseProductList = OrderManageUtil.batchDelete(this.$dataListTable, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		deleteSku:function($deleteButton) {
			this.state.chooseProductList = OrderManageUtil.deleteSku($deleteButton, this.state.chooseProductList);
			if(this.state.chooseProductList.length == 0) {
				this.renderProductList();	
			}
		},
		chooseMaterial:function(material) {
			this.state.chooseMaterialList = OrderManageUtil.chooseMaterialByIsNew(this.state.chooseMaterialList, material);
			this.renderMaterialList();
		},
		changeMaterialEvent:function($materialRow) {
			this.state.chooseMaterialList = this.changeMaterial(this.state.chooseMaterialList, $materialRow);
			this.renderMaterialList();
		},
		getMaterialByMaterialRow:function($materialRow) {
			return  {
				materialNo:$('.materialNo', $materialRow).val(),
				materialId:$('.materialId', $materialRow).val(),
				materialUnitAmount:$('.materialUnitAmount', $materialRow).val(),
				materialCount:$('.materialCount', $materialRow).val(),
				isNew:parseInt($('.isNewMaterial', $materialRow).val()),
			};
		},
		changeMaterial:function(chooseMaterialList, $materialRow) {
			var material = this.getMaterialByMaterialRow($materialRow);
			var index = _.findIndex(chooseMaterialList, {materialNo:material.materialNo, isNew:parseInt(material.isNew)});
			chooseMaterialList[index] = _.extend(chooseMaterialList[index], material);
			return chooseMaterialList;
		},
		batchDeleteMaterial:function() {
			this.state.chooseMaterialList = OrderManageUtil.batchDeleteMaterial(this.$materialDataListTable, this.state.chooseMaterialList);
			this.renderMaterialList();
		},
		deleteMaterial:function(materialNo) {
			var  index = _.findIndex(this.state.chooseMaterialList,{materialNo:materialNo});
			this.state.chooseMaterialList.splice(index,1);			
			this.renderMaterialList();
		},
		resolveChooseProductList:function(data) {
			var chooseProductList = new Array();  //已经选择的商品集合
			if(data) {
				var orderItemsMapByProductId = data.reduce(function(pre,item) {
					var product =  item.hasOwnProperty('productSkuSnapshot') ? JSON.parse(item.productSkuSnapshot) : null;
					if(product) {
						item = _.extend(product.productSkuList[0], item)
					} 
					if(!pre[item.productId]) {
						pre[item.productId] = {
							chooseProductSkuList:new Array()
						};
					}
					pre[item.productId].chooseProductSkuList.push(item);
					pre[item.productId] = _.extend(pre[item.productId], product);
			
					return pre;
				},{});

				chooseProductList = _.values(orderItemsMapByProductId);
			}
			return chooseProductList;
		},
		resolveChooseMaterialList:function(data) {
			var chooseMaterialList = new Array();
			if(data) {
				chooseMaterialList = data.map(function(item) {
					var material = JSON.parse(item.materialSnapshot);
					return _.extend(material, item);
				});
			}
			return chooseMaterialList;
		},
	};

	window.PeerOrderMixIn = PeerOrderMixIn;

})(jQuery);