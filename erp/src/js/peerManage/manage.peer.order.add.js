/**
* 添加调拨同行单
*/
;(function($){

	var AddPeerOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_add];
		},
		initDom:function() {
			this.$form = $("#addPeerOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_peer_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.add();
			});

			self.initCommonEvent(); // from order mixin
		},
		add:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					peerNo:formData['peerNo'],
					// totalDiscountAmount:formData['totalDiscountAmount'],
					taxRate:formData['taxRate'],
					warehouseNo:formData['warehouseNo'],
					deliveryMode:formData['deliveryMode'],
					rentStartTime:new Date(formData['rentStartTime']).getTime(),
					rentType:formData['rentType'],
					rentTimeLength:formData['rentTimeLength'],
					remark:formData['remark'],
				}

				if(!self.state.peerDeploymentOrderConsignInfo) {
					bootbox.alert('请添加归还地址');
					return;
				}

				commitData.peerDeploymentOrderConsignInfo = self.getAddress();

				var peerDeploymentOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						peerDeploymentOrderProductList.push({
							productSkuId:sku.skuId,
							productSkuCount:sku.productSkuCount,
							productUnitAmount:sku.productUnitAmount,
							isNew:sku.isNew,
						})
					})
				});

				var peerDeploymentOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					peerDeploymentOrderMaterialList.push({
						// materialId:material.materialId,
						materialNo:material.materialNo,
						materialCount:material.materialCount,
						materialUnitAmount:material.materialUnitAmount,
						isNew:material.isNew,
					})
				});

				if(peerDeploymentOrderProductList.length == 0 && peerDeploymentOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(peerDeploymentOrderProductList.length > 0) {
					commitData.peerDeploymentOrderProductList = peerDeploymentOrderProductList;
				}

				if(peerDeploymentOrderMaterialList.length > 0) {
					commitData.peerDeploymentOrderMaterialList = peerDeploymentOrderMaterialList;
				}

				var description = "创建同行调拨单";
				Rental.ajax.submit("{0}peerDeploymentOrder/create".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null , description, 'dialogLoading');
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功创建同行调拨单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	self.$form[0].reset();
				        	window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.AddPeerOrder = _.extend(AddPeerOrder, PeerOrderMixIn);

})(jQuery);