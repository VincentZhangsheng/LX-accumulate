;(function($){
	
	var EditSupplier = {
		state:{
			no:null,
		},
		init:function(prams) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_supplier_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_supplier_list_edit];
		},
		initDom:function() {
			var self = this;

			this.$form = $("#editSupplierForm");
			this.$cancelButton = $('.cancelButton', this.$form);

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_supplier_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到供应商编号');
				return;
			}
			Rental.ajax.ajaxData('supplier/getSupplierByNo', {supplierNo:self.state.no}, '加载供应商', function(response) {
				self.initFormData(response.resultMap.data);
			});
		},
		initFormData:function(data) {
			if(!data) return;

	        Rental.ui.renderFormData(this.$form, data);
		
			this.Area.setDefaultValue({
				province:data.province,
				city:data.city,
				district:data.district,
			});
		},
		edit:function() {
			try {
				var self = this;
				
				if(!self.state.no) {
					bootbox.alert('没有找到供应商编号');
					return;
				}
				
				var formData = Rental.form.getFormData(self.$form), des = '编辑企业客户';
				formData.supplierNo = self.state.no;

				Rental.ajax.submit("{0}supplier/update".format(SitePath.service),formData,function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("编辑企业客户", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑供应商",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							// window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.EditSupplier = EditSupplier;

})(jQuery);