/**
* 添加同行
*/
;(function($){
	var AddPeerSupplier = {
		init:function(callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();	
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_supplier_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_peer_supplier_add];
		},
		initDom:function() {
			var self = this;

			this.$form = $("#addPeerSupplierForm");

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {
				}
			});
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_peer_supplier_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});
		},
		add:function() {
			try {
				
				var self = this;
				var formData = Rental.form.getFormData(self.$form);

				var des = '添加同行';
				Rental.ajax.submit("{0}peer/addPeer".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("添加同行失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加同行",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.AddPeerSupplier = AddPeerSupplier;

})(jQuery);