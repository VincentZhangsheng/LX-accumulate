/**
* 添加同行
*/
;(function($){
	var PeerSupplierDetail = {
		state:{
			no:null,
		},
		init:function(callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();	
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_supplier_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_peer_supplier_detail];
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initDom:function() {
			var self = this;

			this.$form = $("#detailPeerSupplierForm");

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {
				}
			});

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_peer_supplier_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});

			self.loadData();
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到同行编号');
				return;
			}
			Rental.ajax.ajaxData('peer/queryDetail', {peerNo:self.state.no}, '加载同行信息', function(response) {
				self.initFormData(response.resultMap.data);
			});
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$form, data);

			self.Area.setDefaultValue({
				province:data.province,
				city:data.city,
				district:data.district,
				disabled:true,
			});

			var array = self.$form.serializeArray();  
            $.each(array, function(i,item) {
	            $("[name="+item.name+"]", self.$form).prop({disabled:true});
	        });
		}
	};

	window.PeerSupplierDetail = PeerSupplierDetail;

})(jQuery);