;(function($){
	
	var AddSupplier = {
		init:function(callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_supplier_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_supplier_list_add];
		},
		initDom:function() {
			var self = this;

			this.$form = $("#addSupplierForm");
			this.$cancelButton = $('.cancelButton', this.$form);

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_supplier_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.add();
			});
		},
		add:function() {
			try {
				
				var self = this, 
					formData = Rental.form.getFormData(self.$form),
					des = '添加供应商';

				Rental.ajax.submit("{0}supplier/add".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("添加供应商", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功添加供应商",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续添加',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.AddSupplier = AddSupplier;

})(jQuery);