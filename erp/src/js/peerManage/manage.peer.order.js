/**
* 同行调拨单
*/
;(function($) {

	var PeerOrderManage = {
		state:{
			workflowType:null,
			transferOrderMode:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_order_list];
			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_peer_order_add],
				edit = this.currentPageAuthor.children[AuthorCode.manage_peer_order_edit],
				view = this.currentPageAuthor.children[AuthorCode.manage_peer_order_detail],
				submit = this.currentPageAuthor.children[AuthorCode.manage_peer_order_submit],
				cancel = this.currentPageAuthor.children[AuthorCode.manage_peer_order_cancel],
				confirm = this.currentPageAuthor.children[AuthorCode.manage_peer_order_confirm],
				returnOrder = this.currentPageAuthor.children[AuthorCode.manage_peer_order_return],
				confirmReturn = this.currentPageAuthor.children[AuthorCode.manage_peer_order_confirm_return];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.rowActionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			confirm && this.rowActionButtons.push(AuthorUtil.button(confirm,'confirmButton','','确认收货'));
			returnOrder && this.rowActionButtons.push(AuthorUtil.button(returnOrder,'returnButton','','归还'));
			confirmReturn && this.rowActionButtons.push(AuthorUtil.button(confirmReturn,'confirmReturnButton','','确认归还'));
			cancel && this.rowActionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.deploymentPeerOrder);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();

			this.state.workflowType = Enum.workflowType.num.transferInOrder;
			this.state.transferOrderMode = Enum.transferOrderMode.num.in;
		},
		initEvent:function() {
			var self = this;

			self.render(new Object());

			Breadcrumb.init([this.currentManageAuthor,this.currentPageAuthor]); //面包屑
			
			//绑定查询事件
			Rental.form.initSearchFormValidation(this.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据
			self.renderCommonActionButton();

			Rental.ui.renderSelect({
				container:$('#peerDeploymentOrderStatus'),
				data:Enum.array(Enum.peerDeploymentOrderStatus),
				func:function(opt, item) {
					return opt.format(item.num, item.value);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:'全部（调拨状态）'
			});

			//绑定刷新按钮事件
			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.initHandleEvent(this.$dataListTable, function() {
				self.doSearch(self.Pager.pagerData.currentPage);
			});

			self.renderWarehouse();
		},
		renderWarehouse:function() {
			var self = this;
			ApiData.warehouse({
				success:function(response) {
					self.renderWarehouseSelect($('#warehouseId'), '请选择调拨仓库', response);
				}
			});
		},
		renderWarehouseSelect:function(container, defaultText, response) {
			console.log(response);
			var self = this;
			Rental.ui.renderSelect({
				container:container,
				data:response,
				func:function(opt, item) {
					return opt.format(item.warehouseId, item.warehouseName);
				},
				change:function(val) {
					self.searchData();
				},
				defaultText:defaultText
			});
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;
			var searchData = $.extend({
				pageNo:1,
				pageSize:self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.deploymentPeerOrder, searchData);

			Rental.ajax.ajaxData('peerDeploymentOrder/page', searchData, '加载同行调拨单', function(response) {
				self.render(response.resultMap.data);	
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo)
			});
		},
		renderList:function(data) {
			var self = this,
				listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					detailUrl:function() {
						return '{0}?no={1}'.format(PageUrl.peerOrderDetail, this.peerDeploymentOrderNo);
					},
					rowActionButtons:function() {
						return self.filterAcitonButtons(self.rowActionButtons, this);
					},
					peerDeploymentOrderStatusValue:function(){
						return Enum.peerDeploymentOrderStatus.getValue(this.peerDeploymentOrderStatus);
					},
					peerDeploymentOrderStatusClass:function() {
						return Enum.peerDeploymentOrderStatus.getClass(this.peerDeploymentOrderStatus);
					},
					rentTypeValue:function() {
						return Enum.rentType.getValue(this.rentType);
					},
					rentTypeUnit:function() {
						return Enum.rentType.getUnit(this.rentType);
					},
					deliveryModeValue:function() {
						return Enum.deliveryMode.getValue(this.deliveryMode)
					}
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		},
	};

	window.PeerOrderManage = _.extend(PeerOrderManage, PeerOrderHandleMixin);

})(jQuery);