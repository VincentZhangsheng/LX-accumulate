;(function($){
	
	var SupplierDetail = {
		state:{
			no:null,
		},
		init:function(prams) {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_supplier_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_supplier_list_detail];
		},
		initDom:function() {
			var self = this;

			this.$form = $("#editSupplierForm");
			this.$cancelButton = $('.cancelButton', this.$form);

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {}
			});

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_supplier_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			self.loadData();

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到供应商编号');
				return;
			}
			Rental.ajax.ajaxData('supplier/getSupplierByNo', {supplierNo:self.state.no}, '加载供应商', function(response) {
				self.initFormData(response.resultMap.data);
			});
		},
		initFormData:function(data) {
			if(!data) return;

			var self = this;

	        Rental.ui.renderFormData(self.$form, data);
		
			self.Area.setDefaultValue({
				province:data.province,
				city:data.city,
				district:data.district,
				disabled:true,
			});

			var array = self.$form.serializeArray();  
            $.each(array, function(i,item) {
	            $("[name="+item.name+"]", self.$form).prop({disabled:true});
	        });
		}
	};

	window.SupplierDetail = SupplierDetail;

})(jQuery);