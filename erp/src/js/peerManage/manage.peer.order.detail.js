/**
* 编辑调拨同行单
*/
;(function($){

	var PeerOrderDetail = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			try {
				this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
				this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_order_list];
				this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_detail];
				this.initActionButtons();
			} catch (e) {
				window.location.href = PageUrl.noAccess;
			}
		},
		initActionButtons:function(){
			this.actionButtons = new Array();
			// this.rowActionButtons = new Array();
			this.rowProductActionuttons = new Array();
			this.rowMaterialActionuttons = new Array();

			if(this.currentManageListAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_add],
				edit = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_edit],
				view = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_detail],
				submit = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_submit],
				cancel = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_cancel],
				confirm = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_confirm],
				returnOrder = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_return],
				confirmReturn = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_confirm_return];

			edit && this.actionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			submit && this.actionButtons.push(AuthorUtil.button(submit,'submitButton','','提交'));
			confirm && this.actionButtons.push(AuthorUtil.button(confirm,'confirmButton','','确认收货'));
			returnOrder && this.actionButtons.push(AuthorUtil.button(returnOrder,'returnButton','','归还'));
			confirmReturn && this.actionButtons.push(AuthorUtil.button(confirmReturn,'confirmReturnButton','','确认归还'));
			cancel && this.actionButtons.push(AuthorUtil.button(cancel,'cancelButton','','取消'));

			view && this.rowProductActionuttons.push(AuthorUtil.button(view,'viewEquimentButton','','查看设备'));
			view && this.rowMaterialActionuttons.push(AuthorUtil.button(view,'viewMaterialButton','','查看散料'));
		},
		initDom:function() {
			this.$form = $("#detailPeerOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_peer_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.loadData();

			self.initCommonEvent(); // from order mixin

			self.initHandleEvent(this.$form, function() {
				self.loadData();
			});
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到同行调拨单编号');
				return;
			}
			Rental.ajax.submit("{0}peerDeploymentOrder/detailPeerDeploymentOrder".format(SitePath.service),{peerDeploymentOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载同行调拨单详情",response.description || '失败');
				}
			}, null ,"加载同行调拨单详情", 'listLoading');
		},
		initData:function(data) {
			this.renderActionButton(data);
			this.renderOrderBaserInfo(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);

			// this.state.peerDeploymentOrderConsignInfo = data.peerDeploymentOrderConsignInfo;
			// this.renderPeerDeploymentOrderConsignInfo();
		},
		renderActionButton:function(order) {
			var self = this,
				actionButtons = self.filterAcitonButtons(self.actionButtons, order),
				data = {
					hasActionButtons:actionButtons.length > 0,
					acitonButtons:actionButtons,
				},
				actionButtonsTpl = $('#actionButtonsTpl').html();

			Mustache.parse(actionButtonsTpl);
			$("#actionButtons").html(Mustache.render(actionButtonsTpl, data));
		},
		renderOrderBaserInfo:function(order) {
			$("#peerDeploymentOrderNo").html(order.peerDeploymentOrderNo);
			var data = _.extend(Rental.render, {
				data:order,
				peerDeploymentOrderStatusValue:function(){
					return Enum.peerDeploymentOrderStatus.getValue(this.peerDeploymentOrderStatus);
				},
				peerDeploymentOrderStatusClass:function() {
					return Enum.peerDeploymentOrderStatus.getClass(this.peerDeploymentOrderStatus);
				},
				rentTypeValue:function() {
					return Enum.rentType.getValue(this.rentType);
				},
				rentTypeUnit:function() {
					return Enum.rentType.getUnit(this.rentType);
				},
				deliveryModeValue:function() {
					return Enum.deliveryMode.getValue(this.deliveryMode)
				}
			});
			var tpl = $("#orderBaseInfoTpl").html();
			Mustache.parse(tpl);
			$('#orderBaseInfo').html(Mustache.render(tpl, data));
		},
		initChooseProductList:function(data) {
			console.log(this.rowProductActionuttons)
			this.state.chooseProductList = this.resolveChooseProductList(data.peerDeploymentOrderProductList);
			this.renderProductList(this.filterAcitonButtons(this.rowProductActionuttons, data));
		},	
		initChooseMaterialList:function(order) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(order.peerDeploymentOrderMaterialList);
			this.renderMaterialList(this.filterAcitonButtons(this.rowMaterialActionuttons, order));
		}
	};

	window.PeerOrderDetail = _.extend(PeerOrderDetail, PeerOrderHandleMixin, PeerOrderMixIn);

})(jQuery);