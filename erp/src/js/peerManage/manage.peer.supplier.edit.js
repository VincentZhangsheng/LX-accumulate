/**
* 编辑同行
*/
;(function($){
	var EditPeerSupplier = {
		state:{
			no:null,
		},
		init:function(callBack) {
			this.initAuthor();
			this.initDom();
			this.initEvent();	
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_supplier_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_peer_supplier_edit];
		},
		initDom:function() {
			var self = this;

			this.$form = $("#editPeerSupplierForm");

			this.$province = $('[name=province]', this.$form);
			this.$city = $('[name=city]', this.$form);
			this.$district = $('[name=district]', this.$form);

			this.Area = new Area();
			this.Area.init({
				$province:self.$province,
				$city:self.$city,
				$district:self.$district,
				callBack:function(result) {
				}
			});

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_peer_supplier_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,self.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(this.$form,function(form){
				self.edit();
			});

			self.loadData();
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到同行编号');
				return;
			}
			Rental.ajax.ajaxData('peer/queryDetail', {peerNo:self.state.no}, '加载同行信息', function(response) {
				self.initFormData(response.resultMap.data);
			});
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;

			Rental.ui.renderFormData(self.$form, data);

			self.Area.setDefaultValue({
				province:data.province,
				city:data.city,
				district:data.district,
			});
		},
		edit:function() {
			try {
				
				var self = this;
				var formData = Rental.form.getFormData(self.$form);
				formData.peerNo = self.state.no;

				var des = '编辑同行';
				Rental.ajax.submit("{0}peer/updatePeer".format(SitePath.service), formData, function(response){
					if(response.success) {
						Rental.notification.success(des, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(des, response.description || '失败');
					}
				}, null, des, 'dialogLoading');

			} catch (e) {
				Rental.notification.error("编辑同行失败", Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "编辑同行",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续编辑',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
							// window.location.reload();
				        }
				    }
				});
			}
			return false;
		}
	};

	window.EditPeerSupplier = EditPeerSupplier;

})(jQuery);