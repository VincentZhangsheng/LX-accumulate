/**
* 同行管理
*/
;(function($) {
	var PeerSupplierManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_supplier];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_supplier_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_peer_supplier_add];
			var edit = this.currentPageAuthor.children[AuthorCode.manage_peer_supplier_edit];
			var view = this.currentPageAuthor.children[AuthorCode.manage_peer_supplier_detail];

			add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));

			view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.searchStorage = Rental.searchStorage.get(Rental.searchStorage.enum.supplierPeer);
			Rental.ui.renderFormByData(this.$searchForm, this.searchStorage || {});

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件
			self.$dataListTable.rtlCheckAll('checkAll','checkItem');
		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, self.searchStorage || {}, Rental.form.getFormData(self.$searchForm) || {}, prams || {});

			Rental.searchStorage.set(Rental.searchStorage.enum.supplierPeer, searchData);

			Rental.ajax.ajaxData('peer/queryPage', searchData, '加载同行列表', function(response) {
				self.render(response.resultMap.data);
			});
		},
		doSearch:function(pageNo) {
			this.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			var self = this;
			self.renderList(data);
			self.Pager.init(data,function(pageNo) {
				self.doSearch(pageNo);
			});
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
				})
			}
			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.PeerSupplierManage = PeerSupplierManage;

})(jQuery);