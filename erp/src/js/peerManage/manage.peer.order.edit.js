/**
* 编辑调拨同行单
*/
;(function($){

	var EditPeerOrder = {
		state:{
			chooseProductList:new Array(),
			chooseMaterialList:new Array(),
			no:null,
		},
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_deployment_order];
			this.currentManageListAuthor = this.currentManageAuthor.children[AuthorCode.manage_peer_order_list];
			this.currentPageAuthor = this.currentManageListAuthor.children[AuthorCode.manage_peer_order_edit];
		},
		initDom:function() {
			this.$form = $("#editPeerOrderForm");

			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");

			this.$materialDataListTpl = $('#materialDataListTpl').html();
			this.$materialDataListTable = $('#materialDataListTable');

			this.state.no = Rental.helper.getUrlPara('no');
		},
		initEvent:function() {
			var self = this;

			Layout.chooseSidebarMenu(AuthorCode.manage_peer_order_list); //激活选中menu菜单
			Breadcrumb.init([self.currentManageAuthor,this.currentManageListAuthor,self.currentPageAuthor]); //面包屑

			Rental.form.initFormValidation(self.$form,function() {
				self.edit();
			});

			self.loadData();

			self.initCommonEvent(); // from order mixin
		},
		loadData:function() {
			var self = this;
			if(!self.state.no) {
				bootbox.alert('没找到同行调拨单编号');
				return;
			}
			Rental.ajax.submit("{0}peerDeploymentOrder/detailPeerDeploymentOrder".format(SitePath.service),{peerDeploymentOrderNo:self.state.no},function(response){
				if(response.success) {
					self.initData(response.resultMap.data);
				} else {
					Rental.notification.error("加载同行调拨单详情",response.description || '失败');
				}
			}, null ,"加载同行调拨单详情", 'listLoading');
		},
		initData:function(data) {
			this.initFormData(data);
			this.initChooseProductList(data);
			this.initChooseMaterialList(data);
			this.state.peerDeploymentOrderConsignInfo = data.peerDeploymentOrderConsignInfo;
			this.renderPeerDeploymentOrderConsignInfo();
		},
		initFormData:function(data) {
			if(!data) return;
			var self = this;
	  		Rental.ui.renderFormData(self.$form, data);
	  		$('[name=rentStartTime]', self.$form).val(new Date(data.rentStartTime).format('yyyy-MM-dd'));
		},
		initChooseProductList:function(data) {
			this.state.chooseProductList = this.resolveChooseProductList(data.peerDeploymentOrderProductList);
			this.renderProductList();
		},	
		initChooseMaterialList:function(order) {
			this.state.chooseMaterialList = this.resolveChooseMaterialList(order.peerDeploymentOrderMaterialList);
			this.renderMaterialList();
		},
		edit:function() {
			var self = this;
			try {
				var formData = Rental.form.getFormData(self.$form);

				var commitData = {
					peerDeploymentOrderNo:self.state.no,
					peerNo:formData['peerNo'],
					// totalDiscountAmount:formData['totalDiscountAmount'],
					taxRate:formData['taxRate'],
					warehouseNo:formData['warehouseNo'],
					deliveryMode:formData['deliveryMode'],
					rentStartTime:new Date(formData['rentStartTime']).getTime(),
					rentType:formData['rentType'],
					rentTimeLength:formData['rentTimeLength'],
					remark:formData['remark'],
				}

				if(!self.state.peerDeploymentOrderConsignInfo) {
					bootbox.alert('请添加归还地址');
					return;
				}

				commitData.peerDeploymentOrderConsignInfo = self.getAddress();

				var peerDeploymentOrderProductList = new Array();
				self.state.chooseProductList.forEach(function(product) {
					product.chooseProductSkuList.forEach(function(sku) {
						peerDeploymentOrderProductList.push({
							productSkuId:sku.skuId,
							productSkuCount:sku.productSkuCount,
							productUnitAmount:sku.productUnitAmount,
							isNew:sku.isNew,
						})
					})
				});

				var peerDeploymentOrderMaterialList = new Array();
				self.state.chooseMaterialList.forEach(function(material) {
					peerDeploymentOrderMaterialList.push({
						// materialId:material.materialId,
						materialNo:material.materialNo,
						materialCount:material.materialCount,
						materialUnitAmount:material.materialUnitAmount,
						isNew:material.isNew,
					})
				});

				if(peerDeploymentOrderProductList.length == 0 && peerDeploymentOrderMaterialList.length == 0) {
					bootbox.alert('请选择商品或配件');
					return;
				}

				if(peerDeploymentOrderProductList.length > 0) {
					commitData.peerDeploymentOrderProductList = peerDeploymentOrderProductList;
				}

				if(peerDeploymentOrderMaterialList.length > 0) {
					commitData.peerDeploymentOrderMaterialList = peerDeploymentOrderMaterialList;
				}

				var description = "编辑同行调拨单";
				Rental.ajax.submit("{0}peerDeploymentOrder/update".format(SitePath.service), commitData, function(response){
					if(response.success) {
						Rental.notification.success(description, response.description || '成功');
						self.callBackFunc(response);
					} else {
						Rental.notification.error(description, response.description || '失败');
					}
				}, null, description, 'dialogLoading');
			} catch(e) {
				Rental.notification.error(description+'失败', Rental.lang.commonJsError +  '<br />' + e );
			}
		},
		callBackFunc:function(response) {
			var self = this;
			if(response.success) {
				bootbox.confirm({
				    message: "成功编辑同行调拨单",
				    buttons: {
				        confirm: {
				            label: '返回列表',
				            className: 'btn-primary'
				        },
				        cancel: {
				            label: '继续创建',
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				        if(result) {
				        	window.history.back();
				        } else {
				        	// window.location.reload();
				        }
				    }
				});
			}
		}
	};

	window.EditPeerOrder = _.extend(EditPeerOrder, PeerOrderMixIn);

})(jQuery);