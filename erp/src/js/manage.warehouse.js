// 供应商管理

;(function($) {

	var WarehouseManage = {
		init:function() {
			this.initAuthor();
			this.initDom();
			this.initEvent();
		},
		initAuthor:function() {
			this.currentManageAuthor = Rental.localstorage.getMapAuthor()[AuthorCode.manage_warehouse];
			this.currentPageAuthor = this.currentManageAuthor.children[AuthorCode.manage_warehouse_list];

			this.initActionButtons();
		},
		initActionButtons:function(){
			this.commonActionButtons = new Array();
			this.rowActionButtons = new Array();

			if(this.currentPageAuthor.hasOwnProperty('children') == false) return;

			var add = this.currentPageAuthor.children[AuthorCode.manage_warehouse_list_add];
			var edit = this.currentPageAuthor.children[AuthorCode.manage_warehouse_list_edit];
			var view = this.currentPageAuthor.children[AuthorCode.manage_warehouse_list_detail];
			var del = this.currentPageAuthor.children[AuthorCode.manage_warehouse_list_delete];

			// add && this.commonActionButtons.push(AuthorUtil.button(add,'addButton','fa fa-plus','添加'));
			// del && this.commonActionButtons.push(AuthorUtil.button(del,'delButton','fa fa-trash-o','删除'));

			// view && this.rowActionButtons.push(AuthorUtil.button(view,'viewButton','','查看'));
			// edit && this.rowActionButtons.push(AuthorUtil.button(edit,'editButton','','编辑'));
			// del && this.rowActionButtons.push(AuthorUtil.button(del,'delButton','','删除'));
		},
		initDom:function() {
			this.$searchForm = $("#searchForm");
			this.$createStartTime = $("#createStartTime");
			this.$createEndTime = $("#createEndTime");
			this.$createTimePicker = $('#createTimePicker');
			this.$createTimePickerInput = $('#createTimePickerInput');
			this.$dataListTpl = $("#dataListTpl").html();
			this.$dataListTable = $("#dataListTable");
			this.$actionCommonButtons = $("#actionCommonButtons");

			this.Pager = new Pager();
		},
		initEvent:function() {

			var self = this;

			Breadcrumb.init([self.currentManageAuthor,self.currentPageAuthor]); //面包屑

			//绑定查询事件
			Rental.form.initSearchFormValidation(self.$searchForm,function(){
				self.searchData();
			});

			self.searchData();  //初始化列表数据

			self.$actionCommonButtons.on("click", '.refreshButton', function(event) {
				event.preventDefault();
				self.searchData();
			});

			self.$actionCommonButtons.on("click", '.addButton', function(event) {
				event.preventDefault();
				bootbox.alert('开发中...');
			});

			self.$actionCommonButtons.on("click", '.delButton', function(event) {
				event.preventDefault();
				bootbox.alert('开发中...');
			});

			self.$dataListTable.on('click', '.viewButton', function(event) {
				event.preventDefault();
				bootbox.alert('开发中...');
			});

			self.$dataListTable.on('click', '.editButton', function(event) {
				event.preventDefault();
				bootbox.alert('开发中...');
			});

			self.$dataListTable.on('click', '.delButton', function(event) {
				event.preventDefault();
				bootbox.alert('开发中...');
			});

			self.renderCommonActionButton(); //渲染操作按钮及事件

			self.$dataListTable.rtlCheckAll('checkAll','checkItem');

		},
		renderCommonActionButton:function() {
			var self = this;
			var actionCommonButtonsTpl = $('#actionCommonButtonsTpl').html();
			Mustache.parse(actionCommonButtonsTpl);
			self.$actionCommonButtons.html(Mustache.render(actionCommonButtonsTpl,{'acitonCommonButtons':self.commonActionButtons}));
		},
		searchData:function(prams) {
			var self = this;

			var searchData = $.extend({
				pageNo:1,
				pageSize: self.Pager.defautPrams.pageSize,
			}, Rental.form.getFormData(WarehouseManage.$searchForm) || {}, prams || {});
			
			Rental.ajax.submit("{0}warehouse/getWarehousePage".format(SitePath.service),searchData,function(response){
				if(response.success) {
					WarehouseManage.render(response.resultMap.data);	
				} else {
					Rental.notification.error("查询采购单列表",response.description || '失败');
				}
			}, null ,"查询商品列表",'listLoading');
		},
		doSearch:function(pageNo) {
			WarehouseManage.searchData({pageNo:pageNo || 1});
		},
		render:function(data) {
			this.renderList(data);
			this.Pager.init(data,this.doSearch)
		},
		renderList:function(data) {
			var self = this;
			
			var listData = data.hasOwnProperty("itemList") ? data.itemList : [],
				hasRowActionButtons = self.rowActionButtons && self.rowActionButtons.length > 0,
				hasCommonActionButtons = self.commonActionButtons && self.commonActionButtons.length > 0;

			var data = {
				hasCommonActionButtons:hasCommonActionButtons,
				hasRowActionButtons:hasRowActionButtons,
				rowActionButtons:self.rowActionButtons,
				dataSource:_.extend(Rental.render, {
					"listData":listData,
					companyTypeStr:function() {
						return Enum.companyType.getValue(this.subCompanyType);
					},
					warehouseTypeStr:function() {
						return Enum.warehouseType.getValue(this.warehouseType);
					}
				})
			}

			Mustache.parse(this.$dataListTpl);
			this.$dataListTable.html(Mustache.render(this.$dataListTpl, data));
		}
	};

	window.WarehouseManage = WarehouseManage;

})(jQuery);