var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var plugins = require('gulp-load-plugins');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var clean = require('gulp-clean');
var less = require('gulp-less');
var concat = require('gulp-concat');
var pump = require('pump');
var minifycss = require('gulp-minify-css');
var rd = require('rd');

var srcPath = './src';
var distPath = './dist'; 

gulp.task('concatRental', function(cb) {
  return gulp.src([
            srcPath + '/js/common/rental.js', 
            srcPath + '/js/common/validate.js',
            srcPath + '/js/common/http.js', 
            srcPath + '/js/common/form.js',
            srcPath + '/js/common/notification.js',
            srcPath + '/js/common/localstorage.js',
            srcPath + '/js/common/searchStorage.js',
            srcPath + '/js/common/author.js',
            srcPath + '/js/common/breadcrumb.js',
            srcPath + '/js/common/pager.js',
            srcPath + '/js/common/pageurl.js',
            srcPath + '/js/common/enum.js',
            srcPath + '/js/common/list.checkbox.js',
            srcPath + '/js/common/area.js',
          ])
          .pipe(concat('rental.common.js'))
          //.pipe(gulp.dest(distPath + '/js/'))
          .pipe(uglify())
          .pipe(rev())
          .pipe(gulp.dest(distPath + '/js/'))
          .pipe(rev.manifest(
            {
              path:"common-manifest.json",
              //base:distPath + '/js',
              //merge: true // merge with the existing manifest if one exists
            }
          ))
          .pipe(gulp.dest(distPath + '/js/'));
});

gulp.task('minJs',function(cb) {
    pump([
          gulp.src([srcPath + '/js/**/*.js','!' + srcPath + '/js/common/*.js']),
          rename({dirname: ''}),
          changed(distPath + '/js/'),
          uglify(),
          rev(),
          gulp.dest(distPath + '/js/'),
          rev.manifest(
            {
              path:"rev-manifest.json",
              //base:distPath + '/js/',
              //merge: true // merge with the existing manifest if one exists
            }
          ),
          gulp.dest(distPath + '/js/'),
        ],
        cb
    );
});


gulp.task('copyLoaderJs',function() {
    var loader = fs.readFileSync(srcPath + '/loader.js', 'utf-8');
   
    if(!fs.existsSync(distPath + '/')) {
        fs.mkdirSync(distPath + '/');
    }
   
    var style_json = JSON.parse(fs.readFileSync(distPath + '/css/rev-manifest.json'));
    var script_json = JSON.parse(fs.readFileSync(distPath + '/js/rev-manifest.json'));
    var common_script_json = JSON.parse(fs.readFileSync(distPath + '/js/common-manifest.json'));
    var s_json = Object.assign(script_json,common_script_json);

    loader = loader.replace('var management_style_json = null;', 'var management_style_json = ' + JSON.stringify(style_json));
    loader = loader.replace('var management_script_json = null;', 'var management_script_json = ' + JSON.stringify(s_json));

    fs.writeFileSync(distPath + '/loader.js', loader);
})


gulp.task('minCss',function(cb) {
    return gulp.src(srcPath + '/css/*.less')
            .pipe(less())
            .pipe(minifycss())
            .pipe(rev())
            .pipe(gulp.dest(distPath + '/css/'))
            .pipe(rev.manifest())
            .pipe(gulp.dest(distPath + '/css/'));
});

gulp.task('copyImg',function(cb) {
    return gulp.src(srcPath + '/img/**')
            .pipe(gulp.dest(distPath + '/img/'));
});

gulp.task('cleanAll',function() {
    return gulp.src(distPath).pipe(clean());
});

gulp.task('build',['cleanAll'],function() {
    runSequence(['concatRental','minJs','minCss','copyImg'],function() {
      runSequence(['copyLoaderJs']);
    });
});

gulp.task('watch',['cleanAll'],function() {
    runSequence(['concatRental','minJs','minCss','copyImg'],function() {
      runSequence(['copyLoaderJs']);
    });
})

gulp.task('dev', function() {
    runSequence(['build']);
    gulp.watch([srcPath + '/**/*.*'], ['watch']);
});






