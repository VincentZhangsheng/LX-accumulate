var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var plugins = require('gulp-load-plugins');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var runSequence = require('run-sequence');
var clean = require('gulp-clean');
var less = require('gulp-less');
var pump = require('pump');
var minifycss = require('gulp-minify-css');
var rd = require('rd');

var srcPath = './src';
var distPath = './dist'; 

gulp.task('minJs',function(cb) {
    pump([
          gulp.src(srcPath + '/rental/js/*.js'),
          uglify(),
          rev(),
          gulp.dest(distPath + '/rental/js/'),
          rev.manifest(),
          gulp.dest(distPath + '/rental/js/'),
        ],
        cb
    );
});


gulp.task('copyLoaderJs',function() {
    var loader = fs.readFileSync(srcPath + '/rental/loader.js', 'utf-8');
   
    if(!fs.existsSync(distPath + '/rental/')) {
        fs.mkdirSync(distPath + '/rental/');
    }
   
    var style_json = JSON.parse(fs.readFileSync(distPath + '/rental/css/rev-manifest.json'));
    var script_json = JSON.parse(fs.readFileSync(distPath + '/rental/js/rev-manifest.json'));

    loader = loader.replace('var common_style_json = null;', 'var common_style_json = ' + JSON.stringify(style_json));
    loader = loader.replace('var common_script_json = null;', 'var common_script_json = ' + JSON.stringify(script_json));

    fs.writeFileSync(distPath + '/rental/loader.js', loader);
})


gulp.task('minCss',function(cb) {
    return gulp.src(srcPath + '/rental/css/*.less')
            .pipe(less())
            .pipe(minifycss())
            .pipe(rev())
            .pipe(gulp.dest(distPath + '/rental/css/'))
            .pipe(rev.manifest())
            .pipe(gulp.dest(distPath + '/rental/css/'));
});

gulp.task('copyImg',function(cb) {
    return gulp.src(srcPath + '/rental/img/**')
            .pipe(gulp.dest(distPath + '/rental/img/'));
});


gulp.task('copyAssets',function(cb) {
    return gulp.src(srcPath + '/assets/**')
            .pipe(gulp.dest(distPath + '/assets/'));
});

gulp.task('copyVendor',function(cb) {
    return gulp.src(srcPath + '/vendor/**')
            .pipe(gulp.dest(distPath + '/vendor/'));
});

gulp.task('cleanAll',function() {
    return gulp.src(distPath).pipe(clean());
});

gulp.task('build',['cleanAll'],function() {
    runSequence(['minJs','minCss','copyImg','copyAssets','copyVendor'],function() {
      runSequence(['copyLoaderJs']);
    });
});

gulp.task('watch',function() {
    runSequence(['minJs','minCss','copyImg','copyAssets','copyVendor'],function() {
      runSequence(['copyLoaderJs']);
    });
})

gulp.task('dev', function() {
    runSequence(['build']);
    gulp.watch([srcPath + '/**/*.*'], ['watch']);
});






