/**
* Author:Web Lee
* Date:2017-07-19
*/
var Loader = Loader || {};

Loader.getJson = function (path,callBack,async) {
    $.ajax({
        type:'post',
        url:path,
        cache: false,
        async: async,
        dataType:"json",
        success:callBack,
    });
}

/**
* 调用：Loader.css(path,fileName1,fileName2...);
*/
Loader.css = function() {
    var arg = arguments,
        path = Array.prototype.splice.call(arg,0,1),
        getUrl = path + "/rev-manifest.json";
    Loader.getJson(getUrl,function(json){
        Loader.createStyleLink(path,arg,json);
    }, false);
};

/**
* 异步加载
* 调用：Loader.cssAsync(path,fileName1,fileName2...);
*/
Loader.cssAsync = function() {
    var arg = arguments,
        path = Array.prototype.splice.call(arg,0,1),
        getUrl = path + "/rev-manifest.json";
    Loader.getJson(getUrl,function(json) {
        Loader.createStyleLink(path,arg,json);
    },true);
}

Loader.createStyleLink = function(path,fileNameArray,json) {
    $.each(fileNameArray,function(i,n) {
        var link = document.createElement('link');
        link.href = path + "/" + json[n];
        link.rel = "stylesheet";
        document.head.appendChild(link);
    });
}


/**
* 调用：Loader.js(path,fileName1,fileName2...)
*/
Loader.js = function() {
    var arg = arguments,
        path = Array.prototype.splice.call(arg,0,1),
        getUrl = path + "/rev-manifest.json";
    Loader.getJson(getUrl,function(json) {
        Loader.createScript(path,arg,json);
    }, false);
};

/**
*  异步加载：Loader.jsAsync(path,fileName1,fileName2...)
*/
Loader.jsAsync = function() {
    var arg = arguments,
        path = Array.prototype.splice.call(arg,0,1),
        getUrl = path + "/rev-manifest.json";
    Loader.getJson(getUrl,function(json) {
        Loader.createScript(path,arg,json);
    }, true);
};

Loader.createScript = function(path,fileNameArray,json) {
    $.each(fileNameArray,function(i,n) {
        var script = document.createElement('script');
        script.src = path + "/" + json[n];
        document.body.appendChild(script)
    });
}