(function() {
    var common_style_json = null;
    var common_script_json = null;
    
    window.common_load_css = function() {
        var arg = arguments,
            path = Array.prototype.splice.call(arg,0,1);
        for (var i = 0; i < arg.length ; i++) {
            var name = arg[i];
            var href = path + "/" + common_style_json[name];
            document.write('<link rel="stylesheet" type="text/css" href='+href+'>')
        }
    }

    window.common_load_script = function() {
        var arg = arguments,
            path = Array.prototype.splice.call(arg,0,1);
        for (var i = 0; i < arg.length ; i++) {
            var name = arg[i];
            var src = path + "/" + common_script_json[name];
            document.write('<script src='+ src +' type="text/javascript"></script>');
        }
    }

})();