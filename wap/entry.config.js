var path = require("path");
module.exports = {
	"payOrder": "./src/pages/order/pay.js", //支付确认订单
	"orderList": "./src/pages/order/list.js", //订单列表
	"orderDetail": "./src/pages/order/detail.js", //订单详细
	"explore": "./src/pages/explore/home.js", //探索
	"help": "./src/pages/help/app.js", //帮助中心
	"account": "./src/pages/account/app.js", //个人中心
	"product": "./src/pages/product/app.js", //商品
	"result": "./src/pages/result/app.js", //商品

	"erpCustomerBind": "./src/pages/erpCustomer/bind/app.js", //ERP客戶綁定
	"erpCustomerAccount": "./src/pages/erpCustomer/account/app.js", //ERP客戶中心
	"erpCustomerOrderList": "./src/pages/erpCustomer/order/list.js", //ERP客戶订单列表
	"erpCustomerOrderDetail": "./src/pages/erpCustomer/order/detail.js", //ERP客戶订单详细
	"erpCustomerStatementList": "./src/pages/erpCustomer/statement/list.js", //ERP客戶结算单列表
	"erpCustomerStatementDetail": "./src/pages/erpCustomer/statement/detail.js", //ERP客戶结算单详细
	"erpCustomerResult": "./src/pages/erpCustomer/result/app.js", //ERP客戶结算单详细
	"erpCustomerRecharge": "./src/pages/erpCustomer/recharge/recharge.js", //ERP客戶-充值
	"erpCustomerRechargeResult": "./src/pages/erpCustomer/recharge/result.js", //ERP客戶-充值
	"erpCustomerRechargeOrder": "./src/pages/erpCustomer/recharge/rechargeOrder.js", //ERP客戶-充值订单

	"erpCustomerUnbindRecharge": "./src/pages/erpCustomer/rechargeUnBind/recharge.js", //ERP客戶-充值
	"erpCustomerUnbindRechargeResult": "./src/pages/erpCustomer/rechargeUnBind/result.js", //ERP客戶-充值
	"erpCustomerUnbindRechargeOrder": "./src/pages/erpCustomer/rechargeUnBind/rechargeOrder.js", //ERP客戶-充值订单


	"aliBind": "./src/pages/ali/bind/app.js", //支付宝生活号-ERP客戶綁定
	"aliAccount": "./src/pages/ali/account/app.js", //支付宝生活号-ERP客戶中心
	"aliOrderList": "./src/pages/ali/order/list.js", //支付宝生活号-ERP客戶订单列表
	"aliOrderDetail": "./src/pages/ali/order/detail.js", //支付宝生活号-ERP客戶订单详细
	"aliStatementList": "./src/pages/ali/statement/list.js", //支付宝生活号-ERP客戶结算单列表
	"aliStatementDetail": "./src/pages/ali/statement/detail.js", //支付宝生活号-ERP客戶结算单详细
	"aliResult": "./src/pages/ali/result/app.js", //支付宝生活号-ERP客戶结算单详细
	"aliRecharge": "./src/pages/ali/recharge/recharge.js", //支付宝生活号-ERP客戶-充值
	"aliRechargeResult": "./src/pages/ali/recharge/result.js", //支付宝生活号-ERP客戶-充值
	"aliRechargeOrder": "./src/pages/ali/recharge/rechargeOrder.js", //支付宝生活号-ERP客戶-充值订单

	"aliUnbindRecharge": "./src/pages/ali/rechargeUnBind/recharge.js", //支付宝生活号-ERP客戶-充值
	"aliUnbindRechargeResult": "./src/pages/ali/rechargeUnBind/result.js", //支付宝生活号-ERP客戶-充值
	"aliUnbindRechargeOrder": "./src/pages/ali/rechargeUnBind/rechargeOrder.js", //支付宝生活号-ERP客戶-充值订单

	"customerSurveyAdd": "./src/pages/other/customerSurvey/add.js", //问卷调查
	"recycleCustomerAdd": "./src/pages/other/recycleCustomer/add.js", //回收推荐客户信息录入

	"eSports": "./src/pages/eSports/app.js", //电竞活动
	"eSportsActivity": "./src/pages/eSports/activity.js", //电竞活动
}