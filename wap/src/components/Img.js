import React, { Component } from 'react';

const Img = ({ img, className, alt }) => {
	if(!img) {
		img = require('../images/no-img.jpg')
	} 
	return (
		<img src={img} alt={alt} />
	)
}

export default Img