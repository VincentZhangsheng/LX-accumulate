import React, { Component } from 'react';
import { Icon, Modal } from 'antd-mobile';

export default class Header extends Component {
    constructor(props) {
      super(props);
      this.state = {
        QRCode:false,
      };
    }
    goBack() {
      if(this.props.hasOwnProperty('backFunc')){
        this.props.backFunc();
      } else if (this.props.hasOwnProperty('backLink')) {
        window.location.href = this.props.backLink;
      } else {
        window.history.back();
      }
    }
    scanQRCode() {
      window.wx.scanQRCode({
        needResult: 1,
        desc: 'scanQRCode desc',
        success: function (res) {
          var result = res.resultStr; 
          if(result.hasOwnProperty('rentalProductInfo')) {
            Modal.operation([
              { text: '查看商品', onPress: () => console.log('查看商品详细信息') },
              { text: '一键续租', onPress: () => console.log('一键续租') },
              { text: '申请维修', onPress: () => console.log('申请维修') },
            ])
          }
        }
      });
    }
    render() {
        let { title, scanQRCode } = this.props;
        return (
          <div className="alt-header">
            <a className="icon" onClick={this.goBack.bind(this)}></a>{title}
            {
              scanQRCode &&
              <Icon type={require('../svg/scan.svg')} size="xs" style={{float:'right'}}  onClick={this.scanQRCode.bind(this)}  />  
            }
          </div>
        );
    }
}