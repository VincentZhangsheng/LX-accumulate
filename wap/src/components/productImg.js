import React, { Component } from 'react';

const ProductImg = ({ img, className, alt }) => {
	var imgUrl = '';
	if(img && img.hasOwnProperty('imgDomain') && img.hasOwnProperty('imgUrl')) {
		imgUrl = img.imgDomain + img.imgUrl;
	} else {
		imgUrl = require('../images/no-img.jpg')
	}
	return (
		<img src={imgUrl} alt={alt} />
	)
}

export default ProductImg