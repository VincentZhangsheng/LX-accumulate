import $ from 'zepto'
import Request from './request.js'
import { Toast } from 'antd-mobile'

export default {
    getWeChatConfig(url) {
        url = url ||  window.location.href.split('#')[0];
        $.ajax({
              type:"POST",
              url:Request.SitePath.service + 'wxjs/getJSConfig',
              contentType:"application/json",
              data:JSON.stringify({url:url}),
              dataType: 'json',
              //cache:false,
              async:false,
              success:(response) => {
                if(response.success) {
                  this.initWeChatConfig(response.resultMap.data);
                } else {
                  Toast.fail('加载微信配置失败', 3);
                }
              },
              error:() => {
                Toast.fail('加载微信配置失败', 3);
              }
        });
    },
    initWeChatConfig(config) {
        window.wx.config({
          debug: false,
          appId: config.appId,//'wxf8b4f85f3a794e77',
          timestamp: config.timestamp,//1508048437,
          nonceStr: config.nonceStr,//'8TSQBUCiAqerW8ak',
          signature: config.signature,//'eeabdef4d66c02133b5cd16f2f76b1713257089d',
          jsApiList: [
            'checkJsApi',
            //'onMenuShareTimeline',
            //'onMenuShareAppMessage',
            //'onMenuShareQQ',
            //'onMenuShareWeibo',
            //'onMenuShareQZone',
            //'hideMenuItems',
            //'showMenuItems',
            //'hideAllNonBaseMenuItem',
            //'showAllNonBaseMenuItem',
            //'translateVoice',
            //'startRecord',
            //'stopRecord',
            //'onVoiceRecordEnd',
            //'playVoice',
            //'onVoicePlayEnd',
            //'pauseVoice',
            //'stopVoice',
            //'uploadVoice',
            //'downloadVoice',
            'chooseImage',
            //'previewImage',
            'uploadImage',
            'downloadImage',
            //'getNetworkType',
            //'openLocation',
            //'getLocation',
            //'hideOptionMenu',
            //'showOptionMenu',
            //'closeWindow',
            'scanQRCode',
            'chooseWXPay',
            //'openProductSpecificView',
            //'addCard',
            //'chooseCard',
            //'openCard'
          ]
        });
    },
    createPayOrder(orderNo,success) {
        if(!orderNo) {
            Toast.fail('找不到订单号', 3);  
            return;
        }
        Request.ajax('order/pay',{orderNo:orderNo},(response) => {
            if(response.success) {
              var data = response.resultMap.data.payParams;
             this.wxPay(data,success);
            } else {
              Toast.fail('生成支付订单失败', 3);  
            }
          },(error) => {
              Toast.fail('生成支付订单失败', 3);
          });
    },
    createPayStatementOrder(statementOrderNo, ajaxComplete, success) {
        if(!statementOrderNo) {
            Toast.fail('找不到结算单号', 3);  
            return;
        }
        Request.ajax('erp/weixinPayStatementOrder',{statementOrderNo:statementOrderNo},(response) => {
          ajaxComplete && ajaxComplete();
          if(response.success) {
            var data = JSON.parse(response.resultMap.data);
            this.initWeChatConfig({
              appId: data.appId, 
              timestamp: data.timeStamp,
              nonceStr: data.nonceStr,
              signature: data.paySign,
            });
            this.wxPay(data, success);
          } else {
            Toast.fail('生成支付结算单失败', 3);  
          }
        },(error) => {
            ajaxComplete && ajaxComplete();
            Toast.fail('生成支付结算失败', 3);
        });
   },
   createRecharegeOrder(amount, ajaxComplete, success) {
        if(!amount) {
            Toast.fail('请输入需要充值的金额', 3);  
            return;
        }
        Request.ajax('erp/wechatCharge',{amount:amount},(response) => {
          ajaxComplete && ajaxComplete();
          if(response.success) {
            var data = JSON.parse(response.resultMap.data);
            this.initWeChatConfig({
              appId: data.appId, 
              timestamp: data.timeStamp,
              nonceStr: data.nonceStr,
              signature: data.paySign,
            });
            this.wxPay(data, success);
          } else {
            Toast.fail('生成充值单失败', 3);  
          }
        },(error) => {
            Toast.fail('生成充值单失败', 3);
            ajaxComplete && ajaxComplete();
        });
   },
   createUnBindRecharegeOrder(prams, ajaxComplete, success) {
        if(!prams.amount) {
            Toast.fail('请输入需要充值的金额', 3);  
            return;
        }
        Request.ajax('weixinUserPay/weixinActiveCharge', prams,(response) => {
          ajaxComplete && ajaxComplete();
          if(response.success) {
            var data = JSON.parse(response.resultMap.data);
            this.initWeChatConfig({
              appId: data.appId, 
              timestamp: data.timeStamp,
              nonceStr: data.nonceStr,
              signature: data.paySign,
            });
            this.wxPay(data, success);
          } else {
            Toast.fail(response.description || '生成充值单失败', 3);  
          }
        },(error) => {
            Toast.fail('生成充值单失败', 3);
            ajaxComplete && ajaxComplete();
        });
   },
   createESportsPayOrder(orderNumber, ajaxComplete, success) {
        if(!orderNumber) {
            Toast.fail('找不到订单编号', 3);  
            return;
        }
        Request.ajax('weixinUserPay/weixinCollegeActivePay', {orderNo:orderNumber},(response) => {
          ajaxComplete && ajaxComplete();
          if(response.success) {
            var data = JSON.parse(response.resultMap.data);
            this.initWeChatConfig({
              appId: data.appId, 
              timestamp: data.timeStamp,
              nonceStr: data.nonceStr,
              signature: data.paySign,
            });
            this.wxPay(data, success);
          } else {
            Toast.fail(response.description || '生成充值单失败', 3);  
          }
        },(error) => {
            Toast.fail('生成充值单失败', 3);
            ajaxComplete && ajaxComplete();
        });
   },
	 wxPay(config,success) {
      window.wx.chooseWXPay({
        "timestamp": config.timeStamp, //1414723227,
        "nonceStr": config.nonceStr, //'noncestr',
        "package": config.package,
        "signType": config.signType, //'SHA1', // 注意：新版支付接口使用 MD5 加密
        "paySign": config.paySign, //'bd5b1933cda6e9548862944836a9b52e8c9a2b69'
        "success":success || ((res) => {
            // 支付成功后的回调函数
            alert(res)
        })
      });
    },
    previewImage(current,urls) {
      window.wx.previewImage({
        current: current,
        urls: urls
      });
    }
}