export default {
	
	getSKU(currentChoosed, productSkuList) {
		var sku = null;
		(productSkuList || []).forEach((item,i) => {
			var isCurrentSKU = item.hasOwnProperty("productSkuPropertyList") && item.productSkuPropertyList.every((item,i) => {
				return currentChoosed.some((choosed,i) => {
					return item.propertyValueId == choosed.categoryPropertyValueId;
				});
			});

			if(isCurrentSKU) {
				sku = item;
				return;
			}

		});

		return sku;
	},
	getProductInfo(productSkuList) {
		if(!productSkuList) return;

		var totalStock = 0, minPrice = 0, maxPrice = 0;
		if(productSkuList.length > 0) {
			totalStock = productSkuList.reduce((stock,item,i) => {
				return stock + item.stock;
			},0);

			var sortBySalePrice = productSkuList.sort((i,j) => {
				return parseFloat(i.salePrice) - parseFloat(j.salePrice);
			});

			//price = ("￥{0}-￥{1}").format(sortBySalePrice[0].salePrice.toFixed(2),sortBySalePrice[productSkuList.length -1].salePrice.toFixed(2))
			minPrice = sortBySalePrice[0].salePrice;
			maxPrice = sortBySalePrice[productSkuList.length -1].salePrice;
		} else {
			totalStock = productSkuList[0].stock;
			minPrice = productSkuList[0].salePrice;
			maxPrice = productSkuList[0].salePrice;
		}
		
		return {
			totalStock:totalStock,
			minPrice:minPrice,
			maxPrice:maxPrice
		}
	},
}