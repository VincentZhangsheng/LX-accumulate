import $ from 'zepto';
import Common from './common.js'

export default {
      SitePath:Common,
	ajax(url,data,success,error) {
		$.ajax({
            type:"POST",
            url:this.SitePath.service + url,
            contentType:"application/json",
            data:JSON.stringify(data),
            dataType: 'json',
            //cache:false,
            success:success || function() {
            },
            error:error || function() {
            }
        });
	},
      ajaxNoPram(url,success,error) {
            $.ajax({
            type:"POST",
            url:this.SitePath.service + url,
            contentType:"application/json",
            //data:JSON.stringify(data),
            dataType: 'json',
            //cache:false,
            success:success || function() {
            },
            error:error || function() {
            }
        });
      }
}