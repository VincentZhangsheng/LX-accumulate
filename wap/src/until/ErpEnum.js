export default {
	//付款方式
	payMode:{
		num:{
			useAfaterPay:1,
			useBeforePay:2,
		},
		numForOrder:{
			useAfaterPay:1,
			useBeforePay:2,
			thirtyPercent:3,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.useAfaterPay:
					return "先付后用";
				case this.num.useBeforePay:
					return "先用后付";
				case this.numForOrder.thirtyPercent:
					return "首付30%";
			}
			return "";
		}
	},
	//订单状态
	orderStatus:{
		num:{
			unSubmit:0,
			inReview:4,
			waitForDelivery:8,
			inHand:12,
			delivered:16,
			confirmReceipt:20,
			completeRestitution:24,
			cancel:28,
			end:32,
		},
		array:function() {
			var array = new Array();
			for (var key in this.num) {
				array.push({
					num:this.num[key],
					value:this.getValue(this.num[key])
				}); 
			}
			return array;
		},
		getValue:function(num) {
			switch(num) {
				case this.num.unSubmit:
					return "待提交";
				case this.num.inReview:
					return "审核中";
				case this.num.waitForDelivery:
					return "待发货";
				case this.num.inHand:
					return "处理中";
				case this.num.delivered:
					return "已发货";
				case this.num.confirmReceipt:
					return "已收货";
				case this.num.completeRestitution:
					return "已归还";
				case this.num.cancel:
					return "已取消";
				case this.num.end:
					return "已结束";
			}
			return "";
		},
	},
	/**
	* 送货方式
	*/
	deliveryMode:{
		num:{
			express:1,
			pickup:2,
			lxExpress:3,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.express:
					return "快递";
				case this.num.pickup:
					return "自提";
				case this.num.lxExpress:
					return "凌雄配送";
				default:
					return '';
			}
		},
		withOutLX:function(selectval) {
			return [
				{
					num:this.num.express,
					value:this.getValue(this.num.express),
					selected:this.num.express == selectval, 
				},
				{
					num:this.num.pickup,
					value:this.getValue(this.num.pickup),
					selected:this.num.pickup == selectval, 
				}
			]
		}
	},
	//租赁方式
	rentType:{
		num:{
			// byNum:1,
			byDay:1,
			byMonth:2,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.byDay:
					return "按天租";
				case this.num.byMonth:
					return "按月租";
			}
			return "";
		},
		getUnit:function(num) {
			switch(num) {
				case this.num.byDay:
					return "天";
				case this.num.byMonth:
					return "个月";
			}
			return "";
		}
	},
	//付款方式
	payMode:{
		num:{
			useAfaterPay:1,
			useBeforePay:2,
		},
		numForOrder:{
			useAfaterPay:1,
			useBeforePay:2,
			thirtyPercent:3,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.useAfaterPay:
					return "先付后用";
				case this.num.useBeforePay:
					return "先用后付";
				case this.numForOrder.thirtyPercent:
					return "首付30%";
			}
			return "";
		}
	},
	/**
	* 结算单状态
	*/
	statementOrderStatus:{
		num:{
			init:0,
			part:4,
			completed:8,
			noSttlement:16,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.init:
					return "未支付";
				case this.num.part:
					return "部分支付";
				case this.num.completed:
					return "完成支付";
				case this.num.noSttlement:
					return "无需结算";
			}
			return "";
		},
		getClass:function(num) {
			switch(num) {
				case this.num.init:
					return "text-primary";
				case this.num.part:
					return "text-info";
				case this.num.completed:
					return "text-muted";
				case this.num.noSttlement:
					return "text-muted";
			}
			return "";
		}
	},
	/**
	* 充值状态
	*/
	chargeStatus:{
		num:{
			init:0,
			rechargeing:4,
			rechargeComplated:8,
			rechargeFail:16,
			rechargeLose:16,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.init:
					return "待支付";
				case this.num.rechargeing:
					return "充值中";
				case this.num.rechargeComplated:
					return "充值成功";
				case this.num.rechargeFail:
					return "充值失败";
				case this.num.rechargeLose:
					return "充值失效";
			}
			return "";
		},
		getClass:function(num) {
			switch(num) {
				case this.num.init:
					return "text-primary";
				case this.num.rechargeing:
					return "text-info";
				case this.num.rechargeComplated:
					return "text-success";
				case this.num.rechargeFail:
					return "text-muted";
				case this.num.rechargeLose:
					return "text-muted";
			}
			return "";
		}
	},
}