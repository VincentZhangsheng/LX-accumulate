export default {
	key:'current_user',
	setUser(userinfo) {
		if(!userinfo) return;
		sessionStorage.setItem(this.key,JSON.stringify(userinfo));
	},
	getUser() {
		var JsonStr = sessionStorage.getItem(this.key);
		if(JsonStr) {
			return JSON.parse(JsonStr);
		}
		return null;
	}
}