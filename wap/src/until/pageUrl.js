import Common from './common.js'

export default {
	forward(url) {
		window.location.href = url;
	},
	back() {
		window.history.back();
	},
	main:Common.base + 'index',
	payOrder:Common.base + 'payOrder',
	orderList:Common.base + 'orerList',
	orderDetail:Common.base + 'orderDetail',
	explore:Common.base + 'explore', //探索
	help:Common.base + 'help', //帮助 
	account:Common.base + 'account', //个人中心
	product:Common.base + 'product', //商品
	result:Common.base + 'result', //商品

	//微信公众号
	erpCustomerBind:Common.base + 'erp-customer/bind', //ERP客户中心
	erpCustomerAccountHome:Common.base + 'erp-customer/account', //ERP客户中心
	erpCustomerOrderList:Common.base + 'erp-customer/order-list', //ERP客户订单列表
	erpCustomerOrderDetail:Common.base + 'erp-customer/order-detail', //ERP客户订单详细
	erpCustomerStatementList:Common.base + 'erp-statement-list', //ERP客户结算单列表
	erpCustomerStatementDetail:Common.base + 'erp-statement-detail', //ERP客户结算单详细
	erpCustomerStatementReult:Common.base + 'erp-result', //ERP客户支付结果页面

	erpCustomerRecharge:Common.base + 'erp-recharge', //ERP客户-充值
	erpCustomerRechargeResult:Common.base + 'erp-recharge-result', //ERP客户-充值结果
	erpCustomerRechargeOrder:Common.base + 'erp-recharge-order', //ERP客户-充值结果

	erpCustomerUnbindRecharge:Common.base + 'unbind-recharge', //ERP客户-充值
	erpCustomerUnbindRechargeResult:Common.base + 'unbind-recharge-result', //ERP客户-充值结果
	erpCustomerUnbindRechargeOrder:Common.base + 'unbind-recharge-order', //ERP客户-充值结果


	//支付宝生活号
	aliErpCustomerBind:Common.base + 'ali-erp-customer/bind', //ERP客户中心
	aliErpCustomerAccountHome:Common.base + 'ali-erp-customer/account', //ERP客户中心
	aliErpCustomerOrderList:Common.base + 'ali-erp-customer/order-list', //ERP客户订单列表
	aliErpCustomerOrderDetail:Common.base + 'ali-erp-customer/order-detail', //ERP客户订单详细
	aliErpCustomerStatementList:Common.base + 'ali-erp-statement-list', //ERP客户结算单列表
	aliErpCustomerStatementDetail:Common.base + 'ali-erp-statement-detail', //ERP客户结算单详细
	aliErpCustomerStatementReult:Common.base + 'ali-erp-result', //ERP客户支付结果页面

	aliErpCustomerRecharge:Common.base + 'ali-erp-recharge', //ERP客户-充值
	aliErpCustomerRechargeResult:Common.base + 'ali-erp-recharge-result', //ERP客户-充值结果
	aliErpCustomerRechargeOrder:Common.base + 'ali-erp-recharge-order', //ERP客户-充值结果

	aliErpCustomerUnbindRecharge:Common.base + 'ali-unbind-recharge', //ERP客户-充值
	aliErpCustomerUnbindRechargeResult:Common.base + 'ali-unbind-recharge-result', //ERP客户-充值结果
	aliErpCustomerUnbindRechargeOrder:Common.base + 'ali-unbind-recharge-order', //ERP客户-充值结果


	loginForActive:Common.base + 'user/loginForActive', //电竞活动公众号授权入口
	// loginForActive:'http://seashare.4kb.cn/user/loginForActive', //电竞活动公众号授权入口
}