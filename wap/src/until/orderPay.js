import { Toast } from 'antd-mobile';
import Request from './request.js'

export default {
	createPayOrder(orderNo) {
      Request.ajax('order/pay',{orderNo:orderNo},(response) => {
        if(response.success) {
          var data = response.resultMap.data.payParams;
         this.wxPay(data);
        } else {
          Toast.fail('生成支付订单失败', 3);  
        }
      },(error) => {
          Toast.fail('生成支付订单失败', 3);
      })
    },

}