import $ from 'zepto'
import Request from './request.js'
import { Toast } from 'antd-mobile'

export default {
   createUnBindRecharegeOrder(prams, ajaxComplete, success) {
        if(!prams.amount) {
            Toast.fail('请输入需要充值的金额', 3);  
            return;
        }
        Request.ajax('zfbUserPay/zfbActiveCharge', prams,(response) => {
          ajaxComplete && ajaxComplete();
          if(response.success) {
            var data = JSON.parse(response.resultMap.data);
            this.aliPay(data.tradeNO, success);
          } else {
            Toast.fail('生成充值单失败', 3);  
          }
        },(error) => {
            Toast.fail('生成充值单失败', 3);
            ajaxComplete && ajaxComplete();
        });
   },
  createRecharegeOrder(amount, ajaxComplete, success) {
        if(!amount) {
            Toast.fail('请输入需要充值的金额', 3);  
            return;
        }
        Request.ajax('erp/alipayCharge', {amount:amount},(response) => {
          ajaxComplete && ajaxComplete();
          if(response.success) {
            var data = JSON.parse(response.resultMap.data);
            this.aliPay(data.tradeNO, success);
          } else {
            Toast.fail('生成充值单失败', 3);  
          }
        },(error) => {
            Toast.fail('生成充值单失败', 3);
            ajaxComplete && ajaxComplete();
        });
   },
	 aliPay(tradeNO,success, error) {
      window.AlipayJSBridge.call("tradePay", {
          tradeNO: tradeNO
      }, function (result) {
          success && success(result);
      });
   }
}