export default {
	key:'new_area_dictionary',
	keyMap:'new_area_dictionary_map',
	set(area) {
		if(!area) return;
		window.localStorage.setItem(this.key,JSON.stringify(area));
		this.setMap(area);
	},
	get() {
		var JsonStr = window.localStorage.getItem(this.key);
		if(JsonStr) {
			return JSON.parse(JsonStr);
		}
		return null;
	},
	setMap(area) {
		function toMap(areaMap, data) {
			(data || []).forEach(item => {
				if(!areaMap[item.value]) {
					areaMap[item.value] = item;
				} 
				if(item.hasOwnProperty('children')) toMap(areaMap, item.children);
			})
		}
		var _map = {};
		toMap(_map, area);
		window.localStorage.setItem(this.keyMap,JSON.stringify(_map));
	},
	getMap() {
		var JsonStr = window.localStorage.getItem(this.keyMap);
		if(JsonStr) {
			return JSON.parse(JsonStr);
		}
		return null;
	}
}