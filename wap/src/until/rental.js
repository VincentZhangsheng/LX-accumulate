export default {
	queryURL(name){
	  let reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`, 'i')
	  let r = window.location.search.substr(1).match(reg)
	  if (r != null) return decodeURI(r[2])
	  return null
	},
/*	getUrlPara(key) {
		var uri = window.location.search;
    	var re = new RegExp("" +key+ "=([^&?]*)", "ig");
    	return ((uri.match(re))?(uri.match(re)[0].substr(key.length+1)):null);
	},*/
	getUrlPara(serach,key) {
		var uri =serach;
    	var re = new RegExp("" +key+ "=([^&?]*)", "ig");
    	return ((uri.match(re))?(uri.match(re)[0].substr(key.length+1)):null);
	},
	escapeHTML(str) {
		return /^0{0,1}(13[0-9]|15[0-9]|17[0-9]|18[0-9]|14[57])[0-9]{8}$/.test(str);
	},
	unescapeHTML(str) {
		return str.stripTags().replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&');
	},
	format:function(date,fmt){
        var o = {
            "M+" : date.getMonth()+1,                 //月份
            "d+" : date.getDate(),                    //日
            "h+" : date.getHours(),                   //小时
            "m+" : date.getMinutes(),                 //分
            "s+" : date.getSeconds(),                 //秒
            "q+" : Math.floor((date.getMonth()+3)/3), //季度
            "S"  : date.getMilliseconds()             //毫秒
        };
        if(/(y+)/.test(fmt))
            fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
        for(var k in o)
            if(new RegExp("("+ k +")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        return fmt;
    }
}