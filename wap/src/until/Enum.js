export default {
	navTab:{
		explore:"explore",
		product:"product",
		help:"help",
		account:"account"
	},
	//租赁方式
	rentType:{
		num:{
			byMonth:1,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.byMonth:
					return "按月租";
			}
			return "";
		}
	},
	//付款方式
	payMode:{
		num:{
			payall:1,
			bymonth:2,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.payall:
					return "一次付清";
				case this.num.bymonth:
					return "按月付款";
			}
			return "";
		}
	},
	//字段类型
	dictionaryType:{
		num:{
			area:1,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.area:
					return "身份城市地区字典";
			}
			return "";
		}
	},
	//订单状态
	orderStatus:{
		num:{
			nopay:0,
			paying:1,
			paid:2,
			delivered:3,
			renting:4,
			overdue:5,
			surrender:6,
			cancel:7,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.nopay:
					return "未支付";
				case this.num.paying:
					return "支付中";
				case this.num.paid:
					return "已付款";
				case this.num.delivered:
					return "已发货";
				case this.num.renting:
					return "租赁中";
				case this.num.overdue:
					return "逾期";
				case this.num.surrender:
					return "退租";
				case this.num.cancel:
					return "取消";
			}
			return "";
		}
	},
	//订单状态
	payStatus:{
		num:{
			nopay:0,
			paying:1,
			paid:2,
			payFail:3,
			payLostEfficacy:4,
			refunding:5,
			refunded:6,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.nopay:
					return "未支付";
				case this.num.paying:
					return "支付中";
				case this.num.paid:
					return "已付款";
				case this.num.payFail:
					return "付款失败";
				case this.num.payLostEfficacy:
					return "付款失效";
				case this.num.refunding:
					return "退款中";
				case this.num.refunded:
					return "已退款";
			}
			return "";
		}
	},
	//身份认证状态
	verifyStatus:{
		num:{
			nocommit:0,
			commited:1,
			verified:2,
			reject:3,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.nocommit:
					return "待认证";
				case this.num.commited:
					return "认证中";
				case this.num.verified:
					return "已认证";
				case this.num.reject:
					return "认证失败";
			}
			return "";
		}
	}
}