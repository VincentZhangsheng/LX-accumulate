import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container } from '../../pages/layout';
import { Header } from '../../components';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../until'
import { List, WingBlank, WhiteSpace, Button, Icon, Toast } from 'antd-mobile';

const Item = List.Item;

export default class PayOrder extends Component {
    constructor(props) {
      super(props);
      this.state = {
        id:Rental.queryURL('id'),
        data:null,
        animating:false,
      };
    }
    componentDidMount() {
      this.loadData();
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('order/queryOrderByNo',{orderNo:this.state.id},(response) => {
        if(response.success) {
          var data = response.resultMap.data;
          this.setState({
            data: data,
            animating:false,
          });
        } else {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);
      })
    }
    createPayOrder() {
      let { id } = this.state;
      WXJSSDK.createPayOrder(id,(res) => {
        PageUrl.forward(PageUrl.result + '#/success');
      });
    }
    render() {
        let { data, animating } = this.state;
        return (
            <Container animating={animating} initWeChatConfig={true}>
                {     
                  data &&          
                  <div>
                    <Header history={this.props.history} title="订单确认" backLink={PageUrl.orderList} />
                     {
                        data.orderConsignInfo && 
                        <List className="my-list">
                          <Item
                            thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                            //arrow="horizontal"
                            multipleLine
                            //onClick={this.linkToEdit.bind(this,'1111')}
                            platform="android"
                            wrap={true}
                          >
                            {data.orderConsignInfo.consigneeName} {data.orderConsignInfo.consigneePhone}                
                            <div className="alt-list-brief">广东省深圳市宝安区{data.orderConsignInfo.address}</div>
                          </Item>
                        </List>
                      }

                     <List renderHeader={() => '商品'} className="my-list">
                       {
                          data.orderProductList.map((item,i) => {
                            let productModeParams = JSON.parse(item.productModeParams);
                            let mainImg = productModeParams.productImgList[0]
                            return (
                              <Item
                              key={i}
                               multipleLine
                               wrap={true}
                             >
                               <div className="atl-order-item-img">
                                 <img src={mainImg.imgDomain + mainImg.imgUrl} alt={item.productName} />
                               </div>
                               <div className="atl-order-item-info">
                                 <div className="atl-order-item-des">
                                   {productModeParams.productName}
                                 </div>
                                 <div className="atl-order-item-attribute">
                                    {
                                      item.hasOwnProperty('productSkuPropertyList') && item.productSkuPropertyList.map((property,j) => {
                                        return (
                                          <span key={j+'-'+property.propertyValueId} className="atl-order-item-attribute-i">{property.propertyValueName}</span>
                                        )
                                      })
                                    }
                                 </div>
                                 <div className="atl-order-item-info-footer">
                                     <span className="atl-order-item-info-footer-price">月租：￥{item.hasOwnProperty('productUnitAmount') && item.productUnitAmount.toFixed(2)}</span>
                                 </div>
                               </div>
                             </Item>
                            )
                          })
                       }
                     </List>
                     <WhiteSpace size="lg" />
                     <List className="my-list">
                       <Item extra={data.rentTimeLength + '个月'}>租期</Item>
                       <Item extra={Enum.payMode.getValue(data.payMode)}>支付类型</Item>
                       <Item extra={data.hasOwnProperty('orderAmountTotal') && '￥' + data.orderAmountTotal.toFixed(2)}>订单金额</Item>
                       <Item extra={ data.hasOwnProperty('orderPayPlanList') && '￥' + data.orderPayPlanList[0].expectAmount.toFixed(2)}>本期支付</Item>
                     </List>
                     <WhiteSpace size="lg" />
                     <WingBlank>
                       <Button type="primary" onClick={this.createPayOrder.bind(this)} >立即支付</Button>
                     </WingBlank>
                     <WhiteSpace size="lg" />
                 </div>
               }
                

            </Container>
        );
    }
}


ReactDOM.render(<PayOrder />, document.getElementById('wrapper'));