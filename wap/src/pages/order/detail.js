import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request, Rental, WXJSSDK, PageUrl } from '../../until'
import { List, Button, Icon, Toast, Modal} from 'antd-mobile';

const Item = List.Item;
const alert = Modal.alert;

export default class OrderDetail extends Component {
    constructor(props) {
      super(props);
      this.state = {
        id:Rental.queryURL('id'),
        data:null,
        animating:false,
      };
    }
    componentDidMount() {
      this.loadData();
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('order/queryOrderByNo',{orderNo:this.state.id},(response) => {
        if(response.success) {
          var data = response.resultMap.data;
          this.setState({
            data: data,
            animating:false,
          });
        } else {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);
      })
    }
    cancelOrder(orderNo,event) {
       event.preventDefault();
       event.stopPropagation();

       var _this = this;
       function cancel() {
          Request.ajax('order/cancelOrder',{orderNo:orderNo},(response) => {
            if(response.success) {
              Toast.success('成功取消订单', 3);  
              _this.loadData();
            } else {
              Toast.fail('生成支付订单失败', 3);  
            }
          },(error) => {
              Toast.fail('生成支付订单失败', 3);
          })
       }

      alert('确认', '是否取消订单', [{ text: '取消'},{ text: '确定',onPress:() => {
        cancel();
      }}]); 
    }
    confirmOrder(orderNo,event) {
       event.preventDefault();
       event.stopPropagation();

       var _this = this;
       function confirm() {
          Request.ajax('order/confirmOrder',{orderNo:orderNo},(response) => {
            if(response.success) {
              Toast.success('成功确认收货订单', 3);  
              _this.loadData();
            } else {
              Toast.fail(response.description || '确认收货失败', 3);  
            }
          },(error) => {
              Toast.fail('确认收货失败', 3);
          })
       }

      alert('确认', '是否确认收货', [{ text: '取消'},{ text: '确定',onPress:() => {
        confirm();
      }}]); 
    }
    createPayOrder(orderNo,event) {
      event.preventDefault();
      event.stopPropagation();

      WXJSSDK.createPayOrder(orderNo,(res) => {
         PageUrl.forward(PageUrl.result + '#/success');
      });
    }
    render() {
        let { data, animating } = this.state;
        let canCancel = data && data.orderStatus == Enum.orderStatus.num.nopay;
        let needPay = data && data.orderStatus != Enum.orderStatus.num.cancel && data.hasOwnProperty('orderPayPlanList') && data.orderPayPlanList.filter(item => {
          return item.payStatus == Enum.payStatus.num.nopay || item.payStatus == Enum.payStatus.num.payFail || item.payStatus == Enum.payStatus.num.payLostEfficacy;
        });
        return (
            <Container animating={animating} initWeChatConfig={true}>
                {
                  data &&
                  <div>
                    <Header history={this.props.history} title="订单详细" backLink={PageUrl.orderList} />
                    <List className="atl-order-detail-list">
                      <Item extra={data.orderNo}>订单编号</Item>
                      <Item extra={Enum.orderStatus.getValue(data.orderStatus)}>状态</Item>
                      <Item extra={data.rentTimeLength + '个月'}>租期</Item>
                      <Item extra={Enum.payMode.getValue(data.payMode)}>支付类型</Item>
                      <Item extra={data.hasOwnProperty('orderAmountTotal') && '￥' + data.orderAmountTotal.toFixed(2)}>订单金额</Item>
                      {
                        (canCancel || needPay) &&
                        <Item
                          //multipleLine
                          wrap={true}
                          className="order-basic-info-btns"
                        >
                          {
                            canCancel && 
                            <Button type="ghost" size="small" inline onClick={this.cancelOrder.bind(this,data.orderNo)}>取消订单</Button>
                          }
                          {
                            data && data.orderStatus == Enum.orderStatus.num.delivered && 
                            <Button type="ghost" size="small" inline onClick={this.confirmOrder.bind(this,data.orderNo)}>确认收货</Button>
                          }
                          {
                            needPay && 
                            <Button type="ghost" size="small" inline onClick={this.createPayOrder.bind(this,data.orderNo)}>交租</Button>
                          }
                        </Item>
                      }
                    </List>
                    {
                      data.orderConsignInfo && 
                      <List className="my-list" renderHeader={'收货地址'}>
                        <Item
                          thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                          //arrow="horizontal"
                          multipleLine
                          platform="android"
                          wrap={true}
                        >
                          {data.orderConsignInfo.consigneeName} {data.orderConsignInfo.consigneePhone}                
                          <div className="alt-list-brief">广东省深圳市宝安区{data.orderConsignInfo.address}</div>
                        </Item>
                      </List>
                    }

                    <List renderHeader={() => '商品'} className="my-list">
                      {
                          data.orderProductList && data.orderProductList.map((item,i) => {
                            let productModeParams = JSON.parse(item.productModeParams);
                            let mainImg = productModeParams.productImgList[0]
                            return (
                              <Item
                              key={i}
                               multipleLine
                               wrap={true}
                             >
                               <div className="atl-order-item-img">
                                 <img src={mainImg.imgDomain + mainImg.imgUrl} alt={item.productName} />
                               </div>
                               <div className="atl-order-item-info">
                                 <div className="atl-order-item-des">
                                   {productModeParams.productName}
                                 </div>
                                 <div className="atl-order-item-attribute">
                                    {
                                      item.hasOwnProperty('productSkuPropertyList') && item.productSkuPropertyList.map((property,j) => {
                                        return (
                                          <span key={j+'-'+property.propertyValueId} className="atl-order-item-attribute-i">{property.propertyValueName}</span>
                                        )
                                      })
                                    }
                                 </div>
                                 <div className="atl-order-item-info-footer">
                                     <span className="atl-order-item-info-footer-price">月租：￥{item.hasOwnProperty('productUnitAmount') && item.productUnitAmount.toFixed(2)}</span>
                                 </div>
                               </div>
                             </Item>
                            )
                          })
                       }
                    </List>
                    {
                      data.hasOwnProperty('orderPayPlanList') &&
                      <List renderHeader={() => '还租计划'} className="atl-plan-list">
                        {
                          data.orderPayPlanList.map((item,i) => {
                            return (
                              <Item key={i+'-'+item.planId} extra={Rental.format(new Date(item.payTimePlan),'yyyy-MM-dd')}>
                                {i+1}期
                                <span className="price">￥{item.expectAmount.toFixed(2)}</span>
                                <span className={'tag tag'+item.payStatus}>{Enum.payStatus.getValue(item.payStatus)}</span>
                              </Item>
                            )
                          })
                        }
                      </List>
                    }
                  </div>
                }
            </Container>
        );
    }
}

ReactDOM.render(<OrderDetail />, document.getElementById('wrapper'));