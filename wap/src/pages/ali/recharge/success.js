import React, { Component } from 'react';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../../until'
import { Result, Icon, WhiteSpace, WingBlank, Button} from 'antd-mobile';

export default class ResultSuccess extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    viewOrder() {
        PageUrl.forward(PageUrl.aliErpCustomerAccountHome);
    }
    render() {
        return (
            <div className="atl-result">
                <WhiteSpace size="lg" />
                <Result
                    img={<Icon type="check-circle" className="icon" style={{ fill: '#1F90E6' }} />}
                    title="充值成功"
                    message="您已经充值成功，如有到账延迟问题请联系凌雄客服"
                  />
                <WhiteSpace size="lg" />
                <WingBlank>
                   <Button type="primary" onClick={this.viewOrder.bind(this)} >返回</Button>
                 </WingBlank>
                 <WhiteSpace size="lg" />
            </div>
        );
    }
}