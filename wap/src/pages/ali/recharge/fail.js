import React, { Component } from 'react';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../../until'
import { Result, Icon, WhiteSpace, WingBlank, Button} from 'antd-mobile';

export default class ResultFail extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    viewOrder() {
        PageUrl.forward(PageUrl.aliErpCustomerAccountHome + '#/accountBalance');
    }
    render() {
        return (
            <div className="atl-result">
                <WhiteSpace size="lg" />
                <Result
                    img={<Icon type="cross-circle-o" className="icon" style={{ fill: '#F13642' }} />}
                    title="充值失败"
                    message="您可以重新发起充值请求"
                  />
                <WhiteSpace />
                <WingBlank>
                   <Button type="primary" onClick={this.viewOrder.bind(this)} >返回</Button>
                 </WingBlank>
                 <WhiteSpace size="lg" />
            </div>
        );
    }
}