import React, { Component } from 'react';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../../until'
import { Result, Icon, WhiteSpace, WingBlank, Button} from 'antd-mobile';

export default class ResultFail extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    viewOrder() {
        // let lstate = this.props.history.location.state,orderNo = null;
        // if(!!lstate) {
        //     orderNo = lstate.orderNo;
        // }
        // if(!!orderNo) {
        //     PageUrl.forward(PageUrl.aliErpCustomerStatementDetail + "?id=" + orderNo);
        // } else {
        //     PageUrl.forward(PageUrl.aliErpCustomerStatementList);
        // }
        PageUrl.forward(PageUrl.aliErpCustomerAccountHome);
    }
    render() {
        return (
            <div className="atl-result">
                <WhiteSpace size="lg" />
                <Result
                    img={<Icon type="cross-circle-o" className="icon" style={{ fill: '#F13642' }} />}
                    title="支付失败"
                    message="您可以到订单管理页面重新发起支付"
                  />
                <WhiteSpace />
                <WingBlank>
                   <Button type="primary" onClick={this.viewOrder.bind(this)} >查看</Button>
                 </WingBlank>
                 <WhiteSpace size="lg" />
            </div>
        );
    }
}