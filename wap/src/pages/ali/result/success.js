import React, { Component } from 'react';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../../until'
import { Result, Icon, WhiteSpace, WingBlank, Button} from 'antd-mobile';

export default class ResultSuccess extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    viewOrder() {
        // let lstate = this.props.history.location.state,orderNo = null;
        // if(!!lstate) {
        //     orderNo = lstate.orderNo;
        // }
        // if(!!orderNo) {
        //     PageUrl.forward(PageUrl.aliErpCustomerStatementDetail + "?id=" + orderNo);
        // } else {
        //     PageUrl.forward(PageUrl.aliErpCustomerStatementList);
        // }
        PageUrl.forward(PageUrl.aliErpCustomerRechargeOrder);
    }
    render() {
        return (
            <div className="atl-result">
                <WhiteSpace size="lg" />
                <Result
                    img={<Icon type="check-circle" className="icon" style={{ fill: '#1F90E6' }} />}
                    title="完成支付"
                    message="您已经完成支付，如有问题请联系爱租客服"
                  />
                <WhiteSpace size="lg" />
                <WingBlank>
                   <Button type="primary" onClick={this.viewOrder.bind(this)} >查看</Button>
                 </WingBlank>
                 <WhiteSpace size="lg" />
            </div>
        );
    }
}