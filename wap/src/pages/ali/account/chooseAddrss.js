import React, { Component } from 'react';
import { Container } from '../../layout';
import { Request, Rental } from '../../../until'
import { List, WingBlank,WhiteSpace, Button, Icon, Toast } from 'antd-mobile';

const Item = List.Item;

export default class ChooseAddress extends Component {
    constructor(props) {
      super(props);
      this.state = {
        from:this.props.match.params.from,
        data:null,
        visibleAdd:false,
        animating:false,
      };
    }
    componentDidMount() {
      this.loadData();
       console.log(this.props.history)
    }
    linkTo(address) {
      let lstate = this.props.history.location.state, id = Rental.getUrlPara(this.props.history.location.search,'id');

      if(!!lstate == false) {
         lstate = new Object();
         //lstate.id = id;
      }
      lstate.address = address;
      this.props.history.replace({
          pathname: '/product/detail/' + id,
          state:lstate,
        });
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('user/queryUserConsignInfo',{pageNo:1,pageSize:100},(response) => {
        if(response.success) {
          this.setState({
            visibleAdd:response.resultMap.data.totalCount == 0,
            data:response.resultMap.data,
            animating:false,
          })
        } else {
          this.setState({animating:false});
          Toast.fail('加载收获地址失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载收获失败', 3);
      })
    }
    render() {
      let { data, animating } = this.state;
        return (
            <Container animating={animating}>
                {
                  data &&
                  <div>
                    <WhiteSpace size="lg" />
                    <List className="atl-choose-address">
                      {
                        ((data.hasOwnProperty('itemList') && data.itemList)  || []).map((item,i) => {
                            return (
                              <Item
                                key={i+'-'+item.consignId}
                                thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                                arrow="horizontal"
                                multipleLine
                                onClick={this.linkTo.bind(this,item)}
                                platform="android"
                                wrap={true}
                              >
                                {item.consigneeName} {item.consigneePhone}                
                                <div className="alt-list-brief">{item.provinceName}{item.cityName}{item.districtName}{item.address}</div>
                              </Item>
                            )
                        })
                      }
                    </List>
                    {
                      this.state.visibleAdd &&
                      <WingBlank style={{marginTop:"0.5rem"}}>
                        <Button className="btn" type="primary"  onClick={e => {
                          this.props.history.push({
                            pathname: '/account/editAddress/add/0'
                          });
                        }}>新增地址</Button>
                      </WingBlank>
                    }
                  </div>
                }
            </Container>
        );
    }
}