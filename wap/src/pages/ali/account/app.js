import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom";

import AccountHome from "./home.js"
// import Address from "./address.js"
// import EditAddress from "./editAddress.js"
// import Indentity from "./identity.js"
// import ViewIndentity from "./viewIdentity.js"

import AccountBalance from "./accountBalance.js"
import BaseInfo from "./baseInfo.js"
import Risk from "./risk.js"

import "../../../style/account.less"

class App extends Component {
  componentWillMount() {
    
  }
  render() {
    return (
       <Router>
         <div>
           <Route exact path="/" component={AccountHome} />
           
           <Route path="/accountBalance" component={AccountBalance} />
           <Route path="/baseInfo" component={BaseInfo} />
           <Route path="/risk" component={Risk} />

         </div>

      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('wrapper'));
