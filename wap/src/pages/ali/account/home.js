
import React, { Component } from 'react';
import { Img } from '../../../components';
import { FooterTabBar, Container } from '../../layout';
import { Enum, Request, User, PageUrl } from '../../../until'
import { List, Icon, WhiteSpace,Toast } from 'antd-mobile';

const Item = List.Item;
const Brief = Item.Brief;

export default class AccountHome extends Component {
    constructor(props) {
      super(props);
      this.state = {
        user:null,
        animating:false,
      };
    }
    componentDidMount() {
      this.loadUser();
    }
    loadUser() {
       this.setState({animating:true});
        Request.ajax('user/getUserInfo',{},(response) => {
          if(response.success) {
            User.setUser(response.resultMap.data);
            this.setState({
              user:response.resultMap.data,
              animating:false,
            })
          } else {
            this.setState({animating:false});
            Toast.fail('加载用户信息失败', 3);  
          }
        },(error) => {
            this.setState({animating:false});
            Toast.fail('加载用户信息失败', 3);
        })
    }
    linkTo(link) {
        PageUrl.forward(link)
    }
    render() {
        let { user, animating } = this.state;

        let hasUserWx = user && user.hasOwnProperty('userPersonZfb'),
            hasCustomer  = user && user.hasOwnProperty('customer');
        return (
            <Container animating={animating} > 
                {
                  <div className="atl-account-header">
                      <div className="atl-account-header-avatar">
                          <Img img={hasUserWx ? user.userPersonZfb.avatar : null}  />
                      </div>
                      <div className="atl-account-header-text">
                        <div style={{ marginBottom: '0.16rem', fontWeight: 'bold' }}>
                          {hasUserWx && <span>{user.userPersonZfb.nickName}</span>}
                          {hasCustomer && <span>&nbsp;({user.customer.customerName})</span>}
                        </div>
                      </div>
                  </div>
                }
                { hasCustomer && <WhiteSpace size="lg" /> }
                {
                  hasCustomer && 
                  <List>
                   {/*<Item
                                        thumb={<Icon type={require('../../../svg/erpCustomer/account.svg')} size="sm" />}
                                        arrow="horizontal"
                                        onClick={this.linkTo.bind(this, PageUrl.aliErpCustomerAccountHome + '#/accountBalance')}
                                      >
                                        账户金额
                                      </Item>
                                      <Item
                                        thumb={<Icon type={require('../../../svg/erpCustomer/risk.svg')} size="sm" />}
                                        arrow="horizontal"
                                        onClick={this.linkTo.bind(this, PageUrl.aliErpCustomerAccountHome + '#/risk')}
                                      >
                                        授信信息
                                      </Item>
                                       <Item
                                        thumb={<Icon type={require('../../../svg/erpCustomer/baseInfo.svg')} size="sm" />}
                                        arrow="horizontal"
                                        onClick={this.linkTo.bind(this, PageUrl.aliErpCustomerAccountHome + '#/baseInfo')}
                                      >
                                        基本信息
                                      </Item>*/}
                     <Item
                      thumb={<Icon type={require('../../../svg/erpCustomer/statement.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerRecharge)}
                    >充值</Item>
                     <Item
                      thumb={<Icon type={require('../../../svg/order.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerRechargeOrder)}
                    >充值记录</Item>
                 </List>
                 }
                { /*hasCustomer && <WhiteSpace size="lg" />*/ }
                {
                  /*hasCustomer &&
                  <List>
                    <Item
                      thumb={<Icon type={require('../../../svg/erpCustomer/statement.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerStatementList)}
                    >结算单</Item>
                    <Item
                      thumb={<Icon type={require('../../../svg/order.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerOrderList)}
                    >我的订单</Item>
                  </List>*/
                }
                {!!user && !hasCustomer && <WhiteSpace size="lg" />}
                {
                  !!user && !hasCustomer &&
                  <List>
                    <Item
                       thumb={<Icon type={require('../../../svg/erpCustomer/baseInfo.svg')} size="sm" />}
                       arrow="horizontal"
                       onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerBind)}
                     >绑定</Item>
                    <Item
                      thumb={<Icon type={require('../../../svg/erpCustomer/statement.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerUnbindRecharge)}
                    >充值</Item>
                    <Item
                      thumb={<Icon type={require('../../../svg/order.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.aliErpCustomerUnbindRechargeOrder)}
                    >充值记录</Item>
                  </List>
                }

            </Container>
        );
    }
}