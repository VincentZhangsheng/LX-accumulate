import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container } from '../../layout';
import { Header } from '../../../components';
import { ErpEnum, Request, Rental, WXJSSDK, PageUrl, Help, DateHelp  } from '../../../until'
import { List, Button, Icon, Toast, Modal } from 'antd-mobile';

const Item = List.Item;
const alert = Modal.alert;

class StatementDetail extends Component {
    constructor(props) {
      super(props);
      this.state = {
        id:Rental.queryURL('id'),
        data:null,
        animating:false,
      };
    }
    componentDidMount() {
      this.loadData();
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('erp/queryStatementOrderByNo', {statementOrderNo:this.state.id}, (response) => {
        console.log(response)
        if(response.success) {
          this.setState({
            data:response.resultMap.data,
            animating:false,
          });
        } else {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);
      })
    }
    createPayOrder(event) {
      event.preventDefault();
      event.stopPropagation();
       let { data } = this.state;

      this.setState({animating:true});
      WXJSSDK.createPayStatementOrder(data.statementOrderNo,() => {
        this.setState({animating:false});
      }, (res) => {
         PageUrl.forward(PageUrl.aliErpCustomerStatementReult + '#/success');
      });
    }
    render() {
        let { data, animating } = this.state;
        return (
            <Container animating={animating}>
                {
                  data &&
                  <div>
                    <Header history={this.props.history} title="结算单详细" backLink={PageUrl.erpCustomerStatementList} />
                    <List className="atl-order-detail-list">
                      {
                        (data.statementStatus != ErpEnum.statementOrderStatus.num.completed && data.statementStatus != ErpEnum.statementOrderStatus.num.noSttlement) &&
                        <Item>
                          <Button type="ghost" 
                              style={{float:'right'}}
                              size="small" 
                              inline 
                              className="atl-order-row-footer-btns-btn" 
                              onClick={this.createPayOrder.bind(this)}>
                             支付
                          </Button>
                        </Item>
                      }
                       <Item extra={data.statementOrderNo}>编号</Item>
                      <Item extra={<span className={ErpEnum.statementOrderStatus.getClass(data.statementStatus)}>{ErpEnum.statementOrderStatus.getValue(data.statementStatus) }</span>}>
                        结算状态
                      </Item>
                      <Item extra={Help.toFixedPrice(data.statementAmount, 2)}>结算单金额</Item>
                      <Item extra={Help.toFixedPrice(data.statementOverdueAmount, 2)}>逾期金额</Item>
                      <Item extra={Help.toFixedPrice(data.statementPaidAmount, 2)}>已支付金额</Item>
                      <Item extra={DateHelp.shotDate(data.statementExpectPayTime)}>预计支付日期</Item>
                      {data.hasOwnProperty('statementPaidTime') && <Item extra={Help.toFixedPrice(data.statementPaidTime, 2)}>支付时间</Item>}
                      <Item extra={DateHelp.shotDate(data.statementStartTime)}>结算开始日期</Item>
                      <Item extra={DateHelp.shotDate(data.statementEndTime)}>结算结束日期</Item>
                    </List>

                    <div className="am-list-header">结算金额信息</div>
                     <List className="atl-order-detail-list">
                      <Item extra={Help.toFixedPrice(data.statementRentDepositAmount, 2)}>租金押金</Item>
                      <Item extra={Help.toFixedPrice(data.statementRentDepositPaidAmount, 2)}>已付租金押金</Item>
                      <Item extra={Help.toFixedPrice(data.statementRentDepositReturnAmount, 2)}>已退租金押金</Item>

                      <Item extra={Help.toFixedPrice(data.statementRentDepositAmount, 2)}>设备押金</Item>
                      <Item extra={Help.toFixedPrice(data.statementRentDepositPaidAmount, 2)}>已付设备押金</Item>
                      <Item extra={Help.toFixedPrice(data.statementRentDepositReturnAmount, 2)}>已退设备押金</Item>

                      <Item extra={Help.toFixedPrice(data.statementRentAmount, 2)}>租金</Item>
                      <Item extra={Help.toFixedPrice(data.statementRentPaidAmount, 2)}>已付租金</Item>
                      <Item extra={Help.toFixedPrice(data.statementAmount, 2)}>结算单总金额</Item>
                      <Item extra={Help.toFixedPrice(data.statementPaidAmount, 2)}>已付总金额</Item>
                      <Item extra={Help.toFixedPrice(data.statementOverdueAmount, 2)}>逾期金额</Item>
                    </List>

                    <div className="am-list-header">结算明细列表</div>
                    <div>
                      {
                          data.hasOwnProperty('statementOrderDetailList') && data.statementOrderDetailList.map((item,i) => {
                          
                            return (
                              <List  key={i} className="order-product-list-item">
                               <Item extra={item.statementOrderDetailId}>ID</Item>
                               <Item extra={item.itemName}>商品名</Item>
                               <Item extra={Help.toFixedPrice(data.unitAmount, 2)}>单价</Item>
                               <Item extra={item.itemCount}>商品数量</Item>
                               <Item extra={ErpEnum.rentType.getValue(item.itemRentType)}>租赁类型</Item>
                               <Item extra={item.statementDetailPhase}>期数</Item>
                               <Item extra={Help.toFixedPrice(item.statementDetailAmount, 2)}>结算单金额</Item>
                               <Item extra={DateHelp.shotDate(item.statementExpectPayTime)}>预计支付时间</Item>
                               <Item extra={Help.toFixedPrice(item.statementDetailPaidAmount, 2)}>已经支付金额</Item>
                               <Item extra={DateHelp.longDate(item.statementDetailPaidTime)}>结算支付时间</Item>
                               <Item extra={Help.toFixedPrice(item.statementDetailOverdueAmount, 2)}>逾期金额</Item>
                               <Item extra={Help.toFixedPrice(item.statementDetailRentDepositReturnAmount, 2)}>退还租金押金</Item>
                               <Item extra={Help.toFixedPrice(item.statementDetailDepositReturnAmount, 2)}>退还押金</Item>
                               <Item extra={ErpEnum.statementOrderStatus.getValue(item.statementDetailStatus)}>状态</Item>
                               <Item extra={DateHelp.shotDate(item.statementStartTime)}>结算开始时间</Item>
                               <Item extra={DateHelp.shotDate(item.statementEndTime)}>结算开始时间</Item>
                             </List>
                            )
                          })
                       }
                    </div>
                    
                  </div>
                }
            </Container>
        );
    }
}

ReactDOM.render(<StatementDetail />, document.getElementById('wrapper'));