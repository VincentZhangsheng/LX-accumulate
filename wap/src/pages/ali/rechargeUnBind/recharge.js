import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../../layout';
import { Header } from '../../../components';
import { Request, PageUrl, User, AliPayJSSDK, Help } from '../../../until'
import { List, InputItem, WingBlank, Button, Toast, Modal, Picker, TextareaItem } from 'antd-mobile';
import $ from 'zepto';
import { createForm } from 'rc-form';

const alert = Modal.alert;

class Recharge extends Component {
	 constructor(props) {
      super(props);
      this.state = {
      	animating:false,
      	moneyfocused:false,
      	companyList:new Array(),
        lastCharge:new Object(),
      };
    }
    componentDidMount() {
    	this.loadCompany();
      this.loadLastCharge();
    }
    loadCompany() {
    	this.setState({animating:true});
        Request.ajax('erp/subCompanyPage',{
        	pageNo:1,
        	pageSize:10000,
        },(response) => {
          if(response.success) {
          	this.resolveCompany(response.resultMap.data);
            this.setState({animating:false});
          } else {
            this.setState({animating:false});
            Toast.fail('加载凌雄分公司列表失败', 3);  
          }
        },(error) => {
            this.setState({animating:false});
            Toast.fail('加载凌雄分公司列表失败', 3);
        })
    }
    loadLastCharge() {
      this.setState({animating:true});
        Request.ajax('zfbUserPay/findChargeByUserId',{},(response) => {
          if(response.success) {
            // console.log(response.resultMap.data);
            this.setState({
              animating:false,
              lastCharge:response.resultMap.data
            });
          } else {
            this.setState({animating:false});
            Toast.fail('加载默认充值信息', 3);  
          }
        },(error) => {
            this.setState({animating:false});
            Toast.fail('加载默认充值信息', 3);
        })
    }
    resolveCompany(data) {
    	if(data && data.hasOwnProperty('itemList')) {
    		var companyList =  this.state.companyList || new Array();
    		data.itemList.forEach(function(item) {
    			companyList.push({
				    label: item.subCompanyName,
				    value: item.subCompanyCode,
			    })
    		});
    		companyList.length > 0 && this.setState({companyList:companyList});
    	}
    }
    createRechargeOrder(event) {
      event.preventDefault();
      event.stopPropagation();

		let formValues = this.props.form.getFieldsValue();

		if(!formValues.subCompanyCode) {
          alert('提示', '请输选择充值金额所属公司', [{ text: '确定'}]);
          return;
       	}

		if(!Help.checkIsNotNull(formValues.customerName) || Help.clearSpace(formValues.customerName) == "") {
          alert('提示', '请输入客户公司名称', [{ text: '确定'}]);
          return;
       	}
		if(!formValues.hasOwnProperty('money') || (formValues.hasOwnProperty('money') && !formValues.money)) {
			alert('提示', '请输入需要充值的金额', [{ text: '确定'}]);
			return;
		}
		if(parseFloat(formValues.money) <= 0) {
			alert('提示', '充值金额必须大于', [{ text: '确定'}]);
			return;
		}
		this.setState({animating:true});

      AliPayJSSDK.createUnBindRecharegeOrder({
      	amount:formValues.money,
      	customerName:formValues.customerName,
      	subCompanyCode:formValues.subCompanyCode[0],
        remark:formValues.remark
      }, () => {
      	this.setState({animating:false});
      }, (res) => {
          if(res.resultCode == '9000') {
            PageUrl.forward(PageUrl.aliErpCustomerUnbindRechargeResult + '#/success');  
          } else {
            PageUrl.forward(PageUrl.aliErpCustomerUnbindRechargeResult + '#/fail?resultCode='+res.resultCode);  
          }
      });
    }
    render() {
    	let { getFieldProps } = this.props.form, { lastCharge } = this.state, choosedSubCompany = lastCharge.subCompanyNo ? [lastCharge.subCompanyNo] : '';
    	return (
    		<Container animating={this.state.animating} >
	    		<Header title="充值" backLink={PageUrl.aliErpCustomerAccountHome}  />
	    		<List className='rental-list-item'>
    			 	<Picker extra="选择凌雄所属分公司"
	                    cols={1}
	                    data={this.state.companyList}
	                    title="凌雄租赁公司"
	                    {...getFieldProps('subCompanyCode', {
	                      initialValue: choosedSubCompany,
	                    })}
	                    onOk={e => console.log('ok', e)}
	                    onDismiss={e => console.log('dismiss', e)}
	                  >
                    	<List.Item arrow="horizontal">所属公司</List.Item>
                  	</Picker>
	    			      <InputItem
                    {...getFieldProps('customerName',{
                      initialValue:lastCharge.customerName || ''
                    })}
                    type='text'
                    className='customerNameInput'
                    placeholder="请输入客户公司名称"
                  >客户公司</InputItem>
		          <InputItem
		            {...getFieldProps('money', {
		              normalize: (v, prev) => {
		                if (v && !/^(([1-9]\d*)|0)(\.\d{0,2}?)?$/.test(v)) {
		                  if (v === '.') {
		                    return '0.';
		                  }
		                  return prev;
		                }
		                return v;
		              },
		            })}
		            type='money'
		            placeholder="请输入充值金额"
		            onFocus={() => {
		              this.setState({
		                moneyfocused: false,
		              });
		            }}
		            focused={this.state.moneyfocused}
		          >充值金额</InputItem>
              <TextareaItem
                  {...getFieldProps('remark',{
                    initialValue:''
                  })}
                  rows={3}
                  placeholder="请输入备注"
               />
		        </List>
		        <WingBlank style={{marginTop:"0.5rem"}}>
                  <Button className="btn" type="primary"  onClick={this.createRechargeOrder.bind(this)}>充值</Button>
                </WingBlank>
	    	</Container>
		)
    }
}

const RechargeForm = createForm()(Recharge);

ReactDOM.render(<RechargeForm />, document.getElementById('wrapper'));