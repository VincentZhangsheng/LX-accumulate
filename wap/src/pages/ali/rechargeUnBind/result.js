import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom";

import ResultSuccess from "./success.js"
import ResultFail from "./fail.js"
import '../../../style/result.less'

class App extends Component {
  render() {
    return (
       <Router>
         <div>
           <Route exact path="/success" component={ResultSuccess} />
           <Route path="/fail" component={ResultFail} />
         </div>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('wrapper'));
