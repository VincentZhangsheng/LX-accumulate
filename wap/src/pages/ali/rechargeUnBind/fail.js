import React, { Component } from 'react';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../../until'
import { Result, Icon, WhiteSpace, WingBlank, Button} from 'antd-mobile';

export default class ResultFail extends Component {
    constructor(props) {
      super(props);
      this.state = {
        resultCode:Rental.queryURL('resultCode'),
      };
    }
    viewOrder() {
        PageUrl.forward(PageUrl.aliErpCustomerUnbindRecharge);
    }
    render() {
        let title = '充值失败', message = '您可以重新发起充值请求', icon = 'cross-circle-o';
        switch(parseInt(this.state.resultCode)) {
          case 8000:
            title = '支付结果未知';
            message = '后台获取支付结果超时，暂时未拿到支付结果，如有疑问请联系凌雄客服';
            // icon = '';
            break;
          case 6004:
            title = '支付结果未知';
            message = '支付过程中网络出错， 暂时未拿到支付结果，如有疑问请联系凌雄客服';
            // icon = '';
            break;
         case 7001:
            title = '未完成充值';
            message = '客户端-钱包中止快捷支付';
            // icon = '';
            break;
          case 6002:
            title = '支付失败';
            message = '普通网络出错';
            // icon = '';
            break;
          case 6001:
            title = '支付失败';
            message = '您已取消支付';
            // icon = '';
            break;
          case 4000:
            title = '支付失败';
            message = '支付失败，如有疑问请联系凌雄客服';
            // icon = '';
            break;
          case 99:
            title = '未完成支付';
            message = '您通过点击忘记密码快捷界面退出，如有疑问请联系凌雄客服';
            // icon = '';
            break;
        }
        return (
            <div className="atl-result">
                <WhiteSpace size="lg" />
                <Result
                    img={<Icon type={icon} className="icon" style={{ fill: '#F13642' }} />}
                    title={title}
                    message={message}
                  />
                <WhiteSpace />
                <WingBlank>
                   <Button type="primary" onClick={this.viewOrder.bind(this)} >返回</Button>
                 </WingBlank>
                 <WhiteSpace size="lg" />
            </div>
        );
    }
}