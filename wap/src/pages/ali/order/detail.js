import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container } from '../../layout';
import { Header, ProductImg } from '../../../components';
import { ErpEnum, Request, Rental, WXJSSDK, PageUrl, DateHelp } from '../../../until'
import { List, Button, Icon, Toast, Modal, WhiteSpace } from 'antd-mobile';

const Item = List.Item;
const alert = Modal.alert;

class OrderDetail extends Component {
    constructor(props) {
      super(props);
      this.state = {
        id:Rental.queryURL('id'),
        data:null,
        animating:false,
      };
    }
    componentDidMount() {
      this.loadData();
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('erp/queryOrderByNo', {orderNo:this.state.id}, (response) => {
        if(response.success) {
          this.setState({
            data:response.resultMap.data,
            animating:false,
          });
        } else {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载订单信息失败', 3);
      })
    }
    render() {
        let { data, animating } = this.state;
        // let canCancel = data && data.orderStatus == Enum.orderStatus.num.nopay;
        // let needPay = data && data.orderStatus != Enum.orderStatus.num.cancel && data.hasOwnProperty('orderPayPlanList') && data.orderPayPlanList.filter(item => {
        //   return item.payStatus == Enum.payStatus.num.nopay || item.payStatus == Enum.payStatus.num.payFail || item.payStatus == Enum.payStatus.num.payLostEfficacy;
        // });

        let totalProductAmount = data && data.hasOwnProperty('totalProductAmount') ? parseFloat(data.totalProductAmount) : 0;
        let totalMaterialAmount = data && data.hasOwnProperty('totalMaterialAmount') ? parseFloat(data.totalMaterialAmount) : 0;
        let totalRentalAmoutFont = (totalProductAmount + totalMaterialAmount).toFixed(2)
        return (
            <Container animating={animating}>
                {
                  data &&
                  <div>
                    <Header history={this.props.history} title="订单详细" backLink={PageUrl.aliErpCustomerOrderList} />
                    <List className="atl-order-detail-list">
                      <Item extra={data.orderNo}>订单编号</Item>
                      <Item extra={ErpEnum.orderStatus.getValue(data.orderStatus)}>状态</Item>
                      <Item extra={'￥' + totalRentalAmoutFont}>租金总额</Item>
                      <Item extra={data.hasOwnProperty('totalRentDepositAmount') && '￥' + data.totalRentDepositAmount.toFixed(2)}>租金押金</Item>
                      <Item extra={data.hasOwnProperty('totalDepositAmount') && '￥' + data.totalDepositAmount.toFixed(2)}>设备押金</Item>
                      <Item extra={data.hasOwnProperty('logisticsAmount') && '￥' + data.logisticsAmount.toFixed(2)}>运费</Item>
                       <Item extra={data.hasOwnProperty('totalOrderAmount') && '￥' + data.totalOrderAmount.toFixed(2)}>订单金额</Item>
                       <Item extra={data.hasOwnProperty('firstNeedPayAmount') && '￥' + data.firstNeedPayAmount.toFixed(2)}>首付金额</Item>
                    </List>
                    {
                      data.orderConsignInfo && 
                      <List className="my-list" renderHeader={'收货信息'}>
                       <Item extra={ErpEnum.deliveryMode.getValue(data.deliveryMode)}>配送方式</Item>
                       <Item extra={DateHelp.shotDate(data.expectDeliveryTime)}>配送日期</Item>
                        <Item
                          thumb={<Icon type={require('../../../svg/address.svg')} size="sm" />}
                          //arrow="horizontal"
                          multipleLine
                          platform="android"
                          wrap={true}
                        >
                          {data.orderConsignInfo.consigneeName} {data.orderConsignInfo.consigneePhone}                
                          <div className="alt-list-brief">{data.orderConsignInfo.provinceName}{data.orderConsignInfo.cityName}{data.orderConsignInfo.districtName}{data.orderConsignInfo.address}</div>
                        </Item>
                      </List>
                    }

                    <List className="my-list" renderHeader={'订单日期'}>
                      <Item extra={DateHelp.shotDate(data.deliveryTime)}>发货时间</Item>
                      <Item extra={DateHelp.shotDate(data.confirmDeliveryTime)}>收货时间</Item>
                      <Item extra={DateHelp.shotDate(data.rentStartTime)}>起租日期</Item>
                      <Item extra={DateHelp.shotDate(data.expectReturnTime)}>预计归还日期</Item>
                      <Item extra={DateHelp.shotDate(data.actualReturnTime)}>实际归还时间</Item>
                    </List>

                    <div className="am-list-header">商品</div>
                    <div>
                      {
                          data.orderProductList && data.orderProductList.map((item,i) => {
                             let productSkuSnapshot = item.hasOwnProperty('productSkuSnapshot') ? JSON.parse(item.productSkuSnapshot) : {};
                             let mainImg = productSkuSnapshot.hasOwnProperty('productImgList') ? productSkuSnapshot.productImgList[0] : {};
                            // let productSkuSnapshot = JSON.parse(item.productSkuSnapshot);
                            // let mainImg = productSkuSnapshot.productImgList[0]
                            return (
                              <List  key={i} className="order-product-list-item">
                                <Item
                                 multipleLine
                                 wrap={true}
                               >
                                 <div className="atl-order-item-img">
                                   <ProductImg img={mainImg} alt={item.productName} />
                                 </div>
                                 <div className="atl-order-item-info">
                                   <div className="atl-order-item-des">
                                     {productSkuSnapshot.productName}
                                   </div>
                                   <div className="atl-order-item-attribute">
                                      {
                                        item.hasOwnProperty('productSkuPropertyList') && item.productSkuPropertyList.map((property,j) => {
                                          return (
                                            <span key={j+'-'+property.propertyValueId} className="atl-order-item-attribute-i">{property.propertyValueName}</span>
                                          )
                                        })
                                      }
                                   </div>
                                   <div className="atl-order-item-info-footer">
                                       <span className="atl-order-item-info-footer-price">单价：￥{item.hasOwnProperty('productUnitAmount') && item.productUnitAmount.toFixed(2)}</span>
                                       <span className="atl-order-item-info-footer-price" style={{marginLeft:'0.32rem'}}>数量：{item.productCount}</span>
                                   </div>
                                 </div>
                               </Item>
                               <Item extra={ErpEnum.rentType.getValue(item.rentType) + ' '+ item.rentTimeLength + ErpEnum.rentType.getUnit(item.rentType)}>租期</Item>
                               <Item extra={ErpEnum.payMode.getValue(item.payMode) + ' 押'+ item.depositCycle + '付' + item.paymentCycle}>支付方式</Item>
                             </List>
                            )
                          })
                       }
                    </div>


                    <div className="am-list-header">配件</div>
                    <div>
                      {
                          data.orderMaterialList && data.orderMaterialList.map((item,i) => {
                            let materialSnapshot = item.hasOwnProperty('materialSnapshot') ? JSON.parse(item.materialSnapshot) : {};
                            let mainImg = materialSnapshot.hasOwnProperty('materialImgList') ? materialSnapshot.materialImgList[0] : {};
                            return (
                            <List key={i} className="order-product-list-item">
                                <Item
                                key={i}
                                 multipleLine
                                 wrap={true}
                               >
                                 <div className="atl-order-item-img">
                                    <ProductImg img={mainImg} alt={item.materialName} />
                                 </div>
                                 <div className="atl-order-item-info">
                                   <div className="atl-order-item-des">
                                     {materialSnapshot.materialName}
                                   </div>
                                   <div className="atl-order-item-attribute">
                                     <span className="atl-order-item-attribute-i">品牌：{materialSnapshot.brandName}</span>
                                   </div>
                                   <div className="atl-order-item-info-footer">
                                       <span className="atl-order-item-info-footer-price">单价：￥{item.hasOwnProperty('materialUnitAmount') && item.materialUnitAmount.toFixed(2)}</span>
                                       <span className="atl-order-item-info-footer-price" style={{marginLeft:'0.32rem'}}>数量：{item.materialCount}</span>
                                   </div>
                                 </div>
                               </Item>
                               <Item extra={ErpEnum.rentType.getValue(item.rentType) + ' '+ item.rentTimeLength + ErpEnum.rentType.getUnit(item.rentType)}>租期</Item>
                               <Item extra={ErpEnum.payMode.getValue(item.payMode) + ' 押'+ item.depositCycle + '付' + item.paymentCycle}>支付方式</Item>
                             </List>
                            )
                          })
                       }
                    </div>

                  </div>
                }
            </Container>
        );
    }
}

ReactDOM.render(<OrderDetail />, document.getElementById('wrapper'));