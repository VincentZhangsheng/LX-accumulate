import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container } from '../../layout';
import { Header, ProductImg } from '../../../components';
import { ErpEnum, Request, WXJSSDK, PageUrl} from '../../../until'
import { ListView, Tabs, Button, Toast, Modal } from 'antd-mobile';

const TabPane = Tabs.TabPane;
const alert = Modal.alert;

export default class OrderList extends Component {
    constructor(props) {
      super(props);
      const dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.state = {
        searchData:{
          pageNo:1,
          pageSize:4,
          orderStatus:-1,
        },
        dataSource,
        dataList:[],
        refreshing: true,
        hasMore:true,
        height: document.documentElement.clientHeight,
      };
    }
    linkToDetail(rowData) {
      PageUrl.forward(PageUrl.aliErpCustomerOrderDetail + "?id=" + rowData.orderNo);
    }
    componentDidMount() {
      this.lv && setTimeout(() => this.setState({
        height: this.state.height - ReactDOM.findDOMNode(this.lv).offsetTop,
      }), 0);
      this.loadData();
    }
    loadData(pageNo) {
      let { searchData, hasMore, dataList } = this.state;
      searchData.pageNo = pageNo || 1;

      if(searchData.orderStatus == -1) {
        delete searchData.orderStatus;
      }

      this.setState({ isLoading: true });
      Request.ajax('erp/queryAllOrder',searchData,(response) => {
        var data = [];
        if(response.success) {
          data = response.resultMap.data.itemList;
          hasMore = searchData.pageNo < response.resultMap.data.pageCount;
          if(searchData.pageNo == 1) {
            dataList = data;
          } else {
            dataList = dataList.concat(data);  
          }
        } else {
          hasMore = false;
          Toast.fail('加载订单信息失败', 3);
        }

        this.setState({
          dataList:dataList,
          dataSource: this.state.dataSource.cloneWithRows(dataList),
          refreshing: false,
          hasMore:hasMore,
          showFinishTxt: true,
          searchData:searchData,
          isLoading:false,
        });
      },(error) => {
        Toast.fail('加载订单信息失败', 3);
      })
    }
    onScroll = (e) => {
      this.scrollerTop = e.scroller.getValues().top;
      this.domScroller = e;
    }
    onEndReached = (event) => {
      if (this.state.isLoading || !this.state.hasMore) {
        return;
      }
      this.setState({ isLoading: true });
      this.loadData(this.state.searchData.pageNo + 1);

    };
    scrollingComplete = () => {
      if (this.scrollerTop >= -1) {
        this.setState({ showFinishTxt: false });
      }
    }
    renderCustomIcon() {
      return [
        <div key="0" className="am-refresh-control-pull">
          <span>{this.state.showFinishTxt ? '刷新完毕' : '下拉可以刷新'}</span>
        </div>,
        <div key="1" className="am-refresh-control-release">
          <span>松开立即刷新</span>
        </div>,
      ];
    }
    renderListView() {
      const separator = (sectionID, rowID) => (
        <div
          key={sectionID+"-"+rowID}
          style={{
            backgroundColor: '#F5F5F9',
            height: "0.1rem",
            borderTop: '1px solid #ECECED',
            borderBottom: '1px solid #ECECED',
          }}
        />
      );
      const row = (rowData, sectionID, rowID) => {
          console.log('rowData', rowData)
          let firstProduct = rowData.hasOwnProperty('orderProductList') ? rowData.orderProductList[0]  || {} : {};
          let productSkuSnapshot = firstProduct.hasOwnProperty('productSkuSnapshot') ? JSON.parse(firstProduct.productSkuSnapshot) : {};
          let mainImg = productSkuSnapshot.hasOwnProperty('productImgList') ? productSkuSnapshot.productImgList[0] : {};

          return (
            <div key={rowID+'-'+rowData.orderId} className="atl-order-row" onClick={this.linkToDetail.bind(this,rowData)}>
              <div className="atl-order-row-head">
                订单编号：{rowData.orderNo}
              </div>
              <div className="atl-order-row-content">
                <div className="atl-order-row-img">
                  <ProductImg img={mainImg}  />
                </div>
                <div className="atl-order-row-info">
                  <div className="atl-order-row-des">
                    {firstProduct.productName}
                  </div>
                </div>
              </div>
              <div className="atl-order-row-footer">
                <div className="atl-order-row-footer-price">
                  共{rowData.totalProductCount}件商品 订单金额：<span className="atl-order-row-footer-price-a">￥{rowData.totalOrderAmount.toFixed(2)}</span>
                </div>
              </div>
            </div>
          );
      };
      return (
        <ListView
            ref={el => this.lv = el}
            dataSource={this.state.dataSource}
            //renderHeader={() => <span>Pull to refresh</span>}
            renderFooter={() => (<div style={{ padding: "0.3rem", textAlign:"center" }}>{this.state.isLoading ? "加载中..." : ""}</div>)}
            renderRow={row}
            renderSeparator={separator}
            initialListSize={4}
            pageSize={4}
            style={{
              height: this.state.height,
              //border: '1px solid #ddd',
              margin: '0.1rem 0',
            }}
            scrollerOptions={{ scrollbars: true, scrollingComplete: this.scrollingComplete }}
            //onScroll={this.onScroll}
            scrollRenderAheadDistance={200}
            scrollEventThrottle={20}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={10}
          />
      )
    }
    renderTabList() {
      let tabArray = new Array();
      tabArray.push(<TabPane tab={"全部"} key="-1">{this.renderListView()}</TabPane>)
      for (var type in ErpEnum.orderStatus.num) {
        let n = ErpEnum.orderStatus.num[type];
        tabArray.push(<TabPane tab={ErpEnum.orderStatus.getValue(n)} key={n} >{this.renderListView()}</TabPane>)
      }
      return tabArray;
    }
    cahangeTab(status) {
      let { searchData, dataSource } = this.state;
      if(searchData.orderStatus != status) {
        searchData.orderStatus = status;
        searchData.pageNo = 1;
        this.setState({
          searchData:searchData,
        });
        this.loadData();
      }
    }
    render() {
        return (
            <Container>
                <Header history={this.props.history} title="订单列表" backLink={PageUrl.aliErpCustomerAccountHome} />
                <Tabs defaultActiveKey="-1" animated={false} onChange={this.cahangeTab.bind(this)}>
                  {this.renderTabList()}
                </Tabs>
            </Container>
        );
    }
}


ReactDOM.render(<OrderList />, document.getElementById('wrapper'));