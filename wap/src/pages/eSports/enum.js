export default {
	//订单状态
	orderStatus:{
		num:{
			unActive:0,
			unPay:1,
			paying:2,
			paid:3,
			delivered:4,
			renting:5,
			refunded:6,
			hasReturn:7,
			payFial:8,
		},
		getValue:function(num) {
			switch(num) {
				case this.num.nopay:
					return "未激活";
				case this.num.unPay:
					return "未支付";
				case this.num.paying:
					return "支付中";
				case this.num.paid:
					return "已支付";
				case this.num.delivered:
					return "已发货";
				case this.num.renting:
					return "租赁中";
				case this.num.refunded:
					return "已退款";
				case this.num.hasReturn:
					return "已退货";
				case this.num.payFial:
					return "支付失败";
			}
			return "";
		}
	},
}