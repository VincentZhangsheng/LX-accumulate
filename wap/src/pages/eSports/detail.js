import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Request, PageUrl, WXJSSDK, Help } from '../../until'
import { Button, Flex, Carousel, Stepper, List, WhiteSpace, WingBlank, Toast, Modal, Icon, TextareaItem, Picker, DatePicker, InputItem } from 'antd-mobile';
import $ from 'zepto';
import classnames from 'classnames';
import Data from  "./data.js"
import { DateHelp } from '../../until'
import moment from 'moment';
import Enum from './enum.js';

const Item = List.Item;
const alert = Modal.alert;

const _rentStartTime = moment(DateHelp.foramtDate(new Date(), 'yyyy-MM-dd')).add(2,'day').format('YYYY-MM-DD');
const documentationImg = ['01.jpg','02.jpg','03.jpg'];

export default class ProductDetail extends Component {
    constructor(props) {
      super(props);
      let product = Data.getProductByID(this.props.match.params.id);
      this.state = {
        animating:false,
        id:this.props.match.params.id,
        data:product,
        rentTimeLength:3,
        rentStartTime:_rentStartTime,
        couponCode:undefined,
        chooseTab:'productDesImg',//productDesImg, productPropertyList
        checkeing:false,
      };
    }  
    componentDidMount() {
    }
    storage = (orderNumber) => {
      let { rentTimeLength, rentStartTime, couponCode, data } = this.state;
      sessionStorage.setItem('order_prams', JSON.stringify({
        productId:data.productId,
        rentTimeLength:rentTimeLength,
        rentStartTime:rentStartTime,
        couponCode:couponCode,
        orderNumber:orderNumber,
      }));
    }
    createOrder = () => {
      let { rentTimeLength, rentStartTime, couponCode, data } = this.state;
      if(!!rentStartTime == false) {
        alert('提示', '请选择起租时间', [{ text: '确定'}]);
        return;
      }
      if(new Date(rentStartTime).getTime() <= new Date().getTime()) {
         alert('提示', '起租日期必须大于当前日期', [{ text: '确定'}]);
        return;
      }
      if(couponCode && this.getCouponAmount(rentTimeLength) <= 0) {
        alert('提示', '您选择的租期暂不能使用优惠劵', [{ text: '确定'}]);
        return;
      }
      this.storage();
      couponCode ? this.checkCouponCodeUsedInOrder() : this.toNextStep();
    }
    checkCouponCodeUsedInOrder = () => {
      const { couponCode } = this.state;
      if(!!couponCode == false) return;
      this.setState({checkeing:true});
      Request.ajax('activity/checkCouponCode', {couponCode:couponCode}, (response) => {
        if(response.success) {
          this.setState({checkeing:false});
          if(response.resultMap.data) {
            this.storage(response.resultMap.data.orderNumber)
            if(response.resultMap.data.status == Enum.orderStatus.num.unActive) {
              //未激活状态
              this.toNextStep();
            } else if (
              response.resultMap.data.status == Enum.orderStatus.num.unPay
              || response.resultMap.data.status == Enum.orderStatus.num.paying
              || response.resultMap.data.status == Enum.orderStatus.num.payFial
            ) {
              this.linkToConfirmOrder(response.resultMap.data.orderNumber);
            } else {
               this.props.history.push({
                 pathname:  `/orderDetail/${response.resultMap.data.orderNumber}`,
               });
            }
          } else {
             Toast.fail('输入优惠劵码不正确，请重新确认', 3);
            // this.toNextStep();
          }
        } else {
          this.setState({checkeing:false});
          alert('优惠劵验证', response.description || "验证优惠劵，请重试", [{ text: '确定'}]);
        }
      }, (error) => {
          this.setState({checkeing:false});
          alert('优惠劵验证', "验证优惠劵，请重试", [{ text: '确定'}]);
      });
    }
    toNextStep = () => {
      this.props.history.push({
        pathname: '/inputCustomerInfo',
      });
    }
    linkToConfirmOrder = (no) => {
      if(Help.isWeiXin()) {
        window.location.href = `${PageUrl.loginForActive}?orderNo=${no}`;
      } else {
        this.props.history.push({
          pathname:  `/codeConfirm/${no}`,
        });
      }
    }
    getRentalPrice = (rentTimeLength) => {
      const { data } = this.state;
      const rentalPrice = data.priceList.reduce(function(pre, item, index) {
          pre[item.month] = item;
          return pre;
      }, {})[rentTimeLength].price;
      return rentalPrice;
    }
    getCouponAmount = (rentTimeLength) => {
      const { data, couponCode } = this.state;
      const rentalPrice = this.getRentalPrice(rentTimeLength);
      const  couponAmount = couponCode && rentalPrice >= data.greaterUseCoupon ? data.couponAmount : 0;
      return couponAmount;
    }
    render() {
        const { data, rentTimeLength, rentStartTime, couponCode, checkeing }  = this.state;

        const rentTimeLengthArr = data.priceList.map(item => {
          return {
            label:`${item.monthDes}/￥${item.price.toFixed(2)}`,
            value:item.month,
          }
        });

        const rentalPrice = this.getRentalPrice(rentTimeLength);
        const  couponAmount = this.getCouponAmount(rentTimeLength);
        const needPay = rentalPrice - couponAmount;

        return (
            <Container animating={this.state.animating}>
              {
                data &&
                <div>
                    {/*<Header history={this.props.history} title="商品详细" backLink={PageUrl.product} />*/}
                    <div className="prod-handle">
                      <div className="need-pay">
                        <span className="lbl">应付：</span>
                        <span className="price"><em>￥</em>{needPay.toFixed(2)}</span>
                      </div>
                      <Button type="primary" className="btn" onClick={this.createOrder} loading={checkeing} disabled={checkeing}>{checkeing ? "处理中..." : "立即下单"}</Button>
                    </div>
                    <div className="product-detail-head">
                      <div className="main-img">
                        <img src={require(`./image/${data.productId}/p_detail.jpg`)}  />
                      </div>
                      <List>
                        <Item
                          multipleLine
                          wrap={true}
                          className="prod-info"
                        >
                          <div className="prod-name">{data.productName} {data.configure}</div>           
                          <div className="prod-price">
                            <div className="price-mon">
                              <span className="num"><em className="rmb">￥</em>{(data.priceList[2].price / data.priceList[2].month).toFixed(2)}</span>/月
                            </div>
                            <div className="price-mon" style={{float:'right'}}>价值:￥{data.cost}</div>
                          </div>
                        </Item>
                      </List>
                    </div>
                    <WhiteSpace size="sm" />

                    <List className="to-order-info">
                       <Picker 
                          extra="请选择"
                          cols={1}
                          data={rentTimeLengthArr}
                          title="选择租期"
                          value={[rentTimeLength]}
                          onOk={e => this.setState({rentTimeLength:e[0]})}
                          onDismiss={e => console.log('dismiss', e)}
                        >
                          <List.Item arrow="horizontal"><span className="lbl">租期</span></List.Item>
                        </Picker>
                        <DatePicker
                          mode="date"
                          title="选择起租日期"
                          extra="请选择"
                          value={moment(rentStartTime)}
                          onChange={(e) => { this.setState({rentStartTime:e.format('YYYY-MM-DD')}); }}
                        >
                          <List.Item arrow="horizontal"><span className="lbl">起租时间</span></List.Item>
                        </DatePicker>
                        <InputItem
                          placeholder="请输入优惠劵码"
                          value={couponCode}
                          onChange={(val) => { this.setState({couponCode:val}); }}
                        ><span className="lbl" style={{color:"#f23d3d"}}>优惠劵</span></InputItem>
                    </List>
                    <WhiteSpace size="sm" />

                    <List className="to-order-info">
                      <Item extra={`￥${rentalPrice.toFixed(2)}`}><div className="lbl">租金金额</div></Item>      
                      <Item extra={`￥0.00`}><div className="lbl">押金</div></Item>      
                      <Item extra={`-￥${couponAmount.toFixed(2)}`}><div className="lbl" style={{color:"#f23d3d"}}>优惠劵</div></Item>      
                      <Item extra={`￥${needPay.toFixed(2)}`}><div className="lbl" style={{color:"#f23d3d"}}>应付金额</div></Item>      
                    </List>
                    <WhiteSpace size="sm" />

                    <div className="prod-tabs">
                      <div className={this.state.chooseTab == 'productDesImg' ? "tab choose" : 'tab'} onClick={() => { this.setState({chooseTab:'productDesImg'}) }}>商品介绍</div>
                      <div className={this.state.chooseTab == 'productPropertyList' ? "tab choose" : 'tab'} onClick={() => { this.setState({chooseTab:'productPropertyList'}) }}>商品参数</div>
                      <div className={this.state.chooseTab == 'rentalExplain' ? "tab choose" : 'tab'} onClick={() => { this.setState({chooseTab:'rentalExplain'}) }}>租赁说明</div>
                    </div>
                    <WhiteSpace size="xs" />
                    {
                      this.state.chooseTab == 'productDesImg' && 
                      <div className="tabContent detail-productDesImg">
                        {
                           data.hasOwnProperty('productDescImg') && data.productDescImg.map((item , index)=> {
                            return (
                              <img src={require(`./image/${data.productId}/des/${item}`)} style={{width:'100%'}} key={index} />
                            )
                          })
                        }
                      </div>
                    }
                    {
                      this.state.chooseTab == 'productPropertyList' &&
                      <div className="tabContent detail-productDesImg">
                        {
                          data.hasOwnProperty('configureImages') && data.configureImages.map((item , index)=> {
                            return (
                              <img src={require(`./image/${data.productId}/${item}`)} style={{width:'100%'}} key={index} />
                            )
                          })
                        }
                      </div>
                    }
                    {
                      this.state.chooseTab == 'rentalExplain' &&
                      <div className="tabContent detail-productDesImg">
                         {
                          documentationImg.map((item , index)=> {
                            return (
                              <img src={require(`./image/documentation/${item}`)} style={{width:'100%'}} key={index} />
                            )
                          })
                        }
                      </div>
                    }
                </div>
              }
            </Container>
        );
    }
}