import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom";

import ProductList from "./listNew.js"
import ProductDetail from "./detail.js"
import InputCustomerInfo from "./inputCustomerInfo.js"
import Confirm from "./confirm.js"
import CodeConfirm from "./codeConfirm.js"
import Success from "./success.js"
import OrderDetail from "./order.js"
import "./app.less"

class App extends Component {
  componentWillMount() {
  }
  render() {
    return (
       <Router>
         <div>
           <Route exact path="/list" component={ProductList} />
           <Route path="/detail/:id" component={ProductDetail} />
           <Route path="/inputCustomerInfo" component={InputCustomerInfo} />
           <Route path="/confirm/:no" component={Confirm} />
           <Route path="/codeConfirm/:no" component={CodeConfirm} />
            <Route path="/orderDetail/:no" component={OrderDetail} />
           <Route path="/success" component={Success} />
         </div>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('wrapper'));
