import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import {Enum, Request, PageUrl, WXJSSDK, AreaDictionary, Help } from '../../until'
import { Button, Flex, Carousel, Stepper, List, WhiteSpace, WingBlank, Toast, Modal, Icon, TextareaItem, Picker, DatePicker, InputItem } from 'antd-mobile';
import $ from 'zepto';
import classnames from 'classnames';
import Data from  "./data.js"
import { createForm } from 'rc-form';
import AreaData from "./area.js"
import CutDownBtn from "./cutDown.js"

const Item = List.Item;
const alert = Modal.alert;

 const defaultCustomerInfo = {};

// const defaultCustomerInfo = {
//   schoolName:'深圳大学',
//   schoolNumber:'0001',
//   realName:'关羽',
//   phone:'',
//   province:300001,
//   city:300064,
//   district:300575,
//   areaDictionary:[300001, 300064, 300575],
//   address:'南山区科苑路',
//   personNo:'422827199009080030',
//   connectRealName:'张飞',
//   connectPhone:'18033402833',
//   remark:'这是一个订单备注',
// }

class InputCustomerInfo extends Component {
    constructor(props) {
      super(props);

      let _orderPrams = sessionStorage.getItem('order_prams');
      _orderPrams =  _orderPrams ?  JSON.parse(_orderPrams) : null;

      let product = Data.getProductByID(_orderPrams.productId);

      this.state = {
        orderPrams:_orderPrams,
        product:product,
        animating:false,
        areaDictionary:[],
        customerInfo:defaultCustomerInfo || {},
        submiting:false, //提交订单状态
        isVerifyPhone:false,
      };
    } 
    componentWillMount() {}
    componentDidMount() {
      this.loadAreaDictionary();
    } 
    loadAreaDictionary = () => {
      var areaMap = AreaDictionary.getMap();
      if(areaMap) {
        // this.setState({areaDictionary:areaDictionary})
        this.resolveArea();
      } else {
        Request.ajax('data/findDataByType',{dataType:Enum.dictionaryType.num.area},(response) => {
          if(response.success) {
            try {
               this.setAreaState(response.resultMap.data);
            } catch(e) {
               Toast.fail('加载地区列表失败', 3);  
            }
          } else {
            Toast.fail('加载地区列表失败', 3);  
          }
        },(error) => {
            Toast.fail('加载地区列表失败', 3);
        })
      }
    }
    setAreaState = (data) => {
      AreaDictionary.set(data);
      this.resolveArea();
    }
    resolveArea = () => {
      let _map = AreaDictionary.getMap();
      function getArea(ds) {
        (ds || []).forEach(item => {
          if(item.hasOwnProperty('children')) {
            getArea(item.children)
          } else {
            item.children = _map[item.value].children;
          }
        })
      }
      getArea(AreaData);
      this.setState({
        areaDictionary:AreaData,
      })
    }
    createOrder() {
      try {

        if(!this.state.isVerifyPhone) {
          Toast.fail('请先获取验证码，验证手机号', 3);  
          return;
        }

        this.props.form.validateFields((error, value) => {
          
          if(error) {
            let keys = Object.keys(error);
            Toast.fail(error[keys[0]].errors[0].message, 3);  
            return false
          };

          if($.trim(value.realName) == $.trim(value.connectRealName)) {
            Toast.fail('姓名和紧急联系人姓名不能相同', 3);  
            return false
          }

          if($.trim(value.phone) == $.trim(value.connectPhone)) {
            Toast.fail('手机号和紧急联系人电话不能相同', 3);  
            return false
          }

          let commitData = this.state.orderPrams;
          commitData.schoolName = value.schoolName;
          commitData.schoolNumber = value.schoolNumber;
          commitData.remark = value.remark;

          let customerPerson = {};
          customerPerson.realName = value.realName;
          customerPerson.phone = value.phone;

          customerPerson.province = value.areaDictionary[0];
          if(value.areaDictionary.length > 1) {
            customerPerson.city = value.areaDictionary[1];  
          }
          if(value.areaDictionary.length > 2) {
            customerPerson.district = value.areaDictionary[2];  
          }

          customerPerson.address = value.address;
          customerPerson.personNo = value.personNo;
          customerPerson.connectRealName = value.connectRealName;
          customerPerson.connectPhone = value.connectPhone;
          // customerPerson.remark = value.remark;

          commitData.customerPerson = customerPerson;
          this.setState({submiting:true});
          Request.ajax('activity/applyOrder',commitData,(response) => {
            this.setState({submiting:false});
            if(response.success && response.resultMap && response.resultMap.hasOwnProperty('data')) {
              this.linkToConfirmOrder(response.resultMap.data.orderNumber);
            } else {
              Toast.fail(`提交订单失败${response.description ? ':'+response.description : ""}`, 3);  
            }
          },(error) => {
              this.setState({submiting:false});
              Toast.fail('提交订单失败', 3);
          })

        });
      } catch(e) {
        console.log(e)
        this.setState({submiting:false});
        Toast.fail('提交订单失败', 3);
      }
    }
    linkToConfirmOrder = (no) => {
      if(Help.isWeiXin()) {
        window.location.href = `${PageUrl.loginForActive}?orderNo=${no}`;
      } else {
        this.props.history.push({
          pathname:  `/codeConfirm/${no}`,
        });
      }
    }
    sendVerifyCode = () => {
        this.props.form.validateFields(['phone'], { force: true }, (error, values) => {
          console.log(error, values, Help.checkMobile(values.phone))
          if(error) {
            console.log(error)
            let keys = Object.keys(error);
            Toast.fail(error[keys[0]].errors[0].message, 3);  
            return false
          };

          if(!Help.checkMobile(values.phone)) {
            Toast.fail('请输入正确手机号', 3);  
            return false
          }

          this.refs.cutDownBtn.btnStatus(true);
          Request.ajax('activity/sendVerifyCode', {phone:Help.clearSpace(values.phone)}, (response) => {
              this.refs.cutDownBtn.btnStatus(false);
              if(response.success) {
                Toast.success('发送验证码成功', 3);  
                this.refs.cutDownBtn.cutDown();
              } else {
                Toast.fail(response.description || '发送验证码失败', 3);  
              }
            },(error) => {
              this.refs.cutDownBtn.btnStatus(false);
              Toast.fail('发送验证码失败', 3);
          });

        });
        
    }
    checkVerifyCode = () => {
      this.props.form.validateFields(['phone', 'verifyCode'], { force: true }, (error, values) => {
          if(!!values.verifyCode == false) return;
          if(error) {
            console.log(error)
            let keys = Object.keys(error);
            Toast.fail(error[keys[0]].errors[0].message, 3);  
            return false
          };
          Request.ajax('activity/checkVerifyCode', values, (response) => {
              if(response.success) {
                Toast.success('成功验证手机号', 3);  
                this.setState({isVerifyPhone:true});
              } else {
                Toast.fail(response.description || '手机号验证失败', 3);  
              }
            },(error) => {
              Toast.fail('手机号验证失败', 3);
          });

        });
    }
    onChangePhone = (value) => {
      this.setState({
        isVerifyPhone:false,
      });
    }
    onBlurVerifyCode = (value) => {
      this.checkVerifyCode();
    }
    getNeedPayPrice = () => {
      const { orderPrams, product } = this.state;
      const rentalPrice = product.priceList.reduce(function(pre, item, index) {
        pre[item.month] = item;
        return pre;
      }, {})[orderPrams.rentTimeLength].price;

      const couponAmount = orderPrams.couponCode && rentalPrice >= product.greaterUseCoupon ? product.couponAmount : 0;

      const needPay = rentalPrice - couponAmount;

      return needPay.toFixed(2);
    }
    checkPhone = (rule, value, callback) => {
      if(!Help.checkMobile(value)) {
        callback('请输入正确手机号');
      } else {
        callback();
      }
    }
    checkConnectPhone = (rule, value, callback) => {
      if(!Help.checkMobile(value)) {
        callback('请输入紧急联系人手机号');
      } else {
        callback();
      }
    }
    checkPersonNo = (rule, value, callback) => {
      if(!Help.checkNumer(value)) {
        callback('身份证号只能输入数字');
      } else {
        callback();
      }
    }
    render() {
        const { getFieldProps } = this.props.form;
        const { animating, areaDictionary, customerInfo, submiting, }  = this.state;
        return (
            <Container animating={this.state.animating}>
               <div>
                    <div className="prod-handle">
                      <div className="need-pay">
                        <span className="lbl">应付：</span>
                        <span className="price"><em>￥</em>{this.getNeedPayPrice()}</span>
                      </div>
                      <Button type="primary" className="btn" onClick={this.createOrder.bind(this)} loading={submiting} disabled={submiting}>{submiting ? "处理中..." : "提交订单"}</Button>
                    </div>

                    <WhiteSpace size="sm" />
                    
                    <List className="to-order-info">
                        <InputItem
                          {...getFieldProps('realName',{
                            initialValue:customerInfo.realName || '',
                            rules: [
                              {required: true, message:'请输入姓名'}
                            ]
                          })}
                          // error={true}
                          placeholder="请输入真实姓名"
                        ><span className="mustInput">*</span>姓名</InputItem>
                        <InputItem
                          {...getFieldProps('personNo',{
                            initialValue:customerInfo.personNo || '',
                            rules: [
                              {required: true, message:'请输入身份证号'},
                              {
                                validator: this.checkPersonNo,
                              },
                            ]
                          })}
                          maxLength={26}
                          placeholder="请输入身份证号"
                        ><span className="mustInput">*</span>身份证号</InputItem>
                        <InputItem
                          {...getFieldProps('phone',{
                            onChange:(val) => {
                              this.onChangePhone(val)
                            },
                            initialValue:customerInfo.phone || '',
                             rules: [
                              {required: true, message:'请输入手机号'},
                              {
                                validator: this.checkPhone,
                              },
                            ]
                          })}
                          maxLength={11}
                          placeholder="请输入手机号"
                        ><span className="mustInput">*</span>手机号</InputItem>
                        <InputItem
                          extra={<CutDownBtn ref="cutDownBtn" sendVerifyCode={this.sendVerifyCode} />}
                          {...getFieldProps('verifyCode',{
                            initialValue:customerInfo.verifyCode || '',
                            rules: [
                              {required: true, message:'请输入验证码'}
                            ]
                          })}
                          onBlur={(val) => { this.onBlurVerifyCode(val); }}
                          placeholder="请输入验证码"
                        ><span className="mustInput">*</span>验证码</InputItem>
                       
                    </List>
                    <WhiteSpace size="sm" />

                    <List className="to-order-info">
                       <Picker extra="请选择"
                          cols={3}
                          data={areaDictionary}
                          title="选择地区"
                          {...getFieldProps('areaDictionary',{
                            initialValue:customerInfo.areaDictionary || [],
                            rules: [
                              {required: true, message:'请选择地区'}
                            ]
                          })}
                          onOk={e => console.log('ok', e)}
                          onDismiss={e => console.log('dismiss', e)}
                        >
                          <List.Item arrow="horizontal"><span className="mustInput">*</span>选择地区</List.Item>
                        </Picker>
                        <TextareaItem
                            {...getFieldProps('address',{
                              initialValue:customerInfo.address || '',
                              rules: [
                                {required: true, message:'请填写详细地址'}
                              ]
                            })}
                            rows={3}
                            placeholder="请填写详细地址"
                         />
                        <InputItem
                          {...getFieldProps('schoolName',{
                            initialValue:customerInfo.schoolName || '',
                            rules: [
                              {required: true, message:'请输入学校名称'}
                            ]
                          })}
                          placeholder="请输入学校名称"
                        ><span className="mustInput">*</span>学校名称</InputItem>
                        <InputItem
                          {...getFieldProps('schoolNumber',{
                            initialValue:customerInfo.schoolNumber || '',
                            rules: [
                              {required: true, message:'请输入学号'}
                            ]
                          })}
                          placeholder="请输入学号"
                        ><span className="mustInput">*</span>学号</InputItem>
                    </List>
                    <WhiteSpace size="sm" />

                    <List className="to-order-info">
                        <InputItem
                          {...getFieldProps('connectRealName',{
                            initialValue:customerInfo.connectRealName || '',
                            rules: [
                              {required: true, message:'请输入紧急联系人姓名'}
                            ]
                          })}
                          placeholder="请输入紧急联系人姓名"
                        ><span className="mustInput">*</span>紧急联系人</InputItem>
                        <InputItem
                          {...getFieldProps('connectPhone',{
                            initialValue:customerInfo.connectPhone || '',
                            rules: [
                              {required: true, message:'请输入紧急联系人电话'},
                              {
                                validator: this.checkConnectPhone,
                              },
                            ]
                          })}
                          maxLength={11}
                          placeholder="请输入紧急联系人电话"
                        ><span className="mustInput">*</span>联系人电话</InputItem>
                        <TextareaItem
                            {...getFieldProps('remark',{
                              initialValue:customerInfo.remark || '',
                            })}
                            rows={3}
                            placeholder="输入订单备注"
                         />
                    </List>
                    <WhiteSpace size="sm" style={{marginBottom:'1.15rem'}} />

                </div>
            </Container>
        );
    }
}


export default createForm()(InputCustomerInfo);