import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request } from '../../until'
import { RefreshControl, ListView, Button, Flex, WhiteSpace, Toast } from 'antd-mobile';
import $ from 'zepto';
import Data from  "./data.js"

export default class ProductList extends Component {
    constructor(props) {
      super(props);
      this.state = {
        dataList:Data.getDataSouce(),
      };
    }
    componentDidMount() {
    }
    linkToDetail = (rowData) => {
      this.props.history.push({
        pathname: '/detail/' + rowData.productId,
      });
    }
    renderListView() {
      const { dataList } = this.state;
      return dataList.map((rowData, rowID) => {
        var price = (rowData.priceList[2].price / rowData.priceList[2].month).toFixed(2);
         return (
            <div className="normal-list-row" key={rowID}>
              <div className="normal-list" onClick={this.linkToDetail.bind(this, rowData)}>
                <div className="pro-img">
                  <img src={require(`./image/${rowData.productId}/p_list.jpg`)}  />
                </div>
                <div className="product-info-box">
                  <div>
                      <div className="product-name">{`${rowData.productName} ${rowData.configure}`}</div>
                      <div className="product-price-m">￥{price}/月</div>
                      <div className="gray-pro-info">
                        <span className="primary">优惠劵满{rowData.greaterUseCoupon}送{rowData.couponAmount}</span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          );
      })
    }
    render() {
        return (
            <Container>
              <div className="atl-product-list">
                {this.renderListView()}
                <div className="link-to-wap-site" onClick={() => { window.location.href = 'http://m.lxzl.com.cn' }}>
                  <div className="link-to-wap-site-main">
                    <div className="logo">
                      <img src={require('./image/logo.png')} />
                    </div>
                    <div className="link-des">
                      <div className="btn">更多商品 <img src={require('./image/arrow.png')} /></div>
                    </div>
                  </div>
                </div>
              </div>
            </Container>
        );
    }
}