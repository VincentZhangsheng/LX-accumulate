const dataSource = [
	{
		productId:100001,
		productName:'LBPC经典学习机',
		configure:'i3 7代/4G/120G SSD/集显/21.5英寸',
		configureImages:['configure.png'],
		subtitle:'',
		productDesc:"",
		productDescImg:['100001_01.jpg','100001_02.jpg','100001_03.jpg','100001_04.jpg','100001_05.jpg','100001_06.jpg'],
		priceList:[
			{
				month:1,
				monthDes:'1个月',
				price:125,
			},
			{
				month:3,
				monthDes:'3个月',
				price:335,
			},
			{
				month:5,
				monthDes:'包学期',
				price:530,
			}
		],
		greaterUseCoupon:200,
		couponAmount:100,
		cost:2800,
	},
	{
		productId:100002,
		productName:'LBPC娱乐电竞机',
		configure:'i5 7代/8G/1T+120 SSD/GTX1050 TI 4G/24英寸',
		configureImages:['configure.png'],
		subtitle:'',
		productDesc:"",
		productDescImg:['100002_01.jpg','100002_02.jpg','100002_03.jpg','100002_04.jpg','100002_05.jpg','100002_06.jpg'],
		priceList:[
			{
				month:1,
				monthDes:'1个月',
				price:270,
			},
			{
				month:3,
				monthDes:'3个月',
				price:720,
			},
			{
				month:5,
				monthDes:'包学期',
				price:1140,
			}
		],
		greaterUseCoupon:300,
		couponAmount:200,
		cost:5500,
	},
	{
		productId:100003,
		productName:'LBPC电竞发烧机',
		configure:'i5 7代/16G/1T+120G SSD/GTX1060 6G/28英寸',
		configureImages:['configure.png'],
		subtitle:'',
		productDesc:"",
		productDescImg:['100003_01.jpg','100003_02.jpg','100003_03.jpg','100003_04.jpg','100003_05.jpg','100003_06.jpg','100003_07.jpg','100003_08.jpg'],
		priceList:[
			{
				month:1,
				monthDes:'1个月',
				price:400,
			},
			{
				month:3,
				monthDes:'3个月',
				price:1080,
			},
			{
				month:5,
				monthDes:'包学期',
				price:1700,
			}
		],
		greaterUseCoupon:500,
		couponAmount:300,
		cost:8200,
	}
];

function getDataSouce() {
	return dataSource;
}

function getProductByID(id) {
	var datamap = dataSource.reduce(function(pre, item, index) {
		pre[item.productId] = item;
		return pre;
	}, {});
	return datamap[id];
}

export default {
	getDataSouce,
	getProductByID,
}