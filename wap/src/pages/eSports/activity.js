import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "./activity.less"

class Activity extends Component {
  componentWillMount() {
  }
  render() {
    const  imgs = [
      {img:'new1_01.png', url:'#'},
      {img:'new1_02.png', url:'/eSports#/list'},
      {img:'new1_03.jpg', url:'#'},
      {img:'new1_04.jpg', url:'#'}
    ];
    return (
       <div className="activity-wapper">
        {
          imgs.map((item, index) => {
            return <a href={`${item.url}`} style={{display:'block'}}><img src={require(`./image/activity/${item.img}`)} key={index} /></a>
          })
        }
       </div>
    );
  }
}

ReactDOM.render(<Activity />, document.getElementById('wrapper'));
