import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Request, PageUrl, WXJSSDK, AreaDictionary, DateHelp, Common } from '../../until'
import { Button, Flex, Carousel, Stepper, List, WhiteSpace, WingBlank, Toast, Modal, Icon, TextareaItem, Picker, DatePicker, InputItem } from 'antd-mobile';
import $ from 'zepto';
import classnames from 'classnames';
import Data from  "./data.js"
import Enum from './enum.js';

const Item = List.Item;
const alert = Modal.alert;

export default class ConfirmOrder extends Component {
    constructor(props) {
      super(props);
      this.state = {
        no:this.props.match.params.no,
        animating:false,
        product:{},
        order:null,
        codeUrl:`${Common.base}user/loginForActive?orderNo=${this.props.match.params.no}`,
      };
    } 
    componentWillMount() {
    }
    componentDidMount() {
      this.loadOrder();
    } 
    loadOrder = () => {
      const { no } = this.state;
      if(!no) {
        Toast.fail('没找到订单编号', 3);  
        return;
      }
      this.setState({animating:true});
      Request.ajax('activity/findByOrderNumber',{orderNumber:no},(response) => {
        this.setState({animating:false});
        if(response.success) {
          this.setState({
            order:response.resultMap.data,
            product:Data.getProductByID(response.resultMap.data.productId)
          })
        } else {
          Toast.fail('加载订单信息', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载订单信息', 3);
      })
    }
    payOrder = () => {
      const { no, order } = this.state;
      if(!(no && order)) {
         Toast.fail('找不到需要支付订单信息', 3);  
         return;
      }
      this.setState({animating:true});
      WXJSSDK.createESportsPayOrder(no, () => {
        this.setState({animating:false});
      }, (res) => {
         this.props.history.push({
            pathname: '/success',
         });
      });
    }
    render() {
        const { animating, areaDictionary, order, product }  = this.state;
        return (
            <Container animating={this.state.animating}>
            {
              !!order &&
               <div>
                    {
                     (order.status == Enum.orderStatus.num.unPay || order.status == Enum.orderStatus.num.paying || order.status == Enum.orderStatus.num.payFial) && 
                     <div className="prod-handle">
                        <div className="need-pay">
                          <span className="lbl">应付租金：</span>
                          <span className="price"><em>￥</em>{order.hasOwnProperty('totalOrderAmount') && order.totalOrderAmount.toFixed(2)}</span>
                        </div>
                        <Button type="primary" className="btn" onClick={this.payOrder} loading={false} disabled={false}>{false ? "处理中..." : "立即支付"}</Button>
                      </div>
                    }

                    <WhiteSpace size="sm" />

                    <List className="to-order-info">
                      <Item
                        thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                        // arrow="horizontal"
                        multipleLine
                        platform="android"
                        wrap={true}
                      >
                        {order.customerName} {order.phone}
                        <div className="alt-list-brief">{order.deliveryAddress}</div>
                      </Item>
                    </List>
                    <WhiteSpace size="sm" />

                    <div className="rental-product-list">
                      <div className="rental-product">
                        <div className="rental-product-img">
                          {product.productId && <img src={require(`./image/${product.productId}/p_confirm.jpg`)}  />}
                        </div>
                        <div className="rental-product-info">
                          <p className="product-name">{product.productName}</p>
                          <p className="product-configure">{product.configure}</p>
                          <p className="product-info"><span className="unit">数量: 1</span><span className="color">颜色: 黑</span></p>
                        </div>
                      </div>
                    </div>
                    <WhiteSpace size="sm" />

                    <List className="to-order-info">
                      <Item extra={order.hasOwnProperty('createTime') && DateHelp.foramtDate(new Date(order.createTime), 'yyyy-MM-dd')}><div className="lbl">下单时间</div></Item>      
                      <Item extra={`凌雄配送`}><div className="lbl">配送方式</div></Item>      
                      <Item extra={order.hasOwnProperty('rentStartTime') && DateHelp.foramtDate(new Date(order.rentStartTime), 'yyyy-MM-dd')}><div className="lbl">配送日期</div></Item>      
                    </List>
                    <WhiteSpace size="sm" />
                    
                    <List className="to-order-info" style={{marginBottom:'1.16rem'}}>
                      <Item extra={order.hasOwnProperty('status') && Enum.orderStatus.getValue(order.status)}><div className="lbl">订单状态</div></Item>    
                      <Item extra={<span><img src={require('./image/weixin.png')} style={{height:'0.32rem',width:'0.32rem', marginRight:15}} />微信支付</span>}><div className="lbl">支付方式</div></Item>    
                      <Item extra={`￥${order.hasOwnProperty('monthRent') && order.monthRent.toFixed(2)}`}><div className="lbl">租金金额</div></Item>      
                      <Item extra={`￥0.00`}><div className="lbl">押金</div></Item>      
                      <Item extra={`-￥${order.hasOwnProperty('monthRent') && order.hasOwnProperty('totalOrderAmount') && (order.monthRent - order.totalOrderAmount).toFixed(2)}`}><div className="lbl" style={{color:"#f23d3d"}}>优惠劵</div></Item>      
                      <Item extra={`￥${order.hasOwnProperty('totalOrderAmount') && order.totalOrderAmount.toFixed(2)}`}><div className="lbl" style={{color:"#f23d3d"}}>应付金额</div></Item>      
                    </List>
                    <WhiteSpace size="sm" />

                </div>
            }
            </Container>
        );
    }
}