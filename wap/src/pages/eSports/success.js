import React, { Component } from 'react';
import { Enum, Request, WXJSSDK, Rental, PageUrl } from '../../until'
import { Result, Icon, WhiteSpace, WingBlank, Button} from 'antd-mobile';
import '../../style/result.less'

export default class ResultSuccess extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    viewOrder = () => {
        this.props.history.push({
            pathname: '/list',
         });
    }
    render() {
        return (
            <div className="atl-result">
                <WhiteSpace size="lg" />
                <Result
                    img={<Icon type="check-circle" className="icon" style={{ fill: '#1F90E6' }} />}
                    title="支付成功"
                    message="您已经成功支付，如有到账延迟问题请联系凌雄客服"
                  />
                <WhiteSpace size="lg" />
                <WingBlank>
                   <Button type="primary" onClick={this.viewOrder} >返回</Button>
                 </WingBlank>
                 <WhiteSpace size="lg" />
            </div>
        );
    }
}