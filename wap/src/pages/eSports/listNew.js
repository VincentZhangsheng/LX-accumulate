import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request } from '../../until'
import { RefreshControl, ListView, Button, Flex, WhiteSpace, Toast } from 'antd-mobile';
import $ from 'zepto';
import Data from  "./data.js"

export default class ProductList extends Component {
    constructor(props) {
      super(props);
      this.state = {
        dataList:Data.getDataSouce(),
        showGuide:false,
      };
    }
    componentDidMount() {
    }
    linkToDetail = (rowData) => {
      this.props.history.push({
        pathname: '/detail/' + rowData.productId,
      });
    }
    renderListView() {
      const { dataList } = this.state;
      return dataList.map((rowData, rowID) => {
        var price = (rowData.priceList[2].price / rowData.priceList[2].month).toFixed(2);
         return (
           <div key={rowID} className="img-container" onClick={this.linkToDetail.bind(this, rowData)}>
              <img src={require(`./image/${rowData.productId}/p_list_new.png`)}  />
           </div>
          );
      })
    }
    render() {
      const { showGuide } = this.state;
        return (
            <div className="esport-wapper">
              {
                showGuide &&
                <div className="guide">
                  <div className="guide-content">
                    <span className="close" onClick={ () => { this.setState({showGuide:false}) }}><img src={require('./image/close.png')} /></span>
                    <img src={require('./image/guide.jpg')} />
                  </div>
                </div>
              }
              <div className="img-container"><img src={require('./image/list/01.png')} /></div>
              <div className="des-handle-container"><img src={require('./image/list/02.png')}  onClick={ () => { this.setState({showGuide:true}) }} /></div>
               {this.renderListView()}
              <div className="img-container"><img src={require('./image/list/04.png')} /></div>
              <div className="esport-footer">
                <div className="img-container"><img src={require('./image/list/07.png')} onClick={() => { window.location.href = 'http://m.lxzl.com.cn' }} /></div>                
              </div>
            </div>
        );
    }
}