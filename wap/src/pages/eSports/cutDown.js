import React, { Component } from 'react';
import { Button } from 'antd-mobile';
import $ from 'zepto';

export default class CutDownBtn extends Component {
    constructor(props) {
      super(props);
      this.state = {
        disabledSendVerifyCodeButton:false,
        cutNumText:'获取验证码',
      };
    } 
    sendVerifyCode = () => {
        this.props.sendVerifyCode && this.props.sendVerifyCode();
    }
    btnStatus = (isDisabled) => {
      this.setState({
        disabledSendVerifyCodeButton:isDisabled,
      });
    }
    cutDown = () => {
      var cutNum = 60, timer = setInterval(() => {
            cutNum -= 1;
            this.setState({
              cutNumText:cutNum + '秒后可重发',
              disabledSendVerifyCodeButton:true,
            });

            if(cutNum == 0) {
              clearInterval(timer);
              this.setState({
                cutNumText:'获取验证码',
                disabledSendVerifyCodeButton:false,
              })
            }
        }, 1000);
    }
    render() {
        const { disabledSendVerifyCodeButton, cutNumText }  = this.state;
        return (
           <Button size="small" type="ghost" disabled={disabledSendVerifyCodeButton} onClick={this.sendVerifyCode} style={{height:'auto',lineHeight:'inherit',border:'none', backgroundColor:"#fff", fontSize:'0.34rem'}}>{cutNumText}</Button>
        );
    }
}