const area = [
	{
		"label": "上海市",
		"value": 300009
	},
	{
		"label": "广东省",
		"value": 300019,
		"children": [{
			"label": "广州市",
			"value": 300263
		}, {
			"label": "深圳市",
			"value": 300265
		},{
			"label": "珠海市",
			"value": 300266
		}],
	},
	{
		"label": "湖北省",
		"value": 300017,
		"children": [{
			"label": "武汉市",
			"value": 300235
		}],
	},
	{
		"label": "江苏省",
		"value": 300010,
		"children": [{
			"label": "南京市",
			"value": 300140
		}],
	},
	{
		"label": "福建省",
		"value": 300013,
		"children": [{
			"label": "厦门市",
			"value": 300182
		}],
	},
]

export default area;