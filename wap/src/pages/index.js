export { default as ExploreHome } from "./explore/home.js";

//商品
export { default as ProductList } from "./product/list.js";
export { default as ProductDetail } from "./product/detail.js";
export { default as Desimg } from "./product/desimg.js";

//个人中心
export { default as AccountHome } from "./account/home.js";

export { default as AccountIndentity } from "./account/identity.js";
export { default as AccountViewIndentity } from "./account/viewIdentity.js";

//地址
export { default as AccountAddress } from "./account/address.js";
export { default as AccountEditAddress } from "./account/editAddress.js";
export { default as ChooseAddress } from "./account/chooseAddrss.js";

export { default as ResultSuccess } from "./result/success.js"
export { default as ResultFail } from "./result/fail.js"