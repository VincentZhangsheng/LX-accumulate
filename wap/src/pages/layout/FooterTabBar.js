import React, { Component } from 'react';
import { TabBar, Icon } from 'antd-mobile';
import { Enum, PageUrl } from '../../until'

export default class FooterTabBar extends Component {
    constructor(props) {
      super(props);
      this.state = {
        selectedTab: this.props.chooseTab,
        hidden: false,
      };
    }
    onPress(linkTo) {
      PageUrl.forward(linkTo);
      //this.props.history.push(linkTo)
    }
    render() {
      return (
        <TabBar
          unselectedTintColor="#949494"
          tintColor="#33A3F4"
          barTintColor="white"
          hidden={this.state.hidden}
        >
          <TabBar.Item
            title="探索"
            key="探索"
            icon={
              <Icon type={require('../../svg/explore.svg')} size="md" />
            }
            selectedIcon={
              <Icon type={require('../../svg/explore-selected.svg')} size="md" />
            }
            selected={this.state.selectedTab === Enum.navTab.explore}
            onPress={this.onPress.bind(this, PageUrl.explore)}
          />
  
          <TabBar.Item
            icon={
              <Icon type={require('../../svg/computer.svg')} size="md" />
            }
            selectedIcon={
              <Icon type={require('../../svg/computer-selected.svg')} size="md" />
            }
            title="优选"
            key="优选"
            selected={this.state.selectedTab === Enum.navTab.product}
            onPress={this.onPress.bind(this, PageUrl.product)}
          />
           
          <TabBar.Item
            icon={
              <Icon type={require('../../svg/help.svg')} size="md" />
            }
            selectedIcon={
              <Icon type={require('../../svg/help-selected.svg')} size="md" />
            }
            title="帮助"
            key="帮助"
            selected={this.state.selectedTab === Enum.navTab.help}
            onPress={this.onPress.bind(this, PageUrl.help)}
          />
          <TabBar.Item
            icon={
              <Icon type={require('../../svg/account.svg')} size="md" />
            }
            selectedIcon={
              <Icon type={require('../../svg/account-selected.svg')} size="md" />
            }
            title="账户"
            key="账户"
            selected={this.state.selectedTab === Enum.navTab.account}
            onPress={this.onPress.bind(this, PageUrl.account)}
          />
        </TabBar>
      );
    }
}