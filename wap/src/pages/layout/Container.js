import React, { Component } from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import { WXJSSDK } from '../../until'
import { ActivityIndicator } from 'antd-mobile';
import "../../style/app.less"

export default class Container extends Component {
    constructor(props) {
      super(props);
      this.state = {
        animating:false,
      };
    }
    componentWillMount() {
      this.props.initWeChatConfig && WXJSSDK.getWeChatConfig();
    }
    componentWillReceiveProps(nextProps) {
      if(nextProps.hasOwnProperty('animating') && nextProps.animating != this.state.animating) {
        this.setState({animating:nextProps.animating});
      }
    }
    render() {
      let { children } = this.props;
      return (
          <CSSTransitionGroup
            transitionName="view-transition-rfl" 
            //transitionAppear={true}
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}>
              <div className="toast-example">
                <ActivityIndicator
                  toast
                  text="加载中..."
                  animating={this.state.animating}
                />
              </div>
              {children}
          </CSSTransitionGroup>
      );
    }
}