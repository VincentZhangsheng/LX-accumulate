import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom";

import ProductList from "./list.js"
import ProductDetail from "./detail.js"
import DesImg from "./desimg.js"
import ChooseAddress from "./chooseAddress.js"

import "../../style/product.less"

class App extends Component {
  componentWillMount() {
  }
  render() {
    return (
       <Router>
         <div>
           <Route exact path="/" component={ProductList} />
           <Route path="/detail/:id" component={ProductDetail} />
           <Route path="/desimg" component={DesImg} />
           <Route path="/chooseAddress" component={ChooseAddress} />
         </div>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('wrapper'));
