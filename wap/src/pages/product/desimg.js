import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Request, Rental } from '../../until'
import { Toast } from 'antd-mobile';

export default class DesImg extends Component {
    constructor(props) {
      super(props);
      this.state = {
        animating:false,
        data:{},
        id:null,
      };
    }
    componentDidMount() {
      let { state, search } = this.props.history.location;
      if(state && state.data) {
        this.setState({
          data:state.data
        })
      } else {
        var id = Rental.getUrlPara(search,'id');
        this.loadData(id);
      }
    }
    loadData(id) {
      this.setState({animating:true});
      Request.ajax('product/queryProductById',{productId:id},(response) => {
        if(response.success) {
          this.setState({
            data: response.resultMap.data,
            animating:false,
          });
        } else {
          this.setState({animating:false});
          Toast.fail('加载商品信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载商品信息失败', 3);
      })
    }
    backFunc() {
      let { state } = this.props.history.location;
      let lstate = this.state.data;
      if(state && state.data) {
        lstate = state;
      }
      this.props.history.replace({
          pathname: '/detail/' + this.state.data.productId,
          state:lstate,
      });
    }
    render() {
      let { animating, data } = this.state;
      return (
          <Container animating={animating}>
              <Header history={this.props.history} title="商品介绍" backFunc={this.backFunc.bind(this)}  />
              <div className="alt-des-imgs">
                {
                  data.hasOwnProperty('productDescImgList') &&
                  data.productDescImgList.map((item,i) => {
                    return (
                      <img key={i} src={item.imgDomain + item.imgUrl} width="100%" />
                    )
                  })
                }
              </div>
          </Container>
      );
    }
}