import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request } from '../../until'
import { RefreshControl, ListView, Button, Flex, WhiteSpace, Toast } from 'antd-mobile';
import $ from 'zepto';

export default class ProductList extends Component {
    constructor(props) {
      super(props);
      const dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.state = {
        searchData:{
          pageNo:1,
          pageSize:8,
        },
        dataSource,
        dataList:[],
        refreshing: true,
        hasMore:true,
        height: document.documentElement.clientHeight,
      };
    }
    linkToDetail(rowData) {
      this.props.history.push({
        pathname: '/detail/' + rowData.productId,
      });
    }
    componentDidMount() {
      this.lv && setTimeout(() => this.setState({
        height: this.state.height - ReactDOM.findDOMNode(this.lv).offsetTop,
      }), 0);
      this.loadData();
    }
    loadData(pageNo) {
      let { searchData, hasMore, dataList } = this.state;
      searchData.pageNo = pageNo || 1;
       this.setState({ isLoading: true });
      Request.ajax('product/queryAllProduct',searchData,(response) => {
        var data = [];
        if(response.success) {
          data = response.resultMap.data.itemList;
          hasMore = searchData.pageNo < response.resultMap.data.pageCount;
          if(searchData.pageNo == 1) {
            dataList = data;
          } else {
            dataList = dataList.concat(data);  
          }
        } else {
          hasMore = false;
          Toast.fail('加载商品信息失败', 3);
        }
        this.setState({
          dataList:dataList,
          dataSource: this.state.dataSource.cloneWithRows(dataList),
          refreshing: false,
          hasMore:hasMore,
          showFinishTxt: true,
          searchData:searchData,
          isLoading:false,
        });
      },(error) => {
        Toast.fail('加载商品信息失败', 3);
      })
    }
    onScroll = (e) => {
      this.scrollerTop = e.scroller.getValues().top;
      this.domScroller = e;
    }
    /*onRefresh = () => {
      if (!this.manuallyRefresh) {
        this.setState({ refreshing: true });
      } else {
        this.manuallyRefresh = false;
      }
      this.loadData();
    }*/
    onEndReached = (event) => {
      if (this.state.isLoading || !this.state.hasMore) {
        return;
      }
      this.setState({ isLoading: true });
      this.loadData(this.state.searchData.pageNo + 1);
    };
    scrollingComplete = () => {
      if (this.scrollerTop >= -1) {
        this.setState({ showFinishTxt: false });
      }
    }
    renderCustomIcon() {
      return [
        <div key="0" className="am-refresh-control-pull">
          <span>{this.state.showFinishTxt ? '刷新完毕' : '下拉可以刷新'}</span>
        </div>,
        <div key="1" className="am-refresh-control-release">
          <span>松开立即刷新</span>
        </div>,
      ];
    }
    renderListView() {
      const separator = (sectionID, rowID) => (
        <div
          key={sectionID+"-"+rowID}
          style={{
            backgroundColor: '#F5F5F9',
            height: "0.1rem",
            borderTop: '1px solid #ECECED',
            borderBottom: '1px solid #ECECED',
          }}
        />
      );
      const row = (rowData, sectionID, rowID) => {
          var mainImg = '';
          if(rowData.hasOwnProperty('productImgList')) {
            mainImg = rowData.productImgList[0].imgDomain+rowData.productImgList[0].imgUrl;
          }
          return (
            <div className="normal-list-row">
              <div className="normal-list" key={rowID} onClick={this.linkToDetail.bind(this,rowData)}>
                <div className="pro-img">
                  <img src={mainImg}  />
                </div>
                <div className="product-info-box">
                  <div>
                      <div className="product-name">{rowData.productName}</div>
                      <div className="product-price-m">￥{rowData.listPrice.toFixed(2)}/月</div>
                      <div className="gray-pro-info">
                        <span>全新设备</span>
                        <span className="primary">随租随还</span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          );
      };
      return (
        <ListView
            className="atl-list-view atl-product-list"
            ref={el => this.lv = el}
            dataSource={this.state.dataSource}
            //renderHeader={() => <span>Pull to refresh</span>}
            renderFooter={() => (<div style={{ padding: "0.3rem", textAlign:"center" }}>{this.state.isLoading ? "加载中..." : ""}</div>)}
            renderRow={row}
            //renderSeparator={separator}
            initialListSize={8}
            pageSize={8}
            style={{
              height: this.state.height,
            }}
            scrollerOptions={{ scrollbars: true, scrollingComplete: this.scrollingComplete }}
            /*refreshControl={<RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
              icon={this.renderCustomIcon()}
            />}*/
            //onScroll={this.onScroll}
            scrollRenderAheadDistance={200}
            scrollEventThrottle={20}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={10}
          />
      )
    }
    render() {
        return (
            <Container>
                {this.renderListView()}
                <FooterTabBar history={this.props.history} chooseTab={Enum.navTab.product} />
            </Container>
        );
    }
}