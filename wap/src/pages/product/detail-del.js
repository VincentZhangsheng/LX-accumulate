import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request, SKU, PageUrl, WXJSSDK } from '../../until'
import { Button, Flex, Carousel, Stepper, List, WhiteSpace, WingBlank, Toast, Modal, Icon, TextareaItem } from 'antd-mobile';
import $ from 'zepto';
import classnames from 'classnames';

const Item = List.Item;
const alert = Modal.alert;

export default class ProductDetail extends Component {
    constructor(props) {
      super(props);

      this.state = {
        id:this.props.match.params.id,
        data:null,
        choosedPerporty:[],
        sku:null,
        totalInfo:null,
        minRentTimeLength:6,
        rentTimeLength:6,
        payMode:Enum.payMode.num.bymonth,
        address:null, 
        buyerRemark:"",
        animating:false,
        isCreateOrder:false,
      };
    }
    linkToOrderConfirm(orderId) {
      this.props.history.push('/product/orderConfirm');
    }
    componentDidMount() {
      this.initData();
    }
    initData() {
      let lstate = this.props.history.location.state,
          hasData = !!lstate && lstate.hasOwnProperty('data');
      if(!!lstate) {
        lstate = $.extend(this.state,lstate);
        this.setState(lstate);
      }
      if(!hasData) {
        this.loadData();
      }
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('product/queryProductById',{productId:this.state.id},(response) => {
        if(response.success) {
          this.setState({
            data: response.resultMap.data,
            totalInfo:SKU.getProductInfo(response.resultMap.data.productSkuList),
            animating:false,
          });
        } else {
          this.setState({animating:false});
          Toast.fail('加载商品信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载商品信息失败', 3);
      })
    }
    chooseProperty(categoryProperty, property,event) {
      let { choosedPerporty } = this.state;
      let hasSamep = choosedPerporty.some((item,i) => {
          return item.propertyId == property.propertyId;
      });
      if(!hasSamep) {
        choosedPerporty.push(property);
      }
      if(!this.isPropertyChoosed(property) && hasSamep) {
        let notsmae = choosedPerporty.filter((item,i) => {
            return item.propertyId != property.propertyId;
        });  
        notsmae.push(property);
        choosedPerporty = notsmae;
      }

      this.setSKU(choosedPerporty);
    }
    isPropertyChoosed(property) {
      return this.state.choosedPerporty.some((item,i) => {
          return item.categoryPropertyValueId == property.categoryPropertyValueId;
      });
    }
    setSKU(choosedPerporty) {
      let { sku } = this.state;
      sku = SKU.getSKU(choosedPerporty,this.state.data.productSkuList);
      this.setState({
        choosedPerporty:choosedPerporty,
        sku:sku
      });
    }
    getSalePrice(min,max) {
      if(!min || !max) return;
      if(min == max) {
        return '￥' + min.toFixed(2)
      }
      return ("￥{0}-￥{1}").format(min.toFixed(2),max.toFixed(2))
    }
    rentTimeLengthChange(val) {
      this.setState({
        rentTimeLength:val,
      })
    }
    changePayMode(val) {
      this.setState({
        payMode:val
      });
    }
    renderPayMode() {
      var arry = new Array();
      for (var num in Enum.payMode.num) {
        let n = Enum.payMode.num[num];
        let className = classnames({
          box:true,
          active:n == this.state.payMode
        })
        arry.push(<div key={n} className="item"><div className={className} onClick={this.changePayMode.bind(this,n)}>{Enum.payMode.getValue(n)}</div></div>)
      }
      return arry;
    }
    getPayPrice(price) {
      let { payMode, rentTimeLength } = this.state;
      switch(payMode) {
          case Enum.payMode.num.payall:
            return (parseFloat(price)*parseFloat(rentTimeLength)).toFixed(2);
          case Enum.payMode.num.bymonth:
            return price.toFixed(2);
        }
        return price.toFixed(2);
    }
    chooseAddress() {
      this.props.history.push({
        pathname:'/chooseAddress',
        state:this.state,
        search:"id="+this.state.id,
      })
    }
    createOrder() {
      let { id, choosedPerporty, payMode, rentTimeLength, sku, buyerRemark, address, data } = this.state;

      if(choosedPerporty.length == 0) {
        alert('提示', '请选择配置', [{ text: '确定'}]);
        return;
      }

      if(choosedPerporty.length < data.productCategoryPropertyList.length) {
        alert('提示', '请选择配置', [{ text: '确定'}]);
        return; 
      }

      if(sku &&sku.stock == 0){
        alert('提示', '您所选配置暂时没库存了', [{ text: '确定'}]);
        return;
      }

      if(!address) {
        alert('提示', '请选择收货地址', [{ text: '取消'},{ text: '确定',onPress:() => {
          this.chooseAddress()
        }}]);
        return;
      }

      let orderProductList = new Array();

      let product = {
        productId:id,
        productCount:1,
        remark:"",
        skuPropertyValueList:new Array()
      }
      product.skuPropertyValueList = choosedPerporty.map((item,i) => {
        return {categoryPropertyValueId:item.categoryPropertyValueId};
      })

      orderProductList.push(product);

      var prams = {
        rentType:Enum.rentType.num.byMonth,
        rentTimeLength:rentTimeLength,
        payMode:payMode,
        logisticsAmount:0, //运费
        userConsignId:address.consignId, //送货地址
        buyerRemark:buyerRemark,
        orderProductList:orderProductList
      }

      this.setState({isCreateOrder:true});
      Request.ajax('order/createOrder',prams,(response) => {
        if(response.success) {
          PageUrl.forward(PageUrl.payOrder + '?id='+response.resultMap.data);
        } else {
          this.setState({isCreateOrder:false});
          alert('下单提示', response.description || "下单失败，请重试", [{ text: '确定'}]);
        }
      },(error) => {
          this.setState({isCreateOrder:false});
          alert('下单提示', "下单失败，请重试", [{ text: '确定'}]);
      });
    }
    linkToDesImg() {
      if(this.state.data.hasOwnProperty('productDescImgList')) {
        this.props.history.push({
          pathname:'/desimg',
          state:this.state,
          search:"id="+this.state.id,
        })  
      } else {
        alert('提示', "目前还没有商品介绍", [{ text: '确定'}]);
      }
    }
    previewImage() {
      let { data } = this.state;
      if(data.hasOwnProperty('productImgList')) {
        var imgArray = data.productImgList.map(item => {
          return item.imgDomain + item.imgUrl;
        })
        WXJSSDK.previewImage(imgArray[0], imgArray);  
      } else {
        alert('提示', "目前还没有商品图", [{ text: '确定'}]);
      }
    }
    render() {
        let { data, choosedPerporty, totalInfo, sku, minRentTimeLength, rentTimeLength, address, isCreateOrder }  = this.state;
        return (
            <Container animating={this.state.animating}>
              {
                data &&
                <div>
                    <Header history={this.props.history} title="商品详细" backLink={PageUrl.product} />
                    <div className="alt-product-detail-head">
                        <div className="title">{data.productName}</div>
                        <div className="des1">￥1.00元起租</div>
                        <div className="des2">在线选租，享受快速免费的送货服务。<br/>请先前往个人中心进行身份认证，方可免押金选择租机。</div>
                        {
                          data.hasOwnProperty('productImgList') && data.productImgList.length > 0 &&
                          <div className="product-main-img" onClick={this.previewImage.bind(this)}>
                            <img src={data.productImgList[0].imgDomain + data.productImgList[0].imgUrl} />
                          </div>
                        }
                        <WhiteSpace size="xs" />
                        <div className="alt-grid">
                          <div className="alt-grid-item" onClick={this.linkToDesImg.bind(this)}><span>商品介绍</span></div>
                          <div className="alt-grid-item" onClick={this.previewImage.bind(this)}><span>图库</span></div>
                        </div>
                    </div>
                    <WhiteSpace size="lg" />
                    {
                      (data.productCategoryPropertyList || []).map((item,i) => {
                        return (
                          <div key={item.categoryPropertyId}>
                            <div className="alt-product-detail-spec">
                              <div className="sub-title">{item.propertyName}</div>
                              {
                                item.hasOwnProperty('productCategoryPropertyValueList') &&
                                <Flex className="spec-items" wrap="wrap">
                                  {
                                    item.productCategoryPropertyValueList.map((property,i) => {
                                      let boxClass = classnames({
                                        box:true,
                                        active:this.isPropertyChoosed(property)
                                      })
                                      return (
                                        <div key={property.categoryPropertyValueId} className="item">
                                          <div className={boxClass} onClick={this.chooseProperty.bind(this,item,property)}>{property.propertyValueName}</div>
                                        </div>
                                      )
                                    })
                                  }
                                </Flex>
                              }
                            </div>
                            <WhiteSpace size="lg" />
                          </div>
                        )
                      })
                    }
                    <div className="alt-product-detail-spec">
                      <div className="sub-title">租金缴纳方式</div>
                      <Flex className="spec-items"  wrap="wrap">
                        {this.renderPayMode()}
                      </Flex>
                    </div>
                    <WhiteSpace size="lg" />
                    <List>
                      {
                        !address &&
                        <Item
                          thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                          arrow="horizontal"
                          platform="android"
                          extra="请选择"
                          onClick={this.chooseAddress.bind(this)}
                        >
                          收货地址             
                        </Item>
                      }
                      {
                        address &&
                        <Item
                          thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                          arrow="horizontal"
                          multipleLine
                          onClick={this.chooseAddress.bind(this)}
                          platform="android"
                          wrap={true}
                        >
                          {address.consigneeName} {address.consigneePhone}                
                          <div className="alt-list-brief">{address.provinceName}{address.cityName}{address.districtName}{address.address}</div>
                        </Item>
                      }
                    </List>
                    <WhiteSpace size="lg" />
                    <List className="my-list">
                      <Item extra={`${sku ? sku.stock : (totalInfo ? totalInfo.totalStock : 0)}件`}>库存</Item>
                      <Item extra={sku ? '￥' + sku.salePrice : (totalInfo ? this.getSalePrice(totalInfo.minPrice,totalInfo.maxPrice): '￥0.00')}>月租金</Item>
                      <Item extra={<Stepper style={{ width: '80%', minWidth: '2rem' }} showNumber defaultValue={rentTimeLength} min={minRentTimeLength} onChange={this.rentTimeLengthChange.bind(this)} />}>租期（月）</Item>
                      {
                        sku && 
                        <Item extra={'￥'+this.getPayPrice(sku.salePrice)}>需支付</Item>  
                      }
                      <TextareaItem
                        rows={3}
                        placeholder="输入备注"
                        value={this.state.buyerRemark}
                        onChange={v => this.setState({buyerRemark:v}) }
                     />
                    </List>
                    <WhiteSpace size="lg" />
                    <WingBlank>
                      <Button type="primary" onClick={this.createOrder.bind(this)} loading={isCreateOrder} disabled={isCreateOrder}>{isCreateOrder ? "正在下单..." : "立即下单"}</Button>
                    </WingBlank>
                    <WhiteSpace size="lg" />
                </div>
              }
            </Container>
        );
    }
}