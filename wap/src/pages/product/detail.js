import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request, SKU, PageUrl, WXJSSDK } from '../../until'
import { Button, Flex, Carousel, Stepper, List, WhiteSpace, WingBlank, Toast, Modal, Icon, TextareaItem } from 'antd-mobile';
import $ from 'zepto';
import classnames from 'classnames';

const Item = List.Item;
const alert = Modal.alert;

export default class ProductDetail extends Component {
    constructor(props) {
      super(props);

      this.state = {
        id:this.props.match.params.id,
        data:null,
        choosedPerporty:[],
        sku:null,
        totalInfo:null,
        minRentTimeLength:1,
        rentTimeLength:1,
        payMode:Enum.payMode.num.bymonth,
        address:null, 
        buyerRemark:"",
        animating:false,
        isCreateOrder:false,
        propertyPannel:false,
        chooseTab:"productDesImg"
      };
    }
    linkToOrderConfirm(orderId) {
      this.props.history.push('/product/orderConfirm');
    }
    componentDidMount() {
      this.initData();
    }
    initData() {
      let lstate = this.props.history.location.state,
          hasData = !!lstate && lstate.hasOwnProperty('data');
      if(!!lstate) {
        lstate = $.extend(this.state,lstate);
        this.setState(lstate);
      }
      if(!hasData) {
        this.loadData();
      }
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('product/queryProductById',{productId:this.state.id},(response) => {
        if(response.success) {
          /*this.setState({
            data: response.resultMap.data,
            totalInfo:SKU.getProductInfo(response.resultMap.data.productSkuList),
            animating:false,
          });*/
          this.setData(response.resultMap.data);
        } else {
          this.setState({animating:false});
          Toast.fail('加载商品信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载商品信息失败', 3);
      })
    }
    setData(data) {
        let choosedPerporty = data .hasOwnProperty('productCategoryPropertyList') ? data.productCategoryPropertyList.map((item) => {
          return item.hasOwnProperty('productCategoryPropertyValueList') && item.productCategoryPropertyValueList[0];
        }) : [];

        this.setState({
          data: data,
          totalInfo:SKU.getProductInfo(data.productSkuList),
          animating:false,
          choosedPerporty:choosedPerporty,
        });
        this.setSKU(choosedPerporty);
    }
    chooseProperty(categoryProperty, property,event) {
      let { choosedPerporty } = this.state;
      let hasSamep = choosedPerporty.some((item,i) => {
          return item.propertyId == property.propertyId;
      });
      if(!hasSamep) {
        choosedPerporty.push(property);
      }
      if(!this.isPropertyChoosed(property) && hasSamep) {
        let notsmae = choosedPerporty.filter((item,i) => {
            return item.propertyId != property.propertyId;
        });  
        notsmae.push(property);
        choosedPerporty = notsmae;
      }

      this.setSKU(choosedPerporty);
    }
    isPropertyChoosed(property) {
      return this.state.choosedPerporty.some((item,i) => {
          return item.categoryPropertyValueId == property.categoryPropertyValueId;
      });
    }
    setSKU(choosedPerporty) {
      let { sku } = this.state;
      sku = SKU.getSKU(choosedPerporty,this.state.data.productSkuList);
      this.setState({
        choosedPerporty:choosedPerporty,
        sku:sku
      });
    }
    getSalePrice(min,max) {
      /*if(!min || !max) return;
      if(min == max) {
        return '￥' + min.toFixed(2)
      }
      return ("￥{0}-￥{1}").format(min.toFixed(2),max.toFixed(2))*/
      if(!min || !max) return;
      if(min == max) {
        return min.toFixed(2)
      }
      return min.toFixed(2)+'-'+max.toFixed(2);
    }
    rentTimeLengthChange(val) {
      this.setState({
        rentTimeLength:val,
      })
    }
    changePayMode(val) {
      this.setState({
        payMode:val
      });
    }
    renderPayMode() {
      var arry = new Array();
      for (var num in Enum.payMode.num) {
        let n = Enum.payMode.num[num];
        let className = classnames({
          box:true,
          active:n == this.state.payMode
        })
        arry.push(<div key={n} className="item"><div className={className} onClick={this.changePayMode.bind(this,n)}>{Enum.payMode.getValue(n)}</div></div>)
      }
      return arry;
    }
    getPayPrice(price) {
      let { payMode, rentTimeLength } = this.state;
      switch(payMode) {
          case Enum.payMode.num.payall:
            return (parseFloat(price)*parseFloat(rentTimeLength)).toFixed(2);
          case Enum.payMode.num.bymonth:
            return price.toFixed(2);
        }
        return price.toFixed(2);
    }
    chooseAddress() {
      this.props.history.push({
        pathname:'/chooseAddress',
        state:this.state,
        search:"id="+this.state.id,
      })
    }
    createOrder() {
      let { id, choosedPerporty, payMode, rentTimeLength, sku, buyerRemark, address, data } = this.state;

      if(choosedPerporty.length == 0) {
        alert('提示', '请选择配置', [{ text: '确定'}]);
        return;
      }

      if(choosedPerporty.length < data.productCategoryPropertyList.length) {
        alert('提示', '请选择配置', [{ text: '确定'}]);
        return; 
      }

      if(sku &&sku.stock == 0){
        alert('提示', '您所选配置暂时没库存了', [{ text: '确定'}]);
        return;
      }

      if(!address) {
        alert('提示', '请选择收货地址', [{ text: '取消'},{ text: '确定',onPress:() => {
          this.chooseAddress()
        }}]);
        return;
      }

      let orderProductList = new Array();

      let product = {
        productId:id,
        productCount:1,
        remark:"",
        productSkuPropertyList:new Array()
      }
      product.productSkuPropertyList = choosedPerporty.map((item,i) => {
        return {propertyValueId:item.categoryPropertyValueId};
      })

      orderProductList.push(product);

      var prams = {
        rentType:Enum.rentType.num.byMonth,
        rentTimeLength:rentTimeLength,
        payMode:payMode,
        logisticsAmount:0, //运费
        userConsignId:address.consignId, //送货地址
        buyerRemark:buyerRemark,
        orderProductList:orderProductList
      }

      this.setState({isCreateOrder:true});
      Request.ajax('order/createOrder',prams,(response) => {
        if(response.success) {
          PageUrl.forward(PageUrl.payOrder + '?id='+response.resultMap.data);
        } else {
          this.setState({isCreateOrder:false});
          alert('下单提示', response.description || "下单失败，请重试", [{ text: '确定'}]);
        }
      },(error) => {
          this.setState({isCreateOrder:false});
          alert('下单提示', "下单失败，请重试", [{ text: '确定'}]);
      });
    }
    linkToDesImg() {
      if(this.state.data.hasOwnProperty('productDescImgList')) {
        this.props.history.push({
          pathname:'/desimg',
          state:this.state,
          search:"id="+this.state.id,
        })  
      } else {
        alert('提示', "目前还没有商品介绍", [{ text: '确定'}]);
      }
    }
    previewImage() {
      let { data } = this.state;
      if(data.hasOwnProperty('productImgList')) {
        var imgArray = data.productImgList.map(item => {
          return item.imgDomain + item.imgUrl;
        })
        WXJSSDK.previewImage(imgArray[0], imgArray);  
      } else {
        alert('提示', "目前还没有商品图", [{ text: '确定'}]);
      }
    }
    render() {
        let { data, choosedPerporty, totalInfo, sku, minRentTimeLength, rentTimeLength, address, isCreateOrder, payMode }  = this.state;
        let hasChoosedPerproty = choosedPerporty && choosedPerporty.length > 0;
        return (
            <Container animating={this.state.animating}>
              {
                data &&
                <div>
                    <Header history={this.props.history} title="商品详细" backLink={PageUrl.product} />
                    <div className="prod-handle">
                      <div className="need-pay">
                        <span className="lbl">需付：</span>
                        <span className="price"><em>￥</em>{sku ? this.getPayPrice(sku.salePrice) : '0.00'}</span>
                      </div>
                      {/*<div className="btn">立即下单</div>*/}
                      <Button type="primary" className="btn" onClick={this.createOrder.bind(this)} loading={isCreateOrder} disabled={isCreateOrder}>{isCreateOrder ? "正在下单..." : "立即下单"}</Button>
                    </div>
                    <div className="product-detail-head">
                      {
                        data.hasOwnProperty('productImgList') && data.productImgList.length > 0 &&
                        <div className="main-img" onClick={this.previewImage.bind(this)}>
                          <img src={data.productImgList[0].imgDomain + data.productImgList[0].imgUrl} />
                        </div>
                      }
                      <List>
                        <Item
                          multipleLine
                          wrap={true}
                          className="prod-info"
                        >
                          <div className="prod-name">{data.productName}</div>           
                          <div className="prod-price">
                            <div className="price-mon">
                              <span className="num"><em className="rmb">￥</em>{sku ? sku.salePrice : (totalInfo ? this.getSalePrice(totalInfo.minPrice,totalInfo.maxPrice): '0.00')}</span>/月
                            </div>
                            <div className="prod-tip">
                              <span>随租随还</span>
                            </div>
                          </div>
                        </Item>
                      </List>
                    </div>
                    <WhiteSpace size="sm" />
                    <List className="to-order-info">
                       <Item 
                          arrow="horizontal" 
                          onClick={() => { this.setState({propertyPannel:true}) }} 
                          extra={
                            <div>
                              {
                                hasChoosedPerproty && 
                                <div className="val">
                                  {choosedPerporty.map((item,i) => {
                                    return (
                                      <span key={i}>{item.propertyValueName}</span>
                                    )
                                  })}
                                  {/*<span>{Enum.payMode.getValue(payMode)}</span>*/}
                                </div>     
                              }
                              {
                                !hasChoosedPerproty &&
                                <div className="val">请选择配置</div>
                              }
                            </div>
                          }
                          className="perporties">
                          <div className="lbl">配置</div>
                          
                                  
                      </Item>
                      <Item extra={`${sku ? sku.stock : (totalInfo ? totalInfo.totalStock : 0)}件`}>
                          <div className="lbl">库存</div>            
                      </Item>
                      <Item extra={<div className="val" style={{float:'right'}}><Stepper style={{ width: '80%', minWidth: '2rem' }} showNumber defaultValue={rentTimeLength} min={minRentTimeLength} onChange={this.rentTimeLengthChange.bind(this)} /></div>}>
                          <div className="lbl">租期</div>
                      </Item>
                      <Item
                          arrow="horizontal" 
                          onClick={() => { this.setState({propertyPannel:true}) }}  
                          extra={Enum.payMode.getValue(payMode)}>
                          <div className="lbl">支付类型</div><div className="val"></div>             
                      </Item>

                    </List>
                    <WhiteSpace size="sm" />
                    <List className="to-order-info">
                      {
                        !address &&
                        <Item
                          thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                          arrow="horizontal"
                          //platform="android"
                          extra={'选择地址'}
                          onClick={this.chooseAddress.bind(this)}
                        >
                        <div className="lbl"> </div>
                        </Item>
                      }
                      {
                        address &&
                        <Item
                          thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                          arrow="horizontal"
                          multipleLine
                          onClick={this.chooseAddress.bind(this)}
                          platform="android"
                          wrap={true}
                        >
                          {address.consigneeName} {address.consigneePhone}                
                          <div className="alt-list-brief">{address.provinceName}{address.cityName}{address.districtName}{address.address}</div>
                        </Item>
                      }
                    </List>
                    <List className="to-order-info">
                      <TextareaItem
                        rows={3}
                        placeholder="输入订单备注"
                        value={this.state.buyerRemark}
                        onChange={v => this.setState({buyerRemark:v}) }
                     />
                    </List>
                    <WhiteSpace size="sm" />

                    <div className="prod-tabs">
                      <div className={this.state.chooseTab == 'productDesImg' ? "tab choose" : 'tab'} onClick={() => { this.setState({chooseTab:'productDesImg'}) }}>商品介绍</div>
                      <div className={this.state.chooseTab == 'productPropertyList' ? "tab choose" : 'tab'} onClick={() => { this.setState({chooseTab:'productPropertyList'}) }}>商品参数</div>
                    </div>
                    <WhiteSpace size="xs" />
                    {
                      this.state.chooseTab == 'productDesImg' && 
                      <div className="tabContent">
                        {
                          data.hasOwnProperty('productDescImgList') &&
                          data.productDescImgList.map((item,i) => {
                            return (
                              <img key={i} src={item.imgDomain + item.imgUrl} width="100%" />
                            )
                          })
                        }
                      </div>
                    }
                    {
                      this.state.chooseTab == 'productPropertyList' &&
                      <div className="tabContent">
                        <List className="to-order-info">
                          {
                            data.hasOwnProperty('productPropertyList') && data.productPropertyList.map(item => {
                              return (
                                <Item
                                  extra={item.propertyValueName}
                                >
                                <div className="lbl">{item.propertyName}</div>
                                </Item>      
                              )
                            })
                          }
                        </List>
                      </div>
                    }
                    <Modal
                      popup
                      visible={this.state.propertyPannel}
                      maskClosable={true}
                      animationType="slide-up"
                      className="choose-property-modal"
                      style={{height:'70%'}}
                      title="选择"
                      closable={true}
                      onClose={() => { this.setState({propertyPannel:false})}}
                      footer={[{ text: '确定', onPress: () => { this.setState({propertyPannel:false})} }]}
                    >
                    <div className="property-list">
                      <div className="alt-product-detail-spec">
                        <div className="sub-title">租金缴纳方式</div>
                        <Flex className="spec-items"  wrap="wrap">
                          {this.renderPayMode()}
                        </Flex>
                      </div>
                      {
                        (data.productCategoryPropertyList || []).map((item,i) => {
                          return (
                            <div key={item.categoryPropertyId}>
                              <div className="alt-product-detail-spec">
                                <div className="sub-title">{item.propertyName}</div>
                                {
                                  item.hasOwnProperty('productCategoryPropertyValueList') &&
                                  <Flex className="spec-items" wrap="wrap">
                                    {
                                      item.productCategoryPropertyValueList.map((property,i) => {
                                        let boxClass = classnames({
                                          box:true,
                                          active:this.isPropertyChoosed(property)
                                        })
                                        return (
                                          <div key={property.categoryPropertyValueId} className="item">
                                            <div className={boxClass} onClick={this.chooseProperty.bind(this,item,property)}>{property.propertyValueName}</div>
                                          </div>
                                        )
                                      })
                                    }
                                  </Flex>
                                }
                              </div>
                              <WhiteSpace size="lg" />
                            </div>
                          )
                        })
                      }
                      <WhiteSpace size="lg" />
                    </div>
                    </Modal>
                
                </div>
              }
            </Container>
        );
    }
}