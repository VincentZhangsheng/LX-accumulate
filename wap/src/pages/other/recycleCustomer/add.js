import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Request, PageUrl, User, Help } from '../../../until'
import { Modal, Toast } from 'antd-mobile';
import $ from 'zepto';
import './add.css'

//const alert = Modal.alert;

class AlertBox extends React.Component {   
    render() {
        return (
            <div className={ 'alertBox ' + this.props.isAlert }>
                <div className=" alertTips">
                    <div className="alert-title">
                        温馨提示
                        <span className="cancel" onClick={ () => this.props.onClick() }>
                            <span className="cancelBtn"></span>
                        </span>
                    </div>
                    <div className="alert-content">{this.props.message}</div>
                    <div className="alert-btn" onClick={ () => this.props.onClick() }>确定</div>
                </div>
            </div>
        );
    }
}

class RecycleCustomerAdd extends Component {
	constructor(props) {
      super(props);
      this.state = {
            corporateContactsName:null,
            corporateContactsPhone:null,
            corporateName: null,
            submitName: null,  
            submitPhone: null,
            submitAddress: null,
            //提示框
            message: '',
            isAlert: 'close', 
            //页面跳转标识  
            successRequest: false,
      };
    }
    componentWillMount() {
        //设置rem单位
        function setRem() {
            var html = document.querySelector("html");
            var clientWidth = document.body && document.body.clientWidth || document.getElementsByTagName('html')[0].offsetWidth;          
            //考虑pc打开，ipad会作为pc样子展示
            /*
            if (clientWidth > 600) {
	            clientWidth = 600;
	            html.style.width = clientWidth + "px";
            }
            */         
            html.style.fontSize = clientWidth / 10 + "px";            
        }
        setRem();
        window.addEventListener('resize', function () {
            setRem();
        }, false);
    }
    componentDidMount() {
        //console.log('componentDidMount');
    }  
    add(){
        let { corporateContactsName, corporateContactsPhone, corporateName, submitName, submitPhone, submitAddress } = this.state;
        if(!corporateContactsName) {
            this.setState({
                isAlert: 'open',
                message: '您的 企业联系人姓名 尚未填写！'
            });
            return;
    	};
    	if(!corporateContactsPhone) {
            this.setState({
                isAlert: 'open',
                message: '您的 企业联系人电话 尚未填写！'
            });
            return;
    	};
    	if(!Help.checkMobile(corporateContactsPhone)) {
            this.setState({
                isAlert: 'open',
                message: '请输入 企业联系人 的正确电话！'
            });
            return;
        };
        if(!corporateName) {
            this.setState({
                isAlert: 'open',
                message: '您的 企业公司名称 尚未填写！'
            });
            return;
        };
        if(!submitName) {
            this.setState({
                isAlert: 'open',
                message: '您的 姓名 尚未填写！'
            });
            return;
        };
        if(!submitPhone) {
            this.setState({
                isAlert: 'open',
                message: '您的 电话 尚未填写！'
            });
            return;
        };
        if(!Help.checkMobile(submitPhone)) {
            this.setState({
                isAlert: 'open',
                message: '请输入 您 的正确电话！'
            });
            return;
        };
        if(!submitAddress) {
            this.setState({
                isAlert: 'open',
                message: '您的 有效联系地址 尚未填写！'
            });
            return;
        };

        //获取url路径后接的参数channel
        function getChannel(){
            var hrefPar = window.location.href.split('?');
            var channel = '';
            var obj = {};
            if(hrefPar[1]){
                var param = hrefPar[1];
                var arr1 = param.split('&');
                for(var i=0; i<arr1.length; i++){
                    obj[arr1[i].split('=')[0]] = arr1[i].split('=')[1];
                }
            }
            channel = obj['channel'] ? obj['channel'] : '';
            return channel;
        };
        const channel = getChannel();
        const url = 'informationCollection/submitInformation'

        Request.ajax( url, {
            corporateContactsName: Help.clearSpace(corporateContactsName), 
            corporateContactsPhone: Help.clearSpace(corporateContactsPhone), 
            corporateName: Help.clearSpace(corporateName), 
            submitName: Help.clearSpace(submitName),
            submitPhone: Help.clearSpace(submitPhone),
            submitAddress: Help.clearSpace(submitAddress),
            channel: channel,
        }, (response) => {
            //console.log(response);
            if(response.success){
                this.setState({
                    isAlert: 'open',
                    message: '您已成功推荐，谢谢您的参与！',
                    //设置跳转标识
                    successRequest: true,
                });   
                
            }else{
                this.setState({
                    isAlert: 'open',
                    message: '提交失败，请重试！'
                });               
            }
        },(error) => {
            //console.log(error);           
            this.setState({
                isAlert: 'open',
                message: '请求发生错误，请检查！',
            }); 
      });
    }
    closeAlertBox(){        
        this.setState({
            isAlert: 'close',
            message: ''
        });
        //请求成功时, 跳转到 京东企业购-企业回收
        if(this.state.successRequest){
            window.location.href="https://pro.m.jd.com/mall/active/4PcK7MTjfd6yAgTkuVEjTeHiJGyo/index.html";
        }
    }
    render() {
    	return (
    		<div id="content">	
                <div id="act">
                    <div id="act-head">
                        <div className="head-txt">京东企业回收为企业客户提供一站式的物资回收场景解决方案。京东商务专员会致电联系您。推荐的公司联系人，请您提前将相关信息知会被推荐方。</div>
                    </div>
                    <div id="act-content">
                        <div id="act-info">
                            <div className="infoBox">
                                <div className="infoTitle">
                                    <span className="starIcon">*</span>企业联系人姓名：
                                </div>
                                <div className="infoInput">
                                    <input type="text" name="" placeholder="输入联系人姓名" onChange={(e) => { this.setState({corporateContactsName:$.trim($(e.target).val())}) }} />
                                </div>
                            </div>
                            <div className="infoBox">
                                <div className="infoTitle">
                                    <span className="starIcon">*</span>企业联系人电话：
                                </div>
                                <div className="infoInput">
                                    <input type="text" name="" placeholder="输入联系人电话" onChange={(e) => { this.setState({corporateContactsPhone:$.trim($(e.target).val())}) }} />
                                </div>
                            </div>
                            <div className="infoBox">
                                <div className="infoTitle">
                                    <span className="starIcon">*</span>企业公司名称：
                                </div>
                                <div className="infoInput">
                                    <input type="text" name="" placeholder="输入企业公司名称" onChange={(e) => { this.setState({corporateName:$.trim($(e.target).val())}) }} />
                                </div>
                            </div>
                            <div className="infoBox">
                                <div className="infoTitle">
                                    <span className="starIcon">*</span>您的姓名：
                                </div>
                                <div className="infoInput">
                                    <input type="text" name="" placeholder="输入您的姓名" onChange={(e) => { this.setState({submitName:$.trim($(e.target).val())}) }} />
                                </div>
                            </div>
                            <div className="infoBox">
                                <div className="infoTitle">
                                    <span className="starIcon">*</span>您的电话：
                                </div>
                                <div className="infoInput">
                                    <input type="text" name="" placeholder="输入您的电话" onChange={(e) => { this.setState({submitPhone:$.trim($(e.target).val())}) }} />
                                </div>
                            </div>
                            <div className="infoBox">
                                <div className="infoTitle">
                                    <span className="starIcon">*</span>您的有效联系地址：
                                </div>
                                <div className="infoInputArea">
                                    <textarea className="myTextArea" placeholder="输入您的有效联系地址" onChange={(e) => { this.setState({submitAddress:$.trim($(e.target).val())}) }}></textarea>
                                </div>
                            </div>
                            <div id="sureBtn">
                                <div className="sureBtn" onClick={this.add.bind(this)}></div>
                            </div>
                        </div>
                    </div>                    
                </div> 

                <AlertBox message={this.state.message} isAlert={this.state.isAlert} onClick={ () => this.closeAlertBox() }/>          
            </div>            
		)
    }
}

ReactDOM.render(<RecycleCustomerAdd />, document.getElementById('wrapper'));