import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Request, PageUrl, User, Help } from '../../../until'
import { Modal, Toast } from 'antd-mobile';
import $ from 'zepto';
import './add.less'

// const alert = Modal.alert;

class CustomerSurveyAdd extends Component {
	 constructor(props) {
      super(props);
      this.state = {
      	customerName:null,
      	customerPhone:null,
      };
    }
    componentWillMount() {

    }
    componentDidMount() {
    }
    add() {
    	let { customerName, customerPhone } = this.state;
    	if(!customerName) {
    		// alert('提示', '请输入姓名', [{ text: '确定'}]);
    		alert('请输入姓名');
    		return;
    	};
    	if(!customerPhone) {
    		// alert('提示', '请输入手机号', [{ text: '确定'}]);
    		alert('请输入手机号');
    		return;
    	};
    	if(!Help.checkMobile(customerPhone)) {
    		alert('请输入正确手机号');
    		return;
    	};
      	Request.ajax('customerSurvey/add', {
      		customerName:Help.clearSpace(customerName), 
      		customerPhone:Help.clearSpace(customerPhone), 
      		submitUrl:window.parent.location.href,
      		remark:'',
      	}, (response) => {
	        if(response.success) {
	        	// Toast.success('提交成功', 3);  
	        	alert('提交成功')
	        } else {
	        	// Toast.fail(response.description || '提交失败', 3);  
	        	alert('提交失败')
	        }
      	},(error) => {
	        // Toast.fail('提交失败', 3);
	        alert('提交失败')
	    });
    }
    render() {
    	return (
    		<section className="online-buying">
				<h2 className="title">在线抢购预留</h2>
				<form action="" method="post">
					<div className="form-conponent-wrapper">
						<div className="white-line-bg">
							<div className="form-control-wrapper">
								<div className="form-control__label">
									<span>姓名<i className="form-control__required">*</i></span>
								</div>
								<div className="form-control__container">
									<input className="customerSurveyInput" name='customerName' type="text" placeholder="请输入您的姓名" onChange={(e) => { this.setState({customerName:$.trim($(e.target).val())}) }} />
								</div>
							</div>
						</div>
						<div className="form-control-wrapper">
							<div className="form-control__label">
								<span>手机号<i className="form-control__required">*</i></span>
							</div>
							<div className="form-control__container">
								<input className="customerSurveyInput" name="customerPhone" type="text" placeholder="请输入您的手机号" onChange={(e) => { this.setState({customerPhone:$.trim($(e.target).val())}) }}  />
							</div>
						</div>
					</div>
					<div className="btn-wrapper">
						<button type="button" className="submit-btn" onClick={this.add.bind(this)}>提交获取抢购单号</button>
					</div>
				</form>
			</section>
		)
    }
}

ReactDOM.render(<CustomerSurveyAdd />, document.getElementById('wrapper'));