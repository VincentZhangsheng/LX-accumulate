import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../layout';
import { Enum, PageUrl } from '../../until';
import "../../style/explore.less"

export default class ExploreHome extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    linkTo(url) {
      PageUrl.forward(url)
      
    }
    render() {
        return (
            <Container>
                <div className="alt-explore-page">
                  <div className="atl-wel-page">
                    <img src={require('../../images/explore/wel-page.png')} style={{transition:'opacity 0.5s ease-in',opacity:1}} />
                  </div>
                  <div className="alt-step">
                    <div className="title">
                      <div className="title-main">
                        <span className="i-wave"></span>
                        <span className="text">租机攻略</span>
                        <span className="i-wave"></span>
                      </div>
                    </div>
                    <div className="step-row">
                      <div className="step" onClick={this.linkTo.bind(this,PageUrl.account)}>
                        <div className="left icon1"></div>
                        <div className="right">
                          <div className="right-main">
                            <div className="tit"><em>1.</em>身份认证</div>
                            <div className="des">身份认证完成即可免押金</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="footprint"></div>
                    <div className="step-row">
                      <div className="step" onClick={this.linkTo.bind(this,PageUrl.account+ "#/address")}>
                        <div className="left icon2"></div>
                        <div className="right">
                          <div className="right-main">
                            <div className="tit"><em>2.</em>添加地址</div>
                            <div className="des">为您提供快捷派送服务</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="footprint"></div>
                    <div className="step-row">
                      <div className="step" onClick={this.linkTo.bind(this,PageUrl.product)}>
                        <div className="left icon3"></div>
                        <div className="right">
                          <div className="right-main">
                            <div className="tit"><em>3.</em>选购产品</div>
                            <div className="des">选择您想租用的产品</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="footprint"></div>
                    <div className="step-row last">
                      <div className="step">
                        <div className="left icon4"></div>
                        <div className="right">
                          <div className="right-main">
                            <div className="tit"><em>4.</em>付款派送</div>
                            <div className="des">付款完成后将立即派送</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <FooterTabBar history={this.props.history} chooseTab={Enum.navTab.explore} />
            </Container>
        );
    }
}

ReactDOM.render(<ExploreHome />, document.getElementById('wrapper'));
