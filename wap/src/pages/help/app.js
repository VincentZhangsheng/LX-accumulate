import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom";
import "../../style/help.less"

import HelpHome from "./home.js"
import { MalfuncitonService, ExchangeService, AfterSalesService, Faq } from "./list"

class App extends Component {
  componentWillMount() {
  }
  render() {
    return (
       <Router>
        <div>
          <Route exact path="/" component={HelpHome} />
          <Route path="/malfunciton-service" component={MalfuncitonService} />
          <Route path="/exchange-service" component={ExchangeService} />
          <Route path="/after-sales-service" component={AfterSalesService} />
          <Route path="/faq" component={Faq} />
        </div>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('wrapper'));
