
import React, { Component } from 'react';

const Faq = () => (
	<div className="help-detail">
		<div className="content">
			<div className="title">常见问题</div>

			<div className="title2">1、机器出现故障怎么办？</div>
			<div className="seciotn">
				<p>在非人为损坏的情况下设备出现故障由凌雄租赁负责维保；若为人为损坏，由客户承担维修和更换零件的费用；在无法维修好的情况下，客户需按官网保值价赔偿。</p>
			</div>

			<div className="title2">2、丢失机器怎么办？</div>
			<div className="seciotn">
				<p>若客户将机器丢失，需按官网保值价赔偿。</p>
			</div>

			<div className="title2">3、设备租用到期快递费用由谁承担？</div>
			<div className="seciotn">
				<p>设备租用到期的快递费用由凌雄租赁承担，提前退租的费用由承租人承担。</p>
			</div>

		</div>
	</div>
)

export default Faq;