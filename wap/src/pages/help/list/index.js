export { default as MalfuncitonService } from "./malfunctionService.js"
export { default as ExchangeService } from "./exchangeService.js"
export { default as AfterSalesService } from "./afterSalesService.js"
export { default as Faq } from "./faq.js"