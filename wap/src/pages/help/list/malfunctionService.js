import React, { Component } from 'react';

const MalfuncitonService = () => (
	<div className="help-detail">
		<div className="content">
			<div className="title">上门服务</div>
			<div className="seciotn">
				<p>1、设备租赁期间出现非人为因素造成的故障，提供免费上门服务，不包括软件故障。人为因素造成的故障，所产生的费用由客户承担。</p>
				<p>2、凌雄租赁自有网点内，提供免费上门服务；非自有网点，采用快递的方式，快递费用凌雄租赁承担。 </p>
				<p>售后服务热线：400-678-5432（周一至周六） </p>
			</div>
		</div>
	</div>
)

export default MalfuncitonService;