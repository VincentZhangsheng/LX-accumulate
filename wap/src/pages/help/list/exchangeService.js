import React, { Component } from 'react';

const ExchangeService = () => (
	<div className="help-detail">
		<div className="content">
			<div className="title">退货政策</div>
			<div className="seciotn">
				<p>1、设备正常租赁到期退货且设备不存在人为损坏的情况，可正常退还设备，无额外费用。</p>
				<p>2、设备正常租赁到期退货，若设备存在人为损坏的情况，客户需承担设备维修费用。</p>
				<p>3、设备租赁未到期，需提前退货且设备存在人为损坏的情况，客户需支付设备维修费及并扣除当期租金。</p>
				<p>4、设备租赁未到期，需提前退货且设备不存在人为损坏的情况，客户无需支付违约金，只扣除当期租金。</p>
			</div>
		</div>
	</div>
)

export default ExchangeService;