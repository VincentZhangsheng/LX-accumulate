import React, { Component } from 'react';

const AfterSalesService = () => (
	<div className="help-detail">
		<div className="content">
			<div className="title">售后政策</div>
			<div className="title2">日常响应服务标准：</div>
			<div className="seciotn">
				<p>1、 在接到您电话一分钟内，我们从电子档案中查询出您设备的所有资料，并根据定期维护服务报告，判断故障原因，通知技术人员</p>
				<p>2、 我们的技术人员将在5分钟内回复您的电话，在电话中帮您解答问题或根据情况派出经验丰富的技术人员前往检查故障原因并作出处理。</p>
				<p>3、 我们有资源丰富的耗材和零配件仓库，零配件保证在4 小时内到位（服务网点内），并以最快的速度和可靠的质量完成设备的检测和调试。</p>
				<p>4、 技术人员将在8 个工作小时内完成维修（服务网点内），设备恢复正常。在遇到严重故障时，我们将为您提供整机更换，保证您的工作顺利进行。</p>
			</div>
		</div>
	</div>
)

export default AfterSalesService;