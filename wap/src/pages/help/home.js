import React, { Component } from 'react';
import { FooterTabBar, Container } from '../layout';
import { Enum, PageUrl } from '../../until'
import "../../style/help.less"

export default class HelpHome extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    linkTo(url) {
      PageUrl.forward(url);
    }
    render() {
        return (
            <Container>
                <div className="atl-help-row" style={{marginTop:'0.3rem'}}>
                  <div className="help-bar" onClick={this.linkTo.bind(this,PageUrl.help+"#/malfunciton-service")}>
                    <div className="help-type">上门服务</div>
                    <div className="help-icon icon1"></div>
                  </div>
                </div>
                <div className="atl-help-row">
                  <div className="help-bar" onClick={this.linkTo.bind(this,PageUrl.help+"#/exchange-service")}>
                    <div className="help-type">退货政策</div>
                    <div className="help-icon icon2"></div>
                  </div>
                </div>
               {/* <div className="atl-help-row">
                                 <div className="help-bar" onClick={this.linkTo.bind(this,PageUrl.help+"#/after-sales-service")}>
                                   <div className="help-type">售后政策</div>
                                   <div className="help-icon icon3"></div>
                                 </div>
                               </div>*/}
                <div className="atl-help-row">
                  <div className="help-bar" onClick={this.linkTo.bind(this,PageUrl.help+"#/faq")}>
                    <div className="help-type">常见问题</div>
                    <div className="help-icon icon4"></div>
                  </div>
                </div>
                <FooterTabBar history={this.props.history} chooseTab={Enum.navTab.help} />
            </Container>
        );
    }
}