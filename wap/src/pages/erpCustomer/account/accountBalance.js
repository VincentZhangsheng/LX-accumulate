import React, { Component } from 'react';
import { Container } from '../../layout';
import { Header } from '../../../components';
import { Request, User, PageUrl } from '../../../until';
import { List, Toast, WhiteSpace, Flex, TextareaItem, Grid, Icon } from 'antd-mobile';
import { createForm } from 'rc-form';

const Item = List.Item;

class CustomerInfo extends Component {
    constructor(props) {
      super(props);
      this.state = {
        // user:null,
        customer:null,
      };
    }
    componentDidMount() {
      // this.loadUser();
      this.loadCustomer();
    }
    loadUser() {
      let user = User.getUser();
      this.setState({user:user});
    }
    loadCustomer() {
        this.setState({animating:true});
        Request.ajax('user/getUserCustomerInfo',{},(response) => {
          if(response.success) {
            // User.setUser(response.resultMap.data);
            this.setState({
              customer:response.resultMap.data,
              animating:false,
            })
          } else {
            this.setState({animating:false});
            Toast.fail('加载用户信息失败', 3);  
          }
        },(error) => {
            this.setState({animating:false});
            Toast.fail('加载用户信息失败', 3);
        })
    }
    linkTo(link) {
        PageUrl.forward(link)
    }
    render() {
        let { customer } = this.state;
        let amountData = customer && customer.hasOwnProperty('customerAccount') ? [
          {
            label:'余额',
            amount:customer.customerAccount.balanceAmount,
          },
          {
            label:'冻结金额',
            amount:customer.customerAccount.totalFrozenAmount,
          },
          {
            label:'设备押金金额',
            amount:customer.customerAccount.depositAmount,
          },
          {
            label:'租金押金金额',
            amount:customer.customerAccount.rentDepositAmount,
          }
        ] : new Array();

        return (
            <Container>
                <Header history={this.props.history} title="账户信息" backLink={PageUrl.erpCustomerAccountHome} />
                <Grid data={amountData}
                  className="amount-grid"
                  columnNum={2}
                  renderItem={ dataItem => (
                    <div className="amount-item">
                      <div>
                        <div className="val">￥{dataItem.amount ? dataItem.amount.toFixed(2):'0.00'}</div>
                        <div className="label">
                          <span>{dataItem.label}</span>
                        </div>
                      </div>
                    </div>
                  )}
                />
                <WhiteSpace size="lg" />
                <List>
                   <Item
                      thumb={<Icon type={require('../../../svg/erpCustomer/statement.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.erpCustomerRecharge)}
                    >充值</Item>
                     <Item
                      thumb={<Icon type={require('../../../svg/order.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.erpCustomerRechargeOrder)}
                    >充值记录</Item>
                </List>
            </Container>
        )
    }
}

export default CustomerInfo;