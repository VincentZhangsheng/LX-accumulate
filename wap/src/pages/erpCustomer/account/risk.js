import React, { Component } from 'react';
import { Container } from '../../layout';
import { Header } from '../../../components';
import { Request, User, PageUrl, ErpEnum } from '../../../until';
import { List, Toast, WhiteSpace, Flex, TextareaItem, Grid } from 'antd-mobile';
import { createForm } from 'rc-form';

const Item = List.Item;

class CustomerInfo extends Component {
    constructor(props) {
      super(props);
      this.state = {
        user:null,
      };
    }
    componentDidMount() {
      this.loadUser();
    }
    loadUser() {
      let user = User.getUser();
      this.setState({user:user});
    }
    render() {
        let { user } = this.state, customerRiskManagement = new Object();
        if(user && user.hasOwnProperty('customer')) {
           customerRiskManagement = user.customer.hasOwnProperty('customerRiskManagement') ? user.customer.customerRiskManagement : {};
        }
        console.log(customerRiskManagement);
        return (
            <Container>
                <Header history={this.props.history} title="授信信息" backLink={PageUrl.erpCustomerAccountHome} />
                <List className="customer-base-info">

                  <Item extra={'￥' + (customerRiskManagement.creditAmount ? customerRiskManagement.creditAmount.toFixed(2) : '0.00') }>授信额度</Item>
                  <Item extra={'￥' + (customerRiskManagement.creditAmount ? customerRiskManagement.creditAmountUsed.toFixed(2) : '0.00') }>已用授信额度</Item>

                  <Item extra={customerRiskManagement.depositCycle}>押金期数</Item>
                  <Item extra={customerRiskManagement.paymentCycle}>付款期数</Item>
                  <Item extra={ErpEnum.payMode.getValue(customerRiskManagement.payMode)}>支付方式</Item>

                  <Item extra={customerRiskManagement.appleDepositCycle}>苹果设备押金期数</Item>
                  <Item extra={customerRiskManagement.applePaymentCycle}>苹果设备付款期数</Item>
                  <Item extra={ErpEnum.payMode.getValue(customerRiskManagement.applePayMode)}>苹果设备支付方式</Item>

                  <Item extra={customerRiskManagement.newDepositCycle}>全新设备押金期数</Item>
                  <Item extra={customerRiskManagement.newPaymentCycle}>全新设备付款期数</Item>
                  <Item extra={ErpEnum.payMode.getValue(customerRiskManagement.newPayMode)}>全新设备支付方式</Item>

                </List>
                <WhiteSpace size="lg" />
            </Container>
        )
    }
}

export default CustomerInfo;