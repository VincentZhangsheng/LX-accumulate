import React, { Component } from 'react';
import { Container } from '../../layout';
import { Header } from '../../../components';
import { Request, User, PageUrl } from '../../../until';
import { List, Toast, WhiteSpace, Flex, TextareaItem, Grid } from 'antd-mobile';
import { createForm } from 'rc-form';

const Item = List.Item;

class CustomerInfo extends Component {
    constructor(props) {
      super(props);
      this.state = {
        user:null,
      };
    }
    componentDidMount() {
      this.loadUser();
    }
    loadUser() {
      let user = User.getUser();
      this.setState({user:user});
    }
    render() {
        let { user } = this.state, customerCompany = new Object();
        if(user && user.hasOwnProperty('customer')) {
           customerCompany = user.customer.hasOwnProperty('customerCompany') ? user.customer.customerCompany : {};
        }
        return (
            <Container>
                <Header history={this.props.history} title="基本信息" backLink={PageUrl.erpCustomerAccountHome} />
                <List className="customer-base-info">
                  <Item extra={customerCompany.companyName}>公司名称</Item>
                  <Item extra={customerCompany.connectRealName}>紧急联系人</Item>
                  <Item extra={customerCompany.connectPhone}>紧急联系人手机号</Item>
                  <Item extra={customerCompany.companyName}>公司名称</Item>
                  <Item extra={customerCompany.landline}>座机电话</Item>
                  <Item extra={customerCompany.provinceName}>省份</Item>
                  <Item extra={customerCompany.cityName}>城市</Item>
                  <Item extra={customerCompany.districtName}>地区</Item>
                  <Item extra={customerCompany.address}>详细地址</Item>
                </List>
                <WhiteSpace size="lg" />
            </Container>
        )
    }
}

export default CustomerInfo;