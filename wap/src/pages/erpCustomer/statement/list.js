import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container } from '../../layout';
import { Header } from '../../../components';
import { ErpEnum, Request, WXJSSDK, PageUrl, Help, DateHelp } from '../../../until'
import { ListView, Tabs, Button, Toast, Modal, List } from 'antd-mobile';

const TabPane = Tabs.TabPane;
const alert = Modal.alert;
const Item = List.Item;

class StatementList extends Component {
    constructor(props) {
      super(props);
      const dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.state = {
        searchData:{
          pageNo:1,
          pageSize:4,
          statementOrderStatus:-1,
        },
        dataSource,
        dataList:[],
        refreshing: true,
        hasMore:true,
        height: document.documentElement.clientHeight,
        animating:false,
      };
    }
    linkToDetail(rowData) {
      PageUrl.forward(PageUrl.erpCustomerStatementDetail + "?id=" + rowData.statementOrderNo);
    }
    componentDidMount() {
      this.lv && setTimeout(() => this.setState({
        height: this.state.height - ReactDOM.findDOMNode(this.lv).offsetTop,
      }), 0);
      this.loadData();
    }
    loadData(pageNo) {
      let { searchData, hasMore, dataList } = this.state;
      searchData.pageNo = pageNo || 1;

      if(searchData.statementOrderStatus == -1) {
        delete searchData.statementOrderStatus;
      }

      this.setState({ isLoading: true });
      Request.ajax('erp/queryAllStatementOrder',searchData,(response) => {
        var data = [];
        if(response.success) {
          data = response.resultMap.data.itemList;
          hasMore = searchData.pageNo < response.resultMap.data.pageCount;
          if(searchData.pageNo == 1) {
            dataList = data;
          } else {
            dataList = dataList.concat(data);  
          }
        } else {
          hasMore = false;
          Toast.fail('加载订单信息失败', 3);
        }

        this.setState({
          dataList:dataList,
          dataSource: this.state.dataSource.cloneWithRows(dataList),
          refreshing: false,
          hasMore:hasMore,
          showFinishTxt: true,
          searchData:searchData,
          isLoading:false,
        });
      },(error) => {
        Toast.fail('加载订单信息失败', 3);
      })
    }
    onScroll = (e) => {
      this.scrollerTop = e.scroller.getValues().top;
      this.domScroller = e;
    }
    onEndReached = (event) => {
      if (this.state.isLoading || !this.state.hasMore) {
        return;
      }
      this.setState({ isLoading: true });
      this.loadData(this.state.searchData.pageNo + 1);

    };
    scrollingComplete = () => {
      if (this.scrollerTop >= -1) {
        this.setState({ showFinishTxt: false });
      }
    }
    renderCustomIcon() {
      return [
        <div key="0" className="am-refresh-control-pull">
          <span>{this.state.showFinishTxt ? '刷新完毕' : '下拉可以刷新'}</span>
        </div>,
        <div key="1" className="am-refresh-control-release">
          <span>松开立即刷新</span>
        </div>,
      ];
    }
    createPayOrder(rowData, event) {
      event.preventDefault();
      event.stopPropagation();

      this.setState({animating:true});
      WXJSSDK.createPayStatementOrder(rowData.statementOrderNo, () => {
        this.setState({animating:false});
      }, (res) => {
         PageUrl.forward(PageUrl.erpCustomerStatementReult + '#/success');
      });
    }
    renderListView() {
      const separator = (sectionID, rowID) => (
        <div
          key={sectionID+"-"+rowID}
          style={{
            backgroundColor: '#F5F5F9',
            height: "0.1rem",
            borderTop: '1px solid #ECECED',
            borderBottom: '1px solid #ECECED',
          }}
        />
      );
      const row = (rowData, sectionID, rowID) => {
          return (
            <List key={rowID} className="statement-list-item" onClick={this.linkToDetail.bind(this,rowData)}>
              <Item extra={rowData.statementOrderNo}>编号</Item>
              <Item extra={<span className={ErpEnum.statementOrderStatus.getClass(rowData.statementStatus)}>{ErpEnum.statementOrderStatus.getValue(rowData.statementStatus) }</span>}>
                结算状态
              </Item>
              <Item extra={Help.toFixedPrice(rowData.statementAmount, 2)}>结算单金额</Item>
              <Item extra={Help.toFixedPrice(rowData.statementOverdueAmount, 2)}>逾期金额</Item>
              <Item extra={Help.toFixedPrice(rowData.statementPaidAmount, 2)}>已支付金额</Item>
              <Item extra={DateHelp.shotDate(rowData.statementExpectPayTime)}>预计支付日期</Item>
              {rowData.hasOwnProperty('statementPaidTime') && <Item extra={Help.toFixedPrice(rowData.statementPaidTime, 2)}>支付时间</Item>}
              {
                (rowData.statementStatus != ErpEnum.statementOrderStatus.num.completed && rowData.statementStatus != ErpEnum.statementOrderStatus.num.noSttlement) &&
                <Item>
                  <Button type="ghost" 
                      style={{float:'right'}}
                      size="small" 
                      inline 
                      className="atl-order-row-footer-btns-btn" 
                      onClick={this.createPayOrder.bind(this, rowData)}>
                     支付
                  </Button>
                </Item>
              }
            </List>
          )
      };
      return (
        <ListView
            ref={el => this.lv = el}
            dataSource={this.state.dataSource}
            //renderHeader={() => <span>Pull to refresh</span>}
            renderFooter={() => (<div style={{ padding: "0.3rem", textAlign:"center" }}>{this.state.isLoading ? "加载中..." : ""}</div>)}
            renderRow={row}
            renderSeparator={separator}
            initialListSize={4}
            pageSize={4}
            style={{
              height: this.state.height,
              //border: '1px solid #ddd',
              margin: '0.1rem 0',
            }}
            scrollerOptions={{ scrollbars: true, scrollingComplete: this.scrollingComplete }}
            //onScroll={this.onScroll}
            scrollRenderAheadDistance={200}
            scrollEventThrottle={20}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={10}
          />
      )
    }
    renderTabList() {
      let tabArray = new Array();
      tabArray.push(<TabPane tab={"全部"} key="-1">{this.renderListView()}</TabPane>)
      for (var type in ErpEnum.statementOrderStatus.num) {
        let n = ErpEnum.statementOrderStatus.num[type];
        tabArray.push(<TabPane tab={ErpEnum.statementOrderStatus.getValue(n)} key={n} >{this.renderListView()}</TabPane>)
      }
      return tabArray;
    }
    cahangeTab(status) {
      let { searchData, dataSource } = this.state;
      if(searchData.statementOrderStatus != status) {
        searchData.statementOrderStatus = status;
        searchData.pageNo = 1;
        this.setState({
          searchData:searchData,
        });
        this.loadData();
      }
    }
    render() {
        return (
            <Container animating={this.state.animating} >
                <Header history={this.props.history} title="结算单列表" backLink={PageUrl.erpCustomerAccountHome} />
                <Tabs defaultActiveKey="-1" animated={false} onChange={this.cahangeTab.bind(this)}>
                  {this.renderTabList()}
                </Tabs>
            </Container>
        );
    }
}


ReactDOM.render(<StatementList />, document.getElementById('wrapper'));