import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../../layout';
import { Header } from '../../../components';
import { Request, PageUrl, User, WXJSSDK } from '../../../until'
import { List, InputItem, WingBlank, Button, Toast, Modal } from 'antd-mobile';
import $ from 'zepto';
import { createForm } from 'rc-form';

const alert = Modal.alert;

class Recharge extends Component {
	 constructor(props) {
      super(props);
      this.state = {
      	animating:false,
      	moneyfocused:false,
      };
    }
    createRechargeOrder(event) {
      event.preventDefault();
      event.stopPropagation();

		let formValues = this.props.form.getFieldsValue();

		if(!formValues.hasOwnProperty('money') || (formValues.hasOwnProperty('money') && !formValues.money)) {
			alert('提示', '请输入需要充值的金额', [{ text: '确定'}]);
			return;
		};

		if(parseFloat(formValues.money) <= 0) {
			alert('提示', '充值金额必须大于', [{ text: '确定'}]);
			return;
		}

		this.setState({animating:true});

      WXJSSDK.createRecharegeOrder(formValues.money, () => {
      	this.setState({animating:false});
      }, (res) => {
         PageUrl.forward(PageUrl.erpCustomerRechargeResult + '#/success');
      });
    }
    render() {
    	const { getFieldProps } = this.props.form;
    	return (
    		<Container animating={this.state.animating} >
	    		<Header title="充值" backLink={PageUrl.erpCustomerAccountHome}  />
	    		<List>
		          <InputItem
		            {...getFieldProps('money', {
		              normalize: (v, prev) => {
		                if (v && !/^(([1-9]\d*)|0)(\.\d{0,2}?)?$/.test(v)) {
		                  if (v === '.') {
		                    return '0.';
		                  }
		                  return prev;
		                }
		                return v;
		              },
		            })}
		            type='money'
		            placeholder="请输入充值金额"
		            onFocus={() => {
		              this.setState({
		                moneyfocused: false,
		              });
		            }}
		            focused={this.state.moneyfocused}
		          >充值金额</InputItem>
		        </List>
		        <WingBlank style={{marginTop:"0.5rem"}}>
                  <Button className="btn" type="primary"  onClick={this.createRechargeOrder.bind(this)}>充值</Button>
                </WingBlank>
	    	</Container>
		)
    }
}

const RechargeForm = createForm()(Recharge);

ReactDOM.render(<RechargeForm />, document.getElementById('wrapper'));