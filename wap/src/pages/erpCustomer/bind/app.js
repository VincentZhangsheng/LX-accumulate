import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FooterTabBar, Container } from '../../layout';
import { Header } from '../../../components';
import { Request, PageUrl, User, Help } from '../../../until'
import { List, InputItem, WingBlank, Button, Toast, Modal } from 'antd-mobile';
import $ from 'zepto';

const alert = Modal.alert;

class BindErpCustomer extends Component {
	 constructor(props) {
      super(props);
      this.state = {
      	customerName:null,
      	phone:null,
      	verifyCode:null,
      	timer:null,
      	disabledSendVerifyCodeButton:false,
      	cutNumText:'发送验证码',
      };
    }
    onChange(value) {
	    this.setState({
	      customerName:value,
	    });
  	}
  	sendVerifyCode() {
  		let { customerName, phone } = this.state;
    	if(!customerName && !phone) {
    		alert('提示', '请输入需要绑定的客户名称和手机号', [{ text: '确定'}]);
    		return;
    	};
    	this.setState({disabledSendVerifyCodeButton:true});
      	Request.ajax('erp/sendCheckCode', {customerName:customerName, phone:Help.clearSpace(phone)}, (response) => {
      		this.setState({disabledSendVerifyCodeButton:false});
	        if(response.success) {
	        	Toast.success('发送验证码成功', 3);  
	        	this.cutDown();
	        } else {
	        	// !!this.state.timer && clearTimeout(this.state.timer);
	        	Toast.fail(response.description || '发送验证码失败', 3);  
	        	// 已经绑定直接调转到客户中心
	        	if(response.code == 'J100026') {
	        		PageUrl.forward(PageUrl.aliErpCustomerAccountHome);
	        	}
	        }
      	},(error) => {
      		this.setState({disabledSendVerifyCodeButton:false});
	        Toast.fail('发送验证码失败', 3);
	    });
  	}
  	cutDown() {
  		var cutNum = 60, timer = setInterval(() => {
  			cutNum -= 1;
  			this.setState({
	  			// timer: timer,
	  			cutNumText:cutNum + '秒后可重发',
	  			disabledSendVerifyCodeButton:true,
	  		});

	  		if(cutNum == 0) {
	  			clearInterval(timer);
	  			this.setState({
		  			// timer: timer,
		  			cutNumText:'发送验证码',
		  			disabledSendVerifyCodeButton:false,
		  		})
	  		}

		}, 1000);
  	}
    bindCustomer() {
    	let { customerName, phone, verifyCode } = this.state;
    	if(!customerName) {
    		alert('提示', '请输入需要绑定的客户名称', [{ text: '确定'}]);
    		return;
    	};
    	if(!phone) {
    		alert('提示', '请输入手机号', [{ text: '确定'}]);
    		return;
    	};
    	if(!verifyCode) {
    		alert('提示', '请输入手机号收到验证', [{ text: '确定'}]);
    		return;
    	};
      	Request.ajax('erp/checkAndBind', {customerName:customerName, phone:Help.clearSpace(phone), verifyCode:verifyCode}, (response) => {
	        if(response.success) {
	        	Toast.success('成功绑定', 3);  
	        	User.setUser(response.resultMap.data);
	          	PageUrl.forward(PageUrl.erpCustomerAccountHome);
	        } else {
	        	Toast.fail(response.description || '绑定失败', 3);  
	        	// 已经绑定直接调转到客户中心
	        	if(response.code == 'J100026') {
	        		PageUrl.forward(PageUrl.erpCustomerAccountHome);
	        	}
	        }
      	},(error) => {
	        Toast.fail('绑定失败', 3);
	    });
    }
    render() {
    	let  { disabledSendVerifyCodeButton, cutNumText } = this.state;
    	return (
    		<Container>
	    		<Header title="绑定客户" />
	    		<List renderFooter={ () => '收到验证码后5分钟内有效'}>
	    		<InputItem
		            type="text"
		            placeholder="输入客户名称"
		            onChange={this.onChange.bind(this)}
		            value={this.state.customerName}
		          ></InputItem>
		          <InputItem
		            type="phone"
		            placeholder="输入手机号"
		            onChange={(value) => {
		            	this.setState({
		            		phone:value
		            	})
		            }}
		            value={this.state.phone}
		          ></InputItem>
		          <List.Item
				      extra={<Button type="ghost"  disabled={disabledSendVerifyCodeButton} onClick={this.sendVerifyCode.bind(this)}>{cutNumText}</Button>}
				      multipleLine
				    >
				       <InputItem
				       		style={{paddingLeft:0}}
				            type="text"
				            placeholder="请输入验证码"
				            onChange={(value) => {
				            	this.setState({
				            		verifyCode:value
				            	})
				            }}
				            value={this.state.verifyCode}
				          ></InputItem>
				    </List.Item>
		        </List>
		        <WingBlank style={{marginTop:"0.5rem"}}>
                  <Button className="btn" type="primary"  onClick={this.bindCustomer.bind(this)}>绑定</Button>
                </WingBlank>
	    	</Container>
		)
    }
}

ReactDOM.render(<BindErpCustomer />, document.getElementById('wrapper'));