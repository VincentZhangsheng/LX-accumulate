import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom";

import AccountHome from "./home.js"
import Address from "./address.js"
import EditAddress from "./editAddress.js"
import Indentity from "./identity.js"
import ViewIndentity from "./viewIdentity.js"

import "../../style/account.less"

class App extends Component {
  componentWillMount() {

  }
  render() {
    return (
       <Router>
         <div>
           <Route exact path="/" component={AccountHome} />

           <Route path="/identity" component={Indentity} />
           <Route path="/viewIdentity" component={ViewIndentity} />

           <Route path="/address" component={Address} />
           <Route path="/editAddress/:type/:id" component={EditAddress} />
         </div>

      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('wrapper'));
