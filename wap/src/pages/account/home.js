import React, { Component } from 'react';
import { FooterTabBar, Container } from '../layout';
import { Enum, Request, User, PageUrl } from '../../until'
import { List, Icon, WhiteSpace,Toast } from 'antd-mobile';

const Item = List.Item;
const Brief = Item.Brief;


export default class AccountHome extends Component {
    constructor(props) {
      super(props);
      this.state = {
        user:{},
        animating:false,
      };
    }
    componentDidMount() {
      this.loadUser();
    }
    loadUser() {
      let user = User.getUser();
      if(user) {
        this.setState({
          user:user
        })
      } else  {
        this.setState({animating:true});
        Request.ajax('user/getUserInfo',{},(response) => {
          if(response.success) {
            User.setUser(response.resultMap.data);
            this.setState({
              user:response.resultMap.data,
              animating:false,
            })
          } else {
            this.setState({animating:false});
            Toast.fail('加载用户信息失败', 3);  
          }
        },(error) => {
            this.setState({animating:false});
            Toast.fail('加载用户信息失败', 3);
        })
      }
      
    }
    linkTo(link) {
        //this.props.history.push(link);
        PageUrl.forward(link)
    }
    render() {
      let { user, animating } = this.state;
      let hasUserWx = user.hasOwnProperty('userPersonWeixin');
      let hasPersionInfo = user.hasOwnProperty('userCollegePersonInfo');
      let notShowPersionInfo = hasPersionInfo && (user.userCollegePersonInfo.verifyStatus == Enum.verifyStatus.num.commited || user.userCollegePersonInfo.verifyStatus == Enum.verifyStatus.num.verified); 
        return (
            <Container animating={animating} initWeChatConfig={true}>
                {
                  hasUserWx &&
                  <div className="atl-account-header">
                      <div className="atl-account-header-avatar">
                          <img src={ user.userPersonWeixin.headimgurl} alt="icon" />
                      </div>
                      <div className="atl-account-header-text">
                        <div style={{ marginBottom: '0.16rem', fontWeight: 'bold' }}>{user.userPersonWeixin.nickname}</div>
                      </div>
                  </div>
                }
                <WhiteSpace size="lg" />
                <List>
                   <Item
                     thumb={<Icon type={require('../../svg/identity.svg')} size="sm" />}
                     arrow="horizontal"
                     onClick={this.linkTo.bind(this,PageUrl.account + (notShowPersionInfo ? '#/viewIdentity' : '#/identity'))}
                     extra={hasPersionInfo?Enum.verifyStatus.getValue(user.userCollegePersonInfo.verifyStatus):''}
                   >
                     身份认证
                   </Item>
                 </List>
                  <WhiteSpace size="lg" />
                  <List>
                    <Item
                      thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.account + '#/address')}
                    >收货地址</Item>
                    <Item
                      thumb={<Icon type={require('../../svg/order.svg')} size="sm" />}
                      arrow="horizontal"
                      onClick={this.linkTo.bind(this,PageUrl.orderList)}
                    >我的订单</Item>
                  </List>
                <FooterTabBar history={this.props.history} chooseTab={Enum.navTab.account} />
            </Container>
        );
    }
}