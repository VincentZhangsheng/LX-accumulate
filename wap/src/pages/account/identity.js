import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Enum, Request, Help, User, PageUrl } from '../../until';
import { List, WingBlank, Button, InputItem, Toast, Modal, WhiteSpace, Icon, Flex, TextareaItem } from 'antd-mobile';
import { createForm } from 'rc-form';

const alert = Modal.alert;


class Indentity extends Component {
    constructor(props) {
      super(props);
      this.state = {
        idCardFrontUrl:null, //身份证正面
        idCardBackUrl:null, //身份证反面
        studentCardFrontUrl:null, //学生证正面
        studentCardBackUrl:null, //学生证内容面
        otherIdCardUrl:null, //手持身份证
        imgDomain:null,
        type:null,
        animating:false,
        btnLoading:false,
        userCollegePersonInfo:{},
      };
    }
    componentDidMount() {
      this.loadUser();
    }
    loadUser() {
      this.setState({animating:true});
      Request.ajax('user/getUserInfo',{},(response) => {
        this.setState({animating:false});
        if(response.success) {
          User.setUser(response.resultMap.data);
          this.setData(response.resultMap.data);
        } else {
          Toast.fail('加载用户信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载用户信息失败', 3);
      })
    }
    setData(user) {
      if(user.hasOwnProperty('userCollegePersonInfo')) {
        let p = user.userCollegePersonInfo;
        this.setState({
          idCardFrontUrl:p.idCardFrontUrl, //身份证正面
          idCardBackUrl:p.idCardBackUrl, //身份证反面
          studentCardFrontUrl:p.studentCardFrontUrl, //学生证正面
          studentCardBackUrl:p.studentCardBackUrl, //学生证内容面
          otherIdCardUrl:p.otherIdCardUrl, //手持身份证
          imgDomain:p.imgDomain,
          userCollegePersonInfo:p || {},
          animating:false,
        })
      }
    }
    chooseImage(type) {
      console.log(type)
      this.setState({type:type});
      window.wx.chooseImage({
        count:1,
        success: (res) => {
          let localId = res.localIds;
          this.uploadImage(localId);
        }
      });
    }
    uploadImage(localId) {
      window.wx.uploadImage({
        localId: localId[0],
        success:(res) => {
         let serverId = res.serverId;
         this.downImage(serverId);
        },
        fail:(res) => {
          Toast.fail('上传图片失败', 3);  
        }
      });
    }
    downImage(serverId) {
      this.setState({animating:true});
      Request.ajax('image/downloadWXDisposableImage',{serverId:serverId},(response) => {
          if(response.success) {
            if(response.resultMap.hasOwnProperty('data') && response.resultMap.data.hasOwnProperty('imgUrl')){
              this.state[this.state.type] = response.resultMap.data.imgUrl;   
              this.state.imgDomain = response.resultMap.data.imgDomain;
              this.state.animating = false;
              this.setState(this.state);
            } else {
              this.setState({animating:false})
              Toast.fail('上传图片失败', 3);  
            }
          } else {
            this.setState({animating:false})
            Toast.fail('上传图片失败', 3);  
          }
      },(error) => {
          this.setState({animating:false})
          Toast.fail('上传图片失败', 3);
      })
    }
    commit() {
      this.save();
    }
    save() {
      var formValues = this.props.form.getFieldsValue();

      if(!Help.checkIsNotNull(formValues.realName) || Help.clearSpace(formValues.realName) == "") {
          alert('提示', '请输入真实姓名', [{ text: '确定'}]);
          return;
      }

      if(!Help.checkIsNotNull(formValues.idCardNo) || Help.clearSpace(formValues.idCardNo) == "") {
          alert('提示', '请输入身份证号码', [{ text: '确定'}]);
          return;
      }

      if(!Help.checkIDCard(formValues.idCardNo)) {
        alert('提示', '请输入正确的身份证号码', [{ text: '确定'}]);
        return;
      }

      if(!Help.checkIsNotNull(formValues.schoolName) || Help.clearSpace(formValues.schoolName) == "") {
          alert('提示', '请输入学校名称', [{ text: '确定'}]);
          return;
      }

      if(!Help.checkIsNotNull(formValues.studentCardNo) || Help.clearSpace(formValues.studentCardNo) == "") {
          alert('提示', '请输入学生证编号', [{ text: '确定'}]);
          return;
      }

      if(!Help.checkIsNotNull(formValues.address) || Help.clearSpace(formValues.address) == "") {
          alert('提示', '请输入详细地址', [{ text: '确定'}]);
          return;
      }

      let { idCardFrontUrl, idCardBackUrl, studentCardFrontUrl, studentCardBackUrl, otherIdCardUrl  } = this.state;

      if(!idCardFrontUrl) {
          alert('提示', '请上传身份证正面照', [{ text: '确定'}]);
          return;
      }
      if(!idCardBackUrl) {
          alert('提示', '请上传身份证反面照', [{ text: '确定'}]);
          return;
      }
      if(!studentCardFrontUrl) {
          alert('提示', '请上传学生证正面照', [{ text: '确定'}]);
          return;
      }
      if(!studentCardBackUrl) {
          alert('提示', '请上传学生证反面照', [{ text: '确定'}]);
          return;
      }

       let commitData = {
        realName:Help.clearSpace(formValues.realName),
        idCardNo:Help.clearSpace(formValues.idCardNo),
        schoolName:Help.clearSpace(formValues.schoolName),
        studentCardNo:Help.clearSpace(formValues.studentCardNo),
        address:Help.clearSpace(formValues.address),
        idCardFrontUrl:idCardFrontUrl,
        idCardBackUrl:idCardBackUrl,
        studentCardFrontUrl:studentCardFrontUrl,
        studentCardBackUrl:studentCardBackUrl,
        otherIdCardUrl:otherIdCardUrl,
      }
      this.setState({btnLoading:true});
      Request.ajax('user/updateUserCollegePersonInfo',commitData,(response) => {
          this.setState({btnLoading:false});
          if(response.success) {
            this.loadUser();
            this.confirmSumitAudit();
          } else {
            Toast.fail('保存失败', 3);  
          }
      },(error) => {
          this.setState({btnLoading:false});
          Toast.fail('保存失败', 3);
      })
    }
    confirmSumitAudit(message,btntext) {
      alert('提交审核', message || '提交审核认证信息', [{ text: '取消'},{ text: btntext || '确定',onPress:() => {
        this.sumitAudit()
      }}]);
    }
    sumitAudit() {
      Request.ajax('user/commitUserCollegePersonInfo',{},(response) => {
          if(response.success) {
            Toast.success('提交审核成功', 3);  
            PageUrl.forward(PageUrl.account+ '#/viewIdentity');
          } else { 
            this.confirmSumitAudit(response.description || '提交审核失败','继续提交');
          }
      },(error) => {
          this.confirmSumitAudit('提交审核失败','继续提交');
      })
    }
    render() {
       const { getFieldProps } = this.props.form;
       const { imgDomain, idCardFrontUrl, idCardBackUrl, studentCardFrontUrl, studentCardBackUrl, otherIdCardUrl , animating, userCollegePersonInfo, btnLoading  } = this.state;
        return (
            <Container animating={animating}>
                <Header history={this.props.history} title="身份认证" backLink={PageUrl.account} />
                <List>
                  <InputItem
                    {...getFieldProps('realName',{
                      initialValue:userCollegePersonInfo.realName
                    })}
                    placeholder="请输入真实姓名"
                  >真实姓名</InputItem>
                  <InputItem
                    {...getFieldProps('idCardNo',{
                      initialValue:userCollegePersonInfo.idCardNo
                    })}
                    type="number"
                    placeholder="请输入身份证号码"
                  >身份证号</InputItem>
                  <InputItem
                    {...getFieldProps('schoolName',{
                      initialValue:userCollegePersonInfo.schoolName
                    })}
                    placeholder="请输入学校名称"
                  >学校名称</InputItem>
                  <InputItem
                    {...getFieldProps('studentCardNo',{
                      initialValue:userCollegePersonInfo.studentCardNo
                    })}
                    placeholder="请输入学生证编号"
                  >学生证编号</InputItem>
                  <TextareaItem
                      {...getFieldProps('address',{
                        initialValue:userCollegePersonInfo.address
                      })}
                      rows={3}
                      placeholder="请填写详细地址"
                   />
                </List>
                <List renderHeader={'身份证'}>
                  <List.Item>
                    <Flex className="atl-identity-imgbox">
                      {
                        !!imgDomain && !!idCardFrontUrl &&
                        <Flex.Item onClick={this.chooseImage.bind(this,'idCardFrontUrl')}>
                          <div className="imgbox"><img src={imgDomain + idCardFrontUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !(!!imgDomain && !!idCardFrontUrl)  &&
                        <Flex.Item className="box" onClick={this.chooseImage.bind(this,'idCardFrontUrl')}>
                         <div>
                          <Icon type={require('../../svg/camera.svg')} size="sm" />
                          <div className="des">正面照</div>
                         </div>
                        </Flex.Item>
                      }
                      {
                        !!imgDomain && !!idCardBackUrl &&
                        <Flex.Item onClick={this.chooseImage.bind(this,'idCardBackUrl')}>
                          <div className="imgbox"><img src={imgDomain + idCardBackUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !(!!imgDomain && !!idCardBackUrl)  &&
                        <Flex.Item className="box" onClick={this.chooseImage.bind(this,'idCardBackUrl')}>
                         <div>
                          <Icon type={require('../../svg/camera.svg')} size="sm" />
                          <div className="des">反面照</div>
                         </div>
                        </Flex.Item>
                      }
                      {
                        !!imgDomain && !!otherIdCardUrl &&
                        <Flex.Item onClick={this.chooseImage.bind(this,'otherIdCardUrl')}>
                          <div className="imgbox"><img src={imgDomain + otherIdCardUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !(!!imgDomain && !!otherIdCardUrl)  &&
                        <Flex.Item className="box" onClick={this.chooseImage.bind(this,'otherIdCardUrl')}>
                         <div>
                          <Icon type={require('../../svg/camera.svg')} size="sm" />
                          <div className="des">手持身份证</div>
                         </div>
                        </Flex.Item>
                      }
                    </Flex>
                  </List.Item>
                </List>
                 <List renderHeader={'学生证'}>
                  <List.Item>
                    <Flex className="atl-identity-imgbox">
                      {
                        !!imgDomain && !!studentCardFrontUrl &&
                        <Flex.Item onClick={this.chooseImage.bind(this,'studentCardFrontUrl')}>
                          <div className="imgbox"><img src={imgDomain + studentCardFrontUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !(!!imgDomain && !!studentCardFrontUrl)  &&
                        <Flex.Item className="box" onClick={this.chooseImage.bind(this,'studentCardFrontUrl')}>
                         <div>
                           <Icon type={require('../../svg/camera.svg')} size="sm" />
                          <div className="des">正面照</div>
                         </div>
                        </Flex.Item>
                      }
                      {
                        !!imgDomain && !!studentCardBackUrl &&
                        <Flex.Item onClick={this.chooseImage.bind(this,'studentCardBackUrl')}>
                          <div className="imgbox"><img src={imgDomain + studentCardBackUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !(!!imgDomain && !!studentCardBackUrl)  &&
                        <Flex.Item className="box" onClick={this.chooseImage.bind(this,'studentCardBackUrl')}>
                         <div>
                           <Icon type={require('../../svg/camera.svg')} size="sm" />
                            <div className="des">内容页</div>
                         </div>
                        </Flex.Item>
                      }
                    </Flex>
                  </List.Item>
                </List>
                 <WhiteSpace size="lg" />
                <WingBlank>
                  <Button className="btn" type="primary"  onClick={this.commit.bind(this)} loading={btnLoading} disabled={btnLoading}>{btnLoading ? "正在保存..." : "保存"}</Button>
                </WingBlank>
                <WhiteSpace size="lg" />
            </Container>
        )
    }
}

export default createForm()(Indentity);