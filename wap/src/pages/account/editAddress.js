import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { List, WingBlank, Button, Picker, TextareaItem, InputItem, Toast, Switch, Modal} from 'antd-mobile';
import { createForm } from 'rc-form';
import { Request, AreaDictionary, Enum, Help, PageUrl } from '../../until'

const alert = Modal.alert;

const school = [ 
  {
    label: '深圳大学',
    value: '深圳大学',
  },
  {
    label: '深圳职业技术学院',
    value: '深圳职业技术学院',
  }
];

class EditAddress extends Component {
    constructor(props) {
      super(props);
      
      this.state = {
        type:this.props.match.params.type,
        id:this.props.match.params.id,
        data:{},
        area:['province','city','district'],
        areaDictionary:[],
        choosedAreaValue:[300019, 300265, 302531],
        choosedSchool:['深圳大学'],
        animating:false,
      };
    }
    componentDidMount() {
      this.loadAreaDictionary();
      if(this.state.type == 'edit') {
        this.loadData();
      }
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('user/queryUserConsignInfo',{pageNo:1,pageSize:100,consignId:this.state.id},(response) => {
        this.setState({animating:false});
        if(response.success) {
          try{
            this.setStateData(response.resultMap.data.itemList[0])
          } catch(e) {
          }
        } else {
          Toast.fail('加载收获地址失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载收获失败', 3);
      });
    }
    setStateData(data) {
      var choosedAreaValue = new Array();
      if(data.province) {
        choosedAreaValue[0] = data.province;
      }
      if(data.city) {
        choosedAreaValue[1] = data.city;
      }
      if(data.district) {
        choosedAreaValue[2] = data.district;
      }

      let address = data.address, { choosedSchool } = this.state;
      school.forEach(function(item,i) {
        if(address.indexOf(item.value) > -1) {
          choosedSchool = new Array();
          choosedSchool.push(item.value)
          address = address.substring(item.value.length);
          return;
        }
      })

      data.address = address;
      
      this.setState({
        choosedSchool:choosedSchool,
        choosedAreaValue:choosedAreaValue,
        data:data
      })
    }
    loadAreaDictionary() {
      var areaDictionary = AreaDictionary.get();
      if(areaDictionary) {
        this.setState({areaDictionary:areaDictionary})
      } else {
        Request.ajax('data/findDataByType',{dataType:Enum.dictionaryType.num.area},(response) => {
          if(response.success) {
            try {
              this.setState({areaDictionary:response.resultMap.data});
              AreaDictionary.set(response.resultMap.data);
            } catch(e) {
            }
          } else {
            Toast.fail('加载地区列表失败', 3);  
          }
        },(error) => {
            Toast.fail('加载地区列表失败', 3);
        })
      }
    }
    commit() {
      if(this.state.type == 'edit') {
        this.edit();  
      } else {
        this.add();
      }
      
    }
    getFormData() {
       let { area } = this.state;
       var formValues = this.props.form.getFieldsValue();

       if(formValues.hasOwnProperty('areaDictionary')  && formValues.areaDictionary.length == 0) {
          alert('提示', '请选择地区', [{ text: '确定'}]);
          return;
       }

       if(!Help.checkIsNotNull(formValues.address) || Help.clearSpace(formValues.address) == "") {
          alert('提示', '请输入详细地址', [{ text: '确定'}]);
          return;
       }

       if(!Help.checkIsNotNull(formValues.consigneeName) || Help.clearSpace(formValues.consigneeName) == "") {
          alert('提示', '请输入收件人姓名', [{ text: '确定'}]);
          return;
       }

       if(!Help.checkIsNotNull(formValues.consigneePhone) ||Help.clearSpace(formValues.consigneePhone) == "") {
          alert('提示', '请输入收件人手机号', [{ text: '确定'}]);
          return;
       }

       if(!Help.checkMobile(Help.clearSpace(formValues.consigneePhone))) {
          alert('提示', '请输入正确手机号', [{ text: '确定'}]);
          return;
       }

       var commitData = {
          consigneeName:Help.clearSpace(formValues.consigneeName),
          consigneePhone:Help.clearSpace(formValues.consigneePhone),
          address:formValues.school + Help.clearSpace(formValues.address),
          isMain:formValues.isMain ? 1 : 0,
        };

      if(formValues.hasOwnProperty('areaDictionary')) {
        formValues.areaDictionary.forEach((item,i) => {
          commitData[area[i]] = item;
        })
      }

      return commitData;
    }
    edit() {
      let commitData = this.getFormData();
      commitData.consignId = this.state.data.consignId;

      Request.ajax('user/updateUserConsignInfo',commitData,(response) => {
        if(response.success) {
          Toast.success('修改成功', 3);  
          PageUrl.back();
        } else {
          Toast.fail('修改失败', 3);  
        }
      },(error) => {
          Toast.fail('修改失败', 3);
      });
    }
    add() {
       let commitData = this.getFormData();
       Request.ajax('user/addUserConsignInfo',commitData,(response) => {
        if(response.success) {
          Toast.success('成功添加', 3);  
          PageUrl.back();
        } else {
          Toast.fail('添加失败', 3);  
        }
      },(error) => {
          Toast.fail('添加失败', 3);
      });
    }
    render() {
        const { getFieldProps } = this.props.form;
        let { data, areaDictionary, choosedAreaValue, choosedSchool, animating } = this.state;
        return (
            <Container animating={animating}>
                <Header history={this.props.history} title="收货地址" />
                <List className="atl-edit-address">
                  <Picker extra="请选择"
                    cols={3}
                    data={areaDictionary}
                    title="选择地区"
                    {...getFieldProps('areaDictionary',{
                      initialValue:choosedAreaValue
                    })}
                    onOk={e => console.log('ok', e)}
                    onDismiss={e => console.log('dismiss', e)}
                  >
                    <List.Item arrow="horizontal">选择地区</List.Item>
                  </Picker>
                  
                  <Picker extra="请选择学校"
                    cols={1}
                    data={school}
                    title="选择学校"
                    {...getFieldProps('school', {
                      initialValue: choosedSchool,
                    })}
                    onOk={e => console.log('ok', e)}
                    onDismiss={e => console.log('dismiss', e)}
                  >
                    <List.Item arrow="horizontal">选择学校</List.Item>
                  </Picker>
                   <TextareaItem
                      {...getFieldProps('address',{
                        initialValue:data.address
                      })}
                      rows={3}
                      placeholder="请填写详细地址"
                   />
                   <InputItem
                    {...getFieldProps('consigneeName',{
                      initialValue:data.consigneeName
                    })}
                    placeholder="请输入收件人姓名"
                  >收件人</InputItem>
                  <InputItem
                    {...getFieldProps('consigneePhone',{
                      initialValue:data.consigneePhone
                    })}
                    type="phone"
                    placeholder="请输入手机号"
                  >手机号码</InputItem>
                  <List.Item
                    extra={<Switch {...getFieldProps('isMain', { initialValue: data.isMain == 1, valuePropName: 'checked' })} />}
                  >默认地址</List.Item>
                </List>
                <WingBlank style={{marginTop:"0.5rem"}}>
                  <Button className="btn" type="primary"  onClick={this.commit.bind(this)}>保存</Button>
                </WingBlank>
            </Container>
        );
    }
};


export default createForm()(EditAddress);