import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Request, User, PageUrl } from '../../until';
import { List, Toast, WhiteSpace, Flex, TextareaItem } from 'antd-mobile';
import { createForm } from 'rc-form';

class ViewIndentity extends Component {
    constructor(props) {
      super(props);
      this.state = {
        idCardFrontUrl:null, //身份证正面
        idCardBackUrl:null, //身份证反面
        studentCardFrontUrl:null, //学生证正面
        studentCardBackUrl:null, //学生证内容面
        otherIdCardUrl:null, //手持身份证
        imgDomain:null,
        type:null,
        animating:false,
        userCollegePersonInfo:{},
      };
    }
    componentDidMount() {
      this.loadUser();
    }
    loadUser() {
      this.setState({animating:true});
      Request.ajax('user/getUserInfo',{},(response) => {
        this.setState({animating:false});
        if(response.success) {
          User.setUser(response.resultMap.data);
          this.setData(response.resultMap.data);
        } else {
          Toast.fail('加载用户信息失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载用户信息失败', 3);
      })
    }
    setData(user) {
      if(user.hasOwnProperty('userCollegePersonInfo')) {
        let p = user.userCollegePersonInfo;
        this.setState({
          idCardFrontUrl:p.idCardFrontUrl, //身份证正面
          idCardBackUrl:p.idCardBackUrl, //身份证反面
          studentCardFrontUrl:p.studentCardFrontUrl, //学生证正面
          studentCardBackUrl:p.studentCardBackUrl, //学生证内容面
          otherIdCardUrl:p.otherIdCardUrl, //手持身份证
          imgDomain:p.imgDomain,
          userCollegePersonInfo:p || {},
          animating:false,
        })
      }
    }
    render() {
       const { getFieldProps } = this.props.form;
       const { imgDomain, idCardFrontUrl, idCardBackUrl, studentCardFrontUrl, studentCardBackUrl, otherIdCardUrl , animating, userCollegePersonInfo  } = this.state;
        return (
            <Container animating={animating}>
                <Header history={this.props.history} title="身份认证" backLink={PageUrl.account} />
                <List className="alt-identity-veiwlist">
                  <List.Item extra={userCollegePersonInfo.realName}>真实姓名</List.Item>
                  <List.Item extra={userCollegePersonInfo.idCardNo}>身份证号</List.Item>
                  <List.Item extra={userCollegePersonInfo.schoolName}>学校名称</List.Item>
                  <List.Item extra={userCollegePersonInfo.studentCardNo}>学生证编号</List.Item>
                  <TextareaItem
                      {...getFieldProps('address',{
                        initialValue:userCollegePersonInfo.address
                      })}
                      rows={3}
                      disabled
                      placeholder="请填写详细地址"
                   />
                </List>
                <List renderHeader={'身份证'}>
                  <List.Item>
                    <Flex className="atl-identity-imgbox">
                      {
                        !!imgDomain && !!idCardFrontUrl &&
                        <Flex.Item>
                          <div className="imgbox"><img src={imgDomain + idCardFrontUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !!imgDomain && !!idCardBackUrl &&
                        <Flex.Item>
                          <div className="imgbox"><img src={imgDomain + idCardBackUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !!imgDomain && !!otherIdCardUrl &&
                        <Flex.Item>
                          <div className="imgbox"><img src={imgDomain + otherIdCardUrl} /></div>
                        </Flex.Item>
                      }
                    </Flex>
                  </List.Item>
                </List>
                 <List renderHeader={'学生证'}>
                  <List.Item>
                    <Flex className="atl-identity-imgbox">
                      {
                        !!imgDomain && !!studentCardFrontUrl &&
                        <Flex.Item>
                          <div className="imgbox"><img src={imgDomain + studentCardFrontUrl} /></div>
                        </Flex.Item>
                      }
                      {
                        !!imgDomain && !!studentCardBackUrl &&
                        <Flex.Item>
                          <div className="imgbox"><img src={imgDomain + studentCardBackUrl} /></div>
                        </Flex.Item>
                      }
                    </Flex>
                  </List.Item>
                </List>
                <WhiteSpace size="lg" />
            </Container>
        )
    }
}

export default createForm()(ViewIndentity);