import React, { Component } from 'react';
import { Container } from '../layout';
import { Header } from '../../components';
import { Request, PageUrl } from '../../until'
import { List, WingBlank, Button, Icon, Toast, SwipeAction, Modal } from 'antd-mobile';

const alert = Modal.alert;
const Item = List.Item;

export default class Address extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data:null,
        animating:false
      };
    }
    componentDidMount() {
      this.loadData();
    }
    linkToEdit(address) {
      this.props.history.push({
        pathname: '/editAddress/edit/' + address.consignId,
        state:address,
      });
    }
    loadData() {
      this.setState({animating:true});
      Request.ajax('user/queryUserConsignInfo',{pageNo:1,pageSize:100},(response) => {
        if(response.success) {
          this.setState({
            data:response.resultMap.data,
            animating:false,
          })
        } else {
          this.setState({animating:false});
          Toast.fail('加载收获地址失败', 3);  
        }
      },(error) => {
          this.setState({animating:false});
          Toast.fail('加载收获失败', 3);
      })
    }
    delete(item) {
      alert('确认', '确定删除吗？', [{ text: '取消'},{ text: '确定',onPress:() => {
        Request.ajax('user/deleteUserConsignInfo',{consignId:item.consignId},(response) => {
          if(response.success) {
            Toast.success('成功删除', 1);  
            this.loadData();
          } else {
            Toast.fail('删除失败', 3);  
          }
        },(error) => {
            Toast.fail('删除添加失败', 3);
        });
      }}]);
    }
    render() {
        let { data, animating } = this.state;
        return (
            <Container animating={animating}>
                {
                  data && 
                  <div>
                    <Header history={this.props.history} title="收货地址" backLink={PageUrl.account} />
                    <List className="my-list">
                      {
                        ((data.hasOwnProperty('itemList') && data.itemList)  || []).map((item,i) => {
                            return (
                              <SwipeAction
                                key={i+'-'+item.consignId}
                                style={{ backgroundColor: 'gray' }}
                                autoClose
                                right={[
                                  {
                                    text: '取消',
                                    //onPress: () => console.log('cancel'),
                                    style: { backgroundColor: '#ddd', color: 'white' },
                                  },
                                  {
                                    text: '删除',
                                    onPress:this.delete.bind(this,item),
                                    style: { backgroundColor: '#F4333C', color: 'white' },
                                  },
                                ]}
                                onOpen={() => console.log('global open')}
                                onClose={() => console.log('global close')}
                              >
                                <Item
                                  thumb={<Icon type={require('../../svg/address.svg')} size="sm" />}
                                  arrow="horizontal"
                                  multipleLine
                                  onClick={this.linkToEdit.bind(this,item)}
                                  platform="android"
                                  wrap={true}
                                >
                                  {item.consigneeName} {item.consigneePhone}                
                                  <div className="alt-list-brief">{item.provinceName}{item.cityName}{item.districtName}{item.address}</div>
                                </Item>
                              </SwipeAction>
                            )
                        })
                      }
                    </List>
                    <WingBlank style={{marginTop:"0.5rem"}}>
                      <Button className="btn" type="primary"  onClick={e => {
                        this.props.history.push({
                          pathname: '/editAddress/add/0'
                        });
                      }}>新增地址</Button>
                    </WingBlank>
                  </div>
                }
            </Container>
        );
    }
}