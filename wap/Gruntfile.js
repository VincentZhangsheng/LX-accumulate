var rd = require('rd');

module.exports = function(grunt) {
    grunt.registerTask('booter', 'copy booter', function(arg) {
        var project = require('./package.json');
        var os = require('os');
        var fs = require('fs');
        var ip = "127.0.0.1";

        var network = os.networkInterfaces();
        for(var key in network) {
            if(network.hasOwnProperty(key)) {
                var linker = network[key];
                for(var i=0;i<linker.length;i++) {
                    if(linker[i].family.toLowerCase() == 'ipv4' && linker[i].address != ip) {
                        ip = linker[i].address;
                        break;
                    }
                }
            }
            if(ip != '127.0.0.1') {
                break;
            }
        }

        var port = project.name.split("").reduce(function(prev, next) {
                if(typeof prev == 'string') {
                    return prev.charCodeAt(0) + next.charCodeAt(0);
                }
                return prev + next.charCodeAt(0);
            }) + 10000;

        var local = "http://" + ip + ":" + port;

        var booter = fs.readFileSync('./src/booter.js', 'utf-8');
        if(!fs.existsSync('./build/static/js')) {
            fs.mkdirSync('./build/static/js');
        }

        booter = booter.replace(/http\:\/\/127\.0\.0\.1:8007/ig, local).replace(/var\sversion\s=\snew\sDate\(\)\.getFullYear\(\);/ig, "var version=" + Date.now() + ";");

        if(arg && (arg == 'advance' || arg == 'product' || arg == 'test' || arg == 'test2')) {
            var allFiles = rd.readFileSync('./build/static/js');
            var pages = {};
            for(i=0;i<allFiles.length;i++) {
                if((/\.js$/).test(allFiles[i])) {
                    var match = allFiles[i].replace(/[\\]/g, '/').match(/static\/js\/(.*)\.(.*)\.js$/);
                    if(match && match.length >= 3) {
                        pages[match[1]] = [match[1], match[2], 'js'].join('.');
                    }
                }
            }

            booter = booter.replace('var pages = null;', 'var pages = ' + JSON.stringify(pages));
        }

        fs.writeFileSync('./build/static/js/booter.js', booter);
    });
};
